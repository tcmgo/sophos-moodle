/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.entidade.enumeracao.EnumeracaoTipo;
import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoArquivo;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.primefaces.model.UploadedFile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "anexo")
@TypeDefs({ @TypeDef(name = "tipoArquivo", typeClass = EnumeracaoTipo.class, parameters = {
		@Parameter(name = "enumClassName", value = "br.gov.go.tcm.sophos.entidade.enumeracao.TipoArquivo") }) })
@Banco(nome = Banco.SOPHOS)
public class Anexo extends EntidadePadrao {

	private static final long serialVersionUID = -7595815654521989974L;

	@Column(name = "descricao")
	private String descricao;

	@Column(name = "tipo_arquivo")
	@Type(type = "tipoArquivo")
	private TipoArquivo tipoArquivo;

	@Column(name = "arquivo_ged")
	private String arquivoGED;

	@Transient
	private UploadedFile file;

	@Transient
	private byte[] bytes;

	public Anexo() {
		super();
	}

	public Anexo(String descricao, TipoArquivo tipoArquivo, String arquivoGED) {
		super();
		this.descricao = descricao;
		this.tipoArquivo = tipoArquivo;
		this.arquivoGED = arquivoGED;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public TipoArquivo getTipoArquivo() {
		return tipoArquivo;
	}

	public void setTipoArquivo(TipoArquivo tipoArquivo) {
		this.tipoArquivo = tipoArquivo;
	}

	public String getArquivoGED() {
		return arquivoGED;
	}

	public void setArquivoGED(String arquivoGED) {
		this.arquivoGED = arquivoGED;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

}
