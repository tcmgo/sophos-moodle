/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.entidade.base.EntidadeLogica;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "alerta")
@Banco(nome = Banco.SOPHOS)
public class Alerta extends EntidadeLogica {

	private static final long serialVersionUID = 3958336940990802481L;
	
	@Column(name = "data_visualizacao")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataVisualizacao;
	
	@JoinColumn(name = "tipo_id", referencedColumnName = "id")
	@ManyToOne(optional = true)
	private Dominio tipo;
	
	@Column(name = "mensagem", length = 2000, nullable = true)
	private String mensagem;
	
	@JoinColumn(name = "participante_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Participante participante;
	
	@JoinColumn(name = "pacote_alertas_id", referencedColumnName = "id")
	@ManyToOne(optional = true)
	private PacoteAlertas pacoteAlertas;

	public Date getDataVisualizacao() {
		return dataVisualizacao;
	}

	public void setDataVisualizacao(Date dataVisualizacao) {
		this.dataVisualizacao = dataVisualizacao;
	}

	public Dominio getTipo() {
		return tipo;
	}

	public void setTipo(Dominio tipo) {
		this.tipo = tipo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Participante getParticipante() {
		return participante;
	}

	public void setParticipante(Participante participante) {
		this.participante = participante;
	}

	public PacoteAlertas getPacoteAlertas() {
		return pacoteAlertas;
	}

	public void setPacoteAlertas(PacoteAlertas pacoteAlertas) {
		this.pacoteAlertas = pacoteAlertas;
	}
	
	public String getMensagemLimitada(int limite){
		if(this.getMensagem().length() > limite){
			return this.getMensagem().substring(0, limite) + " ...";
		}else{
			return this.getMensagem();
		}
		
	}

}
