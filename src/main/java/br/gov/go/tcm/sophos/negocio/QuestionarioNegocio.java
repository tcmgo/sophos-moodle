/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.Opcao;
import br.gov.go.tcm.sophos.entidade.Questionario;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import br.gov.go.tcm.sophos.persistencia.QuestionarioDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionarioNegocio extends NegocioBase<Questionario> {

	private static final long serialVersionUID = -8426728536010364058L;

	/** Atributo QUANTIDADE_MINIMA_OPCOES. */
	private static final int QUANTIDADE_MINIMA_OPCOES = 2;
	
	@Autowired
	private QuestionarioDAO dao;

	@Override
	protected QuestionarioDAO getDAO() {

		return this.dao;
	}
	
	@Override
	protected void executarValidacaoAntesDeSalvar(final Questionario entidade) {

		super.executarValidacaoAntesDeSalvar(entidade);

		if (AjudanteObjeto.eVazio(entidade.getOpcoes()) || entidade.getOpcoes().size() < QuestionarioNegocio.QUANTIDADE_MINIMA_OPCOES) {

			throw new ValidacaoException("Por favor, inclua, no mínimo, duas opções para o questionário.");
		}

		this.definirOrdem(entidade.getOpcoes());
	}
	
	@Override
	protected void executarValidacaoAntesDeAtualizar(Questionario entidade) {

		super.executarValidacaoAntesDeAtualizar(entidade);
		
		if (AjudanteObjeto.eVazio(entidade.getOpcoes()) || entidade.getOpcoes().size() < QuestionarioNegocio.QUANTIDADE_MINIMA_OPCOES) {

			throw new ValidacaoException("Por favor, inclua, no mínimo, duas opções para o questionário.");
		}

		this.definirOrdem(entidade.getOpcoes());
	}

	@Override
	protected void executarValidacaoAntesDeExcluir(Questionario entidade) {

		super.executarValidacaoAntesDeExcluir(entidade);

		if (this.dao.existeOpcaoDoQuestionarioVinculadoAAvaliacoesRealizadas(entidade.getId())) {

			throw new ValidacaoException("O questionário não pode ser excluído, pois existem avaliações vinculadas ao mesmo!");
		}
		
		if (AjudanteObjeto.isTrue(entidade.getAtivo())) {

			throw new ValidacaoException("Por favor, primeiro desative o questionário para excluí-lo.");
		}
	}
	
	private void definirOrdem(List<Opcao> opcoes) {

		for (int indice = 0; indice < opcoes.size(); indice++) {

			opcoes.get(indice).setOrdem(indice);
		}
	}

	public void ativarQuestionario(Questionario questionario) {
		
		this.getDAO().ativarQuestionario(questionario);
	}
}
