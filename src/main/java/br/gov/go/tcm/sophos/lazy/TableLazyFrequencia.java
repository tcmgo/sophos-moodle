/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.lazy;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.ItemFrequencia;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Frequencia;
import br.gov.go.tcm.sophos.entidade.Inscricao;
import br.gov.go.tcm.sophos.entidade.Modulo;
import br.gov.go.tcm.sophos.entidade.dto.FrequenciaEncontroDTO;
import br.gov.go.tcm.sophos.lazy.base.TableLazyDTO;
import br.gov.go.tcm.sophos.lazy.base.TableLazyPadrao;
import br.gov.go.tcm.sophos.negocio.FrequenciaNegocio;
import br.gov.go.tcm.sophos.negocio.InscricaoNegocio;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@Scope("session")
public class TableLazyFrequencia extends TableLazyPadrao<ItemFrequencia> {

	private static final long serialVersionUID = -6545514037278206167L;

	@Autowired
	private InscricaoNegocio inscricaoNegocio;

	@Autowired
	private FrequenciaNegocio frequenciaNegocio;

	private Modulo modulo;
	
	private Evento evento;
	
	@Override
	public List<ItemFrequencia> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

		sortOrder = SortOrder.ASCENDING;
		
		sortField = "pessoa.nome";
		
		this.setLista(new ArrayList<ItemFrequencia>());

		filters.put("evento", this.evento);
		
		filters.put("autorizada", Boolean.TRUE);
		
		TableLazyDTO<Inscricao> tableLazyDTO = this.inscricaoNegocio.listarPaginado(first, pageSize, sortField, sortOrder, filters,	this.getConsulta());
		
		if(AjudanteObjeto.eReferencia(this.getModulo()) && AjudanteObjeto.eReferencia(this.getEvento())){
			
			for (Inscricao inscricao : tableLazyDTO.getLista()) {
				
				List<Frequencia> listaFrequencia = this.frequenciaNegocio.listarFrequenciasModuloParticipante(this.modulo, inscricao.getParticipante());
				
				int freq = this.modulo.getQuantidadeEncontros();
				
				for(int i=listaFrequencia.size()-1;i>=0;i--){
					
					if(i>freq-1) listaFrequencia.remove(i);
					
					
				}
				
				if(AjudanteObjeto.eReferencia(listaFrequencia) && !listaFrequencia.isEmpty()){
					
					for(Frequencia frequencia : listaFrequencia){
						
						List<FrequenciaEncontroDTO> frequenciaEncontros = new ArrayList<FrequenciaEncontroDTO>();
						
						ItemFrequencia itemFrequencia = new ItemFrequencia(frequencia.getParticipante(), frequenciaEncontros);
						
						boolean temNaLista = false;

						for (ItemFrequencia item : this.getLista()) {
							
							

							if (item.getParticipante().equals(frequencia.getParticipante()) && freq!=0) {

								item.getFrequencia().add(new FrequenciaEncontroDTO(frequencia.getId(), frequencia.getPresenca(), frequencia.getEncontro()));
								
								temNaLista = true;

							}

						}

						if (!temNaLista) {

							frequenciaEncontros.add(new FrequenciaEncontroDTO(frequencia.getId(), frequencia.getPresenca(), frequencia.getEncontro()));
							
							itemFrequencia = new ItemFrequencia(frequencia.getParticipante(), frequenciaEncontros);
							
							if(itemFrequencia.getParticipante().getPessoa().getNome().toLowerCase().contains(this.getConsulta().toLowerCase()))this.getLista().add(itemFrequencia);
						}
					}
					
				} else {
				
					List<FrequenciaEncontroDTO> frequenciaEncontros = new ArrayList<FrequenciaEncontroDTO>();

					for (int contador = 1; contador <= this.modulo.getQuantidadeEncontros(); contador++) {

						frequenciaEncontros.add(new FrequenciaEncontroDTO(Boolean.FALSE, contador));

					}

					ItemFrequencia itemFrequencia = new ItemFrequencia(inscricao.getParticipante(), frequenciaEncontros);
					
					if(itemFrequencia.getParticipante().getPessoa().getNome().toLowerCase().contains(this.getConsulta().toLowerCase())) this.getLista().add(itemFrequencia);
					
				}
			}
		}
		
		this.setRowCount(tableLazyDTO.getQuantidade().intValue());

		this.setPageSize(pageSize);
		
		return this.getLista();
	}
	
	@Override
	protected NegocioBase<ItemFrequencia> getServico() {
		return null;
	}

	public Modulo getModulo() {
		return modulo;
	}

	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public InscricaoNegocio getInscricaoNegocio() {
		return inscricaoNegocio;
	}

	public FrequenciaNegocio getFrequenciaNegocio() {
		return frequenciaNegocio;
	}

}
