/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade.enumeracao;

import java.util.Arrays;
import java.util.List;

public enum PaginaEnum {

	ACESSO("Sistema de Gestão Educacional - SOPHOS", "acesso.xhtml", new PerfilEnum[]{PerfilEnum.PUBLICO}),
	
	HOME("Sistema de Gestão Educacional - SOPHOS", "home.xhtml", PerfilEnum.getListaSemPublico()),
	
	MINHA_CONTA("SOPHOS - Minha Conta", "paginas/minha-conta/index.xhtml", PerfilEnum.getListaSemPublico()),
	
	VALIDAR_CERTIFICADO("Sistema de Gestão Educacional - SOPHOS", "validar-certificado.xhtml", new PerfilEnum[]{PerfilEnum.PUBLICO}),
	
	TABELA_DOMINIO("SOPHOS - Tabelas de domínio", "paginas/tabelas/dominio/index.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR}),
	
	TABELA_DOMINIO_CADASTRO("SOPHOS - Tabelas de domínio", "paginas/tabelas/dominio/cadastro.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR}),
	
	TABELA_PROVEDOR_EVENTO("SOPHOS - Provedor do Evento", "paginas/tabelas/provedor-evento/index.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS}),
	
	TABELA_PROVEDOR_EVENTO_CADASTRO("SOPHOS - Provedor do Evento", "paginas/tabelas/provedor-evento/cadastro.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS}),
	
	RELATORIO("SOPHOS - Relatórios", "paginas/relatorio/index.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS}),
	
	PACOTE_ALERTAS("SOPHOS - Pacotes de Alertas", "paginas/pacote_alertas/index.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS}),
	
	CADASTRO_PACOTE_ALERTAS("SOPHOS - Cadastro de Pacote de Alertas", "paginas/pacote_alertas/cadastro.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS}),
	
	ALERTAS("SOPHOS - Alertas", "paginas/alertas/index.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS}),
	
	CADASTRO_ALERTAS("SOPHOS - Cadastro de Alertas", "paginas/alertas/cadastro.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS}),
	
	INSTRUTORES("SOPHOS - Instrutores", "paginas/instrutores/index.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS}),
	
	INSTRUTORES_CADASTRO("SOPHOS - Instrutores", "paginas/instrutores/cadastro.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS}),
	
	PARTICIPANTES_SITUACAO("SOPHOS - Situação dos participantes", "paginas/participantes/situacao.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS}),
	
	PARTICIPANTES_CADASTRO_LIST("SOPHOS - Cadastro de Participantes", "paginas/participantes/index.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS}),
	
	PARTICIPANTES_CADASTRO("SOPHOS - Cadastro de Participantes", "paginas/participantes/cadastro.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS}),
	
	PARTICIPANTES_MEU_CADASTRO("SOPHOS - Participante", "paginas/participantes/meu-cadastro.xhtml", PerfilEnum.getListaSemPublico()),
	
	PARTICIPANTES_INSCRICAO("SOPHOS - Inscrição de Participantes", "paginas/participantes/inscricao-participante.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS}),
	
	EVENTOS_CADASTRO("SOPHOS - Eventos", "paginas/evento/cadastro.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS}),
	
	EVENTOS_AVALIACAO_REACAO("SOPHOS - Avaliações de Reação", "paginas/evento/avaliacao-reacao.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS}),
	
	EVENTOS_VISUALIZAR("Sistema de Gestão Educacional - SOPHOS", "paginas/evento/visualizar.xhtml", new PerfilEnum[]{PerfilEnum.PUBLICO}),
	
	EVENTOS_DETALHE("Sistema de Gestão Educacional - SOPHOS", "paginas/evento/detalhe-evento.xhtml", new PerfilEnum[]{PerfilEnum.PUBLICO}),
	
	EVENTOS("SOPHOS - Eventos", "paginas/evento/index.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS}),
	
	MEUS_EVENTOS("SOPHOS - Meus Cursos / Eventos", "paginas/evento/meus-eventos.xhtml", PerfilEnum.getListaSemPublico()),
	
	INDICACOES("SOPHOS - Indicações", "paginas/evento/indicacoes.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.CHEFE}),
	
	EVENTOS_MINHAS_AVALIACOES("SOPHOS - Minhas Avaliações", "paginas/evento/minhas-avaliacoes.xhtml", PerfilEnum.getListaSemPublico()),
	
	EVENTOS_MINHAS_INSCRICOES("SOPHOS - Meus Pedidos de Inscrições", "paginas/evento/minhas-inscricoes.xhtml", PerfilEnum.getListaSemPublico()),
	
	TABELA_CONFIGURACAO("SOPHOS - Gerenciar Configurações", "paginas/tabelas/configuracao/index.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR}),
	
	TABELA_QUESTIONARIO("SOPHOS - Gerenciar Questionários", "paginas/tabelas/questionario/index.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS}),
	
	TABELA_QUESTIONARIO_CADASTRO("SOPHOS - Gerenciar Questionários", "paginas/tabelas/questionario/cadastro.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS}),
	
	TABELA_LOCALIZACAO_EVENTO("SOPHOS - Localização dos Eventos", "paginas/tabelas/localizacao/index.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS}),
	
	TABELA_LOCALIZACAO_EVENTO_CADASTRO("SOPHOS - Localização dos Eventos", "paginas/tabelas/localizacao/cadastro.xhtml", new PerfilEnum[]{PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS});

	private PaginaEnum(String titulo, String pagina, PerfilEnum[] listaPerfil) {
		this.titulo = titulo;
		this.pagina = pagina;
		this.listaPerfil = listaPerfil;
	}

	private String titulo;

	private String pagina;

	private  PerfilEnum [] listaPerfil;

	public String getTitulo() {
		return titulo;
	}

	public String getPagina() {
		return pagina;
	}

	public PerfilEnum[] getListaPerfil() {
		return listaPerfil;
	}
	
	public List<PerfilEnum> getListPerfil(){
		return Arrays.asList(listaPerfil);
	}


}
