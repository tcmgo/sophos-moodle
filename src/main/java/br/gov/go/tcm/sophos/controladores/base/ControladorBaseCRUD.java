/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores.base;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.base.EntidadeLogica;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoArquivo;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.lazy.base.TableLazyPadrao;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;

import java.lang.reflect.ParameterizedType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public abstract class ControladorBaseCRUD<E extends EntidadePadrao, N extends NegocioBase<E>> extends ControladorBase {

	private static final long serialVersionUID = -3855768743271954651L;

	/** Atributo OPERACAO_SUCESSO. */
	private static final String OPERACAO_SUCESSO = "Operação realizada com sucesso!";

	/** Atributo formulario. */
	private E entidade;

	private List<E> entidades;

	private Boolean visualizar = Boolean.FALSE;

	public abstract TableLazyPadrao<E> getLista();

	protected abstract N getNegocio();
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	private Date dataMinima;

	private Date dataMaxima;

	public void acaoInserir() {

		try {

			this.getNegocio().salvar(this.entidade);

			this.adicionarMensagemInformativa(this.obterMensagemSucessoAoInserir());

			this.executarAposInserirSucesso();

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);
		}
	}

	protected void executarAposInserirSucesso() {

		this.criarNovaInstanciaEntidade();
		
	}

	public void acaoAtualizar() {

		try {

			this.getNegocio().atualizar(this.entidade);

			this.adicionarMensagemInformativa(this.obterMensagemSucessoAoAtualizar());

			this.executarAposAtualizarSucesso();

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);
		}
	}

	protected void executarAposAtualizarSucesso() {

		/// nothing
	}

	public void acaoExcluir(E entidade) {

		try {

			this.getNegocio().excluir(entidade);

			this.adicionarMensagemInformativa(this.obterMensagemSucessoAoExcluir());

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);
		}
	}

	public E getEntidade() {

		if (!AjudanteObjeto.eReferencia(this.entidade)) {

			this.criarNovaInstanciaEntidade();
		}

		return this.entidade;
	}

	public void setEntidade(final E formulario) {

		this.entidade = formulario;
	}

	public List<E> getEntidades() {
		return entidades;
	}

	public void setEntidades(List<E> entidades) {
		this.entidades = entidades;
	}

	public Boolean getVisualizar() {
		return visualizar;
	}

	public void setVisualizar(Boolean visualizar) {
		this.visualizar = visualizar;
	}

	@SuppressWarnings("unchecked")
	protected void criarNovaInstanciaEntidade() {

		try {

			this.entidade = (E) this.obterTipoEntidade().newInstance();

			this.entidade.setDataCadastro(new Date());

			if (this.entidade instanceof EntidadeLogica) {

				((EntidadeLogica) this.entidade).setAtivo(Boolean.TRUE);
			}

		} catch (final Exception e) {

			e.printStackTrace();
		}
	}

	public Class<?> obterTipoEntidade() {

		final ParameterizedType tipo = (ParameterizedType) this.getClass().getGenericSuperclass();

		return (Class<?>) tipo.getActualTypeArguments()[0];
	}

	protected String obterMensagemSucessoAoInserir() {

		return ControladorBaseCRUD.OPERACAO_SUCESSO;
	}

	protected String obterMensagemSucessoAoAtualizar() {

		return ControladorBaseCRUD.OPERACAO_SUCESSO;
	}

	protected String obterMensagemSucessoAoExcluir() {

		return ControladorBaseCRUD.OPERACAO_SUCESSO;
	}

	public void adicionarAcaoBtnOkModalAlerta(String acao) {

		this.executarJS("jQuery('#btnOKModalAlerta').click(function(){ " + acao + " });");
	}

	public String getStringLimitada(final String palavra, final int tamanhoMaximo) {

		String retorno = "";

		if (palavra.length() <= tamanhoMaximo) {

			retorno = palavra;

		} else {

			String novaString = palavra.substring(0, tamanhoMaximo);

			novaString += "...";

			retorno = novaString;
		}

		return retorno;
	}
	
	public String getTiposArquivosPermitidos() {

		final List<TipoArquivo> tipos = Arrays.asList(TipoArquivo.values());

		final StringBuilder sb = new StringBuilder("/(\\.|\\/)(");

		for (final TipoArquivo ta : tipos) {

			sb.append(ta.getExtensao().toLowerCase()).append("|");
		}

		sb.delete(sb.length() - 1, sb.length());

		sb.append(")$/");

		return sb.toString();
	}
	
	public void ativarAba(String classeAba) {
		super.executarJS("ativarAba('" + classeAba + "')");
	}
	
	public SimpleDateFormat getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(SimpleDateFormat dateFormat) {
		this.dateFormat = dateFormat;
	}

	public Date getDataMinima() {

		if (!AjudanteObjeto.eReferencia(dataMinima)) {
			try {
				dataMinima = dateFormat.parse("01/01/1753");
			} catch (ParseException e) {
				this.adicionarMensagemExcecao(
						new ValidacaoException("Ocorreu um erro ao tentar obter a data mínima permitida."));
			}
		}

		return dataMinima;
	}

	public void setDataMinima(Date dataMinima) {
		this.dataMinima = dataMinima;
	}

	public Date getDataMaxima() {

		if (!AjudanteObjeto.eReferencia(dataMaxima)) {
			try {
				dataMaxima = dateFormat.parse("31/12/9999");
			} catch (ParseException e) {
				this.adicionarMensagemExcecao(
						new ValidacaoException("Ocorreu um erro ao tentar obter a data máxima permitida."));
			}
		}

		return dataMaxima;
	}

	public void setDataMaxima(Date dataMaxima) {
		this.dataMaxima = dataMaxima;
	}
}
