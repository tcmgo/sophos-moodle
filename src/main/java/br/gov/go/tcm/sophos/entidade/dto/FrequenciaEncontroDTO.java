/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade.dto;

import java.io.Serializable;

public class FrequenciaEncontroDTO implements Serializable{

	private static final long serialVersionUID = 4530820920863196204L;

	private Long idFrequencia;

	private Boolean presenca;

	private Integer encontro;
	
	private Boolean houveAlteracao = Boolean.FALSE;

	public FrequenciaEncontroDTO() {
		super();
	}

	public FrequenciaEncontroDTO(Boolean presenca, Integer encontro) {
		super();
		this.presenca = presenca;
		this.encontro = encontro;
	}

	public FrequenciaEncontroDTO(Long idFrequencia, Boolean presenca, Integer encontro) {
		super();
		this.idFrequencia = idFrequencia;
		this.presenca = presenca;
		this.encontro = encontro;
	}

	public Boolean getHouveAlteracao() {
		return houveAlteracao;
	}

	public void setHouveAlteracao(Boolean houveAlteracao) {
		this.houveAlteracao = houveAlteracao;
	}

	public Boolean getPresenca() {
		return presenca;
	}

	public void setPresenca(Boolean presenca) {
		if(this.presenca != presenca){
			this.houveAlteracao = Boolean.TRUE;
		}
		this.presenca = presenca;
	}

	public Integer getEncontro() {
		return encontro;
	}

	public void setEncontro(Integer encontro) {
		this.encontro = encontro;
	}

	public Long getIdFrequencia() {
		return idFrequencia;
	}

	public void setIdFrequencia(Long idFrequencia) {
		this.idFrequencia = idFrequencia;
	}

}
