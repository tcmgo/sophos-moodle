/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBase;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.PerfilUsuario;
import br.gov.go.tcm.sophos.entidade.Usuario;
import br.gov.go.tcm.sophos.entidade.enumeracao.PaginaEnum;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.negocio.EventoNegocio;
import br.gov.go.tcm.sophos.negocio.LoginNegocio;
import br.gov.go.tcm.sophos.negocio.UsuarioNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class LoginControlador extends ControladorBase {

	private static final long serialVersionUID = -2982535262900143419L;

	@Autowired
	private LoginNegocio loginNegocio;

	@Autowired
	private UsuarioNegocio usuarioNegocio;

	@Autowired
	private NavegacaoControlador navegacaoControlador;
	
	@Autowired
	private EventoNegocio eventoNegocio;

	private String nomeUsuario;
	
	private String cpf;

	private String senha;
	
	private Boolean mostrarCadastro = false;
	
	private Evento eventoRemocao;
	
	public void acaoLogarPorNomeUsuario() {

		try {

			Usuario usuario = this.loginNegocio.logarPorNomeUsuario(this.getNomeUsuario(), this.getSenha());

			this.definirObjetoNaSessao(USUARIO_LOGADO, usuario);

			this.definirObjetoNaSessao(PERFIL_SELECIONADO, this.usuarioNegocio.obterPerfilPrincipal(usuario));

			if(AjudanteObjeto.eReferencia(usuario.getParticipante()) && AjudanteObjeto.eReferencia(usuario.getParticipante().getId())){
				
				this.navegacaoControlador.acaoAbrirTela(PaginaEnum.EVENTOS_VISUALIZAR);
				
			}else{
				
				this.navegacaoControlador.acaoAbrirTela(PaginaEnum.PARTICIPANTES_MEU_CADASTRO);
			}

		} catch (ValidacaoException e) {

			this.adicionarMensagemInformativa(e.getMessage());

		}
	}
	
	public void acaoLogarPorCPF() {

		try {

			Usuario usuario = this.loginNegocio.logarPorCPF(this.getCpf(), this.getSenha());

			this.definirObjetoNaSessao(USUARIO_LOGADO, usuario);

			this.definirObjetoNaSessao(PERFIL_SELECIONADO, this.usuarioNegocio.obterPerfilPrincipal(usuario));

			if(AjudanteObjeto.eReferencia(usuario.getParticipante()) && AjudanteObjeto.eReferencia(usuario.getParticipante().getId())){
				
				this.navegacaoControlador.acaoAbrirTela(PaginaEnum.EVENTOS_VISUALIZAR);
			}else{
				
				this.navegacaoControlador.acaoAbrirTela(PaginaEnum.PARTICIPANTES_MEU_CADASTRO);
			}

		} catch (ValidacaoException e) {

			this.adicionarMensagemInformativa(e.getMessage());

		}
	}

	public void definirPerfil(PerfilUsuario perfilUsuario){

		this.definirObjetoNaSessao(PERFIL_SELECIONADO, perfilUsuario.getPerfil());
	}
	
	public String acaoSair() {

		this.invalidarSessao();

		return this.acaoAbrirTelaInicial();
	}
	
	public void prepararRemocaoEvento(Evento evento) {
		this.setSenha("");
		this.eventoRemocao = evento;
	}

	public void confirmarRemocaoEvento() {
		
		Usuario usuario = new Usuario();
		
		try {
			
			if (AjudanteObjeto.eReferencia(this.getSenha())) {
				usuario = this.loginNegocio.logarPorNomeUsuario(this.getUsuarioLogado().getNomeUsuario(), this.getSenha());
			}
			
		} catch (ValidacaoException e) {
			usuario = null;
		}

		try {
			
			if (AjudanteObjeto.eReferencia(usuario)) {
				
				eventoNegocio.excluir(this.eventoRemocao);
				this.adicionarMensagemDeAlerta("O evento " + this.eventoRemocao.getTitulo() + " foi removido com sucesso!");
				
			} else {
				
				this.adicionarMensagemDeAlerta("Senha incorreta. A remoção do evento não foi efetuada.");
			}
			
			this.setSenha("");
			
		} catch (ValidacaoException e) {
			this.adicionarMensagemDeAlerta(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getSenha() {

		return senha;
	}

	public void setSenha(String senha) {

		this.senha = senha;
	}

	public void abrirEsqueciSenha() {

	}

	public void enviarNovaSenha() {

	}

	public Boolean getMostrarCadastro() {
		return mostrarCadastro;
	}

	public void setMostrarCadastro(Boolean mostrarCadastro) {
		this.mostrarCadastro = mostrarCadastro;
	}

	public Evento getEventoRemocao() {
		return eventoRemocao;
	}

	public void setEventoRemocao(Evento eventoRemocao) {
		this.eventoRemocao = eventoRemocao;
	}

}
