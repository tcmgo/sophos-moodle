/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "pacote_alertas")
@Banco(nome = Banco.SOPHOS)
public class PacoteAlertas extends EntidadePadrao {

	private static final long serialVersionUID = 3958336940990802481L;
	
	@Column(name = "nome", length = 500, nullable = false)
	private String nome;
	
	@JoinColumn(name = "tipo_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Dominio tipo;
	
	@Column(name = "mensagem", length = 2000, nullable = false)
	private String mensagem;
	
	@JoinColumn(name = "evento_id", referencedColumnName = "id")
	@ManyToOne
	private Evento evento;

	@OneToMany(mappedBy = "pacoteAlertas", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Alerta> alertasList;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Dominio getTipo() {
		return tipo;
	}

	public void setTipo(Dominio tipo) {
		this.tipo = tipo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		if(evento != null){
			this.evento = evento;
		}
	}

	public List<Alerta> getAlertasList() {
		return alertasList;
	}

	public void setAlertasList(List<Alerta> alertasList) {
		this.alertasList = alertasList;
	}
	
	public String getMensagemLimitada(int limite){
		if(this.getMensagem().length() > limite){
			return this.getMensagem().substring(0, limite) + " ...";
		}else{
			return this.getMensagem();
		}
		
	}

}
