/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;

import javax.persistence.*;

@Entity(name = "br.gov.go.tcm.sophos.entidade.ArquivoAdministrativo")
@Table(name = "arquivo_adm")
@Banco(nome = Banco.SOPHOS)
public class ArquivoAdministrativo extends EntidadePadrao {

	private static final long serialVersionUID = 5875571365830804994L;

	@JoinColumn(name = "evento_id", referencedColumnName = "id")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Evento evento;

	@JoinColumn(name = "tipo_arquivoadm_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Dominio tipoArquivoAdministrativo;

	@JoinColumn(name = "arquivo_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Anexo arquivo;

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public Dominio getTipoArquivoAdministrativo() {
		return tipoArquivoAdministrativo;
	}

	public void setTipoArquivoAdministrativo(Dominio tipoArquivoAdministrativo) {
		this.tipoArquivoAdministrativo = tipoArquivoAdministrativo;
	}

	public Anexo getArquivo() {
		return arquivo;
	}

	public void setArquivo(Anexo arquivo) {
		this.arquivo = arquivo;
	}

}
