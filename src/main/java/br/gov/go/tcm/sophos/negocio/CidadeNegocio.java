/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.estrutura.entidade.orcafi.Municipio;
import br.gov.go.tcm.estrutura.persistencia.orcafi.contrato.MunicipioDAO;
import br.gov.go.tcm.sophos.entidade.Cidade;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import br.gov.go.tcm.sophos.persistencia.CidadeDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CidadeNegocio extends NegocioBase<Cidade> {

	private static final long serialVersionUID = -8426728536010364058L;

	@Autowired
	private CidadeDAO dao;
	
	@Autowired
	private MunicipioDAO municipioDAO;

	@Override
	protected CidadeDAO getDAO() {

		return this.dao;
	}

	@SuppressWarnings("unchecked")
	public List<Municipio> listaMunicipios(){
		
		return (List<Municipio>) this.municipioDAO.obterTodos(Municipio.class);
	}
	
}
