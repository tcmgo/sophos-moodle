/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.conversor;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.math.BigDecimal;
import java.text.DecimalFormat;

@FacesConverter("bigDecimalConverter")
public class BigDecimalConverter implements Converter {

	@Override
	public Object getAsObject(final FacesContext context, final UIComponent component, String value) {

		Object object = null;

		try {

			if (value != null && !"".equals(value)) {

				value = value.replaceAll("\\.", "").replaceAll(",", ".").replace("R$ ", "");

				object = new BigDecimal(value);
			}

		} catch (final Exception e) {

			e.printStackTrace();
		}

		return object;
	}

	@Override
	public String getAsString(final FacesContext context, final UIComponent component, final Object object) {

		String value = "";

		if (object != null) {

			value = "R$ " + new DecimalFormat("#,##0.00").format(Double.valueOf(object.toString().trim()));
		}

		return value;
	}
}
