/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.ajudante;

import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import org.primefaces.model.CroppedImage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;

public class AjudanteImagem {

	public static final Double PROPORCAO_IDEAL_MINIMA = 1.30;

	public static final Double PROPORCAO_IDEAL_MAXIMA = 1.39;

	public static final int RESOLUCAO_MINIMA_ALTURA = 540;

	public static final int RESOLUCAO_MINIMA_LARGURA = 720;

	public static CroppedImage cropImage(int croopX, int croopY, int croopW, int croopH, int boundx, int boundy,
			String caminhoImagem, String nomeImagem) {

		try {

			BufferedImage outputImage = ImageIO.read(new File(caminhoImagem));

			BigDecimal percentualX = BigDecimal.valueOf(boundx).multiply(BigDecimal.valueOf(100))
					.divide(BigDecimal.valueOf(outputImage.getWidth()), 2, BigDecimal.ROUND_DOWN);
			BigDecimal percentualY = BigDecimal.valueOf(boundy).multiply(BigDecimal.valueOf(100))
					.divide(BigDecimal.valueOf(outputImage.getHeight()), 2, BigDecimal.ROUND_DOWN);

			int x = BigDecimal.valueOf(croopX).multiply(BigDecimal.valueOf(100)).divide(percentualX, 0, BigDecimal.ROUND_DOWN).intValue();
			int y = BigDecimal.valueOf(croopY).multiply(BigDecimal.valueOf(100))
					.divide(percentualY, 0, BigDecimal.ROUND_DOWN).intValue();
			int w = BigDecimal.valueOf(croopW).multiply(BigDecimal.valueOf(100))
					.divide(percentualX, 0, BigDecimal.ROUND_DOWN).intValue();
			int h = BigDecimal.valueOf(croopH).multiply(BigDecimal.valueOf(100))
					.divide(percentualY, 0, BigDecimal.ROUND_DOWN).intValue();

			BufferedImage cropped = outputImage.getSubimage(x, y, w, h);

			ByteArrayOutputStream croppedOutImage = new ByteArrayOutputStream();
			ImageIO.write(cropped, "jpg", croppedOutImage);

			return new CroppedImage(nomeImagem, croppedOutImage.toByteArray(), x, y, w, h);

		} catch (IOException e) {

			throw new ValidacaoException("O recorte da imagem falhou. Tente novamente!");
		}
	}

	public static CroppedImage cropImage(String caminhoImagem, String nomeImagem) {

		try {

			BufferedImage outputImage = ImageIO.read(new File(caminhoImagem));
			ByteArrayOutputStream croppedOutImage = new ByteArrayOutputStream();
			ImageIO.write(outputImage, "jpg", croppedOutImage);

			return new CroppedImage(nomeImagem, croppedOutImage.toByteArray(), 0, 0, outputImage.getWidth(),
					outputImage.getHeight());

		} catch (IOException e) {

			throw new ValidacaoException("O recorte da imagem falhou. Tente novamente!");
		}
	}

	public static void validarImagem(byte[] arquivo) throws IOException {

		BufferedImage bi = ImageIO.read(new ByteArrayInputStream(arquivo));

		int width = bi.getWidth();

		int height = bi.getHeight();

		if (width < AjudanteImagem.RESOLUCAO_MINIMA_LARGURA || height < AjudanteImagem.RESOLUCAO_MINIMA_ALTURA) {

			throw new ValidacaoException("Imagem não pode ser menor que " + AjudanteImagem.RESOLUCAO_MINIMA_LARGURA + "x" + AjudanteImagem.RESOLUCAO_MINIMA_ALTURA);
		}

		// Double proporcao = Double.valueOf(width) / Double.valueOf(height);
		//
		// if(proporcao < AjudanteImagem.PROPORCAO_IDEAL_MINIMA || proporcao >
		// AjudanteImagem.PROPORCAO_IDEAL_MAXIMA){
		//
		// throw new ValidacaoException("Imagem fora da proporção de " +
		// AjudanteImagem.RESOLUCAO_MINIMA_LARGURA + "x" +
		// AjudanteImagem.RESOLUCAO_MINIMA_ALTURA);
		// }
	}
}
