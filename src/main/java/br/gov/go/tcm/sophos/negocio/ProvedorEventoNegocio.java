/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.ProvedorEvento;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import br.gov.go.tcm.sophos.persistencia.ProvedorEventoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProvedorEventoNegocio extends NegocioBase<ProvedorEvento> {

	private static final long serialVersionUID = -8426728536010364058L;

	@Autowired
	private ProvedorEventoDAO dao;
	
	@Autowired
	private EnderecoNegocio enderecoNegocio;
	
	@Autowired
	private EventoNegocio eventoNegocio;
	
	@Override
	protected ProvedorEventoDAO getDAO() {

		return this.dao;
	}
	
	@Override
	protected void executarValidacaoAntesDeExcluir(ProvedorEvento entidade) {

		super.executarValidacaoAntesDeExcluir(entidade);
		
		List<Evento> eventoList = this.eventoNegocio.listaPorAtributo("provedor", entidade);
			
		if(AjudanteObjeto.eReferencia(eventoList) && eventoList.size() > 0){
			
			throw new ValidacaoException("Não é possível fazer a exclusão, pois esse provedor do evento está relacionado a algum evento.");
		}
	}
	
	@Override
	public void salvar(ProvedorEvento entidade) {

		this.executarValidacaoAntesDeSalvar(entidade);
		
		this.enderecoNegocio.salvar(entidade.getEndereco());
		
		super.salvar(entidade);
	}

	public boolean procurarProvedorEvento(ProvedorEvento entidade) {
		if (AjudanteObjeto.eReferencia(entidade.getCnpj())) {
			if( (dao.obterPorAtributo("cnpj", entidade.getCnpj()) ) != null) {
				return true;
			}
		}
		return false;
	}
}
