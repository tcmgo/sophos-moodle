/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.ajudante;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collection;

public class AjudanteObjeto {

	public static boolean eReferencia(Object o) {

		return o != null;
	}
	
	public static boolean isTrue(Boolean b) {
		
		return b != null && b;
	}

	public static boolean eVazia(String s) {
		
		return !eReferencia(s) || s.isEmpty();
	}

	public static boolean eVazio(Collection<?> etapas) {
		
		return etapas == null || etapas.isEmpty();
	}

	public static boolean isInteger(String s) {
	    try { 
	        Integer.parseInt(s); 
	    } catch(Exception e) { 
	        return false; 
	    }
	    return true;
	}
	
	public static boolean isBigDecimal(String s) {
	    try { 
	        new BigDecimal(s);
	    } catch(Exception e) { 
	        return false; 
	    }
	    return true;
	}
	
	public static boolean isLong(String s) {
	    try { 
	        Long.parseLong(s);
	    } catch(Exception e) { 
	        return false; 
	    }
	    return true;
	}
	
	public static boolean isDate(String dateToValidate, String dateFromat){
		
		if(dateToValidate == null){
			return false;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		try {
			sdf.parse(dateToValidate);
		} catch (Exception e) {
			return false;
		}
		
		return true;
	}
	
	public static boolean isDateDDMMYYYY(String dateToValidate){
		
		if(dateToValidate == null){
			return false;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			sdf.parse(dateToValidate);
		} catch (Exception e) {
			return false;
		}
		
		return true;
	}
}
