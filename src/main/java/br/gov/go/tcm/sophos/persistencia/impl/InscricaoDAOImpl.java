/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.persistencia.impl;

import br.gov.go.tcm.estrutura.entidade.orcafi.Municipio;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Inscricao;
import br.gov.go.tcm.sophos.entidade.Participante;
import br.gov.go.tcm.sophos.persistencia.InscricaoDAO;
import br.gov.go.tcm.sophos.persistencia.base.DAO;
import br.gov.go.tcm.sophos.persistencia.impl.base.DAOGenerico;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.*;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

@Repository
public class InscricaoDAOImpl extends DAOGenerico<Inscricao> implements InscricaoDAO {

	private static final long serialVersionUID = 294560128456827146L;

	@Autowired
	public InscricaoDAOImpl(@Qualifier(DAO.SESSION_FACTORY_SOPHOS) final SessionFactory factory, @Qualifier(DAO.HIBERNATE_TEMPLATE_SOPHOS) final HibernateTemplate hibernateTemplate) {

		this.setHibernateTemplate(hibernateTemplate);

		this.setSessionFactory(factory);
		
	}

	@Override
	public boolean existeInscricaoParaParticipante(Participante participante, Evento evento) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		criteria.add(Restrictions.eq("participante", participante));
		
		criteria.add(Restrictions.eq("evento", evento));
		
		criteria.setProjection(Projections.count("id"));
		
		try {

			return ((Long) criteria.uniqueResult()) > 0;

		} finally {

			session.close();
		}
	}

	@Override
	public Integer obterQuantidadeInscricoes(Evento evento) {
		
		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		criteria.add(Restrictions.eq("evento", evento));
		
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		criteria.setProjection(Projections.count("id"));

		try {

			return ((Long) criteria.uniqueResult()).intValue();

		} finally {

			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Inscricao> listarInscricoesAutorizadas(Evento evento) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		criteria.add(Restrictions.eq("evento", evento));
		
		criteria.add(Restrictions.eq("autorizada", Boolean.TRUE));
				
		criteria.createAlias("participante.pessoa", "pessoa").addOrder(Order.asc("pessoa.nome"));
		
		
		try {

			return criteria.list();

		} finally {

			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Inscricao> listarInscricoesNaoAutorizadas(Evento evento) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		criteria.add(Restrictions.eq("evento", evento));
		
		criteria.add(Restrictions.eq("autorizada", Boolean.FALSE));
				
		criteria.createAlias("participante.pessoa", "pessoa").addOrder(Order.asc("pessoa.nome"));
		
		
		try {

			return criteria.list();

		} finally {

			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Inscricao> listarInscricoesAguardando(Evento evento) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		criteria.add(Restrictions.eq("evento", evento));
		
		//criteria.add(Restrictions.eq("autorizada", null));
		
		criteria.add(Restrictions.isNull("autorizada"));
				
		criteria.createAlias("participante.pessoa", "pessoa").addOrder(Order.asc("pessoa.nome"));
		
		
		try {

			return criteria.list();

		} finally {

			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Inscricao> listarInscricoesAutorizadas(Participante participante) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		criteria.add(Restrictions.eq("participante", participante));
		
		criteria.add(Restrictions.eq("autorizada", Boolean.TRUE));
				
		criteria.createAlias("participante.pessoa", "pessoa").addOrder(Order.asc("pessoa.nome"));
		
		try {

			return criteria.list();

		} finally {

			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Inscricao> listarInscricoesNaoAutorizadas(Participante participante) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		criteria.add(Restrictions.eq("participante", participante));
		
		criteria.add(Restrictions.eq("autorizada", Boolean.FALSE));
				
		criteria.createAlias("participante.pessoa", "pessoa").addOrder(Order.asc("pessoa.nome"));
		
		try {

			return criteria.list();

		} finally {

			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Inscricao> listarInscricoesAguardando(Participante participante) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		criteria.add(Restrictions.eq("participante", participante));
		
		//criteria.add(Restrictions.eq("autorizada", null));
		
		criteria.add(Restrictions.isNull("autorizada"));
				
		criteria.createAlias("participante.pessoa", "pessoa").addOrder(Order.asc("pessoa.nome"));
		
		try {

			return criteria.list();

		} finally {

			session.close();
		}
	}
	
	@Override
	protected void buscaGenericaPorConsulta(String consulta, Criteria criteria, Disjunction queryAll, Field field) {

		super.buscaGenericaPorConsulta(consulta, criteria, queryAll, field);
		
		if(field.getType().equals(Participante.class)){
		
			criteria.createAlias("participante", "participante");
			
			criteria.createAlias("participante.pessoa", "pessoa");
			
			criteria.createAlias("participante.tipo", "tipo");
			
			criteria.createCriteria("participante.endereco", "endereco");
			
			criteria.createAlias("endereco.cidade", "cidade", Criteria.LEFT_JOIN);
			
			// Buscar por município
			DetachedCriteria subQuery = DetachedCriteria.forClass(Municipio.class);
			subQuery.setProjection(Projections.id());
			subQuery.add(Restrictions.ilike("descricao", consulta, MatchMode.ANYWHERE));

			if(!StringUtils.isEmpty(consulta)){
				
				queryAll.add(Restrictions.ilike("pessoa.nome", consulta, MatchMode.ANYWHERE));
				
				queryAll.add(Restrictions.ilike("participante.cargo", consulta, MatchMode.ANYWHERE));
				
				queryAll.add(Restrictions.ilike("participante.lotacao", consulta, MatchMode.ANYWHERE));
				
				queryAll.add(Restrictions.ilike("tipo.descricao", consulta, MatchMode.ANYWHERE));
				
				queryAll.add(Restrictions.ilike("cidade.descricao", consulta, MatchMode.ANYWHERE));

				// Buscar por município
				queryAll.add(Property.forName("endereco.municipioId").in(subQuery));
			}
		}	
	}

	@Override
	public int obterQuantidadeTotal(Evento entidade) {
		
		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		criteria.createAlias("evento", "evento");
		
		criteria.add(Restrictions.eq("evento.id", entidade.getId()));
		
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		criteria.setProjection(Projections.count("id"));

		try {

			return ((Long) criteria.uniqueResult()).intValue();

		} finally {

			session.close();
		}
	}

	@Override
	public int obterQuantidadeAutorizada(Evento entidade) {
		
		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		criteria.createAlias("evento", "evento");
		
		criteria.add(Restrictions.eq("evento.id", entidade.getId()));
		
		criteria.add(Restrictions.eq("autorizada", Boolean.TRUE));
		
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		criteria.setProjection(Projections.count("id"));

		try {

			return ((Long) criteria.uniqueResult()).intValue();

		} finally {

			session.close();
		}
		
	}

	@Override
	public int obterQuantidadeNegada(Evento entidade) {
		
		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		criteria.createAlias("evento", "evento");
		
		criteria.add(Restrictions.eq("evento.id", entidade.getId()));
		
		criteria.add(Restrictions.eq("autorizada", Boolean.FALSE));
		
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		criteria.setProjection(Projections.count("id"));

		try {

			return ((Long) criteria.uniqueResult()).intValue();

		} finally {

			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Inscricao> listar(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters, String consulta) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		defineBuscaProFiltros(filters, criteria);
		
		Disjunction queryAll = definirBuscaPorConsulta(consulta, criteria);
		
		criteria.add(queryAll);
		
		criteria.createAlias("evento", "evento");

		if (sortField != null && !sortField.isEmpty() && sortOrder != null) {
			
			if (sortOrder == SortOrder.ASCENDING) {
				criteria.addOrder(Order.asc(sortField)); 
			} else if (sortOrder == SortOrder.DESCENDING) {
				criteria.addOrder(Order.desc(sortField)); 
			}
			
		} else {
			
			criteria.addOrder(Order.desc("dataCadastro")); 
			//criteria.addOrder(Order.asc("pessoa.nome"));
			
		}
		
		//criteria.addOrder(Order.asc("pessoa.nome"));
		
		criteria.setFirstResult(first);

		criteria.setMaxResults(pageSize);

		try {

			return criteria.list();

		} finally {

			session.close();
		}
	}

}
