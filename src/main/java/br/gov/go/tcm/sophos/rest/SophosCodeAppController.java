package br.gov.go.tcm.sophos.rest;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.*;
import br.gov.go.tcm.sophos.rest.base.RestControllerBase;
import br.gov.go.tcm.sophos.rest.dto.ScaModuloDoEventoDTO;
import br.gov.go.tcm.sophos.rest.dto.ScaPresencaDoQRCodeDTO;
import br.gov.go.tcm.sophos.negocio.EventoNegocio;
import br.gov.go.tcm.sophos.negocio.FrequenciaNegocio;
import br.gov.go.tcm.sophos.negocio.ModuloNegocio;
import br.gov.go.tcm.sophos.negocio.ParticipanteNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping
public class SophosCodeAppController extends RestControllerBase {

    @Autowired
    private FrequenciaNegocio frequenciaNegocio;

    @Autowired
    private ModuloNegocio moduloNegocio;

    @Autowired
    private ParticipanteNegocio participanteNegocio;

    @Autowired
    private EventoNegocio eventoNegocio;

    @RequestMapping(value = "/carregar-presencas", method = RequestMethod.POST)
    public ResponseEntity carregarFrequencias(@RequestBody List<ScaPresencaDoQRCodeDTO> presencasQRCode) {

        List<Frequencia> frequenciasDoParticipante;

        Modulo modulo;

        Participante participante;

        Integer numeroEncontro;

        Frequencia frequencia;

        try {

            for (ScaPresencaDoQRCodeDTO presencaQrCode : presencasQRCode) {

                modulo = moduloNegocio.obter(presencaQrCode.getModuloId());

                numeroEncontro = presencaQrCode.getNumeroEncontro();

                if (numeroEncontro > 0 && numeroEncontro <= modulo.getQuantidadeEncontros()) {

                    participante = participanteNegocio.obter(presencaQrCode.getParticipanteId());

                    frequenciasDoParticipante = this.frequenciaNegocio.listarFrequenciasModuloParticipante(modulo, participante);

                    if((frequenciasDoParticipante == null || frequenciasDoParticipante.size() == 0)) {

                        frequenciasDoParticipante = new ArrayList<>();

                        for(int i = 1; i <= modulo.getQuantidadeEncontros(); i++) {

                            frequencia = new Frequencia();

                            frequencia.setId(null);

                            frequencia.setPresenca(Boolean.FALSE);

                            frequencia.setParticipante(participante);

                            frequencia.setModulo(modulo);

                            frequencia.setEncontro(new Integer(i));

                            frequencia.setHouveAlteracao(Boolean.FALSE);

                            frequencia.setDataCadastro(new Date());

                            frequenciasDoParticipante.add(frequencia);
                        }
                    } else {

                        boolean temPresencaParaOEncontro = false;

                        for (Frequencia frequenciaDoParticipante : frequenciasDoParticipante) {

                            if(frequenciaDoParticipante.getEncontro().compareTo(numeroEncontro) == 0) {

                                temPresencaParaOEncontro = true;
                            }
                        }
                        if(!temPresencaParaOEncontro) {

                            Frequencia frequencia1 = new Frequencia();

                            frequencia1.setId(null);

                            frequencia1.setPresenca(Boolean.FALSE);

                            frequencia1.setParticipante(participante);

                            frequencia1.setModulo(modulo);

                            frequencia1.setEncontro(numeroEncontro);

                            frequencia1.setHouveAlteracao(Boolean.FALSE);

                            frequencia1.setDataCadastro(new Date());

                            frequenciasDoParticipante.add(frequencia1);
                        }
                    }

                    for (Frequencia frequenciaDoParticipante: frequenciasDoParticipante) {

                        if(frequenciaDoParticipante.getEncontro().compareTo(numeroEncontro) == 0) {

                            frequenciaDoParticipante.setPresenca(Boolean.TRUE);
                        }
                    }
                     this.frequenciaNegocio.salvar(frequenciasDoParticipante);

                } else {

                    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                }
            }
        } catch (Exception e) {

            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @RequestMapping(value = "/obter-modulos-evento/{evento-id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<ScaModuloDoEventoDTO> entregarModulosEventoApp(@PathVariable("evento-id") Long eventoId) {

        Evento evento = this.eventoNegocio.obter(eventoId);

        if(!AjudanteObjeto.eReferencia(evento)) {

            return null;
        }
        List<Modulo> modulos = moduloNegocio.listarOrdenadoPorData(evento);

        if(!AjudanteObjeto.eReferencia(modulos)) {

            return null;
        }
        List<ScaModuloDoEventoDTO> modulosDoEvento = new ArrayList<>();

        ScaModuloDoEventoDTO moduloEvento;

        for (Modulo modulo : modulos) {

            moduloEvento = new ScaModuloDoEventoDTO();

            moduloEvento.setId(modulo.getId());

            moduloEvento.setNome(modulo.getTitulo());

            modulosDoEvento.add(moduloEvento);
        }
        return modulosDoEvento;
    }
}
