/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.lazy.base;

import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import java.util.List;
import java.util.Map;

public abstract class TableLazyPadrao<E extends EntidadePadrao> extends LazyDataModel<E> {

	private static final long serialVersionUID = -1057833666384436570L;

	private String consulta = "";

	private List<E> lista;

	private Map<String, Object> filters;

	@Override
	public List<E> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		
		if(this.filters != null){
			
			filters = this.filters;
		}	

		TableLazyDTO<E> tableLazyDTO = this.getServico().listarPaginado(first, pageSize, sortField, sortOrder, filters,	consulta);

		this.setLista(tableLazyDTO.getLista());

		this.setRowCount(tableLazyDTO.getQuantidade().intValue());

		this.setPageSize(pageSize);
		
		this.setConsulta("");
		
		return tableLazyDTO.getLista();

	}

	protected abstract NegocioBase<E> getServico();

	@Override
	public E getRowData(String rowKey) {
		
		for (E entidade : lista) {

			if (entidade.getId().equals(Long.parseLong(rowKey))) {

				return entidade;
			}
		}

		return null;
	}

	@Override
	public Object getRowKey(E entidade) {

		return entidade.getId();
	}

	public String getConsulta() {
		return consulta;
	}

	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}

	public Map<String, Object> getFilters() {
		return filters;
	}

	public void setFilters(Map<String, Object> filters) {
		this.filters = filters;
	}

	public List<E> getLista() {
		return lista;
	}

	public void setLista(List<E> lista) {
		this.lista = lista;
	}

}
