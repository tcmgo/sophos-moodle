/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.Dominio;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoDominioEnum;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import br.gov.go.tcm.sophos.persistencia.DominioDAO;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DominioNegocio extends NegocioBase<Dominio> {

	private static final long serialVersionUID = -8426728536010364058L;
	
	@Autowired
	private DominioDAO dao;

	@Override
	protected DominioDAO getDAO() {
		
		return this.dao;
	}
	
	@Override
	public void excluir(Dominio entidade) {

		Dominio dominio = this.obter(entidade.getId());
		
		dominio.setAtivo(Boolean.FALSE);
		
		super.salvar(dominio);
	}
	
	@Override
	protected void executarValidacaoAntesDeSalvar(Dominio entidade) {

		super.executarValidacaoAntesDeSalvar(entidade);
		
		if(AjudanteObjeto.eReferencia(this.getDAO().obterDominio(entidade.getNome(), entidade.getCodigo()))){
			
			throw new ValidacaoException("Já existe um domínio com este código para esse nome.");
		}
	}
	
	public Dominio obterDominio(TipoDominioEnum nome, Long codigo) {
		
		Dominio dominio = this.getDAO().obterDominio(nome, codigo);
		
		if(!AjudanteObjeto.eReferencia(dominio)){
			
			throw new ValidacaoException("Problema na carga de dados!");
		}
		
		return dominio;
		
	}
	
	public Long obterUltimoCodigoDominio(TipoDominioEnum nome) {
		
		return this.getDAO().obterUltimoCodigoDominio(nome);
		
	}
	
	public List<Dominio> listaPorAtributo(String atributo, Object valor) {

		return this.getDAO().listaOrdenadoPorAtributo(atributo, valor, SortOrder.ASCENDING, "descricao");
	}

}
