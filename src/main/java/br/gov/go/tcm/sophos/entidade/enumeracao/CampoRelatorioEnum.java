/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade.enumeracao;

public enum CampoRelatorioEnum {

	EVENTO, EVENTOS, PARTICIPANTE, INSTRUTOR, MODULO, TIPO_PARTICIPANTE, INDICADA, AUTORIZADA, DATA_INICIO, DATA_FIM, 
	TIPO_EVENTO, PUBLICO_ALVO, EIXO_TEMATICO, LOCALIZACAO, PROVEDOR, CIDADE, CARGO, MODALIDADE, PARTICIPANTES_PRESENTES,
	APROVADOS, INICIAL_NOME_PARTICIPANTE, PNE, LISTA_INSTRUTOR_MODULO, CARGA_HORARIA_MODULO, DATA_MODULO_INICIO, DATA_MODULO_FIM,
	IMPORTADO
}
