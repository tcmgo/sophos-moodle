/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.Dominio;
import br.gov.go.tcm.sophos.entidade.enumeracao.PaginaEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoDominioEnum;
import br.gov.go.tcm.sophos.lazy.TableLazyDominio;
import br.gov.go.tcm.sophos.negocio.DominioNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Scope("session")
public class DominioControlador extends ControladorBaseCRUD<Dominio, DominioNegocio> {

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private DominioNegocio negocio;
	
	@Autowired
	private TableLazyDominio lista;
	
	@Autowired
	private NavegacaoControlador navegacaoControlador;
	
	private List<TipoDominioEnum> opcoesDominio;

	@Override
	protected DominioNegocio getNegocio() {

		return this.negocio;
	}

	public void acaoAbrirTelaDominio(Dominio entidade, Boolean visualizar){

		this.setEntidade(AjudanteObjeto.eReferencia(entidade) ? entidade : new Dominio());
		
		this.setVisualizar(visualizar);
		
		this.navegacaoControlador.processarNavegacao(PaginaEnum.TABELA_DOMINIO_CADASTRO);
		
		this.opcoesDominio = Arrays.asList(TipoDominioEnum.values());
		
	}
	
	public void obterUltimoCodigo(){
		
		if(!AjudanteObjeto.eReferencia(this.getEntidade().getId())){
			
			this.getEntidade().setCodigo(this.getNegocio().obterUltimoCodigoDominio(this.getEntidade().getNome()) + 1);
			
		}
	}
	
	public void acaoAtivar(Dominio dominio){
		
		dominio.setAtivo(Boolean.TRUE);
		
		this.getNegocio().salvar(dominio);
	}
	
	@Override
	public TableLazyDominio getLista() {

		return this.lista;
	}

	public List<TipoDominioEnum> getOpcoesDominio() {
		
		return opcoesDominio;
	}

	public void setOpcoesDominio(List<TipoDominioEnum> opcoesDominio) {

		this.opcoesDominio = opcoesDominio;
	}

}
