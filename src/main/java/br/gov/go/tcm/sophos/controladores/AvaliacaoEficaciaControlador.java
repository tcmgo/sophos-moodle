/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteData;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.*;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoDesempenhoServico;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoDominioEnum;
import br.gov.go.tcm.sophos.lazy.base.TableLazyPadrao;
import br.gov.go.tcm.sophos.negocio.AvaliacaoEficaciaNegocio;
import br.gov.go.tcm.sophos.negocio.DominioNegocio;
import br.gov.go.tcm.sophos.negocio.InscricaoNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
@Scope("session")
public class AvaliacaoEficaciaControlador extends ControladorBaseCRUD<AvaliacaoEficacia, AvaliacaoEficaciaNegocio> {

	private static final long serialVersionUID = 370915393940283176L;

	@Autowired
	private AvaliacaoEficaciaNegocio negocio;

	@Autowired
	private DominioNegocio dominioNegocio;

	@Autowired
	private InscricaoNegocio inscricaoNegocio;

	private List<Dominio> listaTipoDesempenhoServico;

	public AvaliacaoEficaciaNegocio getNegocio() {
		return negocio;
	}

	public void setNegocio(AvaliacaoEficaciaNegocio negocio) {
		this.negocio = negocio;
	}

	public void exibirAvaliacaoEficacia(Indicacao indicacao) {

		limpaPreenchimentos();

		this.getEntidade().setIndicacao(indicacao);
		this.getEntidade().setParticipante(indicacao.getParticipante());
		this.getEntidade().setEvento(indicacao.getEvento());

	}

	private void limpaPreenchimentos() {

		this.setEntidade(new AvaliacaoEficacia());
		this.getEntidade().setDataCadastro(new Date());
		this.getEntidade().setIndicacao(new Indicacao());
		this.getEntidade().setEvento(new Evento());
		this.getEntidade().setParticipante(new Participante());
		this.getEntidade().setDesempenhoServico(dominioNegocio.obterDominio(TipoDominioEnum.TipoDesempenhoServico,
				TipoDesempenhoServico.MELHORIA.getCodigo()));
		this.getEntidade().setMelhoriaDesempenho(true);
		this.getEntidade().setObservacao(new String());

	}

	public List<Dominio> getListaTipoDesempenhoServico() {
		this.listaTipoDesempenhoServico = dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.TipoDesempenhoServico);
		return listaTipoDesempenhoServico;
	}

	public void setListaTipoDesempenhoServico(List<Dominio> listaTipoDesempenhoServico) {
		if (listaTipoDesempenhoServico != null) {
			this.listaTipoDesempenhoServico = listaTipoDesempenhoServico;
		}
	}

	public boolean isMostrarOpcaoAvaliacaoEficacia(Indicacao indicacao) {

		if (!AjudanteObjeto.eReferencia(indicacao)) {
			return false;
		}

		boolean isDataEventoMenorDataAtual = indicacao.getEvento().getDataFimRealizacao() == null
				? AjudanteData.dataMenorQueReferencia(indicacao.getEvento().getDataFimPrevisto(), new Date())
				: AjudanteData.dataMenorQueReferencia(indicacao.getEvento().getDataFimRealizacao(), new Date());
		boolean isAprovado = indicacao.getAprovado() == null ? false : indicacao.getAprovado();
		boolean isPossuiAvaliacaoEficacia = AjudanteObjeto
				.eReferencia(negocio.obterPorAtributo("indicacao", indicacao));
		boolean isPossuiInscricao = inscricaoNegocio.existeInscricaoParaParticipante(indicacao.getParticipante(),
				indicacao.getEvento());

		if (isDataEventoMenorDataAtual && isAprovado && !isPossuiAvaliacaoEficacia && isPossuiInscricao) {
			return true;
		}

		return false;

	}

	public void preencherTipoDesempenhoServico() {

		if (AjudanteObjeto.eReferencia(this.getEntidade().getDesempenhoServico())) {
			Dominio desempenhoServico = dominioNegocio.obterDominio(TipoDominioEnum.TipoDesempenhoServico,
					this.getEntidade().getDesempenhoServico().getCodigo());
			this.getEntidade().setDesempenhoServico(desempenhoServico);
		}
	}

	@Override
	public TableLazyPadrao<AvaliacaoEficacia> getLista() {
		return null;
	}

}
