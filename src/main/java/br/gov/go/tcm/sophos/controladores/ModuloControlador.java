/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Instrutor;
import br.gov.go.tcm.sophos.entidade.Modulo;
import br.gov.go.tcm.sophos.entidade.ModuloInstrutor;
import br.gov.go.tcm.sophos.lazy.TableLazyModulo;
import br.gov.go.tcm.sophos.negocio.ModuloInstrutorNegocio;
import br.gov.go.tcm.sophos.negocio.ModuloNegocio;
import br.gov.go.tcm.sophos.persistencia.FrequenciaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

@Component
@Scope("view")
public class ModuloControlador extends ControladorBaseCRUD<Modulo, ModuloNegocio> implements Serializable {

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private ModuloNegocio negocio;

	@Autowired
	private ModuloInstrutorNegocio moduloInstrutorNegocio;

	@Autowired
	private TableLazyModulo lista;

	private Evento evento;

	private List<Instrutor> listaInstrutor;
	
	private int quantidadeEncontrosModulo;
	
	@Autowired
	private FrequenciaDAO frequenciaDAO;

	public void excluir(Modulo modulo) {
		
		if(this.lista.getLista().size() == 1){
			
			this.adicionarMensagemDeAlerta("Operação não realizada. Não é possivel excluir módulo quando evento tiver apenas um módulo");
		
		}else{

			if (AjudanteObjeto.eReferencia(modulo)) {
	
				ModuloInstrutor moduloInstrutor = this.moduloInstrutorNegocio.obterPorAtributo("modulo", modulo);
	
				if (AjudanteObjeto.eReferencia(moduloInstrutor)) {
					this.moduloInstrutorNegocio.excluir(moduloInstrutor);
				}
	
				this.getNegocio().excluir(modulo);
	
			}
		
		}

	}

	public void acaoEditar(Modulo modulo) {

		this.setEntidade(modulo);
		
		this.getEntidade().setInstrutorList(this.moduloInstrutorNegocio.listaPorAtributo("modulo", this.getEntidade()));

		this.setListaInstrutor(new ArrayList<Instrutor>());
		
		quantidadeEncontrosModulo = this.getEntidade().getQuantidadeEncontros();

		for (ModuloInstrutor moduloInstrutor : this.getEntidade().getInstrutorList()) {

			Instrutor instrutor = moduloInstrutor.getInstrutor();

			instrutor.setIdModuloInstrutor(moduloInstrutor.getId());

			this.getListaInstrutor().add(instrutor);
		}

	}

	public void acaoInserir() {

		BigDecimal somatorioCargaHorariaModulos = negocio.obterSomatorioCargaHorariaModulos(evento);
		DecimalFormat df = new DecimalFormat("###");
		
		if(quantidadeEncontrosModulo>0){
			
			if(quantidadeEncontrosModulo!=this.getEntidade().getQuantidadeEncontros()){
				
				if(frequenciaDAO.listarFrequenciasModulo(this.getEntidade()).size()!=0) {
					this.adicionarMensagemDeAlerta("Operação não realizada. Não é alterar quantidade de encontros pois já existem frequências cadastradas.");
					return ;
				}
				
				
			}
			
		}

		if (this.getEntidade().getId() == null && somatorioCargaHorariaModulos.add(this.getEntidade().getCargaHoraria())
				.compareTo(this.evento.getCargaHoraria()) == 1) {

			this.adicionarMensagemDeAlerta(
					"Operação não realizada. O cadastro desse módulo excederá a carga horária total do evento. O evento possui "
							+ "carga horaria de " + df.format(evento.getCargaHoraria()) + "h e já foram registradas "
							+ df.format(somatorioCargaHorariaModulos)
							+ "h nos módulos já cadastrados. Por favor, verifique a carga "
							+ "horária informada para o novo módulo e tente novamente.");

		} else {

			this.getEntidade().setInstrutorList(new ArrayList<ModuloInstrutor>());

			for (Instrutor instrutor : this.getListaInstrutor()) {

				ModuloInstrutor moduloInstrutor = new ModuloInstrutor();

				moduloInstrutor.setModulo(this.getEntidade());

				moduloInstrutor.setInstrutor(instrutor);

				moduloInstrutor.setDataCadastro(new Date());

				if (AjudanteObjeto.eReferencia(instrutor.getIdModuloInstrutor())) {

					moduloInstrutor.setId(instrutor.getIdModuloInstrutor());
				}

				this.getEntidade().getInstrutorList().add(moduloInstrutor);

			}

			this.getEntidade().setEvento(this.evento);

			super.acaoInserir();

		}

	}

	@Override
	protected void executarAposInserirSucesso() {

		super.executarAposInserirSucesso();
		quantidadeEncontrosModulo = 0;
		this.listaInstrutor = new ArrayList<Instrutor>();
	}

	public void selecionarEvento(Evento evento) {

		Modulo modulo = new Modulo();

		modulo.setNotaAprovacao(BigDecimal.valueOf(ModuloNegocio.RESOLUCAO_APROVACAO));

		modulo.setFrequenciaAprovacao(BigDecimal.valueOf(ModuloNegocio.RESOLUCAO_FREQUENCIA));

		this.setEntidade(modulo);

		this.getEntidade().setInstrutorList(new ArrayList<ModuloInstrutor>());

		listaInstrutor = new ArrayList<Instrutor>();

		this.evento = evento;
	}

	@Override
	protected ModuloNegocio getNegocio() {

		return this.negocio;
	}

	@Override
	public TableLazyModulo getLista() {

		if (AjudanteObjeto.eReferencia(this.evento)) {

			Map<String, Object> filters = new HashMap<String, Object>();

			filters.put("evento", this.evento);

			this.lista.setFilters(filters);

		}

		return this.lista;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public List<Instrutor> getListaInstrutor() {
		return listaInstrutor;
	}

	public void setListaInstrutor(List<Instrutor> listaInstrutor) {
		this.listaInstrutor = listaInstrutor;
	}

}
