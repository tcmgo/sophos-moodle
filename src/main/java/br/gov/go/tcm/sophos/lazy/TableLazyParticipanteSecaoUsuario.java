/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.lazy;

import br.gov.go.tcm.sophos.controladores.base.ControladorBase;
import br.gov.go.tcm.sophos.entidade.Participante;
import br.gov.go.tcm.sophos.entidade.Usuario;
import br.gov.go.tcm.sophos.lazy.base.TableLazyDTO;
import br.gov.go.tcm.sophos.lazy.base.TableLazyPadrao;
import br.gov.go.tcm.sophos.negocio.IndicacaoNegocio;
import br.gov.go.tcm.sophos.negocio.ParticipanteNegocio;
import br.gov.go.tcm.sophos.negocio.UsuarioNegocio;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@Scope("session")
public class TableLazyParticipanteSecaoUsuario extends TableLazyPadrao<Participante> {

	private static final long serialVersionUID = -6545514037278206167L;

	@Autowired
	private ParticipanteNegocio negocio;

	@Autowired
	private IndicacaoNegocio indicacaoNegocio;

	@Autowired
	private UsuarioNegocio usuarioNegocio;

	@Override
	protected ParticipanteNegocio getServico() {

		return this.negocio;
	}

	@Override
	public List<Participante> load(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {

		if (this.getFilters() != null) {
			filters = this.getFilters();
		}

		List<Participante> participantes = new ArrayList<>();
		List<Usuario> usuarios = new ArrayList<>();

		Usuario usuarioLogado = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.get(ControladorBase.USUARIO_LOGADO);

		usuarios = indicacaoNegocio.listaUsuariosSetor(usuarioLogado, this.getConsulta());
		List<Long> idsUsuariosSetor = new ArrayList<Long>();
		for (Usuario usuarioSetor : usuarios) {
			idsUsuariosSetor.add(usuarioSetor.getId());
		}

		usuarios = usuarioNegocio.listarPorListaIds(idsUsuariosSetor, first, pageSize, sortField, sortOrder, filters,
				this.getConsulta());
		for (Usuario usuarioFiltrado : usuarios) {
			participantes.add(usuarioFiltrado.getParticipante());
		}

		Long quantidade = Long.valueOf(participantes.size());

		this.setLista(participantes);

		TableLazyDTO<Participante> tableLazyDTO = new TableLazyDTO<Participante>(this.getLista(), quantidade);

		this.setRowCount(tableLazyDTO.getQuantidade().intValue());

		this.setPageSize(pageSize);
		
		this.setConsulta("");

		return tableLazyDTO.getLista();

	}
}
