/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.*;
import br.gov.go.tcm.sophos.entidade.enumeracao.PaginaEnum;
import br.gov.go.tcm.sophos.lazy.TableLazyProvedorEvento;
import br.gov.go.tcm.sophos.negocio.BancoFebrabanNegocio;
import br.gov.go.tcm.sophos.negocio.CidadeNegocio;
import br.gov.go.tcm.sophos.negocio.EstadoNegocio;
import br.gov.go.tcm.sophos.negocio.ProvedorEventoNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

@Component
@Scope("session")
public class ProvedorEventoControlador extends ControladorBaseCRUD<ProvedorEvento, ProvedorEventoNegocio> {

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private ProvedorEventoNegocio negocio;
	
	@Autowired
	private TableLazyProvedorEvento lista;
	
	@Autowired
	private NavegacaoControlador navegacaoControlador;
	
	@Autowired
	private BancoFebrabanNegocio bancoFebrabanNegocio;
	
	@Autowired
	private CidadeNegocio cidadeNegocio;
	
	@Autowired
	private EstadoNegocio estadoNegocio;
	
	private List<BancoFebraban> bancos;
	
	private Estado estado;
	
	private List<Estado> estados;
	
	private List<Cidade> cidades;

	@Override
	protected ProvedorEventoNegocio getNegocio() {

		return this.negocio;
	}
	
	public void listarCidades(){
		
		if(AjudanteObjeto.eReferencia(estado)){
			
			this.cidades = this.cidadeNegocio.listaPorAtributo("estado", estado);

		}
	}

	public void verificaExistenciaCNPJ() {
		if (!StringUtils.isEmpty(this.getEntidade().getCnpj())) {
			if(negocio.procurarProvedorEvento( this.getEntidade())) {
				this.adicionarMensagemDeErro("CNPJ já cadastrado. Verifique a lista de provedores de eventos.");
				this.getEntidade().setCnpj(null);
			}
		}
	}

	public void acaoAbrirTela(ProvedorEvento entidade, Boolean visualizar){

		if(AjudanteObjeto.eReferencia(entidade)){
			
			if(AjudanteObjeto.eReferencia(entidade.getEndereco()) && AjudanteObjeto.eReferencia(entidade.getEndereco().getCidade())){
				
				this.estado = entidade.getEndereco().getCidade().getEstado();
			
				this.listarCidades();
			}
			
			this.setEntidade(entidade);
			
			entidade.setEndereco(AjudanteObjeto.eReferencia(entidade.getEndereco()) ? entidade.getEndereco() : new Endereco());
			
			
		}else{
			
			this.criarNovaInstanciaEntidade();
		}
		
		this.setVisualizar(visualizar);
		
		this.navegacaoControlador.processarNavegacao(PaginaEnum.TABELA_PROVEDOR_EVENTO_CADASTRO);
		
	}
	
	@Override
	protected void criarNovaInstanciaEntidade() {
		
		this.setEntidade(new ProvedorEvento());
		
		this.getEntidade().setDataCadastro(new Date());
		
		this.getEntidade().setEndereco(new Endereco());
		
		this.estado = new Estado();
		
	}
	
	@Override
	public TableLazyProvedorEvento getLista() {

		return this.lista;
	}

	public List<BancoFebraban> getBancos() {
		
		if(!AjudanteObjeto.eReferencia(bancos)){
			
			this.bancos = this.bancoFebrabanNegocio.obterListaBancos();
		}
		
		return this.bancos;
	}

	public List<Estado> getEstados() {
		
		if(!AjudanteObjeto.eReferencia(estados)){
			
			this.estados = this.estadoNegocio.listar();
		}
	
		return estados;
	}

	public List<Cidade> getCidades() {
		
		return cidades;
	}

	public Estado getEstado() {
		
		return estado;
	}

	public void setEstado(Estado estado) {
		
		this.estado = estado;
	}

}
