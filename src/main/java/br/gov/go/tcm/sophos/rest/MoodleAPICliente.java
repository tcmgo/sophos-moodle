package br.gov.go.tcm.sophos.rest;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.rest.base.APIClientBase;
import br.gov.go.tcm.sophos.rest.contexto.ApplicationContextProvider;
import br.gov.go.tcm.sophos.rest.dto.MoodleCursoDTO;
import br.gov.go.tcm.sophos.rest.dto.MoodlePessoaDTO;
import br.gov.go.tcm.sophos.rest.dto.MoodleRespostaDeErroDTO;
import br.gov.go.tcm.sophos.rest.dto.MoodleTabelaPrincipalNotaDTO;
import br.gov.go.tcm.sophos.rest.dto.MoodleTokenDTO;

@Service
public class MoodleAPICliente extends APIClientBase {

    @Autowired
    private ApplicationContextProvider applicationContextProvider;

    private RestTemplate restTemplate;

    private String moodleContexto;

    private String moodleContextoAPI;

    private String moodleUsuarioAPI;

    private String moodleSenhaUsuarioAPI;

    private MoodleTokenDTO moodleTokenUsuario;

    private String moodleIdPapelEstudante;

    private String moodleNomeServicoExterno;

    @PostConstruct
    private void init() {

        this.restTemplate = super.obterRestTemplate();

        this.moodleContexto = (String) this.applicationContextProvider.getApplicationContext().getBean("moodleContexto");

        this.moodleContextoAPI = this.moodleContexto + this.applicationContextProvider.getApplicationContext().getBean("caminhoMoodleAPI");

        this.moodleUsuarioAPI = (String) this.applicationContextProvider.getApplicationContext().getBean("moodleUsuarioAPI");

        this.moodleSenhaUsuarioAPI = (String) this.applicationContextProvider.getApplicationContext().getBean("moodleSenhaUsuarioAPI");

        this.moodleNomeServicoExterno = (String) this.applicationContextProvider.getApplicationContext().getBean("moodleNomeServicoExterno");

        this.moodleIdPapelEstudante = (String) this.applicationContextProvider.getApplicationContext().getBean("moodleIdPapelEstudante");
    }

    private void moodleObterTokenUsuario() {

        String url = this.moodleContexto
                + "/login/token.php?"
                + "service=" + this.moodleNomeServicoExterno
                + "&username=" + this.moodleUsuarioAPI
                + "&password=" + this.moodleSenhaUsuarioAPI;

        try {

            this.moodleTokenUsuario = this.restTemplate.getForObject(url, MoodleTokenDTO.class);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public List<MoodleCursoDTO> moodleObterListaCursos() {

        this.moodleObterTokenUsuario();

        final String moodleWsFunction = "core_course_get_courses";

        final String url = this.moodleContextoAPI
                + "wstoken=" + this.moodleTokenUsuario.getToken()
                + "&wsfunction=" + moodleWsFunction
                + "&moodlewsrestformat=json";

        try {

            return Arrays.asList(this.restTemplate.getForEntity(URI.create(url), MoodleCursoDTO[].class).getBody());

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    public MoodleCursoDTO moodleObterCursoPorId(Long id) {

        this.moodleObterTokenUsuario();

        final String moodleWsFunction = "core_course_get_courses";

        final String url = this.moodleContextoAPI
                + "wstoken=" + this.moodleTokenUsuario.getToken()
                + "&wsfunction=" + moodleWsFunction
                + "&moodlewsrestformat=json"
                + "&options[ids][0]=" + id.intValue();

        try {

            MoodleCursoDTO[] cursos = this.restTemplate.getForEntity(url, MoodleCursoDTO[].class).getBody();

            return cursos.length > 0? cursos[0] : null;

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    public void forcarPrimeiroLoginPessoaMoodle(String cpf, String senha) {

        this.moodleObterTokenUsuario();

        String cpfTratado = cpf.replace(".", "").replace("-", "");

        final String url = this.moodleContexto
                + "/login/token.php?service=login-rest"
                + "&username=" + cpfTratado
                + "&password=" + senha;

        try {

            /*
                O Plugin oficial do Moodle responsável por autenticação através de bancos de dados externos
                faz uma verificação toda vez que alguém tenta um login. A chamada abaixo faz com que um login seja tentado
                na API Rest do Moodle, e se o cpf e a senha estiverem corretos com o que está cadastrado no Sophos, o Moodle irá
                criar este usuário internamente, mesmo que o login via Rest API não seja permitido para este usuário.
                Ou seja, a chamada abaixo força a criação no Moodle de uma pessoa cadastrada no Sophos que nunca tenha acessado o Moodle.
                Através do primeiro login abaixo, a criação é feita.
                O retorno não interessa já que o login não será bem sucedido por falta de permissão de acesso via Rest API.
            */

            this.restTemplate.getForObject(url, Object.class);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public MoodlePessoaDTO moodleObterPessoaPorCpf(String cpf) {

        this.moodleObterTokenUsuario();

        final String moodleWsFunction = "core_user_get_users_by_field";

        String cpfTratado = cpf.replace(".", "").replace("-", "");

        final String url = this.moodleContextoAPI
                + "wstoken=" + this.moodleTokenUsuario.getToken()
                + "&wsfunction=" + moodleWsFunction
                + "&moodlewsrestformat=json"
                + "&field=" + "username"
                + "&values[0]=" + cpfTratado;

        try {

            MoodlePessoaDTO[] pessoas = this.restTemplate.getForEntity(url, MoodlePessoaDTO[].class).getBody();

            return pessoas.length > 0? pessoas[0] : null;

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    public Boolean moodleMatricularEstudanteCurso(String pessoaCpf, Long cursoMoodleId) {

        this.moodleObterTokenUsuario();

        MoodleCursoDTO moodleCursoDTO = this.moodleObterCursoPorId(cursoMoodleId);

        if(!AjudanteObjeto.eReferencia(moodleCursoDTO)) {

            return Boolean.FALSE;
        }
        MoodlePessoaDTO moodlePessoaDTO = this.moodleObterPessoaPorCpf(pessoaCpf);

        if(!AjudanteObjeto.eReferencia(moodlePessoaDTO)) {

            return Boolean.FALSE;
        }
        final String moodleWsFunction = "enrol_manual_enrol_users";

        final String url = this.moodleContextoAPI
                + "wstoken=" + this.moodleTokenUsuario.getToken()
                + "&wsfunction=" + moodleWsFunction
                + "&moodlewsrestformat=json"
                + "&enrolments[0][roleid]=" + this.moodleIdPapelEstudante
                + "&enrolments[0][userid]=" + moodlePessoaDTO.getId().intValue()
                + "&enrolments[0][courseid]=" + cursoMoodleId.intValue();

        ResponseEntity<MoodleRespostaDeErroDTO> responseEntity = this.restTemplate.getForEntity(url, MoodleRespostaDeErroDTO.class);

        return !AjudanteObjeto.eReferencia(responseEntity.getBody())? Boolean.TRUE : Boolean.FALSE;
    }

    public MoodleTabelaPrincipalNotaDTO moodleObterNotaPorId(Long cursoMoodleId, Long userId) {

        this.moodleObterTokenUsuario();

        final String moodleWsFunction = "gradereport_user_get_grades_table";

        final String url = this.moodleContextoAPI
                + "wstoken=" + this.moodleTokenUsuario.getToken()
                + "&wsfunction=" + moodleWsFunction
                + "&moodlewsrestformat=json"
                + "&courseid=" + cursoMoodleId.intValue()
                + "&userid=" + userId.longValue();

        try {

        	MoodleTabelaPrincipalNotaDTO notas = 
        			this.restTemplate.getForEntity(url, MoodleTabelaPrincipalNotaDTO.class).getBody();

            return notas;

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    public MoodleTabelaPrincipalNotaDTO moodleObterNotas(Long cursoMoodleId) {

        this.moodleObterTokenUsuario();

        final String moodleWsFunction = "gradereport_user_get_grades_table";

        final String url = this.moodleContextoAPI
                + "wstoken=" + this.moodleTokenUsuario.getToken()
                + "&wsfunction=" + moodleWsFunction
                + "&moodlewsrestformat=json"
                + "&courseid=" + cursoMoodleId.intValue();

        try {

        	MoodleTabelaPrincipalNotaDTO notas = 
        			this.restTemplate.getForEntity(url, MoodleTabelaPrincipalNotaDTO.class).getBody();

            return notas;

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }
    
    public boolean moodleRetirarMatriculaEstudanteCurso(String pessoaCpf, Long cursoMoodleId) {

        this.moodleObterTokenUsuario();

        MoodleCursoDTO moodleCursoDTO = this.moodleObterCursoPorId(cursoMoodleId);

        if(!AjudanteObjeto.eReferencia(moodleCursoDTO)) {

            return Boolean.FALSE;
        }
        MoodlePessoaDTO moodlePessoaDTO = this.moodleObterPessoaPorCpf(pessoaCpf);

        if(!AjudanteObjeto.eReferencia(moodlePessoaDTO)) {

            return Boolean.FALSE;
        }
        final String moodleWsFunction = "enrol_manual_unenrol_users";

        final String url = this.moodleContextoAPI
                + "wstoken=" + this.moodleTokenUsuario.getToken()
                + "&wsfunction=" + moodleWsFunction
                + "&moodlewsrestformat=json"
                + "&enrolments[0][roleid]=" + this.moodleIdPapelEstudante
                + "&enrolments[0][userid]=" + moodlePessoaDTO.getId().intValue()
                + "&enrolments[0][courseid]=" + cursoMoodleId.intValue();

        ResponseEntity<MoodleRespostaDeErroDTO> responseEntity = this.restTemplate.getForEntity(url, MoodleRespostaDeErroDTO.class);

        return !AjudanteObjeto.eReferencia(responseEntity.getBody())? Boolean.TRUE : Boolean.FALSE;
    }
}
