/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.base.EntidadeLogica;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "questionario")
@Banco(nome = Banco.SOPHOS)
public class Questionario extends EntidadeLogica {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = -3838856954155296523L;

	@Column(name = "pergunta", nullable = false, length = 2000)
	/** Atributo pergunta. */
	private String pergunta;

	@Column(name = "para_instrutor")
	private Boolean paraInstrutor;

	@OneToMany(mappedBy = "questionario", cascade = CascadeType.ALL)
	/** Atributo opcoes. */
	private List<Opcao> opcoes;

	@Transient
	/** Atributo opcoesRemovidas. */
	private List<Opcao> opcoesRemovidas;

	/**
	 * Retorna o valor do atributo <code>pergunta</code>
	 *
	 * @return <code>String</code>
	 */
	public String getPergunta() {

		return this.pergunta;
	}

	/**
	 * Define o valor do atributo <code>pergunta</code>.
	 *
	 * @param pergunta
	 */
	public void setPergunta(final String pergunta) {

		this.pergunta = pergunta;
	}

	/**
	 * Retorna o valor do atributo <code>opcoes</code>
	 *
	 * @return <code>List<Opcao></code>
	 */
	public List<Opcao> getOpcoes() {

		if (!AjudanteObjeto.eReferencia(this.opcoes)) {

			this.opcoes = new ArrayList<Opcao>();
		}

		return this.opcoes;
	}

	/**
	 * Define o valor do atributo <code>opcoes</code>.
	 *
	 * @param opcoes
	 */
	public void setOpcoes(final List<Opcao> opcoes) {

		this.opcoes = opcoes;
	}

	public Boolean getParaInstrutor() {
		return paraInstrutor;
	}

	public void setParaInstrutor(Boolean paraInstrutor) {
		this.paraInstrutor = paraInstrutor;
	}

	public List<Opcao> getOpcoesRemovidas() {
		if(opcoesRemovidas == null){
			opcoesRemovidas = new ArrayList<Opcao>();
		}
		return opcoesRemovidas;
	}

	public void setOpcoesRemovidas(List<Opcao> opcoesRemovidas) {
		this.opcoesRemovidas = opcoesRemovidas;
	}

}
