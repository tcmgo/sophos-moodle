/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade.enumeracao;

import br.gov.go.tcm.estrutura.entidade.enumeracao.StringEnumeracaoPersistente;

public enum TipoDesempenhoServico implements StringEnumeracaoPersistente{
	
	NENHUMA_MELHORIA(1L), POUCA_MELHORIA(2L), MELHORIA(3L), SIGNIFICATIVA_MELHORIA(4L);
	
	private Long codigo;
	
	private TipoDesempenhoServico(Long codigo) {
		this.codigo = codigo;
	}

	public Long getCodigo() {
		return codigo;
	}

	@Override
	public String getValorOrdinal() {
		// TODO Auto-generated method stub
		return null;
	}

}
