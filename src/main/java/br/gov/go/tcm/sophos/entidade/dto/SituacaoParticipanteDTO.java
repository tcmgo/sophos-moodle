/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade.dto;

import br.gov.go.tcm.sophos.entidade.Inscricao;

import java.io.Serializable;
import java.math.BigDecimal;

public class SituacaoParticipanteDTO implements Serializable {

	private static final long serialVersionUID = 7976889475907375757L;

	private Boolean aprovado;

	private Boolean avaliado;

	private BigDecimal nota;

	private Inscricao inscricao;

	private MeuEventoDTO meuEventoRef;

	public SituacaoParticipanteDTO() {
		super();
	}

	public SituacaoParticipanteDTO(Boolean aprovado, Boolean avaliado, BigDecimal nota, Inscricao inscricao) {
		super();
		this.aprovado = aprovado;
		this.avaliado = avaliado;
		this.nota = nota;
		this.inscricao = inscricao;
	}

	public SituacaoParticipanteDTO(Boolean aprovado, Boolean avaliado, BigDecimal nota, Inscricao inscricao,
			MeuEventoDTO meuEventoRef) {
		super();
		this.aprovado = aprovado;
		this.avaliado = avaliado;
		this.nota = nota;
		this.inscricao = inscricao;
		this.meuEventoRef = meuEventoRef;
	}

	public MeuEventoDTO getMeuEventoRef() {
		return meuEventoRef;
	}

	public void setMeuEventoRef(MeuEventoDTO meuEventoRef) {
		this.meuEventoRef = meuEventoRef;
	}

	public Boolean getAprovado() {
		return aprovado;
	}

	public void setAprovado(Boolean aprovado) {
		this.aprovado = aprovado;
	}

	public Boolean getAvaliado() {
		return avaliado;
	}

	public void setAvaliado(Boolean avaliado) {
		this.avaliado = avaliado;
	}

	public BigDecimal getNota() {
		return nota;
	}

	public void setNota(BigDecimal nota) {
		this.nota = nota;
	}

	public Inscricao getInscricao() {
		return inscricao;
	}

	public void setInscricao(Inscricao inscricao) {
		this.inscricao = inscricao;
	}

}
