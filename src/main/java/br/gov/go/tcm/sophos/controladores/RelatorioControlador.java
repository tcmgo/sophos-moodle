/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 *
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 *
 * Este cabeçalho deve ser mantido.
 *
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteData;
import br.gov.go.tcm.estrutura.entidade.orcafi.Municipio;
import br.gov.go.tcm.estrutura.entidade.transiente.Arquivo;
import br.gov.go.tcm.estrutura.negocio.ajudante.AjudanteDeArquivos;
import br.gov.go.tcm.estrutura.persistencia.orcafi.contrato.MunicipioDAO;
import br.gov.go.tcm.estrutura.relatorio.GeradorRelatorio;
import br.gov.go.tcm.estrutura.relatorio.fonteDeDados.AjudanteParametrosRelatorio;
import br.gov.go.tcm.estrutura.relatorio.fonteDeDados.FonteDadosRelatorioJavaBean;
import br.gov.go.tcm.estrutura.relatorio.fonteDeDados.ParametroVisao;
import br.gov.go.tcm.sophos.ajudante.AjudanteCSV;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.ajudante.AjudanteRelatorio;
import br.gov.go.tcm.sophos.controladores.base.ControladorBase;
import br.gov.go.tcm.sophos.entidade.*;
import br.gov.go.tcm.sophos.entidade.dto.AvaliacaoReacaoDTO;
import br.gov.go.tcm.sophos.entidade.enumeracao.*;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.negocio.*;
import br.gov.go.tcm.sophos.persistencia.EventoDAO;
import br.gov.go.tcm.sophos.persistencia.InstrutorDAO;
import br.gov.go.tcm.sophos.persistencia.ModuloDAO;
import br.gov.go.tcm.sophos.persistencia.ModuloInstrutorDAO;
import br.gov.go.tcm.sophos.persistencia.base.DAO;
import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.type.OrientationEnum;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.util.CellRangeAddress;
import org.hibernate.SessionFactory;
import org.hibernate.impl.SessionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import java.io.*;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
@Scope("session")
public class RelatorioControlador extends ControladorBase implements Serializable {

    private static final long serialVersionUID = 7926664851995574887L;

    @Autowired
    private CertificadoNegocio certificadoNegocio;

    @Autowired
    private InstrutorNegocio instrutorNegocio;

    @Autowired
    private DominioNegocio dominioNegocio;

    @Autowired
    private EstadoNegocio estadoNegocio;

    @Autowired
    private CidadeNegocio cidadeNegocio;

    @Autowired
    private ModuloNegocio moduloNegocio;

    @Autowired
    private InscricaoNegocio inscricaoNegocio;

    @Autowired
    private ParticipanteNegocio participanteNegocio;

    @Autowired
    private AvaliacaoNegocio avaliacaoNegocio;

    @Autowired
    private QuestionarioAvaliacaoNegocio questionarioAvaliacaoNegocio;

    @Autowired
    private OpcaoNegocio opcaoNegocio;

    @Autowired
    private NotaNegocio notaNegocio;

    @Autowired
    private FrequenciaNegocio frequenciaNegocio;

    @Autowired
    private MunicipioDAO municipioDAO;

    @Autowired
    private ModuloDAO moduloDAO;

    @Autowired
    private InstrutorDAO instrutorDAO;

    @Autowired
    private EventoDAO eventoDAO;

    @Autowired
    private ModuloInstrutorDAO moduloInstrutorDAO;

    @Autowired
    private AjudanteDeArquivos ajudanteArquivos;

    @Autowired
    private AnexoNegocio anexoNegocio;

    @Autowired
    @Qualifier(DAO.SESSION_FACTORY_SOPHOS)
    private SessionFactory sessionFactory;

    @Autowired
    private ServletContext context;

    private Evento evento;

    private Modulo modulo;

    private Instrutor instrutor;

    private Inscricao inscricao;

    private Participante participante;

    private Dominio tipoParticipante;

    private Dominio tipoEvento;

    private Dominio publicoAlvo;

    private Dominio eixoTematico;

    private Dominio modalidade;

    private LocalizacaoEvento localizacaoEvento;

    private ProvedorEvento provedorEvento;

    private Estado estado;

    private Cidade cidade;

    private Dominio cargo;

    private BooleanEnum indicada;

    private AutorizaEnum autorizada;

    private BooleanEnum pne;

    private Date dataInicio;

    private Date dataFim;

    private List<RelatorioEnum> relatorioList;

    private List<Dominio> tipoParticipanteList;

    private List<Dominio> tipoEventoList;

    private List<Dominio> eixoTematicoList;

    private List<Dominio> modalidadeList;

    private List<Estado> estadoList;

    private List<Cidade> cidadeList;

    private List<Dominio> cargoList;

    private RelatorioEnum relatorioSelecionado;

    private Map<String, Object> filtro;

    private List<Instrutor> instrutorList;

    private List<BooleanEnum> booleanItemList;

    private List<AutorizaEnum> autorizaItemList;

    private List<Evento> eventoList;

    private int tipoSaida;

    private BooleanEnum participantesPresentes;

    private BooleanEnum aprovados;

    private List<String> selectedLetras;
    private List<String> letras;

    private BigDecimal cargaHorarioModulo = null;

    private Date dataModuloInicio = null;

    private Date dataModuloFim = null;

    private BooleanEnum importado;

    @PostConstruct
    public void inicializarTela() {

        if (AjudanteObjeto.eReferencia(this.getUsuarioLogado())) {

            this.limparCampos();

            init();

            this.tipoParticipanteList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.TipoPublicoAlvo);

            this.tipoEventoList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.TipoEvento);

            this.eixoTematicoList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.EixoTematico);

            this.modalidadeList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.Modalidade);

            this.estadoList = this.estadoNegocio.listar();

            this.cargoList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.NomeCargo);
            Collections.sort(this.cargoList);

            this.relatorioList = new ArrayList<RelatorioEnum>();

            for (RelatorioEnum relatorio : RelatorioEnum.values()) {

                for (PerfilUsuario perfilUsuario : this.getUsuarioLogado().getPerfilList()) {

                    if (relatorio.getListPerfil().contains(perfilUsuario.getPerfil())) {

                        this.relatorioList.add(relatorio);

                        break;
                    }
                }
            }

            Collections.sort(relatorioList, new Comparator<RelatorioEnum>() {

                @Override
                public int compare(RelatorioEnum o1, RelatorioEnum o2) {
                    return o1.getDescricao().compareTo(o2.getDescricao());
                }
            });

        }

    }

    public List<String> getSelectedLetras() {
        return selectedLetras;
    }

    public void setSelectedLetras(List<String> selectedLetras) {
        this.selectedLetras = selectedLetras;
    }

    public List<String> getLetras() {
        return letras;
    }


    public void init() {

        letras = new ArrayList<String>();
        selectedLetras = new ArrayList<String>();
        letras.add("Todas");
        letras.add("A");
        letras.add("B");
        letras.add("C");
        letras.add("D");
        letras.add("E");
        letras.add("F");
        letras.add("G");
        letras.add("H");
        letras.add("I");
        letras.add("J");
        letras.add("K");
        letras.add("L");
        letras.add("M");
        letras.add("N");
        letras.add("O");
        letras.add("P");
        letras.add("Q");
        letras.add("R");
        letras.add("S");
        letras.add("T");
        letras.add("U");
        letras.add("V");
        letras.add("W");
        letras.add("X");
        letras.add("Y");
        letras.add("Z");
        selectedLetras.add("Todas");
        cargaHorarioModulo = new BigDecimal("0.0");
        dataModuloInicio = null;
        dataModuloFim = null;


    }

    public void listarCidades() {

        if (AjudanteObjeto.eReferencia(estado)) {

            this.cidadeList = this.cidadeNegocio.listaPorAtributo("estado", estado);

        }
    }

    public void limparCampos() {

        this.eventoList = new ArrayList<Evento>();

        this.evento = new Evento();

        this.modulo = new Modulo();

        this.instrutor = new Instrutor();

        this.inscricao = null;

        this.indicada = null;

        this.autorizada = null;

        this.pne = null;

        this.dataFim = null;

        this.dataInicio = null;

        this.tipoParticipante = new Dominio();

        this.tipoEvento = new Dominio();

        this.publicoAlvo = new Dominio();

        this.eixoTematico = new Dominio();

        this.modalidade = new Dominio();

        this.localizacaoEvento = new LocalizacaoEvento();

        this.provedorEvento = new ProvedorEvento();

        this.estado = new Estado();

        this.cidade = new Cidade();

        this.cargo = new Dominio();

        this.importado = null;
    }

    public String gerarEmailsParticipante() {

        try {

            File arquivoTemporario = null;

            Set<String> listaEmails = new HashSet<String>();

            if (AjudanteObjeto.eReferencia(this.evento) && AjudanteObjeto.eReferencia(this.evento.getId())) {

                List<Inscricao> inscricaoList = null;

                if (AjudanteObjeto.eReferencia(autorizada) && autorizada.getValor() != null && autorizada.getValor()) {

                    inscricaoList = this.inscricaoNegocio.listarInscricoesAutorizadas(evento);

                } else if (AjudanteObjeto.eReferencia(autorizada) && autorizada.getValor() != null && !autorizada.getValor()) {

                    inscricaoList = this.inscricaoNegocio.listarInscricoesNaoAutorizadas(evento);

                } else if (AjudanteObjeto.eReferencia(autorizada) && autorizada.getValor() == null) {

                    inscricaoList = this.inscricaoNegocio.listarInscricoesAguardando(evento);

                } else {

                    inscricaoList = this.inscricaoNegocio.listaPorAtributo("evento", evento);
                }

                arquivoTemporario = File.createTempFile("emails_" + evento.getTitulo().replace(" ", "_"), ".txt");

                for (Inscricao inscricao : inscricaoList) {

                    listaEmails.add(inscricao.getParticipante().getPessoa().getEmail());
                }

            } else {


                List<Participante> participantes = this.participanteNegocio.listar();

                for (Participante participante : participantes) {

                    listaEmails.add(participante.getPessoa().getEmail());
                }

                arquivoTemporario = File.createTempFile("emails_participantes", ".txt");
            }

            FileWriter writer = new FileWriter(arquivoTemporario);

            AjudanteCSV.writeLine(writer, listaEmails);

            writer.flush();
            writer.close();

            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(arquivoTemporario));

            this.download(bufferedInputStream, TipoArquivo.TXT);

        } catch (IOException e) {

            e.printStackTrace();
        }

        return "index";
    }

    public String gerarRelatorioXLS() {

        try {

            List<Modulo> modulos = this.moduloNegocio.listaPorAtributo("evento", evento);

            if (modulos.size() == 0) {

                this.adicionarMensagemDeAlerta("O relatório não pode ser emitido porque o evento selecionado não possui módulos cadastrados. Edite o evento para ter ao menos um MÓDULO ÚNICO.");

            } else {

                File arquivoTemporario = null;

                HSSFWorkbook workbook = new HSSFWorkbook();

                HSSFSheet sheet = workbook.createSheet("PARTICIPANTES");

                if (AjudanteObjeto.eReferencia(this.evento) && AjudanteObjeto.eReferencia(this.evento.getId())) {

                    List<Inscricao> inscricaoList = null;

                    if (AjudanteObjeto.eReferencia(autorizada) && autorizada.getValor() != null && autorizada.getValor()) {

                        inscricaoList = this.inscricaoNegocio.listarInscricoesAutorizadas(evento);

                    } else if (AjudanteObjeto.eReferencia(autorizada) && autorizada.getValor() != null && !autorizada.getValor()) {

                        inscricaoList = this.inscricaoNegocio.listarInscricoesNaoAutorizadas(evento);

                    } else if (AjudanteObjeto.eReferencia(autorizada) && autorizada.getValor() == null) {

                        inscricaoList = this.inscricaoNegocio.listarInscricoesAguardando(evento);

                    } else {

                        inscricaoList = this.inscricaoNegocio.listaPorAtributo("evento", evento);
                    }

                    arquivoTemporario = File.createTempFile("participantes_" + evento.getTitulo().replace(" ", "_"), ".xls");

                    Font fonte = workbook.createFont();
                    fonte.setBoldweight(Font.BOLDWEIGHT_BOLD);
                    fonte.setFontHeight(Short.valueOf("200"));
                    HSSFCellStyle estiloCentralizadoNegrito = workbook.createCellStyle();
                    estiloCentralizadoNegrito.setAlignment(HSSFCellStyle.ALIGN_CENTER);
                    estiloCentralizadoNegrito.setFont(fonte);

                    HSSFCellStyle estiloCentralizado = workbook.createCellStyle();
                    estiloCentralizado.setAlignment(HSSFCellStyle.ALIGN_CENTER);

                    HSSFRow rowEvento = sheet.createRow(0);
                    Cell celulaEvento = rowEvento.createCell(0);
                    celulaEvento.setCellValue(evento.getTitulo());
                    celulaEvento.setCellStyle(estiloCentralizadoNegrito);
                    sheet = this.mesclarCelulas(sheet, 0, 0, 0, 22);

                    HSSFRow rowEixoTematico = sheet.createRow(1);
                    Cell celulaEixoTematico = rowEixoTematico.createCell(0);
                    celulaEixoTematico.setCellValue("Eixo temático: " + evento.getEixoTematico().getDescricao());
                    celulaEixoTematico.setCellStyle(estiloCentralizado);
                    sheet = this.mesclarCelulas(sheet, 1, 1, 0, 22);

                    HSSFRow rowDataEvento = sheet.createRow(2);
                    Date dataInicioEvento = evento.getDataInicioRealizacao() != null ? evento.getDataInicioRealizacao() : evento.getDataInicioPrevisto();
                    Date dataFinalEvento = evento.getDataFimRealizacao() != null ? evento.getDataFimRealizacao() : evento.getDataFimPrevisto();
                    Cell celulaDataEvento = rowDataEvento.createCell(0);
                    celulaDataEvento.setCellValue("Período: " + AjudanteData.converterDataParaString(dataInicioEvento, "dd/MM/yyyy") + " a " + AjudanteData.converterDataParaString(dataFinalEvento, "dd/MM/yyyy"));
                    celulaDataEvento.setCellStyle(estiloCentralizado);
                    sheet = this.mesclarCelulas(sheet, 2, 2, 0, 22);

                    HSSFRow rowQuebra = sheet.createRow(3);
                    rowQuebra.createCell(0).setCellValue("");
                    sheet = this.mesclarCelulas(sheet, 3, 3, 0, 22);

                    HSSFRow rowhead = sheet.createRow(4);

                    this.gerarTitulosXlsParticipante(rowhead, Boolean.TRUE);

                    for (int i = 0; i < inscricaoList.size(); i++) {

                        HSSFRow row = sheet.createRow(i + 5);

                        Inscricao inscricao = inscricaoList.get(i);

                        this.gerarLinhaXlsParticipante(row, inscricao.getParticipante(), modulos);
                    }

                } else {

                    arquivoTemporario = File.createTempFile("participantes", ".xls");

                    HSSFRow rowhead = sheet.createRow(0);

                    this.gerarTitulosXlsParticipante(rowhead, Boolean.FALSE);

                    List<Participante> participantes = this.participanteNegocio.listar();

                    for (int i = 0; i < participantes.size(); i++) {

                        HSSFRow row = sheet.createRow(i + 1);

                        Participante participante = participantes.get(i);

                        this.gerarLinhaXlsParticipante(row, participante, null);
                    }
                }

                FileOutputStream fileOut = new FileOutputStream(arquivoTemporario);

                workbook.write(fileOut);

                fileOut.close();

                BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(arquivoTemporario));

                this.download(bufferedInputStream, TipoArquivo.XLS);

            }

        } catch (Exception e) {

            System.out.println(e.getMessage());

            e.printStackTrace();
        }

        return "index";
    }

    private void gerarTitulosXlsParticipante(HSSFRow rowhead, Boolean possuiEvento) {

        rowhead.createCell(0).setCellValue("Código");
        rowhead.createCell(1).setCellValue("Nome");
        rowhead.createCell(2).setCellValue("CPF");
        rowhead.createCell(3).setCellValue("Email");
        rowhead.createCell(4).setCellValue("Data Nascimento");
        rowhead.createCell(5).setCellValue("Sexo");
        rowhead.createCell(6).setCellValue("Telefone");
        rowhead.createCell(7).setCellValue("Celular");
        rowhead.createCell(8).setCellValue("Cidade");
        rowhead.createCell(9).setCellValue("Estado");
        rowhead.createCell(10).setCellValue("Bairro");
        rowhead.createCell(11).setCellValue("CEP");
        rowhead.createCell(12).setCellValue("logradouro");
        rowhead.createCell(13).setCellValue("Complemento");
        rowhead.createCell(14).setCellValue("Número");
        rowhead.createCell(15).setCellValue("Lotação");
        rowhead.createCell(16).setCellValue("Matricula");
        rowhead.createCell(17).setCellValue("Cargo");
        rowhead.createCell(18).setCellValue("Data de Admissão");
        rowhead.createCell(19).setCellValue("Profissão");
        rowhead.createCell(20).setCellValue("Formação Acadêmica");
        rowhead.createCell(21).setCellValue("Escolaridade");
        rowhead.createCell(22).setCellValue("Tipo");
        if (possuiEvento) {
            rowhead.createCell(23).setCellValue("Nota");
            rowhead.createCell(24).setCellValue("Frequência(%)");
        }

    }

    private void gerarLinhaXlsParticipante(HSSFRow row, Participante participante, List<Modulo> modulos) {

        row.createCell(0).setCellValue(participante.getId());
        row.createCell(1).setCellValue(participante.getPessoa().getNome());
        row.createCell(2).setCellValue(participante.getPessoa().getCpf());
        row.createCell(3).setCellValue(participante.getPessoa().getEmail());
        row.createCell(4).setCellValue(AjudanteData.converterDataParaString(participante.getPessoa().getDataNascimento(), "dd/MM/yyyy"));

        if (AjudanteObjeto.eReferencia(participante.getPessoa().getSexo())) {

            row.createCell(5).setCellValue(participante.getPessoa().getSexo().getDescricao());
        }

        row.createCell(6).setCellValue(participante.getPessoa().getTelefone());
        row.createCell(7).setCellValue(participante.getPessoa().getCelular());

        if (AjudanteObjeto.eReferencia(participante.getEndereco())) {

            if (AjudanteObjeto.eReferencia(participante.getEndereco().getCidade())) {

                row.createCell(8).setCellValue(participante.getEndereco().getCidade().getDescricao());

                row.createCell(9).setCellValue(participante.getEndereco().getCidade().getEstado().getUf());

            } else if (AjudanteObjeto.eReferencia(participante.getEndereco().getMunicipioId())) {

                Municipio municipio = (Municipio) municipioDAO.obterEntidadePorId(Municipio.class, participante.getEndereco().getMunicipioId());

                row.createCell(8).setCellValue(AjudanteObjeto.eReferencia(municipio) ? municipio.getDescricao() : "");
            }

            row.createCell(10).setCellValue(participante.getEndereco().getBairro());
            row.createCell(11).setCellValue(participante.getEndereco().getCep());
            row.createCell(12).setCellValue(participante.getEndereco().getLogradouro());
            row.createCell(13).setCellValue(participante.getEndereco().getComplemento());
            row.createCell(14).setCellValue(participante.getEndereco().getNumero());
        }

        row.createCell(15).setCellValue(participante.getLotacao());
        row.createCell(16).setCellValue(participante.getMatricula());
        row.createCell(17).setCellValue(participante.getCargo());
        row.createCell(18).setCellValue(AjudanteData.converterDataParaString(participante.getDataAdmissao(), "dd/MM/yyyy"));
        row.createCell(19).setCellValue(participante.getProfissao());

        if (AjudanteObjeto.eReferencia(participante.getFormacaoAcademica())) {
            row.createCell(20).setCellValue(participante.getFormacaoAcademica().getDescricao());
        }

        if (AjudanteObjeto.eReferencia(participante.getEscolaridade())) {
            row.createCell(21).setCellValue(participante.getEscolaridade().getDescricao());
        }
        row.createCell(22).setCellValue(participante.getTipo().getDescricao());

        if (AjudanteObjeto.eReferencia(this.evento) && AjudanteObjeto.eReferencia(this.evento.getId())) {

            row.createCell(23).setCellValue(new DecimalFormat("#,##0.00").format(notaNegocio.obterMediaGeralParticipanteEvento(participante, this.evento, modulos)));
            row.createCell(24).setCellValue(new DecimalFormat("#,##0.00").format(frequenciaNegocio.obterFrequenciaParticipanteEvento(participante, this.evento, modulos)));
        }
    }

    private HSSFSheet mesclarCelulas(HSSFSheet sheet, int linhaInicio, int linhaFim, int colunaInicio, int colunaFim) {

        sheet.addMergedRegion(new CellRangeAddress(
                linhaInicio,
                linhaFim,
                colunaInicio,
                colunaFim
        ));

        return sheet;

    }

    public String gerarRelatorioPDF() {

        this.tipoSaida = GeradorRelatorio.SAIDA_PDF;

        return gerarRelatorio(true);

    }

    public String gerarDeclaracaoParticipante(Evento evento, Inscricao inscricao) {

        this.relatorioSelecionado = RelatorioEnum.RELATORIO_DECLARACAO_PARTICIPANTE;
        this.evento = evento;
        this.inscricao = inscricao;
        this.tipoSaida = GeradorRelatorio.SAIDA_PDF;

        return gerarRelatorio(true);

    }

    public String gerarDeclaracaoInstrutorSemDownload(Evento evento, Instrutor instrutor) {

        this.relatorioSelecionado = RelatorioEnum.RELATORIO_CERTIFICADO_INSTRUTOR;
        this.evento = evento;
        this.instrutor = instrutor;
        this.tipoSaida = GeradorRelatorio.SAIDA_PDF;

        return gerarRelatorio(false);

    }

    public String gerarRelatorioExcel() {

        this.tipoSaida = GeradorRelatorio.SAIDA_XLS;

        return gerarRelatorio(true);
    }

    private String gerarRelatorio(Boolean isDownload) {

        SessionImpl session = null;

        try {

            session = (SessionImpl) this.sessionFactory.openSession();

            AjudanteParametrosRelatorio ajudanteParametro = defineParametrosPadrao();

            ajudanteParametro.adicionarParametroRelatorio("REPORT_CONNECTION", session.connection());

            this.defineParametrosRelatorio(ajudanteParametro);

            if (this.getRelatorioSelecionado().equals(RelatorioEnum.RELATORIO_CERTIFICADO_INSTRUTOR)) {

                try {
                    boolean isGerarNovo = !certificadoNegocio.isJaTemCertificadoInstrutor(getEvento(), getInstrutor());
                    Certificado certificado = this.certificadoNegocio.obterCertificado(getEvento(), getInstrutor() , isGerarNovo);

                    Arquivo arquivo = AjudanteDeArquivos.abrir(certificado.getArquivo().getArquivoGED());

                    this.download(arquivo.getStream(), TipoArquivo.PDF);

                } catch (Exception e) {

                    e.printStackTrace();
                }

                return "index";
            }

            ajudanteParametro.adicionarParametroRelatorio("LIST_QUESTIONARIO_AVALIACAO", this.definiFonteDadosRelatorio());

            FonteDadosRelatorioJavaBean fonteDados = new FonteDadosRelatorioJavaBean(null);

            byte[] relatorio = null;

            if (this.getRelatorioSelecionado().equals(RelatorioEnum.RELATORIO_LISTA_FREQUENCIA) && !AjudanteObjeto.eReferencia(this.getModulo().getId())) {

                relatorio = this.gerarRelatorioListaFrequencia(ajudanteParametro, fonteDados, this.tipoSaida);

            } else {

                relatorio = AjudanteRelatorio.gerarRelatorio(obterNomeRelatorio(), ajudanteParametro.getParametrosRelatorio(), fonteDados, this.tipoSaida, false);
            }

            BufferedInputStream bufferedInputStream = new BufferedInputStream(new ByteArrayInputStream(relatorio));

            if (isDownload) {
                this.download(bufferedInputStream, this.tipoSaida == GeradorRelatorio.SAIDA_XLS ? TipoArquivo.XLS : TipoArquivo.PDF);
            }

            /*
            if (this.getRelatorioSelecionado().equals(RelatorioEnum.RELATORIO_DECLARACAO_INSTRUTOR)) {
                salvarDeclaracaoInstrutor(ajudanteParametro.getParametrosRelatorio(), bufferedInputStream);
            }
            */

        } catch (ValidacaoException ex) {

            this.adicionarMensagemExcecao(ex);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            session.close();
        }

        return "index";

    }

    private void salvarDeclaracaoInstrutor(List<ParametroVisao> parametros, BufferedInputStream bufferedInputStream) throws IOException {

        Instrutor instrutor = null;
        Evento evento = null;

        for (ParametroVisao parametro : parametros) {

            if (parametro.getId().equals("ID_INSTRUTOR")) {
                instrutor = instrutorNegocio.obterPorAtributo("id", Long.parseLong(parametro.getValor().toString()));
            } else if (parametro.getId().equals("ID_EVENTO")) {
                evento = eventoDAO.obterPorAtributo("id", Long.parseLong(parametro.getValor().toString()));
            }
        }

        ModuloInstrutor moduloInstrutor = null;
        List<ModuloInstrutor> modulosInstrutores = new ArrayList<ModuloInstrutor>();

        if (AjudanteObjeto.eReferencia(evento)) {
            modulosInstrutores = moduloInstrutorDAO.listarInstrutoresEvento(evento);
        }

        for (ModuloInstrutor mi : modulosInstrutores) {

            if (mi.getInstrutor().equals(instrutor)) {
                moduloInstrutor = mi;
                break;
            }

        }

        if (AjudanteObjeto.eReferencia(moduloInstrutor)) {

            Anexo anexoASalvar = criarAnexo(instrutor.getPessoa().getNome(), bufferedInputStream);
            moduloInstrutor.setArquivo(anexoASalvar);
            moduloInstrutorDAO.salvarOuAtualizar(moduloInstrutor);

        }
    }

    private Anexo criarAnexo(String nome, BufferedInputStream bufferedInputStream) throws IOException {

        Anexo anexo = new Anexo();

        anexo.setDataCadastro(new Date());

        anexo.setDescricao("declaracao_instrutor_" + nome);

        anexo.setTipoArquivo(TipoArquivo.PDF);

        Arquivo arquivo = new Arquivo();

        arquivo.setStream(bufferedInputStream);

        arquivo.setNome(anexo.getDescricao());

        arquivo.setSufixo("." + anexo.getTipoArquivo().getExtensao());

        String idArquivo = this.ajudanteArquivos.salvar(arquivo);

        anexo.setArquivoGED(idArquivo);
        this.anexoNegocio.salvarSimples(anexo);

        return anexo;
    }

    private List<AvaliacaoReacaoDTO> definiFonteDadosRelatorio() {

        List<AvaliacaoReacaoDTO> avaliacaoResultList = new ArrayList<AvaliacaoReacaoDTO>();

        if (this.getRelatorioSelecionado().equals(RelatorioEnum.RELATORIO_CONSOLIDADO_EVENTO)) {

            List<Modulo> modulos = this.moduloNegocio.listaPorAtributo("evento", evento);

            for (Modulo modulo : modulos) {

                List<Avaliacao> avaliacaoList = this.avaliacaoNegocio.listaPorAtributo("modulo", modulo);

                for (Avaliacao avaliacao : avaliacaoList) {

                    List<QuestionarioAvaliacao> questionarioAvaliacaoList = this.questionarioAvaliacaoNegocio.listaPorAtributo("avaliacao", avaliacao);

                    for (QuestionarioAvaliacao questionarioAvaliacao : questionarioAvaliacaoList) {

                        List<Opcao> opcoes = this.opcaoNegocio.listaPorAtributo("questionario", questionarioAvaliacao.getQuestionario());

                        for (Opcao opcao : opcoes) {

                            avaliacaoResultList.add(new AvaliacaoReacaoDTO(evento.getTitulo(), questionarioAvaliacao.getQuestionario().getPergunta(), opcao.getDescricao(), 0));
                        }

                        if (AjudanteObjeto.eReferencia(questionarioAvaliacao.getInstrutor())) {

                            avaliacaoResultList.add(new AvaliacaoReacaoDTO(questionarioAvaliacao.getInstrutor().getPessoa().getNome(), questionarioAvaliacao.getQuestionario().getPergunta(), questionarioAvaliacao.getOpcao().getDescricao(), 1));

                        } else {

                            avaliacaoResultList.add(new AvaliacaoReacaoDTO(evento.getTitulo(), questionarioAvaliacao.getQuestionario().getPergunta(), questionarioAvaliacao.getOpcao().getDescricao(), 1));
                        }
                    }
                }
            }
        }

        return avaliacaoResultList;
    }

    private String obterNomeRelatorio() {

        return this.getPath() + "/template-" + this.obterOrientacao() + ".jasper";
    }

    private String getPath() {

        return context.getRealPath("/WEB-INF/relatorios/");
    }

    private byte[] gerarRelatorioListaFrequencia(AjudanteParametrosRelatorio ajudanteParametro, FonteDadosRelatorioJavaBean fonteDados, int tipoSaida) throws IOException, JRException, FileNotFoundException {

        byte[] relatorio;

        JRAbstractExporter exp;

        List<Object> list = new ArrayList<Object>();

        List<Modulo> modulos = this.moduloNegocio.listaPorAtributo("evento", this.getEvento());

        ByteArrayOutputStream output = new ByteArrayOutputStream();

        for (Modulo modulo : modulos) {

            ajudanteParametro.adicionarParametroRelatorio("ID_MODULO", modulo.getId());

            ajudanteParametro.adicionarParametroRelatorio("TITULO_MODULO", modulo.getTitulo());

            list.add(AjudanteRelatorio.obterRelatorio(obterNomeRelatorio(), ajudanteParametro.getParametrosRelatorio(), fonteDados, tipoSaida));
        }

        if (GeradorRelatorio.SAIDA_PDF == tipoSaida) {

            exp = new JRPdfExporter();

            exp.setParameter(JRPdfExporterParameter.JASPER_PRINT_LIST, list);

            exp.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, output);

            exp.exportReport();

        } else if (GeradorRelatorio.SAIDA_XLS == tipoSaida) {

            exp = new JRXlsExporter();

            exp.setParameter(JRXlsExporterParameter.JASPER_PRINT_LIST, list);

            exp.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, output);

            exp.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);

            exp.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);

            exp.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
        }

        relatorio = output.toByteArray();

        return relatorio;
    }

    private void defineParametrosRelatorio(AjudanteParametrosRelatorio ajudanteParametro) {

        for (CampoRelatorioEnum campoRelatorio : this.getRelatorioSelecionado().getListaCamposRelatorio()) {

            if (campoRelatorio.equals(CampoRelatorioEnum.EVENTO) && AjudanteObjeto.eReferencia(this.getEvento().getId())) {

                ajudanteParametro.adicionarParametroRelatorio("ID_EVENTO", evento.getId());

                ajudanteParametro.adicionarParametroRelatorio("TIPO_EVENTO", evento.getTipoEvento().getDescricao());

                ajudanteParametro.adicionarParametroRelatorio("TITULO_EVENTO", evento.getTitulo());


            } else if (campoRelatorio.equals(CampoRelatorioEnum.INSTRUTOR) && AjudanteObjeto.eReferencia(this.getInstrutor().getId())) {

                ajudanteParametro.adicionarParametroRelatorio("ID_INSTRUTOR", instrutor.getId());

                ajudanteParametro.adicionarParametroRelatorio("NOME_INSTRUTOR", instrutor.getNome());

                if (evento.getModuloUnico() == false) {

                    List<ModuloInstrutor> listaModuloInstrutor = moduloInstrutorDAO.listarInstrutoresEvento(evento);
                    List<String> listaInstrutorModulo = new ArrayList<String>();

                    for (int i = 0; i < listaModuloInstrutor.size(); i++) {

                        if (instrutor.getNome().equals(listaModuloInstrutor.get(i).getInstrutor().getNome())) {
                            listaInstrutorModulo.add(listaModuloInstrutor.get(i).getModulo().getTitulo());
                            cargaHorarioModulo = cargaHorarioModulo.add(listaModuloInstrutor.get(i).getModulo().getCargaHoraria());

                            if (dataModuloInicio == null) {
                                dataModuloInicio = listaModuloInstrutor.get(i).getModulo().getDataInicio();
                                dataModuloFim = listaModuloInstrutor.get(i).getModulo().getDataFim();
                            } else {
                                dataModuloFim = listaModuloInstrutor.get(i).getModulo().getDataFim();
                            }

                        }

                    }

                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    String stringDataInicio = dateFormat.format(dataModuloInicio);
                    String stringDataFim = dateFormat.format(dataModuloFim);


                    if (listaModuloInstrutor.size() != listaInstrutorModulo.size()) {


                        String instrutorModuloTitulo = StringUtils.join(listaInstrutorModulo, ", ");

                        ajudanteParametro.adicionarParametroRelatorio("LISTA_INSTRUTOR_MODULO", instrutorModuloTitulo);
                        ajudanteParametro.adicionarParametroRelatorio("CARGA_HORARIA_MODULO", cargaHorarioModulo.toString());
                        ajudanteParametro.adicionarParametroRelatorio("DATA_MODULO_INICIO", stringDataInicio);
                        ajudanteParametro.adicionarParametroRelatorio("DATA_MODULO_FIM", stringDataFim);


                        cargaHorarioModulo = new BigDecimal("0.0");
                        dataModuloInicio = null;
                        dataModuloFim = null;

                    } else {

                        cargaHorarioModulo = new BigDecimal("0.0");
                        dataModuloInicio = null;
                        dataModuloFim = null;

                    }

                }

            } else if (campoRelatorio.equals(CampoRelatorioEnum.PARTICIPANTE) && AjudanteObjeto.eReferencia(this.getInscricao().getId())) {

                ajudanteParametro.adicionarParametroRelatorio("ID_PARTICIPANTE", inscricao.getParticipante().getId());

                ajudanteParametro.adicionarParametroRelatorio("NOME_PARTICIPANTE", inscricao.getParticipante().getPessoa().getNome());

            } else if (campoRelatorio.equals(CampoRelatorioEnum.MODULO) && AjudanteObjeto.eReferencia(this.getModulo().getId())) {

                ajudanteParametro.adicionarParametroRelatorio("ID_MODULO", modulo.getId());

                ajudanteParametro.adicionarParametroRelatorio("TITULO_MODULO", modulo.getTitulo());

            } else if (campoRelatorio.equals(CampoRelatorioEnum.TIPO_PARTICIPANTE) && AjudanteObjeto.eReferencia(tipoParticipante)) {

                ajudanteParametro.adicionarParametroRelatorio("ID_TIPO_PARTICIPANTE", tipoParticipante.getId());

                ajudanteParametro.adicionarParametroRelatorio("DESC_TIPO_PARTICIPANTE", tipoParticipante.getDescricao());

            } else if (campoRelatorio.equals(CampoRelatorioEnum.DATA_INICIO) && AjudanteObjeto.eReferencia(dataInicio)) {

                ajudanteParametro.adicionarParametroRelatorio("DATA_INICIO", dataInicio);

            } else if (campoRelatorio.equals(CampoRelatorioEnum.DATA_FIM) && AjudanteObjeto.eReferencia(dataFim)) {

                ajudanteParametro.adicionarParametroRelatorio("DATA_FIM", dataFim);

            } else if (campoRelatorio.equals(CampoRelatorioEnum.INDICADA) && AjudanteObjeto.eReferencia(indicada)) {

                ajudanteParametro.adicionarParametroRelatorio("INDICADA", indicada.getValor() ? 1L : 0L);

            } else if (campoRelatorio.equals(CampoRelatorioEnum.AUTORIZADA) && AjudanteObjeto.eReferencia(autorizada)) {


                if (autorizada.getValor() != null) {
                    ajudanteParametro.adicionarParametroRelatorio("AUTORIZADA", autorizada.getValor() ? 1L : 0L);
                } else {
                    ajudanteParametro.adicionarParametroRelatorio("AUTORIZADA", 3L);
                }

            } else if (campoRelatorio.equals(CampoRelatorioEnum.TIPO_EVENTO) && AjudanteObjeto.eReferencia(tipoEvento)) {

                ajudanteParametro.adicionarParametroRelatorio("TIPO_EVENTO", tipoEvento.getDescricao());

                ajudanteParametro.adicionarParametroRelatorio("ID_TIPO_EVENTO", tipoEvento.getId());

            } else if (campoRelatorio.equals(CampoRelatorioEnum.PUBLICO_ALVO) && AjudanteObjeto.eReferencia(publicoAlvo)) {

                ajudanteParametro.adicionarParametroRelatorio("ID_PUBLICO_ALVO", publicoAlvo.getId());

                ajudanteParametro.adicionarParametroRelatorio("PUBLICO_ALVO", publicoAlvo.getDescricao());

            } else if (campoRelatorio.equals(CampoRelatorioEnum.EIXO_TEMATICO) && AjudanteObjeto.eReferencia(eixoTematico)) {

                ajudanteParametro.adicionarParametroRelatorio("EIXO_TEMATICO", eixoTematico.getDescricao());

                ajudanteParametro.adicionarParametroRelatorio("ID_EIXO_TEMATICO", eixoTematico.getId());

            } else if (campoRelatorio.equals(CampoRelatorioEnum.MODALIDADE) && AjudanteObjeto.eReferencia(modalidade)) {

                ajudanteParametro.adicionarParametroRelatorio("MODALIDADE", modalidade.getDescricao());

                ajudanteParametro.adicionarParametroRelatorio("ID_MODALIDADE", modalidade.getId());

            } else if (campoRelatorio.equals(CampoRelatorioEnum.LOCALIZACAO) && AjudanteObjeto.eReferencia(this.getLocalizacaoEvento().getId())) {

                ajudanteParametro.adicionarParametroRelatorio("LOCALIZACAO", localizacaoEvento.getDescricao());

                ajudanteParametro.adicionarParametroRelatorio("ID_LOCALIZACAO", localizacaoEvento.getId());

            } else if (campoRelatorio.equals(CampoRelatorioEnum.PROVEDOR) && AjudanteObjeto.eReferencia(this.getProvedorEvento().getId())) {

                ajudanteParametro.adicionarParametroRelatorio("PROVEDOR", provedorEvento.getDescricao());

                ajudanteParametro.adicionarParametroRelatorio("ID_PROVEDOR", provedorEvento.getId());

            } else if (campoRelatorio.equals(CampoRelatorioEnum.CIDADE) && AjudanteObjeto.eReferencia(cidade)) {

                ajudanteParametro.adicionarParametroRelatorio("CIDADE", cidade.getDescricao());

                ajudanteParametro.adicionarParametroRelatorio("ID_CIDADE", cidade.getId());

            } else if (campoRelatorio.equals(CampoRelatorioEnum.CARGO) && AjudanteObjeto.eReferencia(cargo)) {

                ajudanteParametro.adicionarParametroRelatorio("CARGO", cargo.getDescricao());

                ajudanteParametro.adicionarParametroRelatorio("ID_CARGO", cargo.getId());

            } else if (campoRelatorio.equals(CampoRelatorioEnum.PARTICIPANTES_PRESENTES) && AjudanteObjeto.eReferencia(participantesPresentes)) {

                ajudanteParametro.adicionarParametroRelatorio("FILTRAR_FREQUENCIA", participantesPresentes.getValor() ? 1L : 0L);

            } else if (campoRelatorio.equals(CampoRelatorioEnum.APROVADOS) && AjudanteObjeto.eReferencia(aprovados)) {

                ajudanteParametro.adicionarParametroRelatorio("APROVADOS", aprovados.getValor() ? 1L : 0L);

            } else if (campoRelatorio.equals(CampoRelatorioEnum.INICIAL_NOME_PARTICIPANTE) && AjudanteObjeto.eReferencia(selectedLetras) && selectedLetras.size() > 0 && !selectedLetras.get(0).equals("Todas")) {

                String inicialNome = StringUtils.join(selectedLetras, ",");

                ajudanteParametro.adicionarParametroRelatorio("INICIAL_NOME_PARTICIPANTE", inicialNome);

            } else if (campoRelatorio.equals(CampoRelatorioEnum.PNE) && AjudanteObjeto.eReferencia(pne)) {

                ajudanteParametro.adicionarParametroRelatorio("PNE", pne.getValor() ? 1L : 0L);

            } else if (campoRelatorio.equals(CampoRelatorioEnum.EVENTOS) && AjudanteObjeto.eReferencia(this.eventoList) && this.eventoList.size() > 0) {

                List<Long> listId = new ArrayList<Long>();

                for (Evento evento : this.eventoList) {

                    listId.add(evento.getId());
                }

                ajudanteParametro.adicionarParametroRelatorio("EVENTOS", listId.toString().replace("[", "").replace("]", ""));

            } else if (campoRelatorio.equals(CampoRelatorioEnum.IMPORTADO) && AjudanteObjeto.eReferencia(importado)) {

                ajudanteParametro.adicionarParametroRelatorio("IMPORTADO", importado.getValor() ? 1L : 0L);
            }

        }
    }

    private AjudanteParametrosRelatorio defineParametrosPadrao() {

        Dominio imgTopo = null, topoRelatorio = null, assinatura1 = null, cargoAssinatura1 = null;

        try {

            imgTopo = this.dominioNegocio.obterDominio(TipoDominioEnum.Configuracao, ConfiguracaoEnum.IMG_TOPO_RELATORIO.getCodigo());

            topoRelatorio = this.dominioNegocio.obterDominio(TipoDominioEnum.Configuracao, ConfiguracaoEnum.TOPO_RELATORIO.getCodigo());

            assinatura1 = this.dominioNegocio.obterDominio(TipoDominioEnum.Configuracao, ConfiguracaoEnum.ASSINATURA_1_RELATORIO.getCodigo());

            cargoAssinatura1 = this.dominioNegocio.obterDominio(TipoDominioEnum.Configuracao, ConfiguracaoEnum.CARGO_ASSINATURA_1_RELATORIO.getCodigo());

        } catch (Exception ex) {

            throw new ValidacaoException("Está faltando algum dado institucional. Vá ate a aba de configurações > Dados Institucionais e preencha os dados.");

        }

        AjudanteParametrosRelatorio ajudanteParametro = new AjudanteParametrosRelatorio();

        try {

            ajudanteParametro.adicionarParametroRelatorio("IMG_TOPO", AjudanteDeArquivos.obterCaminhoArquivoPorId(imgTopo.getDescricao()));

        } catch (Exception e) {

            System.out.println(e.getMessage());
        }

        ajudanteParametro.adicionarParametroRelatorio("TOPO", topoRelatorio.getDescricao());

        ajudanteParametro.adicionarParametroRelatorio("TITULO_RELATORIO", this.getRelatorioSelecionado().getDescricao());

        ajudanteParametro.adicionarParametroRelatorio("SUBREPORT_DIR", this.getPath() + "/");

        ajudanteParametro.adicionarParametroRelatorio("RELATORIO", this.getRelatorioSelecionado().getRelatorio());

        ajudanteParametro.adicionarParametroRelatorio("ASSINATURA_1", assinatura1.getDescricao());

        ajudanteParametro.adicionarParametroRelatorio("CARGO_ASSINATURA_1", cargoAssinatura1.getDescricao());

        return ajudanteParametro;
    }

    private String obterOrientacao() {

        return this.getRelatorioSelecionado().getOrientacao().equals(OrientationEnum.PORTRAIT) ? "retrato" : "paisagem";
    }

    public Boolean verificaVisibilidade(CampoRelatorioEnum campoRelatorioEnum) {

        for (CampoRelatorioEnum campo : this.getRelatorioSelecionado().getListaCamposRelatorio()) {

            if (campo.equals(campoRelatorioEnum)) {

                return Boolean.TRUE;
            }
        }

        return Boolean.FALSE;
    }

    public Boolean verificaObrigatoriedade(CampoRelatorioEnum campoRelatorioEnum) {

        for (CampoRelatorioEnum campo : this.getRelatorioSelecionado().getListaCamposObrigatoriosRelatorio()) {

            if (campo.equals(campoRelatorioEnum)) {

                return Boolean.TRUE;
            }
        }

        return Boolean.FALSE;
    }

    public Map<String, Object> getFiltro() {

        filtro = new HashMap<String, Object>();

        if (AjudanteObjeto.eReferencia(this.getEvento()) && AjudanteObjeto.eReferencia(this.getEvento().getId())) {

            filtro.put("evento", this.getEvento());

        }

        return filtro;
    }

    public List<Instrutor> getInstrutorList() {

        if (AjudanteObjeto.eReferencia(this.getEvento()) && AjudanteObjeto.eReferencia(this.getEvento().getId())) {

            instrutorList = instrutorNegocio.listarInstrutoresEvento(this.getEvento());

        } else {

            instrutorList = instrutorNegocio.listar();
        }

        return instrutorList;
    }

    public void setInstrutorList(List<Instrutor> instrutorList) {
        this.instrutorList = instrutorList;
    }

    public void setFiltro(Map<String, Object> filtro) {
        this.filtro = filtro;
    }

    public Evento getEvento() {
        if (evento == null) {
            evento = new Evento();
        }
        return evento;
    }

    public void setEvento(Evento evento) {
        if (AjudanteObjeto.eReferencia(evento) && AjudanteObjeto.eReferencia(evento.getId())) {
            this.evento = evento;
            this.modulo = null;
        }
    }

    public Instrutor getInstrutor() {
        if (instrutor == null) {
            instrutor = new Instrutor();
        }
        return instrutor;
    }

    public void setInstrutor(Instrutor instrutor) {
        this.instrutor = instrutor;
    }

    public Inscricao getInscricao() {
        if (inscricao == null) {
            inscricao = new Inscricao();
            inscricao.setParticipante(new Participante());
            inscricao.getParticipante().setPessoa(new Pessoa());
        }
        return inscricao;
    }

    public void setInscricao(Inscricao inscricao) {
        if (inscricao != null) {
            this.inscricao = inscricao;
        }
    }


    public Participante getParticipante() {
        if (participante == null) {
            participante = new Participante();
            participante.setPessoa(new Pessoa());
        }
        return participante;
    }

    public void setParticipante(Participante participante) {
        if (participante != null) {
            this.participante = participante;
        }
    }

    public List<RelatorioEnum> getRelatorioList() {
        return relatorioList;
    }

    public void setRelatorioList(List<RelatorioEnum> relatorioList) {
        this.relatorioList = relatorioList;
    }

    public RelatorioEnum getRelatorioSelecionado() {
        return relatorioSelecionado;
    }

    public void setRelatorioSelecionado(RelatorioEnum relatorioSelecionado) {
        this.relatorioSelecionado = relatorioSelecionado;
    }

    public Modulo getModulo() {
        if (modulo == null) {
            modulo = new Modulo();
        }
        return modulo;
    }

    public void setModulo(Modulo modulo) {
        this.modulo = modulo;
    }

    public Dominio getTipoParticipante() {
        return tipoParticipante;
    }

    public void setTipoParticipante(Dominio tipoParticipante) {
        this.tipoParticipante = tipoParticipante;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public List<Dominio> getTipoParticipanteList() {
        return tipoParticipanteList;
    }

    public void setTipoParticipanteList(List<Dominio> tipoParticipanteList) {
        this.tipoParticipanteList = tipoParticipanteList;
    }

    public BooleanEnum getIndicada() {
        return indicada;
    }

    public void setIndicada(BooleanEnum indicada) {
        this.indicada = indicada;
    }

    public AutorizaEnum getAutorizada() {
        return autorizada;
    }

    public void setAutorizada(AutorizaEnum autorizada) {
        this.autorizada = autorizada;
    }

    public List<AutorizaEnum> getAutorizaItemList() {
        if (!AjudanteObjeto.eReferencia(autorizaItemList)) {
            autorizaItemList = Arrays.asList(AutorizaEnum.values());
        }
        return autorizaItemList;
    }

    public void setAutorizaItemList(List<AutorizaEnum> autorizaItemList) {
        this.autorizaItemList = autorizaItemList;
    }

    public List<BooleanEnum> getBooleanItemList() {
        if (!AjudanteObjeto.eReferencia(booleanItemList)) {
            booleanItemList = Arrays.asList(BooleanEnum.values());
        }
        return booleanItemList;
    }

    public void setBooleanItemList(List<BooleanEnum> booleanItemList) {
        this.booleanItemList = booleanItemList;
    }

    public Dominio getTipoEvento() {
        return tipoEvento;
    }

    public void setTipoEvento(Dominio tipoEvento) {
        this.tipoEvento = tipoEvento;
    }

    public Dominio getPublicoAlvo() {
        return publicoAlvo;
    }

    public void setPublicoAlvo(Dominio publicoAlvo) {
        this.publicoAlvo = publicoAlvo;
    }

    public Dominio getEixoTematico() {
        return eixoTematico;
    }

    public void setEixoTematico(Dominio eixoTematico) {
        this.eixoTematico = eixoTematico;
    }

    public LocalizacaoEvento getLocalizacaoEvento() {
        if (localizacaoEvento == null) {
            localizacaoEvento = new LocalizacaoEvento();
        }
        return localizacaoEvento;
    }

    public void setLocalizacaoEvento(LocalizacaoEvento localizacaoEvento) {
        this.localizacaoEvento = localizacaoEvento;
    }

    public ProvedorEvento getProvedorEvento() {
        if (provedorEvento == null) {
            provedorEvento = new ProvedorEvento();
        }
        return provedorEvento;
    }

    public void setProvedorEvento(ProvedorEvento provedorEvento) {
        this.provedorEvento = provedorEvento;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public Dominio getCargo() {
        return cargo;
    }

    public void setCargo(Dominio cargo) {
        this.cargo = cargo;
    }

    public Dominio getModalidade() {
        return modalidade;
    }

    public void setModalidade(Dominio modalidade) {
        this.modalidade = modalidade;
    }

    public List<Dominio> getTipoEventoList() {
        return tipoEventoList;
    }

    public void setTipoEventoList(List<Dominio> tipoEventoList) {
        this.tipoEventoList = tipoEventoList;
    }

    public List<Dominio> getEixoTematicoList() {
        return eixoTematicoList;
    }

    public void setEixoTematicoList(List<Dominio> eixoTematicoList) {
        this.eixoTematicoList = eixoTematicoList;
    }

    public List<Dominio> getModalidadeList() {
        return modalidadeList;
    }

    public void setModalidadeList(List<Dominio> modalidadeList) {
        this.modalidadeList = modalidadeList;
    }

    public List<Estado> getEstadoList() {
        return estadoList;
    }

    public void setEstadoList(List<Estado> estadoList) {
        this.estadoList = estadoList;
    }

    public List<Cidade> getCidadeList() {
        return cidadeList;
    }

    public void setCidadeList(List<Cidade> cidadeList) {
        this.cidadeList = cidadeList;
    }

    public List<Dominio> getCargoList() {
        return cargoList;
    }

    public void setCargoList(List<Dominio> cargoList) {
        this.cargoList = cargoList;
    }

    public int getTipoSaida() {
        return tipoSaida;
    }

    public void setTipoSaida(int tipoSaida) {
        this.tipoSaida = tipoSaida;
    }

    public List<Evento> getEventoList() {
        return eventoList;
    }

    public void setEventoList(List<Evento> eventoList) {
        this.eventoList = eventoList;
    }

    public BooleanEnum getParticipantesPresentes() {
        return participantesPresentes;
    }

    public void setParticipantesPresentes(BooleanEnum participantesPresentes) {
        this.participantesPresentes = participantesPresentes;
    }

    public BooleanEnum getAprovados() {
        return aprovados;
    }

    public void setAprovados(BooleanEnum aprovados) {
        this.aprovados = aprovados;
    }

    public BooleanEnum getPne() {
        return pne;
    }

    public void setPne(BooleanEnum pne) {
        this.pne = pne;
    }

    public BooleanEnum getImportado() {
        return importado;
    }

    public void setImportado(BooleanEnum importado) {
        this.importado = importado;
    }

}