/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade.dto;

import br.gov.go.tcm.sophos.entidade.enumeracao.ComparadorCriteriaEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoLogicoEnum;

import java.io.Serializable;

public class FiltroDTO implements Serializable{

	private static final long serialVersionUID = -3277129201905472150L;

	private String atributo;

	private Object valor;

	private ComparadorCriteriaEnum comparador;

	private TipoLogicoEnum tipoLogicoEnum;

	public FiltroDTO(String atributo, Object valor, ComparadorCriteriaEnum comparador, TipoLogicoEnum tipoLogicoEnum) {
		super();
		this.atributo = atributo;
		this.valor = valor;
		this.comparador = comparador;
		this.tipoLogicoEnum = tipoLogicoEnum;
	}

	public FiltroDTO(String atributo, Object valor, ComparadorCriteriaEnum comparador) {
		super();
		this.atributo = atributo;
		this.valor = valor;
		this.comparador = comparador;
	}

	public TipoLogicoEnum getTipoLogicoEnum() {
		return tipoLogicoEnum;
	}

	public void setTipoLogicoEnum(TipoLogicoEnum tipoLogicoEnum) {
		this.tipoLogicoEnum = tipoLogicoEnum;
	}

	public String getAtributo() {
		return atributo;
	}

	public void setAtributo(String atributo) {
		this.atributo = atributo;
	}

	public Object getValor() {
		return valor;
	}

	public void setValor(Object valor) {
		this.valor = valor;
	}

	public ComparadorCriteriaEnum getComparador() {
		return comparador;
	}

	public void setComparador(ComparadorCriteriaEnum comparador) {
		this.comparador = comparador;
	}

}
