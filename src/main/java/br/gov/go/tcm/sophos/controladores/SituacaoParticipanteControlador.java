/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 *
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 *
 * Este cabeçalho deve ser mantido.
 *
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.estrutura.entidade.transiente.Arquivo;
import br.gov.go.tcm.estrutura.negocio.ajudante.AjudanteDeArquivos;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.Certificado;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Inscricao;
import br.gov.go.tcm.sophos.entidade.Participante;
import br.gov.go.tcm.sophos.entidade.dto.MeuEventoDTO;
import br.gov.go.tcm.sophos.entidade.dto.SituacaoParticipanteDTO;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoArquivo;
import br.gov.go.tcm.sophos.lazy.base.TableLazyPadrao;
import br.gov.go.tcm.sophos.negocio.CertificadoNegocio;
import br.gov.go.tcm.sophos.negocio.EventoNegocio;
import br.gov.go.tcm.sophos.negocio.InscricaoNegocio;
import br.gov.go.tcm.sophos.negocio.ParticipanteNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
@Scope("view")
public class SituacaoParticipanteControlador extends ControladorBaseCRUD<Participante, ParticipanteNegocio> {

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private ParticipanteNegocio negocio;

	@Autowired
	private InscricaoNegocio inscricaoNegocio;

	@Autowired
	private EventoNegocio eventoNegocio;

	@Autowired
	private CertificadoNegocio certificadoNegocio;

	private Evento eventoSelecionado = new Evento();

	private String nomeParticipante = "";

	private List<SituacaoParticipanteDTO> situacaoList;

	public void atualizarLista() {

		if (AjudanteObjeto.eReferencia(this.eventoSelecionado) && AjudanteObjeto.eReferencia(this.eventoSelecionado.getId())) {

			List<Inscricao> inscricaoList = inscricaoNegocio.listaPorAtributo("evento", this.eventoSelecionado);

			situacaoList = new ArrayList<SituacaoParticipanteDTO>();

			for (Inscricao inscricao : inscricaoList) {

				MeuEventoDTO eventoInscricaoDTO = this.eventoNegocio.montarListaMeusEventos(inscricao,
						inscricao.getParticipante());

				SituacaoParticipanteDTO situacaoParticipanteDTO = new SituacaoParticipanteDTO(
						eventoInscricaoDTO.getAprovado(), eventoInscricaoDTO.getAvaliado(), eventoInscricaoDTO.getNota(),
						inscricao, eventoInscricaoDTO);

				if(situacaoParticipanteDTO.getInscricao().getParticipante().getPessoa().getNome().toUpperCase().contains(this.getNomeParticipante().toUpperCase())){

					situacaoList.add(situacaoParticipanteDTO);
				}

			}

		} else {
			this.adicionarMensagemDeAlerta("Por favor, selecione um evento.");
		}
	}

	public boolean isJaTemCertificado(Evento evento, Participante participante) {

		try {
			return this.certificadoNegocio.isJaTemCertificadoParticipante(evento, participante);
		} catch (IOException e) {
			e.printStackTrace();
			return false;

		}

	}


	public void imprimirCertificado(MeuEventoDTO meuEvento, Participante participante, Boolean isGerarNovo){

		try {

			Certificado certificado = this.certificadoNegocio.obterCertificado(meuEvento.getEvento(), participante, isGerarNovo);

			Arquivo arquivo = AjudanteDeArquivos.abrir(certificado.getArquivo().getArquivoGED());

			this.download(arquivo.getStream(), TipoArquivo.PDF);

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	@Override
	public TableLazyPadrao<Participante> getLista() {
		return null;
	}

	@Override
	protected ParticipanteNegocio getNegocio() {
		return negocio;
	}

	public List<SituacaoParticipanteDTO> getSituacaoList() {
		return situacaoList;
	}

	public void setSituacaoList(List<SituacaoParticipanteDTO> situacaoList) {
		this.situacaoList = situacaoList;
	}

	public Evento getEventoSelecionado() {
		return eventoSelecionado;
	}

	public void setEventoSelecionado(Evento eventoSelecionado) {
		if (AjudanteObjeto.eReferencia(eventoSelecionado) && AjudanteObjeto.eReferencia(eventoSelecionado.getId())) {
			this.eventoSelecionado = eventoSelecionado;
		}
	}

	public String getNomeParticipante() {
		return nomeParticipante;
	}

	public void setNomeParticipante(String nomeParticipante) {
		this.nomeParticipante = nomeParticipante;
	}

}