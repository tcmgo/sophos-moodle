/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.persistencia.impl;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteData;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.Dominio;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.persistencia.EventoDAO;
import br.gov.go.tcm.sophos.persistencia.base.DAO;
import br.gov.go.tcm.sophos.persistencia.impl.base.DAOGenerico;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class EventoDAOImpl extends DAOGenerico<Evento> implements EventoDAO {

	private static final long serialVersionUID = 294560128456827146L;

	@Autowired
	public EventoDAOImpl(@Qualifier(DAO.SESSION_FACTORY_SOPHOS) final SessionFactory factory, @Qualifier(DAO.HIBERNATE_TEMPLATE_SOPHOS) final HibernateTemplate hibernateTemplate) {

		this.setHibernateTemplate(hibernateTemplate);

		this.setSessionFactory(factory);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Evento> listar(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters, String consulta) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		defineBuscaProFiltros(filters, criteria);
		
		Disjunction queryAll = definirBuscaPorConsulta(consulta, criteria);
		
		criteria.add(queryAll);
		
		criteria.addOrder(Order.desc("dataInicioPrevisto"));

		criteria.addOrder(Order.desc("dataInicioRealizacao"));
		
		criteria.addOrder(Order.asc("titulo"));
		
		criteria.setFirstResult(first);

		criteria.setMaxResults(pageSize);

		try {

			return criteria.list();

		} finally {

			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Evento> listarEventosParaVisualizacao(Dominio tipo) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		criteria.add(Restrictions.eq("mostrarNaHome", Boolean.TRUE));
		
		if(AjudanteObjeto.eReferencia(tipo)){

			criteria.createAlias("publicoAlvoList", "publicoAlvoList");
			
			criteria.add(Restrictions.eq("publicoAlvoList.publicoAlvo", tipo));

			criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		}
		
		criteria.addOrder(Order.desc("id"));

		try {

			return criteria.list();

		} finally {

			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Evento> listarEventoComCertificadosDisponiveis() {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		criteria.add(Restrictions.eq("permiteCertificado", Boolean.TRUE));
		
		criteria.add(Restrictions.eq("mostrarNaHome", Boolean.TRUE));
		
		criteria.addOrder(Order.desc("id"));

		try {

			return criteria.list();

		} finally {

			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Evento> listarEventoComInscricoesAbertas() {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);

		Criteria criteria = session.createCriteria(this.getTipoEntidade());

		criteria.add(Restrictions.eq("mostrarNaHome", Boolean.TRUE));

		Date dataAtual = AjudanteData.zerarHoraData(new Date());

		Conjunction andPreInscricao = Restrictions.conjunction();

		andPreInscricao.add(Restrictions.eq("permitePreInscricao", Boolean.TRUE));
		andPreInscricao.add(Restrictions.ge("dataFimPreInscricao", dataAtual));
		andPreInscricao.add(Restrictions.le("dataInicioPreInscricao", dataAtual));

		Conjunction andRealizacao = Restrictions.conjunction();

		andRealizacao.add(Restrictions.ge("dataFimRealizacao", dataAtual));
		andRealizacao.add(Restrictions.le("dataInicioRealizacao", dataAtual));

		Disjunction orPreinscricaoRealizacao = Restrictions.disjunction();

		orPreinscricaoRealizacao.add(andPreInscricao);
		orPreinscricaoRealizacao.add(andRealizacao);

		criteria.add(orPreinscricaoRealizacao);

		criteria.addOrder(Order.desc("id"));

		try {

			return criteria.list();

		} finally {

			session.close();

		/*final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);

		Criteria criteria = session.createCriteria(this.getTipoEntidade());

		criteria.add(Restrictions.eq("mostrarNaHome", Boolean.TRUE));

		Date dataAtual = AjudanteData.zerarHoraData(new Date());

		Disjunction or = Restrictions.disjunction();

		Conjunction andPreInscricao = Restrictions.conjunction();

		andPreInscricao.add(Restrictions.eq("permitePreInscricao", Boolean.TRUE));
		andPreInscricao.add(Restrictions.ge("dataFimPreInscricao", dataAtual));
		andPreInscricao.add(Restrictions.le("dataInicioPreInscricao", dataAtual));

		Conjunction andPrevisao = Restrictions.conjunction();
		andPrevisao.add(Restrictions.le("dataInicioPrevisto", dataAtual));

		or.add(andPreInscricao);
		or.add(andPrevisao);

		criteria.add(or);

		criteria.addOrder(Order.desc("id"));

		try {

			return criteria.list();

		} finally {

			session.close();*/
		}
	}
}
