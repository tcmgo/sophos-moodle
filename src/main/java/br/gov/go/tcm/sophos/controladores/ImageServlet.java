/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.estrutura.container.spring.ContextoSpring;
import br.gov.go.tcm.estrutura.entidade.transiente.Arquivo;
import br.gov.go.tcm.estrutura.negocio.ajudante.AjudanteDeArquivos;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.negocio.EventoNegocio;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

@WebServlet("/images/*")
@Component
public class ImageServlet extends HttpServlet {
	
    private static final long serialVersionUID = -4635239628819022096L;
    
	public static final Logger log = Logger.getLogger(ImageServlet.class);
 
	private EventoNegocio negocio;
	
	private String caminhoGed;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		
	   super.init(config);

	   ApplicationContext ac = (ApplicationContext) config.getServletContext().getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
	   
	   this.caminhoGed = (String) ac.getBean(ContextoSpring.DIRETORIO_GED);
	   
	   this.negocio = (EventoNegocio) ac.getBean("eventoNegocio");
	   
	}
	
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	
        String s = request.getPathInfo().substring(1);
        
        Arquivo arquivo = null;
        
        try{
        	
        	Evento evento = this.negocio.obter(Long.parseLong(s));
        	  
        	System.out.println("ABRINDO ARQUIVO no GED: "+ caminhoGed +" | ARQUIVO: "+ evento.getImagem().getArquivoGED() +"."+ evento.getImagem().getTipoArquivo().getExtensao());
        	
        	arquivo = AjudanteDeArquivos.abrir(evento.getImagem().getArquivoGED() +"."+ evento.getImagem().getTipoArquivo().getExtensao(), caminhoGed);
        	
        	System.out.println("ACESSOU GED SEM ERROS");
        	
        } catch (Exception e) {

        	System.out.println("ERRO AO ABRIR ARQUIVO: " + e.getMessage());
        	
		}
        
    	response.setContentType("image/png"); 
    	
    	OutputStream out = response.getOutputStream();
    	
    	if(arquivo == null){
    		
    		System.out.println("NAO EXISTE O ARQUIVO: ARQUIVO == NULL");
    		
    		@SuppressWarnings("deprecation")
			FileInputStream imgPadrao = new FileInputStream(request.getRealPath("/") + "/resources/imagens/sophos_imagem_empty.jpg");
    		
    		arquivo = new Arquivo();
    		
    		arquivo.setStream(imgPadrao);
    	}
    	
    	System.out.println("\n");
    	
    	out.write(IOUtils.toByteArray(arquivo.getStream())); 
    	
    	out.close();
 
    }

}
