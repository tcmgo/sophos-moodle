/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "usuario")
@Banco(nome = Banco.SOPHOS)
public class Usuario extends EntidadePadrao {

	private static final long serialVersionUID = -8705737449122033471L;

	@ManyToOne(optional = false)
	@JoinColumn(name = "pessoa_id", referencedColumnName = "id")
	private Pessoa pessoa;

	@ManyToOne
	@JoinColumn(name = "participante_id", referencedColumnName = "id")
	private Participante participante;

	@ManyToOne(optional = false)
	@JoinColumn(name = "tipo_usuario_id", referencedColumnName = "id")
	private Dominio tipoUsuario;

	@Column(name = "usuario_id")
	private Long usuarioId;

	@Column(name = "senha")
	private String senha;

	@Column(name = "nome_usuario")
	private String nomeUsuario;

	@Column(name = "matricula")
	private String matricula;

	@Column(name = "CodSecao")
	private String secaoID;

	@OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL)
	private List<PerfilUsuario> perfilList;

	@Transient
	private String repitaSenha;
	
	@Column(name = "token_recup_senha")
	private String token;

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Participante getParticipante() {
		return participante;
	}

	public void setParticipante(Participante participante) {
		this.participante = participante;
	}

	public Dominio getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(Dominio tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getRepitaSenha() {
		return repitaSenha;
	}

	public void setRepitaSenha(String repitaSenha) {
		this.repitaSenha = repitaSenha;
	}

	public String getSecaoID() {
		return secaoID;
	}

	public void setSecaoID(String secaoID) {
		this.secaoID = secaoID;
	}

	public List<PerfilUsuario> getPerfilList() {
		if(perfilList == null){
			perfilList = new ArrayList<PerfilUsuario>();
		}
		return perfilList;
	}

	public void setPerfilList(List<PerfilUsuario> perfilList) {
		this.perfilList = perfilList;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
