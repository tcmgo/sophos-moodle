/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.Alerta;
import br.gov.go.tcm.sophos.entidade.Inscricao;
import br.gov.go.tcm.sophos.entidade.PacoteAlertas;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import br.gov.go.tcm.sophos.persistencia.PacoteAlertasDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PacoteAlertasNegocio extends NegocioBase<PacoteAlertas> {

	private static final long serialVersionUID = -8426728536010364058L;

	@Autowired
	private PacoteAlertasDAO dao;

	@Autowired
	private InscricaoNegocio inscricaoNegocio;

	@Autowired
	private AlertaNegocio alertaNegocio;
	
	@Override
	protected PacoteAlertasDAO getDAO() {

		return this.dao;
	}

	@Override
	public void salvar(PacoteAlertas entidade) {

		if (!AjudanteObjeto.eReferencia(entidade.getId())) {

			List<Inscricao> inscricaoList = this.inscricaoNegocio.listarInscricoesAutorizadas(entidade.getEvento());

			List<Alerta> alertasList = new ArrayList<Alerta>();

			for (Inscricao inscricao : inscricaoList) {

				Alerta alerta = new Alerta();

				alerta.setPacoteAlertas(entidade);

				alerta.setAtivo(Boolean.TRUE);

				alerta.setDataCadastro(new Date());

				alerta.setParticipante(inscricao.getParticipante());

				alertasList.add(alerta);
			}

			entidade.setAlertasList(alertasList);

		}

		super.salvar(entidade);

	}
	
	@Override
	public void excluir(PacoteAlertas entidade) {

		List<Alerta> alertasList = this.alertaNegocio.listaPorAtributo("pacoteAlertas", entidade);
		
		for (Alerta alerta : alertasList) {
			
			this.alertaNegocio.excluir(alerta);
		}
		
		super.excluir(entidade);
	}

}
