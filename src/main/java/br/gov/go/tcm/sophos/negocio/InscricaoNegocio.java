/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 *
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 *
 * Este cabeçalho deve ser mantido.
 *
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.estrutura.entidade.orcafi.Municipio;
import br.gov.go.tcm.estrutura.persistencia.orcafi.contrato.MunicipioDAO;
import br.gov.go.tcm.estrutura.relatorio.GeradorRelatorio;
import br.gov.go.tcm.estrutura.relatorio.fonteDeDados.AjudanteParametrosRelatorio;
import br.gov.go.tcm.estrutura.relatorio.fonteDeDados.FonteDadosVazia;
import br.gov.go.tcm.estrutura.util.spring.SpringUtils;
import br.gov.go.tcm.sophos.ajudante.AjudanteEmail;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.ajudante.AjudanteRelatorio;
import br.gov.go.tcm.sophos.entidade.*;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoDominioEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoPublicoAlvo;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import br.gov.go.tcm.sophos.persistencia.CidadeDAO;
import br.gov.go.tcm.sophos.persistencia.InscricaoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class InscricaoNegocio extends NegocioBase<Inscricao> {

	private static final long serialVersionUID = -8426728536010364058L;

	@Autowired
	private InscricaoDAO dao;

	@Autowired
	private EventoPublicoAlvoNegocio eventoPublicoAlvoNegocio;

	@Autowired
	private ServletContext contextoAplicacao;

	@Autowired
	private ModuloNegocio moduloNegocio;

	@Autowired
	private MunicipioDAO municipioDAO;
	
	@Autowired
	private CidadeDAO cidadeDAO;

	@Autowired
	private EventoNegocio eventoNegocio;

	@Override
	protected InscricaoDAO getDAO() {

		return this.dao;
	}

	public Boolean verificaAutorizacaoInscricao(Evento evento){

		if(AjudanteObjeto.eReferencia(evento.getPermitePreInscricao())){

			return evento.getPermitePreInscricao() ? null : Boolean.TRUE;
		}
		return Boolean.TRUE;
	}

	@Override
	public void executarValidacaoAntesDeSalvar(Inscricao entidade) {

		if (!entidade.getEvento().getEventoImportado()) {

				if (verificaOrgaoJurisdicionado(entidade.getParticipante(), entidade.getEvento())) {

					throw new ValidacaoException("Preecha o orgão em [MEU CADASTRO > INFORMAÇÕES FUNCIONAIS] para completar essa ação.");
				}

				if (existeInscricaoParaParticipante(entidade.getParticipante(), entidade.getEvento())) {

					throw new ValidacaoException("Já existe uma inscrição para este evento.");
				}

				if (!this.eventoNegocio.verificaPermissaoInscricao(entidade.getEvento())) {

					throw new ValidacaoException("Inscrição não concluída. Data em que foi realizada a inscrição não é elegível para o evento.");
				}

				/*if(AjudanteObjeto.eReferencia(entidade.getEvento().getPermitePreInscricao()) && entidade.getEvento().getPermitePreInscricao()) {

					if(!AjudanteData.dataEstaEntreOPeriodo(new Date(), entidade.getEvento().getDataInicioPreInscricao(), entidade.getEvento().getDataFimPreInscricao())){

						throw new ValidacaoException("Inscrição não é possível. Data atual fora do período de inscrição.");
					}

				} else if(AjudanteData.dataMenorQueReferencia(entidade.getEvento().getDataInicioPrevisto(), new Date())){

					throw new ValidacaoException("Período para inscrições está encerrado.");
				}*/

				if(entidade.getEvento().getVagas() <= this.dao.obterQuantidadeInscricoes(entidade.getEvento())){

					throw new ValidacaoException("Limite de vagas atingido.");
				}

				this.validarPublicoAlvo(entidade);

		}
	}

	private boolean verificaOrgaoJurisdicionado(Participante participante, Evento evento) {

		if(participante.getTipo().equals(TipoDominioEnum.TipoPublicoAlvo.name(), TipoPublicoAlvo.JURISDICIONADO.getCodigo())){

			return !AjudanteObjeto.eReferencia(participante.getCodigoOrgao());
		}

		return false;
	}

	private void validarPublicoAlvo(Inscricao entidade) {

		List<EventoPublicoAlvo> publicoAlvoList = this.eventoPublicoAlvoNegocio.listaPorAtributo("evento", entidade.getEvento());

		Boolean permitePublicoAlvo = Boolean.FALSE;

		for(EventoPublicoAlvo eventoPublicoAlvo : publicoAlvoList){

			if(eventoPublicoAlvo.getPublicoAlvo().equals(entidade.getParticipante().getTipo())){

				permitePublicoAlvo = Boolean.TRUE;
			}
		}

		if(!permitePublicoAlvo){

			throw new ValidacaoException("Publico alvo do evento não corresponde ao tipo do participante.");
		}
	}

	public void salvar(List<Inscricao> inscricaoList) {

		for (Inscricao inscricao : inscricaoList) {

			try {
				// pedido escola de contas--  salvar inscrições não tendo validação
				this.salvarSemValidacao(inscricao);

			} catch (Exception ex) {

				System.out.println(ex);
			}
		}
	}

	private void salvarSemValidacao(Inscricao entidade){

		if (entidade.getId() == null) {

			if (existeInscricaoParaParticipante(entidade.getParticipante(), entidade.getEvento())) {

				throw new ValidacaoException("Já existe uma inscrição para este evento.");
			}

			this.getDAO().salvar(entidade);

		} else {

			this.atualizar(entidade);
		}
	}

	public boolean existeInscricaoParaParticipante(Participante participante, Evento evento) {

		return dao.existeInscricaoParaParticipante(participante, evento);
	}

	public String gerarUrlQRCodeParaInscricao(Inscricao inscricao, String dimensaoPx) {

		final String dimensao = dimensaoPx;

		final String rootApiOnlineQrCode = "http://api.qrserver.com/v1/create-qr-code/?size=";

		/*
				Dados para PresencaQRCode: Id do evento + Id do participante + Titulo do evento + Nome da pessoa vinculada ao participante.
				Assim, cada pessoa cadastrada no Sophos terá um PresencaQRCode único por evento.
				Os dados são escritos no formato JSON, para facilitar o processamento no aplicativo de leitura do QR.
		 */
		String data = "{" +
				"\"eventoId\":" + inscricao.getEvento().getId() +
				",\"participanteId\":" + inscricao.getParticipante().getId() +
				",\"eventoNome\":\"" + inscricao.getEvento().getTitulo() + "\"" +
				",\"participanteNome\":\"" + inscricao.getParticipante().getPessoa().getNome() + "\"" +
				"}";

		String URL = null;

		try {

			URL = rootApiOnlineQrCode + dimensao + "x" + dimensao + "&data=" + URLEncoder.encode(data, "UTF-8");

		} catch (UnsupportedEncodingException e) {

			e.printStackTrace();
		}
		return URL;
	}

	private String gerarCorpoEmailInscricaoAprovada(Inscricao inscricao) {

		final String contextoPublicacaoAplicacao = (String) SpringUtils.getApplicationContext().getBean("contextoPublicoDaAplicacao");

		String imgArteEvento = "";

		if(inscricao.getEvento().getImagem() != null) {

			String urlArteEvento = contextoPublicacaoAplicacao + "/images/" + inscricao.getEvento().getId();

			imgArteEvento = "<p><img src=\"" + urlArteEvento + "\" alt=\"evento-img\"/></p>";
		}

		String urlRodapeEscolaContas = contextoPublicacaoAplicacao + "/resources/imagens/rodape-escola-de-contas.png";

		String corpoEmail = "<html>" +

				"<body>" +

				"<p style=\"text-align: justify;\">Sr(a) " + inscricao.getNomeParticipante() + ", sua inscrição para o evento " + inscricao.getEvento().getTitulo() + " foi aprovada!</p>" +

				"<p style=\"text-align: justify; font-size: 18px;\"><strong>IMPORTANTE: Para registrar sua participação, informamos que será necessário apresentar o Cartão do Participante, anexo a este e-mail, no(s) dia(s) do evento.</strong></p>" +

				imgArteEvento +

				"<p style=\"text-align: justify;\">Atenciosamente," +

				"<br>Superintendência da Escola de Contas" +

				"<br>Tribunal de Contas dos Municípios do Estado de Goiás" +

				"<br>(62) 3216-6204</p>" +

				"<p><img src=\"" + urlRodapeEscolaContas + "\" alt=\"Rodape-Escola-Contas\"/></p>" +

				"</body>" +

				"</html>";

		return corpoEmail;
	}

	public byte[] gerarCartaoParticipante(Inscricao inscricao) {

		if(!AjudanteObjeto.eReferencia(inscricao)) {

			return null;
		}

		String conteudoCartao = "";

		String prefixoTagNegrito = "<style pdfFontName='Helvetica-Bold'>";

		String sufixoTagNegrito = "</style>";

		String saltoDeLinha = "\n";

		String contextoQRCodeInscricao = this.gerarUrlQRCodeParaInscricao(inscricao, "420");

		String nomeParticipante = inscricao.getNomeParticipante();

		String nomeDoEvento = inscricao.getEvento().getTitulo();

		String local = inscricao.getEvento().getLocalizacao().getDescricao();

		String endereco = inscricao.getEvento().getLocalizacao().getEndereco().toString();

		Format formatadorDatas = new SimpleDateFormat("dd/MM/yyyy");

		String dataDoEvento = "";

		if(AjudanteObjeto.eReferencia(inscricao.getEvento().getDataInicioPrevisto())) {

			dataDoEvento += formatadorDatas.format(inscricao.getEvento().getDataInicioPrevisto());

			if(AjudanteObjeto.eReferencia(inscricao.getEvento().getDataFimPrevisto())) {

				dataDoEvento += " a ";
			}
		}
		if(AjudanteObjeto.eReferencia(inscricao.getEvento().getDataFimPrevisto())) {

			dataDoEvento += formatadorDatas.format(inscricao.getEvento().getDataFimPrevisto());
		}

		String horaInicio = "";

		List<Modulo> modulosEvento = this.moduloNegocio.listarOrdenadoPorData(inscricao.getEvento());

		Modulo primeiroModuloDoEvento;

		if(AjudanteObjeto.eReferencia(modulosEvento) && modulosEvento.size() > 0) {

			primeiroModuloDoEvento = this.moduloNegocio.listarOrdenadoPorData(inscricao.getEvento()).get(0);

			if(AjudanteObjeto.eReferencia(primeiroModuloDoEvento)) {

				if(!AjudanteObjeto.eVazia(primeiroModuloDoEvento.getHoraInicioTurno1())) {

					horaInicio += primeiroModuloDoEvento.getHoraInicioTurno1();
				}
				else if(!AjudanteObjeto.eVazia(primeiroModuloDoEvento.getHoraInicioTurno2())) {

					horaInicio += primeiroModuloDoEvento.getHoraInicioTurno2();
				}
			}
		}
		
		String cidadeParticipante = "";
		
		if (AjudanteObjeto.eReferencia(inscricao.getParticipante().getEndereco().getMunicipioId())) {
			
			Municipio municipioParticipante = (Municipio) this.municipioDAO.obterEntidadePorId(Municipio.class, inscricao.getParticipante().getEndereco().getMunicipioId());
			cidadeParticipante = municipioParticipante != null ? municipioParticipante.getDescricao() : "";
			
		} else {
			
			Cidade cidadeParticipanteCidade = this.cidadeDAO.obterPorAtributo("id", inscricao.getParticipante().getEndereco().getCidade().getId());
			cidadeParticipante = cidadeParticipanteCidade != null ? cidadeParticipanteCidade.getDescricao() : "";
			
		}

		String cargoParticipante = AjudanteObjeto.eReferencia(inscricao.getParticipante().getCargo()) ? inscricao.getParticipante().getCargo() : "";

		conteudoCartao = prefixoTagNegrito + "EVENTO: " + sufixoTagNegrito + nomeDoEvento + saltoDeLinha +
						saltoDeLinha +
						prefixoTagNegrito + "LOCAL: " + sufixoTagNegrito + local + saltoDeLinha +
						saltoDeLinha +
						prefixoTagNegrito + "ENDEREÇO: " + sufixoTagNegrito + endereco + saltoDeLinha +
						saltoDeLinha +
						prefixoTagNegrito + "DATA DO EVENTO: " + sufixoTagNegrito + dataDoEvento + saltoDeLinha +
						prefixoTagNegrito + "HORÁRIO DE INÍCIO: " + sufixoTagNegrito + horaInicio + saltoDeLinha +
						saltoDeLinha +
						prefixoTagNegrito + "<style size='12'>" + nomeParticipante + "</style>" + sufixoTagNegrito + saltoDeLinha +
						"<style size='8'>" + cidadeParticipante + "</style>" + saltoDeLinha +
						"<style size='8'>" + cargoParticipante + "</style>";

		AjudanteParametrosRelatorio parametrosCartaoParticipante = new AjudanteParametrosRelatorio();

		parametrosCartaoParticipante.adicionarParametroRelatorio("contextoLogoTCMGO", contextoAplicacao.getRealPath("/resources/imagens/tcmgo-logo-novo.png"));

		parametrosCartaoParticipante.adicionarParametroRelatorio("contextoImagemQRCode", contextoQRCodeInscricao);

		parametrosCartaoParticipante.adicionarParametroRelatorio("conteudoCartao", conteudoCartao);

		String caminhoTemplateCartaoParticipante = contextoAplicacao.getRealPath("/WEB-INF/cartao-participante-jr/cartao-participante-template.jasper");

		return AjudanteRelatorio.gerarRelatorio(caminhoTemplateCartaoParticipante, parametrosCartaoParticipante.getParametrosRelatorio(),
                new FonteDadosVazia(), GeradorRelatorio.SAIDA_PDF, false);
	}

	public void enviarEmailInscricaoAprovada(Inscricao inscricao) {

		String tituloEmail = "TCMGO - Tribunal de Contas dos Municípios do Estado de Goiás";

		String subjectEmail = "Inscrição Aprovada - " + inscricao.getEvento().getTitulo();

		String corpoEmail = this.gerarCorpoEmailInscricaoAprovada(inscricao);

		byte[] saidaDoCartao;

		InputStream cartaoParticipanteIS = null;

		try {
			saidaDoCartao = this.gerarCartaoParticipante(inscricao);

			cartaoParticipanteIS = new ByteArrayInputStream(saidaDoCartao);

		} catch (Exception e) {

			e.printStackTrace();
		}
		AjudanteEmail.enviarEmailComAnexo(inscricao.getParticipante().getPessoa().getEmail(), tituloEmail, subjectEmail, corpoEmail, cartaoParticipanteIS,
				"application/pdf", "Cartao do Participante.pdf", "Cartão do Participante.");
	}

	public void aprovarInscricao(Inscricao inscricao, Usuario usuarioLogado) {

		inscricao.setAutorizada(Boolean.TRUE);

		inscricao.setDataAvaliacaoPreInscricao(new Date());

		inscricao.setAvaliador(usuarioLogado);

		this.enviarEmailInscricaoAprovada(inscricao);

		this.salvar(inscricao);
	}

	public void reprovarInscricao(Inscricao inscricao, Usuario usuarioLogado) {

		inscricao.setAutorizada(Boolean.FALSE);

		inscricao.setDataAvaliacaoPreInscricao(new Date());

		inscricao.setAvaliador(usuarioLogado);

		this.salvar(inscricao);
	}

	public List<Inscricao> listarInscricoesAutorizadas(Evento evento) {

		return this.getDAO().listarInscricoesAutorizadas(evento);
	}

	public List<Inscricao> listarInscricoesNaoAutorizadas(Evento evento) {

		return this.getDAO().listarInscricoesNaoAutorizadas(evento);
	}

	public List<Inscricao> listarInscricoesAguardando(Evento evento) {

		return this.getDAO().listarInscricoesAguardando(evento);
	}

	public List<Inscricao> listarInscricoesAutorizadas(Participante participante) {

		return this.getDAO().listarInscricoesAutorizadas(participante);
	}

	public List<Inscricao> listarInscricoesNaoAutorizadas(Participante participante) {

		return this.getDAO().listarInscricoesNaoAutorizadas(participante);
	}

	public List<Inscricao> listarInscricoesAguardando(Participante participante) {

		return this.getDAO().listarInscricoesAguardando(participante);
	}

	public int obterQuantidadeTotal(Evento entidade) {

		return this.getDAO().obterQuantidadeTotal(entidade);
	}

	public int obterQuantidadeAutorizada(Evento entidade) {

		return this.getDAO().obterQuantidadeAutorizada(entidade);
	}

	public int obterQuantidadeNegada(Evento entidade) {

		return this.getDAO().obterQuantidadeNegada(entidade);
	}
}
