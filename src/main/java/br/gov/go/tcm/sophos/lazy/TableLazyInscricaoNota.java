/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.lazy;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Inscricao;
import br.gov.go.tcm.sophos.entidade.Modulo;
import br.gov.go.tcm.sophos.entidade.Nota;
import br.gov.go.tcm.sophos.lazy.base.TableLazyDTO;
import br.gov.go.tcm.sophos.lazy.base.TableLazyPadrao;
import br.gov.go.tcm.sophos.negocio.InscricaoNegocio;
import br.gov.go.tcm.sophos.negocio.NotaNegocio;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
@Scope("session")
public class TableLazyInscricaoNota extends TableLazyPadrao<Nota> {

	private static final long serialVersionUID = -6545514037278206167L;

	@Autowired
	private NotaNegocio negocio;
	
	@Autowired
	private InscricaoNegocio inscricaoNegocio;
	
	private Modulo modulo;
	
	private Evento evento;

	@Override
	public List<Nota> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		
		sortOrder = SortOrder.ASCENDING;
		
		sortField = "pessoa.nome";
		
		filters.put("evento", evento);
		
		filters.put("autorizada", Boolean.TRUE);
		
		TableLazyDTO<Inscricao> tableLazyDTO = this.inscricaoNegocio.listarPaginado(first, pageSize, sortField, sortOrder, filters,	this.getConsulta());

		this.setLista(new ArrayList<Nota>());
		
		
		if (AjudanteObjeto.eReferencia(this.modulo)) {
			
			for (Inscricao inscricao : tableLazyDTO.getLista()) {
				
				
				Nota nota = this.negocio.obterNotaParticipanteModulo(this.modulo, inscricao.getParticipante());
				
				
				if(AjudanteObjeto.eReferencia(nota)){
					
					if(nota.getParticipante().getPessoa().getNome().toLowerCase().contains(this.getConsulta().toLowerCase()))this.getLista().add(nota);
					
				}else{
				
					Nota notaTemp = new Nota();

					notaTemp.setDataCadastro(new Date());

					notaTemp.setModulo(this.modulo);
					
					notaTemp.setValor(BigDecimal.valueOf(0));

					notaTemp.setParticipante(inscricao.getParticipante());

					if(notaTemp.getParticipante().getPessoa().getNome().toLowerCase().contains(this.getConsulta().toLowerCase()))this.getLista().add(notaTemp);
				}
			}

		}
		
		this.setRowCount(tableLazyDTO.getQuantidade().intValue());

		this.setPageSize(pageSize);
		
		return this.getLista();

	}
	
	@Override
	protected NotaNegocio getServico() {

		return this.negocio;
	}

	public Modulo getModulo() {
		return modulo;
	}

	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}
	
}
