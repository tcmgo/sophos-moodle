/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "inscricao")
@Banco(nome = Banco.SOPHOS)
public class Inscricao extends EntidadePadrao {

	private static final long serialVersionUID = 7426197962349273286L;
	
	@Column(name = "pre_inscricao")
	private Boolean preInscricao;
	
	@Column(name = "autorizada")
	private Boolean autorizada;
	
	@Column(name = "justificativa_participante", length = 2000)
	@Basic(fetch = FetchType.LAZY)
	private String justificativaParticipante;
	
	@Column(name = "data_avaliacao_preinscricao")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataAvaliacaoPreInscricao;
	
	@JoinColumn(name = "avaliador_id", referencedColumnName = "id")
	@ManyToOne
	private Usuario avaliador;
	
	@JoinColumn(name = "evento_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Evento evento;

	@JoinColumn(name = "participante_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Participante participante;
	
	@JoinColumn(name = "indicacao_id", referencedColumnName = "id")
	@ManyToOne
	private Indicacao indicacao;
	
	public Boolean getPreInscricao() {
		return preInscricao;
	}

	public void setPreInscricao(Boolean preInscricao) {
		this.preInscricao = preInscricao;
	}
	
	public String getJustificativaParticipante() {
		return justificativaParticipante;
	}

	public void setJustificativaParticipante(String justificativaParticipante) {
		this.justificativaParticipante = justificativaParticipante;
	}

	public Boolean getAutorizada() {
		return autorizada;
	}

	public void setAutorizada(Boolean autorizada) {
		this.autorizada = autorizada;
	}

	public Date getDataAvaliacaoPreInscricao() {
		return dataAvaliacaoPreInscricao;
	}

	public void setDataAvaliacaoPreInscricao(Date dataAvaliacaoPreInscricao) {
		this.dataAvaliacaoPreInscricao = dataAvaliacaoPreInscricao;
	}

	public Usuario getAvaliador() {
		return avaliador;
	}

	public void setAvaliador(Usuario avaliador) {
		this.avaliador = avaliador;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public Participante getParticipante() {
		return participante;
	}

	public void setParticipante(Participante participante) {
		this.participante = participante;
	}

	public Indicacao getIndicacao() {
		return indicacao;
	}

	public void setIndicacao(Indicacao indicacao) {
		this.indicacao = indicacao;
	}

	public String getNomeParticipante(){
		if(AjudanteObjeto.eReferencia(this.participante)){
			return this.participante.getPessoa().getNome();
		}
		return null;
	}

}
