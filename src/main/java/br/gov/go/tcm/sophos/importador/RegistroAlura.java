/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.importador;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.annotations.SerializedName;

@XmlRootElement(name="registroAlura")
@XmlAccessorType (XmlAccessType.FIELD)
public class RegistroAlura {
	
	@SerializedName("Email do aluno")
	private String emailDoAluno;
	
	@SerializedName("Curso")
	private String curso;
	
	@SerializedName("Carga horária estimada")
	private BigDecimal cargaHorariaEstimada;
	
	@SerializedName("Quantidade de seções")
	private BigDecimal quantidadeSecoes;
	
	@SerializedName("Iniciado em")
	private String iniciadoEm;
	
	@SerializedName("Terminado em")
	private String terminadoEm;
	
	@SerializedName("% Seções Finalizadas")
	private BigDecimal secoesFinalizadas;
	
	@SerializedName("% Exercícios Feitos")
	private BigDecimal exerciciosFeitos;
	
	@SerializedName("Aproveitamento")
	private BigDecimal aproveitamento;
	

	public String getEmailDoAluno() {
		return emailDoAluno;
	}

	public void setEmailDoAluno(String emailDoAluno) {
		this.emailDoAluno = emailDoAluno;
	}

	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}

	public BigDecimal getCargaHorariaEstimada() {
		return cargaHorariaEstimada;
	}

	public void setCargaHorariaEstimada(BigDecimal cargaHorariaEstimada) {
		this.cargaHorariaEstimada = cargaHorariaEstimada;
	}

	public BigDecimal getQuantidadeSecoes() {
		return quantidadeSecoes;
	}

	public void setQuantidadeSecoes(BigDecimal quantidadeSecoes) {
		this.quantidadeSecoes = quantidadeSecoes;
	}

	public String getIniciadoEm() {
		return iniciadoEm;
	}

	public void setIniciadoEm(String iniciadoEm) {
		this.iniciadoEm = iniciadoEm;
	}

	public String getTerminadoEm() {
		return terminadoEm;
	}

	public void setTerminadoEm(String terminadoEm) {
		this.terminadoEm = terminadoEm;
	}

	public BigDecimal getSecoesFinalizadas() {
		return secoesFinalizadas;
	}

	public void setSecoesFinalizadas(BigDecimal secoesFinalizadas) {
		this.secoesFinalizadas = secoesFinalizadas;
	}

	public BigDecimal getExerciciosFeitos() {
		return exerciciosFeitos;
	}

	public void setExerciciosFeitos(BigDecimal exerciciosFeitos) {
		this.exerciciosFeitos = exerciciosFeitos;
	}

	public BigDecimal getAproveitamento() {
		return aproveitamento;
	}

	public void setAproveitamento(BigDecimal aproveitamento) {
		this.aproveitamento = aproveitamento;
	}

}
