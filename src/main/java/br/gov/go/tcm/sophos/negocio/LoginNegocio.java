/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.estrutura.entidade.controledeacesso.Modulo;
import br.gov.go.tcm.estrutura.entidade.controledeacesso.UsuarioMonitor;
import br.gov.go.tcm.estrutura.entidade.controledeacesso.UsuarioPerfilModulo;
import br.gov.go.tcm.estrutura.entidade.enumeracao.UsuarioEnumeracao;
import br.gov.go.tcm.estrutura.entidade.usuario.UsuarioChefeSecao;
import br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato.ModuloDAO;
import br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato.UsuarioMonitorDAO;
import br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato.UsuarioPerfilModuloDAO;
import br.gov.go.tcm.estrutura.persistencia.usuario.contrato.UsuarioChefeSecaoDAO;
import br.gov.go.tcm.estrutura.seguranca.ajudante.AjudanteCriptografia;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.Participante;
import br.gov.go.tcm.sophos.entidade.PerfilUsuario;
import br.gov.go.tcm.sophos.entidade.Pessoa;
import br.gov.go.tcm.sophos.entidade.Usuario;
import br.gov.go.tcm.sophos.entidade.enumeracao.PerfilEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoDominioEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoUsuario;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.persistencia.PerfilUsuarioDAO;
import br.gov.go.tcm.sophos.persistencia.UsuarioDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Service
public class LoginNegocio implements Serializable {

	private static final long serialVersionUID = 4961151277342318972L;

	private static final String USUARIO_INTERNO_INEXISTENTE = "NOME DE USUÁRIO inexistente. Favor tentar o login com um NOME DE USUÁRIO válido. ";
	
	private static final String USUARIO_EXTERNO_INEXISTENTE = "CPF inexistente. Favor tentar o login com um CPF válido. ";

	private static final String SENHA_INVALIDA = "Senha inválida para o CPF/NOME DE USUÁRIO informado.";

	@Autowired
	private UsuarioDAO usuarioDAO;

	@Autowired
	private UsuarioNegocio usuarioNegocio;

	@Autowired
	private UsuarioMonitorDAO usuarioMonitorDAO;

	@Autowired
	private DominioNegocio dominioNegocio;

	@Autowired
	private PessoaNegocio pessoaNegocio;

	@Autowired
	private PerfilUsuarioDAO perfilUsuarioDAO;

	@Autowired
	private ModuloDAO moduloDAO;

	@Autowired
	private UsuarioPerfilModuloDAO usuarioPerfilModuloDAO;

	@Autowired
	private UsuarioChefeSecaoDAO usuarioChefeSecaoDAO;

	@Autowired
	private ParticipanteNegocio participanteNegocio;

	public Usuario logarPorNomeUsuario(String nomeUsuario, String senha) {

		Usuario usuario = obterUsuarioPorNomeUsuario(nomeUsuario);
		
		return this.logar(usuario, senha);

	}
	
	public Usuario logarPorCPF(String cpf, String senha) {

		Usuario usuario = obterUsuarioPorCPF(cpf);
		
		return this.logar(usuario, senha);

	}
	
	public Usuario logar(Usuario usuario, String senha) {

		usuario.setPerfilList(this.perfilUsuarioDAO.listaPorAtributo("usuario", usuario));

		if (usuario.getTipoUsuario().equals(TipoUsuario.class.getSimpleName(),
				TipoUsuario.USUARIO_INTERNO.getCodigo())) {

			this.verificaSenhaUsuarioInterno(senha, usuario);

		} else if (usuario.getTipoUsuario().equals(TipoUsuario.class.getSimpleName(),
				TipoUsuario.USUARIO_EXTERNO.getCodigo())) {

			this.verificaSenha(senha, usuario.getSenha());

		}

		if (!usuarioNegocio.existePerfilParticipante(usuario)) {

			usuarioNegocio.definePerfilParticipante(usuario);
		}

		this.verificaParticipanteUsuario(usuario);

		return usuario;

	}

	private void verificaParticipanteUsuario(Usuario usuario) {

		if (!AjudanteObjeto.eReferencia(usuario.getParticipante())) {

			Participante participante = null;

			if (usuario.getTipoUsuario().equals(TipoUsuario.class.getSimpleName(),
					TipoUsuario.USUARIO_INTERNO.getCodigo())) {

				participante = this.participanteNegocio.obterPorAtributo("matricula", usuario.getMatricula());

			} else {

				participante = this.participanteNegocio.obterPorAtributo("pessoa.cpf", usuario.getPessoa().getCpf());
			}

			if (AjudanteObjeto.eReferencia(participante)
					&& !AjudanteObjeto.eReferencia(this.usuarioDAO.obterPorAtributo("participante", participante))) {

				usuario.setParticipante(participante);

				this.usuarioDAO.atualizar(usuario);
			}
		}
	}

	private void verificaSenha(String senha, String senhaUsuario) {

		String senhaMD5 = AjudanteCriptografia.gerarMD5(senha);

		if (!senhaMD5.equals(senhaUsuario)) {

			throw new ValidacaoException(SENHA_INVALIDA);
		}
	}

	private void verificaSenhaUsuarioInterno(String senha, Usuario usuario) {

		UsuarioMonitor usuarioMonitor = (UsuarioMonitor) this.usuarioMonitorDAO.obterEntidadePorId(UsuarioMonitor.class,
				usuario.getUsuarioId());

		this.verificaSenha(senha, usuarioMonitor.getSenha());

		this.definirPerfilUsuarioInterno(usuario, usuarioMonitor);

		this.verificaUsuarioInternoPerfilChefe(usuario, usuarioMonitor);
	}

	private void verificaUsuarioInternoPerfilChefe(Usuario usuario, UsuarioMonitor usuarioMonitor) {

		UsuarioChefeSecao chefeSecao = this.usuarioChefeSecaoDAO.obterUsuarioChefeSecao(usuarioMonitor.getId(),
				Long.parseLong(usuarioMonitor.getCodigoSecao()));

		if (AjudanteObjeto.eReferencia(chefeSecao)) {

			Boolean existePerfilChefe = Boolean.FALSE;

			for (PerfilUsuario perfilUsuario : usuario.getPerfilList()) {

				if (perfilUsuario.getPerfil().equals(PerfilEnum.CHEFE)) {

					existePerfilChefe = Boolean.TRUE;
				}
			}

			if (!existePerfilChefe) {

				PerfilUsuario perfilUsuario = new PerfilUsuario();

				perfilUsuario.setUsuario(usuario);

				perfilUsuario.setPerfil(PerfilEnum.CHEFE);

				this.perfilUsuarioDAO.salvar(perfilUsuario);

				usuario.getPerfilList().add(perfilUsuario);
			}
		}
	}

	private void definirPerfilUsuarioInterno(Usuario usuario, UsuarioMonitor usuarioMonitor) {

		Modulo modulo = (Modulo) moduloDAO.obterEntidadePorId(Modulo.class, new Long(Modulo.MODULO_TCM_SOPHOS));

		List<UsuarioPerfilModulo> lstUsuarioPerfilModulo = usuarioPerfilModuloDAO
				.selecionarLstUsuarioPerfilModuloPorModuloUsuarioIdTipoUsuario(modulo, usuarioMonitor.getId(),
						UsuarioEnumeracao.USUARIO_INTERNO);

		for (UsuarioPerfilModulo usuarioPerfilModulo : lstUsuarioPerfilModulo) {

			PerfilEnum perfilUsuarioMonitor = PerfilEnum.valueOf(usuarioPerfilModulo.getPerfil().getDescricao());

			Boolean existePerfil = Boolean.FALSE;

			for (PerfilUsuario perfilUsuario : usuario.getPerfilList()) {

				if (perfilUsuario.getPerfil().equals(perfilUsuarioMonitor)) {

					existePerfil = Boolean.TRUE;

					break;
				}
			}

			if (!existePerfil && perfilUsuarioMonitor != null) {

				PerfilUsuario perfilUsuario = new PerfilUsuario();

				perfilUsuario.setUsuario(usuario);

				perfilUsuario.setPerfil(perfilUsuarioMonitor);

				this.perfilUsuarioDAO.salvar(perfilUsuario);

				usuario.getPerfilList().add(perfilUsuario);
			}
		}
	}

	private Usuario obterUsuarioPorNomeUsuario(String nomeUsuario) {

		Usuario usuario = this.usuarioDAO.obterPorAtributo("nomeUsuario", nomeUsuario);

		if (!AjudanteObjeto.eReferencia(usuario)) {

			return this.verificaExistenciaUsuarioTCMPorNomeUsuario(nomeUsuario);

		}

		return usuario;
	}

	private Usuario obterUsuarioPorCPF(String cpf) {

		Pessoa pessoa = pessoaNegocio.obterPorAtributo("cpf", cpf);

		Usuario usuario = null;

		if (AjudanteObjeto.eReferencia(pessoa)) {
			usuario = this.usuarioDAO.obterPorAtributo("pessoa", pessoa);
		}

		if (!AjudanteObjeto.eReferencia(usuario)) {

			return this.verificaExistenciaUsuarioTCMPorCPF(cpf);

		}

		return usuario;
	}

	private Usuario verificaExistenciaUsuarioTCMPorNomeUsuario(String nomeUsuario) {

		Usuario usuario = null;

		usuario = this.verificaExistenciaESalvaUsuarioInterno(nomeUsuario);

		if (!AjudanteObjeto.eReferencia(usuario) || !AjudanteObjeto.eReferencia(usuario.getId())) {

			throw new ValidacaoException(USUARIO_INTERNO_INEXISTENTE);
		}

		return usuario;
	}
	
	private Usuario verificaExistenciaUsuarioTCMPorCPF(String cpf) {

		Usuario usuario = null;

		usuario = this.verificaExistenciaESalvaUsuarioInternoPorCPF(cpf);

		if (!AjudanteObjeto.eReferencia(usuario) || !AjudanteObjeto.eReferencia(usuario.getId())) {

			throw new ValidacaoException(USUARIO_EXTERNO_INEXISTENTE);
		}

		return usuario;
	}

	@SuppressWarnings("unchecked")
	private Usuario verificaExistenciaESalvaUsuarioInterno(String nomeUsuario) {

		UsuarioMonitor usuarioMonitor = new UsuarioMonitor();

		usuarioMonitor.setNomeUsuario(nomeUsuario);

		usuarioMonitor.setAtivo(Boolean.TRUE);

		List<UsuarioMonitor> lstUsuariosMonitor = (List<UsuarioMonitor>) this.usuarioMonitorDAO
				.obterPorExemplo(usuarioMonitor, 0);

		if (lstUsuariosMonitor.size() > 0) {

			Usuario usuario = new Usuario();

			usuario.setPessoa(new Pessoa());

			usuario.setDataCadastro(new Date());

			usuarioMonitor = lstUsuariosMonitor.get(0);

			usuario.setTipoUsuario(this.dominioNegocio.obterDominio(TipoDominioEnum.TipoUsuario,
					TipoUsuario.USUARIO_INTERNO.getCodigo()));

			usuario.setNomeUsuario(usuarioMonitor.getNomeUsuario());

			usuario.setUsuarioId(usuarioMonitor.getId());

			usuario.setSecaoID(usuarioMonitor.getCodigoSecao());

			usuario.getPessoa().setNome(usuarioMonitor.getNomeCompleto());

			usuario.setMatricula(usuarioMonitor.getMatricula());

			this.pessoaNegocio.salvar(usuario.getPessoa());

			this.usuarioDAO.salvar(usuario);

			return usuario;
		}

		return null;
	}
	
	@SuppressWarnings("unchecked")
	private Usuario verificaExistenciaESalvaUsuarioInternoPorCPF(String cpf) {

		UsuarioMonitor usuarioMonitor = new UsuarioMonitor();
		
		Pessoa pessoa = pessoaNegocio.obterPorAtributo("cpf", cpf);
		
		Usuario usuarioCpf = null;
		if (AjudanteObjeto.eReferencia(pessoa)) {
			usuarioCpf = usuarioNegocio.obterPorAtributo("pessoa", pessoa);
		}
		
		String nomeUsuario = null;
		if (AjudanteObjeto.eReferencia(usuarioCpf) && AjudanteObjeto.eReferencia(usuarioCpf.getNomeUsuario())) {
			nomeUsuario = usuarioCpf.getNomeUsuario();
		}
		
		if (!AjudanteObjeto.eReferencia(nomeUsuario)) {
			return null;
		}

		usuarioMonitor.setNomeUsuario(nomeUsuario);

		usuarioMonitor.setAtivo(Boolean.TRUE);

		List<UsuarioMonitor> lstUsuariosMonitor = (List<UsuarioMonitor>) this.usuarioMonitorDAO.obterPorExemplo(usuarioMonitor, 0);

		if (lstUsuariosMonitor.size() > 0) {

			Usuario usuario = new Usuario();

			usuario.setPessoa(new Pessoa());

			usuario.setDataCadastro(new Date());

			usuarioMonitor = lstUsuariosMonitor.get(0);

			usuario.setTipoUsuario(this.dominioNegocio.obterDominio(TipoDominioEnum.TipoUsuario,
					TipoUsuario.USUARIO_INTERNO.getCodigo()));

			usuario.setNomeUsuario(usuarioMonitor.getNomeUsuario());

			usuario.setUsuarioId(usuarioMonitor.getId());

			usuario.setSecaoID(usuarioMonitor.getCodigoSecao());

			usuario.getPessoa().setNome(usuarioMonitor.getNomeCompleto());

			usuario.setMatricula(usuarioMonitor.getMatricula());

			this.pessoaNegocio.salvar(usuario.getPessoa());

			this.usuarioDAO.salvar(usuario);

			return usuario;
		}

		return null;
	}

}
