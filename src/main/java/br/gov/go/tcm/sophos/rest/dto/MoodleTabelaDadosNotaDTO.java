package br.gov.go.tcm.sophos.rest.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MoodleTabelaDadosNotaDTO {

    private MoodleTabelaDadosTotalNotaDTO contributiontocoursetotal;

	public MoodleTabelaDadosTotalNotaDTO getContributiontocoursetotal() {
		return contributiontocoursetotal;
	}

	public void setContributiontocoursetotal(MoodleTabelaDadosTotalNotaDTO contributiontocoursetotal) {
		this.contributiontocoursetotal = contributiontocoursetotal;
	}

}
