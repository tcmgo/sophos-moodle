/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.persistencia.impl;

import br.gov.go.tcm.sophos.entidade.Usuario;
import br.gov.go.tcm.sophos.persistencia.UsuarioDAO;
import br.gov.go.tcm.sophos.persistencia.base.DAO;
import br.gov.go.tcm.sophos.persistencia.impl.base.DAOGenerico;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

@Repository
public class UsuarioDAOImpl extends DAOGenerico<Usuario> implements UsuarioDAO {

	private static final long serialVersionUID = 294560128456827146L;

	@Autowired
	public UsuarioDAOImpl(@Qualifier(DAO.SESSION_FACTORY_SOPHOS) final SessionFactory factory, @Qualifier(DAO.HIBERNATE_TEMPLATE_SOPHOS) final HibernateTemplate hibernateTemplate) {

		this.setHibernateTemplate(hibernateTemplate);

		this.setSessionFactory(factory);
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Usuario> listarPorListaIds(List<Long> ids, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters, String consulta) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());

		criteria.add(Restrictions.in("id", ids));
		
		super.defineBuscaProFiltros(filters, criteria); 
		
		Disjunction queryAll = super.definirBuscaPorConsulta(consulta, criteria);
		
		criteria.add(queryAll);
		
		if(SortOrder.ASCENDING.equals(sortOrder) && !StringUtils.isEmpty(sortField)){
			
			criteria.addOrder(Order.asc(sortField));
			
		}else if(SortOrder.DESCENDING.equals(sortOrder) && !StringUtils.isEmpty(sortField)){
			
			criteria.addOrder(Order.desc(sortField));
		}
		
		criteria.setFirstResult(first);

		criteria.setMaxResults(pageSize);

		try {

			return criteria.list();

		} finally {

			session.close();
		}
	}
	
	@Override
	public boolean existeUsuarioComNomeUsuario(final String nomeUsuario) {

		return this.validarPropriedadeIgual(Usuario.class, new String[] { "nomeUsuario" }, new Object[] { nomeUsuario });
	}

	@Override
	public Usuario obterUsuarioPorCPF(String cpf) {
		
		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());

		criteria.createAlias("pessoa", "pessoa");
		
		criteria.add(Restrictions.eq("pessoa.cpf", cpf));

		criteria.setMaxResults(1);

		criteria.addOrder(Order.desc("id"));
		
		try {

			return (Usuario) criteria.uniqueResult();
		}
		finally {

			session.close();
		}
	}

}
