/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;

import javax.persistence.*;
import java.text.Normalizer;

@Entity
@Table(name = "cidade")
@Banco(nome = Banco.SOPHOS)
public class Cidade extends EntidadePadrao {

	private static final long serialVersionUID = 2802149304997615610L;

	@Column(name = "descricao", nullable = false)
	private String descricao;

	@JoinColumn(name = "estado_id", referencedColumnName = "id")
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Estado estado;

	public String getDescricao() {

		if (AjudanteObjeto.eReferencia(this.descricao)) {

			String descricaoMaiusculaSemAcentos = Normalizer
					.normalize(this.descricao.toUpperCase(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
			
			descricaoMaiusculaSemAcentos = descricaoMaiusculaSemAcentos.replace(" DE ", " ");
			descricaoMaiusculaSemAcentos = descricaoMaiusculaSemAcentos.replace(" DO ", " ");
			descricaoMaiusculaSemAcentos = descricaoMaiusculaSemAcentos.replace("D'A", "DA");
			descricaoMaiusculaSemAcentos = descricaoMaiusculaSemAcentos.replace("TAQUARAL GOIAS", "TAQUARAL");

			return descricaoMaiusculaSemAcentos;

		} else {
			return null;
		}

	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

}
