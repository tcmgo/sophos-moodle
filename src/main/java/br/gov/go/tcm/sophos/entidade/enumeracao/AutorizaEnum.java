package br.gov.go.tcm.sophos.entidade.enumeracao;

public enum AutorizaEnum {

	SIM("SIM", true), NAO("NÃO", false), A("AGUARDANDO AUTORIZAÇÃO", null);

	private String descricao;

	private Boolean valor;

	private AutorizaEnum(String descricao, Boolean valor) {
		this.descricao = descricao;
		this.valor = valor;
	}

	public String getDescricao() {
		return descricao;
	}

	public Boolean getValor() {
		return valor;
	}

	public static AutorizaEnum getAutorizaEnum(String value) {

		for (AutorizaEnum autorizaEnum : AutorizaEnum.values()) {

			if (autorizaEnum.getDescricao().equals(value)) {

				return autorizaEnum;
			}
		}

		return null;
	}

}
