/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade.base;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteReflexao;
import br.gov.go.tcm.estrutura.entidade.base.Entidade;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.listener.LogListener;
import org.springframework.beans.factory.annotation.Configurable;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@MappedSuperclass
@EntityListeners(LogListener.class)
@Configurable
public class EntidadePadrao extends br.gov.go.tcm.estrutura.entidade.base.EntidadePadrao implements Entidade {

	private static final long serialVersionUID = -3885557336867153545L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "data_cadastro", updatable = false, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;

	@Transient
	private String uuid;

	public EntidadePadrao() {
		this.dataCadastro = new Date();
		this.uuid = UUID.randomUUID().toString();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public Date getDataCadastro() {

		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	@PrePersist
	public void preInsert() {

		this.setDataCadastro(new Date());
	}

	public int hashCodeID() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public boolean equalsID(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntidadePadrao other = (EntidadePadrao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	public int hashCodeUUID() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	public boolean equalsUUID(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntidadePadrao other = (EntidadePadrao) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		if(AjudanteObjeto.eReferencia(this.id)){
			return this.hashCodeID();
		}else{
			return this.hashCodeUUID();
		}
	}

	@Override
	public boolean equals(Object obj) {
		if(AjudanteObjeto.eReferencia(this.id)){
			return this.equalsID(obj);
		}else{
			return this.equalsUUID(obj);
		}
	}

	@Override
	public String toString() {
		String descObjeto = "[";
		AjudanteReflexao assistenteReflexao = new AjudanteReflexao();
		List<String> propriedades = assistenteReflexao.obterPropriedades(this.getClass());

		for (String propriedade : propriedades) {
			Object valor;
			// FIXME: implementar exceção;
			// try {
			valor = assistenteReflexao.obterValorPropriedade(this, propriedade);
			// } catch (ReflexaoExcecao e) {
			// e.printStackTrace();
			// return "";
			// }

			if (valor != null && List.class.isAssignableFrom(valor.getClass())) {
				continue;
			}

			descObjeto += propriedade + "=" + ((valor != null) ? valor.toString().trim() : "") + " ,";
		}
		if (descObjeto.length() > 1)
			descObjeto = descObjeto.substring(0, descObjeto.length() - 1);
		descObjeto += "]";
		return descObjeto;
	}

	public boolean isNovo() {

		return !AjudanteObjeto.eReferencia(this.id);
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
