/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.ajudante;

import br.gov.go.tcm.estrutura.container.spring.ContextoSpring;
import br.gov.go.tcm.estrutura.util.email.AjudanteMail;
import br.gov.go.tcm.estrutura.util.email.OrigemMailAutenticacao;

import java.io.InputStream;

public class AjudanteEmail {
	
	public static void enviarEmail(final String email, final String titulo, final String sujeito, final String mensagem) {

		final OrigemMailAutenticacao origemMailAutenticacao = new OrigemMailAutenticacao();

		origemMailAutenticacao.setOrigem(ContextoSpring.getEmailPortal());

		origemMailAutenticacao.setRotuloOrigem(titulo);

		origemMailAutenticacao.setDestino(email);

		origemMailAutenticacao.setUsuario(ContextoSpring.getEmailCadastroUsuario());

		origemMailAutenticacao.setSenha(ContextoSpring.getEmailCadastroSenha());

		final Runnable envioEmailRunnable = new Runnable() {

			@Override
			public void run() {

				final AjudanteMail ajudante = new AjudanteMail();

				ajudante.enviarMensagem(new String[] { email }, sujeito, mensagem, origemMailAutenticacao,
						AjudanteMail.ConteudoEmail.HTML);
			}
		};

		new Thread(envioEmailRunnable).start();
	}

	public static void enviarEmailComAnexo(final String email, final String titulo, final String sujeito, final String mensagem,
										   final InputStream isAnexo, final String tipoArquivoAnexo, final String nomeArquivoAnexo, final String descricaoArquivoAnexo) {

		final OrigemMailAutenticacao origemMailAutenticacao = new OrigemMailAutenticacao();

		origemMailAutenticacao.setOrigem(ContextoSpring.getEmailPortal());

		origemMailAutenticacao.setRotuloOrigem(titulo);

		origemMailAutenticacao.setDestino(email);

		origemMailAutenticacao.setUsuario(ContextoSpring.getEmailCadastroUsuario());

		origemMailAutenticacao.setSenha(ContextoSpring.getEmailCadastroSenha());

		final Runnable envioEmailRunnable = new Runnable() {

			@Override
			public void run() {

				final AjudanteMail ajudante = new AjudanteMail();

				ajudante.enviarMensagem(new String[] { email }, sujeito, mensagem, origemMailAutenticacao,
						isAnexo, tipoArquivoAnexo, nomeArquivoAnexo, descricaoArquivoAnexo, AjudanteMail.ConteudoEmail.HTML);
			}
		};

		new Thread(envioEmailRunnable).start();
	}
}
