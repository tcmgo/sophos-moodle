/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.persistencia;

import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Inscricao;
import br.gov.go.tcm.sophos.entidade.Participante;
import br.gov.go.tcm.sophos.persistencia.base.DAO;

import java.util.List;

public interface InscricaoDAO extends DAO<Inscricao>{

	boolean existeInscricaoParaParticipante(Participante participante, Evento evento);

	Integer obterQuantidadeInscricoes(Evento evento);

	List<Inscricao> listarInscricoesAutorizadas(Evento evento);

	List<Inscricao> listarInscricoesAutorizadas(Participante participante);
	
	List<Inscricao> listarInscricoesNaoAutorizadas(Evento evento);

	List<Inscricao> listarInscricoesNaoAutorizadas(Participante participante);
	
	List<Inscricao> listarInscricoesAguardando(Evento evento);

	List<Inscricao> listarInscricoesAguardando(Participante participante);

	int obterQuantidadeTotal(Evento entidade);

	int obterQuantidadeAutorizada(Evento entidade);

	int obterQuantidadeNegada(Evento entidade);

}
