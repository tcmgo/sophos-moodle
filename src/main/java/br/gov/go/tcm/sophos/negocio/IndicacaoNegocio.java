/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.estrutura.entidade.controledeacesso.UsuarioMonitor;
import br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato.UsuarioMonitorDAO;
import br.gov.go.tcm.sophos.ajudante.AjudanteMensagem;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.*;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import br.gov.go.tcm.sophos.persistencia.IndicacaoDAO;
import br.gov.go.tcm.sophos.persistencia.base.DAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class IndicacaoNegocio extends NegocioBase<Indicacao> {

	private static final long serialVersionUID = -8426728536010364058L;

	@Autowired
	private UsuarioMonitorDAO usuarioMonitorDao;

	@Autowired
	private UsuarioNegocio usuarioNegocio;

	@Autowired
	private InscricaoNegocio inscricaoNegocio;

	@Autowired
	private IndicacaoDAO dao;

	@Override
	protected DAO<Indicacao> getDAO() {
		return dao;
	}

	public UsuarioMonitorDAO getUsuarioMonitorDao() {
		return usuarioMonitorDao;
	}

	public List<Usuario> listaUsuariosSetor(Usuario usuarioLogado, String consulta) {

		List<Usuario> usuariosSetor = new ArrayList<Usuario>();
		List<UsuarioMonitor> usuariosMonitor = usuarioMonitorDao
				.obterTodosUsuariosAtivosPorSecao(usuarioLogado.getSecaoID());

		for (UsuarioMonitor usuarioMonitor : usuariosMonitor) {

			Usuario usuarioParticipante = (Usuario) usuarioNegocio.obterPorAtributo("usuarioId",
					usuarioMonitor.getId());

			if (AjudanteObjeto.eReferencia(usuarioParticipante)
					&& AjudanteObjeto.eReferencia(usuarioParticipante.getParticipante())
					&& AjudanteObjeto.eReferencia(usuarioParticipante.getParticipante().getPessoa())) {

				usuariosSetor.add(usuarioParticipante);

			}

		}

		return usuariosSetor;

	}

	public boolean existeIndicacaoParaParticipante(Participante participante, Evento evento) {

		return dao.existeIndicacaoParaParticipante(participante, evento);

	}

	public boolean existeInscricaoRealizada(Participante participante, Evento evento) {

		return inscricaoNegocio.existeInscricaoParaParticipante(participante, evento);
	}

	public void aprovar(Indicacao indicacao, Usuario usuarioLogado) {

		Inscricao inscricao = new Inscricao();
		inscricao.setIndicacao(indicacao);
		inscricao.setParticipante(indicacao.getParticipante());
		inscricao.setEvento(indicacao.getEvento());
		Boolean autorizada = inscricaoNegocio.verificaAutorizacaoInscricao(indicacao.getEvento());
		inscricao.setAutorizada(autorizada);
		inscricao.setDataCadastro(new Date());
		inscricaoNegocio.salvar(inscricao);

		indicacao.setAprovado(true);
		indicacao.setDataAvaliacao(new Date());
		indicacao.setAvaliador(usuarioLogado);
		this.atualizar(indicacao);

	}

	public void reprovar(Indicacao indicacao, Usuario usuarioLogado) {

		indicacao.setAprovado(false);
		indicacao.setDataAvaliacao(new Date());
		indicacao.setAvaliador(usuarioLogado);
		this.atualizar(indicacao);

	}

	@Override
	public void excluir(Indicacao indicacao) {

		this.executarValidacaoAntesDeExcluir(indicacao);

		if (inscricaoNegocio.existeInscricaoParaParticipante(indicacao.getParticipante(), indicacao.getEvento())) {
			throw new ValidacaoException(AjudanteMensagem.obterMensagemPelaChave("impossivel_remover_indicacao"));
		} else {
			this.getDAO().excluir(indicacao);
		}

	}

}
