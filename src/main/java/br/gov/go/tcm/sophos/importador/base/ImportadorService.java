/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 *
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 *
 * Este cabeçalho deve ser mantido.
 *
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.importador.base;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.Certificado;
import br.gov.go.tcm.sophos.entidade.Dominio;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.EventoPublicoAlvo;
import br.gov.go.tcm.sophos.entidade.Frequencia;
import br.gov.go.tcm.sophos.entidade.Inscricao;
import br.gov.go.tcm.sophos.entidade.LocalizacaoEvento;
import br.gov.go.tcm.sophos.entidade.Modulo;
import br.gov.go.tcm.sophos.entidade.Nota;
import br.gov.go.tcm.sophos.entidade.Participante;
import br.gov.go.tcm.sophos.entidade.Pessoa;
import br.gov.go.tcm.sophos.entidade.ProvedorEvento;
import br.gov.go.tcm.sophos.entidade.dto.FiltroDTO;
import br.gov.go.tcm.sophos.entidade.enumeracao.ComparadorCriteriaEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoDominioEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoLogicoEnum;
import br.gov.go.tcm.sophos.negocio.CertificadoNegocio;
import br.gov.go.tcm.sophos.negocio.DominioNegocio;
import br.gov.go.tcm.sophos.negocio.EnderecoNegocio;
import br.gov.go.tcm.sophos.negocio.EventoNegocio;
import br.gov.go.tcm.sophos.negocio.FrequenciaNegocio;
import br.gov.go.tcm.sophos.negocio.InscricaoNegocio;
import br.gov.go.tcm.sophos.negocio.LocalizacaoEventoNegocio;
import br.gov.go.tcm.sophos.negocio.ModuloNegocio;
import br.gov.go.tcm.sophos.negocio.NotaNegocio;
import br.gov.go.tcm.sophos.negocio.ParticipanteNegocio;
import br.gov.go.tcm.sophos.negocio.PessoaNegocio;
import br.gov.go.tcm.sophos.negocio.ProvedorEventoNegocio;

@Service("importadorService")
public abstract class ImportadorService {

	@Autowired
	EventoNegocio eventoNegocio;

	@Autowired
	PessoaNegocio pessoaNegocio;

	@Autowired
	ParticipanteNegocio participanteNegocio;

	@Autowired
	InscricaoNegocio inscricaoNegocio;

	@Autowired
	ModuloNegocio moduloNegocio;

	@Autowired
	NotaNegocio notaNegocio;

	@Autowired
	FrequenciaNegocio frequenciaNegocio;

	@Autowired
	DominioNegocio dominioNegocio;

	@Autowired
	LocalizacaoEventoNegocio localizacaoNegocio;

	@Autowired
	EnderecoNegocio enderecoNegoacio;

	@Autowired
	ProvedorEventoNegocio provedorNegocio;

	@Autowired
	private CertificadoNegocio certificadoNegocio;

	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	private ResultadoImportador resultado = new ResultadoImportador();

	public abstract List<ItemImportado> importar(String URL, String serverName);

	public ResultadoImportador armazenar(List<ItemImportado> itens) {

		for (ItemImportado item : itens) {

			Evento evento = obterEventoJaCadastrado(item.getEvento());

			if (!AjudanteObjeto.eReferencia(evento)) {

				evento = salvarEvento(item.getEvento());
				Inscricao inscricao = salvarInscricao(evento, item.getEmailParticipante());
				salvarNota(inscricao, item.getNota());
				salvarFrequencia(inscricao, item.getFrequencia());
				this.gerarCertificados(evento);

			} else {

				Inscricao inscricao = obterInscricaoJaRealizada(evento, item.getEmailParticipante());

				if (!AjudanteObjeto.eReferencia(inscricao)) {

					inscricao = salvarInscricao(evento, item.getEmailParticipante());
					salvarNota(inscricao, item.getNota());
					salvarFrequencia(inscricao, item.getFrequencia());
					this.gerarCertificados(evento);

				} else {

					if (!isNotaJaLancada(evento, item.getEmailParticipante())) {
						salvarNota(inscricao, item.getNota());
					}

					if (!isFrequenciaJaLancada(evento, item.getEmailParticipante())) {
						salvarFrequencia(inscricao, item.getFrequencia());
					}

					this.gerarCertificados(evento);

				}

			}

		}

		ResultadoImportador resultadoRetorno = resultado;
		resultado = new ResultadoImportador();
		return resultadoRetorno;

	}

	private Evento salvarEvento(EventoImportado eventoImportado) {

		if (AjudanteObjeto.eReferencia(eventoImportado)) {

			Evento evento = montarEvento(eventoImportado);
			eventoNegocio.salvar(evento);
			resultado.iterarEventosImportados();

			return evento;
		}

		return null;

	}

	private Evento montarEvento(EventoImportado eventoImportado) {

		Evento evento = new Evento();

		try {

			evento.setCargaHoraria(eventoImportado.getCargaHoraria());
			evento.setConteudo(eventoImportado.getConteudo());
			evento.setDataInicioPrevisto(simpleDateFormat.parse(eventoImportado.getDataInicioPrevista()));
			evento.setDataFimPrevisto(simpleDateFormat.parse(eventoImportado.getDataFimPrevista()));
			evento.setDataInicioRealizacao(simpleDateFormat.parse(eventoImportado.getDataInicioRealizacao()));
			evento.setDataFimRealizacao(simpleDateFormat.parse(eventoImportado.getDataFimRealizacao()));
			evento.setTitulo(eventoImportado.getTitulo());
			evento.setVagas(eventoImportado.getVagas());
			evento.setPermitePreInscricao(false);
			evento.setModuloUnico(true);
			evento.setPermiteCertificado(true);
			evento.setMostrarNaHome(false);
			evento.setMediaFrequenciaAprovacao(BigDecimal.valueOf(75));
			evento.setMediaNotaAprovacao(BigDecimal.valueOf(70));
			evento.setAvaliarPorMedia(true);
			evento.setEventoImportado(true);

			Dominio eixoTematico = montarDominio(TipoDominioEnum.EixoTematico, eventoImportado.getEixoTematico());
			evento.setEixoTematico(eixoTematico);

			LocalizacaoEvento localizacao = montarLocalizacaoEvento(eventoImportado.getLocalizacao());
			evento.setLocalizacao(localizacao);

			Dominio modalidade = montarDominio(TipoDominioEnum.Modalidade, eventoImportado.getModalidade());
			evento.setModalidade(modalidade);

			ProvedorEvento provedor = montarProvedorEvento(eventoImportado.getCNPJProvedor());
			evento.setProvedor(provedor);

			Participante responsavel = montarResponsavelEvento(eventoImportado.getEmailResponsavel());
			evento.setResponsavelEvento(responsavel);

			Dominio tipo = montarDominio(TipoDominioEnum.TipoEvento, eventoImportado.getTipoEvento());
			evento.setTipoEvento(tipo);

			List<EventoPublicoAlvo> publicoAlvoList = new ArrayList<EventoPublicoAlvo>();
			Dominio dominioPublicoAlvo = montarDominio(TipoDominioEnum.TipoPublicoAlvo,
					eventoImportado.getPublicoAlvo());
			EventoPublicoAlvo publicoAlvo = new EventoPublicoAlvo();
			publicoAlvo.setPublicoAlvo(dominioPublicoAlvo);
			publicoAlvo.setEvento(evento);
			publicoAlvoList.add(publicoAlvo);
			evento.setPublicoAlvoList(publicoAlvoList);

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return evento;

	}

	private Dominio montarDominio(TipoDominioEnum tipo, String nome) {

		Dominio dominio = dominioNegocio.obterPorAtributo("descricao", nome);

		if (!AjudanteObjeto.eReferencia(dominio)) {
			dominio = new Dominio();
			dominio.setDescricao(nome);
			dominio.setNome(tipo);
			dominio.setCodigo(dominioNegocio.obterUltimoCodigoDominio(tipo) + 1L);
			dominio.setAtivo(true);
			dominioNegocio.salvar(dominio);
		}

		return dominio;
	}

	private LocalizacaoEvento montarLocalizacaoEvento(String descricao) {

		LocalizacaoEvento localizacao = localizacaoNegocio.obterPorAtributo("descricao", descricao);

		return localizacao;

	}

	private ProvedorEvento montarProvedorEvento(String cnpj) {

		ProvedorEvento provedor = provedorNegocio.obterPorAtributo("cnpj", cnpj);

		return provedor;

	}

	private Participante montarResponsavelEvento(String email) {

		Pessoa pessoa = pessoaNegocio.obterPorAtributo("email", email);
		Participante participante = participanteNegocio.obterPorAtributo("pessoa", pessoa);

		return participante;

	}

	private Participante obterParticipante(String email) {

		Participante participante = participanteNegocio.obterPorAtributo("pessoa.email", email);

		return participante;
	}

	private Inscricao montarInscricao(Evento evento, Participante participante) {

		Inscricao inscricao = new Inscricao();
		inscricao.setEvento(evento);
		inscricao.setParticipante(participante);
		inscricao.setAutorizada(true);

		return inscricao;

	}

	private Modulo obterModuloUnico(Evento evento) {
		return moduloNegocio.obterPorAtributo("evento", evento);
	}

	private Nota montarNota(Participante participante, Modulo modulo, BigDecimal valorNota) {

		Nota nota = new Nota();
		nota.setParticipante(participante);
		nota.setValor(valorNota);
		nota.setModulo(modulo);

		return nota;
	}

	private Frequencia montarFrequencia(Participante participante, Modulo modulo, Boolean frequenciaSatisfatoria) {

		Frequencia frequencia = new Frequencia();
		frequencia.setParticipante(participante);
		frequencia.setEncontro(1);
		frequencia.setPresenca(frequenciaSatisfatoria);
		frequencia.setModulo(modulo);

		return frequencia;
	}

	private Inscricao salvarInscricao(Evento evento, String emailParticipante) {

		if (AjudanteObjeto.eReferencia(evento) && !AjudanteObjeto.eVazia(emailParticipante)) {

			Participante participante = obterParticipante(emailParticipante);

			if (AjudanteObjeto.eReferencia(participante)) {

				Inscricao inscricao = montarInscricao(evento, participante);
				inscricaoNegocio.salvar(inscricao);
				resultado.iterarInscricoesImportadas();

				return inscricao;

			}

		}

		return null;

	}

	private Nota salvarNota(Inscricao inscricao, BigDecimal valorNota) {

		if (AjudanteObjeto.eReferencia(inscricao) && AjudanteObjeto.eReferencia(inscricao.getEvento())
				&& AjudanteObjeto.eReferencia(inscricao.getParticipante()) && AjudanteObjeto.eReferencia(valorNota)) {

			Modulo modulo = obterModuloUnico(inscricao.getEvento());

			if (AjudanteObjeto.eReferencia(modulo)) {

				Nota nota = montarNota(inscricao.getParticipante(), modulo, valorNota.setScale(2, BigDecimal.ROUND_HALF_EVEN));
				notaNegocio.salvar(nota);
				resultado.iterarNotasLancadas();

				return nota;

			}

		}

		return null;

	}

	private Frequencia salvarFrequencia(Inscricao inscricao, BigDecimal valorFrequencia) {

		if (AjudanteObjeto.eReferencia(inscricao) && AjudanteObjeto.eReferencia(inscricao.getEvento())
				&& AjudanteObjeto.eReferencia(inscricao.getParticipante())
				&& AjudanteObjeto.eReferencia(valorFrequencia)) {

			Modulo modulo = obterModuloUnico(inscricao.getEvento());

			if (AjudanteObjeto.eReferencia(modulo)) {

				Boolean frequenciaSatisfatoria = valorFrequencia.compareTo(inscricao.getEvento().getMediaFrequenciaAprovacao()) >= 0 ? true : false;
				Frequencia frequencia = montarFrequencia(inscricao.getParticipante(), modulo, frequenciaSatisfatoria);
				frequenciaNegocio.salvar(frequencia);
				resultado.iterarFrequenciasLancadas();

				return frequencia;

			}

		}

		return null;

	}

	private Evento obterEventoJaCadastrado(EventoImportado evento) {

		List<Evento> listaResultadoBusca = eventoNegocio.listaPorAtributo("titulo", evento.getTitulo());

		if (AjudanteObjeto.eReferencia(listaResultadoBusca) && listaResultadoBusca.size() > 0) {

			try {

				for (Evento eventoResultadoBusca : listaResultadoBusca) {

					if (eventoResultadoBusca.getEventoImportado()
							&& eventoResultadoBusca.getDataInicioPrevisto()
							.compareTo(simpleDateFormat.parse(evento.getDataInicioPrevista())) == 0
							&& eventoResultadoBusca.getDataFimPrevisto()
							.compareTo(simpleDateFormat.parse(evento.getDataFimPrevista())) == 0
							&& eventoResultadoBusca.getDataInicioRealizacao()
							.compareTo(simpleDateFormat.parse(evento.getDataInicioRealizacao())) == 0
							&& eventoResultadoBusca.getDataFimRealizacao()
							.compareTo(simpleDateFormat.parse(evento.getDataFimRealizacao())) == 0) {

						return eventoResultadoBusca;

					}

				}

			} catch (ParseException e) {
				e.printStackTrace();
			}

			return null;

		} else {
			return null;
		}

	}

	private Inscricao obterInscricaoJaRealizada(Evento evento, String emailParticipante) {

		Participante participante = obterParticipante(emailParticipante);

		List<FiltroDTO> filtros = new ArrayList<FiltroDTO>();
		FiltroDTO filtroEvento = new FiltroDTO("evento", evento, ComparadorCriteriaEnum.EQ);
		filtroEvento.setTipoLogicoEnum(TipoLogicoEnum.AND);
		FiltroDTO filtroParticipante = new FiltroDTO("participante", participante, ComparadorCriteriaEnum.EQ);
		filtroParticipante.setTipoLogicoEnum(TipoLogicoEnum.AND);
		filtros.add(filtroEvento);
		filtros.add(filtroParticipante);

		List<Inscricao> inscricoes = this.inscricaoNegocio.listaPorFiltros(filtros);

		if (AjudanteObjeto.eReferencia(inscricoes) && inscricoes.size() > 0) {
			return inscricoes.get(0);
		} else {
			return null;
		}


	}

	private boolean isFrequenciaJaLancada(Evento evento, String emailParticipante) {

		Participante participante = obterParticipante(emailParticipante);
		Modulo modulo = obterModuloUnico(evento);

		BigDecimal frequencia = frequenciaNegocio.obterFrequenciaModulo(modulo, participante);

		if (AjudanteObjeto.eReferencia(frequencia) && frequencia.compareTo(BigDecimal.valueOf(0)) > 0) {
			return true;
		} else {
			return false;
		}

	}

	private boolean isNotaJaLancada(Evento evento, String emailParticipante) {

		Participante participante = obterParticipante(emailParticipante);
		Modulo modulo = obterModuloUnico(evento);

		Nota nota = notaNegocio.obterNotaParticipanteModulo(modulo, participante);

		if (AjudanteObjeto.eReferencia(nota)) {
			return true;
		} else {
			return false;
		}

	}

	private void gerarCertificados(Evento evento) {

		if (AjudanteObjeto.eReferencia(evento)) {

			List<Inscricao> inscricoes = inscricaoNegocio.listarInscricoesAutorizadas(evento);

			for (Inscricao inscricao : inscricoes) {

				try {

					Certificado certificado = null;

					if (AjudanteObjeto.eReferencia(inscricao.getParticipante())) {

						if (!certificadoNegocio.isJaTemCertificadoParticipante(evento, inscricao.getParticipante())) {
							certificado = certificadoNegocio.obterCertificado(evento, inscricao.getParticipante(), true);
						}
					}

					if (AjudanteObjeto.eReferencia(certificado)) {
						resultado.iterarCertificadosGerados();
					}

				} catch (IOException e) {
					e.printStackTrace();
				}

			}

		}

	}

}
