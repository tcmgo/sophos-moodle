/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.entidade.orcafi.Municipio;
import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.ajudante.AjudanteString;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;

import javax.persistence.*;

@Entity
@Table(name = "endereco")
@Banco(nome = Banco.SOPHOS)
public class Endereco extends EntidadePadrao {

	private static final long serialVersionUID = 4219692834878224822L;

	@Column(name = "bairro", length = 100)
	private String bairro;

	@Column(name = "cep", length = 9)
	private String cep;

	@Column(name = "complemento")
	private String complemento;

	@Column(name = "logradouro")
	private String logradouro;

	@Column(name = "numero", length = 10)
	private String numero;

	@JoinColumn(name = "cidade_id", referencedColumnName = "id")
	@ManyToOne
	private Cidade cidade;

	@Column(name = "municipio_id")
	private Long municipioId;
	
	@Transient
	private Municipio municipio;

	@Override
	public String toString() {

		String enderecoString = "";

		String logradouro = this.logradouro;

		String numero = this.numero;

		String complemento = this.complemento;

		String bairro = this.bairro;

		if(!AjudanteString.verificarStringNulaOuVazia(logradouro)) {

			enderecoString += logradouro;
		}
		if(!AjudanteString.verificarStringNulaOuVazia(numero)) {

			if(!AjudanteString.verificarStringNulaOuVazia(enderecoString)) {

				enderecoString += ", ";
			}

			enderecoString += numero;
		}
		if(!AjudanteString.verificarStringNulaOuVazia(complemento)) {

			if(!AjudanteString.verificarStringNulaOuVazia(enderecoString)) {

				enderecoString += ", ";
			}

			enderecoString += complemento;
		}
		if(!AjudanteString.verificarStringNulaOuVazia(bairro)) {

			if(!AjudanteString.verificarStringNulaOuVazia(enderecoString)) {

				enderecoString += " - ";
			}

			enderecoString += bairro;
		}
		if(AjudanteObjeto.eReferencia(this.getCidade())) {

			if(!AjudanteString.verificarStringNulaOuVazia(enderecoString)) {

				enderecoString += " - ";
			}

			enderecoString += this.getCidade().getDescricao();

		} else if(AjudanteObjeto.eReferencia(this.getMunicipio())) {

			if(!AjudanteString.verificarStringNulaOuVazia(enderecoString)) {

				enderecoString += " - ";
			}

			enderecoString += this.getMunicipio().getDescricao();
		}

		return enderecoString;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Long getMunicipioId() {
		return municipioId;
	}

	public void setMunicipioId(Long municipioId) {
		this.municipioId = municipioId;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
}
