/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.persistencia.impl;

import br.gov.go.tcm.sophos.entidade.Questionario;
import br.gov.go.tcm.sophos.persistencia.QuestionarioDAO;
import br.gov.go.tcm.sophos.persistencia.base.DAO;
import br.gov.go.tcm.sophos.persistencia.impl.base.DAOGenerico;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class QuestionarioDAOImpl extends DAOGenerico<Questionario> implements QuestionarioDAO {

	private static final long serialVersionUID = 294560128456827146L;

	@Autowired
	public QuestionarioDAOImpl(@Qualifier(DAO.SESSION_FACTORY_SOPHOS) final SessionFactory factory, @Qualifier(DAO.HIBERNATE_TEMPLATE_SOPHOS) final HibernateTemplate hibernateTemplate) {

		this.setHibernateTemplate(hibernateTemplate);

		this.setSessionFactory(factory);
		
	}
	
	@Override
	public boolean existeOpcaoDoQuestionarioVinculadoAAvaliacoesRealizadas(Long id) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);

		final Criteria criteria = session.createCriteria(Questionario.class);

		criteria.createAlias("opcoes", "opcao");

		criteria.createAlias("opcao.avaliacoes", "avaliacao");

		criteria.add(Restrictions.idEq(id));

		criteria.setProjection(Projections.countDistinct("avaliacao.id"));

		try {

			return ( (Number) criteria.uniqueResult() ).intValue() > 0;

		} finally {

			session.close();
		}
	}
	
	@Override
	public void ativarQuestionario(Questionario questionario) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);

		Query updateQuestionario = session.createQuery("UPDATE Questionario q SET q.ativo = :ativo WHERE q.id = :id");

		try {

			updateQuestionario.setParameter("ativo", questionario.getAtivo());

			updateQuestionario.setParameter("id", questionario.getId());

			updateQuestionario.executeUpdate();

		} finally {

			session.close();
		}
	}
}
