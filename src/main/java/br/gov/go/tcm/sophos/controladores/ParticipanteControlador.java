/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteData;
import br.gov.go.tcm.estrutura.entidade.orcafi.Municipio;
import br.gov.go.tcm.estrutura.entidade.orcafi.Orgao;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.*;
import br.gov.go.tcm.sophos.entidade.enumeracao.PaginaEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoDominioEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoPublicoAlvo;
import br.gov.go.tcm.sophos.lazy.TableLazyParticipante;
import br.gov.go.tcm.sophos.negocio.*;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Component
@Scope("session")
public class ParticipanteControlador extends ControladorBaseCRUD<Participante, ParticipanteNegocio> {

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private ParticipanteNegocio negocio;

	@Autowired
	private TableLazyParticipante lista;

	@Autowired
	private NavegacaoControlador navegacaoControlador;

	@Autowired
	private CidadeNegocio cidadeNegocio;

	@Autowired
	private EstadoNegocio estadoNegocio;

	@Autowired
	private DominioNegocio dominioNegocio;

	@Autowired
	private UsuarioNegocio usuarioNegocio;

	private Estado estado;

	private List<Estado> estados;

	private List<Cidade> cidades;

	private List<Municipio> municipioList;

	private List<Dominio> sexoList;

	private List<Dominio> formacaoAcademicaList;

	private List<Dominio> nivelEscolaridadeList;

	private List<Dominio> tipoPublicAlvoList;

	private List<Orgao> orgaosList;

	private List<Dominio> cargosList;

	private boolean outroCargo;

	private Dominio dominioCargo;

	private List<String> lotacaoList;

	private List<Dominio> tiposNecessidadesEspeciaisList;

	private List<Dominio> mandatosList;

	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	
	@PostConstruct
	public void configurarMascaraData() {
		this.executarJS("defineMascaras()");
	}

	@Override
	protected ParticipanteNegocio getNegocio() {

		return this.negocio;
	}

	public void listarCidades() {

		if (AjudanteObjeto.eReferencia(estado)) {

			this.cidades = this.cidadeNegocio.listaPorAtributo("estado", estado, SortOrder.ASCENDING, "descricao");

			if (estado.getUf().equalsIgnoreCase("GO")) {

				this.municipioList = this.cidadeNegocio.listaMunicipios();
				Collections.sort(this.municipioList);
			}

		}
	}

	public void listarOrgaos() {

		if (AjudanteObjeto.eReferencia(this.getEntidade().getEndereco().getMunicipioId())) {

			this.orgaosList = this.getNegocio().listarOrgaosAtivos(this.getEntidade().getEndereco().getMunicipioId());

		}
	}

	public void verificaExistenciaCPF() {

		if (!StringUtils.isEmpty(this.getEntidade().getPessoa().getCpf())) {

			// Participante é servidor do TCM
			if(this.getNegocio().PreecherParticipanteBaseRH(this.getEntidade())) {
					
				if (AjudanteObjeto.eReferencia(this.getEntidade().getTipo())
						&& (this.getEntidade().getTipo().equals(TipoDominioEnum.TipoPublicoAlvo.name(), 3L)
								|| this.getEntidade().getTipo().equals(TipoDominioEnum.TipoPublicoAlvo.name(), 2L))) {
	
					this.setEstado(this.estadoNegocio.obterPorAtributo("uf", "GO"));
				}
								
			} else {
				// Participante não é servidor do TCM, é usuário externo
				this.getNegocio().PreecherParticipanteBaseSOPHOS(this.getEntidade());
				
				this.setEstado(this.estadoNegocio.obterPorAtributo("uf", "GO"));
			}

			this.listarCidades();
		}

	}

	public void selecionarTipoParticipante() {

		if (this.getEntidade().getTipo().getCodigo().equals(TipoPublicoAlvo.JURISDICIONADO.getCodigo())) {

			this.selecionarEstado("GO");
			this.limparMunicipio();

		} else if (this.getEntidade().getTipo().getCodigo().equals(TipoPublicoAlvo.SOCIEDADE.getCodigo())) {

			this.limparEstado();
			this.limparMunicipio();

		} else if (this.getEntidade().getTipo().getCodigo().equals(TipoPublicoAlvo.INTERNO_TCM.getCodigo())) {

			this.selecionarEstado("GO");
			this.selecionarMunicipioGoiania();

		}
	}

	private void selecionarEstado(String uf) {
		this.estado = this.estadoNegocio.obterPorAtributo("uf", uf.toUpperCase());
		this.listarCidades();
	}

	private void selecionarMunicipioGoiania() {
		this.getEntidade().getEndereco().setMunicipioId(89L);
	}

	private void limparEstado() {
		this.estado = null;
	}

	private void limparMunicipio() {
		this.getEntidade().getEndereco().setCidade(null);
		this.getEntidade().getEndereco().setMunicipioId(null);
		this.orgaosList = null;
		this.getEntidade().setCodigoOrgao(null);
		this.getEntidade().setCodigoMunicipio(null);
	}

	public void acaoAbrirTela(Participante entidade, Boolean visualizar) {

		if (AjudanteObjeto.eReferencia(entidade)) {

			if (entidade.getTipo().getCodigo().equals(2L) || entidade.getTipo().getCodigo().equals(4L)) {

				if(AjudanteObjeto.eReferencia(entidade.getEndereco().getCidade())) {

					this.estado = entidade.getEndereco().getCidade().getEstado();
				}
				if(!AjudanteObjeto.eReferencia(this.estado)) {

					this.estado = this.estadoNegocio.obterPorAtributo("uf", "GO");
				}
			} else {

				this.estado = this.estadoNegocio.obterPorAtributo("uf", "GO");
			}

			this.listarCidades();

			this.setEntidade(entidade);

			entidade.setEndereco(
					AjudanteObjeto.eReferencia(entidade.getEndereco()) ? entidade.getEndereco() : new Endereco());

		} else {

			this.criarNovaInstanciaEntidade();
		}

		this.setVisualizar(visualizar);

		this.navegacaoControlador.processarNavegacao(PaginaEnum.PARTICIPANTES_CADASTRO);

		if (this.getEntidade().getCargo() != null && !this.getEntidade().getCargo().equals("")) {

			Dominio cargo = this.dominioNegocio.obterPorAtributo("descricao", this.getEntidade().getCargo());
			if (cargo != null) {

				this.setOutroCargo(false);
				this.setDominioCargo(cargo);

			} else {

				this.setOutroCargo(true);
				this.setDominioCargo(this.dominioNegocio.obterDominio(TipoDominioEnum.NomeCargo, 24L));
			}

		}

	}

	@Override
	protected void criarNovaInstanciaEntidade() {

		this.setEntidade(new Participante());

		this.getEntidade().setDataCadastro(new Date());

		this.getEntidade().setEndereco(new Endereco());

		this.getEntidade().setPessoa(new Pessoa());

		this.estado = new Estado();

	}

	@Override
	protected void executarAposInserirSucesso() {
		this.navegacaoControlador.processarNavegacao(PaginaEnum.PARTICIPANTES_CADASTRO_LIST);
	}

	@Override
	public TableLazyParticipante getLista() {

		return this.lista;
	}

	public void verificaMatricula() {

		if (!StringUtils.isEmpty(this.getEntidade().getMatricula())) {

			this.getEntidade().setMatricula(this.getEntidade().getMatricula().replace("_", ""));

			if (this.getEntidade().getMatricula().length() == 5) {
				this.getEntidade().setMatricula("0".concat(this.getEntidade().getMatricula()));
			}

			if (!usuarioNegocio.isExisteMatriculaBaseUsuariosInterno(this.getEntidade().getMatricula())) {
				this.getEntidade().setMatricula("");
				this.adicionarMensagemDeAlerta(this.obterMensagemPelaChave("matricula_nao_encontrada"));
			}

		}

	}

	public List<Estado> getEstados() {

		if (!AjudanteObjeto.eReferencia(estados)) {

			this.estados = this.estadoNegocio.listar();
		}

		return estados;
	}

	public List<Dominio> getFormacaoAcademicaList() {

		if (!AjudanteObjeto.eReferencia(formacaoAcademicaList)) {

			this.formacaoAcademicaList = this.dominioNegocio.listaPorAtributo("nome",
					TipoDominioEnum.FormacaoAcademica);
		}

		return formacaoAcademicaList;
	}

	public List<Dominio> getNivelEscolaridadeList() {

		if (!AjudanteObjeto.eReferencia(nivelEscolaridadeList)) {

			this.nivelEscolaridadeList = this.dominioNegocio.listaPorAtributo("nome",
					TipoDominioEnum.NivelEscolaridade);
		}

		return nivelEscolaridadeList;
	}

	public List<Dominio> getSexoList() {

		if (!AjudanteObjeto.eReferencia(sexoList)) {

			this.sexoList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.TipoSexo);
		}

		return sexoList;
	}

	public List<Dominio> getTipoPublicAlvoList() {

		if (!AjudanteObjeto.eReferencia(tipoPublicAlvoList)) {

			this.tipoPublicAlvoList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.TipoPublicoAlvo);
		}

		return tipoPublicAlvoList;
	}

	@Override
	public void acaoInserir() {

		if (!AjudanteData.dataEstaEntreOPeriodo(this.getEntidade().getPessoa().getDataNascimento(),
				this.getDataMinima(), this.getDataMaxima())) {

			this.adicionarMensagemInformativa(
					"A data de nascimento deve estar entre " + dateFormat.format(this.getDataMinima()) + " e "
							+ dateFormat.format(this.getDataMaxima()) + ". Verifique!");

		} else if (AjudanteObjeto.eReferencia(this.getEntidade().getDataAdmissao())
				&& !AjudanteData.dataEstaEntreOPeriodo(this.getEntidade().getDataAdmissao(), this.getDataMinima(),
						this.getDataMaxima())) {

			this.adicionarMensagemInformativa(
					"A data de admissão no cargo deve estar entre " + dateFormat.format(this.getDataMinima()) + " e "
							+ dateFormat.format(this.getDataMaxima()) + ". Verifique!");

		} else {

			this.getEntidade().getPessoa().setNome(this.getEntidade().getPessoa().getNome().toUpperCase());
			super.acaoInserir();

		}

	};

	public List<Cidade> getCidades() {

		return cidades;
	}

	public List<Orgao> getOrgaosList() {

		if (!AjudanteObjeto.eReferencia(this.orgaosList)) {
			this.listarOrgaos();
		}

		return orgaosList;
	}

	public List<Municipio> getMunicipioList() {

		return municipioList;
	}

	public Estado getEstado() {

		return estado;
	}

	public void setEstado(Estado estado) {

		this.estado = estado;
	}

	public List<Dominio> getCargosList() {

		if (!AjudanteObjeto.eReferencia(cargosList)) {

			this.cargosList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.NomeCargo, SortOrder.ASCENDING, "descricao");

		}

		return cargosList;
	}

	public void setCargosList(List<Dominio> cargosList) {
		this.cargosList = cargosList;
	}

	public boolean isOutroCargo() {
		return outroCargo;
	}

	public void setOutroCargo(boolean outroCargo) {
		this.outroCargo = outroCargo;
	}

	public Dominio getDominioCargo() {
		return dominioCargo;
	}

	public void setDominioCargo(Dominio dominioCargo) {
		this.dominioCargo = dominioCargo;
	}

	public void verificaDominioCargo() {

		if (this.dominioCargo.equals(this.dominioNegocio.obterDominio(TipoDominioEnum.NomeCargo, 24L))) {

			this.setOutroCargo(true);
			this.getEntidade().setCargo("");

		} else {

			this.getEntidade().setCargo(this.dominioCargo.getDescricao());
			this.setOutroCargo(false);
		}

	}

	public List<String> getLotacaoList() {
		if (!AjudanteObjeto.eReferencia(lotacaoList)) {
			lotacaoList = this.getNegocio().listarLotacoes();
		}
		return lotacaoList;
	}

	public void setLotacaoList(List<String> lotacaoList) {
		this.lotacaoList = lotacaoList;
	}

	public String obterDescricaoMunicipio(Endereco endereco) {
		
		if (endereco.getMunicipioId() != null) {
			
			return negocio.obterDescricaoMunicipio(endereco.getMunicipioId());
			
		} else if (endereco.getCidade() != null) {
			
			return endereco.getCidade().getDescricao();
		}
		return "";
	}

	public String obterDescricaoCidade(Endereco endereco) {

		if (endereco.getCidade() != null) {

			return endereco.getCidade().getDescricao();

		} else if (endereco.getMunicipioId() != null) {

			return negocio.obterDescricaoMunicipio(endereco.getMunicipioId());
		}
		return "";
	}

	public List<Dominio> getTiposNecessidadesEspeciaisList() {

		if (!AjudanteObjeto.eReferencia(tiposNecessidadesEspeciaisList)) {

			this.tiposNecessidadesEspeciaisList = this.dominioNegocio.listaPorAtributo("nome",
					TipoDominioEnum.TipoNecessidadeEspecial, SortOrder.ASCENDING, "codigo");
		}

		return tiposNecessidadesEspeciaisList;
	}

	public void setTiposNecessidadesEspeciaisList(List<Dominio> tiposNecessidadesEspeciaisList) {
		this.tiposNecessidadesEspeciaisList = tiposNecessidadesEspeciaisList;
	}

	public List<Dominio> getMandatosList() {

		if (!AjudanteObjeto.eReferencia(mandatosList)) {

			this.mandatosList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.Mandato,
					SortOrder.ASCENDING, "codigo");
		}

		return mandatosList;
	}

	public void setMandatosList(List<Dominio> mandatosList) {
		this.mandatosList = mandatosList;
	}

}
