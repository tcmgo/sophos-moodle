/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.*;
import br.gov.go.tcm.sophos.entidade.enumeracao.PaginaEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoAlertaEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoDominioEnum;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.lazy.TableLazyAlerta;
import br.gov.go.tcm.sophos.negocio.AlertaNegocio;
import br.gov.go.tcm.sophos.negocio.DominioNegocio;
import br.gov.go.tcm.sophos.negocio.PacoteAlertasNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
//@Scope("view")
@Scope("session")
public class AlertaControlador extends ControladorBaseCRUD<Alerta, AlertaNegocio> {

	private static final long serialVersionUID = 370915393940283176L;

	@Autowired
	private AlertaNegocio negocio;

	@Autowired
	private TableLazyAlerta lista;
	
	@Autowired
	private DominioNegocio dominioNegocio;

	@Autowired
	private NavegacaoControlador navegacaoControlador;
	
	@Autowired
	private PacoteAlertasNegocio pacoteAlertasNegocio;

	private List<Dominio> tipoAlertasList;
	
	private Boolean inserirPacote = Boolean.FALSE;
	
	private PacoteAlertas pacoteAlertas;
	
	private List<Alerta> alertasDoUsuario;
	
	private List<Alerta> alertasList;

	public void acaoAbrirTela(Alerta entidade, Boolean visualizar) {

		if (AjudanteObjeto.eReferencia(entidade)) {
			
			if(AjudanteObjeto.eReferencia(entidade.getPacoteAlertas())){
				
				this.inserirPacote = Boolean.TRUE;
				
				pacoteAlertas = entidade.getPacoteAlertas();
				
			}else{
				
				this.inserirPacote = Boolean.FALSE;
			}

			this.setEntidade(entidade);

		} else {

			this.criarNovaInstanciaEntidade();
		}

		this.setVisualizar(visualizar);

		this.navegacaoControlador.processarNavegacao(PaginaEnum.CADASTRO_ALERTAS);

	}
	
	public void geraMensagemPadrao() {
		
		Dominio tipo;
		Dominio tipoAlertaAvaliacaoReacao;
		String mensagem = "";
		
		tipoAlertaAvaliacaoReacao = dominioNegocio.obterDominio(TipoDominioEnum.TipoAlertas, TipoAlertaEnum.AVALIACAO_REACAO.getCodigo());
		
		if (inserirPacote) {
			
			if (!AjudanteObjeto.eReferencia(this.getPacoteAlertas()) || !AjudanteObjeto.eReferencia(this.getPacoteAlertas().getTipo())) {
				return;
			}
			
			tipo = this.getPacoteAlertas().getTipo();
			
			if (tipo.getCodigo().equals(tipoAlertaAvaliacaoReacao.getCodigo()) && AjudanteObjeto.eReferencia(this.getPacoteAlertas().getEvento().getTitulo()) ) {
				
				mensagem = "Prezado, não se esqueça de preencher a avaliação de reação do evento/curso " + this.getPacoteAlertas().getEvento().getTitulo() +
						   ". O preenchimento é requisito para emissão do certificado. Escola de Contas";
				
				this.getPacoteAlertas().setMensagem(mensagem);
				
			} else {
				
				this.getPacoteAlertas().setMensagem("");
			}
			
		} else {
			
			if (!AjudanteObjeto.eReferencia(this.getEntidade()) || !AjudanteObjeto.eReferencia(this.getEntidade().getTipo())) {
				return;
			}
			
			tipo = this.getEntidade().getTipo();
			
			if (tipo.getCodigo().equals(tipoAlertaAvaliacaoReacao.getCodigo()) && AjudanteObjeto.eReferencia(this.getEntidade().getParticipante().getPessoa().getNome())) {
				
				mensagem = "Prezado " + this.getEntidade().getParticipante().getPessoa().getNome()  + ", não se esqueça de preencher a avaliação de reação dos eventos/cursos " +
				           "que você participou. O preenchimento é requisito para emissão dos certificados. Escola de Contas";
				
				this.getEntidade().setMensagem(mensagem);
				
			} else {
				
				this.getEntidade().setMensagem("");
			}
			
		}
		
	}
	
	public void acaoVisualizarAlerta(Alerta entidade){
		
		if(AjudanteObjeto.eReferencia(entidade.getPacoteAlertas())){
			
			this.inserirPacote = Boolean.TRUE;
			
			pacoteAlertas = entidade.getPacoteAlertas();
			
		}else{
			
			this.inserirPacote = Boolean.FALSE;
		}

		this.getNegocio().visualizarAlerta(entidade);
		
		this.atualizarAlertasDoUsuario();
		
		this.setEntidade(entidade);
		
		this.setVisualizar(Boolean.TRUE);

		this.navegacaoControlador.processarNavegacao(PaginaEnum.CADASTRO_ALERTAS);
		
		this.executarJS("fecharModal('modalAlertas')");
	}
	
	@Override
	protected void criarNovaInstanciaEntidade() {

		super.criarNovaInstanciaEntidade();
		
		this.setPacoteAlertas(new PacoteAlertas());
		
		this.getPacoteAlertas().setEvento(new Evento());
		
		this.getEntidade().setParticipante(new Participante());
	}
	
	@Override
	public void acaoInserir() {

		if(this.getInserirPacote()){

			try {

				this.pacoteAlertasNegocio.salvar(this.getPacoteAlertas());

				this.adicionarMensagemInformativa(this.obterMensagemSucessoAoInserir());

				this.executarAposInserirSucesso();
				
				this.navegacaoControlador.processarNavegacao(PaginaEnum.ALERTAS);

			} catch (final ValidacaoException e) {

				this.adicionarMensagemExcecao(e);
			}
			
		}else{
			
			super.acaoInserir();
			
			this.navegacaoControlador.processarNavegacao(PaginaEnum.ALERTAS);
		}
		
		this.atualizarAlertasDoUsuario();
	}
	
	@Override
	public void acaoExcluir(Alerta entidade) {

		if(AjudanteObjeto.eReferencia(entidade.getPacoteAlertas())){
			
			try {

				this.pacoteAlertasNegocio.excluir(entidade.getPacoteAlertas());
				
				this.adicionarMensagemInformativa(this.obterMensagemSucessoAoExcluir());

			} catch (final ValidacaoException e) {

				this.adicionarMensagemExcecao(e);
			}
			
		}else{
			
			super.acaoExcluir(entidade);
		}
		
		this.atualizarAlertasDoUsuario();
	}

	public void atualizarAlertasDoUsuario(){
		
		if(AjudanteObjeto.eReferencia(this.getUsuarioLogado()) && AjudanteObjeto.eReferencia(this.getUsuarioLogado().getParticipante())){
			
			alertasDoUsuario = this.getNegocio().listaAlertaNaoVisualizados(this.getUsuarioLogado().getParticipante());
			
		}
	}
	
	@Override
	protected AlertaNegocio getNegocio() {

		return this.negocio;
	}

	@Override
	public TableLazyAlerta getLista() {
		return this.lista;
	}

	public List<Dominio> getTipoAlertasList() {
		
		if (!AjudanteObjeto.eReferencia(tipoAlertasList)) {

			this.tipoAlertasList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.TipoAlertas);
		}

		return tipoAlertasList;
	}

	public void setTipoAlertasList(List<Dominio> tipoAlertasList) {
		this.tipoAlertasList = tipoAlertasList;
	}

	public DominioNegocio getDominioNegocio() {
		return dominioNegocio;
	}

	public Boolean getInserirPacote() {
		return inserirPacote;
	}

	public void setInserirPacote(Boolean inserirPacote) {
		this.inserirPacote = inserirPacote;
	}

	public PacoteAlertas getPacoteAlertas() {
		return pacoteAlertas;
	}

	public void setPacoteAlertas(PacoteAlertas pacoteAlertas) {
		this.pacoteAlertas = pacoteAlertas;
	}

	public List<Alerta> getAlertasList() {
		if(alertasList == null && AjudanteObjeto.eReferencia(this.getUsuarioLogado()) && AjudanteObjeto.eReferencia(this.getUsuarioLogado().getParticipante())){
			alertasList = this.getNegocio().listaPorAtributo("participante", this.getUsuarioLogado().getParticipante());
		}
		return alertasList;
	}

	public void setAlertasList(List<Alerta> alertasList) {
		this.alertasList = alertasList;
	}

	public List<Alerta> getAlertasDoUsuario() {
		if(alertasDoUsuario == null){
			this.atualizarAlertasDoUsuario();
		}
		return alertasDoUsuario;
	}

	public void setAlertasDoUsuario(List<Alerta> alertasDoUsuario) {
		this.alertasDoUsuario = alertasDoUsuario;
	}

}
