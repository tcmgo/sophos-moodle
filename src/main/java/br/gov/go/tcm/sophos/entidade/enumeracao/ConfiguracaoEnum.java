/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade.enumeracao;

import java.util.Arrays;

public enum ConfiguracaoEnum {

	IMG_CERTIFICADO_FRENTE("Imagem de frente do certificado", 1L), 
	IMG_CERTIFICADO_VERSO("Imagem do verso do certificado", 2L),
	IMG_TOPO_RELATORIO("Imagem do topo dos relatórios", 3L),
	TOPO_RELATORIO("Título do topo dos relatórios", 4L),
	ASSINATURA_1_RELATORIO("Assinatura relatório 1", 5L),
	CARGO_ASSINATURA_1_RELATORIO("Cargo assinatura relatório 1", 6L);
	
	private String descricao;
	
	private Long codigo;

	private ConfiguracaoEnum(String descricao, Long codigo) {
		this.descricao = descricao;
		this.codigo = codigo;
	}
	
	public static ConfiguracaoEnum obterPorCodigo(Long codigo){
		
		for(ConfiguracaoEnum configuracaoEnum : Arrays.asList(ConfiguracaoEnum.values())){
			
			if(configuracaoEnum.getCodigo().equals(codigo)){
				
				return configuracaoEnum;
			}
		}
		
		return null;
	}

	public String getDescricao() {
		return descricao;
	}

	public Long getCodigo() {
		return codigo;
	}
	
	
}
