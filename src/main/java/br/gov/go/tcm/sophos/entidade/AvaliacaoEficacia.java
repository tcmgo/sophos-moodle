/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;

import javax.persistence.*;

@Entity
@Table(name = "avaliacao_eficacia")
@Banco(nome = Banco.SOPHOS)
public class AvaliacaoEficacia extends EntidadePadrao {

	private static final long serialVersionUID = -7371565966573959196L;

	@Column(name = "melhoria_desempenho")
	private boolean melhoriaDesempenho;
	
	@JoinColumn(name = "desempenho_servico_id", referencedColumnName = "id")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Dominio desempenhoServico;

	@JoinColumn(name = "evento_id", referencedColumnName = "id")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Evento evento;

	@JoinColumn(name = "participante_id", referencedColumnName = "id")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Participante participante;

	@JoinColumn(name = "indicacao_id", referencedColumnName = "id")
	@OneToOne(fetch = FetchType.LAZY)
	private Indicacao indicacao;

	@Column(name = "observacao", length = 2000)
	private String observacao;

	
	public Dominio getDesempenhoServico() {
		return desempenhoServico;
	}

	public void setDesempenhoServico(Dominio desempenhoServico) {
		this.desempenhoServico = desempenhoServico;
	}

	public boolean isMelhoriaDesempenho() {
		return melhoriaDesempenho;
	}

	public void setMelhoriaDesempenho(boolean melhoriaDesempenho) {
		this.melhoriaDesempenho = melhoriaDesempenho;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public Participante getParticipante() {
		return participante;
	}

	public void setParticipante(Participante participante) {
		this.participante = participante;
	}

	public Indicacao getIndicacao() {
		return indicacao;
	}

	public void setIndicacao(Indicacao indicacao) {
		this.indicacao = indicacao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

}
