/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Frequencia;
import br.gov.go.tcm.sophos.entidade.Modulo;
import br.gov.go.tcm.sophos.entidade.Participante;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import br.gov.go.tcm.sophos.persistencia.FrequenciaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Service
public class FrequenciaNegocio extends NegocioBase<Frequencia> {

	private static final long serialVersionUID = -8426728536010364058L;

	@Autowired
	private FrequenciaDAO dao;
	
	@Autowired
	private ModuloNegocio moduloNegocio;

	@Override
	protected FrequenciaDAO getDAO() {

		return this.dao;
	}

	public void salvar(List<Frequencia> frequenciaList) {

		for(Frequencia frequencia : frequenciaList){
			
			Long frequenciaBancoId = this.getDAO().obterFrequenciaModuloParticipante(frequencia.getModulo(), frequencia.getParticipante(), frequencia.getEncontro());
			
			if(frequenciaBancoId != null) {
			
				frequencia.setId(frequenciaBancoId);
			}
			
			super.salvar(frequencia);
		}
	}

	public Boolean verificaAprovacaoFrequenciaModulo(Modulo modulo, Participante participante) {

		List<Frequencia> frequencias = this.getDAO().listarPresencasModuloParticipante(modulo, participante);
		
		if(frequencias.size() > 0){
			
			BigDecimal percentagemFrequencia = BigDecimal.valueOf(frequencias.size()).divide(BigDecimal.valueOf(modulo.getQuantidadeEncontros())).multiply(BigDecimal.valueOf(100));
			
			return modulo.getFrequenciaAprovacao().doubleValue() <= percentagemFrequencia.doubleValue();
		}
		
		return Boolean.FALSE;
	}

	public BigDecimal obterFrequenciaModulo(Modulo modulo, Participante participante) {
		
		List<Frequencia> frequencias = this.getDAO().listarPresencasModuloParticipante(modulo, participante);
		
		if(frequencias.size() > 0){			
												
			//BigDecimal percentagemFrequencia = BigDecimal.valueOf(frequencias.size()).divide(BigDecimal.valueOf(modulo.getQuantidadeEncontros())).multiply(BigDecimal.valueOf(100));
									
			BigDecimal percentagemFrequencia = BigDecimal.ZERO;
			BigDecimal divisao = BigDecimal.ZERO;						
			divisao = BigDecimal.valueOf(frequencias.size()) .divide(BigDecimal.valueOf(modulo.getQuantidadeEncontros()), 2, RoundingMode.HALF_UP );
			percentagemFrequencia = divisao.multiply(BigDecimal.valueOf(100));
					
			return percentagemFrequencia;
			
		}

		return BigDecimal.valueOf(0);
	}

	public List<Frequencia> obterFrequenciaModuloParticipante(Modulo modulo, Participante participante) {
		
		return this.getDAO().listarFrequenciasModuloParticipante(modulo, participante);
		
	}

	public BigDecimal obterFrequenciaParticipanteEvento(Participante participante, Evento evento, List<Modulo> modulos) {

		BigDecimal somatorio = BigDecimal.valueOf(0);
		
		if(!AjudanteObjeto.eReferencia(modulos)){
			
			modulos = this.moduloNegocio.listaPorAtributo("evento", evento);
		}
				
		for(Modulo modulo : modulos){
			
			somatorio = somatorio.add(this.obterFrequenciaModulo(modulo, participante));
		}
		
		return somatorio.divide(BigDecimal.valueOf(modulos.size()), 2, BigDecimal.ROUND_HALF_UP);
	}

	public List<Frequencia> listarFrequenciasModuloParticipante(Modulo modulo, Participante participante) {

		return this.getDAO().listarFrequenciasModuloParticipante(modulo, participante);
	}
}
