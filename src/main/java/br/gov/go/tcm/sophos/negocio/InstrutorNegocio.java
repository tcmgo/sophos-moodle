/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Instrutor;
import br.gov.go.tcm.sophos.entidade.ModuloInstrutor;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import br.gov.go.tcm.sophos.persistencia.InstrutorDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InstrutorNegocio extends NegocioBase<Instrutor> {

	private static final long serialVersionUID = -8426728536010364058L;

	@Autowired
	private InstrutorDAO dao;
	
	@Autowired
	private AnexoNegocio anexoNegocio;
	
	@Autowired
	private EnderecoNegocio enderecoNegocio;
	
	@Autowired
	private PessoaNegocio pessoaNegocio;
	
	@Autowired
	private ModuloInstrutorNegocio moduloInstrutorNegocio;

	@Override
	protected InstrutorDAO getDAO() {

		return this.dao;
	}

	@Override
	public void salvar(Instrutor entidade) {

		this.executarValidacaoAntesDeSalvar(entidade);
		
		Instrutor instrutorOld = null;
		
		if(entidade.getId() != null){
			
			instrutorOld = this.obter(entidade.getId());
		}
		
		if(AjudanteObjeto.eReferencia(entidade.getCurriculo())){
			
			this.anexoNegocio.salvar(entidade.getCurriculo());
		}
		
		if(AjudanteObjeto.eReferencia(entidade.getAssinatura())){
			
			this.anexoNegocio.salvar(entidade.getAssinatura());
		}

		this.enderecoNegocio.salvar(entidade.getEndereco());
		
		this.pessoaNegocio.salvar(entidade.getPessoa());
		
		super.salvar(entidade);
		
		if(instrutorOld != null){
			
			this.verificaMudancaAnexo(entidade, instrutorOld);
		}
	}

	private void verificaMudancaAnexo(Instrutor entidade, Instrutor instrutorOld) {
		
		if(AjudanteObjeto.eReferencia(entidade.getId())){
			
			if(AjudanteObjeto.eReferencia(instrutorOld.getCurriculo()) && (!AjudanteObjeto.eReferencia(entidade.getCurriculo()) || !AjudanteObjeto.eReferencia(entidade.getCurriculo().getArquivoGED()))){
				
				this.anexoNegocio.excluir(instrutorOld.getCurriculo());
			}
			
			if(AjudanteObjeto.eReferencia(instrutorOld.getAssinatura()) && (!AjudanteObjeto.eReferencia(entidade.getAssinatura()) || !AjudanteObjeto.eReferencia(entidade.getAssinatura().getArquivoGED()))){
				
				this.anexoNegocio.excluir(instrutorOld.getAssinatura());
			}

		}
	}
	
	@Override
	protected void executarValidacaoAntesDeExcluir(Instrutor entidade) {

		super.executarValidacaoAntesDeExcluir(entidade);
		
		List<ModuloInstrutor> moduloInstrutorList = this.moduloInstrutorNegocio.listaPorAtributo("instrutor", entidade);
		
		if(AjudanteObjeto.eReferencia(moduloInstrutorList) && moduloInstrutorList.size() > 0){
			
			throw new ValidacaoException("Não e possível a exclusão desse instrutor, pois está vinculado a algum evento.");
		}
	}

	public List<Instrutor> listarInstrutoresEvento(Evento evento) {
		
		List<Instrutor> result = new ArrayList<Instrutor>();
		
		List<ModuloInstrutor> moduloInstrutorList = this.moduloInstrutorNegocio.listarInstrutoresEvento(evento);
		
		for(ModuloInstrutor moduloInstrutor : moduloInstrutorList){
			
			if(!result.contains(moduloInstrutor.getInstrutor())){
				
				result.add(moduloInstrutor.getInstrutor());
			}
		}
		
		return result;
	}
}
