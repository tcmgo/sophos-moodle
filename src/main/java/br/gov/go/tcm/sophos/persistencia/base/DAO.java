/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.persistencia.base;

import br.gov.go.tcm.estrutura.persistencia.base.contrato.GenericoDAO;
import br.gov.go.tcm.sophos.entidade.dto.FiltroDTO;
import org.hibernate.criterion.MatchMode;
import org.primefaces.model.SortOrder;

import java.util.List;
import java.util.Map;

public interface DAO<E> extends GenericoDAO {

	/** Atributo SESSION_FACTORY_WEB. */
	String SESSION_FACTORY_SOPHOS = "sessionFactorySophos";

	/** Atributo HIBERNATE_TEMPLATE_WEB. */
	String HIBERNATE_TEMPLATE_SOPHOS = "hibernateTemplateSophos";
	
	E obterPorAtributo(String atributo, Object valor);
	
	List<E> listaPorAtributo(String atributo, Object valor);
	
	List<E> listaPorFiltros(List<FiltroDTO> filtros, String... alias);
	
	List<E> listaOrdenadoPorAtributo(String atributo, Object valor, SortOrder order, String atributoOrdenacao);
	
	List<E> listaPorAtributo(String atributo, String valor, MatchMode matchMode);
	
	Long obterQuantidade(Map<String, Object> filters, String consulta);

	List<E> listar(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters, String consulta);

}
