/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteData;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.*;
import br.gov.go.tcm.sophos.entidade.dto.TipoQuestionarioAvaliacaoDTO;
import br.gov.go.tcm.sophos.lazy.TableLazyAvaliacao;
import br.gov.go.tcm.sophos.negocio.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@Scope("view")
public class MinhasAvaliacoesControlador extends ControladorBaseCRUD<Avaliacao, AvaliacaoNegocio> {

	private static final String MODULO = "MÓDULO";

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private AvaliacaoNegocio negocio;

	@Autowired
	private QuestionarioNegocio questionarioNegocio;
	
	@Autowired
	private QuestionarioAvaliacaoNegocio questionarioAvaliacaoNegocio;

	@Autowired
	private OpcaoNegocio opcaoNegocio;

	@Autowired
	private ModuloInstrutorNegocio moduloInstrutorNegocio;

	@Autowired
	private TableLazyAvaliacao lista;

	private Modulo modulo;

	private List<TipoQuestionarioAvaliacaoDTO> mapQuestionarioAvaliacao;

	@Override
	public TableLazyAvaliacao getLista() {

		Map<String, Object> filters = new HashMap<String, Object>();

		if (AjudanteObjeto.eReferencia(this.getUsuarioLogado().getParticipante())) {

			filters.put("participante", this.getUsuarioLogado().getParticipante());

		} else {

			filters.put("participante", null);
		}

		this.lista.setFilters(filters);

		return this.lista;
	}

	public TipoQuestionarioAvaliacaoDTO obterQuestionarioAvaliacaoPorChave(String chave){
		
		for(TipoQuestionarioAvaliacaoDTO tipoQuestionarioAvaliacaoDTO : mapQuestionarioAvaliacao){
			
			if(tipoQuestionarioAvaliacaoDTO.getChave().equals(chave)){
				
				return tipoQuestionarioAvaliacaoDTO;
			}
		}
		
		return null;
	}
	
	public void selecionarAvaliacao(Avaliacao avaliacao){
		
		this.setVisualizar(Boolean.TRUE);
		
		this.setEntidade(avaliacao);
		
		this.setModulo(avaliacao.getModulo());
		
		List<QuestionarioAvaliacao> questionarioAvaliacaoList = questionarioAvaliacaoNegocio.listaPorAtributo("avaliacao", avaliacao);
		
		this.mapQuestionarioAvaliacao = new ArrayList<TipoQuestionarioAvaliacaoDTO>();
		
		for(QuestionarioAvaliacao questionarioAvaliacao : questionarioAvaliacaoList){
			
			questionarioAvaliacao.getQuestionario().setOpcoes(this.opcaoNegocio.listaPorAtributo("questionario", questionarioAvaliacao.getQuestionario()));
			
			if(AjudanteObjeto.eReferencia(questionarioAvaliacao.getInstrutor())){
				
				TipoQuestionarioAvaliacaoDTO  tipoQuestionarioAvaliacaoDTO = new TipoQuestionarioAvaliacaoDTO(questionarioAvaliacao.getInstrutor().getNome());
				
				if(!mapQuestionarioAvaliacao.contains(tipoQuestionarioAvaliacaoDTO)){
					
					mapQuestionarioAvaliacao.add(tipoQuestionarioAvaliacaoDTO);
					
				}
				
				this.obterQuestionarioAvaliacaoPorChave(questionarioAvaliacao.getInstrutor().getNome()).getQuestionarioAvaliacaoList().add(questionarioAvaliacao);
				
			}else{
				
				TipoQuestionarioAvaliacaoDTO  tipoQuestionarioAvaliacaoDTO = new TipoQuestionarioAvaliacaoDTO(MODULO);
				
				if(!mapQuestionarioAvaliacao.contains(tipoQuestionarioAvaliacaoDTO)){
					
					mapQuestionarioAvaliacao.add(tipoQuestionarioAvaliacaoDTO);
					
				}
					
				this.obterQuestionarioAvaliacaoPorChave(MODULO).getQuestionarioAvaliacaoList().add(questionarioAvaliacao);
			}
		}
		
		this.executarJS("openModal('modalAvaliacao')");
	}
	
	@Override
	public void acaoInserir() {

		this.getEntidade().setQuestionarioAvaliacaoList(new ArrayList<QuestionarioAvaliacao>());

		for (TipoQuestionarioAvaliacaoDTO tipoQuestionarioAvaliacaoDTO : mapQuestionarioAvaliacao) {

			this.getEntidade().getQuestionarioAvaliacaoList().addAll(tipoQuestionarioAvaliacaoDTO.getQuestionarioAvaliacaoList());
		}

		super.acaoInserir();
	}

	public List<Modulo> listaNaoRealizadas() {

		return this.getNegocio().listarModulosNaoAvaliados(this.getUsuarioLogado().getParticipante());
	}

	public void selecionarModulo(Modulo modulo) {

		this.setVisualizar(Boolean.FALSE);
		
		this.modulo = modulo;

		this.setEntidade(new Avaliacao());

		this.getEntidade().setQuestionarioAvaliacaoList(new ArrayList<QuestionarioAvaliacao>());

		this.getEntidade().setModulo(this.modulo);

		this.getEntidade().setParticipante(this.getUsuarioLogado().getParticipante());

		this.modulo.setInstrutorList(this.moduloInstrutorNegocio.listaPorAtributo("modulo", this.modulo));

		List<Questionario> questionarioList = this.questionarioNegocio.listaPorAtributo("ativo", Boolean.TRUE);

		this.mapQuestionarioAvaliacao = new ArrayList<TipoQuestionarioAvaliacaoDTO>();

		for (Questionario questionario : questionarioList) {

			if (questionario.getParaInstrutor()) {

				this.montarQuestionarioInstrutor(questionario);

			} else {

				this.montarQuestionarioModulo(questionario);

			}

		}

	}

	private void montarQuestionarioModulo(Questionario questionario) {

		QuestionarioAvaliacao questionarioAvaliacao = new QuestionarioAvaliacao();

		questionarioAvaliacao.setAvaliacao(this.getEntidade());

		questionarioAvaliacao.setDataCadastro(new Date());

		questionarioAvaliacao.setQuestionario(questionario);

		questionarioAvaliacao.getQuestionario().setOpcoes(this.opcaoNegocio.listaPorAtributo("questionario", questionario));

		TipoQuestionarioAvaliacaoDTO  tipoQuestionarioAvaliacaoDTO = new TipoQuestionarioAvaliacaoDTO(MODULO);
		
		if(!mapQuestionarioAvaliacao.contains(tipoQuestionarioAvaliacaoDTO)){
			
			mapQuestionarioAvaliacao.add(tipoQuestionarioAvaliacaoDTO);
			
		}
			
		this.obterQuestionarioAvaliacaoPorChave(MODULO).getQuestionarioAvaliacaoList().add(questionarioAvaliacao);
		
	}

	private void montarQuestionarioInstrutor(Questionario questionario) {

		for (ModuloInstrutor moduloInstrutor : this.modulo.getInstrutorList()) {

			QuestionarioAvaliacao questionarioAvaliacao = new QuestionarioAvaliacao();

			questionarioAvaliacao.setAvaliacao(this.getEntidade());

			questionarioAvaliacao.setDataCadastro(new Date());

			questionarioAvaliacao.setQuestionario(questionario);

			questionarioAvaliacao.setInstrutor(moduloInstrutor.getInstrutor());

			questionarioAvaliacao.getQuestionario().setOpcoes(this.opcaoNegocio.listaPorAtributo("questionario", questionario));

			TipoQuestionarioAvaliacaoDTO  tipoQuestionarioAvaliacaoDTO = new TipoQuestionarioAvaliacaoDTO(moduloInstrutor.getInstrutor().getNome());
			
			if(!mapQuestionarioAvaliacao.contains(tipoQuestionarioAvaliacaoDTO)){
				
				mapQuestionarioAvaliacao.add(tipoQuestionarioAvaliacaoDTO);
				
			}	
				
			this.obterQuestionarioAvaliacaoPorChave(moduloInstrutor.getInstrutor().getNome()).getQuestionarioAvaliacaoList().add(questionarioAvaliacao);
			
		}
	}
	
	public boolean verificaPermissaoAvaliar(Modulo modulo) {

		Date dataFinal;
		if (AjudanteObjeto.eReferencia(modulo.getEvento().getDataInicioRealizacao())) {
			dataFinal = modulo.getEvento().getDataFimRealizacao();
		} else {
			dataFinal = modulo.getEvento().getDataFimPrevisto();
		}

		if (AjudanteData.dataMaiorIgualDataAtual(dataFinal)) {
			return false;
		} else {
			return true;
		}

	}

	@Override
	protected AvaliacaoNegocio getNegocio() {

		return this.negocio;
	}

	public Modulo getModulo() {
		return modulo;
	}

	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}

	public List<TipoQuestionarioAvaliacaoDTO> getMapQuestionarioAvaliacao() {
		return mapQuestionarioAvaliacao;
	}

	public void setMapQuestionarioAvaliacao(List<TipoQuestionarioAvaliacaoDTO> mapQuestionarioAvaliacao) {
		this.mapQuestionarioAvaliacao = mapQuestionarioAvaliacao;
	}

}
