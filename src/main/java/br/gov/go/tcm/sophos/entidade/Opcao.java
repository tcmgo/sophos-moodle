/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.entidade.base.EntidadeLogica;

import javax.persistence.*;

@Entity
@Table(name = "opcao_questionario")
@Banco(nome = Banco.SOPHOS)
public class Opcao extends EntidadeLogica implements Comparable<Opcao> {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = 1413113467434961375L;

	@Column(name = "descricao")
	/** Atributo descricao. */
	private String descricao;

	@ManyToOne(optional = false)
	@JoinColumn(name = "questionario_id")
	/** Atributo questionario. */
	private Questionario questionario;

	@Column(name = "ordem")
	/** Atributo ordem. */
	private Integer ordem;

	/**
	 * Retorna o valor do atributo <code>descricao</code>
	 *
	 * @return <code>String</code>
	 */
	public String getDescricao() {

		return this.descricao;
	}

	/**
	 * Define o valor do atributo <code>descricao</code>.
	 *
	 * @param descricao
	 */
	public void setDescricao(final String descricao) {

		this.descricao = descricao;
	}

	/**
	 * Retorna o valor do atributo <code>questionario</code>
	 *
	 * @return <code>Questionario</code>
	 */
	public Questionario getQuestionario() {

		return this.questionario;
	}

	/**
	 * Define o valor do atributo <code>questionario</code>.
	 *
	 * @param questionario
	 */
	public void setQuestionario(final Questionario questionario) {

		this.questionario = questionario;
	}

	/**
	 * Retorna o valor do atributo <code>ordem</code>
	 *
	 * @return <code>Integer</code>
	 */
	public Integer getOrdem() {

		return this.ordem;
	}

	/**
	 * Define o valor do atributo <code>ordem</code>.
	 *
	 * @param ordem
	 */
	public void setOrdem(final Integer ordem) {

		this.ordem = ordem;
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ( ( this.descricao == null ) ? 0 : this.descricao.hashCode() );
		return result;
	}

	@Override
	public boolean equals(final Object obj) {

		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final Opcao other = (Opcao) obj;
		if (this.descricao == null) {
			if (other.descricao != null) {
				return false;
			}
		} else if (!this.descricao.equals(other.descricao)) {
			return false;
		}
		return true;
	}

	@Override
	public int compareTo(Opcao o) {

		try {

			return this.ordem.compareTo(o.ordem);

		} catch (Exception e) {

			return 0;
		}
	}

}
