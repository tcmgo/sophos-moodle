/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Frequencia;
import br.gov.go.tcm.sophos.entidade.Inscricao;
import br.gov.go.tcm.sophos.entidade.Modulo;
import br.gov.go.tcm.sophos.entidade.dto.FrequenciaEncontroDTO;
import br.gov.go.tcm.sophos.entidade.dto.FrequenciaParticipanteDTO;
import br.gov.go.tcm.sophos.entidade.dto.ModuloFrequenciaParticipanteDTO;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.lazy.TableLazyFrequencia;
import br.gov.go.tcm.sophos.lazy.base.TableLazyPadrao;
import br.gov.go.tcm.sophos.negocio.FrequenciaNegocio;
import br.gov.go.tcm.sophos.negocio.InscricaoNegocio;
import br.gov.go.tcm.sophos.negocio.ModuloNegocio;
import br.gov.go.tcm.sophos.negocio.MoodleNegocio;
import br.gov.go.tcm.sophos.rest.dto.MoodlePessoaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
@Scope("session")
public class FrequenciaControlador extends ControladorBaseCRUD<Frequencia, FrequenciaNegocio> {

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private FrequenciaNegocio negocio;

	@Autowired
	private MoodleNegocio moodleNegocio;

	@Autowired
	private ModuloNegocio moduloNegocio;

	@Autowired
	private InscricaoNegocio inscricaoNegocio;

	private Evento evento;

	private Modulo modulo;

	private List<Modulo> modulosList;

	private List<Frequencia> frequencias;

	private List<ModuloFrequenciaParticipanteDTO> moduloFrequeciaParticipanteList;
	
	@Autowired
	private TableLazyFrequencia tableLazyFrequencia;

	public void selecionarEvento(Evento evento) {
		
		List<Modulo> modulos = this.moduloNegocio.listaPorAtributo("evento", evento);
		
		if (modulos.size() == 0) {
			
			this.adicionarMensagemDeAlerta("Não é possível atribuir frequências, pois o evento não possui módulos cadastrados. Edite o evento para ter ao menos um MÓDULO ÚNICO.");
			
		} else {

			this.setEntidade(new Frequencia());
	
			this.evento = evento;
			
			this.tableLazyFrequencia.setEvento(evento);
			
			this.tableLazyFrequencia.setConsulta("");
	
			this.modulo = new Modulo();
	
			this.setModulosList(this.moduloNegocio.listaPorAtributo("evento", evento));
	
			moduloFrequeciaParticipanteList = new ArrayList<ModuloFrequenciaParticipanteDTO>();
	
			if (this.getModulosList().size() > 1) {
	
				moduloFrequeciaParticipanteList.add(null);
			}
	
			this.montarListaFrequenciaVisualizacao();
	
			this.montarListaGeralFrequenciaVisualizacao();
			
			this.executarJS("openModal('modalFrequencia')");
		
		}
	}

	private void montarListaGeralFrequenciaVisualizacao() {

		ModuloFrequenciaParticipanteDTO moduloFrequenciaParticipanteDTO = new ModuloFrequenciaParticipanteDTO();

		moduloFrequenciaParticipanteDTO.setModulo("GERAL");

		moduloFrequenciaParticipanteDTO.setQuantidadeEncontros(0);

		moduloFrequenciaParticipanteDTO.setFrequenciaParticipanteList(new ArrayList<FrequenciaParticipanteDTO>());

		for (ModuloFrequenciaParticipanteDTO moduloFrequenciaParticipanteTempDTO : moduloFrequeciaParticipanteList) {

			if (AjudanteObjeto.eReferencia(moduloFrequenciaParticipanteTempDTO)) {

				moduloFrequenciaParticipanteDTO.setQuantidadeEncontros(moduloFrequenciaParticipanteDTO.getQuantidadeEncontros() + moduloFrequenciaParticipanteTempDTO.getQuantidadeEncontros());

				for (FrequenciaParticipanteDTO frequenciaParticipanteTempDTO : moduloFrequenciaParticipanteTempDTO.getFrequenciaParticipanteList()) {

					if (moduloFrequenciaParticipanteDTO.getFrequenciaParticipanteList().contains(frequenciaParticipanteTempDTO)) {

						for (FrequenciaParticipanteDTO frequenciaParticipanteDTO : moduloFrequenciaParticipanteDTO.getFrequenciaParticipanteList()) {

							if (frequenciaParticipanteTempDTO.equals(frequenciaParticipanteDTO)) {

								frequenciaParticipanteDTO.setQuantidadePresenca(frequenciaParticipanteDTO.getQuantidadePresenca()+ frequenciaParticipanteTempDTO.getQuantidadePresenca());

								break;
							}
						}

					} else {

						moduloFrequenciaParticipanteDTO.getFrequenciaParticipanteList().add(frequenciaParticipanteTempDTO.clone());
					}
				}
			}
		}

		if (this.getModulosList().size() > 1) {

			moduloFrequeciaParticipanteList.set(0, moduloFrequenciaParticipanteDTO);
		}

	}

	private void montarListaFrequenciaVisualizacao() {
		
		FrequenciaNegocio frequenciaNegocio = new FrequenciaNegocio();
		

		List<Inscricao> inscricoes = this.inscricaoNegocio.listarInscricoesAutorizadas(this.evento);

		for (Modulo modulo : this.getModulosList()) {

			ModuloFrequenciaParticipanteDTO moduloFrequenciaParticipanteDTO = new ModuloFrequenciaParticipanteDTO();

			moduloFrequenciaParticipanteDTO.setIdModulo(modulo.getId());

			moduloFrequenciaParticipanteDTO.setModulo(modulo.getTitulo());

			moduloFrequenciaParticipanteDTO.setQuantidadeEncontros(modulo.getQuantidadeEncontros());

			moduloFrequenciaParticipanteDTO.setFrequenciaParticipanteList(new ArrayList<FrequenciaParticipanteDTO>());

			List<Frequencia> listFrequecia = this.getNegocio().listaPorAtributo("modulo", modulo);
					
			for (Inscricao inscricao : inscricoes) {
			
				Boolean existeFrequencia = Boolean.FALSE;
				
				FrequenciaParticipanteDTO frequenciaParticipanteDTO;

				for (Frequencia frequencia : listFrequecia) {				

					if (inscricao.getParticipante().equals(frequencia.getParticipante()) && frequencia.getPresenca()) {
						
						existeFrequencia = Boolean.TRUE;

						break;
					}
				}

				if (existeFrequencia) {
				
					frequenciaParticipanteDTO = novoFrequenciaParticipanteDTO(inscricao);
					
					int freq = moduloFrequenciaParticipanteDTO.getQuantidadeEncontros();
					for (Frequencia frequencia : listFrequecia) {
						if (inscricao.getParticipante().equals(frequencia.getParticipante()) && frequencia.getPresenca()) {
							
		
							    if(freq>0) frequenciaParticipanteDTO.setQuantidadePresenca(frequenciaParticipanteDTO.getQuantidadePresenca() + 1);
		                        freq--;
							
						}
					
					}
						
						moduloFrequenciaParticipanteDTO.getFrequenciaParticipanteList().add(frequenciaParticipanteDTO);
				

				}  else {

					frequenciaParticipanteDTO = novoFrequenciaParticipanteDTO(inscricao);

					if (!moduloFrequenciaParticipanteDTO.getFrequenciaParticipanteList().contains(frequenciaParticipanteDTO)) {

						moduloFrequenciaParticipanteDTO.getFrequenciaParticipanteList().add(frequenciaParticipanteDTO);
					}
				}
			}

			moduloFrequeciaParticipanteList.add(moduloFrequenciaParticipanteDTO);
					
		}
	}

	private FrequenciaParticipanteDTO novoFrequenciaParticipanteDTO(Inscricao inscricao) {

		FrequenciaParticipanteDTO frequenciaParticipanteDTO;

		frequenciaParticipanteDTO = new FrequenciaParticipanteDTO();

		frequenciaParticipanteDTO.setIdParticipante(inscricao.getParticipante().getId());

		frequenciaParticipanteDTO.setParticipante(inscricao.getParticipante().getPessoa().getNome());

		frequenciaParticipanteDTO.setQuantidadePresenca(0);

		return frequenciaParticipanteDTO;
	}

	public void montarTabelaFrequencia() {

		if (AjudanteObjeto.eReferencia(this.modulo)) {
			
			this.tableLazyFrequencia.setModulo(this.modulo);

		}
	}

	public void salvarFrequencia() {

		try {

			List<Frequencia> frequenciaList = montarListaFrequencia();

			this.getNegocio().salvar(frequenciaList);
			
			this.adicionarMensagemInformativa(this.obterMensagemSucessoAoInserir());
			
			this.executarAposInserirSucesso();
			
			this.moduloFrequeciaParticipanteList = new ArrayList<ModuloFrequenciaParticipanteDTO>();
						
			this.montarListaFrequenciaVisualizacao();
			
			this.montarListaGeralFrequenciaVisualizacao();
			
			//this.executarJS("openModal('modalFrequencia')");

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);
		}
	}

	public void salvarFrequenciaMoodle(Inscricao inscricaoSophos) {

		try {
			List<Frequencia> frequenciaList = montarListaFrequenciaMoodle(inscricaoSophos);

			this.getNegocio().salvar(frequenciaList);

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);
		}
	}
	
	public void msgSucesso(){
		
		this.adicionarMensagemInformativa(this.obterMensagemSucessoAoInserir());
		
	}
	
	@Autowired
	FrequenciaNegocio frequenciaNegocio;
	
	private List<Frequencia> montarListaFrequencia() {
		
		List<Frequencia> frequenciaList = new ArrayList<Frequencia>();

		for (ItemFrequencia itemFrequencia : this.tableLazyFrequencia.getLista()) {

			for (FrequenciaEncontroDTO frequenciaEncontroDTO : itemFrequencia.getFrequencia()) {

				Frequencia frequencia = new Frequencia();
				
				frequencia.setId(frequenciaEncontroDTO.getIdFrequencia());

				frequencia.setModulo(this.modulo);

				frequencia.setParticipante(itemFrequencia.getParticipante());

				frequencia.setEncontro(frequenciaEncontroDTO.getEncontro());

				frequencia.setDataCadastro(new Date());

				frequencia.setPresenca(frequenciaEncontroDTO.getPresenca());

				frequencia.setHouveAlteracao(frequenciaEncontroDTO.getHouveAlteracao());

				frequenciaList.add(frequencia);
			}
		}

		return frequenciaList;
	}
	
	private List<Frequencia> montarListaFrequenciaMoodle(Inscricao inscricaoSophos) {
		
		List<FrequenciaEncontroDTO> frequenciaEncontros = new ArrayList<FrequenciaEncontroDTO>();

		for (int contador = 1; contador <= this.modulo.getQuantidadeEncontros(); contador++) {

			frequenciaEncontros.add(new FrequenciaEncontroDTO(Boolean.TRUE, contador));

		}

		ItemFrequencia itemFrequencia = new ItemFrequencia(
				inscricaoSophos.getParticipante(), frequenciaEncontros);
		
		List<Frequencia> frequenciaList = new ArrayList<Frequencia>();

		for (FrequenciaEncontroDTO frequenciaEncontroDTO : itemFrequencia.getFrequencia()) {

				Frequencia frequencia = new Frequencia();

				frequencia.setId(frequenciaEncontroDTO.getIdFrequencia());

				frequencia.setModulo(this.modulo);

				frequencia.setParticipante(itemFrequencia.getParticipante());

				frequencia.setEncontro(frequenciaEncontroDTO.getEncontro());

				frequencia.setDataCadastro(new Date());

				frequencia.setPresenca(frequenciaEncontroDTO.getPresenca());

				frequencia.setHouveAlteracao(frequenciaEncontroDTO.getHouveAlteracao());

				frequenciaList.add(frequencia);
		}

		return frequenciaList;
	}

	// Atribuição de presença quando participante concluir curso no Moodle (EAD) com nota > 70,00
	public void importarFrequenciasDoMoodle() {

		try {
			if (this.moodleNegocio.eventoTemCursoMoodleVinculado(this.evento)) {
				
				List<Inscricao> listInscricoesSophos = 
						this.inscricaoNegocio.listarInscricoesAutorizadas(this.evento);
				
				Map<Long, Double> hashMapIdNotas = 
						this.moodleNegocio.obterHashMapIdNotasDoCursoMoodleVinculado(
								this.evento.getCursoMoodleId());
		
				for (Inscricao inscricaoSophos : listInscricoesSophos) {
					
			        MoodlePessoaDTO moodlePessoaSophosDTO = this.moodleNegocio.obterPessoaPorCpf(
			        		inscricaoSophos.getParticipante().getPessoa().getCpf());
			        
			        if (hashMapIdNotas.containsKey( moodlePessoaSophosDTO.getId() ) ) {
			        	
						// Se nota > 70, salvar frequência
						if(hashMapIdNotas.get(moodlePessoaSophosDTO.getId()) > 70) {
							
							List<Frequencia> frequenciaSophos = 
									this.frequenciaNegocio.obterFrequenciaModuloParticipante(
									this.modulo, inscricaoSophos.getParticipante());

							Boolean salvarFrequencia = false;
							
							if(frequenciaSophos.isEmpty()) {
								salvarFrequenciaMoodle(inscricaoSophos);
								
							}else{
								for(Frequencia frequencia : frequenciaSophos) {
									
									if(frequencia.getPresenca().equals(Boolean.FALSE)) {
										frequencia.setPresenca(true);
										salvarFrequencia = true;
									}
								}
								if(salvarFrequencia) {
									this.frequenciaNegocio.salvar(frequenciaSophos);
								}
							}
						}
					}
				}					
			}
		} catch (ValidacaoException e) {
	
			this.adicionarMensagemInformativa(e.getMessage());
	
		} catch (Exception e) {
	
			e.printStackTrace();
	
			this.adicionarMensagemInformativa("Ocorreu um erro durante a importação das frequências do Moodle. Por favor, entre em contato com a Escola de Contas.");
	
		}
	}

	@Override
	public TableLazyPadrao<Frequencia> getLista() {

		return null;
	}

	public Modulo getModulo() {
		return modulo;
	}

	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}

	public FrequenciaNegocio getNegocio() {
		return negocio;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public List<Modulo> getModulosList() {
		return modulosList;
	}

	public void setModulosList(List<Modulo> modulosList) {
		this.modulosList = modulosList;
	}

	public List<Frequencia> getFrequencias() {
		return frequencias;
	}

	public void setFrequencias(List<Frequencia> frequencias) {
		this.frequencias = frequencias;
	}

	public List<ModuloFrequenciaParticipanteDTO> getModuloFrequeciaParticipanteList() {
		return moduloFrequeciaParticipanteList;
	}

	public void setModuloFrequeciaParticipanteList(
			List<ModuloFrequenciaParticipanteDTO> moduloFrequeciaParticipanteList) {
		this.moduloFrequeciaParticipanteList = moduloFrequeciaParticipanteList;
	}

	public TableLazyFrequencia getTableLazyFrequencia() {
		return tableLazyFrequencia;
	}
	
}
