/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteData;
import br.gov.go.tcm.estrutura.entidade.controledeacesso.UsuarioMonitor;
import br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato.UsuarioMonitorDAO;
import br.gov.go.tcm.estrutura.seguranca.ajudante.AjudanteCriptografia;
import br.gov.go.tcm.estrutura.util.spring.SpringUtils;
import br.gov.go.tcm.sophos.ajudante.AjudanteEmail;
import br.gov.go.tcm.sophos.ajudante.AjudanteMensagem;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.PerfilUsuario;
import br.gov.go.tcm.sophos.entidade.Usuario;
import br.gov.go.tcm.sophos.entidade.enumeracao.PerfilEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoDominioEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoUsuario;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import br.gov.go.tcm.sophos.persistencia.PerfilUsuarioDAO;
import br.gov.go.tcm.sophos.persistencia.UsuarioDAO;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class UsuarioNegocio extends NegocioBase<Usuario> {

	private static final long serialVersionUID = 7904679535820226570L;

	@Autowired
	private UsuarioDAO dao;

	@Autowired
	private PessoaNegocio pessoaNegocio;

	@Autowired
	private DominioNegocio dominioNegocio;

	@Autowired
	private UsuarioMonitorDAO usuarioMonitorDAO;

	@Autowired
	private PerfilUsuarioDAO perfilUsuarioDAO;

	@Override
	protected UsuarioDAO getDAO() {

		return this.dao;
	}

	@Override
	protected void executarValidacaoAntesDeSalvar(Usuario entidade) {
		
		/*entidade.setNomeUsuario(entidade.getPessoa().getCpf());
		
		entidade.setNomeUsuario(entidade.getNomeUsuario().replace(".", ""));
		entidade.setNomeUsuario(entidade.getNomeUsuario().replace("-", ""));*/
		
		entidade.setNomeUsuario(null);
		
		super.executarValidacaoAntesDeSalvar(entidade);

		/*if (verificaExistenciaUsuarioInterno(entidade.getNomeUsuario())) {

			throw new ValidacaoException("Nome de usuário " + entidade.getNomeUsuario() + " já existe na base de dados interna do TCM. Favor escolher um nome de usuário único.");
		}

		if (AjudanteObjeto.eReferencia(this.getDAO().obterPorAtributo("nomeUsuario", entidade.getNomeUsuario()))) {

			throw new ValidacaoException("Nome de usuário " + entidade.getNomeUsuario() + " já existe na base de dados do Sophos. Favor escolher um nome de usuário único.");
		}*/

	}

	public Boolean isExisteMatriculaBaseUsuariosInterno(String matricula) {

		Boolean existe = false;
		Boolean numerosDigitosCorreto = false;

		if (!StringUtils.isEmpty(matricula)) {

			numerosDigitosCorreto = matricula.length() == 6 ? true : false;

			if (numerosDigitosCorreto) {
				existe = this.verificaExistenciaMatricula(matricula);
			}

		}

		if (existe) {
			return true;
		} else {
			return false;
		}

	}

	@SuppressWarnings("unchecked")
	private Boolean verificaExistenciaUsuarioInterno(String nomeUsuario) {

		UsuarioMonitor usuarioMonitor = new UsuarioMonitor();

		usuarioMonitor.setNomeUsuario(nomeUsuario);

		usuarioMonitor.setAtivo(Boolean.TRUE);

		List<UsuarioMonitor> lstUsuariosMonitor = (List<UsuarioMonitor>) this.usuarioMonitorDAO
				.obterPorExemplo(usuarioMonitor, 0);

		return lstUsuariosMonitor.size() > 0;
	}

	@SuppressWarnings("unchecked")
	private Boolean verificaExistenciaMatricula(String matricula) {

		UsuarioMonitor usuarioMonitor = new UsuarioMonitor();

		usuarioMonitor.setMatricula(matricula);

		usuarioMonitor.setAtivo(Boolean.TRUE);

		List<UsuarioMonitor> lstUsuariosMonitor = (List<UsuarioMonitor>) this.usuarioMonitorDAO
				.obterPorExemplo(usuarioMonitor, 0);

		return lstUsuariosMonitor.size() > 0;
	}

	@Override
	public void salvar(Usuario entidade) {

		this.executarValidacaoAntesDeSalvar(entidade);

		entidade.setSenha(AjudanteCriptografia.gerarMD5(entidade.getSenha()));

		entidade.setTipoUsuario(
				this.dominioNegocio.obterDominio(TipoDominioEnum.TipoUsuario, TipoUsuario.USUARIO_EXTERNO.getCodigo()));

		this.pessoaNegocio.salvar(entidade.getPessoa());

		super.salvar(entidade);

		this.definePerfilParticipante(entidade);
	}

	public void definePerfilParticipante(Usuario entidade) {

		entidade.setPerfilList(this.perfilUsuarioDAO.listaPorAtributo("usuario", entidade));

		Boolean existePerfilParticipante = Boolean.FALSE;

		for (PerfilUsuario perfilUsuario : entidade.getPerfilList()) {

			if (perfilUsuario.getPerfil().equals(PerfilEnum.PARTICIPANTE)) {

				existePerfilParticipante = Boolean.TRUE;

				break;
			}
		}

		if (!existePerfilParticipante) {

			PerfilUsuario perfilUsuario = new PerfilUsuario();

			perfilUsuario.setUsuario(entidade);

			perfilUsuario.setPerfil(PerfilEnum.PARTICIPANTE);

			this.perfilUsuarioDAO.salvar(perfilUsuario);
			
			ArrayList<PerfilUsuario> lista = new ArrayList<PerfilUsuario>();
			
			lista.add(perfilUsuario);
			
			entidade.setPerfilList(lista);

		}
	}

	public void salvarSimples(Usuario entidade) {

		super.salvar(entidade);

		this.definePerfilParticipante(entidade);

	}

	public void redefinirSenha(Usuario entidade, String senhaAtual, String senha, String senhaRepetida) {

		if (!AjudanteCriptografia.gerarMD5(senhaAtual).equals(entidade.getSenha())) {

			throw new ValidacaoException(AjudanteMensagem.obterMensagemPelaChave("senha_atual_nao_confere"));
		}

		this.redefinirSenha(entidade, senha, senhaRepetida);
	}

	public void redefinirSenha(final Usuario usuario, final String senha, final String senhaRepetida) {

		if (!senha.equals(senhaRepetida)) {

			throw new ValidacaoException(AjudanteMensagem.obterMensagemPelaChave("senhas_nao_conferem"));
		}

		usuario.setSenha(AjudanteCriptografia.gerarMD5(senha));

		usuario.setToken(null);

		super.salvar(usuario);
	}

	public List<Usuario> listarPorListaIds(List<Long> ids, int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters, String consulta) {

		return dao.listarPorListaIds(ids, first, pageSize, sortField, sortOrder, filters, consulta);
	}

	public String solicitarRedefinicaoSenha(final String emailOuCPFUsuario) {

		final Usuario usuario;

		if (pessoaNegocio.existePessoaComEmail(emailOuCPFUsuario)) {

			usuario = this.getDAO().obterPorAtributo("pessoa",
					pessoaNegocio.obterPorAtributo("email", emailOuCPFUsuario));

		} else {
			
			String cpf = emailOuCPFUsuario;

			if (cpf != null && cpf.length() == 11)
	               cpf = cpf.substring(0, 3) + "." + cpf.substring(3, 6) + "." + cpf.substring(6, 9) + "-" + cpf.substring(9, 11);
	 
			if(pessoaNegocio.existePessoaComCPF(cpf)){
				usuario = this.getDAO().obterPorAtributo("pessoa",
				pessoaNegocio.obterPorAtributo("cpf", cpf));
			} else {

				throw new ValidacaoException(AjudanteMensagem.obterMensagemPelaChave("nunhum_usuario_encontrado"));
			}
						
			}		
		 /*else {

			if (dao.existeUsuarioComNomeUsuario(emailOuNomeUsuario)) {

				usuario = this.getDAO().obterPorAtributo("nomeUsuario", emailOuNomeUsuario);

			} 
			

		}*/ // Funcionalidade retirada solicitação GLPI ID 

		if (!usuario.getTipoUsuario().getCodigo().equals(TipoUsuario.USUARIO_EXTERNO.getCodigo())) {

			throw new ValidacaoException(AjudanteMensagem.obterMensagemPelaChave("nao_podem_solicitar_redefinicao"));
		}

		usuario.setToken(UUID.randomUUID().toString());

		this.atualizar(usuario);

		this.enviarEmailRedefinicaoSenhaUsuario(usuario);

		return usuario.getPessoa().getEmail();
	}

	private void enviarEmailRedefinicaoSenhaUsuario(Usuario usuario) {

		final String sujeito = "TCM - SOPHOS - REDEFINIÇÃO DE SENHA";
		final String templateMensagem = "<b>Olá {0}!</b> <br/><br/> Você solicitou uma redefinição de senha para o CPF <b>{2}</b> no Sistema Sophos.<br/><br/>Data da requisição: {3}<br/>Para redefinir sua senha acesse o link: <a href=\"{4}\">{4}<a><br/><br/><b>Escola de Contas<br/>Tribunal de Contas dos Municípios do Estado de Goiás - TCM GO</b>";
		final String contextoPublicacaoAplicacao = (String) SpringUtils.getApplicationContext()
				.getBean("contextoPublicoDaAplicacao");
		final String link = contextoPublicacaoAplicacao + "esqueci-minha-senha.jsf?token=" + usuario.getToken();
		final String mensagem = MessageFormat.format(templateMensagem, usuario.getPessoa().getNome().toUpperCase(), usuario.getNomeUsuario(), usuario.getPessoa().getCpf(),
				AjudanteData.obterDataAtualFormatadaModeloPadrao(), link);

		AjudanteEmail.enviarEmail(usuario.getPessoa().getEmail(),"TCM - SOPHOS", sujeito, mensagem);
	}

	public void verificaCPFUnico(String cpf) {

		Usuario usuario = this.getDAO().obterUsuarioPorCPF(cpf);

		if (AjudanteObjeto.eReferencia(usuario)) {

			throw new ValidacaoException(
					"Esse CPF já está registrado em nossa base de dados. Por favor tente recuperar sua senha.");

		}
	}

	public PerfilEnum obterPerfilPrincipal(Usuario usuario) {

		if (AjudanteObjeto.eReferencia(usuario.getPerfilList()) && usuario.getPerfilList().size() > 0) {

			for (PerfilUsuario perfilUsuario : usuario.getPerfilList()) {

				if (perfilUsuario.getPerfil().equals(PerfilEnum.PARTICIPANTE)) {

					return perfilUsuario.getPerfil();
				}
			}

			return usuario.getPerfilList().get(0).getPerfil();
		}

		return null;
	}

	public boolean existePerfilParticipante(Usuario usuario) {

		if (AjudanteObjeto.eReferencia(usuario.getPerfilList()) && usuario.getPerfilList().size() > 0) {

			for (PerfilUsuario perfilUsuario : usuario.getPerfilList()) {

				if (perfilUsuario.getPerfil().equals(PerfilEnum.PARTICIPANTE)) {

					return Boolean.TRUE;
				}
			}
		}

		return Boolean.FALSE;
	}

	public List<PerfilUsuario> listarPerfisUsuario(Usuario usuario) {

		return this.perfilUsuarioDAO.listaPorAtributo("usuario", usuario);
	}

	public void validarUsuario(Usuario usuario) {

		this.executarValidacaoAntesDeSalvar(usuario);

	}
}
