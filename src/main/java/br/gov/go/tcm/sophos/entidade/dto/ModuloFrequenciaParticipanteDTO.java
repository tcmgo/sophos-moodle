/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade.dto;

import org.springframework.context.annotation.Scope;

import java.io.Serializable;
import java.util.List;

@Scope("session")
public class ModuloFrequenciaParticipanteDTO implements Serializable {

	private static final long serialVersionUID = -8068647438351960387L;

	private Long idModulo;

	private String modulo;

	private Integer quantidadeEncontros;
	
	private List<FrequenciaParticipanteDTO> frequenciaParticipanteList;

	public Integer getQuantidadeEncontros() {
		return quantidadeEncontros;
	}

	public void setQuantidadeEncontros(Integer quantidadeEncontros) {
		this.quantidadeEncontros = quantidadeEncontros;
	}

	public Long getIdModulo() {
		return idModulo;
	}

	public void setIdModulo(Long idModulo) {
		this.idModulo = idModulo;
	}

	public String getModulo() {
		return modulo;
	}

	public void setModulo(String modulo) {
		this.modulo = modulo;
	}

	public List<FrequenciaParticipanteDTO> getFrequenciaParticipanteList() {
		return frequenciaParticipanteList;
	}

	public void setFrequenciaParticipanteList(List<FrequenciaParticipanteDTO> frequenciaParticipanteList) {
		this.frequenciaParticipanteList = frequenciaParticipanteList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idModulo == null) ? 0 : idModulo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModuloFrequenciaParticipanteDTO other = (ModuloFrequenciaParticipanteDTO) obj;
		if (idModulo == null) {
			if (other.idModulo != null)
				return false;
		} else if (!idModulo.equals(other.idModulo))
			return false;
		return true;
	}

	
}
