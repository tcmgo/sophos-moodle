/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.ajudante;

import br.gov.go.tcm.estrutura.relatorio.GeradorRelatorio;
import br.gov.go.tcm.estrutura.relatorio.excecao.GeradorRelatorioExcecao;
import br.gov.go.tcm.estrutura.relatorio.fonteDeDados.FonteDadosRelatorio;
import br.gov.go.tcm.estrutura.relatorio.fonteDeDados.ParametroVisao;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class AjudanteRelatorio {

	@SuppressWarnings("deprecation")
	public static byte[] gerarRelatorio(String caminho, List<ParametroVisao> parametros,
			FonteDadosRelatorio fonteDadosRelatorio, int tipoSaida, boolean isCertificado) throws GeradorRelatorioExcecao {

		try {
			File reportFile = new File(caminho);
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportFile.getPath());
			Map<String, Object> parametrosJasper = new HashMap<String, Object>();

			for (ParametroVisao parametro : parametros) {
				parametrosJasper.put(parametro.getId(), parametro.getValor());
			}

			JasperPrint jasperPrint = null;

			if (fonteDadosRelatorio.getFonteDados() instanceof Connection) {
				jasperPrint = JasperFillManager.fillReport(jasperReport, parametrosJasper,
						(Connection) fonteDadosRelatorio.getFonteDados());
			}

			if (fonteDadosRelatorio.getFonteDados() instanceof JRDataSource) {
				jasperPrint = JasperFillManager.fillReport(jasperReport, parametrosJasper,
						(JRDataSource) fonteDadosRelatorio.getFonteDados());
			}

			int i = 0;
			for (Iterator<JRPrintPage> iterator = jasperPrint.getPages().iterator(); iterator.hasNext();) {
				
				i++;
				
				JRPrintPage page = iterator.next();
				if ((!iterator.hasNext() && page.getElements().isEmpty()) || (isCertificado && i==3)) {
					iterator.remove();
				}
			}

			byte[] saida = null;
			if(GeradorRelatorio.SAIDA_PDF == tipoSaida){
				
				saida = JasperExportManager.exportReportToPdf(jasperPrint);
				
			}else if(GeradorRelatorio.SAIDA_XLS == tipoSaida){
				
				ByteArrayOutputStream output = new ByteArrayOutputStream(); 
				
				JRXlsExporter exporterXLS = new JRXlsExporter(); 
				exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint); 
				exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, output); 
				exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE); 
				exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE); 
				exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE); 
				
				exporterXLS.exportReport();
				
				saida = output.toByteArray();
			}

			return saida;

		} catch (JRException e) {
			e.printStackTrace();
			throw new GeradorRelatorioExcecao(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new GeradorRelatorioExcecao(e.getMessage());
		}
	}

	public static JasperPrint obterRelatorio(String caminho, List<ParametroVisao> parametros, FonteDadosRelatorio fonteDadosRelatorio, int tipoSaida) throws GeradorRelatorioExcecao {

		try {
			File reportFile = new File(caminho);
			@SuppressWarnings("deprecation")
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportFile.getPath());
			Map<String, Object> parametrosJasper = new HashMap<String, Object>();

			for (ParametroVisao parametro : parametros) {
				parametrosJasper.put(parametro.getId(), parametro.getValor());
			}

			JasperPrint jasperPrint = null;

			if (fonteDadosRelatorio.getFonteDados() instanceof Connection) {
				jasperPrint = JasperFillManager.fillReport(jasperReport, parametrosJasper,
						(Connection) fonteDadosRelatorio.getFonteDados());
			}

			if (fonteDadosRelatorio.getFonteDados() instanceof JRDataSource) {
				jasperPrint = JasperFillManager.fillReport(jasperReport, parametrosJasper,
						(JRDataSource) fonteDadosRelatorio.getFonteDados());
			}

			for (Iterator<JRPrintPage> iterator = jasperPrint.getPages().iterator(); iterator.hasNext();) {
				JRPrintPage page = iterator.next();
				if (!iterator.hasNext() && page.getElements().isEmpty()) {
					iterator.remove();
				}
			}

			return jasperPrint;

		} catch (JRException e) {
			e.printStackTrace();
			throw new GeradorRelatorioExcecao(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new GeradorRelatorioExcecao(e.getMessage());
		}
	}
}
