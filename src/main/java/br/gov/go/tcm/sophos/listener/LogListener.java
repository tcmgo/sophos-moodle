/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.listener;

import br.gov.go.tcm.estrutura.container.faces.AjudanteContextoFaces;
import br.gov.go.tcm.estrutura.util.spring.SpringUtils;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.LoginControlador;
import br.gov.go.tcm.sophos.entidade.Log;
import br.gov.go.tcm.sophos.entidade.Usuario;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoOperacao;
import br.gov.go.tcm.sophos.persistencia.LogDAO;
import org.hibernate.event.*;

import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

public class LogListener implements Serializable, PostInsertEventListener, PostUpdateEventListener, PostDeleteEventListener {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = 1322766760877085916L;

	private void registrarLog(Object entidade, TipoOperacao operacao) {

		if (entidade instanceof EntidadePadrao && !( entidade instanceof Log )) {

			Log log = new Log();

			log.setDataCadastro(new Date());

			log.setOrigem(operacao);

			log.setIdEntidade(( (EntidadePadrao) entidade ).getId());

			log.setTabela(this.obterNomeTabela(entidade));

			log.setUsuario(this.obterUsuarioLogado());

			this.getDAO().salvar(log);
		}
	}

	private Usuario obterUsuarioLogado() {

		Usuario usuario = (Usuario) AjudanteContextoFaces.getObjetoSessao(LoginControlador.USUARIO_LOGADO);

		return AjudanteObjeto.eReferencia(usuario) ? usuario : null;
	}
	
	private LogDAO getDAO() {

		return SpringUtils.getApplicationContext().getBean(LogDAO.class);
	}

	private String obterNomeTabela(Object entidade) {

		return entidade.getClass().getAnnotation(Table.class).name();
	}

	@Override
	public void onPostDelete(PostDeleteEvent event) {

		this.registrarLog(event.getEntity(), TipoOperacao.REMOCAO);
	}

	@Override
	public void onPostUpdate(PostUpdateEvent event) {

		this.registrarLog(event.getEntity(), TipoOperacao.ALTERACAO);
	}

	@Override
	public void onPostInsert(PostInsertEvent event) {

		this.registrarLog(event.getEntity(), TipoOperacao.INCLUSAO);
	}
}
