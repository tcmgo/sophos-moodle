/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade.dto;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteReflexao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ModuloAvaliacaoChartDTO implements Serializable{

	private static final long serialVersionUID = 3442455796705215713L;

	private String titulo;

	private Long id;

	private List<QuestionarioAvaliacaoChartDTO> questionarios;
	
	public ModuloAvaliacaoChartDTO(String titulo, Long id) {
		super();
		this.titulo = titulo;
		this.id = id;
		this.questionarios = new ArrayList<QuestionarioAvaliacaoChartDTO>();
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<QuestionarioAvaliacaoChartDTO> getQuestionarios() {
		return questionarios;
	}

	public void setQuestionarios(List<QuestionarioAvaliacaoChartDTO> questionarios) {
		this.questionarios = questionarios;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModuloAvaliacaoChartDTO other = (ModuloAvaliacaoChartDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		String descObjeto = "[";
		AjudanteReflexao assistenteReflexao = new AjudanteReflexao();
		List<String> propriedades = assistenteReflexao.obterPropriedades(this.getClass());

		for (String propriedade : propriedades) {
			Object valor;
			// FIXME: implementar exceção;
			// try {
			valor = assistenteReflexao.obterValorPropriedade(this, propriedade);
			// } catch (ReflexaoExcecao e) {
			// e.printStackTrace();
			// return "";
			// }

			if (valor != null && List.class.isAssignableFrom(valor.getClass())) {
				continue;
			}

			descObjeto += propriedade + "=" + ((valor != null) ? valor.toString().trim() : "") + " ,";
		}
		if (descObjeto.length() > 1)
			descObjeto = descObjeto.substring(0, descObjeto.length() - 1);
		descObjeto += "]";
		return descObjeto;
	}

}
