/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 *
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 *
 * Este cabeçalho deve ser mantido.
 *
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade.enumeracao;

import net.sf.jasperreports.engine.type.OrientationEnum;

import java.util.Arrays;
import java.util.List;

public enum RelatorioEnum {

	RELATORIO_CERTIFICADO_INSTRUTOR("Certificado de Instrutor",
			new CampoRelatorioEnum[] {CampoRelatorioEnum.EVENTO, CampoRelatorioEnum.INSTRUTOR},
			new CampoRelatorioEnum[] {CampoRelatorioEnum.EVENTO, CampoRelatorioEnum.INSTRUTOR},
			new PerfilEnum[] { PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS },
			"declaracao-instrutor.jasper", OrientationEnum.PORTRAIT),

	RELATORIO_DECLARACAO_PARTICIPANTE("Declaração de Participante",
			new CampoRelatorioEnum[] {CampoRelatorioEnum.EVENTO, CampoRelatorioEnum.PARTICIPANTE},
			new CampoRelatorioEnum[] {CampoRelatorioEnum.EVENTO, CampoRelatorioEnum.PARTICIPANTE},
			new PerfilEnum[] { PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS},
			"declaracao-participante.jasper", OrientationEnum.PORTRAIT),

	RELATORIO_AVALIACAO_EFICACIA("Avaliações de Reação",
			new CampoRelatorioEnum[] {CampoRelatorioEnum.EVENTO},
			new CampoRelatorioEnum[] {CampoRelatorioEnum.EVENTO},
			new PerfilEnum[] { PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS},
			"avaliacao_eficacia.jasper", OrientationEnum.PORTRAIT),

	RELATORIO_EVENTO("Relatório de Eventos",
			new CampoRelatorioEnum[] {CampoRelatorioEnum.TIPO_EVENTO, CampoRelatorioEnum.PUBLICO_ALVO, CampoRelatorioEnum.EIXO_TEMATICO, CampoRelatorioEnum.DATA_INICIO,
					CampoRelatorioEnum.DATA_FIM, CampoRelatorioEnum.LOCALIZACAO, CampoRelatorioEnum.PROVEDOR, CampoRelatorioEnum.CIDADE, CampoRelatorioEnum.MODALIDADE, CampoRelatorioEnum.IMPORTADO},
			new CampoRelatorioEnum[] {},
			new PerfilEnum[] { PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS},
			"eventos.jasper", OrientationEnum.LANDSCAPE),

	RELATORIO_REGISTRO_FREQUENCIA("Registro de Frequência",
			new CampoRelatorioEnum[] {CampoRelatorioEnum.EVENTO, CampoRelatorioEnum.MODULO},
			new CampoRelatorioEnum[] {CampoRelatorioEnum.EVENTO, CampoRelatorioEnum.MODULO},
			new PerfilEnum[] { PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS},
			"registro-frequencia.jasper", OrientationEnum.LANDSCAPE),

	RELATORIO_PARTICIPANTE("Relatório de Participantes",
			new CampoRelatorioEnum[] {CampoRelatorioEnum.EVENTO, CampoRelatorioEnum.AUTORIZADA, CampoRelatorioEnum.TIPO_PARTICIPANTE, CampoRelatorioEnum.PNE},
			new CampoRelatorioEnum[] {},
			new PerfilEnum[] { PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS },
			"participantes.jasper", OrientationEnum.LANDSCAPE),

	RELATORIO_PRE_INSCRICOES("Relatório de Pré-inscrições",
			new CampoRelatorioEnum[] {CampoRelatorioEnum.EVENTO, CampoRelatorioEnum.TIPO_PARTICIPANTE, CampoRelatorioEnum.DATA_INICIO, CampoRelatorioEnum.DATA_FIM, CampoRelatorioEnum.INDICADA, CampoRelatorioEnum.AUTORIZADA, CampoRelatorioEnum.CARGO},
			new CampoRelatorioEnum[] {CampoRelatorioEnum.EVENTO},
			new PerfilEnum[] { PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS },
			"pre-inscricoes.jasper", OrientationEnum.LANDSCAPE),

	RELATORIO_LISTA_FREQUENCIA("Lista de Frequência",
			new CampoRelatorioEnum[] {CampoRelatorioEnum.EVENTO, CampoRelatorioEnum.MODULO, CampoRelatorioEnum.CARGO, CampoRelatorioEnum.INICIAL_NOME_PARTICIPANTE},
			new CampoRelatorioEnum[] {CampoRelatorioEnum.EVENTO},
			new PerfilEnum[] { PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS },
			"lista-frequencia.jasper", OrientationEnum.PORTRAIT),

	RELATORIO_MUNICIPIOS_CURSOS("Relatório de municípios participantes e ausentes em curso",
			new CampoRelatorioEnum[] {CampoRelatorioEnum.EVENTO, CampoRelatorioEnum.APROVADOS, CampoRelatorioEnum.DATA_INICIO,
					CampoRelatorioEnum.DATA_FIM},
			new CampoRelatorioEnum[] {CampoRelatorioEnum.EVENTO, CampoRelatorioEnum.APROVADOS},
			new PerfilEnum[] { PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS },
			"municipios-participantes-ausentes.jasper", OrientationEnum.PORTRAIT),

	RELATORIO_UNIDADES_TCM_CURSOS("Relatório de unidades TCM participantes e ausentes em cursos",
			new CampoRelatorioEnum[] {CampoRelatorioEnum.EVENTOS, CampoRelatorioEnum.DATA_INICIO,
					CampoRelatorioEnum.DATA_FIM},
			new CampoRelatorioEnum[] {},
			new PerfilEnum[] { PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS },
			"unidades-tcm-participantes-ausentes.jasper", OrientationEnum.PORTRAIT),

	RELATORIO_CONSOLIDADO_EVENTO("Relatório Consolidado de Evento",
			new CampoRelatorioEnum[] {CampoRelatorioEnum.EVENTO},
			new CampoRelatorioEnum[] {CampoRelatorioEnum.EVENTO},
			new PerfilEnum[] { PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS },
			"consolidado-evento.jasper", OrientationEnum.PORTRAIT),

	RELATORIO_PERFIL_ALUNO("Relatório Perfil de Participante",
			new CampoRelatorioEnum[] {CampoRelatorioEnum.EVENTO, CampoRelatorioEnum.PUBLICO_ALVO, CampoRelatorioEnum.DATA_INICIO, CampoRelatorioEnum.DATA_FIM, CampoRelatorioEnum.PARTICIPANTES_PRESENTES},
			new CampoRelatorioEnum[] {CampoRelatorioEnum.EVENTO},
			new PerfilEnum[] { PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS },
			"perfil-aluno.jasper", OrientationEnum.PORTRAIT),

	RELATORIO_HISTORICO_PARTICIPANTE("Histórico de Participante",
			new CampoRelatorioEnum[] {CampoRelatorioEnum.PARTICIPANTE},
			new CampoRelatorioEnum[] {CampoRelatorioEnum.PARTICIPANTE},
			new PerfilEnum[] { PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS },
			"historico-participante.jasper", OrientationEnum.PORTRAIT),

	RELATORIO_PARTICIPANTES_CURSOS("Relatório de Participantes em Cursos",
			new CampoRelatorioEnum[] {CampoRelatorioEnum.DATA_INICIO, CampoRelatorioEnum.DATA_FIM, CampoRelatorioEnum.IMPORTADO},
			new CampoRelatorioEnum[] {},
			new PerfilEnum[] { PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS },
			"participantes_cursos.jasper", OrientationEnum.LANDSCAPE),

	RELATORIO_HISTORICO_INSTRUTOR("Histórico de Instrutor",
			new CampoRelatorioEnum[] {CampoRelatorioEnum.INSTRUTOR},
			new CampoRelatorioEnum[] {CampoRelatorioEnum.INSTRUTOR},
			new PerfilEnum[] { PerfilEnum.ADMINISTRADOR, PerfilEnum.ESCOLA_DE_CONTAS },
			"historico-instrutor.jasper", OrientationEnum.PORTRAIT);

	private RelatorioEnum(String descricao, CampoRelatorioEnum[] listaCamposRelatorio, 	CampoRelatorioEnum[] listaCamposObrigatoriosRelatorio, PerfilEnum[] listaPerfil, String relatorio, OrientationEnum orientacao) {
		this.descricao = descricao;
		this.listaPerfil = listaPerfil;
		this.listaCamposRelatorio = listaCamposRelatorio;
		this.listaCamposObrigatoriosRelatorio = listaCamposObrigatoriosRelatorio;
		this.relatorio = relatorio;
		this.orientacao = orientacao;
	}

	private String descricao;

	private String relatorio;

	private OrientationEnum orientacao;

	private PerfilEnum[] listaPerfil;

	private CampoRelatorioEnum[] listaCamposRelatorio;

	private CampoRelatorioEnum[] listaCamposObrigatoriosRelatorio;

	public CampoRelatorioEnum[] getListaCamposObrigatoriosRelatorio() {
		return listaCamposObrigatoriosRelatorio;
	}

	public CampoRelatorioEnum[] getListaCamposRelatorio() {
		return listaCamposRelatorio;
	}

	public String getDescricao() {
		return descricao;
	}

	public PerfilEnum[] getListaPerfil() {
		return listaPerfil;
	}

	public List<PerfilEnum> getListPerfil() {
		return Arrays.asList(listaPerfil);
	}

	public String getRelatorio() {
		return relatorio;
	}

	public OrientationEnum getOrientacao() {
		return orientacao;
	}

	public static RelatorioEnum getRelatorio(String descricao){

		for(RelatorioEnum relatorioEnum : RelatorioEnum.values()){

			if(relatorioEnum.getDescricao().equals(descricao)){

				return relatorioEnum;
			}
		}

		return null;
	}

}