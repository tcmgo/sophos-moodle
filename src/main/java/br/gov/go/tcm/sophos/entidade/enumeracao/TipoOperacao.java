/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade.enumeracao;

import br.gov.go.tcm.estrutura.entidade.enumeracao.NumberEnumeracaoPersistente;

public enum TipoOperacao implements NumberEnumeracaoPersistente {

	INCLUSAO(1), ALTERACAO(2), REMOCAO(3);

	/** Atributo valor. */
	private int valor;

	/**
	 * Responsável pela criação de novas instâncias desta classe.
	 * 
	 * @param valor
	 */
	private TipoOperacao( final int valor ) {

		this.valor = valor;
	}

	@Override
	public Number getValorOrdinal() {

		return this.valor;
	}

}
