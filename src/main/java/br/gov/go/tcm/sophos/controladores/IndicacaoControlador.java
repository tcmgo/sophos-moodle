/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Indicacao;
import br.gov.go.tcm.sophos.entidade.Participante;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.lazy.TableLazyIndicacao;
import br.gov.go.tcm.sophos.negocio.IndicacaoNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Scope("session")
public class IndicacaoControlador extends ControladorBaseCRUD<Indicacao, IndicacaoNegocio> {

	private static final long serialVersionUID = 370915393940283176L;

	@Autowired
	private IndicacaoNegocio negocio;

	@Autowired
	private TableLazyIndicacao lista;

	@Autowired
	private EventoControlador eventoControlador;

	@Override
	protected IndicacaoNegocio getNegocio() {

		return this.negocio;
	}

	@Override
	public TableLazyIndicacao getLista() {
		return this.lista;
	}

	public void novaIndicacao() {

		limpaPreenchimentos();
		limpaListaIndicacoes();
	}

	private void limpaPreenchimentos() {

		this.setEntidade(new Indicacao());
		this.getEntidade().setJustificativachefe(new String());
		this.getEntidade().setEvento(new Evento());
		this.getEntidade().setParticipante(new Participante());
	}

	public void limpaListaIndicacoes() {
		this.setEntidades(new ArrayList<Indicacao>());
	}

	public void exibirJustificativa(Indicacao indicacao) {
		this.setEntidade(new Indicacao());
		this.setEntidade(indicacao);
	}

	public void adicionarIndicacaoNaLista() {

		if (!isValidacaoAprovada()) {
			return;
		}

		this.getEntidade().setChefe(this.getUsuarioLogado());
		this.getEntidade().setDataCadastro(new Date());
		List<Indicacao> indicacoes = new ArrayList<Indicacao>();
		
		if (AjudanteObjeto.eReferencia(this.getEntidades())) {
			indicacoes = this.getEntidades();
		}
		
		indicacoes.add(this.getEntidade());
		this.setEntidades(indicacoes);

		Evento eventoSelecionado = this.getEntidade().getEvento();
		this.setEntidade(new Indicacao());
		this.getEntidade().setEvento(eventoSelecionado);
		this.getEntidade().setParticipante(new Participante());

	}

	private boolean isValidacaoAprovada() {

		if (!AjudanteObjeto.eReferencia(this.getEntidade().getEvento())) {
			this.adicionarMensagemDeAlerta(this.obterMensagemPelaChave("informe_evento"));
			return false;
		}

		if (!AjudanteObjeto.eReferencia(this.getEntidade().getParticipante())) {
			this.adicionarMensagemDeAlerta(this.obterMensagemPelaChave("informe_participante"));
			return false;
		}

		if (isIndicacaoJaAdiconada(this.getEntidade())) {
			this.adicionarMensagemDeAlerta(this.obterMensagemPelaChave("indicacao_ja_adicionada"));
			return false;
		}

		if (isIndicacaoJaSalva(this.getEntidade())) {
			this.adicionarMensagemDeAlerta(this.obterMensagemPelaChave("indicacao_ja_salva"));
			return false;
		}

		if (isInscricaoJaRealizada(this.getEntidade())) {
			this.adicionarMensagemDeAlerta(this.obterMensagemPelaChave("inscricao_ja_realizada"));
			return false;
		}

		return true;
	}

	private boolean isIndicacaoJaAdiconada(Indicacao indicacao) {

		if (AjudanteObjeto.eReferencia(this.getEntidades())) {

			for (Indicacao indicacaoDaLista : this.getEntidades()) {

				if (indicacaoDaLista.getEvento().hashCode() == indicacao.getEvento().hashCode()
						&& indicacaoDaLista.getParticipante().hashCode() == indicacao.getParticipante().hashCode()) {
					return true;
				}
			}

		}

		return false;

	}

	private boolean isIndicacaoJaSalva(Indicacao indicacao) {

		return negocio.existeIndicacaoParaParticipante(indicacao.getParticipante(), indicacao.getEvento());
	}

	private boolean isInscricaoJaRealizada(Indicacao indicacao) {

		return negocio.existeInscricaoRealizada(indicacao.getParticipante(), indicacao.getEvento());
	}

	public void removerIndicacaoDaLista(Indicacao indicacao) {
		this.getEntidades().remove(indicacao);
	}

	public void salvarTodaLista() {

		try {

			for (Indicacao indicacao : this.getEntidades()) {

				this.getNegocio().salvar(indicacao);

			}

			limpaListaIndicacoes();
			this.adicionarMensagemInformativa(this.obterMensagemSucessoAoInserir());

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);
		}

	}

	public List<Indicacao> listaNaoAvaliadosPorEvento(Evento evento) {

		if (!AjudanteObjeto.eReferencia(evento) || !AjudanteObjeto.eReferencia(evento.getId())) {
			return new ArrayList<Indicacao>();
		}

		List<Indicacao> indicacoesEvento = new ArrayList<Indicacao>();

		for (Indicacao indicacao : negocio.listaPorAtributo("evento", evento)) {

			if (!AjudanteObjeto.eReferencia(indicacao.getAprovado())) {
				indicacoesEvento.add(indicacao);
			}
		}

		return indicacoesEvento;
	}

	public void aprovar(Indicacao indicacao) {

		try {

			if (negocio.existeInscricaoRealizada(indicacao.getParticipante(), indicacao.getEvento())) {

				this.adicionarMensagemDeAlerta(this.obterMensagemPelaChave("inscricao_ja_realizada"));

			} else {

				negocio.aprovar(indicacao, this.getUsuarioLogado());
				eventoControlador.listarInscricoesEvento(indicacao.getEvento());
				this.adicionarMensagemInformativa(this.obterMensagemPelaChave("indicacao_aprovada"));

			}

		} catch (final ValidacaoException e) {
			this.adicionarMensagemExcecao(e);
		}

	}

	public void reprovar(Indicacao indicacao) {

		try {

			negocio.reprovar(indicacao, this.getUsuarioLogado());
			this.adicionarMensagemInformativa(this.obterMensagemPelaChave("indicacao_reprovada"));

		} catch (final ValidacaoException e) {
			this.adicionarMensagemExcecao(e);
		}

	}
	
	@Override
	public void acaoExcluir(Indicacao indicacao) {
		
		try {
			
			this.getNegocio().excluir(indicacao);
			
		} catch (final ValidacaoException e) {
			this.adicionarMensagemExcecao(e);
		}
		
	}

}
