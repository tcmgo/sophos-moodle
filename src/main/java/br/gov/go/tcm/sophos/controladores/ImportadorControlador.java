/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.controladores.base.ControladorBase;
import br.gov.go.tcm.sophos.importador.ImportadorAluraService;
import br.gov.go.tcm.sophos.importador.base.ItemImportado;
import br.gov.go.tcm.sophos.importador.base.ResultadoImportador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.GregorianCalendar;
import java.util.List;

@Component	
@Scope("session")
public class ImportadorControlador extends ControladorBase implements Serializable {

	private static final long serialVersionUID = -4716947033969461862L;

	@Autowired
	ImportadorAluraService importadorService;

	public void realizarImportacaoAlura() {

		GregorianCalendar calendar = new GregorianCalendar();
		String dia = String.valueOf(calendar.get(GregorianCalendar.DAY_OF_MONTH));
		String mes = String.valueOf(calendar.get(GregorianCalendar.MONTH) + 1);
		String ano = String.valueOf(calendar.get(GregorianCalendar.YEAR));

		if (dia.length() == 1) {
			dia = "0".concat(dia);
		}

		if (mes.length() == 1) {
			mes = "0".concat(mes);
		}

		String dataAtual = dia.concat("/").concat(mes).concat("/").concat(ano);
		String dataInicial = "30/11/2016";

		/**
		 * https://cursos.alura.com.br
		 * Usuário: ? / Senha: ?
		 * Clicar sobre o nome (direita, em cima) / Meus times / Relatórios / Desempenho do Time
		 * Tipos de cursos: marcar: Cursos iniciados no período e Cursos finalizados no período
		 * Categoria: marcar Todas as Categorias
		 * Embaixo, clicar em: "da  nossa API" / Copiar o conteúdo de: Para Get
		 * Adequar as datas
		 */
		String URL = "*url_importacao_cursos_alura*";

		try {

			String serverName = this.getServerName();
			List<ItemImportado> itens = importadorService.importar(URL, serverName);
			ResultadoImportador resultado = importadorService.armazenar(itens);

			if (resultado.getEventosImportados() > 0 || resultado.getInscricoesImportadas() > 0
					|| resultado.getNotasLancadas() > 0 || resultado.getFrequenciasLancadas() > 0
					|| resultado.getCertificadosGerados() > 0) {

				this.adicionarMensagemDeAlerta(
						"Os registros do ambiente de ensino online Alura foram obtidos com sucesso! "
								+ resultado.getEventosImportados() + " cursos, " + resultado.getInscricoesImportadas()
								+ " inscrições, " + resultado.getNotasLancadas() + " notas e "
								+ resultado.getFrequenciasLancadas() + " registros de frequências foram importados."
								+ resultado.getCertificadosGerados()
								+ " certificados foram gerados e armazenados no gerenciador eletrônico de documentos do TCMGO.");

			} else {

				this.adicionarMensagemDeAlerta(
						"Não há registros a serem importados do ambiente de ensino online Alura ou certificados a serem gerados.");

			}

		} catch (Exception e) {

			this.adicionarMensagemDeAlerta("Ocorreu um ERRO ao tentar realizar a conexão ao serviço Alura.");

		}

	}

}
