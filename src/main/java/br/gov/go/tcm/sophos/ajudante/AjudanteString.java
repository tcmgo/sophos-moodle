package br.gov.go.tcm.sophos.ajudante;

public class AjudanteString {

    public static boolean verificarStringNulaOuVazia(String string) {

        if(string != null && !string.trim().isEmpty()) return false;

        return true;
    }
}
