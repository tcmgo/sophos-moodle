/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.ajudante.AjudanteImagem;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.*;
import br.gov.go.tcm.sophos.entidade.dto.FiltroDTO;
import br.gov.go.tcm.sophos.entidade.enumeracao.*;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.lazy.TableLazyEvento;
import br.gov.go.tcm.sophos.lazy.TableLazyGasto;
import br.gov.go.tcm.sophos.lazy.TableLazyInscricao;
import br.gov.go.tcm.sophos.lazy.TableLazyModulo;
import br.gov.go.tcm.sophos.negocio.*;
import br.gov.go.tcm.sophos.persistencia.ModuloInstrutorDAO;
import br.gov.go.tcm.sophos.rest.dto.MoodleCursoDTO;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.CroppedImage;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
@Scope("session")
public class EventoControlador extends ControladorBaseCRUD<Evento, EventoNegocio> {

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private EventoNegocio negocio;

	@Autowired
	private TableLazyEvento lista;

	@Autowired
	private NavegacaoControlador navegacaoControlador;

	@Autowired
	private DominioNegocio dominioNegocio;

	@Autowired
	private UsuarioNegocio usuarioNegocio;

	@Autowired
	private InscricaoNegocio inscricaoNegocio;
	
	@Autowired
	private TableLazyInscricao inscricoesEvento;
	
	@Autowired
	private TableLazyGasto gastosEvento;
	
	@Autowired
	private TableLazyModulo modulosEvento;
	
	@Autowired
	private AnexoNegocio anexoNegocio;
	
	@Autowired
	private CertificadoNegocio certificadoNegocio;
	
	@Autowired
	private ModuloInstrutorDAO moduloInstrutorDAO;
	
	@Autowired
	private RelatorioControlador relatorioControlador;

	@Autowired
	private MoodleNegocio moodleNegocio;
	
	private Usuario usuarioInternoResponsavel;

	private List<Dominio> tipoEventoList;

	private List<Dominio> publicoAlvoList;

	private List<Long> publicoAlvoSelecionadoList;

	private List<Dominio> modalidadeList;

	private List<Dominio> areaTribunalList;

	private List<Dominio> eixoTematicoList;

	private String nomeImagemOriginal;

	private String caminhoImagemOriginal;

	private String nomeImagemRecortada;

	private String consulta;
	
	private String horaFimPreInscricao;

	private int quantidadeInscricoes;

	private int quantidadeInscricoesAutorizadas;

	private int quantidadeInscricoesNegadas;
	
	private Anexo anexoImagemCertificadoFrente;
	
	private Anexo anexoImagemCertificadoVerso;

	private List<MoodleCursoDTO> cursosMoodle = null;

	@PostConstruct
	public void init(){
		
		if (AjudanteObjeto.eReferencia(this.getEntidade())) {
			Calendar c = Calendar.getInstance();
			this.getEntidade().setDataFimPreInscricao(c.getTime());
		}
		
	}
	
	public Boolean verificaDisponibilizarCertificado(Integer rowId){
		
		Integer indice = rowId > 9 ? rowId%10 : rowId;
		
		Evento evento = this.lista.getLista().get(indice);
		
		return evento.getPermiteCertificado() == null || evento.getPermiteCertificado().equals(Boolean.FALSE);
		
	}
	
	public Boolean verificaCancelarCertificado(Integer rowId){
		
		return !this.verificaDisponibilizarCertificado(rowId);
		
	}
	
	public void disponibilizarCertificado(Evento evento) {

		try {

			this.getNegocio().disponibilizarCertificado(evento);
			
			int quantCertificados = this.gerarCertificados(evento);
			int quantDeclaracoesInstrutor = this.gerarDeclaracoesInstrutor(evento);

			this.adicionarMensagemDeAlerta("Os certificados foram disponibilizados para download pelos participantes. No total " + quantCertificados + " certificado(s) e " + quantDeclaracoesInstrutor + " declaração(ões) de instrutor foram gerados com sucesso! Os arquivos foram armazenados no gerenciador eletrônico de documentos do TCMGO.");

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);
		}
	}

	public void cancelarCertificado(Evento evento) {

		try {

			this.getNegocio().cancelarCertificado(evento);

			this.adicionarMensagemInformativa(this.obterMensagemSucessoAoInserir());

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);
		}
	}
	
	public int gerarCertificados(Evento evento) {
		
		int contador = 0;
		
		if (AjudanteObjeto.eReferencia(evento)) {
			
			List<Inscricao> inscricoes = inscricaoNegocio.listarInscricoesAutorizadas(evento);
			
			for (Inscricao inscricao : inscricoes) {
				
				try {
					
					Certificado certificado = null;
					if (AjudanteObjeto.eReferencia(inscricao.getParticipante())) {
						certificado = certificadoNegocio.obterCertificado(evento, inscricao.getParticipante(), true);
					}
					
					if (AjudanteObjeto.eReferencia(certificado)) {
						contador++;
					}
					
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
			
		}
		
		return contador;
		
	}
	
	public int gerarDeclaracoesInstrutor(Evento evento) {
		
		int contador = 0;
		
		if (AjudanteObjeto.eReferencia(evento)) {
			
			List<ModuloInstrutor> modulosInstrutores = moduloInstrutorDAO.listarInstrutoresEvento(evento);
			
			for (ModuloInstrutor moduloInstrutor : modulosInstrutores) {
				
				try {
					
					String retorno = null;
					if (AjudanteObjeto.eReferencia(moduloInstrutor) && AjudanteObjeto.eReferencia(moduloInstrutor.getInstrutor())) {
						retorno = relatorioControlador.gerarDeclaracaoInstrutorSemDownload(evento, moduloInstrutor.getInstrutor());
					}
					
					if (AjudanteObjeto.eReferencia(retorno)) {
						contador++;
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		}
		
		return contador;
	}
	
	public void exibirOcultarNaHome(Evento evento) {

		try {

			this.getNegocio().exibirOcultarNaHome(evento);

			this.adicionarMensagemInformativa(this.obterMensagemSucessoAoInserir());

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);
		}
	}

	public void aprovarInscricao(Inscricao inscricao) {

		this.inscricaoNegocio.aprovarInscricao(inscricao, this.getUsuarioLogado());

		obterQuantitativos();

		this.adicionarMensagemInformativa("Inscrição no Sophos autorizada com sucesso!");

		try {

			if (this.moodleNegocio.eventoTemCursoMoodleVinculado(inscricao.getEvento())) {

				boolean sucesso = this.moodleNegocio.inscreverParticipanteCursoMoodleVinculado(inscricao.getEvento(), inscricao);

				String mensagem = sucesso? "Participante foi matriculado no Curso Moodle correspondente com sucesso!"
						: "Participante não foi matriculado no Curso Moodle correspondente. Por favor, verifique no Moodle.";

				this.adicionarMensagemInformativa(mensagem);
			}
		} catch (ValidacaoException e) {

			this.adicionarMensagemInformativa(e.getMessage());
		}
	}

	public void reprovarInscricao(Inscricao inscricao) {

		this.inscricaoNegocio.reprovarInscricao(inscricao, this.getUsuarioLogado());

		obterQuantitativos();

		this.adicionarMensagemInformativa("Inscrição no Sophos negada com sucesso!");

		try {

			if (this.moodleNegocio.eventoTemCursoMoodleVinculado(inscricao.getEvento())) {

				boolean sucesso = this.moodleNegocio.desinscreverParticipanteCursoMoodleVinculado(inscricao.getEvento(), inscricao);

				String mensagem = sucesso ? "Matrícula do Participante no Curso Moodle correspondente foi retirada com sucesso!"
						: "Matrícula do Participante no Curso Moodle correspondente não foi retirada. Por favor, verifique no Moodle.";

				this.adicionarMensagemInformativa(mensagem);
			}
		} catch (ValidacaoException e) {

			this.adicionarMensagemInformativa(e.getMessage());
		}
	}

	public void limparAnexoImagem() {
		this.getEntidade().setImagem(null);
	}
	
	public void limparAnexoImagemCertificadoFrente() {
		this.setAnexoImagemCertificadoFrente(null);
	}
	
	public void limparAnexoImagemCertificadoVerso() {
		this.setAnexoImagemCertificadoVerso(null);
	}

	@Override
	protected void executarAposInserirSucesso() {

		super.executarAposInserirSucesso();

		this.navegacaoControlador.processarNavegacao(PaginaEnum.EVENTOS);
	}

	@SuppressWarnings("deprecation")
	public void acaoInserir() {
		
		SimpleDateFormat sdf = null;
		List<Anexo> anexos = new ArrayList<Anexo>();

		try {

			if(AjudanteObjeto.eReferencia(this.getEntidade().getPermitePreInscricao()) && this.getEntidade().getPermitePreInscricao().booleanValue()
					&& (!AjudanteObjeto.eReferencia(this.getEntidade().getCursoMoodleId()) || this.getEntidade().getCursoMoodleId().intValue() == 0)) {

				if(this.getHoraFimPreInscricao().equals("")){

					throw new ValidacaoException("Campos Obrigatórios: HORÁRIO FIM PRÉ-INSCRIÇÃO");
				}
			}
			if (AjudanteObjeto.eReferencia(anexoImagemCertificadoFrente) && !AjudanteObjeto.eReferencia(anexoImagemCertificadoVerso)) {
				throw new ValidacaoException("Por favor, além da imagem de frente de certificado, envie uma imagem de verso de certificado.");
			}
			
			if (!AjudanteObjeto.eReferencia(anexoImagemCertificadoFrente) && AjudanteObjeto.eReferencia(anexoImagemCertificadoVerso)) {	
				throw new ValidacaoException("Por favor, além da imagem de verso de certificado, envie uma imagem de frente de certificado.");
			}
			
			if(this.getEntidade().getDataFimPreInscricao()!=null){
				String data = this.getHoraFimPreInscricao();
				int hora = Integer.parseInt(data.charAt(0)+"")*10 + Integer.parseInt(data.charAt(1)+"");
				int minuto = Integer.parseInt(data.charAt(3)+"")*10 + Integer.parseInt(data.charAt(4)+"");
				
				if(hora>23 || minuto>59 || hora<0 || minuto <0){
					throw new ValidacaoException("O horário do fim da pré-inscrição está fora do limite.");
				}
				
				sdf = new SimpleDateFormat("HH:mm");
				Calendar cal = Calendar.getInstance();
				
				cal.setTime(this.getEntidade().getDataFimPreInscricao());
				cal.set(this.getEntidade().getDataFimPreInscricao().getYear() + 1900, this.getEntidade().getDataFimPreInscricao().getMonth(), this.getEntidade().getDataFimPreInscricao().getDate(), 
						sdf.parse(data).getHours(), sdf.parse(data).getMinutes(), 0);
				
				this.getEntidade().setDataFimPreInscricao(cal.getTime());
				
			}
			

			this.getEntidade().setTitulo(this.getEntidade().getTitulo().toUpperCase());

			this.getEntidade().setResponsavelEvento(this.getUsuarioInternoResponsavel().getParticipante());

			this.getEntidade().setPublicoAlvoList(new ArrayList<EventoPublicoAlvo>());
			
			for (Long idPublicoAlvo : publicoAlvoSelecionadoList) {

				EventoPublicoAlvo eventoPublicoAlvo = new EventoPublicoAlvo();

				eventoPublicoAlvo.setEvento(this.getEntidade());

				eventoPublicoAlvo.setDataCadastro(new Date());

				eventoPublicoAlvo.setPublicoAlvo(this.dominioNegocio.obter(idPublicoAlvo));

				this.getEntidade().getPublicoAlvoList().add(eventoPublicoAlvo);
			}
			
			if (AjudanteObjeto.eReferencia(this.anexoImagemCertificadoFrente)) {
				
				this.anexoNegocio.salvar(this.anexoImagemCertificadoFrente);
				this.getEntidade().setImagemCertificadoFrente(this.anexoImagemCertificadoFrente);
				
			} else {
				
				if (AjudanteObjeto.eReferencia(this.getEntidade().getImagemCertificadoFrente())) {
					anexos.add(this.getEntidade().getImagemCertificadoFrente());
					this.getEntidade().setImagemCertificadoFrente(null);
				}
				
			}
			
			if (AjudanteObjeto.eReferencia(anexoImagemCertificadoVerso)) {
				
				this.anexoNegocio.salvar(this.anexoImagemCertificadoVerso);
				this.getEntidade().setImagemCertificadoVerso(this.anexoImagemCertificadoVerso);
				
			} else {
				
				if (AjudanteObjeto.eReferencia(this.getEntidade().getImagemCertificadoVerso())) {
					anexos.add(this.getEntidade().getImagemCertificadoVerso());
					this.getEntidade().setImagemCertificadoVerso(null);
				}
				
			}
			
			this.getNegocio().salvar(this.getEntidade());
			
			this.removerAnexos(anexos);

			this.adicionarMensagemInformativa(this.obterMensagemSucessoAoInserir());

			this.executarAposInserirSucesso();

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void removerAnexos(List<Anexo> anexos) {
		
		for (Anexo anexo : anexos) {
			this.anexoNegocio.excluir(anexo);
		}
	}

	public void acaoAbrirTela(Evento entidade, Boolean visualizar) {

		if (AjudanteObjeto.eReferencia(entidade)) {

			this.usuarioInternoResponsavel = this.usuarioNegocio.obterPorAtributo("participante", entidade.getResponsavelEvento());

			entidade.setPublicoAlvoList(this.negocio.obterPublicoAlvo(entidade));

			this.setPublicoAlvoSelecionadoList(new ArrayList<Long>());

			for (EventoPublicoAlvo publicoAlvo : entidade.getPublicoAlvoList()) {

				this.getPublicoAlvoSelecionadoList().add(publicoAlvo.getPublicoAlvo().getId());
			}

			this.setEntidade(entidade);
			
			if(this.getEntidade().getDataFimPreInscricao()!=null){
				Calendar c = Calendar.getInstance();
				c.setTime(this.getEntidade().getDataFimPreInscricao());
				if(c.getTime().getHours()!=0) {
					if(this.getEntidade().getDataFimPreInscricao().getMinutes()!=0)this.setHoraFimPreInscricao("" + this.getEntidade().getDataFimPreInscricao().getHours() + "" + this.getEntidade().getDataFimPreInscricao().getMinutes() + "");
					else this.setHoraFimPreInscricao("" + this.getEntidade().getDataFimPreInscricao().getHours() + "00");
					
				} else{
					if(this.getEntidade().getPermitePreInscricao()) this.setHoraFimPreInscricao("2359");
					else this.setHoraFimPreInscricao("");
				}
				
			} else{
				
				this.setHoraFimPreInscricao("");
				
			}
			
			obterQuantitativos();
			
			if (AjudanteObjeto.eReferencia(this.anexoImagemCertificadoFrente)) {
				this.anexoImagemCertificadoFrente = null;
			}
			
			if (AjudanteObjeto.eReferencia(this.anexoImagemCertificadoVerso)) {
				this.anexoImagemCertificadoVerso = null;
			}
			
			if (AjudanteObjeto.eReferencia(this.getEntidade().getImagemCertificadoFrente())) {
				this.anexoImagemCertificadoFrente = this.anexoNegocio.obterPorAtributo("arquivoGED", this.getEntidade().getImagemCertificadoFrente().getArquivoGED());
			}
			
			if (AjudanteObjeto.eReferencia(this.getEntidade().getImagemCertificadoVerso())) {
				this.anexoImagemCertificadoVerso = this.anexoNegocio.obterPorAtributo("arquivoGED", this.getEntidade().getImagemCertificadoVerso().getArquivoGED());
			}

		} else {

			this.criarNovaInstanciaEntidade();
			
			if(this.getEntidade().getDataFimPreInscricao()==null){
				horaFimPreInscricao = "";
			} else{
				horaFimPreInscricao = "" + this.getEntidade().getDataFimPreInscricao().getHours() + "" + this.getEntidade().getDataFimPreInscricao().getMinutes() + "";
			}

			
		}
		
		this.setVisualizar(visualizar);

		this.navegacaoControlador.processarNavegacao(PaginaEnum.EVENTOS_CADASTRO);

	}
	
	public void obterQuantitativos() {
		this.quantidadeInscricoes = this.inscricaoNegocio.obterQuantidadeTotal(this.getEntidade());
		this.quantidadeInscricoesAutorizadas = this.inscricaoNegocio.obterQuantidadeAutorizada(this.getEntidade());
		this.quantidadeInscricoesNegadas = this.inscricaoNegocio.obterQuantidadeNegada(this.getEntidade());
	}

	public List<Inscricao> filtrarInscricoes() {

		if (AjudanteObjeto.eReferencia(this.getEntidade()) && AjudanteObjeto.eReferencia(this.getEntidade().getId())) {

			List<FiltroDTO> filtros = new ArrayList<FiltroDTO>();
			FiltroDTO filtroEvento = new FiltroDTO("evento", this.getEntidade(), ComparadorCriteriaEnum.EQ,
					TipoLogicoEnum.AND);
			FiltroDTO filtroNomeParticipante = new FiltroDTO("participantepessoa.nome", this.consulta,
					ComparadorCriteriaEnum.LIKE, TipoLogicoEnum.OR);
			FiltroDTO filtroNomeTipo = new FiltroDTO("participantetipo.descricao", this.consulta,
					ComparadorCriteriaEnum.LIKE, TipoLogicoEnum.OR);
			FiltroDTO filtroNomeLotacao = new FiltroDTO("participante.lotacao", this.consulta,
					ComparadorCriteriaEnum.LIKE, TipoLogicoEnum.OR);
			FiltroDTO filtroNomeCargo = new FiltroDTO("participante.cargo", this.consulta, ComparadorCriteriaEnum.LIKE,
					TipoLogicoEnum.OR);
			filtros.add(filtroEvento);
			filtros.add(filtroNomeParticipante);
			filtros.add(filtroNomeTipo);
			filtros.add(filtroNomeLotacao);
			filtros.add(filtroNomeCargo);

			List<Inscricao> inscricoes = new ArrayList<Inscricao>();
			
			inscricoes = this.inscricaoNegocio.listaPorFiltros(filtros, "participante",
					"participante.pessoa", "participante.tipo");
			
			this.quantidadeInscricoes = inscricoes.size();
			this.quantidadeInscricoesAutorizadas = 0;
			this.quantidadeInscricoesNegadas = 0;
			
			for (Inscricao inscricao : inscricoes) {
				if (AjudanteObjeto.eReferencia(inscricao.getAutorizada()) && inscricao.getAutorizada()) {
					this.quantidadeInscricoesAutorizadas++;
				} else if (AjudanteObjeto.eReferencia(inscricao.getAutorizada()) && !inscricao.getAutorizada()) {
					this.quantidadeInscricoesNegadas++;
				}
			}

			return inscricoes;

		} else {

			return new ArrayList<Inscricao>();

		}

	}

	public List<Inscricao> obterInscricoes() {

		List<FiltroDTO> filtros = new ArrayList<FiltroDTO>();
		FiltroDTO filtroEvento = new FiltroDTO("evento", this.getEntidade(), ComparadorCriteriaEnum.EQ);
		FiltroDTO filtroNomeParticipante = new FiltroDTO("participante", this.getUsuarioLogado().getParticipante(),
				ComparadorCriteriaEnum.LIKE);
		filtros.add(filtroEvento);
		filtros.add(filtroNomeParticipante);

		List<Inscricao> inscricoes = this.inscricaoNegocio.listaPorFiltros(filtros);

		return inscricoes;

	}

	public void listarInscricoesEvento(Evento evento) {

		this.getEntidade().setInscricaoList(this.inscricaoNegocio.listaPorAtributo("evento", evento, SortOrder.DESCENDING, "dataCadastro"));
	}

	public void fileUploadAction(FileUploadEvent event) {

		try {

			limparImagensFisicas();

			AjudanteImagem.validarImagem(event.getFile().getContents());

			String caminhoReal = obterCaminhoReal();

			File file = new File(caminhoReal + "/resources/imagens/upload/");

			file.mkdirs();

			caminhoImagemOriginal = caminhoReal + "resources" + File.separator + "imagens" + File.separator + "upload"
					+ File.separator + event.getFile().getFileName();

			setNomeImagemOriginal(event.getFile().getFileName());

			FileOutputStream fos = new FileOutputStream(caminhoImagemOriginal);

			fos.write(event.getFile().getContents());

			fos.close();

		} catch (Exception ex) {

			this.adicionarMensagemDeErro(ex.getMessage());
		}
	}

	public void salvarImagemRecortada() {

		try {

			byte[] imagemRecortada = obterImagemRecortada().getBytes();

			AjudanteImagem.validarImagem(imagemRecortada);

			this.getEntidade().setImagem(new Anexo());

			this.getEntidade().getImagem().setDataCadastro(new Date());

			this.getEntidade().getImagem().setBytes(imagemRecortada);

			this.getEntidade().getImagem().setDescricao(nomeImagemRecortada);

			this.getEntidade().getImagem().setTipoArquivo(TipoArquivo.JPG);

		} catch (Exception ex) {

			this.adicionarMensagemDeErro(ex.getMessage());

		} finally {

			limparImagensFisicas();

			limparImagensSessao();
		}

	}

	private CroppedImage obterImagemRecortada() {

		Double croopX = null;
		Double croopY = null;
		Double croopW = null;
		Double croopH = null;
		Double boundx = null;
		Double boundy = null;

		try {

			croopX = Double.valueOf(this.obterObjetoDaRequisicao().getParameter("croop-x"));
			croopY = Double.valueOf(this.obterObjetoDaRequisicao().getParameter("croop-y"));
			croopW = Double.valueOf(this.obterObjetoDaRequisicao().getParameter("croop-w"));
			croopH = Double.valueOf(this.obterObjetoDaRequisicao().getParameter("croop-h"));
			boundx = Double.valueOf(this.obterObjetoDaRequisicao().getParameter("boundx"));
			boundy = Double.valueOf(this.obterObjetoDaRequisicao().getParameter("boundy"));

		} catch (Exception ex) {

			setNomeImagemRecortada(UUID.randomUUID().toString());
			return AjudanteImagem.cropImage(caminhoImagemOriginal, getNomeImagemRecortada());

		}

		setNomeImagemRecortada(UUID.randomUUID().toString());
		return AjudanteImagem.cropImage(croopX.intValue(), croopY.intValue(), croopW.intValue(), croopH.intValue(),
				boundx.intValue(), boundy.intValue(), caminhoImagemOriginal, getNomeImagemRecortada());

	}

	private void limparImagensFisicas() {

		if (!AjudanteObjeto.eVazia(caminhoImagemOriginal)) {

			new File(caminhoImagemOriginal).delete();
		}

	}

	private void limparImagensSessao() {

		this.nomeImagemOriginal = null;
		this.caminhoImagemOriginal = null;
		this.nomeImagemRecortada = null;
	}

	@Override
	protected void criarNovaInstanciaEntidade() {

		this.setEntidade(new Evento());

		this.getEntidade().setDataCadastro(new Date());

		this.getEntidade().setProvedor(new ProvedorEvento());

		this.getEntidade().setLocalizacao(new LocalizacaoEvento());

		this.getEntidade().setPublicoAlvoList(new ArrayList<EventoPublicoAlvo>());

		this.usuarioInternoResponsavel = new Usuario();

		this.usuarioInternoResponsavel.setPessoa(new Pessoa());

		this.publicoAlvoSelecionadoList = new ArrayList<Long>();
		
		this.anexoImagemCertificadoFrente = null;
		
		this.anexoImagemCertificadoVerso = null;
	}

	public Map<String, Object> getFiltersParticipante() {

		Map<String, Object> filters = new HashMap<String, Object>();

		filters.put("tipoUsuario",
				dominioNegocio.obterDominio(TipoDominioEnum.TipoUsuario, TipoUsuario.USUARIO_INTERNO.getCodigo()));

		filters.put("participante", FilterEnum.NOT_NULL);

		return filters;
	}

	public List<Dominio> getTipoEventoList() {

		if (!AjudanteObjeto.eReferencia(tipoEventoList)) {

			this.tipoEventoList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.TipoEvento);
		}
		return tipoEventoList;
	}

	public List<Dominio> getPublicoAlvoList() {

		if (!AjudanteObjeto.eReferencia(publicoAlvoList)) {

			this.publicoAlvoList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.TipoPublicoAlvo);
		}
		return publicoAlvoList;
	}

	public List<Dominio> getModalidadeList() {

		if (!AjudanteObjeto.eReferencia(modalidadeList)) {

			this.modalidadeList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.Modalidade);
		}
		return modalidadeList;
	}

	public List<Dominio> getAreaTribunalList() {

		if (!AjudanteObjeto.eReferencia(areaTribunalList)) {

			this.areaTribunalList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.TipoAreaTribunal);
		}
		return areaTribunalList;
	}

	public List<Dominio> getEixoTematicoList() {

		if (!AjudanteObjeto.eReferencia(eixoTematicoList)) {

			this.eixoTematicoList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.EixoTematico);
		}
		return eixoTematicoList;
	}

	@Override
	public TableLazyEvento getLista() {

		return this.lista;
	}

	@Override
	protected EventoNegocio getNegocio() {

		return this.negocio;
	}

	public boolean isMostrarPrevisao(Evento evento) {

		if (AjudanteObjeto.eReferencia(evento.getDataInicioRealizacao())
				&& AjudanteObjeto.eReferencia(evento.getDataFimRealizacao())) {
			return false;
		} else {
			return true;
		}
	}
	
	public Integer obterNumeroInscricoes(Evento evento) {
		
		Integer qtInscricoes = 0;
		
		if (AjudanteObjeto.eReferencia(evento)) {
			qtInscricoes = evento.getInscricaoList().size();
		}
		return qtInscricoes;
	}
	
	public Integer obterNumeroIndicacoes(Evento evento) {
		
		Integer qtIndicacoes = 0;
		
		if (AjudanteObjeto.eReferencia(evento)) {
			qtIndicacoes = evento.getIndicacaoList().size();
		}
		return qtIndicacoes;
	}

	@Override
	public void acaoExcluir(Evento evento) {
		try {
			
		negocio.excluir(evento);
		
		} catch (ValidacaoException e) {
			this.adicionarMensagemDeAlerta(e.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Usuario getUsuarioInternoResponsavel() {

		return usuarioInternoResponsavel;
	}

	public void setUsuarioInternoResponsavel(Usuario usuarioInternoResponsavel) {

		if (usuarioInternoResponsavel != null) {
			this.usuarioInternoResponsavel = usuarioInternoResponsavel;
		}
	}

	public List<Long> getPublicoAlvoSelecionadoList() {

		return publicoAlvoSelecionadoList;
	}

	public void setPublicoAlvoSelecionadoList(List<Long> publicoAlvoSelecionadoList) {

		this.publicoAlvoSelecionadoList = publicoAlvoSelecionadoList;
	}

	public String getNomeImagemOriginal() {
		return nomeImagemOriginal;
	}

	public void setNomeImagemOriginal(String nomeImagemOriginal) {
		this.nomeImagemOriginal = nomeImagemOriginal;
	}

	public String getCaminhoImagemOriginal() {
		return caminhoImagemOriginal;
	}

	public void setCaminhoImagemOriginal(String caminhoImagemOriginal) {
		this.caminhoImagemOriginal = caminhoImagemOriginal;
	}

	public String getNomeImagemRecortada() {
		return nomeImagemRecortada;
	}

	public void setNomeImagemRecortada(String nomeImagemRecortada) {
		this.nomeImagemRecortada = nomeImagemRecortada;
	}

	public String getConsulta() {
		return consulta;
	}

	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}

	public int getQuantidadeInscricoes() {
		return quantidadeInscricoes;
	}

	public void setQuantidadeInscricoes(int quantidadeInscricoes) {
		this.quantidadeInscricoes = quantidadeInscricoes;
	}

	public int getQuantidadeInscricoesAutorizadas() {
		return quantidadeInscricoesAutorizadas;
	}

	public void setQuantidadeInscricoesAutorizadas(int quantidadeInscricoesAutorizadas) {
		this.quantidadeInscricoesAutorizadas = quantidadeInscricoesAutorizadas;
	}

	public int getQuantidadeInscricoesNegadas() {
		return quantidadeInscricoesNegadas;
	}

	public void setQuantidadeInscricoesNegadas(int quantidadeInscricoesNegadas) {
		this.quantidadeInscricoesNegadas = quantidadeInscricoesNegadas;
	}

	public TableLazyInscricao getInscricoesEvento() {
		
		Map<String, Object> filters = new HashMap<String, Object>();

		if (AjudanteObjeto.eReferencia(this.getEntidade())) {

			filters.put("evento",  this.getEntidade());
		}
		this.inscricoesEvento.setFilters(filters);
		
		return inscricoesEvento;
	}

	public void setInscricoesEvento(TableLazyInscricao inscricoesEvento) {
		this.inscricoesEvento = inscricoesEvento;
	}

	public TableLazyGasto getGastosEvento() {
		
		Map<String, Object> filters = new HashMap<String, Object>();

		if (AjudanteObjeto.eReferencia(this.getEntidade())) {

			filters.put("evento",  this.getEntidade());
		}
		this.gastosEvento.setFilters(filters);
		
		return gastosEvento;
	}

	public void setGastosEvento(TableLazyGasto gastosEvento) {
		this.gastosEvento = gastosEvento;
	}

	public TableLazyModulo getModulosEvento() {
		
		Map<String, Object> filters = new HashMap<String, Object>();

		if (AjudanteObjeto.eReferencia(this.getEntidade())) {

			filters.put("evento",  this.getEntidade());
			
		}

		this.modulosEvento.setFilters(filters);
		
		return modulosEvento;
	}

	public void setModulosEvento(TableLazyModulo modulosEvento) {
		this.modulosEvento = modulosEvento;
	}
	
	public String getHoraFimPreInscricao() {
		return horaFimPreInscricao;
	}

	public void setHoraFimPreInscricao(String horaFimPreInscricao) {
		this.horaFimPreInscricao = horaFimPreInscricao;
	}
	
	public Anexo getAnexoImagemCertificadoFrente() {
		return anexoImagemCertificadoFrente;
	}

	public void setAnexoImagemCertificadoFrente(Anexo anexoImagemCertificadoFrente) {
		this.anexoImagemCertificadoFrente = anexoImagemCertificadoFrente;
	}

	public Anexo getAnexoImagemCertificadoVerso() {
		return anexoImagemCertificadoVerso;
	}

	public void setAnexoImagemCertificadoVerso(Anexo anexoImagemCertificadoVerso) {
		this.anexoImagemCertificadoVerso = anexoImagemCertificadoVerso;
	}
	
	public void uploadImagem(final FileUploadEvent event) {

		this.getEntidade().setImagem(new Anexo());

		this.getEntidade().getImagem().setDataCadastro(new Date());

		this.getEntidade().getImagem().setFile(event.getFile());

		this.getEntidade().getImagem().setDescricao(event.getFile().getFileName());

		this.getEntidade().getImagem().setTipoArquivo(TipoArquivo.getTipoArquivo(event.getFile().getContentType()));

	}

	public void uploadImagemFrenteCertificado(final FileUploadEvent eventoFrente) {

		this.anexoImagemCertificadoFrente = new Anexo();

		this.anexoImagemCertificadoFrente.setFile(eventoFrente.getFile());

		this.anexoImagemCertificadoFrente.setDescricao(UUID.randomUUID().toString());

		this.anexoImagemCertificadoFrente.setTipoArquivo(TipoArquivo.getTipoArquivo(eventoFrente.getFile().getContentType()));
	}

	public void uploadImagemVersoCertificado(final FileUploadEvent eventoVerso) {

		this.anexoImagemCertificadoVerso = new Anexo();

		this.anexoImagemCertificadoVerso.setFile(eventoVerso.getFile());

		this.anexoImagemCertificadoVerso.setDescricao(UUID.randomUUID().toString());

		this.anexoImagemCertificadoVerso.setTipoArquivo(TipoArquivo.getTipoArquivo(eventoVerso.getFile().getContentType()));
	}

	public List<MoodleCursoDTO> getCursosMoodle() {

		try {

			this.cursosMoodle = this.moodleNegocio.obterListaCursosMoodle();

		} catch (Exception e) {

			e.printStackTrace();
		}
		return cursosMoodle;
	}

	public boolean eventoPodeVincularCursoMoodle() {

		return this.moodleNegocio.eventoPodeVincularCursoMoodle(this.getEntidade());
	}

	public boolean eventoTemCursoMoodleVinculado() {

		return this.moodleNegocio.eventoTemCursoMoodleVinculado(this.getEntidade());
	}
}
