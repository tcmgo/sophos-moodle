package br.gov.go.tcm.sophos.rest.dto;

import java.io.Serializable;

public class ScaPresencaDoQRCodeDTO implements Serializable {

    private Long eventoId;

    private Long participanteId;

    private Integer numeroEncontro;

    private Long moduloId;

    public Long getEventoId() {
        return eventoId;
    }

    public void setEventoId(Long eventoId) {
        this.eventoId = eventoId;
    }

    public Long getParticipanteId() {
        return participanteId;
    }

    public void setParticipanteId(Long participanteId) {
        this.participanteId = participanteId;
    }

    public Integer getNumeroEncontro() {
        return numeroEncontro;
    }

    public void setNumeroEncontro(Integer numeroEncontro) {
        this.numeroEncontro = numeroEncontro;
    }

    public Long getModuloId() {
        return moduloId;
    }

    public void setModuloId(Long moduloId) {
        this.moduloId = moduloId;
    }
}
