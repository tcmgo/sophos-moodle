/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteData;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Indicacao;
import br.gov.go.tcm.sophos.entidade.Inscricao;
import br.gov.go.tcm.sophos.entidade.enumeracao.PaginaEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoArquivo;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.lazy.TableLazyInscricao;
import br.gov.go.tcm.sophos.negocio.InscricaoNegocio;
import br.gov.go.tcm.sophos.negocio.MoodleNegocio;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.*;

@Component
@Scope("session")
public class InscricaoControlador extends ControladorBaseCRUD<Inscricao, InscricaoNegocio> {

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private InscricaoNegocio negocio;

	@Autowired
	private TableLazyInscricao lista;

	@Autowired
	private NavegacaoControlador navegacaoControlador;
	
	@Autowired
	private InscricaoNegocio inscricaoNegocio;

	@Autowired
	private MoodleNegocio moodleNegocio;

	private List<Inscricao> inscricoesSelecionadas;
	
	private boolean selecionar = true;
	
	private boolean modalPreInscricao = true;

	private String senhaParaMoodle = "";

	public void preRenderView() {

		if (AjudanteObjeto.eReferencia(this.getEntidade().getEvento()) && modalPreInscricao) {

			this.selecionarEvento(this.getEntidade().getEvento());
		}
	}

	@Override
	public void acaoInserir() {

		try {

			this.getNegocio().salvar(this.getEntidade());

			Evento evento = this.getEntidade().getEvento();

			if(evento.getCursoMoodleId() != null) {
				
				this.adicionarMensagemInformativa(this.obterMensagemSucessoAoInserir() + 
						" Link de acesso ao evento: " +
						"https://<dominio_acesso_moodle>/ead/course/view.php?id=" +
						 evento.getCursoMoodleId());
			}else {
				this.adicionarMensagemInformativa(this.obterMensagemSucessoAoInserir());
			}

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);
		}

	}

	public void selecionarEvento(Evento evento) {

		this.setEntidade(new Inscricao());

		this.getEntidade().setDataCadastro(new Date());

		this.getEntidade().setEvento(evento);

		if (AjudanteObjeto.eReferencia(this.getEntidade().getEvento().getPermitePreInscricao())) {

			this.executarJS("openModal('modalInscricao')");
		}
	}

	public void selecionarEventoSemLogin(Evento evento) {

		this.getEntidade().setEvento(evento);

		this.navegacaoControlador.processarNavegacao(PaginaEnum.ACESSO);
	}

	public void realizarInscricao() {

		Boolean autorizada = this.negocio.verificaAutorizacaoInscricao(this.getEntidade().getEvento());

		this.getEntidade().setAutorizada(autorizada);

		this.getEntidade().setPreInscricao(this.getEntidade().getEvento().getPermitePreInscricao());

		this.getEntidade().setParticipante(this.getUsuarioLogado().getParticipante());

		if (this.moodleNegocio.eventoTemCursoMoodleVinculado(this.getEntidade().getEvento())) { //Se o evento tem sincronização com o Moodle

			if (this.inscricaoValida()) { //Se a inscrição Sophos pode ser realizada

				if (this.realizarInscricaoMoodle()) { //Fazer a inscrição no Moodle e verificar se foi sucedida

					this.acaoInserir(); //Realizar inscricao Sophos
				}
			} else {

				this.acaoInserir(); //Se a inscricao Sophos nao pode ser realizada, tentar fazer para retornar problemas ao usuario
			}
		} else {

			this.acaoInserir(); //Se o evento não tem sincronização com o Moodle, seguir fluxo normal
		}
		this.executarAposInserirSucesso(); //Se inscricao Sophos foi feita ou não, este método deve ser chamado para resetar o estado do controlador

		this.setEntidade(new Inscricao());
	}

	public boolean inscricaoValida() {

		try {

			this.negocio.executarValidacaoAntesDeSalvar(this.getEntidade());

		} catch (Exception e) {

			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	public boolean realizarInscricaoMoodle() {

		try {

			if (this.moodleNegocio.eventoTemCursoMoodleVinculado(this.getEntidade().getEvento())) {

				if (!this.moodleNegocio.participanteExisteNoMoodle(this.getUsuarioLogado().getParticipante())) {

					this.moodleNegocio.forcarPrimeiroLoginMoodle(this.getEntidade(), this.getUsuarioLogado(), this.senhaParaMoodle);
				}
				if (!this.moodleNegocio.inscreverParticipanteCursoMoodleVinculado(this.getEntidade().getEvento(), this.getEntidade())) {

					this.adicionarMensagemInformativa("Ocorreu um erro durante a inscrição no Moodle. Por favor, entre em contato com a Escola de Contas.");

					return Boolean.FALSE;
				}
				this.senhaParaMoodle = "";
			}
		} catch (ValidacaoException e) {

			this.adicionarMensagemInformativa(e.getMessage());

			return Boolean.FALSE;

		} catch (Exception e) {

			e.printStackTrace();

			this.adicionarMensagemInformativa("Ocorreu um erro durante a inscrição no Moodle. Por favor, entre em contato com a Escola de Contas.");

			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	public boolean participanteExisteNoMoodle() {

		return this.moodleNegocio.participanteExisteNoMoodle(this.getUsuarioLogado().getParticipante());
	}

	public boolean eventoTemCursoMoodleVinculado() {

		return this.moodleNegocio.eventoTemCursoMoodleVinculado(this.getEntidade().getEvento());
	}

	public void naoConfirmarInscricao() {
		this.fechaModal();
	}

	@Override
	protected void executarAposInserirSucesso() {
		this.fechaModal();
	}

	private void fechaModal() {

		RequestContext requestContext = RequestContext.getCurrentInstance();

		requestContext.execute("fecharModal('modalInscricao')");

		this.setEntidade(new Inscricao());

		this.senhaParaMoodle = "";
	}

	public void exibirJustificativaPreInscricao(Inscricao inscricao) {
		if (AjudanteObjeto.eReferencia(inscricao) && AjudanteObjeto.eReferencia(inscricao.getId())) {
			this.setEntidade(new Inscricao());
			this.getEntidade().setIndicacao(new Indicacao());
			this.setEntidade(inscricao);
			this.modalPreInscricao = false;
		}
	}

	@Override
	public TableLazyInscricao getLista() {

		Map<String, Object> filters = new HashMap<String, Object>();

		if (AjudanteObjeto.eReferencia(this.getUsuarioLogado().getParticipante())) {

			filters.put("participante", this.getUsuarioLogado().getParticipante());

		} else {

			filters.put("participante", null);
		}

		this.lista.setFilters(filters);

		return lista;
	}

	@Override
	protected InscricaoNegocio getNegocio() {

		return negocio;
	}

	public boolean verificaPermissaoExcluir(Inscricao inscricao) {
		
		if (!AjudanteObjeto.eReferencia(inscricao) || !AjudanteObjeto.eReferencia(inscricao.getEvento())) {
			return false;
		}

		Date dataInicio;
		if (AjudanteObjeto.eReferencia(inscricao.getEvento().getDataInicioRealizacao())) {
			dataInicio = inscricao.getEvento().getDataInicioRealizacao();
		} else {
			dataInicio = inscricao.getEvento().getDataInicioPrevisto();
		}

		if (AjudanteData.dataMaiorDataAtual(dataInicio)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean verificaPermissaoComprovanteInscricao(Inscricao inscricao) {

		if (!AjudanteObjeto.eReferencia(inscricao) || !AjudanteObjeto.eReferencia(inscricao.getEvento())) {

			return false;
		}

		Date dataFinal;

		if (AjudanteObjeto.eReferencia(inscricao.getEvento().getDataFimRealizacao())) {

			dataFinal = inscricao.getEvento().getDataFimRealizacao();

		} else {

			dataFinal = inscricao.getEvento().getDataFimPrevisto();
		}

		if (AjudanteData.dataMaiorIgualDataAtual(dataFinal) && AjudanteObjeto.eReferencia(inscricao.getAutorizada())) {

			if(inscricao.getAutorizada()) {

				return true;
			}
			else {

				return false;
			}
		} else {

			return false;
		}
	}

	public void baixarCartaoParticipante(Inscricao inscricao) {

		try {

			byte[] saidaDoCartao = this.inscricaoNegocio.gerarCartaoParticipante(inscricao);

			InputStream isCartaoParticipante = new ByteArrayInputStream(saidaDoCartao);

			this.download(isCartaoParticipante, TipoArquivo.PDF);

		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	
	public List<Inscricao> getInscricoesSelecionadas() {
		return inscricoesSelecionadas;
	}

	public void setInscricoesSelecionadas(List<Inscricao> inscricoesSelecionadas) {
		this.inscricoesSelecionadas = inscricoesSelecionadas;
	}
	
	public void selecionarTodos(Evento evento) {
		
		this.limparSelecionados();
		
		if (this.selecionar) {
			
			List<Inscricao> inscricoes = inscricaoNegocio.listaPorAtributo("evento", evento);
		
			for (Inscricao inscricao : inscricoes) {
				this.inscricoesSelecionadas.add(inscricao);
			}
			
			this.executarJS("jQuery('#btnSelecionarTodos').html('DESSELECIONAR TODAS')");
			
		}
		
		this.selecionar = !this.selecionar;
		
	}
	
	public void limparSelecionados() {
		
		this.inscricoesSelecionadas = new ArrayList<Inscricao>();
		this.executarJS("jQuery('#btnSelecionarTodos').html('SELECIONAR TODAS')");
	}
	
	public void aprovarSelecionadas() {

		if (AjudanteObjeto.eVazio(this.inscricoesSelecionadas)) {

			this.adicionarMensagemDeAlerta("Nenhuma inscrição foi selecionada. Selecione inscrições a serem aprovadas.");

		} else {

			for (Inscricao inscricao : this.inscricoesSelecionadas) {

				if ((!AjudanteObjeto.eReferencia(inscricao.getAutorizada())) ||
						(AjudanteObjeto.eReferencia(inscricao.getAutorizada()) && !inscricao.getAutorizada()))  {

					this.inscricaoNegocio.aprovarInscricao(inscricao, this.getUsuarioLogado());
				}
			}
			this.adicionarMensagemInformativa("Inscrições aprovadas com sucesso!");
		}
		this.limparSelecionados();
	}
	
	public void reprovarSelecionadas() {

		if (AjudanteObjeto.eVazio(this.inscricoesSelecionadas)) {

			this.adicionarMensagemDeAlerta("Nenhuma inscrição foi selecionada. Selecione inscrições a serem reprovadas.");

		} else {

			for (Inscricao inscricao : this.inscricoesSelecionadas) {

				if ((!AjudanteObjeto.eReferencia(inscricao.getAutorizada())) ||
						(AjudanteObjeto.eReferencia(inscricao.getAutorizada()) && inscricao.getAutorizada()))  {

					this.inscricaoNegocio.reprovarInscricao(inscricao, this.getUsuarioLogado());
				}
			}
			this.adicionarMensagemInformativa("Inscrições reprovadas com sucesso!");
		}
		this.limparSelecionados();
	}

	public String getSenhaParaMoodle() {
		return senhaParaMoodle;
	}

	public void setSenhaParaMoodle(String senhaParaMoodle) {
		this.senhaParaMoodle = senhaParaMoodle;
	}
}
