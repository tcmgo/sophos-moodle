/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.Dominio;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Gasto;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoDominioEnum;
import br.gov.go.tcm.sophos.lazy.TableLazyGasto;
import br.gov.go.tcm.sophos.negocio.DominioNegocio;
import br.gov.go.tcm.sophos.negocio.GastoNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Scope("session")
public class GastoControlador extends ControladorBaseCRUD<Gasto, GastoNegocio> {

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private GastoNegocio negocio;

	@Autowired
	private TableLazyGasto lista;

	@Autowired
	private DominioNegocio dominioNegocio;

	private Evento evento;

	private List<Dominio> tipoGastoList;

	private List<Dominio> fonteGastoList;
	
	private String numeroProcesso;

	public void acaoInserir() {

		this.manipularNumeroProcesso();
		
		this.getEntidade().setEvento(this.evento);

		super.acaoInserir();

	}
	
	public void acaoEditar(Gasto gasto) {
		
		this.setEntidade(gasto);
	}
	
	@Override
	protected void executarAposInserirSucesso() {

		super.executarAposInserirSucesso();
		
		this.numeroProcesso = "";
	}

	public void selecionarEvento(Evento evento) {

		this.setEntidade(new Gasto());
		
		this.setFonteGastoList(this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.FonteGasto));
		
		this.setTipoGastoList(this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.TipoGasto));
		
		this.evento = evento;
	}

	@Override
	public void setEntidade(Gasto entidade) {

		super.setEntidade(entidade);
		
		this.setNumeroProcesso(entidade.getNumeroProcesso());
		
	}

	private void manipularNumeroProcesso() {
		
		if(!StringUtils.isEmpty(this.getNumeroProcesso())){

			this.getEntidade().setAnoProcesso(Integer.parseInt(this.getNumeroProcesso().split("/")[1]));
			
			this.getEntidade().setSeqProcesso(Integer.parseInt(this.getNumeroProcesso().split("/")[0]));
			
		}
	}
	
	@Override
	public TableLazyGasto getLista() {

		if (AjudanteObjeto.eReferencia(this.evento)) {

			Map<String, Object> filters = new HashMap<String, Object>();

			filters.put("evento", this.evento);

			this.lista.setFilters(filters);
		}

		return this.lista;
	}

	@Override
	protected GastoNegocio getNegocio() {

		return this.negocio;
	}

	public List<Dominio> getTipoGastoList() {
		return tipoGastoList;
	}

	public void setTipoGastoList(List<Dominio> tipoGastoList) {
		this.tipoGastoList = tipoGastoList;
	}

	public List<Dominio> getFonteGastoList() {
		return fonteGastoList;
	}

	public void setFonteGastoList(List<Dominio> fonteGastoList) {
		this.fonteGastoList = fonteGastoList;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public String getNumeroProcesso() {
		return numeroProcesso;
	}

	public void setNumeroProcesso(String numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}
	
}
