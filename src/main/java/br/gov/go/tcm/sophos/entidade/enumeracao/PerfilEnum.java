/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade.enumeracao;

import br.gov.go.tcm.estrutura.entidade.enumeracao.StringEnumeracaoPersistente;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum PerfilEnum implements StringEnumeracaoPersistente {

	PARTICIPANTE("Participante"), ADMINISTRADOR("Administrador"), INSTRUTOR("Instrutor"), PUBLICO("Público"), CHEFE("Chefe"), ESCOLA_DE_CONTAS("Escola de contas");

	private String descricao;

	private PerfilEnum(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public String getValorOrdinal() {

		return this.name();
	}

	public String getDescricao() {
		return descricao;
	}
	
	public static PerfilEnum [] getListaSemPublico(){
		
		List<PerfilEnum> list = new ArrayList<PerfilEnum>(Arrays.asList(values()));
		
		list.remove(PerfilEnum.PUBLICO);
		
		return list.toArray(new PerfilEnum[list.size()]);
	}

}
