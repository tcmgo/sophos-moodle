/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.estrutura.entidade.controledeacesso.Modulo;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBase;
import br.gov.go.tcm.sophos.entidade.*;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;
import br.gov.go.tcm.sophos.lazy.*;
import br.gov.go.tcm.sophos.lazy.base.TableLazyPadrao;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.context.FacesContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Scope("session")
public class AutoCompleteControlador extends ControladorBase {

	private static final String PARTICIPANTE_SECAO_USUARIO = "PARTICIPANTE_SECAO_USUARIO";

	private static final long serialVersionUID = -9198490669566081973L;

	@Autowired
	private TableLazyDominio tableLazyDominio;

	@Autowired
	private TableLazyParticipanteSimples tableLazyParticipante;

	@Autowired
	private TableLazyParticipanteSecaoUsuario tableLazyParticipanteSecaoUsuario;

	@Autowired
	private TableLazyProvedorEvento tableLazyProvedorEvento;

	@Autowired
	private TableLazyLocalizacaoEvento tableLazyLocalizacaoEvento;

	@Autowired
	private TableLazyUsuario tableLazyUsuario;

	@Autowired
	private TableLazyInstrutor tableLazyInstrutor;

	@Autowired
	private TableLazyEvento tableLazyEvento;
	
	@Autowired
	private TableLazyModulo tableLazyModulo;
	
	@Autowired
	private TableLazyInscricao tableLazyInscricao;

	private TableLazyPadrao<?> lista;

	private EntidadePadrao entidadeMultiSelect;

	private Boolean mostrarLimpar = Boolean.FALSE;

	@SuppressWarnings("unchecked")
	public TableLazyPadrao<?> getLista(String tipo) {

		FacesContext fc = FacesContext.getCurrentInstance();

		Map<String, Object> filters = fc.getApplication().evaluateExpressionGet(fc, "#{cc.attrs.filters}", 	HashMap.class);

		if (tipo.equalsIgnoreCase(Dominio.class.getSimpleName())) {

			this.lista = tableLazyDominio;

		} else if (tipo.equalsIgnoreCase(Participante.class.getSimpleName())) {

			this.lista = tableLazyParticipante;

		} else if (tipo.equalsIgnoreCase(ProvedorEvento.class.getSimpleName())) {

			this.lista = tableLazyProvedorEvento;

		} else if (tipo.equalsIgnoreCase(LocalizacaoEvento.class.getSimpleName())) {

			this.lista = tableLazyLocalizacaoEvento;

		} else if (tipo.equalsIgnoreCase(Usuario.class.getSimpleName())) {

			this.lista = tableLazyUsuario;

		} else if (tipo.equalsIgnoreCase(Instrutor.class.getSimpleName())) {

			this.lista = tableLazyInstrutor;

		} else if (tipo.equalsIgnoreCase(Evento.class.getSimpleName())) {

			this.lista = tableLazyEvento;

		} else if (tipo.equalsIgnoreCase(Modulo.class.getSimpleName())) {

			this.lista = tableLazyModulo;

		} else if (tipo.equalsIgnoreCase(Inscricao.class.getSimpleName())) {

			this.lista = tableLazyInscricao;

		}else if (tipo.equalsIgnoreCase(PARTICIPANTE_SECAO_USUARIO)) {

			this.lista = tableLazyParticipanteSecaoUsuario;

		}

		if(AjudanteObjeto.eReferencia(filters)){
			
			this.lista.setFilters(filters);
		}

		return lista;
	}
	
	public Boolean tamanhoArrayMaiorQue2(String [] str){
		
		return str.length > 2;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void onRowSelect(SelectEvent event) {

		FacesContext fc = FacesContext.getCurrentInstance();

		List lista = fc.getApplication().evaluateExpressionGet(fc, "#{cc.attrs.lista}", List.class);

		if (!lista.contains(event.getObject())) {

			lista.add(event.getObject());
		}

	}

	@SuppressWarnings("rawtypes")
	public void limparItens() {

		FacesContext fc = FacesContext.getCurrentInstance();

		List lista = fc.getApplication().evaluateExpressionGet(fc, "#{cc.attrs.lista}", List.class);

		lista.clear();

	}
	
	@SuppressWarnings("rawtypes")
	public void excluirItem(Integer index){
		
		FacesContext fc = FacesContext.getCurrentInstance();

		List lista = fc.getApplication().evaluateExpressionGet(fc, "#{cc.attrs.lista}", List.class);

		lista.remove(index.intValue());
	}

	public TableLazyPadrao<?> getLista() {
		return lista;
	}

	public EntidadePadrao getEntidadeMultiSelect() {
		return entidadeMultiSelect;
	}

	public void setEntidadeMultiSelect(EntidadePadrao entidadeMultiSelect) {
		this.entidadeMultiSelect = entidadeMultiSelect;
	}

	@SuppressWarnings("rawtypes")
	public Boolean getMostrarLimpar() {

		FacesContext fc = FacesContext.getCurrentInstance();

		List lista = fc.getApplication().evaluateExpressionGet(fc, "#{cc.attrs.lista}", List.class);

		if (lista != null) {

			this.mostrarLimpar = lista.size() > 0;
		}

		return mostrarLimpar;
	}

	public void setMostrarLimpar(Boolean mostrarLimpar) {
		this.mostrarLimpar = mostrarLimpar;
	}

}
