/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.persistencia.impl.base;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteData;
import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.estrutura.persistencia.base.implementacao.GenericoDAOImplementacao;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.Dominio;
import br.gov.go.tcm.sophos.entidade.Pessoa;
import br.gov.go.tcm.sophos.entidade.dto.FiltroDTO;
import br.gov.go.tcm.sophos.entidade.enumeracao.ComparadorCriteriaEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.FilterEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoLogicoEnum;
import br.gov.go.tcm.sophos.persistencia.base.DAO;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.*;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.util.StringUtils;

import javax.persistence.Lob;
import javax.persistence.Transient;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class DAOGenerico<E> extends GenericoDAOImplementacao implements DAO<E> {

	private static final long serialVersionUID = 2870573904625501416L;

	@Autowired
	@Qualifier(DAO.SESSION_FACTORY_SOPHOS)
	private SessionFactory sessionFactorySophos;

	public SessionFactory getSessionFactorySophos() {
		return sessionFactorySophos;
	}

	@Override
	public SessionFactory getSessionFactoryNomeBanco(String nomeBanco) {

		if (nomeBanco.equals(Banco.SOPHOS)) {

			return this.sessionFactorySophos;
		}

		return super.getSessionFactoryNomeBanco(nomeBanco);
	}

	@SuppressWarnings("unchecked")
	public E obterPorAtributo(String atributo, Object valor) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		if(atributo.contains(".")){
			
			criteria.createAlias(atributo.split("\\.")[0], atributo.split("\\.")[0]);
		}

		criteria.add(Restrictions.eq(atributo, valor));

		criteria.setMaxResults(1);

		try {

			return (E) criteria.uniqueResult();

		} finally {

			session.close();
		}
		
	}

	@SuppressWarnings("unchecked")
	public List<E> listaPorAtributo(String atributo, Object valor) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());

		criteria.add(Restrictions.eq(atributo, valor));

		try {

			return criteria.list();

		} finally {

			session.close();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<E> listaPorFiltros(List<FiltroDTO> filtros, String... alias) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		if(AjudanteObjeto.eReferencia(alias)){
		
			for (int i = 0; i < alias.length; i++) {
				
				criteria.createAlias(alias[i], alias[i].replace(".", ""));
			}
		}
		
		Conjunction criteriaAnd = Restrictions.conjunction();
		
		Disjunction criteriaOr = Restrictions.disjunction();
		
		for (FiltroDTO filtro : filtros) {
			
			if (AjudanteObjeto.eReferencia(filtro.getValor())) {
				
				if (filtro.getComparador() == ComparadorCriteriaEnum.EQ) {
					if(filtro.getTipoLogicoEnum().equals(TipoLogicoEnum.AND)){
						criteriaAnd.add(Restrictions.eq(filtro.getAtributo(), filtro.getValor()));
					}else{
						criteriaOr.add(Restrictions.eq(filtro.getAtributo(), filtro.getValor()));
					}
					
				} else if (filtro.getComparador() == ComparadorCriteriaEnum.LIKE) {
					if(filtro.getTipoLogicoEnum().equals(TipoLogicoEnum.AND)){
						criteriaAnd.add(Restrictions.ilike(filtro.getAtributo(), filtro.getValor().toString(), MatchMode.ANYWHERE));
					}else{
						criteriaOr.add(Restrictions.ilike(filtro.getAtributo(), filtro.getValor().toString(), MatchMode.ANYWHERE));
					}
				} 
				
			}
			
		}

		criteria.add(criteriaAnd);
		
		criteria.add(criteriaOr);
		
		try {

			return criteria.list();

		} finally {

			session.close();
		}
		
	}

	@SuppressWarnings("unchecked")
	protected Class<E> getTipoEntidade() {

		return (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	@Override
	public Long obterQuantidade(Map<String, Object> filters, String consulta) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		defineBuscaProFiltros(filters, criteria);
		
		Disjunction queryAll = definirBuscaPorConsulta(consulta, criteria);
		
		criteria.add(queryAll);
		
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		criteria.setProjection(Projections.count("id"));

		try {

			return (Long) criteria.uniqueResult();

		} finally {

			session.close();
		}
		
	}

	protected void defineBuscaProFiltros(Map<String, Object> filters, Criteria criteria) {
		
		if(AjudanteObjeto.eReferencia(filters)){
			
			for(String str : filters.keySet()){
				
				Object obj = filters.get(str);
				
				if(obj.equals(FilterEnum.NOT_NULL)){
					
					criteria.add(Restrictions.isNotNull(str));
					
				}else if(obj.equals(FilterEnum.NULL)){
					
					criteria.add(Restrictions.isNull(str));
					
				}else{
					
					criteria.add(Restrictions.eq(str, obj));
				}
			}
		}
	}

	protected Disjunction definirBuscaPorConsulta(String consulta, Criteria criteria) {
		
		Disjunction queryAll = Restrictions.disjunction();
		
		for(Field field: this.getTipoEntidade().getDeclaredFields()){
			
			if(!field.getName().equalsIgnoreCase("serialVersionUID") && !AjudanteObjeto.eReferencia(field.getAnnotation(Transient.class))){
				
				this.buscaGenericaPorConsulta(consulta, criteria, queryAll, field);
			}
		}
		
		return queryAll;
	}

	protected void buscaGenericaPorConsulta(String consulta, Criteria criteria, Disjunction queryAll, Field field) {
		
		if(field.getType().equals(String.class)){
			
			if(field.getAnnotation(Lob.class) == null && !StringUtils.isEmpty(consulta)){
				
				if(!field.getName().equals("justificativaParticipante"))queryAll.add(Restrictions.ilike(field.getName(), consulta, MatchMode.ANYWHERE));
				
			}
			
		}else if(field.getType().equals(Integer.class) && AjudanteObjeto.isInteger(consulta)){
			
			queryAll.add(Restrictions.eq(field.getName(), Integer.valueOf(consulta)));
			
		}else if(field.getType().equals(BigDecimal.class) && AjudanteObjeto.isBigDecimal(consulta)){
			
			queryAll.add(Restrictions.eq(field.getName(), new BigDecimal(consulta)));
			
		}else if(field.getType().equals(Long.class) && AjudanteObjeto.isLong(consulta)){
			
			queryAll.add(Restrictions.eq(field.getName(), Long.valueOf(consulta)));
			
		}else if(field.getType().equals(Date.class) && AjudanteObjeto.isDateDDMMYYYY(consulta)){
			
			queryAll.add(Restrictions.eq(field.getName(), AjudanteData.converterParaData(consulta)));
			
		}else if(field.getType().equals(Dominio.class)){
			
			criteria.createAlias(field.getName(), field.getName(), CriteriaSpecification.LEFT_JOIN);
			
			queryAll.add(Restrictions.ilike(field.getName()+".descricao", consulta, MatchMode.ANYWHERE));
			
		}else if(field.getType().equals(Pessoa.class)){
			
			criteria.createAlias(field.getName(), field.getName(), CriteriaSpecification.LEFT_JOIN);
			
			queryAll.add(Restrictions.ilike(field.getName()+".nome", consulta, MatchMode.ANYWHERE));
			
			queryAll.add(Restrictions.ilike(field.getName()+".cpf", consulta, MatchMode.START));
			
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<E> listar(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters, String consulta) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		defineBuscaProFiltros(filters, criteria);
		
		Disjunction queryAll = definirBuscaPorConsulta(consulta, criteria);
		
		criteria.add(queryAll);
		
		if(SortOrder.ASCENDING.equals(sortOrder) && !StringUtils.isEmpty(sortField)){
			
			criteria.addOrder(Order.asc(sortField));
			
		}else if(SortOrder.DESCENDING.equals(sortOrder) && !StringUtils.isEmpty(sortField)){
			
			criteria.addOrder(Order.desc(sortField));
		}
		
		criteria.setFirstResult(first);

		criteria.setMaxResults(pageSize);

		try {

			return criteria.list();

		} finally {

			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<E> listaPorAtributo(String atributo, String valor, MatchMode matchMode) {
		
		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());

		criteria.add(Restrictions.ilike(atributo, valor, matchMode));

		try {

			return criteria.list();

		} finally {

			session.close();
		}
	}
	
	public boolean validarPropriedadeIgual(final Class<?> entidade, final String[] propriedade, final Object[] valor) {

		if (propriedade.length != valor.length) {

			throw new IllegalArgumentException("propriedade.length not equals valor.length");
		}

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);

		final Criteria criteria = session.createCriteria(entidade);

		for (int i = 0; i < propriedade.length; i++) {

			criteria.add(Restrictions.eq(propriedade[i], valor[i]));
		}

		criteria.setProjection(Projections.rowCount());

		try {

			return ( (Number) criteria.uniqueResult() ).intValue() > 0;

		} finally {

			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<E> listaOrdenadoPorAtributo(String atributo, Object valor, SortOrder order, String atributoOrdenacao) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());

		criteria.add(Restrictions.eq(atributo, valor));
		
		if(SortOrder.ASCENDING.equals(order) && !StringUtils.isEmpty(atributoOrdenacao)){
			
			criteria.addOrder(Order.asc(atributoOrdenacao));
			
		}else if(SortOrder.DESCENDING.equals(order) && !StringUtils.isEmpty(atributoOrdenacao)){
			
			criteria.addOrder(Order.desc(atributoOrdenacao));
		}

		try {

			return criteria.list();

		} finally {

			session.close();
		}
	}
}
