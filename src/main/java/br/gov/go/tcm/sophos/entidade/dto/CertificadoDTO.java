/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CertificadoDTO implements Cloneable, Serializable {

	private static final long serialVersionUID = -1268810677217799278L;

	private String participante;

	private String evento;

	private String cargaHoraria;

	private String dataEvento;

	private String localEvento;

	private String codigoVerificacao;
	
	private String conteudoEvento;

	private Boolean detalhado;

	private String modulo;

	private List<ModuloInstrutorCertificadoDTO> moduloInstrutorCertificadoList;
	
	private String tipoEvento;
	
	private String linkValidacaoCertificado;

	public String getModulo() {
		return modulo;
	}

	public void setModulo(String modulo) {
		this.modulo = modulo;
	}

	public Boolean getDetalhado() {
		return detalhado;
	}

	public void setDetalhado(Boolean detalhado) {
		this.detalhado = detalhado;
	}

	public List<ModuloInstrutorCertificadoDTO> getModuloInstrutorCertificadoList() {
		if (moduloInstrutorCertificadoList == null) {
			moduloInstrutorCertificadoList = new ArrayList<ModuloInstrutorCertificadoDTO>();
		}
		return moduloInstrutorCertificadoList;
	}

	public void setModuloInstrutorCertificadoList(List<ModuloInstrutorCertificadoDTO> moduloInstrutorCertificadoList) {
		this.moduloInstrutorCertificadoList = moduloInstrutorCertificadoList;
	}

	public String getCodigoVerificacao() {
		return codigoVerificacao;
	}

	public void setCodigoVerificacao(String codigoVerificacao) {
		this.codigoVerificacao = codigoVerificacao;
	}

	public String getCargaHoraria() {
		return cargaHoraria;
	}

	public void setCargaHoraria(String cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}

	public String getDataEvento() {
		return dataEvento;
	}

	public void setDataEvento(String dataEvento) {
		this.dataEvento = dataEvento;
	}

	public String getLocalEvento() {
		return localEvento;
	}

	public void setLocalEvento(String localEvento) {
		this.localEvento = localEvento;
	}

	public String getParticipante() {
		return participante;
	}

	public void setParticipante(String participante) {
		this.participante = participante;
	}

	public String getEvento() {
		return evento;
	}

	public void setEvento(String evento) {
		this.evento = evento;
	}

	public String getConteudoEvento() {
		return conteudoEvento;
	}

	public void setConteudoEvento(String conteudoEvento) {
		this.conteudoEvento = conteudoEvento;
	}

	public String getTipoEvento() {
		return tipoEvento;
	}

	public void setTipoEvento(String tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public String getLinkValidacaoCertificado() {
		return linkValidacaoCertificado;
	}

	public void setLinkValidacaoCertificado(String linkValidacaoCertificado) {
		this.linkValidacaoCertificado = linkValidacaoCertificado;
	}

	@Override
	public CertificadoDTO clone() {

		try {

			return (CertificadoDTO) super.clone();

		} catch (CloneNotSupportedException e) {

			return null;
		}
	}
}
