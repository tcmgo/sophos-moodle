package br.gov.go.tcm.sophos.rest.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MoodleTabelaNotaDTO {

    private Long courseid;

    private Long userid;

    private String userfullname;
    
    private MoodleTabelaDadosNotaDTO[] tabledata;

	public Long getCourseid() {
		return courseid;
	}

	public void setCourseid(Long courseid) {
		this.courseid = courseid;
	}

	public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}

	public String getUserfullname() {
		return userfullname;
	}

	public void setUserfullname(String userfullname) {
		this.userfullname = userfullname;
	}

	public MoodleTabelaDadosNotaDTO[] getTabledata() {
		return tabledata;
	}

	public void setTabledata(MoodleTabelaDadosNotaDTO[] tabledata) {
		this.tabledata = tabledata;
	}

}
