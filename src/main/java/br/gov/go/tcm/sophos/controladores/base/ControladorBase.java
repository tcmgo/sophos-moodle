/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores.base;

import br.gov.go.tcm.estrutura.controlador.base.ControladorPadrao;
import br.gov.go.tcm.estrutura.entidade.transiente.Arquivo;
import br.gov.go.tcm.estrutura.negocio.ajudante.AjudanteDeArquivos;
import br.gov.go.tcm.sophos.ajudante.AjudanteMensagem;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.Usuario;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoArquivo;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import org.apache.commons.io.IOUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.UUID;

public abstract class ControladorBase extends ControladorPadrao {

	private static final long serialVersionUID = 2817440972489923069L;

	public static final String USUARIO_LOGADO = "usuario.logado";
	
	public static final String PERFIL_SELECIONADO = "usuario.perfil";

	@Autowired
	/** Atributo ajudanteArquivos. */
	private AjudanteDeArquivos ajudanteArquivos;

	public String obterMensagemPelaChave(final String chave) {

		return AjudanteMensagem.obterMensagemPelaChave(chave);
	}

	public void adicionarMensagemValidacao(ValidacaoException excecaoValidacao) {

		this.addMessageError(this.obterMensagemPelaChave(excecaoValidacao.getMessage()));
	}
	
	public String obterCaminhoReal() {

		ServletContext context = (ServletContext) obterContextoJSF().getExternalContext().getContext();

		return  context.getRealPath("/");

	}

	public FacesContext obterContextoJSF() {

		return FacesContext.getCurrentInstance();
	}

	public Usuario getUsuarioLogado() {

		return this.obterAtributoNaSessao(USUARIO_LOGADO, Usuario.class);
	}

	public Boolean verificaParticipante() {

		if (AjudanteObjeto.eReferencia(this.getUsuarioLogado())) {

			return AjudanteObjeto.eReferencia(this.getUsuarioLogado().getParticipante()) && AjudanteObjeto.eReferencia(this.getUsuarioLogado().getParticipante().getId());
		}

		return Boolean.TRUE;
	}

	public String clientId(final String idComponent) {

		final FacesContext context = this.obterContexto();

		final UIViewRoot root = context.getViewRoot();

		final UIComponent c = this.findComponent(root, idComponent);

		try {

			return c.getClientId(context);

		} catch (Exception e) {

			return idComponent;
		}
	}

	private UIComponent findComponent(final UIComponent componente, final String id) {

		if (id.equals(componente.getId())) {

			return componente;
		}

		final Iterator<UIComponent> filhos = componente.getFacetsAndChildren();

		while (filhos.hasNext()) {

			final UIComponent encontrado = this.findComponent(filhos.next(), id);

			if (encontrado != null) {

				return encontrado;
			}
		}

		return null;
	}

	@SuppressWarnings({ "unchecked" })
	public <T> T obterAtributoNaSessao(String chave, Class<T> tipoDoAtributo) {

		try {

			return (T) this.obterContextoJSF().getExternalContext().getSessionMap().get(chave);

		} catch (Exception e) {

			return null;
		}
	}

	public void registrarAtributoNaSessao(String chave, Object valor) {

		this.obterContextoJSF().getExternalContext().getSessionMap().put(chave, valor);
	}

	public void downloadGet() {

		this.download(this.obterObjetoDaRequisicao().getParameter("arquivo"));
	}

	public void download(final String idArquivo) {

		try {

			final Arquivo arquivo = AjudanteDeArquivos.abrir(idArquivo);

			final OutputStream os = this.obterObjetoDaResposta().getOutputStream();

			os.write(IOUtils.toByteArray(arquivo.getStream()));

			this.obterContexto().responseComplete();

		} catch (final Exception e) {

			this.adicionarMensagemDeErro(e.getMessage());
		}
	}
	
	public void download(final InputStream stream, TipoArquivo tipoArquivo) {

		try {

			this.obterObjetoDaResposta().reset();

			if(tipoArquivo.equals(TipoArquivo.PDF)){
				
				this.obterObjetoDaResposta().setContentType("application/pdf"); 
				
			}else{
				
				this.obterObjetoDaResposta().setContentType("application/octet-stream"); 
			}
			
			this.obterObjetoDaResposta().setHeader("Content-disposition", "attachment; filename=" + UUID.randomUUID() + "." + tipoArquivo.getExtensao());

			this.obterObjetoDaResposta().getOutputStream().write(IOUtils.toByteArray(stream));
			
			this.obterObjetoDaResposta().getOutputStream().flush();

			this.obterObjetoDaResposta().getOutputStream().close();
			
			this.obterObjetoDaResposta().flushBuffer();
			
			this.obterContexto().responseComplete();

		} catch (final Exception e) {

			this.adicionarMensagemDeErro(e.getMessage());
		}
	}

	public void upload(final FileUploadEvent event) {

		final UploadedFile file = event.getFile();

		try {

			final Arquivo arquivo = new Arquivo();

			arquivo.setStream(file.getInputstream());

			arquivo.setNome(file.getFileName());

			arquivo.setSufixo(file.getFileName());

			final String idArquivo = this.ajudanteArquivos.salvar(arquivo);

			this.executarAposUpload(event, idArquivo);

		} catch (final Exception e) {

			e.printStackTrace();

			this.adicionarMensagemDeErro(e.getMessage());
		}
	}

	protected void executarAposUpload(FileUploadEvent event, String idArquivo) {

		System.out.println("upload success: " + idArquivo);
	}

	public FacesContext obterContexto() {

		return FacesContext.getCurrentInstance();
	}

	public HttpServletRequest obterObjetoDaRequisicao() {

		return (HttpServletRequest) this.obterContexto().getExternalContext().getRequest();
	}

	public HttpServletResponse obterObjetoDaResposta() {

		return (HttpServletResponse) this.obterContexto().getExternalContext().getResponse();
	}

	public Object obterObjetoNaSessao(final String chave) {

		return this.obterContexto().getExternalContext().getSessionMap().get(chave);
	}

	public void definirObjetoNaSessao(final String chave, final Object objeto) {

		this.obterContexto().getExternalContext().getSessionMap().put(chave, objeto);
	}

	public void adicionarMensagem(final Severity severidade, final String mensagem) {

		this.obterContexto().addMessage(null, new FacesMessage(severidade, this.obterMensagemPelaChave(mensagem),
				this.obterMensagemPelaChave(mensagem)));
	}

	public void adicionarMensagemInformativa(final String mensagem, final Object... parametros) {

		if (parametros.length == 0) {

			this.adicionarMensagem(FacesMessage.SEVERITY_INFO, mensagem);

		} else {

			this.adicionarMensagem(FacesMessage.SEVERITY_INFO,
					MessageFormat.format(this.obterMensagemPelaChave(mensagem), parametros));
		}
	}

	public void adicionarMensagemDeAlerta(final String mensagem) {

		this.adicionarMensagem(FacesMessage.SEVERITY_WARN, mensagem);
	}

	public void redirecionar(final String url) {

		try {

			final String contextPath = this.obterContexto().getExternalContext().getRequestContextPath();

			this.obterContexto().getExternalContext().redirect(contextPath + url);

			this.obterContexto().responseComplete();

		} catch (final IOException e) {

			this.addMessageError(e.getMessage());
		}
	}

	public void adicionarMensagemExcecao(final ValidacaoException excecao) {

		if (AjudanteObjeto.eReferencia(excecao.getArgumentos())) {

			this.adicionarMensagemDeErro(
					MessageFormat.format(this.obterMensagemPelaChave(excecao.getMessage()), excecao.getArgumentos()));

		} else {

			this.adicionarMensagemDeErro(excecao.getMessage());
		}
	}

	public void adicionarMensagemDeErro(final String mensagem) {

		this.adicionarMensagem(FacesMessage.SEVERITY_ERROR, mensagem);
	}

	public void executarJS(final String script) {

		RequestContext.getCurrentInstance().execute("try{" + script + "}catch(e){}");
	}

	public void invalidarSessao() {

		this.obterContexto().getExternalContext().invalidateSession();
	}

	public String acaoAbrirTelaInicial() {

		return "home";
	}
}
