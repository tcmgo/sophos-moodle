/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 *
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 *
 * Este cabeçalho deve ser mantido.
 *
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "certificado")
@Banco(nome = Banco.SOPHOS)
public class Certificado extends EntidadePadrao {

	private static final long serialVersionUID = 5570523119504876878L;

	@JoinColumn(name = "evento_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Evento evento;

	@JoinColumn(name = "participante_id", referencedColumnName = "id")
	@ManyToOne
	private Participante participante;

	@JoinColumn(name = "instrutor_id", referencedColumnName = "id")
	@ManyToOne
	private Instrutor instrutor;

	@JoinColumn(name = "arquivo_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Anexo arquivo;

	@Column(name = "codigo_verificacao", nullable = false, length = 17)
	private String codigoVerificacao;

	@Column(name = "data_emissao", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataEmissao;

	public String getCodigoVerificacao() {
		return codigoVerificacao;
	}

	public void setCodigoVerificacao(String codigoVerificacao) {
		this.codigoVerificacao = codigoVerificacao;
	}

	public Date getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public Participante getParticipante() {
		return participante;
	}

	public void setParticipante(Participante participante) {
		this.participante = participante;
	}

	public Anexo getArquivo() {
		return arquivo;
	}

	public void setArquivo(Anexo arquivo) {
		this.arquivo = arquivo;
	}

	public Instrutor getInstrutor() { return instrutor; }

	public void setInstrutor(Instrutor instrutor) { this.instrutor = instrutor; }
}
