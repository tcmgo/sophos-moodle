/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;

import javax.persistence.*;

@Entity
@Table(name = "provedor_evento")
@Banco(nome = Banco.SOPHOS)
public class ProvedorEvento extends EntidadePadrao {

	private static final long serialVersionUID = -4470802226093199484L;

	@Column(name = "descricao", nullable = false)
	private String descricao;

	@Column(name = "agencia", length = 50)
	private String agencia;

	@JoinColumn(name = "banco_id", referencedColumnName = "id")
	@ManyToOne
	private BancoFebraban banco;

	@Column(name = "cnpj", length = 18)
	private String cnpj;

	@Column(name = "conta_corrente", length = 20)
	private String contaCorrente;

	@Column(name = "contato")
	private String contato;

	@Column(name = "email")
	private String email;

	@Column(name = "telefone", length = 14)
	private String telefone;

	@Column(name = "celular", length = 14)
	private String celular;

	@OneToOne
	@JoinColumn(name = "endereco_id")
	private Endereco endereco;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public BancoFebraban getBanco() {
		return banco;
	}

	public void setBanco(BancoFebraban banco) {
		this.banco = banco;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getContaCorrente() {
		return contaCorrente;
	}

	public void setContaCorrente(String contaCorrente) {
		this.contaCorrente = contaCorrente;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

}
