package br.gov.go.tcm.sophos.entidade.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class DesempenhoDTO implements Serializable {
	
	private static final long serialVersionUID = 4029973751324144456L;
	
	private List<ModuloDesempenhoDTO> listaDesempenho;
	private Boolean aprovado;
	private Boolean avaliado;
	BigDecimal notaMedia;
	BigDecimal frequenciaMedia;
	
	public List<ModuloDesempenhoDTO> getListaDesempenho() {
		return listaDesempenho;
	}
	public void setListaDesempenho(List<ModuloDesempenhoDTO> listaDesempenho) {
		this.listaDesempenho = listaDesempenho;
	}
	public Boolean getAprovado() {
		return aprovado;
	}
	public void setAprovado(Boolean aprovado) {
		this.aprovado = aprovado;
	}
	public Boolean getAvaliado() {
		return avaliado;
	}
	public void setAvaliado(Boolean avaliado) {
		this.avaliado = avaliado;
	}
	public BigDecimal getNotaMedia() {
		return notaMedia;
	}
	public void setNotaMedia(BigDecimal notaMedia) {
		this.notaMedia = notaMedia;
	}
	public BigDecimal getFrequenciaMedia() {
		return frequenciaMedia;
	}
	public void setFrequenciaMedia(BigDecimal frequenciaMedia) {
		this.frequenciaMedia = frequenciaMedia;
	}

}
