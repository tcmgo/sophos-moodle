/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.*;
import br.gov.go.tcm.sophos.entidade.dto.ModuloAvaliacaoChartDTO;
import br.gov.go.tcm.sophos.entidade.dto.OpcaoQuestionarioChartDTO;
import br.gov.go.tcm.sophos.entidade.dto.QuestionarioAvaliacaoChartDTO;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import br.gov.go.tcm.sophos.persistencia.AvaliacaoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AvaliacaoNegocio extends NegocioBase<Avaliacao> {

	private static final long serialVersionUID = -8426728536010364058L;

	@Autowired
	private AvaliacaoDAO dao;
	
	@Autowired
	private InscricaoNegocio inscricaoNegocio;
	
	@Autowired
	private QuestionarioAvaliacaoNegocio questionarioAvaliacaoNegocio;

	@Autowired
	private ModuloNegocio moduloNegocio;

	@Override
	protected AvaliacaoDAO getDAO() {

		return this.dao;
	}
	
	public void gerarListaDeAvalicoes(List<Avaliacao> avaliacoes, List<String> observacoesList, ModuloAvaliacaoChartDTO moduloAvaliacaoChartDTO) {
		
		for(Avaliacao avaliacao : avaliacoes){
			
			for(QuestionarioAvaliacao questionarioAvaliacao : this.questionarioAvaliacaoNegocio.listaPorAtributo("avaliacao", avaliacao)){
			
				QuestionarioAvaliacaoChartDTO questionarioAvaliacaoChartDTO = new QuestionarioAvaliacaoChartDTO(questionarioAvaliacao.getQuestionario().getPergunta(), questionarioAvaliacao.getQuestionario().getId());

				questionarioAvaliacaoChartDTO.setInstrutor(AjudanteObjeto.eReferencia(questionarioAvaliacao.getInstrutor()) ? questionarioAvaliacao.getInstrutor().getPessoa().getNome() : null);
				
				OpcaoQuestionarioChartDTO opcaoSelecionada = new OpcaoQuestionarioChartDTO(questionarioAvaliacao.getOpcao().getId(), questionarioAvaliacao.getOpcao().getDescricao());
				
				if(moduloAvaliacaoChartDTO.getQuestionarios().contains(questionarioAvaliacaoChartDTO)){
					
					questionarioAvaliacaoChartDTO = moduloAvaliacaoChartDTO.getQuestionarios().get(moduloAvaliacaoChartDTO.getQuestionarios().indexOf(questionarioAvaliacaoChartDTO));
					
					if(questionarioAvaliacaoChartDTO.getOpcoes().contains(opcaoSelecionada)){
						
						questionarioAvaliacaoChartDTO.getOpcoes().get(questionarioAvaliacaoChartDTO.getOpcoes().indexOf(opcaoSelecionada)).addQuantidade();
						
					}else{
						
						questionarioAvaliacaoChartDTO.getOpcoes().add(opcaoSelecionada);
					}
					
				}else{
					
					questionarioAvaliacaoChartDTO.getOpcoes().add(opcaoSelecionada);
					
					moduloAvaliacaoChartDTO.getQuestionarios().add(questionarioAvaliacaoChartDTO);
				}
			}
			
			observacoesList.add(avaliacao.getObservacao());
		}
		
	}
	
	
	@Override
	protected void executarValidacaoAntesDeSalvar(Avaliacao entidade) {

		super.executarValidacaoAntesDeSalvar(entidade);
		
		for(QuestionarioAvaliacao questionarioAvaliacao : entidade.getQuestionarioAvaliacaoList()){
			
			if(!AjudanteObjeto.eReferencia(questionarioAvaliacao.getOpcao()) || !AjudanteObjeto.eReferencia(questionarioAvaliacao.getOpcao().getId())){
				
				if(AjudanteObjeto.eReferencia(questionarioAvaliacao.getInstrutor())){
					
					throw new ValidacaoException("A questão \"" + questionarioAvaliacao.getQuestionario().getPergunta() + "\" da aba \"" + questionarioAvaliacao.getInstrutor().getPessoa().getNome() + "\" não foi respondida!");
				
				}else{
					
					throw new ValidacaoException("A questão \"" + questionarioAvaliacao.getQuestionario().getPergunta() + "\" da aba \"MÓDULO\" não foi respondida!");
				}
			}
		}
	}

	public List<Modulo> listarModulosNaoAvaliados(Participante participante) {
		
		List<Modulo> result = new ArrayList<Modulo>();

		List<Inscricao> inscricaoList = this.inscricaoNegocio.listaPorAtributo("participante", participante);

		List<Avaliacao> avaliacoesRealizadas = this.listaPorAtributo("participante", participante);
		
		for (Inscricao inscricao : inscricaoList) {
			
			List<Modulo> listaParaAdd = this.moduloNegocio.listaPorAtributo("evento", inscricao.getEvento());
			
			for(Avaliacao avaliacao : avaliacoesRealizadas){
				
				if(listaParaAdd.contains(avaliacao.getModulo())){
					
					listaParaAdd.remove(avaliacao.getModulo());
				}
			}
			
			result.addAll(listaParaAdd);
		}
		
		return result;
	}
}
