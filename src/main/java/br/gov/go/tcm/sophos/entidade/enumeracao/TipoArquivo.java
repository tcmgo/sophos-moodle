/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade.enumeracao;

import br.gov.go.tcm.estrutura.entidade.enumeracao.StringEnumeracaoPersistente;

public enum TipoArquivo implements StringEnumeracaoPersistente{

	PDF("pdf", new String[]{"application/pdf"}), 
	XLS("xls", new String[]{"application/vnd.ms-excel"}), 
	XLSX("xlsx", new String[]{"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"}), 
	DOC("doc", new String[]{"application/msword"}), 
	DOCX("docx", new String[]{"application/vnd.openxmlformats-officedocument.wordprocessingml.document"}), 
	PNG("png", new String[]{"image/png"}), 
	JPG("jpg", new String[]{"image/jpeg","image/pjpeg"}), 
	JPEG("jpeg", new String[]{"image/jpeg","image/pjpeg" }), 
	BMP("bmp", new String[]{"image/bmp"}), 
	CSV("csv", new String[]{"text/x-comma-separated-values", "text/comma-separated-values", "application/octet-stream"}), 
	TXT("txt", new String[]{"text/plain"}), 
	ODS("ods", new String[]{"application/vnd.oasis.opendocument.spreadsheet"}),
	ODT("odt", new String[]{"application/vnd.oasis.opendocument.text"}),
	PPT("ppt", new String[]{"application/vnd.ms-powerpoint"}),
	PPTX("pptx", new String[]{"application/vnd.openxmlformats-officedocument.presentationml.presentation"});
	
	private String extensao;
	
	private String[] contentTypes;

	private TipoArquivo(String extensao, String[] contentTypes) {
		this.extensao = extensao;
		this.contentTypes = contentTypes;
	}

	public String getExtensao() {
		return extensao;
	}

	public String[] getContentType() {
		return contentTypes;
	}

	public static TipoArquivo getTipoArquivo(String contentType) {

		for(TipoArquivo tipoArquivo : TipoArquivo.values()) {
			
			for (String content : tipoArquivo.contentTypes) {
				
				if (contentType.trim().toLowerCase().equals(content.trim().toLowerCase())) {
					return tipoArquivo;
				}
			}			

		}
		
		return null;
	}

	@Override
	public String getValorOrdinal() {

		return this.getExtensao();
	}
	
}
