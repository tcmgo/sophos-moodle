/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.importador;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import br.gov.go.tcm.sophos.importador.base.EventoImportado;
import br.gov.go.tcm.sophos.importador.base.ImportadorService;
import br.gov.go.tcm.sophos.importador.base.ItemImportado;

@Service("importadorAluraService")
public class ImportadorAluraService extends ImportadorService {

	private static int HTTP_COD_SUCESSO = 200;

	@Override
	public List<ItemImportado> importar(String strURL, String serverName) {

		List<ItemImportado> itens = new ArrayList<ItemImportado>();

		try {

			URL url = new URL(strURL);
			HttpsURLConnection conexao;
						
			if (serverName.equals("localhost")) {
				//Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.10.100.1", 80));
				//conexao = (HttpsURLConnection) url.openConnection(proxy);
				conexao = (HttpsURLConnection) url.openConnection();				
			} else {
				conexao = (HttpsURLConnection) url.openConnection();
			}

			conexao.setRequestMethod("GET");
			conexao.setRequestProperty("Accept", "application/json;charset=UTF-8");

			int codResposta = conexao.getResponseCode();
			if (codResposta != HTTP_COD_SUCESSO) {
				throw new RuntimeException("Ocorreu um ERRO ao tentar realizar a conexão ao serviço Alura: " + conexao.getResponseCode());
			}

			BufferedReader reader = new BufferedReader(new InputStreamReader(conexao.getInputStream(), "UTF-8"));
			StringBuilder builder = new StringBuilder();

			String linha;
			while ((linha = reader.readLine()) != null) {
				builder.append(linha + '\n');
			}

			String jsonString = builder.toString();

			Type type = new TypeToken<ArrayList<RegistroAlura>>() {}.getType();
			List<RegistroAlura> registrosAlura = new Gson().fromJson(jsonString, type);

			for (RegistroAlura registroAlura : registrosAlura) {

				ItemImportado item = new ItemImportado();

				EventoImportado evento = new EventoImportado(registroAlura.getCargaHorariaEstimada(),
						registroAlura.getCurso(), registroAlura.getIniciadoEm(), registroAlura.getTerminadoEm(),
						registroAlura.getIniciadoEm(), registroAlura.getTerminadoEm(), registroAlura.getCurso().toUpperCase(), 1,
						"Tecnologia", "Alura - Cursos Online de Tecnologia", "EAD", "01.100.111/0001-11",
						"teste@tcm.go.gov.br", "CURSO", "SERVIDOR TCM");
				
				item.setEvento(evento);
				item.setEmailParticipante(registroAlura.getEmailDoAluno());
				item.setFrequencia(registroAlura.getSecoesFinalizadas());
				item.setNota(registroAlura.getAproveitamento());

				itens.add(item);

			}

			conexao.disconnect();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return itens;

	}

}
