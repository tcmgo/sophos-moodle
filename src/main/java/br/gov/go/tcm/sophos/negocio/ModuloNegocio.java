/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteData;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Modulo;
import br.gov.go.tcm.sophos.entidade.ModuloInstrutor;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import br.gov.go.tcm.sophos.persistencia.ModuloDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class ModuloNegocio extends NegocioBase<Modulo> {

	public static final int RESOLUCAO_FREQUENCIA = 75;

	public static final int RESOLUCAO_APROVACAO = 70;

	private static final long serialVersionUID = -8426728536010364058L;

	@Autowired
	private ModuloDAO dao;

	@Autowired
	private ModuloInstrutorNegocio moduloInstrutorNegocio;

	@Autowired
	private EventoNegocio eventoNegocio;

	@Override
	protected ModuloDAO getDAO() {

		return this.dao;
	}

	@Override
	protected void executarValidacaoAntesDeSalvar(Modulo entidade) {

		super.executarValidacaoAntesDeSalvar(entidade);

		if (!AjudanteObjeto.eReferencia(entidade.getId()) && entidade.getEvento().getModuloUnico()) {

			List<Modulo> modulos = this.listaPorAtributo("evento", entidade.getEvento());

			if (AjudanteObjeto.eReferencia(modulos) && modulos.size() > 0) {

				throw new ValidacaoException("Evento marcado como módulo único. Já existe um módulo para esse evento.");
			}

			if (entidade.getCargaHoraria().compareTo(entidade.getEvento().getCargaHoraria()) != 0) {

				throw new ValidacaoException("Evento marcado como módulo único. Carga Horária diferente do evento.");
			}
		}

		Date dataInicioEvento = null;

		Date dataFimEvento = null;

		if (AjudanteObjeto.eReferencia(entidade.getEvento().getDataInicioRealizacao())
				&& AjudanteObjeto.eReferencia(entidade.getEvento().getDataFimRealizacao())) {

			dataInicioEvento = entidade.getEvento().getDataInicioRealizacao();

			dataFimEvento = entidade.getEvento().getDataFimRealizacao();

		} else {

			dataInicioEvento = entidade.getEvento().getDataInicioPrevisto();

			dataFimEvento = entidade.getEvento().getDataFimPrevisto();
		}

		if (!AjudanteData.dataEstaEntreOPeriodo(entidade.getDataInicio(), dataInicioEvento, dataFimEvento)
				|| !AjudanteData.dataEstaEntreOPeriodo(entidade.getDataFim(), dataInicioEvento, dataFimEvento)) {

			throw new ValidacaoException("Data de realização do módulo fora da data de realização do evento.");
		}
	}

	@Override
	public void salvar(Modulo entidade) {

		if (AjudanteObjeto.eReferencia(entidade.getId())) {

			List<ModuloInstrutor> moduloInstrutorListOld = this.moduloInstrutorNegocio.listaPorAtributo("modulo",
					entidade);

			for (ModuloInstrutor moduloInstrutor : moduloInstrutorListOld) {

				if (!entidade.getInstrutorList().contains(moduloInstrutor)) {

					this.moduloInstrutorNegocio.excluir(moduloInstrutor);
				}
			}
		}

		entidade.setEvento(this.eventoNegocio.obter(entidade.getEvento().getId()));

		super.salvar(entidade);

		for (ModuloInstrutor moduloInstrutor : entidade.getInstrutorList()) {

			if (!this.moduloInstrutorNegocio.existeModuloInstrutor(moduloInstrutor.getModulo(),
					moduloInstrutor.getInstrutor())) {

				this.moduloInstrutorNegocio.salvar(moduloInstrutor);
			}

		}

	}

	public void inserirModuloUnico(Evento evento) {

		Modulo modulo = new Modulo();

		modulo.setTitulo("MÓDULO ÚNICO");

		modulo.setCargaHoraria(evento.getCargaHoraria());

		if (AjudanteObjeto.eReferencia(evento.getDataInicioRealizacao()) && AjudanteObjeto.eReferencia(evento.getDataFimRealizacao())) {

			modulo.setDataInicio(evento.getDataInicioRealizacao());

			modulo.setDataFim(evento.getDataFimRealizacao());

		} else {

			modulo.setDataInicio(evento.getDataInicioPrevisto());

			modulo.setDataFim(evento.getDataFimPrevisto());
		}

		modulo.setNotaAprovacao(BigDecimal.valueOf(RESOLUCAO_APROVACAO));

		modulo.setFrequenciaAprovacao(BigDecimal.valueOf(RESOLUCAO_FREQUENCIA));

		modulo.setQuantidadeEncontros(1);

		modulo.setEvento(evento);

		super.salvar(modulo);
	}

	public List<Modulo> listarOrdenadoPorData(Evento evento) {

		return this.getDAO().listarOrdenadoPorData(evento);
	}

	public BigDecimal obterSomatorioCargaHorariaModulos(Evento evento) {

		BigDecimal somatorio = new BigDecimal(0);
		List<Modulo> modulos = this.listaPorAtributo("evento", evento);

		if (modulos.isEmpty() == false) {
			for (Modulo modulo : modulos) {
				somatorio = somatorio.add(modulo.getCargaHoraria());
			}
		}

		return somatorio;
	}
}
