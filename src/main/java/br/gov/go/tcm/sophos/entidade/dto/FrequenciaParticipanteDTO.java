/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class FrequenciaParticipanteDTO implements Cloneable, Serializable{

	private static final long serialVersionUID = -3255453346045605591L;

	private Long idParticipante;
	
	private String participante;

	private Integer quantidadePresenca;

	private BigDecimal percentualPresenca;

	public Long getIdParticipante() {
		return idParticipante;
	}

	public void setIdParticipante(Long idParticipante) {
		this.idParticipante = idParticipante;
	}

	public String getParticipante() {
		return participante;
	}

	public void setParticipante(String participante) {
		this.participante = participante;
	}

	public Integer getQuantidadePresenca() {
		return quantidadePresenca;
	}

	public void setQuantidadePresenca(Integer quantidadePresenca) {
		this.quantidadePresenca = quantidadePresenca;
	}

	public BigDecimal getPercentualPresenca() {
		return percentualPresenca;
	}

	public void setPercentualPresenca(BigDecimal percentualPresenca) {
		this.percentualPresenca = percentualPresenca;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idParticipante == null) ? 0 : idParticipante.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FrequenciaParticipanteDTO other = (FrequenciaParticipanteDTO) obj;
		if (idParticipante == null) {
			if (other.idParticipante != null)
				return false;
		} else if (!idParticipante.equals(other.idParticipante))
			return false;
		return true;
	}

	@Override
	public FrequenciaParticipanteDTO clone()  {
		try {
			return (FrequenciaParticipanteDTO) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
}
