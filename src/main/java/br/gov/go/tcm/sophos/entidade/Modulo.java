/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity(name = "br.gov.go.tcm.sophos.entidade.Modulo")
@Table(name = "modulo")
@Banco(nome = Banco.SOPHOS)
public class Modulo extends EntidadePadrao {

	private static final long serialVersionUID = 1346317030293965321L;

	@Column(name = "carga_horaria", nullable = false)
	private BigDecimal cargaHoraria;

	@Column(name = "data_fim", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataFim;

	@Column(name = "data_inicio", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataInicio;

	@Column(name = "titulo", nullable = false)
	private String titulo;

	@Column(name = "nota_aprovacao", nullable = false)
	private BigDecimal notaAprovacao;

	@Column(name = "frequencia_aprovacao")
	private BigDecimal frequenciaAprovacao;

	@Column(name = "hora_fim_turno1", length = 5)
	private String horaFimTurno1;

	@Column(name = "hora_fim_turno2", length = 5)
	private String horaFimTurno2;

	@Column(name = "hora_inicio_turno1", length = 5)
	private String horaInicioTurno1;

	@Column(name = "hora_inicio_turno2", length = 5)
	private String horaInicioTurno2;

	@Column(name = "observacao", length = 2000)
	private String observacao;

	@Column(name = "quantidade_encontros", nullable = false)
	private Integer quantidadeEncontros;

	@JoinColumn(name = "evento_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Evento evento;

	@OneToMany(mappedBy = "modulo", fetch = FetchType.LAZY)
	private List<ModuloInstrutor> instrutorList;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "modulo")
	private List<Avaliacao> avaliacaoList;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "modulo")
	private List<Material> materialList;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "modulo")
	private List<Nota> notaList;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "modulo")
	private List<Frequencia> frequenciaList;

	public BigDecimal getCargaHoraria() {
		return cargaHoraria;
	}

	public void setCargaHoraria(BigDecimal cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}

	public List<Avaliacao> getAvaliacaoList() {
		return avaliacaoList;
	}

	public void setAvaliacaoList(List<Avaliacao> avaliacaoList) {
		this.avaliacaoList = avaliacaoList;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public BigDecimal getNotaAprovacao() {
		return notaAprovacao;
	}

	public void setNotaAprovacao(BigDecimal notaAprovacao) {
		this.notaAprovacao = notaAprovacao;
	}

	public BigDecimal getFrequenciaAprovacao() {
		return frequenciaAprovacao;
	}

	public void setFrequenciaAprovacao(BigDecimal frequenciaAprovacao) {
		this.frequenciaAprovacao = frequenciaAprovacao;
	}

	public List<Material> getMaterialList() {
		return materialList;
	}

	public void setMaterialList(List<Material> materialList) {
		this.materialList = materialList;
	}

	public String getHoraFimTurno1() {
		return horaFimTurno1;
	}

	public void setHoraFimTurno1(String horaFimTurno1) {
		this.horaFimTurno1 = horaFimTurno1;
	}

	public String getHoraFimTurno2() {
		return horaFimTurno2;
	}

	public void setHoraFimTurno2(String horaFimTurno2) {
		this.horaFimTurno2 = horaFimTurno2;
	}

	public String getHoraInicioTurno1() {
		return horaInicioTurno1;
	}

	public void setHoraInicioTurno1(String horaInicioTurno1) {
		this.horaInicioTurno1 = horaInicioTurno1;
	}

	public String getHoraInicioTurno2() {
		return horaInicioTurno2;
	}

	public void setHoraInicioTurno2(String horaInicioTurno2) {
		this.horaInicioTurno2 = horaInicioTurno2;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public List<ModuloInstrutor> getInstrutorList() {
		return instrutorList;
	}

	public void setInstrutorList(List<ModuloInstrutor> instrutorList) {
		this.instrutorList = instrutorList;
	}

	public List<Nota> getNotaList() {
		return notaList;
	}

	public void setNotaList(List<Nota> notaList) {
		this.notaList = notaList;
	}

	public List<Frequencia> getFrequenciaList() {
		return frequenciaList;
	}

	public void setFrequenciaList(List<Frequencia> frequenciaList) {
		this.frequenciaList = frequenciaList;
	}

	public Integer getQuantidadeEncontros() {
		return quantidadeEncontros;
	}

	public void setQuantidadeEncontros(Integer quantidadeEncontros) {
		this.quantidadeEncontros = quantidadeEncontros;
	}

}
