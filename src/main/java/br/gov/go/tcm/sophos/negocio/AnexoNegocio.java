/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.estrutura.entidade.transiente.Arquivo;
import br.gov.go.tcm.estrutura.negocio.ajudante.AjudanteDeArquivos;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.Anexo;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import br.gov.go.tcm.sophos.persistencia.AnexoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;

@Service
public class AnexoNegocio extends NegocioBase<Anexo> {

	private static final long serialVersionUID = -8426728536010364058L;

	@Autowired
	private AnexoDAO dao;

	@Autowired
	/** Atributo ajudanteArquivos. */
	private AjudanteDeArquivos ajudanteArquivos;

	@Override
	protected AnexoDAO getDAO() {

		return this.dao;
	}

	@Override
	public void salvar(Anexo entidade) {

		final Arquivo arquivo = new Arquivo();

		try {
			
			if(!AjudanteObjeto.eReferencia(entidade.getId()) && (AjudanteObjeto.eReferencia(entidade.getFile()) || AjudanteObjeto.eReferencia(entidade.getBytes()))){

				arquivo.setStream(AjudanteObjeto.eReferencia(entidade.getFile()) ? entidade.getFile().getInputstream() : new ByteArrayInputStream(entidade.getBytes()));
				
				arquivo.setNome(entidade.getDescricao());
				
				arquivo.setSufixo("."+entidade.getTipoArquivo().getExtensao());
				
				final String idArquivo = this.ajudanteArquivos.salvar(arquivo);
				
				entidade.setArquivoGED(idArquivo);
				
			}
			
			super.salvar(entidade);

		} catch (IOException e) {

			throw new ValidacaoException("Erro ao salvar anexo: " + entidade.getDescricao());
		}
	}

	public void salvarSimples(Anexo anexo) {

		super.salvar(anexo);
	}

}
