/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.entidade.enumeracao.EnumeracaoTipo;
import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.entidade.base.EntidadeLogica;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoDominioEnum;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "dominio")
@TypeDefs({ @TypeDef(name = "tipoDominio", typeClass = EnumeracaoTipo.class, parameters = {
		@Parameter(name = "enumClassName", value = "br.gov.go.tcm.sophos.entidade.enumeracao.TipoDominioEnum") }) })
@Banco(nome = Banco.SOPHOS)
public class Dominio extends EntidadeLogica implements Comparable<Dominio> {

	private static final long serialVersionUID = 5365316975514728098L;

	@Column(name = "nome", nullable = false)
	@Type(type = "tipoDominio")
	private TipoDominioEnum nome;

	@Column(name = "codigo", nullable = false, length = 4)
	private Long codigo;

	@Column(name = "descricao", nullable = false)
	private String descricao;

	public TipoDominioEnum getNome() {
		return nome;
	}

	public void setNome(TipoDominioEnum nome) {
		this.nome = nome;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean equals(String nome, Long codigo){
		
		return this.getCodigo().equals(codigo) && this.getNome().equals(TipoDominioEnum.valueOf(nome));
		
	}
	
	@Override
	public int compareTo(Dominio dominio) {
		if(this.getCodigo() != null){
			return this.getCodigo().compareTo(dominio.getCodigo());
		}
		return 0;
	}
}
