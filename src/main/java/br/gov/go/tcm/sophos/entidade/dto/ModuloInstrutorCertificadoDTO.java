/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade.dto;

import java.io.Serializable;

public class ModuloInstrutorCertificadoDTO implements Cloneable, Serializable {

	private static final long serialVersionUID = -6332525871429097121L;

	private String instrutor;

	private String curriculoInstrutor;

	private String modulo;

	private String cargaHorariaModulo;

	public ModuloInstrutorCertificadoDTO() {
		super();
	}

	public ModuloInstrutorCertificadoDTO(String instrutor, String modulo, String cargaHorariaModulo) {
		super();
		this.instrutor = instrutor;
		this.modulo = modulo;
		this.cargaHorariaModulo = cargaHorariaModulo;
	}

	public ModuloInstrutorCertificadoDTO(String instrutor, String curriculoInstrutor, String modulo,
			String cargaHorariaModulo) {
		super();
		this.instrutor = instrutor;
		this.curriculoInstrutor = curriculoInstrutor;
		this.modulo = modulo;
		this.cargaHorariaModulo = cargaHorariaModulo;
	}

	public String getInstrutor() {
		return instrutor;
	}

	public void setInstrutor(String instrutor) {
		this.instrutor = instrutor;
	}

	public String getCurriculoInstrutor() {
		return curriculoInstrutor;
	}

	public void setCurriculoInstrutor(String curriculoInstrutor) {
		this.curriculoInstrutor = curriculoInstrutor;
	}

	public String getModulo() {
		return modulo;
	}

	public void setModulo(String modulo) {
		this.modulo = modulo;
	}

	public String getCargaHorariaModulo() {
		return cargaHorariaModulo;
	}

	public void setCargaHorariaModulo(String cargaHorariaModulo) {
		this.cargaHorariaModulo = cargaHorariaModulo;
	}

	@Override
	public ModuloInstrutorCertificadoDTO clone() {

		try {

			return (ModuloInstrutorCertificadoDTO) super.clone();

		} catch (CloneNotSupportedException e) {

			return null;
		}
	}
}
