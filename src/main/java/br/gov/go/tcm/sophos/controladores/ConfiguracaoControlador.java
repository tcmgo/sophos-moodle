/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.Anexo;
import br.gov.go.tcm.sophos.entidade.Dominio;
import br.gov.go.tcm.sophos.entidade.enumeracao.ConfiguracaoEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoArquivo;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoDominioEnum;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.lazy.base.TableLazyPadrao;
import br.gov.go.tcm.sophos.negocio.AnexoNegocio;
import br.gov.go.tcm.sophos.negocio.DominioNegocio;
import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.*;

@Component
@Scope("view")
public class ConfiguracaoControlador extends ControladorBaseCRUD<Dominio, DominioNegocio> {

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private DominioNegocio negocio;

	@Autowired
	private AnexoNegocio anexoNegocio;

	private List<Dominio> configuracaoList;

	private Anexo anexoFrenteCentificado;

	private Anexo anexoVersoCentificado;
	
	private Anexo anexoImgTopoRelatorio;

	@PostConstruct
	public void inicializarTela() {

		List<ConfiguracaoEnum> configuracaoEnumList = Arrays.asList(ConfiguracaoEnum.values());

		configuracaoList = this.getNegocio().listaPorAtributo("nome", TipoDominioEnum.Configuracao);

		for (ConfiguracaoEnum configuracaoEnum : configuracaoEnumList) {

			Boolean existe = Boolean.FALSE;

			Iterator<Dominio> itDominioConf = configuracaoList.iterator();

			while (itDominioConf.hasNext()) {

				Dominio conf = itDominioConf.next();

				if (configuracaoEnum.getCodigo().equals(conf.getCodigo())) {
					
					if(configuracaoEnum.equals(ConfiguracaoEnum.IMG_CERTIFICADO_FRENTE)){
						
						this.anexoFrenteCentificado = this.anexoNegocio.obterPorAtributo("arquivoGED", conf.getDescricao());
						
					}else if(configuracaoEnum.equals(ConfiguracaoEnum.IMG_CERTIFICADO_VERSO)){
						
						this.anexoVersoCentificado = this.anexoNegocio.obterPorAtributo("arquivoGED", conf.getDescricao());
						
					}else if(configuracaoEnum.equals(ConfiguracaoEnum.IMG_TOPO_RELATORIO)){
						
						this.anexoImgTopoRelatorio = this.anexoNegocio.obterPorAtributo("arquivoGED", conf.getDescricao());
					}

					existe = Boolean.TRUE;

					break;
				}

			}

			if (!existe) {

				Dominio dominioConf = new Dominio();

				dominioConf.setAtivo(Boolean.TRUE);

				dominioConf.setNome(TipoDominioEnum.Configuracao);

				dominioConf.setCodigo(configuracaoEnum.getCodigo());

				configuracaoList.add(dominioConf);
			}
		}
	}

	@Override
	public void acaoInserir() {

		try {

			for (Dominio dominio : this.getConfiguracaoList()) {

				if (dominio.getCodigo().equals(ConfiguracaoEnum.IMG_CERTIFICADO_FRENTE.getCodigo()) && AjudanteObjeto.eReferencia(anexoFrenteCentificado)) {

					this.anexoNegocio.salvar(this.anexoFrenteCentificado);

					dominio.setDescricao(this.anexoFrenteCentificado.getArquivoGED());

				} else if (dominio.getCodigo().equals(ConfiguracaoEnum.IMG_CERTIFICADO_VERSO.getCodigo()) && AjudanteObjeto.eReferencia(anexoVersoCentificado)) {

					this.anexoNegocio.salvar(this.anexoVersoCentificado);

					dominio.setDescricao(this.anexoVersoCentificado.getArquivoGED());
					
				} else if (dominio.getCodigo().equals(ConfiguracaoEnum.IMG_TOPO_RELATORIO.getCodigo()) && AjudanteObjeto.eReferencia(anexoImgTopoRelatorio)) {

					this.anexoNegocio.salvar(this.anexoImgTopoRelatorio);

					dominio.setDescricao(this.anexoImgTopoRelatorio.getArquivoGED());
				}
				
				if(!StringUtils.isEmpty(dominio.getDescricao())){
					
					this.getNegocio().salvar(dominio);
				}
				

			}

			this.adicionarMensagemInformativa(this.obterMensagemSucessoAoInserir());

			this.executarAposInserirSucesso();

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);
		}

	}
	
	@Override
	protected void executarAposInserirSucesso() {

		super.executarAposInserirSucesso();
		
		this.inicializarTela();
	}

	public void uploadFrenteCertificado(final FileUploadEvent event) {

		this.anexoFrenteCentificado = new Anexo();

		this.anexoFrenteCentificado.setFile(event.getFile());

		this.anexoFrenteCentificado.setDescricao(event.getFile().getFileName());

		this.anexoFrenteCentificado.setTipoArquivo(TipoArquivo.getTipoArquivo(event.getFile().getContentType()));

	}

	public void uploadVersoCertificado(final FileUploadEvent event) {

		this.anexoVersoCentificado = new Anexo();

		this.anexoVersoCentificado.setFile(event.getFile());

		this.anexoVersoCentificado.setDescricao(event.getFile().getFileName());

		this.anexoVersoCentificado.setTipoArquivo(TipoArquivo.getTipoArquivo(event.getFile().getContentType()));

	}
	
	public void uploadImgTopoRelatorio(final FileUploadEvent event) {

		this.anexoImgTopoRelatorio = new Anexo();

		this.anexoImgTopoRelatorio.setFile(event.getFile());

		this.anexoImgTopoRelatorio.setDescricao(event.getFile().getFileName());

		this.anexoImgTopoRelatorio.setTipoArquivo(TipoArquivo.getTipoArquivo(event.getFile().getContentType()));

	}
	
	public void limparAnexoFrente(){
		
		if(AjudanteObjeto.eReferencia(this.anexoFrenteCentificado.getId())){
			
			this.anexoNegocio.excluir(this.anexoFrenteCentificado);
			
			this.negocio.excluir(this.negocio.obterDominio(TipoDominioEnum.Configuracao, ConfiguracaoEnum.IMG_CERTIFICADO_FRENTE.getCodigo()));
		}
		
		this.anexoFrenteCentificado = null;
	}
	
	public void limparAnexoVerso(){
		
		if(AjudanteObjeto.eReferencia(this.anexoVersoCentificado.getId())){
			
			this.anexoNegocio.excluir(this.anexoVersoCentificado);
			
			this.negocio.excluir(this.negocio.obterDominio(TipoDominioEnum.Configuracao, ConfiguracaoEnum.IMG_CERTIFICADO_VERSO.getCodigo()));
		}

		this.anexoVersoCentificado = null;
	}
	
	public void limparAnexoTopoRelatorio(){
		
		if(AjudanteObjeto.eReferencia(this.anexoImgTopoRelatorio.getId())){
			
			this.anexoNegocio.excluir(this.anexoImgTopoRelatorio);
			
			this.negocio.excluir(this.negocio.obterDominio(TipoDominioEnum.Configuracao, ConfiguracaoEnum.IMG_TOPO_RELATORIO.getCodigo()));
		}

		this.anexoImgTopoRelatorio = null;
	}

	public String obterDescricaoConfiguracaoPorCodigo(Long codigo) {

		return ConfiguracaoEnum.obterPorCodigo(codigo).getDescricao();
	}

	@Override
	protected DominioNegocio getNegocio() {

		return this.negocio;
	}

	@Override
	public TableLazyPadrao<Dominio> getLista() {

		return null;
	}

	public List<Dominio> getConfiguracaoList() {
		Collections.sort(configuracaoList, new Comparator<Dominio>() {
			@Override
			public int compare(Dominio o1, Dominio o2) {
				return o1.getCodigo().compareTo(o2.getCodigo());
			}
		});
		return configuracaoList;
	}

	public void setConfiguracaoList(List<Dominio> configuracaoList) {
		this.configuracaoList = configuracaoList;
	}

	public Anexo getAnexoFrenteCentificado() {
		return anexoFrenteCentificado;
	}

	public void setAnexoFrenteCentificado(Anexo anexoFrenteCentificado) {
		this.anexoFrenteCentificado = anexoFrenteCentificado;
	}

	public Anexo getAnexoVersoCentificado() {
		return anexoVersoCentificado;
	}

	public void setAnexoVersoCentificado(Anexo anexoVersoCentificado) {
		this.anexoVersoCentificado = anexoVersoCentificado;
	}

	public Anexo getAnexoImgTopoRelatorio() {
		return anexoImgTopoRelatorio;
	}

	public void setAnexoImgTopoRelatorio(Anexo anexoImgTopoRelatorio) {
		this.anexoImgTopoRelatorio = anexoImgTopoRelatorio;
	}

}
