/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "gasto")
@Banco(nome = Banco.SOPHOS)
public class Gasto extends EntidadePadrao {

	private static final long serialVersionUID = 3958336940990802481L;

	@Column(name = "numero_empenho")
	private Integer numeroEmpenho;

	@Column(name = "ano_processo")
	private Integer anoProcesso;
	
	@Column(name = "seq_processo")
	private Integer seqProcesso;

	@Column(name = "observacao", length = 2000)
	private String observacao;

	@Column(name = "data_empenho")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataEmpenho;

	@Column(name = "valor", nullable = false)
	private BigDecimal valor;

	@JoinColumn(name = "evento_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Evento evento;

	@JoinColumn(name = "fonte_gasto_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Dominio fonteGasto;

	@JoinColumn(name = "tipo_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Dominio tipoId;

	public Integer getNumeroEmpenho() {
		return numeroEmpenho;
	}

	public void setNumeroEmpenho(Integer numeroEmpenho) {
		this.numeroEmpenho = numeroEmpenho;
	}

	public Integer getAnoProcesso() {
		return anoProcesso;
	}

	public void setAnoProcesso(Integer anoProcesso) {
		this.anoProcesso = anoProcesso;
	}

	public Integer getSeqProcesso() {
		return seqProcesso;
	}

	public void setSeqProcesso(Integer seqProcesso) {
		this.seqProcesso = seqProcesso;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Date getDataEmpenho() {
		return dataEmpenho;
	}

	public void setDataEmpenho(Date dataEmpenho) {
		this.dataEmpenho = dataEmpenho;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public Dominio getFonteGasto() {
		return fonteGasto;
	}

	public void setFonteGasto(Dominio fonteGasto) {
		this.fonteGasto = fonteGasto;
	}

	public Dominio getTipoId() {
		return tipoId;
	}

	public void setTipoId(Dominio tipoId) {
		this.tipoId = tipoId;
	}

	public String getNumeroProcesso(){
		if(AjudanteObjeto.eReferencia(this.getSeqProcesso()) && AjudanteObjeto.eReferencia(this.getAnoProcesso())){
			return this.getSeqProcesso()+"/"+this.getAnoProcesso();
		}
		return "";
	}
}
