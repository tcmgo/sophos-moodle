/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.Dominio;
import br.gov.go.tcm.sophos.entidade.Usuario;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoDominioEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoUsuario;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.lazy.base.TableLazyPadrao;
import br.gov.go.tcm.sophos.negocio.DominioNegocio;
import br.gov.go.tcm.sophos.negocio.UsuarioNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Scope("session")
public class MinhaContaControlador extends ControladorBaseCRUD<Usuario, UsuarioNegocio> {

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private UsuarioNegocio negocio;

	@Autowired
	private DominioNegocio dominioNegocio;

	private List<Dominio> sexoList;

	private Boolean permiteAlterarSenha = Boolean.FALSE;

	private String senhaAtual;

	private String senha;

	private String senhaRepetida;

	public void inicializarTela() {

		this.setEntidade(this.getUsuarioLogado());

		this.permiteAlterarSenha = this.getEntidade().getTipoUsuario().equals(this.dominioNegocio.obterDominio(TipoDominioEnum.TipoUsuario, TipoUsuario.USUARIO_EXTERNO.getCodigo()));
	}

	@Override
	public void acaoInserir() {

		try {

			this.getNegocio().salvarSimples(this.getEntidade());

			this.adicionarMensagemInformativa(this.obterMensagemSucessoAoInserir());

			this.executarAposInserirSucesso();

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);
		}
	}
	
	public void acaoRedefinirSenha() {

		try {

			this.setEntidade(this.getUsuarioLogado());
			
			this.negocio.redefinirSenha(this.getEntidade(), this.getSenhaAtual(), this.getSenha(), this.getSenhaRepetida());
			
			this.adicionarMensagemInformativa("Senha alterada com sucesso!");
			
			this.setSenhaAtual(null);

			this.setSenha(null);

			this.setSenhaRepetida(null);

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);
		}
	}

	@Override
	public TableLazyPadrao<Usuario> getLista() {

		return null;
	}

	@Override
	protected UsuarioNegocio getNegocio() {

		return negocio;
	}

	public List<Dominio> getSexoList() {

		if (!AjudanteObjeto.eReferencia(sexoList)) {

			this.sexoList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.TipoSexo);
		}

		return sexoList;
	}

	public Boolean getPermiteAlterarSenha() {
		return permiteAlterarSenha;
	}

	public void setPermiteAlterarSenha(Boolean permiteAlterarSenha) {
		this.permiteAlterarSenha = permiteAlterarSenha;
	}

	public String getSenhaAtual() {
		return senhaAtual;
	}

	public void setSenhaAtual(String senhaAtual) {
		this.senhaAtual = senhaAtual;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getSenhaRepetida() {
		return senhaRepetida;
	}

	public void setSenhaRepetida(String senhaRepetida) {
		this.senhaRepetida = senhaRepetida;
	}

}
