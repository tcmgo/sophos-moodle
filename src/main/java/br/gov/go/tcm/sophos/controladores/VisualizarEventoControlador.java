/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.estrutura.entidade.orcafi.Municipio;
import br.gov.go.tcm.estrutura.persistencia.orcafi.contrato.MunicipioDAO;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.ModuloInstrutor;
import br.gov.go.tcm.sophos.entidade.enumeracao.PaginaEnum;
import br.gov.go.tcm.sophos.lazy.TableLazyEvento;
import br.gov.go.tcm.sophos.negocio.EventoNegocio;
import br.gov.go.tcm.sophos.negocio.EventoPublicoAlvoNegocio;
import br.gov.go.tcm.sophos.negocio.ModuloInstrutorNegocio;
import br.gov.go.tcm.sophos.negocio.ModuloNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.util.List;

@Component
@Scope("session")
public class VisualizarEventoControlador extends ControladorBaseCRUD<Evento, EventoNegocio> {

	private static final String EVENTO_SELECIONADO = "EVENTO_SELECIONADO";

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private EventoNegocio negocio;

	@Autowired
	private EventoPublicoAlvoNegocio eventoPublicoAlvo;

	@Autowired
	private ModuloNegocio moduloNegocio;

	@Autowired
	private ModuloInstrutorNegocio moduloInstrutorNegocio;

	@Autowired
	private NavegacaoControlador navegacaoControlador;
	
	@Autowired
	private MunicipioDAO municipioDAO;

	private List<Evento> listaEvento;

	private List<Evento> listaEventoCertificadosDisponiveis;

	private List<Evento> listaEventoInscricaoAbertas;

	private List<ModuloInstrutor> listaInstrutor;

	public void inicializarTela() {

		this.listaEvento = this.negocio.listarPorTipoParticipante(this.getUsuarioLogado());

		for (Evento evento : this.listaEvento) {

			evento.setPublicoAlvoList(eventoPublicoAlvo.listaPorAtributo("evento", evento));
			
		}

		this.listaEventoCertificadosDisponiveis = this.negocio.listarEventoComCertificadosDisponiveis();

		this.listaEventoInscricaoAbertas = this.negocio.listarEventoComInscricoesAbertas();

		Evento evento = this.obterAtributoNaSessao(EVENTO_SELECIONADO, Evento.class);

		if (AjudanteObjeto.eReferencia(evento)) {

			this.setEntidade(evento);
		}

	}

	public void selecionarEvento(Evento evento) {

		evento.setPublicoAlvoList(eventoPublicoAlvo.listaPorAtributo("evento", evento));

		evento.setModuloList(moduloNegocio.listarOrdenadoPorData(evento));

		this.setListaInstrutor(moduloInstrutorNegocio.listarInstrutoresEvento(evento));
		
		if(AjudanteObjeto.eReferencia(evento.getLocalizacao().getEndereco().getMunicipioId())){
			
			evento.getLocalizacao().getEndereco().setMunicipio((Municipio) municipioDAO.obterEntidadePorId(Municipio.class, evento.getLocalizacao().getEndereco().getMunicipioId()));
		}

		this.setEntidade(evento);

		Evento eventoSessao = this.obterAtributoNaSessao(EVENTO_SELECIONADO, Evento.class);

		if (AjudanteObjeto.eReferencia(eventoSessao)) {

			this.obterContextoJSF().getExternalContext().getSessionMap().remove(EVENTO_SELECIONADO);

		}

		this.registrarAtributoNaSessao(EVENTO_SELECIONADO, this.getEntidade());

	}

	public void selecionarEvento(Long idEvento) {

		Evento evento = this.negocio.obter(idEvento);

		this.selecionarEvento(evento);

		this.navegacaoControlador.acaoAbrirTela(PaginaEnum.EVENTOS_DETALHE);

	}

	public void linkParaEvento(Long idEvento) {

		Evento evento = this.negocio.obter(idEvento);

		try {
			if(evento.getCursoMoodleId() != null) {
				 ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				 externalContext.redirect("https://<dominio_moodle>/ead/course/view.php?id="
						 + evento.getCursoMoodleId());
			}else {
				selecionarEvento(idEvento);
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected EventoNegocio getNegocio() {

		return this.negocio;
	}

	@Override
	public TableLazyEvento getLista() {

		return null;
	}

	public List<Evento> getListaEvento() {

		return listaEvento;
	}

	public void setListaEvento(List<Evento> listaEvento) {
		this.listaEvento = listaEvento;
	}

	public List<Evento> getListaEventoCertificadosDisponiveis() {
		return listaEventoCertificadosDisponiveis;
	}

	public void setListaEventoCertificadosDisponiveis(List<Evento> listaEventoCertificadosDisponiveis) {
		this.listaEventoCertificadosDisponiveis = listaEventoCertificadosDisponiveis;
	}

	public List<Evento> getListaEventoInscricaoAbertas() {
		return listaEventoInscricaoAbertas;
	}

	public void setListaEventoInscricaoAbertas(List<Evento> listaEventoInscricaoAbertas) {
		this.listaEventoInscricaoAbertas = listaEventoInscricaoAbertas;
	}

	public List<ModuloInstrutor> getListaInstrutor() {
		return listaInstrutor;
	}

	public void setListaInstrutor(List<ModuloInstrutor> listaInstrutor) {
		this.listaInstrutor = listaInstrutor;
	}

	public boolean verificaPermissaoInscricao(Evento evento) {

		return this.negocio.verificaPermissaoInscricao(evento);
	}

}
