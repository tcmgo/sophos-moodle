/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.sophos.entidade.ArquivoAdministrativo;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import br.gov.go.tcm.sophos.persistencia.ArquivoAdministrativoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ArquivoAdministrativoNegocio extends NegocioBase<ArquivoAdministrativo> {

	private static final long serialVersionUID = -8426728536010364058L;

	@Autowired
	private ArquivoAdministrativoDAO dao;
	
	@Autowired
	private AnexoNegocio anexoNegocio;

	@Override
	protected ArquivoAdministrativoDAO getDAO() {

		return this.dao;
	}
	
	@Override
	public void salvar(ArquivoAdministrativo entidade) {

		this.anexoNegocio.salvar(entidade.getArquivo());
		
		super.salvar(entidade);
	}
	
	@Override
	public void excluir(ArquivoAdministrativo entidade) {

		super.excluir(entidade);
		
		this.anexoNegocio.excluir(entidade.getArquivo());
	}
	
}
