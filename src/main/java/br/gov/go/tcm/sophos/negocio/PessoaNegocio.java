/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.Pessoa;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import br.gov.go.tcm.sophos.persistencia.PessoaDAO;
import br.gov.go.tcm.sophos.rh.PessoaVo;
import br.gov.go.tcm.sophos.rh.RHPessoaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PessoaNegocio extends NegocioBase<Pessoa> {

	private static final long serialVersionUID = 1582876277102965222L;

	@Autowired
	private PessoaDAO dao;
	
	@Autowired
	private RHPessoaDAO RHPessoaDAO;

	@Override
	protected PessoaDAO getDAO() {

		return this.dao;
	}

	public boolean existePessoaComEmail(String email) {
		return dao.existePessoaComEmail(email);
	}
	
	public boolean existePessoaComCPF(String cpf) {
		return dao.existePessoaComCPF(cpf);
	}
	
	public boolean existePessoaComCPFBaseRH(String cpf) {
		
		PessoaVo pesssoaVo = RHPessoaDAO.obterPorCPF(cpf.replace(".", "").replace("-", ""));
		
		if (AjudanteObjeto.eReferencia(pesssoaVo)) {
			return true;
		} else {
			return false;
		}
 	}

}
