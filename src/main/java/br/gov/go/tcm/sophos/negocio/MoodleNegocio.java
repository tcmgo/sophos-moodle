package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.estrutura.seguranca.ajudante.AjudanteCriptografia;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.ajudante.AjudanteString;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Inscricao;
import br.gov.go.tcm.sophos.entidade.Participante;
import br.gov.go.tcm.sophos.entidade.Usuario;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.rest.MoodleAPICliente;
import br.gov.go.tcm.sophos.rest.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
    Camada dedicada a implementar as regras de negócio relacionadas a integração Moodle -> Sophos.
    A comunicação com o Moodle é feita através de chamadas Rest.
 */

@Service
public class MoodleNegocio implements Serializable {

    @Autowired
    private MoodleAPICliente moodleAPICliente;

    public List<MoodleCursoDTO> obterListaCursosMoodle() {

        return this.moodleAPICliente.moodleObterListaCursos();
    }

    public boolean eventoTemCursoMoodleVinculado(Evento evento) {

        if (AjudanteObjeto.eReferencia(evento)) {

            if (AjudanteObjeto.eReferencia(evento.getCursoMoodleId())) {

                if (evento.getCursoMoodleId().intValue() > 0) {

                    return Boolean.TRUE;
                }
            }
        }
        return Boolean.FALSE;
    }

    public boolean participanteExisteNoMoodle(Participante participante) {

        if (!AjudanteObjeto.eReferencia(participante)) return Boolean.FALSE;
        if (!AjudanteObjeto.eReferencia(participante.getPessoa())) return Boolean.FALSE;
        if (AjudanteString.verificarStringNulaOuVazia(participante.getPessoa().getCpf())) return Boolean.FALSE;

        if(this.moodleAPICliente.moodleObterPessoaPorCpf(participante.getPessoa().getCpf()) == null) {

            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    /*
        O Plugin oficial do Moodle responsável por autenticação através de bancos de dados externos
        faz uma verificação toda vez que alguém tenta logar. A chamada abaixo faz com que um login seja tentado
        através da API Rest do Moodle, fazendo com que o Moodle consulte o banco de dados externo,
        e se o cpf e a senha estiverem corretos com o que está no Sophos, o Moodle irá
        criar internamente um usuário com estes dados, pronto para ser operado pelas outras chamadas.
        Ou seja, a chamada abaixo força a criação no Moodle de uma pessoa que está cadastrada no Sophos mas que nunca tenha acessado o Moodle.
        Através do primeiro login forçado abaixo, a criação é feita.
        O retorno não interessa já que o login não será bem sucedido por falta de permissão de acesso via Rest API.
    */
    public void forcarPrimeiroLoginMoodle(Inscricao inscricao, Usuario usuario, String senhaColetada) {

        if (!AjudanteObjeto.eReferencia(inscricao)) return;
        if (!AjudanteObjeto.eReferencia(inscricao.getParticipante())) return;
        if (!AjudanteObjeto.eReferencia(inscricao.getParticipante().getPessoa())) return;
        if (AjudanteString.verificarStringNulaOuVazia(inscricao.getParticipante().getPessoa().getCpf())) return;
        if (!AjudanteObjeto.eReferencia(usuario)) return;
        if (!AjudanteObjeto.eReferencia(usuario.getSenha())) return;

        if (AjudanteObjeto.eReferencia(inscricao.getAutorizada())) {

            if (inscricao.getAutorizada().booleanValue() == Boolean.TRUE) {

                if (AjudanteString.verificarStringNulaOuVazia(senhaColetada)) {

                    throw new ValidacaoException("Por favor, preencha a senha.");

                } else if (!AjudanteCriptografia.gerarMD5(senhaColetada).equals(usuario.getSenha())) {

                    throw new ValidacaoException("Senha incorreta.");

                } else {

                    this.moodleAPICliente.forcarPrimeiroLoginPessoaMoodle(inscricao.getParticipante().getPessoa().getCpf(), senhaColetada);
                }
            }
        }
    }

    public boolean inscreverParticipanteCursoMoodleVinculado(Evento evento, Inscricao inscricao) {

        if (!this.eventoTemCursoMoodleVinculado(evento)) return Boolean.FALSE;
        if (!AjudanteObjeto.eReferencia(inscricao)) return Boolean.FALSE;
        if (!AjudanteObjeto.eReferencia(inscricao.getParticipante())) return Boolean.FALSE;
        if (!AjudanteObjeto.eReferencia(inscricao.getParticipante().getPessoa())) return Boolean.FALSE;

        if (AjudanteObjeto.eReferencia(inscricao.getAutorizada())) {

            if (inscricao.getAutorizada().booleanValue() == Boolean.TRUE) {

                if (!AjudanteObjeto.eVazia(inscricao.getParticipante().getPessoa().getCpf())) {

                    if (this.moodleAPICliente.moodleObterPessoaPorCpf(inscricao.getParticipante().getPessoa().getCpf()) == null) return Boolean.FALSE;

                    this.moodleAPICliente.moodleMatricularEstudanteCurso(inscricao.getParticipante().getPessoa().getCpf(), inscricao.getEvento().getCursoMoodleId());
                }
            }
        }
        return Boolean.TRUE;
    }

    public String importarNotaDoCursoMoodleVinculado(Evento evento, Inscricao inscricao) {

        MoodlePessoaDTO moodlePessoaDTO = this.moodleAPICliente.moodleObterPessoaPorCpf(
        		inscricao.getParticipante().getPessoa().getCpf());
                
        MoodleTabelaPrincipalNotaDTO moodleTabelaPrincipalNotaDTO = 
        		this.moodleAPICliente.moodleObterNotaPorId(evento.getCursoMoodleId(), 
        				moodlePessoaDTO.getId());
        
        MoodleTabelaNotaDTO[] tables = moodleTabelaPrincipalNotaDTO.getTables();

        // Consulta retorna apenas uma tabela
        MoodleTabelaDadosNotaDTO[] tabledata = tables[0].getTabledata();

        // Consulta retorna 3 dados da tabela. O segundo dado da tabela consta a porcentagem total
        MoodleTabelaDadosTotalNotaDTO contributiontocoursetotal = 
        		tabledata[1].getContributiontocoursetotal();
        		
		try {
			return contributiontocoursetotal.getContent();
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
    }

    public Map<Long, Double> obterHashMapIdNotasDoCursoMoodleVinculado(
    		Long cursoMoodleId) {
    
    	MoodleTabelaPrincipalNotaDTO moodleTabelaPrincipalNotaDTO =
    			this.moodleAPICliente.moodleObterNotas(cursoMoodleId);
    	
    	Map<Long, Double> hashMapIdNotas = new HashMap<Long, Double>();

		MoodleTabelaNotaDTO[] tables = moodleTabelaPrincipalNotaDTO.getTables();
		
		for(MoodleTabelaNotaDTO table : tables) {
				
			MoodleTabelaDadosNotaDTO[] tabledata = table.getTabledata();
				
			// Consulta retorna 3 dados da tabela. O segundo dado da tabela consta a
			// porcentagem total
			// Ex.: https://<dominio>/ead/webservice/rest/server.php?wstoken=<token_acesso>&wsfunction=gradereport_user_get_grades_table&moodlewsrestformat=json&courseid=71
			MoodleTabelaDadosTotalNotaDTO contributiontocoursetotal = 
					tabledata[1].getContributiontocoursetotal();
			
			String notaString = contributiontocoursetotal.getContent();
			
			//"content":"-"
			if(!notaString.equals("-")) {
				
				//"content":"95,00 %"
				Double nota = Double.parseDouble(notaString.substring(
						0, notaString.length() - 2).replace(",", "."));
		        
				hashMapIdNotas.put(table.getUserid(), nota);
			}
		}
		return hashMapIdNotas;
    }

    public MoodlePessoaDTO obterPessoaPorCpf(String cpf) {
    
    	return this.moodleAPICliente.moodleObterPessoaPorCpf(cpf);
    }

    public boolean desinscreverParticipanteCursoMoodleVinculado(Evento evento, Inscricao inscricao) {

        if (!this.eventoTemCursoMoodleVinculado(evento)) return Boolean.FALSE;
        if (!AjudanteObjeto.eReferencia(inscricao)) return Boolean.FALSE;
        if (!AjudanteObjeto.eReferencia(inscricao.getParticipante())) return Boolean.FALSE;
        if (!AjudanteObjeto.eReferencia(inscricao.getParticipante().getPessoa())) return Boolean.FALSE;

        if (!AjudanteObjeto.eVazia(inscricao.getParticipante().getPessoa().getCpf())) {

            if (this.moodleAPICliente.moodleObterPessoaPorCpf(inscricao.getParticipante().getPessoa().getCpf()) == null) return Boolean.FALSE;

            this.moodleAPICliente.moodleRetirarMatriculaEstudanteCurso(inscricao.getParticipante().getPessoa().getCpf(), inscricao.getEvento().getCursoMoodleId());
        }
        return Boolean.TRUE;
    }

    public boolean eventoPodeVincularCursoMoodle(Evento evento) {

        if (!AjudanteObjeto.eReferencia(evento)) return Boolean.FALSE;
        if (!AjudanteObjeto.eReferencia(evento.getModalidade())) return Boolean.FALSE;
        if (!AjudanteObjeto.eReferencia(evento.getModalidade().getCodigo())) return Boolean.FALSE;

        if (evento.getModalidade().getCodigo().intValue() != 2) return Boolean.FALSE;

        return Boolean.TRUE;
    }
}
