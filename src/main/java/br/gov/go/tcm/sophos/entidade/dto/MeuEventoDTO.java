/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade.dto;

import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Inscricao;

import java.io.Serializable;
import java.math.BigDecimal;

public class MeuEventoDTO implements Serializable{

	private static final long serialVersionUID = -4814098485439374174L;

	private Evento evento;

	private Boolean aprovado;

	private Boolean avaliado;

	private BigDecimal nota;
	
	private Inscricao inscricao;

	public MeuEventoDTO() {
		super();
	}

	public MeuEventoDTO(Evento evento, Boolean aprovado, Boolean avaliado, Inscricao inscricao) {
		super();
		this.evento = evento;
		this.aprovado = aprovado;
		this.avaliado = avaliado;
		this.inscricao = inscricao;
	}

	public MeuEventoDTO(Evento evento, Boolean aprovado, Boolean avaliado, BigDecimal nota) {
		super();
		this.evento = evento;
		this.aprovado = aprovado;
		this.avaliado = avaliado;
		this.nota = nota;
	}

	public MeuEventoDTO(Evento evento, Boolean aprovado, Boolean avaliado, BigDecimal nota, Inscricao inscricao) {
		super();
		this.evento = evento;
		this.aprovado = aprovado;
		this.avaliado = avaliado;
		this.nota = nota;
		this.inscricao = inscricao;
	}

	public Inscricao getInscricao() {
		return inscricao;
	}

	public void setInscricao(Inscricao inscricao) {
		this.inscricao = inscricao;
	}

	public BigDecimal getNota() {
		return nota;
	}

	public void setNota(BigDecimal nota) {
		this.nota = nota;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public Boolean getAprovado() {
		return aprovado;
	}

	public void setAprovado(Boolean aprovado) {
		this.aprovado = aprovado;
	}

	public Boolean getAvaliado() {
		return avaliado;
	}

	public void setAvaliado(Boolean avaliado) {
		this.avaliado = avaliado;
	}

}
