/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "evento_publico_alvo")
@Banco(nome = Banco.SOPHOS)
public class EventoPublicoAlvo extends EntidadePadrao {

	private static final long serialVersionUID = 6691829646900296754L;

	@JoinColumn(name = "publico_alvo_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Dominio publicoAlvo;

	@JoinColumn(name = "evento_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Evento evento;

	public Dominio getPublicoAlvo() {
		return publicoAlvo;
	}

	public void setPublicoAlvo(Dominio publicoAlvo) {
		this.publicoAlvo = publicoAlvo;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

}
