/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.persistencia.impl;

import br.gov.go.tcm.sophos.entidade.Dominio;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoDominioEnum;
import br.gov.go.tcm.sophos.persistencia.DominioDAO;
import br.gov.go.tcm.sophos.persistencia.base.DAO;
import br.gov.go.tcm.sophos.persistencia.impl.base.DAOGenerico;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class DominioDAOImpl extends DAOGenerico<Dominio> implements DominioDAO {

	private static final long serialVersionUID = 294560128456827146L;

	@Autowired
	public DominioDAOImpl(@Qualifier(DAO.SESSION_FACTORY_SOPHOS) final SessionFactory factory, @Qualifier(DAO.HIBERNATE_TEMPLATE_SOPHOS) final HibernateTemplate hibernateTemplate) {

		this.setHibernateTemplate(hibernateTemplate);

		this.setSessionFactory(factory);
	}
	
	public Dominio obterDominio(TipoDominioEnum nome, Long codigo){
		
		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		criteria.add(Restrictions.eq("nome", nome));
		
		criteria.add(Restrictions.eq("codigo", codigo));
		
		criteria.setMaxResults(1);
		
		try {

			return (Dominio) criteria.uniqueResult();

		} finally {

			session.close();
		}
		
	}

	@Override
	public Long obterUltimoCodigoDominio(TipoDominioEnum nome) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		criteria.add(Restrictions.eq("nome", nome));
		
		criteria.setMaxResults(1);
		
		criteria.addOrder(Order.desc("codigo"));
		
		try {

			return ((Dominio) criteria.uniqueResult()).getCodigo();

		} finally {

			session.close();
		}
	}
	
}
