/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Modulo;
import br.gov.go.tcm.sophos.entidade.Nota;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.lazy.TableLazyInscricaoNota;
import br.gov.go.tcm.sophos.negocio.InscricaoNegocio;
import br.gov.go.tcm.sophos.negocio.ModuloNegocio;
import br.gov.go.tcm.sophos.negocio.NotaNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Scope("session")
public class NotaControlador extends ControladorBaseCRUD<Nota, NotaNegocio> {

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private NotaNegocio negocio;

	@Autowired
	private ModuloNegocio moduloNegocio;

	@Autowired
	private InscricaoNegocio inscricaoNegocio;

	private Evento evento;

	private Modulo modulo;

	private List<Modulo> modulosList;

	@Autowired
	private TableLazyInscricaoNota lista;
	
	public void selecionarEvento(Evento evento) {
		
		List<Modulo> modulos = this.moduloNegocio.listaPorAtributo("evento", evento);
		
		if (modulos.size() == 0) {
			
			this.adicionarMensagemDeAlerta("Não é possível atribuir notas, pois o evento não possui módulos cadastrados. Edite o evento para ter ao menos um MÓDULO ÚNICO.");
			
		} else {
		
			this.setEntidade(new Nota());
	
			this.evento = evento;
			
			this.modulo = new Modulo();
	
			this.lista.setEvento(evento);
			
			this.lista.setConsulta("");
			
			this.setModulosList(this.moduloNegocio.listaPorAtributo("evento", evento));
			
			this.executarJS("openModal('modalNota')");
		
		}
	}

	public void popularNotas() {
		
		this.lista.setModulo(this.modulo);
	}

	public void salvarNota() {

		try {

			this.getNegocio().salvar(this.getLista().getLista());

			this.adicionarMensagemInformativa(this.obterMensagemSucessoAoInserir());

			this.executarAposInserirSucesso();

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);
		}

	}
	

	@Override
	protected NotaNegocio getNegocio() {

		return this.negocio;
	}

	@Override
	public TableLazyInscricaoNota getLista() {

		return this.lista;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public Modulo getModulo() {
		return modulo;
	}

	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}

	public List<Modulo> getModulosList() {
		return modulosList;
	}

	public void setModulosList(List<Modulo> modulosList) {
		this.modulosList = modulosList;
	}
	
	
}
