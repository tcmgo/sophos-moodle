/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Inscricao;
import br.gov.go.tcm.sophos.entidade.Participante;
import br.gov.go.tcm.sophos.entidade.Pessoa;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.lazy.TableLazyInscricao;
import br.gov.go.tcm.sophos.negocio.InscricaoNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Scope("session")
public class InscricaoParticipanteControlador extends ControladorBaseCRUD<Inscricao, InscricaoNegocio> {

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private InscricaoNegocio negocio;

	@Autowired
	private TableLazyInscricao lista;

	private List<Participante> participanteList;

	private Evento evento;

	private Participante participante;

	public void inicializarTela() {

		this.participanteList = new ArrayList<Participante>();

		this.evento = new Evento();

		this.novoParticipante();
	}

	public void removerParticipante(Participante participante) {

		this.getParticipanteList().remove(participante);
	}

	public void adicionarParticipante() {

		if (isCamposPreechidos()) {

			if (AjudanteObjeto.eReferencia(this.getParticipante()) && AjudanteObjeto.eReferencia(this.getParticipante().getId()) && !this.getParticipanteList().contains(this.getParticipante())) {

				if(this.negocio.existeInscricaoParaParticipante(this.getParticipante(), this.getEvento())){
					
					this.adicionarMensagemDeErro("Já existe uma inscrição deste participante para este evento.");
					
				}else{
					
					this.getParticipanteList().add(this.getParticipante());

				}
				
			}

			this.novoParticipante();

		}
	}

	private boolean isCamposPreechidos() {

		if (!AjudanteObjeto.eReferencia(this.getEvento()) || !AjudanteObjeto.eReferencia(this.getEvento().getId())) {

			this.adicionarMensagemDeAlerta(this.obterMensagemPelaChave("informe_evento"));
			return false;

		} else if (!AjudanteObjeto.eReferencia(this.getParticipante())
				|| !AjudanteObjeto.eReferencia(this.getParticipante().getId())) {

			this.adicionarMensagemDeAlerta(this.obterMensagemPelaChave("informe_participante"));
			return false;

		} else {

			return true;
		}

	}

	private void novoParticipante() {

		this.participante = new Participante();

		this.participante.setPessoa(new Pessoa());
	}

	@Override
	public void acaoInserir() {

		try {

			List<Inscricao> inscricaoList = prepararSalvar();

			this.getNegocio().salvar(inscricaoList);

			this.adicionarMensagemInformativa(this.obterMensagemSucessoAoInserir());

			this.executarAposInserirSucesso();

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);
		}

	}

	@Override
	protected void executarAposInserirSucesso() {

		super.executarAposInserirSucesso();

		this.inicializarTela();
	}

	private List<Inscricao> prepararSalvar() {

		List<Inscricao> inscricaoList = new ArrayList<Inscricao>();

		Boolean autorizada = this.negocio.verificaAutorizacaoInscricao(this.getEvento());

		for (Participante participante : this.getParticipanteList()) {

			Inscricao inscricao = new Inscricao();

			inscricao.setAutorizada(autorizada);

			inscricao.setPreInscricao(this.getEvento().getPermitePreInscricao());

			inscricao.setEvento(this.getEvento());

			inscricao.setParticipante(participante);

			inscricaoList.add(inscricao);
		}

		return inscricaoList;
	}

	@Override
	public TableLazyInscricao getLista() {

		return lista;
	}

	@Override
	protected InscricaoNegocio getNegocio() {

		return negocio;
	}

	public Evento getEvento() {
		if (evento == null) {
			evento = new Evento();
		}
		return evento;
	}

	public void setEvento(Evento evento) {
		if(evento != null){
			this.evento = evento;
		}
	}

	public List<Participante> getParticipanteList() {
		if (participanteList == null) {
			this.participanteList = new ArrayList<Participante>();
		}
		return participanteList;
	}

	public void setParticipanteList(List<Participante> participanteList) {
		this.participanteList = participanteList;
	}

	public Participante getParticipante() {
		if (participante == null) {
			this.novoParticipante();
		}
		return participante;
	}

	public void setParticipante(Participante participante) {
		if (AjudanteObjeto.eReferencia(participante)) {
			this.participante = participante;
		}
	}

}
