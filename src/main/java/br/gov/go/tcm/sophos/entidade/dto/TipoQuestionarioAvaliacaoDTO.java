/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade.dto;

import br.gov.go.tcm.sophos.entidade.QuestionarioAvaliacao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TipoQuestionarioAvaliacaoDTO implements Serializable{
	
	private static final long serialVersionUID = -4723761972777012624L;

	private String chave;

	private List<QuestionarioAvaliacao> questionarioAvaliacaoList;

	public TipoQuestionarioAvaliacaoDTO() {
		super();
	}

	public TipoQuestionarioAvaliacaoDTO(String chave) {
		super();
		this.chave = chave;
	}

	public TipoQuestionarioAvaliacaoDTO(String chave, List<QuestionarioAvaliacao> questionarioAvaliacaoList) {
		super();
		this.chave = chave;
		this.questionarioAvaliacaoList = questionarioAvaliacaoList;
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public List<QuestionarioAvaliacao> getQuestionarioAvaliacaoList() {
		if(questionarioAvaliacaoList == null){
			questionarioAvaliacaoList = new ArrayList<QuestionarioAvaliacao>();
		}
		return questionarioAvaliacaoList;
	}

	public void setQuestionarioAvaliacaoList(List<QuestionarioAvaliacao> questionarioAvaliacaoList) {
		this.questionarioAvaliacaoList = questionarioAvaliacaoList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((chave == null) ? 0 : chave.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoQuestionarioAvaliacaoDTO other = (TipoQuestionarioAvaliacaoDTO) obj;
		if (chave == null) {
			if (other.chave != null)
				return false;
		} else if (!chave.equals(other.chave))
			return false;
		return true;
	}
	
}
