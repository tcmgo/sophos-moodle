/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.ajudante;

import java.util.Random;
import java.util.UUID;

public class AjudanteIdentificacao {

	public static String uuid() {

		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	public static String gerarCodigoValidacao() {
		
		String str = new String("ABCDEFGHIJKLMNPQRSTUVWXYZ123456789");
		
		StringBuffer sb = new StringBuffer();
		
		Random r = new Random();
		
		int te = 0;
		
		for (int i = 1; i <= 15; i++) {
			
			te = r.nextInt(33);
			
			sb.append(str.charAt(te));
		}

		int m = sb.length() / 5;

		for (int i = 0, j = 1; j <= m - 1; i++, j++) {
			
			sb.insert(j * 5 + i, ".");
		}

		return sb.toString();
	}
	
	public static void main(String[] args) {
		
		System.out.println(AjudanteIdentificacao.gerarCodigoValidacao());
		
	}
}
