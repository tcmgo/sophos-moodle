/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.estrutura.entidade.transiente.Arquivo;
import br.gov.go.tcm.estrutura.negocio.ajudante.AjudanteDeArquivos;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.*;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoArquivo;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoDominioEnum;
import br.gov.go.tcm.sophos.lazy.TableLazyMaterial;
import br.gov.go.tcm.sophos.negocio.DominioNegocio;
import br.gov.go.tcm.sophos.negocio.MaterialNegocio;
import br.gov.go.tcm.sophos.negocio.ModuloNegocio;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@Scope("session")
public class MaterialControlador extends ControladorBaseCRUD<Material, MaterialNegocio> {

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private MaterialNegocio negocio;

	@Autowired
	private ModuloNegocio moduloNegocio;

	@Autowired
	private DominioNegocio dominioNegocio;

	@Autowired
	private TableLazyMaterial lista;

	private List<Modulo> modulosList;

	private List<Dominio> tipoMaterialList;

	private Evento evento;
	
	public void limparAnexo(){
		
		this.getEntidade().setArquivo(new Anexo());
	}

	public void selecionarEvento(Evento evento, Boolean visualizar) {

		this.setVisualizar(visualizar);
		
		this.setEntidade(new Material());

		this.getEntidade().setArquivo(new Anexo());

		this.evento = evento;

		this.setModulosList(this.moduloNegocio.listaPorAtributo("evento", evento));

		this.setTipoMaterialList(this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.TipoMaterial));
		
		this.executarJS("openModalMaterial()");
	}

	@Override
	public void download(final String idArquivo) {

		try {

			final Arquivo arquivo = AjudanteDeArquivos.abrir(idArquivo);
			
			this.obterObjetoDaResposta().setContentType("application/octet-stream"); 
			
			this.obterObjetoDaResposta().setHeader("Content-disposition", "attachment; filename=" + UUID.randomUUID() + "." + arquivo.getSufixo());

			this.obterObjetoDaResposta().getOutputStream().write(IOUtils.toByteArray(arquivo.getStream()));
			
			this.obterObjetoDaResposta().getOutputStream().flush();

			this.obterObjetoDaResposta().getOutputStream().close();
			
			this.obterObjetoDaResposta().flushBuffer();

			this.obterContexto().responseComplete();

		} catch (final Exception e) {

			this.adicionarMensagemDeErro(e.getMessage());
		}
	}
	
	@Override
	public void acaoInserir() {
		
		if (this.getEntidade().getArquivo().getFile() != null) {
			
			this.getEntidade().setEvento(this.evento);
			
			super.acaoInserir();
			
			this.getEntidade().setArquivo(new Anexo());
			
		} else {
			this.adicionarMensagemDeAlerta("Por favor, informe o anexo.");
		}

		
	}
	
	
	public void limpar() {

		Evento evento = this.getEntidade().getEvento();

		this.setEntidade(new Material());

		this.getEntidade().setArquivo(new Anexo());

		this.getEntidade().setEvento(evento);
	}

	public void uploadAnexo(final FileUploadEvent event) {

		this.getEntidade().setArquivo(new Anexo());

		this.getEntidade().getArquivo().setDataCadastro(new Date());

		this.getEntidade().getArquivo().setFile(event.getFile());

		this.getEntidade().getArquivo().setDescricao(event.getFile().getFileName());

		this.getEntidade().getArquivo().setTipoArquivo(TipoArquivo.getTipoArquivo(event.getFile().getContentType()));

	}

	@Override
	public TableLazyMaterial getLista() {

		if (AjudanteObjeto.eReferencia(this.evento)) {

			Map<String, Object> filters = new HashMap<String, Object>();

			filters.put("evento", this.evento);

			this.lista.setFilters(filters);
		}

		return lista;
	}

	@Override
	protected MaterialNegocio getNegocio() {

		return this.negocio;
	}

	public List<Modulo> getModulosList() {
		return modulosList;
	}

	public void setModulosList(List<Modulo> modulosList) {
		this.modulosList = modulosList;
	}

	public List<Dominio> getTipoMaterialList() {
		return tipoMaterialList;
	}

	public void setTipoMaterialList(List<Dominio> tipoMaterialList) {
		this.tipoMaterialList = tipoMaterialList;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

}
