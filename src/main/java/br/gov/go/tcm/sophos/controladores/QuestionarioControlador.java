/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.Opcao;
import br.gov.go.tcm.sophos.entidade.Questionario;
import br.gov.go.tcm.sophos.entidade.QuestionarioAvaliacao;
import br.gov.go.tcm.sophos.entidade.enumeracao.PaginaEnum;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.lazy.TableLazyQuestionario;
import br.gov.go.tcm.sophos.negocio.OpcaoNegocio;
import br.gov.go.tcm.sophos.negocio.QuestionarioAvaliacaoNegocio;
import br.gov.go.tcm.sophos.negocio.QuestionarioNegocio;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Component
@Scope("session")
public class QuestionarioControlador extends ControladorBaseCRUD<Questionario, QuestionarioNegocio> {

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private QuestionarioNegocio negocio;
	
	@Autowired
	private QuestionarioAvaliacaoNegocio questionarioAvaliacaoNegocio;
	
	@Autowired
	private OpcaoNegocio opcaoNegocio;

	@Autowired
	private TableLazyQuestionario lista;

	/** Atributo opcao. */
	private Opcao opcao;

	@Autowired
	private NavegacaoControlador navegacaoControlador;

	public void acaoAbrirTela(Questionario entidade, Boolean visualizar) {

		if (AjudanteObjeto.eReferencia(entidade)) {

			entidade.setOpcoes(this.opcaoNegocio.listarOpcoesAtivas(entidade));
			
			this.setEntidade(entidade);
			
			this.novaOpcao();

		} else {

			this.criarNovaInstanciaEntidade();
		}

		this.setVisualizar(visualizar);

		this.navegacaoControlador.processarNavegacao(PaginaEnum.TABELA_QUESTIONARIO_CADASTRO);

	}
	
	public void acaoAtivarQuestionario(final Questionario questionario) {

		questionario.setAtivo(!questionario.getAtivo());
		
		this.negocio.ativarQuestionario(questionario);

		this.adicionarMensagemInformativa("Questionário "+ (questionario.getAtivo() ? "ativado" : "desativado") +" com sucesso!");
	}

	@Override
	protected void criarNovaInstanciaEntidade() {

		this.setEntidade(new Questionario());

		this.getEntidade().setDataCadastro(new Date());

		this.getEntidade().setAtivo(Boolean.FALSE);
		
		this.getEntidade().setOpcoes(new ArrayList<Opcao>());
		
		this.novaOpcao();

	}
	
	public void paraCima(Integer indice) {

		Collections.swap(this.getEntidade().getOpcoes(), indice - 1, indice);
	}

	public void paraBaixo(Integer indice) {

		Collections.swap(this.getEntidade().getOpcoes(), indice + 1, indice);
	}
	
	public void remover(final Opcao opcao) {

		this.getEntidade().getOpcoes().remove(opcao);

		if (!opcao.isNovo()) {

			opcao.setAtivo(Boolean.FALSE);

			this.getEntidade().getOpcoesRemovidas().add(opcao);
		}
	}
	
	public void adicionarOpcao() {

		this.getOpcao().setQuestionario(this.getEntidade());

		this.getEntidade().getOpcoes().add(this.getOpcao());

		this.novaOpcao();
	}
	
	private void novaOpcao() {

		this.opcao = new Opcao();

		this.opcao.setAtivo(Boolean.TRUE);

		this.opcao.setDataCadastro(new Date());

		this.opcao.setQuestionario(this.getEntidade());
	}

	@Override
	protected QuestionarioNegocio getNegocio() {

		return this.negocio;
	}

	@Override
	public TableLazyQuestionario getLista() {

		return this.lista;
	}

	public Opcao getOpcao() {
		return opcao;
	}

	public void setOpcao(Opcao opcao) {
		this.opcao = opcao;
	}
	
	@Override
	public void acaoExcluir(Questionario questionario) {

		try {
				
			boolean isPodeExcluir = true;
			
            List<Opcao> listaOpcoes = this.opcaoNegocio.listaPorAtributo("questionario", questionario, SortOrder.ASCENDING, "id");
					
			for (Opcao opcao : listaOpcoes) {
				
				List<QuestionarioAvaliacao> opcoesUtilizadas = this.questionarioAvaliacaoNegocio.listaPorAtributo("opcao", opcao,SortOrder.ASCENDING, "id");
				
				if (opcoesUtilizadas != null && opcoesUtilizadas.size() > 0) {
					
					isPodeExcluir = false;
					break;
					
				} else {
					
					this.opcaoNegocio.excluir(opcao);
				}
				
				
			}
			
			if (isPodeExcluir) {
				
				this.negocio.excluir(questionario);
				this.adicionarMensagemInformativa(this.obterMensagemSucessoAoExcluir());
				
			} else {
				
				this.adicionarMensagemInformativa("Não é possível realizar a exclusão, pois o questionário já foi utilizado em avaliações anteriores.");
			}
			

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);
		}
	}
	
	

}
