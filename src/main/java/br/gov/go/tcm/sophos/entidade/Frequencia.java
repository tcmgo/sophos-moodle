/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;

import javax.persistence.*;

@Entity
@Table(name = "frequencia")
@Banco(nome = Banco.SOPHOS)
public class Frequencia extends EntidadePadrao implements Cloneable {

	private static final long serialVersionUID = -4793757001184897819L;

	@Column(name = "encontro", nullable = false)
	private Integer encontro;

	@Column(name = "presenca")
	private Boolean presenca;

	@JoinColumn(name = "modulo_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Modulo modulo;

	@JoinColumn(name = "participante_id")
	@ManyToOne(optional = false)
	private Participante participante;
	
	@Transient
	private Boolean houveAlteracao = Boolean.FALSE;

	public Integer getEncontro() {
		return encontro;
	}

	public void setEncontro(Integer encontro) {
		this.encontro = encontro;
	}

	public Modulo getModulo() {
		return modulo;
	}

	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}

	public Participante getParticipante() {
		return participante;
	}

	public void setParticipante(Participante participante) {
		this.participante = participante;
	}

	public Boolean getPresenca() {
		return presenca;
	}

	public void setPresenca(Boolean presenca) {
		this.presenca = presenca;
	}

	public Boolean getHouveAlteracao() {
		return houveAlteracao;
	}

	public void setHouveAlteracao(Boolean houveAlteracao) {
		this.houveAlteracao = houveAlteracao;
	}

	public Frequencia clone() {
		try {
			return (Frequencia) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

}
