/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteData;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.Certificado;
import br.gov.go.tcm.sophos.lazy.base.TableLazyPadrao;
import br.gov.go.tcm.sophos.negocio.CertificadoNegocio;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
@Scope("session")
public class ValidarCertificadoControlador extends ControladorBaseCRUD<Certificado, CertificadoNegocio> implements Serializable {

	public static final String CONFIGURACAO = "Configuracao";

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private CertificadoNegocio negocio;

	private String codigoVerificacao;
	
	private Boolean existeCertificado;
	
	private String datasEmissoes;

	@PostConstruct
	public void inicializarTela(){
		
		limparCampos();
	}
	
	public void verificarCodigo(){
		
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/YYYY");
		List<Certificado> certificados = this.getNegocio().listaPorAtributo("codigoVerificacao", codigoVerificacao, SortOrder.DESCENDING, "id");
		
		if(AjudanteObjeto.eReferencia(certificados) && certificados.size() > 0) {
			
			datasEmissoes = "";
			
			this.setEntidades(certificados);
			this.setEntidade(certificados.get(0));
			
			if (certificados.size() == 1) {
				
				datasEmissoes += formatador.format(certificados.get(0).getDataEmissao()) + ".";
				
			} else {
				
				TreeSet<Date> datas = new TreeSet<Date>();
				
				for (Certificado certificado : certificados) {
					datas.add(AjudanteData.zerarHoraData(certificado.getDataEmissao()));
				}
				
				Iterator<Date> iterator = datas.iterator();
				int i = 1;
				
				while (iterator.hasNext()) {
					
					if (i == datas.size()) {
						datasEmissoes += " e " + formatador.format(iterator.next()) + ".";
					} else if (i == (datas.size()-1)) {
						datasEmissoes += formatador.format(iterator.next());
					} else {
						datasEmissoes += formatador.format(iterator.next()) + ", ";
					}
					
					i++;
				    
				}
				
			}
			
		} else {
			
			limparCampos();
			
			this.existeCertificado = false;
			
			this.adicionarMensagemDeAlerta("Não existe certificado(s) para esse código.");
		}
	}

	public void limparCampos() {
		
		this.codigoVerificacao = "";
		
		this.existeCertificado = null;
		
		this.datasEmissoes = "";
		
		this.setEntidades(new ArrayList<Certificado>());
		
		this.setEntidade(new Certificado());
	}
	
	@Override
	protected CertificadoNegocio getNegocio() {

		return this.negocio;
	}

	@Override
	public TableLazyPadrao<Certificado> getLista() {

		return null;
	}

	public String getCodigoVerificacao() {
		return codigoVerificacao;
	}

	public void setCodigoVerificacao(String codigoVerificacao) {
		this.codigoVerificacao = codigoVerificacao;
	}

	public Boolean getExisteCertificado() {
		return existeCertificado;
	}

	public void setExisteCertificado(Boolean existeCertificado) {
		this.existeCertificado = existeCertificado;
	}

	public String getDatasEmissoes() {
		return datasEmissoes;
	}

	public void setDatasEmissoes(String datasEmissoes) {
		this.datasEmissoes = datasEmissoes;
	}

}
