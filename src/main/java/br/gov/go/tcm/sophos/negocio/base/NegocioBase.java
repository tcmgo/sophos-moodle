/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio.base;

import br.gov.go.tcm.estrutura.entidade.base.Entidade;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;
import br.gov.go.tcm.sophos.entidade.dto.FiltroDTO;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.lazy.base.TableLazyDTO;
import br.gov.go.tcm.sophos.persistencia.base.DAO;
import org.hibernate.criterion.MatchMode;
import org.primefaces.model.SortOrder;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

public abstract class NegocioBase<E extends EntidadePadrao> implements Serializable {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = -8779573953799584616L;

	/** Atributo OBJETO_NULO. */
	private static final String OBJETO_NULO = "msg.objeto.nulo";

	protected abstract DAO<E> getDAO();

	@SuppressWarnings("unchecked")
	public List<E> listar() {

		return (List<E>) this.getDAO().obterTodos(this.obterTipoEntidade());
	}

	@SuppressWarnings("unchecked")
	public E obter(Long id) {

		return (E) this.getDAO().obterEntidadePorId(this.obterTipoEntidade(), id);
	}

	@SuppressWarnings("unchecked")
	protected Class<? extends Entidade> obterTipoEntidade() {

		final ParameterizedType tipo = (ParameterizedType) this.getClass().getGenericSuperclass();

		return (Class<? extends Entidade>) tipo.getActualTypeArguments()[0];
	}

	public void salvar(final E entidade) {

		if (entidade.getId() == null) {

			this.executarValidacaoAntesDeSalvar(entidade);

			this.getDAO().salvar(entidade);

		} else {

			this.atualizar(entidade);
		}

	}

	public void atualizar(final E entidade) {

		this.executarValidacaoAntesDeAtualizar(entidade);

		this.getDAO().atualizar(entidade);
	}

	public void excluir(final E entidade) {

		this.executarValidacaoAntesDeExcluir(entidade);

		this.getDAO().excluir(entidade);
	}

	protected void executarValidacaoAntesDeExcluir(final E entidade) {

		this.eNulo(entidade);
	}

	protected void executarValidacaoAntesDeAtualizar(final E entidade) {

		this.eNulo(entidade);
	}

	protected void executarValidacaoAntesDeSalvar(final E entidade) {

		this.eNulo(entidade);
	}

	protected void eNulo(final E entidade) {

		if (!AjudanteObjeto.eReferencia(entidade)) {

			throw new ValidacaoException(NegocioBase.OBJETO_NULO);
		}
	}

	public TableLazyDTO<E> listarPaginado(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters, String consulta) {

		Long quantidade = this.getDAO().obterQuantidade(filters, consulta);

		List<E> lista = this.getDAO().listar(first, pageSize, sortField, sortOrder, filters, consulta);

		return new TableLazyDTO<E>(lista, quantidade);
	}

	public List<E> listaPorAtributo(String atributo, Object valor) {

		return this.getDAO().listaPorAtributo(atributo, valor);
	}
	
	public List<E> listaPorFiltros(List<FiltroDTO> filtros, String ... alias) {

		return this.getDAO().listaPorFiltros(filtros, alias);
	}

	public List<E> listaPorAtributo(String atributo, String valor, MatchMode matchMode) {

		return this.getDAO().listaPorAtributo(atributo, valor, matchMode);
	}
	
	public List<E> listaPorAtributo(String atributo, Object valor, SortOrder order, String atributoOrdenacao) {

		return this.getDAO().listaOrdenadoPorAtributo(atributo, valor, order, atributoOrdenacao);
	}

	public E obterPorAtributo(String atributo, Object valor) {

		return this.getDAO().obterPorAtributo(atributo, valor);
	}
}
