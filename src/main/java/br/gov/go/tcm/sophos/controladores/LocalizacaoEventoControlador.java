/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.Cidade;
import br.gov.go.tcm.sophos.entidade.Endereco;
import br.gov.go.tcm.sophos.entidade.Estado;
import br.gov.go.tcm.sophos.entidade.LocalizacaoEvento;
import br.gov.go.tcm.sophos.entidade.enumeracao.PaginaEnum;
import br.gov.go.tcm.sophos.lazy.TableLazyLocalizacaoEvento;
import br.gov.go.tcm.sophos.negocio.CidadeNegocio;
import br.gov.go.tcm.sophos.negocio.EstadoNegocio;
import br.gov.go.tcm.sophos.negocio.LocalizacaoEventoNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
@Scope("session")
public class LocalizacaoEventoControlador extends ControladorBaseCRUD<LocalizacaoEvento, LocalizacaoEventoNegocio> {

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private LocalizacaoEventoNegocio negocio;
	
	@Autowired
	private TableLazyLocalizacaoEvento lista;
	
	@Autowired
	private NavegacaoControlador navegacaoControlador;
	
	@Autowired
	private CidadeNegocio cidadeNegocio;

	@Autowired
	private EstadoNegocio estadoNegocio;

	private Estado estado;
	
	private List<Estado> estados;

	private List<Cidade> cidades;

	public void listarCidades() {

		if (AjudanteObjeto.eReferencia(estado)) {

			this.cidades = this.cidadeNegocio.listaPorAtributo("estado", estado);

		}
	}
	
	public void acaoAbrirTela(LocalizacaoEvento entidade, Boolean visualizar){

		if (AjudanteObjeto.eReferencia(entidade)) {

			if(AjudanteObjeto.eReferencia(entidade.getEndereco()) && AjudanteObjeto.eReferencia(entidade.getEndereco().getCidade())){
				
				this.estado = entidade.getEndereco().getCidade().getEstado();
				
				this.listarCidades();
				
			}

			this.setEntidade(entidade);

		} else {

			this.criarNovaInstanciaEntidade();
		}
		
		this.setVisualizar(visualizar);
		
		this.navegacaoControlador.processarNavegacao(PaginaEnum.TABELA_LOCALIZACAO_EVENTO_CADASTRO);
		
	}
	
	@Override
	protected void criarNovaInstanciaEntidade() {

		this.setEntidade(new LocalizacaoEvento());

		this.getEntidade().setDataCadastro(new Date());

		this.getEntidade().setEndereco(new Endereco());
		
		this.estado = new Estado();

	}
	
	public List<Estado> getEstados() {

		if (!AjudanteObjeto.eReferencia(estados)) {

			this.estados = this.estadoNegocio.listar();
		}

		return estados;
	}
	
	@Override
	protected LocalizacaoEventoNegocio getNegocio() {

		return this.negocio;
	}
	
	public List<Cidade> getCidades() {

		return cidades;
	}
	
	public Estado getEstado() {

		return estado;
	}

	public void setEstado(Estado estado) {

		this.estado = estado;
	}
	@Override
	public TableLazyLocalizacaoEvento getLista() {

		return this.lista;
	}

}
