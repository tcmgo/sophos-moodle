/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "instrutor")
@Banco(nome = Banco.SOPHOS)
public class Instrutor extends EntidadePadrao {

	private static final long serialVersionUID = 1211518501202361659L;

	@Column(name = "instituicao")
	private String instituicao;

	@Column(name = "observacao", length = 2000)
	private String observacao;

	@Column(name = "perfil", length = 2000)
	private String perfil;

	@Column(name = "CodSecao")
	private String secaoId;

	@JoinColumn(name = "arquivo_projeto_id", referencedColumnName = "id")
	@ManyToOne
	private Anexo projeto;

	@JoinColumn(name = "arquivo_assinatura_id", referencedColumnName = "id")
	@ManyToOne
	private Anexo assinatura;

	@JoinColumn(name = "arquivo_curriculo_id", referencedColumnName = "id")
	@ManyToOne
	private Anexo curriculo;

	@JoinColumn(name = "formacao_academica_id", referencedColumnName = "id")
	@ManyToOne
	private Dominio formacaoAcademica;

	@JoinColumn(name = "nivel_escolaridade_id", referencedColumnName = "id")
	@ManyToOne
	private Dominio nivelEscolaridade;

	@JoinColumn(name = "situacao_instrutor_id", referencedColumnName = "id")
	@ManyToOne
	private Dominio situacaoInstrutor;

	@JoinColumn(name = "tipo_Instrutor_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Dominio tipoInstrutor;

	@OneToOne
	@JoinColumn(name = "endereco_id")
	private Endereco endereco;

	@OneToOne
	@JoinColumn(name = "pessoa_id")
	private Pessoa pessoa;

	@OneToMany(mappedBy = "instrutor", fetch = FetchType.LAZY)
	private List<ModuloInstrutor> moduloList;

	@Transient
	private Long idModuloInstrutor;

	public String getNome() {

		if (AjudanteObjeto.eReferencia(this.getPessoa())) {

			return this.getPessoa().getNome();

		}

		return "";
	}

	public String getInstituicao() {
		return instituicao;
	}

	public void setInstituicao(String instituicao) {
		this.instituicao = instituicao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Anexo getProjeto() {
		return projeto;
	}

	public void setProjeto(Anexo projeto) {
		this.projeto = projeto;
	}

	public Anexo getAssinatura() {
		return assinatura;
	}

	public void setAssinatura(Anexo assinatura) {
		this.assinatura = assinatura;
	}

	public Anexo getCurriculo() {
		return curriculo;
	}

	public void setCurriculo(Anexo curriculo) {
		this.curriculo = curriculo;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public Dominio getFormacaoAcademica() {
		return formacaoAcademica;
	}

	public void setFormacaoAcademica(Dominio formacaoAcademica) {
		this.formacaoAcademica = formacaoAcademica;
	}

	public Dominio getNivelEscolaridade() {
		return nivelEscolaridade;
	}

	public void setNivelEscolaridade(Dominio nivelEscolaridade) {
		this.nivelEscolaridade = nivelEscolaridade;
	}

	public Dominio getSituacaoInstrutor() {
		return situacaoInstrutor;
	}

	public void setSituacaoInstrutor(Dominio situacaoInstrutor) {
		this.situacaoInstrutor = situacaoInstrutor;
	}

	public Dominio getTipoInstrutor() {
		return tipoInstrutor;
	}

	public void setTipoInstrutor(Dominio tipoInstrutor) {
		this.tipoInstrutor = tipoInstrutor;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public String getSecaoId() {
		return secaoId;
	}

	public void setSecaoId(String secaoId) {
		this.secaoId = secaoId;
	}

	public List<ModuloInstrutor> getModuloList() {
		return moduloList;
	}

	public void setModuloList(List<ModuloInstrutor> moduloList) {
		this.moduloList = moduloList;
	}

	public Long getIdModuloInstrutor() {
		return idModuloInstrutor;
	}

	public void setIdModuloInstrutor(Long idModuloInstrutor) {
		this.idModuloInstrutor = idModuloInstrutor;
	}

}
