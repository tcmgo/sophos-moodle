/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.persistencia.impl;

import br.gov.go.tcm.sophos.entidade.Frequencia;
import br.gov.go.tcm.sophos.entidade.Modulo;
import br.gov.go.tcm.sophos.entidade.Participante;
import br.gov.go.tcm.sophos.persistencia.FrequenciaDAO;
import br.gov.go.tcm.sophos.persistencia.base.DAO;
import br.gov.go.tcm.sophos.persistencia.impl.base.DAOGenerico;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FrequenciaDAOImpl extends DAOGenerico<Frequencia> implements FrequenciaDAO {

	private static final long serialVersionUID = 294560128456827146L;

	@Autowired
	public FrequenciaDAOImpl(@Qualifier(DAO.SESSION_FACTORY_SOPHOS) final SessionFactory factory, @Qualifier(DAO.HIBERNATE_TEMPLATE_SOPHOS) final HibernateTemplate hibernateTemplate) {

		this.setHibernateTemplate(hibernateTemplate);

		this.setSessionFactory(factory);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Frequencia> listarPresencasModuloParticipante(Modulo modulo, Participante participante) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		criteria.add(Restrictions.eq("modulo", modulo));
		
		criteria.add(Restrictions.eq("participante", participante));
		
		criteria.add(Restrictions.eq("presenca", Boolean.TRUE));
		
		try {

			return criteria.list();

		} finally {

			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Frequencia> listarFrequenciasModuloParticipante(Modulo modulo, Participante participante) {
		
		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		criteria.add(Restrictions.eq("modulo", modulo));
		
		criteria.add(Restrictions.eq("participante", participante));
		
		try {

			return criteria.list();

		} finally {

			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Frequencia> listarFrequenciasModulo(Modulo modulo) {
		
		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		criteria.add(Restrictions.eq("modulo", modulo));
		
		try {

			return criteria.list();

		} finally {

			session.close();
		}
	}

	@Override
	public Long obterFrequenciaModuloParticipante(Modulo modulo, Participante participante, Integer encontro) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);
		
		Criteria criteria = session.createCriteria(this.getTipoEntidade());
		
		criteria.add(Restrictions.eq("modulo", modulo));
		
		criteria.add(Restrictions.eq("participante", participante));
		
		criteria.add(Restrictions.eq("encontro", encontro));
		
		criteria.addOrder(Order.desc("id"));
		
		criteria.setMaxResults(1);
		
		criteria.setProjection(Projections.id());
		
		try {			

			return criteria.uniqueResult()!= null ? ((Long) criteria.uniqueResult()).longValue() : null;

		} finally {

			session.close();
		}
		
	}

}
