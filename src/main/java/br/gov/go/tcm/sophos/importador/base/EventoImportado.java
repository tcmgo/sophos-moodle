/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.importador.base;

import java.math.BigDecimal;

public class EventoImportado {
	
	private BigDecimal cargaHoraria;
	
	private String conteudo;
	
	private String dataInicioPrevista;
	
	private String dataFimPrevista;
	
	private String dataInicioRealizacao;
	
	private String dataFimRealizacao;
	
	private String titulo;
	
	private Integer vagas;
	
	private String eixoTematico;
	
	private String localizacao;

	private String modalidade;
	
	private String CNPJProvedor;
	
	private String emailResponsavel;
	
	private String tipoEvento;
	
	private String publicoAlvo;
	
	public EventoImportado(BigDecimal cargaHoraria, String conteudo, String dataInicioPrevista, String dataFimPrevista,
			String dataInicioRealizacao, String dataFimRealizacao, String titulo, Integer vagas, String eixoTematico,
			String localizacao, String modalidade, String CNPJProvedor,
			String emailResponsavel, String tipoEvento, String publicoAlvo) {
		super();
		this.cargaHoraria = cargaHoraria;
		this.conteudo = conteudo;
		this.dataInicioPrevista = dataInicioPrevista;
		this.dataFimPrevista = dataFimPrevista;
		this.dataInicioRealizacao = dataInicioRealizacao;
		this.dataFimRealizacao = dataFimRealizacao;
		this.titulo = titulo;
		this.vagas = vagas;
		this.eixoTematico = eixoTematico;
		this.localizacao = localizacao;
		this.modalidade = modalidade;
		this.CNPJProvedor = CNPJProvedor;
		this.emailResponsavel = emailResponsavel;
		this.tipoEvento = tipoEvento;
		this.publicoAlvo = publicoAlvo;
	}
	

	public BigDecimal getCargaHoraria() {
		return cargaHoraria;
	}

	public void setCargaHoraria(BigDecimal cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	public String getDataInicioPrevista() {
		return dataInicioPrevista;
	}

	public void setDataInicioPrevista(String dataInicioPrevista) {
		this.dataInicioPrevista = dataInicioPrevista;
	}

	public String getDataFimPrevista() {
		return dataFimPrevista;
	}

	public void setDataFimPrevista(String dataFimPrevista) {
		this.dataFimPrevista = dataFimPrevista;
	}

	public String getDataInicioRealizacao() {
		return dataInicioRealizacao;
	}

	public void setDataInicioRealizacao(String dataInicioRealizacao) {
		this.dataInicioRealizacao = dataInicioRealizacao;
	}

	public String getDataFimRealizacao() {
		return dataFimRealizacao;
	}

	public void setDataFimRealizacao(String dataFimRealizacao) {
		this.dataFimRealizacao = dataFimRealizacao;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Integer getVagas() {
		return vagas;
	}

	public void setVagas(Integer vagas) {
		this.vagas = vagas;
	}

	public String getEixoTematico() {
		return eixoTematico;
	}

	public void setEixoTematico(String eixoTematico) {
		this.eixoTematico = eixoTematico;
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public String getModalidade() {
		return modalidade;
	}

	public void setModalidade(String modalidade) {
		this.modalidade = modalidade;
	}


	public String getCNPJProvedor() {
		return CNPJProvedor;
	}


	public void setCNPJProvedor(String cNPJProvedor) {
		CNPJProvedor = cNPJProvedor;
	}


	public String getEmailResponsavel() {
		return emailResponsavel;
	}

	public void setEmailResponsavel(String emailResponsavel) {
		this.emailResponsavel = emailResponsavel;
	}

	public String getTipoEvento() {
		return tipoEvento;
	}

	public void setTipoEvento(String tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public String getPublicoAlvo() {
		return publicoAlvo;
	}

	public void setPublicoAlvo(String publicoAlvo) {
		this.publicoAlvo = publicoAlvo;
	}

}
