/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteData;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.*;
import br.gov.go.tcm.sophos.entidade.dto.DesempenhoDTO;
import br.gov.go.tcm.sophos.entidade.dto.MeuEventoDTO;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import br.gov.go.tcm.sophos.persistencia.EventoDAO;
import org.joda.time.DateTimeComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class EventoNegocio extends NegocioBase<Evento> {

	private static final long serialVersionUID = -8426728536010364058L;

	@Autowired
	private EventoDAO dao;

	@Autowired
	private EventoPublicoAlvoNegocio eventoPublicoAlvoNegocio;

	@Autowired
	private ModuloNegocio moduloNegocio;

	@Autowired
	private AnexoNegocio anexoNegocio;
	
	@Autowired
	private CertificadoNegocio certificadoNegocio;

	@Autowired
	private InscricaoNegocio inscricaoNegocio;

	@Autowired
	private IndicacaoNegocio indicacaoNegocio;
	
	@Autowired
	private NotaNegocio notaNegocio;
	
	@Autowired
	private FrequenciaNegocio frequenciaNegocio;

	@Autowired
	private MaterialNegocio materialNegocio;
	
	@Autowired
	private GastoNegocio gastoNegocio;
	
	@Autowired
	private ModuloInstrutorNegocio moduloInstrutorNegocio;
	
	@Autowired
	private ArquivoAdministrativoNegocio arquivoAdmNegocio;

	@Override
	protected EventoDAO getDAO() {

		return this.dao;
	}

	@Override
	protected void executarValidacaoAntesDeSalvar(Evento entidade) {

		super.executarValidacaoAntesDeSalvar(entidade);

		if (!entidade.getEventoImportado()) {

			if(AjudanteObjeto.eReferencia(entidade.getModalidade())) {

				if(AjudanteObjeto.eReferencia(entidade.getModalidade().getCodigo())) {

					if(entidade.getModalidade().getCodigo().intValue() != 2) {

						entidade.setCursoMoodleId(null);
					}
				}
			}

			if(AjudanteObjeto.eReferencia(entidade.getCursoMoodleId())) {

				if(entidade.getCursoMoodleId().intValue() > 0) {

					entidade.setPermitePreInscricao(Boolean.FALSE);

					entidade.setDataInicioPreInscricao(null);

					entidade.setDataFimPreInscricao(null);

				} else {

					entidade.setCursoMoodleId(null);
				}
			}
			if (entidade.getPermitePreInscricao()) {

				if (!AjudanteObjeto.eReferencia(entidade.getDataInicioPreInscricao())
						|| !AjudanteObjeto.eReferencia(entidade.getDataFimPreInscricao())) {

					throw new ValidacaoException("Campos Obrigatórios: DATA INÍCIO PRÉ-INSCRIÇÃO, DATA FIM PRÉ-INSCRIÇÃO");
				}
				if (!AjudanteObjeto.eReferencia(entidade.getId())
						&& !(DateTimeComparator.getDateOnlyInstance().compare(entidade.getDataInicioPreInscricao(), new Date()) >= 0)) {

					throw new ValidacaoException("DATA INÍCIO PRÉ-INSCRIÇÃO tem que ser maior ou igual a data atual");
				}
				if (!(DateTimeComparator.getDateOnlyInstance().compare(entidade.getDataInicioPreInscricao(), entidade.getDataFimPreInscricao()) <= 0)) {

					throw new ValidacaoException("DATA INÍCIO PRÉ-INSCRIÇÃO tem que ser menor ou igual DATA FIM PRÉ-INSCRIÇÃO");
				}
			}
			if (!AjudanteObjeto.eReferencia(entidade.getId())
					&& !(DateTimeComparator.getDateOnlyInstance().compare(entidade.getDataInicioPrevisto(), new Date()) > 0)) {

				throw new ValidacaoException("DATA INÍCIO PREVISTO tem que ser maior que data atual");
			}
			if (!(DateTimeComparator.getDateOnlyInstance().compare(entidade.getDataInicioPrevisto(), entidade.getDataFimPrevisto()) < 0)) {

				throw new ValidacaoException("DATA INÍCIO PREVISTO tem que ser menor que DATA FIM PREVISTO");
			}
			if (AjudanteObjeto.eReferencia(entidade.getDataInicioRealizacao())
					&& AjudanteObjeto.eReferencia(entidade.getDataFimRealizacao())) {

				if (!(DateTimeComparator.getDateOnlyInstance().compare(entidade.getDataInicioRealizacao(), entidade.getDataFimRealizacao()) <= 0)) {

					throw new ValidacaoException("DATA INÍCIO REALIZAÇÃO tem que ser menor que DATA FIM REALIZAÇÃO");
				}
			}
			if(entidade.getPermitePreInscricao()) {

				if(AjudanteObjeto.eReferencia(entidade.getDataInicioRealizacao())) {

					if(!(DateTimeComparator.getDateOnlyInstance().compare(entidade.getDataFimPreInscricao(), entidade.getDataInicioRealizacao()) <= 0)) {

						throw new ValidacaoException("DATA FIM PRÉ-INSCRIÇÃO tem que ser menor ou igual que DATA INÍCIO REALIZAÇÃO");
					}
				}
				if(AjudanteObjeto.eReferencia(entidade.getDataInicioPrevisto())) {

					if(!(DateTimeComparator.getDateOnlyInstance().compare(entidade.getDataFimPreInscricao(), entidade.getDataInicioPrevisto()) <= 0)) {

						throw new ValidacaoException("DATA FIM PRÉ-INSCRIÇÃO tem que ser menor ou igual que DATA INÍCIO PREVISTO");
					}
				}
			}
		}
	}

	public boolean verificaPermissaoInscricao(Evento evento) {

		if(AjudanteObjeto.eReferencia(evento.getPermitePreInscricao())) {

			Date dataAtual = AjudanteData.DDMMYYYY();

			if(evento.getPermitePreInscricao() == Boolean.TRUE) {

				if(AjudanteObjeto.eReferencia(evento.getDataInicioPreInscricao())
						&& AjudanteObjeto.eReferencia(evento.getDataFimPreInscricao())) {

					if(
							(DateTimeComparator.getDateOnlyInstance().compare(evento.getDataInicioPreInscricao(), dataAtual) <= 0)
									&& (DateTimeComparator.getDateOnlyInstance().compare(dataAtual, evento.getDataFimPreInscricao()) <= 0)
					) {

						return true;
					}
				}
				if(AjudanteObjeto.eReferencia(evento.getDataInicioRealizacao())) {

					if((DateTimeComparator.getDateOnlyInstance().compare(evento.getDataInicioRealizacao(), dataAtual)) == 0) {

						return true;
					}
				}
			} else {

				if(AjudanteObjeto.eReferencia(evento.getDataInicioRealizacao())
						&& AjudanteObjeto.eReferencia(evento.getDataFimRealizacao())) {

					if(DateTimeComparator.getDateOnlyInstance().compare(
							dataAtual, evento.getDataFimRealizacao()) <= 0) {

						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public void salvar(Evento entidade) {

		this.executarValidacaoAntesDeSalvar(entidade);

		Evento eventoOld = null;

		if (entidade.getId() != null) {

			eventoOld = this.obter(entidade.getId());

			this.verificaMudancasAutalizacao(entidade, eventoOld);
		}

		if (AjudanteObjeto.eReferencia(entidade.getImagem())) {

			this.anexoNegocio.salvar(entidade.getImagem());
		}

		List<EventoPublicoAlvo> publicoAlvoList = entidade.getPublicoAlvoList();

		entidade.setPublicoAlvoList(null);

		super.salvar(entidade);

		if (eventoOld != null) {

			this.verificaMudancaAnexo(entidade, eventoOld);

		}

		List<Modulo> modulos = moduloNegocio.listarOrdenadoPorData(entidade);

		if (entidade.getModuloUnico() && modulos.size() == 0) {

			this.moduloNegocio.inserirModuloUnico(entidade);
		}

		if (AjudanteObjeto.eReferencia(publicoAlvoList)) {

			for (EventoPublicoAlvo publicoAlvo : publicoAlvoList) {

				publicoAlvo.setEvento(entidade);

				this.eventoPublicoAlvoNegocio.salvar(publicoAlvo);
			}
		}
	}

	private void verificaMudancaAnexo(Evento entidade, Evento eventoOld) {

		if (AjudanteObjeto.eReferencia(eventoOld.getImagem()) && (!AjudanteObjeto.eReferencia(entidade.getImagem())
				|| !AjudanteObjeto.eReferencia(entidade.getImagem().getArquivoGED()))) {

			this.anexoNegocio.excluir(eventoOld.getImagem());
		}
	}

	private void verificaMudancasAutalizacao(Evento entidade, Evento eventoOld) {

		if (AjudanteObjeto.eReferencia(entidade.getId())) {

			List<EventoPublicoAlvo> eventoPublicoAlvoListOld = this.obterPublicoAlvo(eventoOld);

			for (EventoPublicoAlvo publicoAlvoOld : eventoPublicoAlvoListOld) {

				this.eventoPublicoAlvoNegocio.excluir(publicoAlvoOld);
			}
		}
	}

	public List<EventoPublicoAlvo> obterPublicoAlvo(Evento entidade) {

		return this.eventoPublicoAlvoNegocio.listaPorAtributo("evento", entidade);
	}

	public MeuEventoDTO montarListaMeusEventos(Inscricao inscricao, Participante participante) {
		
		DesempenhoDTO desempenho = certificadoNegocio.calcularDesempenho(inscricao.getEvento(), participante);
		MeuEventoDTO meuEventoDTO = new MeuEventoDTO(inscricao.getEvento(), desempenho.getAprovado(), desempenho.getAvaliado(), inscricao);
		
		return meuEventoDTO;
	}

	public void disponibilizarCertificado(Evento evento) {

		Evento entidade = this.obter(evento.getId());

		if (!AjudanteObjeto.eReferencia(evento.getDataFimRealizacao()) || !AjudanteData.dataMenorQueReferencia(evento.getDataFimRealizacao(), new Date())) {

			throw new ValidacaoException("A disponibilização do certificado pode ser efetuada somente após o fim da realização do evento.");
		}

		entidade.setPermiteCertificado(Boolean.TRUE);

		super.salvar(entidade);
	}
	
	public void cancelarCertificado(Evento evento) {
		
		Evento entidade = this.obter(evento.getId());
		
		entidade.setPermiteCertificado(Boolean.FALSE);
		
		super.salvar(entidade);
	}

	public List<Evento> listarPorTipoParticipante(Usuario usuarioLogado) {

		if (AjudanteObjeto.eReferencia(usuarioLogado) && AjudanteObjeto.eReferencia(usuarioLogado.getParticipante())) {

			return this.getDAO().listarEventosParaVisualizacao(usuarioLogado.getParticipante().getTipo());

		} else {

			return this.getDAO().listarEventosParaVisualizacao(null);
		}
	}

	public List<Evento> listarEventoComCertificadosDisponiveis() {

		return this.getDAO().listarEventoComCertificadosDisponiveis();
	}

	public List<Evento> listarEventoComInscricoesAbertas() {

		return this.getDAO().listarEventoComInscricoesAbertas();
	}

	public void exibirOcultarNaHome(Evento evento) {

		Evento entidade = this.obter(evento.getId());

		entidade.setMostrarNaHome(!evento.getMostrarNaHome());

		super.salvar(entidade);
	}
	
	@Override
	public void excluir(Evento evento) {
		
		Integer qtNotasLancadas = this.obterNumeroNotasLancadas(evento);
		Integer qtFrequenciasLancadas = this.obterNumeroFrequenciasLancadas(evento);
		
		if (qtNotasLancadas > 0 || qtFrequenciasLancadas > 0) {
			
			throw new ValidacaoException("Não é possível remover o evento " + evento.getTitulo() + ", pois há " + qtNotasLancadas + " nota(s) e " + qtFrequenciasLancadas + " frequência(s) já lançadas.");
			
		} else {
			
			for (Inscricao inscricao : inscricaoNegocio.listaPorAtributo("evento", evento)) {
				inscricaoNegocio.excluir(inscricao);
			}

			for (Indicacao indicacao : indicacaoNegocio.listaPorAtributo("evento", evento)) {
				indicacaoNegocio.excluir(indicacao);
			}
			
			for (Certificado certificado : certificadoNegocio.listaPorAtributo("evento", evento)) {
				certificadoNegocio.excluir(certificado);
			}
			
			for (Modulo modulo : moduloNegocio.listaPorAtributo("evento", evento)) {
				
				for (ModuloInstrutor moduloInstrutor : moduloInstrutorNegocio.listaPorAtributo("modulo", modulo)) {
					moduloInstrutorNegocio.excluir(moduloInstrutor);
				}
				
				for (Material material : materialNegocio.listaPorAtributo("modulo", modulo)) {					
					materialNegocio.excluir(material);
				}
				
				moduloNegocio.excluir(modulo);
			}
			
			for (EventoPublicoAlvo publicoAlvo : eventoPublicoAlvoNegocio.listaPorAtributo("evento", evento)) {
				eventoPublicoAlvoNegocio.excluir(publicoAlvo);
			}
			
			for (Gasto gasto : gastoNegocio.listaPorAtributo("evento", evento)) {
				gastoNegocio.excluir(gasto);
			}
			
			for (ArquivoAdministrativo arquivoAdm : arquivoAdmNegocio.listaPorAtributo("evento", evento)) {
				arquivoAdmNegocio.excluir(arquivoAdm);
			}

			if (AjudanteObjeto.eReferencia(evento.getImagem())) {
				anexoNegocio.excluir(evento.getImagem());
			}
			
			if (AjudanteObjeto.eReferencia(evento.getImagemCertificadoFrente())) {
				anexoNegocio.excluir(evento.getImagemCertificadoFrente());
			}
			
			if (AjudanteObjeto.eReferencia(evento.getImagemCertificadoVerso())) {
				anexoNegocio.excluir(evento.getImagemCertificadoVerso());
			}
			super.excluir(evento);
		}
	}
	
	public Integer obterNumeroNotasLancadas(Evento evento) {
		
		Integer qtNotasLancadas = 0;
		
		if (AjudanteObjeto.eReferencia(evento)) {
			
			List<Modulo> modulos = moduloNegocio.listaPorAtributo("evento", evento);
			
			List<Nota> notas = new ArrayList<Nota>();
			for (Modulo modulo : modulos) {
				notas = notaNegocio.listaPorAtributo("modulo", modulo);
			}
			
			qtNotasLancadas = notas.size();
		}
		 
		return qtNotasLancadas;
	}
	
	public Integer obterNumeroFrequenciasLancadas(Evento evento) {
		
		Integer qtFrequenciaLancadas = 0;
		
		if (AjudanteObjeto.eReferencia(evento)) {
			
			List<Modulo> modulos = moduloNegocio.listaPorAtributo("evento", evento);
			
			List<Frequencia> frequencias = new ArrayList<Frequencia>();
			for (Modulo modulo : modulos) {
				frequencias = frequenciaNegocio.listaPorAtributo("modulo", modulo);
			}
			
			qtFrequenciaLancadas = frequencias.size();
		}
		 
		return qtFrequenciaLancadas;
	}
}
