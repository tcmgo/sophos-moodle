/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "participante")
@Banco(nome = Banco.SOPHOS)
public class Participante extends EntidadePadrao implements Comparable<Participante> {

	private static final long serialVersionUID = -8257364103900392541L;

	@ManyToOne(optional = false)
	@JoinColumn(name = "pessoa_id", referencedColumnName = "id")
	private Pessoa pessoa;

	@OneToOne
	@JoinColumn(name = "endereco_id")
	private Endereco endereco;

	@Column(name = "codOrgao")
	private Integer codigoOrgao;
	
	@Column(name = "codMunicipio")
	private Integer codigoMunicipio;

	@Column(name = "lotacao")
	private String lotacao;

	@Column(name = "matricula", length = 20)
	private String matricula;

	@Column(name = "cargo")
	private String cargo;

	@Column(name = "profissao")
	private String profissao;

	@Column(name = "observacao", length = 2000)
	private String observacao;

	@JoinColumn(name = "formacao_academica_id", referencedColumnName = "id")
	@ManyToOne
	private Dominio formacaoAcademica;

	@JoinColumn(name = "escolaridade_id", referencedColumnName = "id")
	@ManyToOne
	private Dominio escolaridade;
	
	@JoinColumn(name = "mandato_id", referencedColumnName = "id")
	@ManyToOne
	private Dominio mandato;

	@JoinColumn(name = "publico_alvo_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Dominio tipo;
	
	@Column(name = "data_admissao")
	@Temporal(TemporalType.DATE)
	private Date dataAdmissao;
	
	@Column(name = "instituicao")
	private String instituicao;

	@OneToMany(mappedBy = "participante", fetch = FetchType.LAZY)
	private List<Frequencia> frequenciaList;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "participante")
	private List<Inscricao> inscricaoList;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "participante")
	private List<Nota> notaList;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "participante")
	private List<Avaliacao> avaliacaoList;
	
	@Column(name = "pne")
	private Boolean pne = false;
	
	@JoinColumn(name = "tipo_necessidade_especial_id", referencedColumnName = "id")
	@ManyToOne
	private Dominio tipoNecessidadeEspecial;
	
	@Column(name = "outra_necessidade_especial")
	private String outraNecessidadeEspecial;
	
	@Transient
	private String nomeUsuario;

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Integer getCodigoOrgao() {
		return codigoOrgao;
	}

	public void setCodigoOrgao(Integer codigoOrgao) {
		this.codigoOrgao = codigoOrgao;
	}

	public Integer getCodigoMunicipio() {
		return codigoMunicipio;
	}

	public void setCodigoMunicipio(Integer codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}

	public String getLotacao() {
		return lotacao == null ? null : lotacao.toUpperCase();
	}

	public void setLotacao(String lotacao) {
		this.lotacao = lotacao;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getCargo() {
		return cargo == null ? null : cargo.toUpperCase();
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getProfissao() {
		return profissao == null ? null : profissao.toUpperCase();
	}

	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Dominio getFormacaoAcademica() {
		return formacaoAcademica;
	}

	public void setFormacaoAcademica(Dominio formacaoAcademica) {
		this.formacaoAcademica = formacaoAcademica;
	}

	public Dominio getEscolaridade() {
		return escolaridade;
	}

	public void setEscolaridade(Dominio escolaridade) {
		this.escolaridade = escolaridade;
	}

	public Dominio getMandato() {
		return mandato;
	}

	public void setMandato(Dominio mandato) {
		this.mandato = mandato;
	}

	public Dominio getTipo() {
		return tipo;
	}

	public void setTipo(Dominio tipo) {
		this.tipo = tipo;
	}

	public List<Frequencia> getFrequenciaList() {
		return frequenciaList;
	}

	public void setFrequenciaList(List<Frequencia> frequenciaList) {
		this.frequenciaList = frequenciaList;
	}

	public List<Inscricao> getInscricaoList() {
		return inscricaoList;
	}

	public void setInscricaoList(List<Inscricao> inscricaoList) {
		this.inscricaoList = inscricaoList;
	}

	public List<Nota> getNotaList() {
		return notaList;
	}

	public void setNotaList(List<Nota> notaList) {
		this.notaList = notaList;
	}

	public List<Avaliacao> getAvaliacaoList() {
		return avaliacaoList;
	}

	public void setAvaliacaoList(List<Avaliacao> avaliacaoList) {
		this.avaliacaoList = avaliacaoList;
	}

	public Date getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(Date dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}
	
	public String getInstituicao() {
		return instituicao;
	}

	public void setInstituicao(String instituicao) {
		this.instituicao = instituicao;
	}

	@Override
	public int compareTo(Participante participante) {
		return this.pessoa.getNome().compareTo(participante.getPessoa().getNome());
	}

	public Boolean getPne() {
		return pne;
	}

	public void setPne(Boolean pne) {
		this.pne = pne;
	}

	public Dominio getTipoNecessidadeEspecial() {
		return tipoNecessidadeEspecial;
	}

	public void setTipoNecessidadeEspecial(Dominio tipoNecessidadeEspecial) {
		this.tipoNecessidadeEspecial = tipoNecessidadeEspecial;
	}

	public String getOutraNecessidadeEspecial() {
		return outraNecessidadeEspecial;
	}

	public void setOutraNecessidadeEspecial(String outraNecessidadeEspecial) {
		this.outraNecessidadeEspecial = outraNecessidadeEspecial;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

}
