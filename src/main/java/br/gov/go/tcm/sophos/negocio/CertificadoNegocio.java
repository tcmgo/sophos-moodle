/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 *
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 *
 * Este cabeçalho deve ser mantido.
 *
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteData;
import br.gov.go.tcm.estrutura.entidade.transiente.Arquivo;
import br.gov.go.tcm.estrutura.negocio.ajudante.AjudanteDeArquivos;
import br.gov.go.tcm.estrutura.relatorio.GeradorRelatorio;
import br.gov.go.tcm.estrutura.relatorio.fonteDeDados.AjudanteParametrosRelatorio;
import br.gov.go.tcm.estrutura.relatorio.fonteDeDados.FonteDadosRelatorioJavaBean;
import br.gov.go.tcm.sophos.ajudante.AjudanteIdentificacao;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.ajudante.AjudanteRelatorio;
import br.gov.go.tcm.sophos.entidade.*;
import br.gov.go.tcm.sophos.entidade.dto.CertificadoDTO;
import br.gov.go.tcm.sophos.entidade.dto.DesempenhoDTO;
import br.gov.go.tcm.sophos.entidade.dto.ModuloDesempenhoDTO;
import br.gov.go.tcm.sophos.entidade.dto.ModuloInstrutorCertificadoDTO;
import br.gov.go.tcm.sophos.entidade.enumeracao.ConfiguracaoEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoArquivo;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoDominioEnum;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import br.gov.go.tcm.sophos.persistencia.CertificadoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CertificadoNegocio extends NegocioBase<Certificado> {

	private static final String IMG_VERSO = "IMG_VERSO";

	private static final String IMG_FRENTE = "IMG_FRENTE";

	private static final long serialVersionUID = -8426728536010364058L;

	@Autowired
	private CertificadoDAO dao;

	@Autowired
	private AjudanteDeArquivos ajudanteArquivos;

	@Autowired
	private ModuloNegocio moduloNegocio;

	@Autowired
	private DominioNegocio dominioNegocio;

	@Autowired
	private ModuloInstrutorNegocio moduloInstrutorNegocio;

	@Autowired
	private AnexoNegocio anexoNegocio;

	@Autowired
	private String imgPadraoCertificadoVerso;

	@Autowired
	private String imgPadraoCertificadoFrente;

	@Autowired
	private NotaNegocio notaNegocio;

	@Autowired
	private FrequenciaNegocio frequenciaNegocio;

	@Autowired
	private ServletContext context;

	@Override
	protected CertificadoDAO getDAO() {

		return this.dao;
	}

	public Boolean isJaTemCertificadoParticipante(Evento evento, Participante participante) throws IOException {

		Certificado certificadoJaSalvo = this.getDAO().obterCertificado(evento, participante);

		if (AjudanteObjeto.eReferencia(certificadoJaSalvo)) {
			return true;
		} else {
			return false;
		}

	}

	public Boolean isJaTemCertificadoInstrutor(Evento evento, Instrutor instrutor) throws IOException {

		Certificado certificadoJaSalvo = this.getDAO().obterCertificado(evento, instrutor);

		if (AjudanteObjeto.eReferencia(certificadoJaSalvo)) {
			return true;
		} else {
			return false;
		}

	}

	public Certificado obterCertificado(Evento evento, Participante participante, Boolean isGerarNovo) throws IOException {

		Certificado certificadoJaSalvo = this.getDAO().obterCertificado(evento, participante);

		if (isGerarNovo) {

			Certificado certificadoASalvar = new Certificado();

			certificadoASalvar.setDataCadastro(new Date());

			certificadoASalvar.setDataEmissao(new Date());

			certificadoASalvar.setEvento(evento);

			certificadoASalvar.setParticipante(participante);

			if (AjudanteObjeto.eReferencia(certificadoJaSalvo)) {

				certificadoASalvar.setCodigoVerificacao(certificadoJaSalvo.getCodigoVerificacao());

			} else {

				certificadoASalvar.setCodigoVerificacao(AjudanteIdentificacao.gerarCodigoValidacao());

			}

			Anexo anexoASalvar = criarAnexoParticipante(certificadoASalvar);

			if (AjudanteObjeto.eReferencia(certificadoASalvar) && AjudanteObjeto.eReferencia(anexoASalvar)) {
				certificadoASalvar.setArquivo(anexoASalvar);
			}

			this.salvar(certificadoASalvar);

			return certificadoASalvar;

		} else {

			return certificadoJaSalvo;
		}


	}

	public Certificado obterCertificado(Evento evento, Instrutor instrutor, Boolean isGerarNovo) throws IOException {

		Certificado certificadoJaSalvo = this.getDAO().obterCertificado(evento, instrutor);

		if (isGerarNovo) {

			Certificado certificadoASalvar = new Certificado();

			certificadoASalvar.setDataCadastro(new Date());

			certificadoASalvar.setDataEmissao(new Date());

			certificadoASalvar.setEvento(evento);

			certificadoASalvar.setInstrutor(instrutor);

			if (AjudanteObjeto.eReferencia(certificadoJaSalvo)) {

				certificadoASalvar.setCodigoVerificacao(certificadoJaSalvo.getCodigoVerificacao());

			} else {

				certificadoASalvar.setCodigoVerificacao(AjudanteIdentificacao.gerarCodigoValidacao());

			}

			Anexo anexoASalvar = criarAnexoInstrutor(certificadoASalvar);

			if (AjudanteObjeto.eReferencia(certificadoASalvar) && AjudanteObjeto.eReferencia(anexoASalvar)) {
				certificadoASalvar.setArquivo(anexoASalvar);
			}

			this.salvar(certificadoASalvar);

			return certificadoASalvar;

		} else {

			return certificadoJaSalvo;
		}


	}

	private Anexo criarAnexoParticipante(Certificado certificado) throws IOException {

		Anexo anexo = new Anexo();

		anexo.setDataCadastro(new Date());

		anexo.setDescricao("certificado_" + certificado.getParticipante().getPessoa().getNome());

		anexo.setTipoArquivo(TipoArquivo.PDF);

		Arquivo arquivo = new Arquivo();

		arquivo.setStream(this.gerarCertificadoIreportParticipante(certificado));

		arquivo.setNome(anexo.getDescricao());

		arquivo.setSufixo("." + anexo.getTipoArquivo().getExtensao());

		String idArquivo = this.ajudanteArquivos.salvar(arquivo);

		anexo.setArquivoGED(idArquivo);

		this.anexoNegocio.salvarSimples(anexo);

		return anexo;
	}

	private Anexo criarAnexoInstrutor(Certificado certificado) throws IOException {

		Anexo anexo = new Anexo();

		anexo.setDataCadastro(new Date());

		anexo.setDescricao("certificado_" + certificado.getInstrutor().getPessoa().getNome());

		anexo.setTipoArquivo(TipoArquivo.PDF);

		Arquivo arquivo = new Arquivo();

		arquivo.setStream(this.gerarCertificadoIreportInstrutor(certificado));

		arquivo.setNome(anexo.getDescricao());

		arquivo.setSufixo("." + anexo.getTipoArquivo().getExtensao());

		String idArquivo = this.ajudanteArquivos.salvar(arquivo);

		anexo.setArquivoGED(idArquivo);

		this.anexoNegocio.salvarSimples(anexo);

		return anexo;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private InputStream gerarCertificadoIreportParticipante(Certificado certificado) throws IOException {

		List<CertificadoDTO> certificadoList = new ArrayList<CertificadoDTO>();

		CertificadoDTO certificadoDTO = montarCertificadoParticipante(certificado);

		montarListaModulosInstrutores(certificado, certificadoDTO);

		certificadoList.add(certificadoDTO);

		FonteDadosRelatorioJavaBean fonteDados = new FonteDadosRelatorioJavaBean((List) certificadoList);

		AjudanteParametrosRelatorio ajudanteParametro = new AjudanteParametrosRelatorio();

		ajudanteParametro.adicionarParametroRelatorio("SUBREPORT_DIR", getPath() + File.separator);

		defineImgCertificado(ajudanteParametro, certificado.getEvento());

		String tituloEvento = certificado.getEvento().getTitulo();
		byte[] relatorio;

		if (tituloEvento.equals("PROGRAMA DE ACOLHIMENTO E INTEGRAÇÃO - PAI ")) {
			//foi criado um certificado específico para o PAI - Programa de Acolhimento e Integração
			relatorio = AjudanteRelatorio.gerarRelatorio(this.getPath() + "/certificado_PAI.jasper",
					ajudanteParametro.getParametrosRelatorio(), fonteDados, GeradorRelatorio.SAIDA_PDF, true);
		} else {

			relatorio = AjudanteRelatorio.gerarRelatorio(obterNomeRelatorioParticipante(),
					ajudanteParametro.getParametrosRelatorio(), fonteDados, GeradorRelatorio.SAIDA_PDF, true);

		}

		BufferedInputStream bufferedInputStream = new BufferedInputStream(new ByteArrayInputStream(relatorio));

		return bufferedInputStream;

	}

	private InputStream gerarCertificadoIreportInstrutor(Certificado certificado) throws IOException {

		List<CertificadoDTO> certificadoList = new ArrayList<CertificadoDTO>();

		CertificadoDTO certificadoDTO = montarCertificadoInstrutor(certificado);

		montarListaModulosInstrutores(certificado, certificadoDTO);

		certificadoList.add(certificadoDTO);

		FonteDadosRelatorioJavaBean fonteDados = new FonteDadosRelatorioJavaBean((List) certificadoList);

		AjudanteParametrosRelatorio ajudanteParametro = new AjudanteParametrosRelatorio();

		ajudanteParametro.adicionarParametroRelatorio("SUBREPORT_DIR", getPath() + File.separator);

		defineImgCertificado(ajudanteParametro, certificado.getEvento());

		byte[] relatorio;

		relatorio = AjudanteRelatorio.gerarRelatorio(obterNomeRelatorioInstrutor(),
				ajudanteParametro.getParametrosRelatorio(), fonteDados, GeradorRelatorio.SAIDA_PDF, true);

		BufferedInputStream bufferedInputStream = new BufferedInputStream(new ByteArrayInputStream(relatorio));

		return bufferedInputStream;

	}

	private String obterNomeRelatorioParticipante() {

		return this.getPath() + "/certificado.jasper";
	}

	private String obterNomeRelatorioInstrutor() {

		return this.getPath() + "/certificado_instrutor.jasper";
	}

	private String getPath() {

		return context.getRealPath("/WEB-INF/relatorios/");
	}

	private void defineImgCertificado(AjudanteParametrosRelatorio ajudanteParametro, Evento evento) {

		if (AjudanteObjeto.eReferencia(evento) &&
				AjudanteObjeto.eReferencia(evento.getImagemCertificadoFrente()) &&
				AjudanteObjeto.eReferencia(evento.getImagemCertificadoVerso())) {

			adicionarImgCertificadoNoRelatorio(ajudanteParametro, evento.getImagemCertificadoFrente().getArquivoGED(), IMG_FRENTE,
					imgPadraoCertificadoFrente);

			adicionarImgCertificadoNoRelatorio(ajudanteParametro, evento.getImagemCertificadoVerso().getArquivoGED(), IMG_VERSO,
					imgPadraoCertificadoVerso);

		} else {

			Dominio configuracaoImgCertificadoFrente = this.dominioNegocio.obterDominio(TipoDominioEnum.Configuracao,
					ConfiguracaoEnum.IMG_CERTIFICADO_FRENTE.getCodigo());

			Dominio configuracaoImgCertificadoVerso = this.dominioNegocio.obterDominio(TipoDominioEnum.Configuracao,
					ConfiguracaoEnum.IMG_CERTIFICADO_VERSO.getCodigo());

			adicionarImgCertificadoNoRelatorio(ajudanteParametro, configuracaoImgCertificadoFrente, IMG_FRENTE,
					imgPadraoCertificadoFrente);

			adicionarImgCertificadoNoRelatorio(ajudanteParametro, configuracaoImgCertificadoVerso, IMG_VERSO,
					imgPadraoCertificadoVerso);

		}

	}

	private void adicionarImgCertificadoNoRelatorio(AjudanteParametrosRelatorio ajudanteParametro,
													Dominio configuracaoImgCertificado, String chave, String imgPadraoCertificado) {

		if (AjudanteObjeto.eReferencia(configuracaoImgCertificado)) {

			try {

				ajudanteParametro.adicionarParametroRelatorio(chave,
						AjudanteDeArquivos.obterCaminhoArquivoPorId(configuracaoImgCertificado.getDescricao()));

			} catch (Exception ex) {

				ajudanteParametro.adicionarParametroRelatorio(chave, imgPadraoCertificado);

			}

		} else {

			ajudanteParametro.adicionarParametroRelatorio(chave, imgPadraoCertificado);
		}
	}

	private void adicionarImgCertificadoNoRelatorio(AjudanteParametrosRelatorio ajudanteParametro,
													String descricaoImagemGED, String chave, String imgPadraoCertificado) {

		if (!AjudanteObjeto.eVazia(descricaoImagemGED)) {

			try {

				ajudanteParametro.adicionarParametroRelatorio(chave,
						AjudanteDeArquivos.obterCaminhoArquivoPorId(descricaoImagemGED));

			} catch (Exception ex) {

				ajudanteParametro.adicionarParametroRelatorio(chave, imgPadraoCertificado);

			}

		} else {

			ajudanteParametro.adicionarParametroRelatorio(chave, imgPadraoCertificado);
		}

	}

	private void montarListaModulosInstrutores(Certificado certificado, CertificadoDTO certificadoDTO) {

		List<Modulo> moduloList = this.moduloNegocio.listaPorAtributo("evento", certificado.getEvento());

		certificadoDTO.setDetalhado(!(moduloList.size() > 1));

		certificadoDTO.setModulo(moduloList.get(0).getTitulo());

		for (Modulo modulo : moduloList) {

			List<ModuloInstrutor> moduloInstrutorList = this.moduloInstrutorNegocio.listaPorAtributo("modulo", modulo);

			if (AjudanteObjeto.eReferencia(moduloInstrutorList) && moduloInstrutorList.size() > 0) {

				for (ModuloInstrutor moduloInstrutor : moduloInstrutorList) {

					if (certificadoDTO.getDetalhado()) {

						ModuloInstrutorCertificadoDTO moduloInstrutorCertificadoDTO = new ModuloInstrutorCertificadoDTO(
								moduloInstrutor.getInstrutor().getNome(), modulo.getTitulo(),
								Integer.toString(modulo.getCargaHoraria().intValue()));

						moduloInstrutorCertificadoDTO.setCurriculoInstrutor(moduloInstrutor.getInstrutor().getPerfil());

						certificadoDTO.getModuloInstrutorCertificadoList().add(moduloInstrutorCertificadoDTO);

					} else {

						certificadoDTO.getModuloInstrutorCertificadoList()
								.add(new ModuloInstrutorCertificadoDTO(moduloInstrutor.getInstrutor().getNome(),
										modulo.getTitulo(), Integer.toString(modulo.getCargaHoraria().intValue())));
					}
				}

			} else {

				certificadoDTO.getModuloInstrutorCertificadoList().add(new ModuloInstrutorCertificadoDTO("",
						modulo.getTitulo(), Integer.toString(modulo.getCargaHoraria().intValue())));
			}

		}
	}

	private CertificadoDTO montarCertificadoParticipante(Certificado certificado) {

		CertificadoDTO certificadoDTO = new CertificadoDTO();
		certificadoDTO.setEvento(certificado.getEvento().getTitulo());
		certificadoDTO.setParticipante(certificado.getParticipante().getPessoa().getNome());

		String dataEvento;
		if (certificado.getEvento().getDataInicioRealizacao().equals(certificado.getEvento().getDataFimRealizacao())) {

			dataEvento = AjudanteData.converterDataParaString(certificado.getEvento().getDataInicioRealizacao(),
					"' em' dd 'de' MMMMM 'de' yyyy");

		} else {

			String strMesAnoInicio = AjudanteData
					.converterDataParaString(certificado.getEvento().getDataInicioRealizacao(), "'de' MMMMM 'de' yyyy");
			String strMesAnoFim = AjudanteData.converterDataParaString(certificado.getEvento().getDataFimRealizacao(),
					"'de' MMMMM 'de' yyyy");

			if (strMesAnoInicio.equals(strMesAnoFim)) {

				dataEvento = AjudanteData.converterDataParaString(certificado.getEvento().getDataInicioRealizacao(),
						"', no período de' dd") + " a "
						+ AjudanteData.converterDataParaString(certificado.getEvento().getDataFimRealizacao(), "dd")
						+ " " + strMesAnoInicio;

			} else {

				dataEvento = AjudanteData.converterDataParaString(certificado.getEvento().getDataInicioRealizacao(),
						"', no período de' dd 'de' MMMMM 'de' yyyy") + " a "
						+ AjudanteData.converterDataParaString(certificado.getEvento().getDataFimRealizacao(),
						"dd 'de' MMMMM 'de' yyyy");

			}

		}

		certificadoDTO.setDataEvento(dataEvento);
		certificadoDTO.setCargaHoraria(Integer.toString(certificado.getEvento().getCargaHoraria().intValue()));
		certificadoDTO.setLocalEvento(certificado.getEvento().getLocalizacao().getDescricao().trim());
		certificadoDTO.setCodigoVerificacao(certificado.getCodigoVerificacao());
		certificadoDTO.setConteudoEvento(certificado.getEvento().getConteudo());
		certificadoDTO.setTipoEvento(certificado.getEvento().getTipoEvento().getDescricao().toLowerCase());
		certificadoDTO.setLinkValidacaoCertificado("http://www.tcm.go.gov.br/escolatcm/validacao-do-certificado/");

		return certificadoDTO;
	}

	private CertificadoDTO montarCertificadoInstrutor(Certificado certificado) {

		CertificadoDTO certificadoDTO = new CertificadoDTO();
		certificadoDTO.setEvento(certificado.getEvento().getTitulo());
		certificadoDTO.setParticipante(certificado.getInstrutor().getPessoa().getNome());

		String dataEvento;
		if (certificado.getEvento().getDataInicioRealizacao().equals(certificado.getEvento().getDataFimRealizacao())) {

			dataEvento = AjudanteData.converterDataParaString(certificado.getEvento().getDataInicioRealizacao(),
					"' em' dd 'de' MMMMM 'de' yyyy");

		} else {

			String strMesAnoInicio = AjudanteData
					.converterDataParaString(certificado.getEvento().getDataInicioRealizacao(), "'de' MMMMM 'de' yyyy");
			String strMesAnoFim = AjudanteData.converterDataParaString(certificado.getEvento().getDataFimRealizacao(),
					"'de' MMMMM 'de' yyyy");

			if (strMesAnoInicio.equals(strMesAnoFim)) {

				dataEvento = AjudanteData.converterDataParaString(certificado.getEvento().getDataInicioRealizacao(),
						"', no período de' dd") + " a "
						+ AjudanteData.converterDataParaString(certificado.getEvento().getDataFimRealizacao(), "dd")
						+ " " + strMesAnoInicio;

			} else {

				dataEvento = AjudanteData.converterDataParaString(certificado.getEvento().getDataInicioRealizacao(),
						"', no período de' dd 'de' MMMMM 'de' yyyy") + " a "
						+ AjudanteData.converterDataParaString(certificado.getEvento().getDataFimRealizacao(),
						"dd 'de' MMMMM 'de' yyyy");

			}

		}

		certificadoDTO.setDataEvento(dataEvento);
		certificadoDTO.setCargaHoraria(Integer.toString(cargaHorariaInstrutor(certificado)));
		certificadoDTO.setLocalEvento(certificado.getEvento().getLocalizacao().getDescricao().trim());
		certificadoDTO.setCodigoVerificacao(certificado.getCodigoVerificacao());
		certificadoDTO.setConteudoEvento(certificado.getEvento().getConteudo());
		certificadoDTO.setTipoEvento(certificado.getEvento().getTipoEvento().getDescricao().toLowerCase());
		certificadoDTO.setLinkValidacaoCertificado("http://www.tcm.go.gov.br/escolatcm/validacao-do-certificado/");

		return certificadoDTO;
	}

	private int cargaHorariaInstrutor (Certificado certificado){
		List<Modulo> moduloList = this.moduloNegocio.listaPorAtributo("evento", certificado.getEvento());

		int cargaHorariaInstrutor = 0;

		for(Modulo modulo : moduloList){
			List<ModuloInstrutor> moduloInstrutorList = this.moduloInstrutorNegocio.listaPorAtributo("modulo", modulo);

			for(ModuloInstrutor moduloInstrutor : moduloInstrutorList){
				if(moduloInstrutor.getInstrutor().equals(certificado.getInstrutor())){
					cargaHorariaInstrutor += moduloInstrutor.getModulo().getCargaHoraria().intValue();
				}
			}
		}

		return cargaHorariaInstrutor;
	}

	public DesempenhoDTO calcularDesempenho(Evento evento, Participante participante) {

		DesempenhoDTO desempenhoDTO = new DesempenhoDTO();

		Boolean avaliado = Boolean.FALSE;
		Boolean aprovado = Boolean.FALSE;
		Boolean calcular = Boolean.TRUE;

		BigDecimal somatorioFrequencia = BigDecimal.valueOf(0d);
		BigDecimal somatorioNota = BigDecimal.valueOf(0d);
		BigDecimal notaMedia = BigDecimal.valueOf(0d);
		BigDecimal frequenciaMedia = BigDecimal.valueOf(0d);

		List<ModuloDesempenhoDTO> listaDesempenho = new ArrayList<ModuloDesempenhoDTO>();
		List<Modulo> modulosEvento = this.moduloNegocio.listaPorAtributo("evento", evento);

		if (!modulosEvento.isEmpty()) {

			for (Modulo modulo : modulosEvento){

				Boolean aprovadoModulo = Boolean.FALSE;
				Nota nota = this.notaNegocio.obterNotaParticipanteModulo(modulo, participante);

				BigDecimal frequencia = this.frequenciaNegocio.obterFrequenciaModulo(modulo, participante);
				if (frequencia.compareTo(BigDecimal.valueOf(100)) == 1) {
					frequencia = BigDecimal.valueOf(100);
				}

				BigDecimal notaValor = BigDecimal.valueOf(0);

				if(AjudanteObjeto.eReferencia(nota)){

					notaValor = nota.getValor();
					somatorioFrequencia = somatorioFrequencia.add(frequencia);
					somatorioNota = somatorioNota.add(notaValor);
					aprovadoModulo = (nota.getValor().doubleValue() >= modulo.getNotaAprovacao().doubleValue()) && frequencia.doubleValue() >= modulo.getFrequenciaAprovacao().doubleValue();

				} else {

					if (AjudanteObjeto.eReferencia(modulo.getNotaAprovacao()) && modulo.getNotaAprovacao() != BigDecimal.valueOf(0)) {

						calcular = Boolean.FALSE;
						avaliado = Boolean.FALSE;
						aprovado = Boolean.FALSE;

						break;

					}

				}

				listaDesempenho.add(new ModuloDesempenhoDTO(modulo.getTitulo(), evento.getTitulo(), participante.getPessoa().getNome(), notaValor, frequencia, aprovadoModulo));

			}

			if (calcular) {

				frequenciaMedia = somatorioFrequencia.divide(BigDecimal.valueOf(modulosEvento.size()), 2, RoundingMode.HALF_UP);
				notaMedia = somatorioNota.divide(BigDecimal.valueOf(modulosEvento.size()), 2, RoundingMode.HALF_UP);

				if (evento.getAvaliarPorMedia()) {

					avaliado = Boolean.TRUE;
					aprovado = (notaMedia.doubleValue() >= (evento.getMediaNotaAprovacao().doubleValue()) && frequenciaMedia.doubleValue() >= evento.getMediaFrequenciaAprovacao().doubleValue());


				} else {

					aprovado = Boolean.TRUE;
					avaliado = Boolean.TRUE;

					for (ModuloDesempenhoDTO desempenho : listaDesempenho) {
						if (desempenho.getAprovado() == false) {
							aprovado = Boolean.FALSE;
							break;
						}
					}

				}

			}


		}

		desempenhoDTO.setListaDesempenho(listaDesempenho);
		desempenhoDTO.setAprovado(aprovado);
		desempenhoDTO.setAvaliado(avaliado);
		desempenhoDTO.setNotaMedia(notaMedia);
		desempenhoDTO.setFrequenciaMedia(frequenciaMedia);

		return desempenhoDTO;

	}
}
