package br.gov.go.tcm.sophos.rest.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MoodleTabelaPrincipalNotaDTO {

    private MoodleTabelaNotaDTO[] tables;

    public MoodleTabelaNotaDTO[] getTables() {
		return tables;
	}

	public void setTables(MoodleTabelaNotaDTO[] tables) {
		this.tables = tables;
	}

}
