/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBase;
import br.gov.go.tcm.sophos.entidade.Usuario;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.negocio.UsuarioNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

@Component
@Scope("view")
public class EsqueciMinhaSenhaControlador extends ControladorBase {

	private static final long serialVersionUID = 7903297002465207330L;

	@Autowired
	private UsuarioNegocio negocio;
	private String emailOuCPFUsuario;
	private Usuario usuario;
	private String senha;
	private String senhaRepetida;

	public void preRenderView() {
		
		final String token = this.obterObjetoDaRequisicao().getParameter("token");

		if (!AjudanteObjeto.eVazia(token)) {

			this.usuario = this.negocio.obterPorAtributo("token", token);

			if (!AjudanteObjeto.eReferencia(this.usuario)) {

				this.adicionarMensagemInformativa(this.obterMensagemPelaChave("link_invalido"));
			}
		}
	}


	public void acaoRedefinirSenha() {

		try {

			this.negocio.redefinirSenha(this.usuario, this.senha, this.senhaRepetida);
			this.redirecionar("/index.jsf?paginaanterior=esqueci-minha-senha");

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);
		}
	}


	public void acaoSolicitarRedefinicaoSenha() {

		try {

			String emailDestinatario = this.negocio.solicitarRedefinicaoSenha(this.getEmailOuCPFUsuario());
			String templateMensagem = this.obterMensagemPelaChave("email_redefinicao_enviado");
			this.adicionarMensagemInformativa(MessageFormat.format(templateMensagem, emailDestinatario));

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);

		} catch (final Exception e) {

			this.adicionarMensagemDeErro(e.getMessage());
		}
	}


	public UsuarioNegocio getNegocio() {
		return negocio;
	}


	public void setNegocio(UsuarioNegocio negocio) {
		this.negocio = negocio;
	}

	public String getEmailOuCPFUsuario() {
		return emailOuCPFUsuario;
	}


	public void setEmailOuCPFUsuario(String emailOuCPFUsuario) {
		this.emailOuCPFUsuario = emailOuCPFUsuario;
	}


	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}


	public void setSenha(String senha) {
		this.senha = senha;
	}


	public String getSenhaRepetida() {
		return senhaRepetida;
	}


	public void setSenhaRepetida(String senhaRepetida) {
		this.senhaRepetida = senhaRepetida;
	}

}
