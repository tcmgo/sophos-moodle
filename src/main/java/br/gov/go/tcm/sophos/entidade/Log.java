/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.entidade.enumeracao.EnumeracaoTipo;
import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoOperacao;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

@Entity(name = "br.gov.go.tcm.sophos.entidade.Log")
@Table(name = "log")
@TypeDefs({ @TypeDef(name = "operacao", typeClass = EnumeracaoTipo.class, parameters = { @Parameter(name = "enumClassName", value = "br.gov.go.tcm.sophos.entidade.enumeracao.TipoOperacao") }) })
@Banco(nome = Banco.SOPHOS)
public class Log extends EntidadePadrao {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = -7985033457448175547L;

	@Column(name = "tabela")
	/** Atributo tabela. */
	private String tabela;

	@Column(name = "entidade_id")
	/** Atributo idEntidade. */
	private Long idEntidade;

	@Column(name = "operacao")
	@Type(type = "operacao")
	/** Atributo origem. */
	private TipoOperacao origem;

	@ManyToOne
	@JoinColumn(name = "usuario_id")
	/** Atributo usuario. */
	private Usuario usuario;

	/**
	 * Retorna o valor do atributo <code>tabela</code>
	 *
	 * @return <code>String</code>
	 */
	public String getTabela() {

		return this.tabela;
	}

	/**
	 * Define o valor do atributo <code>tabela</code>.
	 *
	 * @param tabela
	 */
	public void setTabela(final String tabela) {

		this.tabela = tabela;
	}

	/**
	 * Retorna o valor do atributo <code>origem</code>
	 *
	 * @return <code>TipoOperacao</code>
	 */
	public TipoOperacao getOrigem() {

		return this.origem;
	}

	/**
	 * Define o valor do atributo <code>origem</code>.
	 *
	 * @param origem
	 */
	public void setOrigem(final TipoOperacao origem) {

		this.origem = origem;
	}


	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	/**
	 * Retorna o valor do atributo <code>idEntidade</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getIdEntidade() {

		return this.idEntidade;
	}

	/**
	 * Define o valor do atributo <code>idEntidade</code>.
	 *
	 * @param idEntidade
	 */
	public void setIdEntidade(final Long idEntidade) {

		this.idEntidade = idEntidade;
	}

}
