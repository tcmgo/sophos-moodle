/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBase;
import br.gov.go.tcm.sophos.entidade.PerfilUsuario;
import br.gov.go.tcm.sophos.entidade.Usuario;
import br.gov.go.tcm.sophos.entidade.enumeracao.PaginaEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.PerfilEnum;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import org.hibernate.LazyInitializationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.LinkedList;

@Component
@Scope("session")
public class NavegacaoControlador extends ControladorBase implements Serializable {

	private static final long serialVersionUID = -7221896392417104343L;

	private final LinkedList<PaginaEnum> historico;

	private PaginaEnum navegacao;
	
	public Boolean possuiPermissaoPorBloco(PaginaEnum... paginaEnumArray){
		
		for(PaginaEnum paginaEnum : paginaEnumArray){
			
			if(this.possuiPermissao(paginaEnum)){
				
				return Boolean.TRUE;
				
			}
		}
		
		return Boolean.FALSE;
	}
	
	public Boolean possuiPermissao(PaginaEnum paginaEnum){
		
		Usuario usuario = this.getUsuarioLogado();
		
		if(paginaEnum.getListPerfil().contains(PerfilEnum.PUBLICO)){
			
			return Boolean.TRUE;
			
		}else{
			
			if(AjudanteObjeto.eReferencia(usuario)){
				
				try{

					for(PerfilUsuario perfilUsuario : usuario.getPerfilList()){
						
						if(paginaEnum.getListPerfil().contains(perfilUsuario.getPerfil())){
							
							return Boolean.TRUE;
						}
					}
					
				}catch(LazyInitializationException ex){
					
					return Boolean.FALSE;
				}
			}
			
		}
		
		return Boolean.FALSE;
	}

	public Boolean isPaginaAcesso(){
		
		return this.getPagina().equals(PaginaEnum.ACESSO);
	}
	
	public NavegacaoControlador() {

		this.historico = new LinkedList<PaginaEnum>();
	}	

	public PaginaEnum getPagina() {

		if (this.navegacao == null) {

			this.acaoAbrirTela(PaginaEnum.EVENTOS_VISUALIZAR);
		}

		return this.navegacao;
	}

	public void acaoAbrirTela(PaginaEnum pagina) {

		this.processarNavegacao(pagina);
	}
	
	public void teste() {

		throw new ValidacaoException("TESTE");
	}
	
	public void inicialInit() {

		if (AjudanteObjeto.eReferencia(this.obterAtributoNaSessao(ControladorBase.USUARIO_LOGADO, Usuario.class))) {

			this.redirecionar("/");
		}
	}

	public void acaoVoltar() {

		PaginaEnum navegacao = this.historico.pollLast();

		if (AjudanteObjeto.eReferencia(navegacao) && navegacao.equals(this.navegacao)) {

			navegacao = this.historico.pollLast();
		}

		this.navegacao = navegacao;

		this.historico.add(navegacao);
		
	}

	public void processarNavegacao(final PaginaEnum pagina) {

		this.navegacao = pagina;

		this.historico.add(this.navegacao);

	}

	public PaginaEnum getNavegacao() {
		return navegacao;
	}

	public void setNavegacao(PaginaEnum navegacao) {
		this.navegacao = navegacao;
	}
	
}
