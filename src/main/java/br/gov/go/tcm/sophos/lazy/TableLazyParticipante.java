/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.lazy;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.Participante;
import br.gov.go.tcm.sophos.entidade.Usuario;
import br.gov.go.tcm.sophos.lazy.base.TableLazyDTO;
import br.gov.go.tcm.sophos.lazy.base.TableLazyPadrao;
import br.gov.go.tcm.sophos.negocio.ParticipanteNegocio;
import br.gov.go.tcm.sophos.negocio.UsuarioNegocio;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@Scope("session")
public class TableLazyParticipante extends TableLazyPadrao<Participante> {

	private static final long serialVersionUID = -6545514037278206167L;

	@Autowired
	private ParticipanteNegocio negocio;
	
	@Autowired
	private UsuarioNegocio usuarioNegocio;
	
	private String consulta = "";

	@Override
	protected ParticipanteNegocio getServico() {

		return this.negocio;
	}

	@Override
	public List<Participante> load(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		
		 TableLazyDTO<Participante> tableLazyDTO = this.getServico().listarPaginado(first, pageSize, sortField, sortOrder, filters, consulta);

		this.setRowCount(tableLazyDTO.getQuantidade().intValue());

		this.setPageSize(pageSize);
		
		List<Participante> participantes = tableLazyDTO.getLista();
		
		for (int i = 0; i < participantes.size(); i++) {
			
			Usuario usuario = usuarioNegocio.obterPorAtributo("participante", participantes.get(i));
			
			if (AjudanteObjeto.eReferencia(usuario) && AjudanteObjeto.eReferencia(usuario.getNomeUsuario())) {
				
				participantes.get(i).setNomeUsuario(usuario.getNomeUsuario().toLowerCase());
			}
		}
		
		this.setConsulta("");

		return participantes;

	}

	public String getConsulta() {
		return consulta;
	}

	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}

}
