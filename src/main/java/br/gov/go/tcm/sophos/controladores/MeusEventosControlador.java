/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 *
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 *
 * Este cabeçalho deve ser mantido.
 *
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.estrutura.entidade.transiente.Arquivo;
import br.gov.go.tcm.estrutura.negocio.ajudante.AjudanteDeArquivos;
import br.gov.go.tcm.sophos.controladores.base.ControladorBase;
import br.gov.go.tcm.sophos.entidade.Certificado;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Inscricao;
import br.gov.go.tcm.sophos.entidade.Participante;
import br.gov.go.tcm.sophos.entidade.dto.DesempenhoDTO;
import br.gov.go.tcm.sophos.entidade.dto.MeuEventoDTO;
import br.gov.go.tcm.sophos.entidade.dto.ModuloDesempenhoDTO;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoArquivo;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoDominioEnum;
import br.gov.go.tcm.sophos.lazy.TableLazyMeusEventos;
import br.gov.go.tcm.sophos.negocio.CertificadoNegocio;
import br.gov.go.tcm.sophos.negocio.DominioNegocio;
import br.gov.go.tcm.sophos.negocio.EventoNegocio;
import br.gov.go.tcm.sophos.negocio.InscricaoNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

@Component
@Scope("request")
public class MeusEventosControlador extends ControladorBase{

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private EventoNegocio negocio;

	@Autowired
	private CertificadoNegocio certificadoNegocio;

	@Autowired
	private DominioNegocio dominioNegocio;

	@Autowired
	private InscricaoNegocio inscricaoNegocio;

	@Autowired
	private TableLazyMeusEventos lista;

	private List<ModuloDesempenhoDTO> desempenhoList;

	private Evento evento;

	private Boolean aprovadoEvento = false;

	private BigDecimal notaMedia = BigDecimal.valueOf(0d);

	private BigDecimal frequenciaMedia = BigDecimal.valueOf(0d);

	private BigDecimal cargaHorariaPresencial = BigDecimal.valueOf(0d);

	private BigDecimal cargaHorariaSemiPresencial = BigDecimal.valueOf(0d);

	private BigDecimal cargaHorariaEad = BigDecimal.valueOf(0d);

	private BigDecimal cargaHorariaTotal = BigDecimal.valueOf(0d);

	@PostConstruct
	public void init(){

		List<Inscricao> inscricoes = inscricaoNegocio.listaPorAtributo("participante", this.getUsuarioLogado().getParticipante());

		for (Inscricao inscricao : inscricoes) {

			if (inscricao.getEvento().getModalidade().equals(dominioNegocio.obterDominio(TipoDominioEnum.Modalidade, 1L)) &&
					certificadoNegocio.calcularDesempenho(inscricao.getEvento(), this.getUsuarioLogado().getParticipante()).getAprovado()) {
				cargaHorariaPresencial = cargaHorariaPresencial.add(inscricao.getEvento().getCargaHoraria());
			}

			if (inscricao.getEvento().getModalidade().equals(dominioNegocio.obterDominio(TipoDominioEnum.Modalidade, 2L)) &&
					certificadoNegocio.calcularDesempenho(inscricao.getEvento(), this.getUsuarioLogado().getParticipante()).getAprovado()) {
				cargaHorariaEad = cargaHorariaEad.add(inscricao.getEvento().getCargaHoraria());
			}

			if (inscricao.getEvento().getModalidade().equals(dominioNegocio.obterDominio(TipoDominioEnum.Modalidade, 3L)) &&
					certificadoNegocio.calcularDesempenho(inscricao.getEvento(), this.getUsuarioLogado().getParticipante()).getAprovado()) {
				cargaHorariaSemiPresencial = cargaHorariaSemiPresencial.add(inscricao.getEvento().getCargaHoraria());
			}

			if (certificadoNegocio.calcularDesempenho(inscricao.getEvento(), this.getUsuarioLogado().getParticipante()).getAprovado()) {
				cargaHorariaTotal = cargaHorariaTotal.add(inscricao.getEvento().getCargaHoraria());
			}

		}

	}

	public void carregarDesempenhoUsuarioLogado(Evento evento){

		this.carregarDesempenho(evento, this.getUsuarioLogado().getParticipante());
	}

	public void carregarDesempenho(Evento evento, Participante participante){

		this.evento = evento;

		DesempenhoDTO desempenhoDTO = certificadoNegocio.calcularDesempenho(evento, participante);

		this.setDesempenhoList(desempenhoDTO.getListaDesempenho());
		this.aprovadoEvento = desempenhoDTO.getAprovado();
		this.notaMedia = desempenhoDTO.getNotaMedia();
		this.frequenciaMedia = desempenhoDTO.getFrequenciaMedia();

		this.executarJS("openModalDesempenho()");

	}

	public boolean isJaTemCertificado(MeuEventoDTO meuEvento) {

		try {
			return this.certificadoNegocio.isJaTemCertificadoParticipante(meuEvento.getEvento(), this.getUsuarioLogado().getParticipante());
		} catch (IOException e) {
			e.printStackTrace();
			return false;

		}

	}

	public void imprimirCertificado(MeuEventoDTO meuEvento, Boolean isGerarNovo){

		try {

			Certificado certificado = this.certificadoNegocio.obterCertificado(meuEvento.getEvento(), this.getUsuarioLogado().getParticipante(), isGerarNovo);

			Arquivo arquivo = AjudanteDeArquivos.abrir(certificado.getArquivo().getArquivoGED());

			this.download(arquivo.getStream(), TipoArquivo.PDF);

		} catch (Exception e) {

			e.printStackTrace();
		}


	}

	public TableLazyMeusEventos getLista() {

		this.lista.setUsuarioLogado(this.getUsuarioLogado());

		return this.lista;
	}

	protected EventoNegocio getNegocio() {

		return this.negocio;
	}

	public List<ModuloDesempenhoDTO> getDesempenhoList() {
		return desempenhoList;
	}

	public void setDesempenhoList(List<ModuloDesempenhoDTO> desempenhoList) {
		this.desempenhoList = desempenhoList;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public Boolean getAprovadoEvento() {
		return aprovadoEvento;
	}

	public void setAprovadoEvento(Boolean aprovadoEvento) {
		this.aprovadoEvento = aprovadoEvento;
	}

	public BigDecimal getNotaMedia() {
		return notaMedia;
	}

	public void setNotaMedia(BigDecimal notaMedia) {
		this.notaMedia = notaMedia;
	}

	public BigDecimal getFrequenciaMedia() {
		return frequenciaMedia;
	}

	public void setFrequenciaMedia(BigDecimal frequenciaMedia) {
		this.frequenciaMedia = frequenciaMedia;
	}

	public BigDecimal getCargaHorariaPresencial() {

		return cargaHorariaPresencial;
	}

	public void setCargaHorariaPresencial(BigDecimal cargaHorariaPresencial) {
		this.cargaHorariaPresencial = cargaHorariaPresencial;
	}

	public BigDecimal getCargaHorariaSemiPresencial() {
		return cargaHorariaSemiPresencial;
	}

	public void setCargaHorariaSemiPresencial(BigDecimal cargaHorariaSemiPresencial) {
		this.cargaHorariaSemiPresencial = cargaHorariaSemiPresencial;
	}

	public BigDecimal getCargaHorariaEad() {
		return cargaHorariaEad;
	}

	public void setCargaHorariaEad(BigDecimal cargaHorariaEad) {
		this.cargaHorariaEad = cargaHorariaEad;
	}

	public BigDecimal getCargaHorariaTotal() {
		return cargaHorariaTotal;
	}

	public void setCargaHorariaTotal(BigDecimal cargaHorariaTotal) {
		this.cargaHorariaTotal = cargaHorariaTotal;
	}

}