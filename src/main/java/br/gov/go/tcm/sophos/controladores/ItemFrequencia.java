/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.entidade.Participante;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;
import br.gov.go.tcm.sophos.entidade.dto.FrequenciaEncontroDTO;

import java.util.List;

public class ItemFrequencia extends EntidadePadrao implements Comparable<ItemFrequencia> {
	
	private static final long serialVersionUID = 1L;

	private Participante participante;
	
	private List<FrequenciaEncontroDTO> frequencia;

	public ItemFrequencia(Participante participante, List<FrequenciaEncontroDTO> frequencia) {
		this.participante = participante;
		this.frequencia = frequencia;
	}

	public Participante getParticipante() {
		return participante;
	}

	public void setParticipante(Participante participante) {
		this.participante = participante;
	}

	public List<FrequenciaEncontroDTO> getFrequencia() {
		return frequencia;
	}

	public void setFrequencia(List<FrequenciaEncontroDTO> frequencia) {
		this.frequencia = frequencia;
	}
	
	@Override
	public int compareTo(ItemFrequencia f) {
		if(this.getParticipante().getPessoa().getNome() != null){
			return this.getParticipante().getPessoa().getNome().compareTo(f.getParticipante().getPessoa().getNome());
		}
		return 0;
	}

}
