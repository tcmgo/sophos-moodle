/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.*;
import br.gov.go.tcm.sophos.entidade.enumeracao.PaginaEnum;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoArquivo;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoDominioEnum;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.lazy.TableLazyInstrutor;
import br.gov.go.tcm.sophos.negocio.*;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
@Scope("session")
public class InstrutorControlador extends ControladorBaseCRUD<Instrutor, InstrutorNegocio> {

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private InstrutorNegocio negocio;

	@Autowired
	private TableLazyInstrutor lista;

	@Autowired
	private NavegacaoControlador navegacaoControlador;

	@Autowired
	private CidadeNegocio cidadeNegocio;

	@Autowired
	private EstadoNegocio estadoNegocio;

	@Autowired
	private DominioNegocio dominioNegocio;
	
	@Autowired
	private ModuloInstrutorNegocio moduloInstrutorNegocio;

	private List<Dominio> formacaoAcademicaList;

	private List<Dominio> nivelEscolaridadeList;

	private List<Dominio> situacaoInstrutorList;

	private List<Dominio> tipoInstrutorList;

	private List<Dominio> sexoList;

	private Estado estado;

	private List<Estado> estados;

	private List<Cidade> cidades;

	@Override
	protected InstrutorNegocio getNegocio() {

		return this.negocio;
	}

	public void uploadAssinatura(final FileUploadEvent event) {

		this.getEntidade().setAssinatura(new Anexo());

		this.getEntidade().getAssinatura().setDataCadastro(new Date());

		this.getEntidade().getAssinatura().setFile(event.getFile());

		this.getEntidade().getAssinatura().setDescricao(event.getFile().getFileName());

		this.getEntidade().getAssinatura().setTipoArquivo(TipoArquivo.getTipoArquivo(event.getFile().getContentType()));

	}

	public void uploadCurriculo(final FileUploadEvent event) {

		this.getEntidade().setCurriculo(new Anexo());

		this.getEntidade().getCurriculo().setDataCadastro(new Date());

		this.getEntidade().getCurriculo().setFile(event.getFile());

		this.getEntidade().getCurriculo().setDescricao(event.getFile().getFileName());

		this.getEntidade().getCurriculo().setTipoArquivo(TipoArquivo.getTipoArquivo(event.getFile().getContentType()));

	}

	public void acaoInserir() {

		try {
			
			this.getEntidade().getPessoa().setNome(this.getEntidade().getPessoa().getNome().toUpperCase());

			this.getEntidade().setSituacaoInstrutor(this.dominioNegocio.obterDominio(TipoDominioEnum.SituacaoInstrutor, 1L));

			this.getNegocio().salvar(this.getEntidade());

			this.adicionarMensagemInformativa(this.obterMensagemSucessoAoInserir());

			this.executarAposInserirSucesso();

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);
		}
	}

	public void listarCidades() {

		if (AjudanteObjeto.eReferencia(estado)) {

			this.cidades = this.cidadeNegocio.listaPorAtributo("estado", estado);

		}
	}

	public void acaoAbrirTela(Instrutor entidade, Boolean visualizar) {

		if (AjudanteObjeto.eReferencia(entidade)) {

			if (AjudanteObjeto.eReferencia(entidade.getEndereco())
					&& AjudanteObjeto.eReferencia(entidade.getEndereco().getCidade())
					&& AjudanteObjeto.eReferencia(entidade.getEndereco().getCidade().getEstado())) {
				
				this.estado = entidade.getEndereco().getCidade().getEstado();
				this.listarCidades();
				
			} 

			this.setEntidade(entidade);

			entidade.setEndereco(
					AjudanteObjeto.eReferencia(entidade.getEndereco()) ? entidade.getEndereco() : new Endereco());

		} else {

			this.criarNovaInstanciaEntidade();
		}

		this.setVisualizar(visualizar);

		this.navegacaoControlador.processarNavegacao(PaginaEnum.INSTRUTORES_CADASTRO);

	}

	@Override
	protected void criarNovaInstanciaEntidade() {

		this.setEntidade(new Instrutor());

		this.getEntidade().setEndereco(new Endereco());

		this.getEntidade().setPessoa(new Pessoa());

		this.getEntidade().setDataCadastro(new Date());

		this.estado = new Estado();

	}
	
	@Override
	protected void executarAposInserirSucesso() {
		this.navegacaoControlador.processarNavegacao(PaginaEnum.INSTRUTORES);
	}

	public void limparAnexoCurriculo() {

		this.getEntidade().setCurriculo(null);
	}

	public void limparAnexoAssinatura() {

		this.getEntidade().setAssinatura(null);
	}

	@Override
	public TableLazyInstrutor getLista() {

		return this.lista;
	}

	public List<Estado> getEstados() {

		if (!AjudanteObjeto.eReferencia(estados)) {

			this.estados = this.estadoNegocio.listar();
		}

		return estados;
	}

	public List<Dominio> getFormacaoAcademicaList() {

		if (!AjudanteObjeto.eReferencia(formacaoAcademicaList)) {

			this.formacaoAcademicaList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.FormacaoAcademica);
		}

		return formacaoAcademicaList;
	}

	public List<Dominio> getNivelEscolaridadeList() {

		if (!AjudanteObjeto.eReferencia(nivelEscolaridadeList)) {

			this.nivelEscolaridadeList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.NivelEscolaridade);
		}

		return nivelEscolaridadeList;
	}

	public List<Dominio> getSituacaoInstrutorList() {

		if (!AjudanteObjeto.eReferencia(situacaoInstrutorList)) {

			this.situacaoInstrutorList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.SituacaoInstrutor);
		}

		return situacaoInstrutorList;
	}

	public List<Dominio> getTipoInstrutorList() {

		if (!AjudanteObjeto.eReferencia(tipoInstrutorList)) {

			this.tipoInstrutorList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.TipoInstrutor);
		}

		return tipoInstrutorList;
	}

	public List<Dominio> getSexoList() {

		if (!AjudanteObjeto.eReferencia(sexoList)) {

			this.sexoList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.TipoSexo);
		}

		return sexoList;
	}

	public List<Cidade> getCidades() {

		return cidades;
	}

	public Estado getEstado() {

		return estado;
	}

	public void setEstado(Estado estado) {

		this.estado = estado;
	}
	
	@Override
	public void acaoExcluir(Instrutor instrutor) {

		try {
			
            List<ModuloInstrutor> listaModuloInstrutor = this.moduloInstrutorNegocio.listaPorAtributo("instrutor", instrutor, SortOrder.ASCENDING, "id");
            
            if (listaModuloInstrutor != null && listaModuloInstrutor.size() > 0) {
            	
            	this.adicionarMensagemInformativa("Não é possível realizar a exclusão, pois o instrutor já ministrou algum módulo de eventos.");
         
            } else {
            	
            	this.negocio.excluir(instrutor);
    			this.adicionarMensagemInformativa(this.obterMensagemSucessoAoExcluir());
            }

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);
		}
	}

}
