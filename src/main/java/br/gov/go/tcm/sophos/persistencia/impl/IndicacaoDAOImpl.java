/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.persistencia.impl;

import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Indicacao;
import br.gov.go.tcm.sophos.entidade.Participante;
import br.gov.go.tcm.sophos.persistencia.IndicacaoDAO;
import br.gov.go.tcm.sophos.persistencia.base.DAO;
import br.gov.go.tcm.sophos.persistencia.impl.base.DAOGenerico;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import java.lang.reflect.Field;

@Repository
public class IndicacaoDAOImpl extends DAOGenerico<Indicacao> implements IndicacaoDAO {

	private static final long serialVersionUID = 294560128456827146L;

	@Autowired
	public IndicacaoDAOImpl(@Qualifier(DAO.SESSION_FACTORY_SOPHOS) final SessionFactory factory,
			@Qualifier(DAO.HIBERNATE_TEMPLATE_SOPHOS) final HibernateTemplate hibernateTemplate) {

		this.setHibernateTemplate(hibernateTemplate);

		this.setSessionFactory(factory);

	}

	@Override
	public boolean existeIndicacaoParaParticipante(Participante participante, Evento evento) {

		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactorySophos());

		final Session session = this.getSession(hibernateTemplate);

		Criteria criteria = session.createCriteria(this.getTipoEntidade());

		criteria.add(Restrictions.eq("participante", participante));

		criteria.add(Restrictions.eq("evento", evento));

		criteria.setProjection(Projections.count("id"));

		try {

			return ((Long) criteria.uniqueResult()) > 0;

		} finally {

			session.close();
		}
	}

	@Override
	protected void buscaGenericaPorConsulta(String consulta, Criteria criteria, Disjunction queryAll, Field field) {

		super.buscaGenericaPorConsulta(consulta, criteria, queryAll, field);

		if (field.getType().equals(Participante.class)) {
			
			criteria.createAlias("participante", "participante");

			criteria.createAlias("participante.pessoa", "pessoa");

			queryAll.add(Restrictions.ilike("pessoa.nome", consulta, MatchMode.ANYWHERE));

			queryAll.add(Restrictions.ilike("participante.matricula", consulta, MatchMode.ANYWHERE));

		} else if (field.getType().equals(Evento.class)) {

			criteria.createAlias("evento", "evento");

			queryAll.add(Restrictions.ilike("evento.titulo", consulta, MatchMode.ANYWHERE));

		}
	}
	
}
