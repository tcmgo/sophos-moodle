/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteData;
import br.gov.go.tcm.estrutura.entidade.orcafi.Municipio;
import br.gov.go.tcm.estrutura.entidade.orcafi.Orgao;
import br.gov.go.tcm.estrutura.seguranca.ajudante.AjudanteCriptografia;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.*;
import br.gov.go.tcm.sophos.entidade.enumeracao.*;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.lazy.base.TableLazyPadrao;
import br.gov.go.tcm.sophos.negocio.*;
import org.hibernate.exception.SQLGrammarException;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Component
@Scope("session")
public class MeuCadastroParticipanteControlador extends ControladorBaseCRUD<Participante, ParticipanteNegocio> {

	private static final long serialVersionUID = 5890650357536207877L;

	@Autowired
	private ParticipanteNegocio negocio;

	@Autowired
	private CidadeNegocio cidadeNegocio;

	@Autowired
	private EstadoNegocio estadoNegocio;

	@Autowired
	private DominioNegocio dominioNegocio;

	@Autowired
	private UsuarioNegocio usuarioNegocio;
	
	@Autowired
	private PessoaNegocio pessoaNegocio;

	@Autowired
	private NavegacaoControlador navegacaoControlador;

	private Estado estado;

	private List<Estado> estados;

	private List<Cidade> cidades;

	private List<Municipio> municipioList;

	private List<Dominio> sexoList;

	private List<Dominio> formacaoAcademicaList;

	private List<Dominio> nivelEscolaridadeList;

	private List<Dominio> tipoPublicAlvoList;

	private List<Orgao> orgaosList;

	private Usuario usuario;

	private Usuario usuarioCadastro;

	private List<Dominio> cargosList;

	private List<String> lotacaoList;

	private boolean outroCargo;

	private Dominio dominioCargo;

	private List<Dominio> tiposNecessidadesEspeciaisList;

	private List<Dominio> mandatosList;
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@Override
	protected void executarAposInserirSucesso() {

		this.usuario.setParticipante(this.getEntidade());

		if (AjudanteObjeto.eReferencia(this.usuario.getId())) {

			this.obterContextoJSF().getExternalContext().getSessionMap().remove(USUARIO_LOGADO);

			this.definirObjetoNaSessao(USUARIO_LOGADO, this.usuario);

			usuario.setPerfilList(this.usuarioNegocio.listarPerfisUsuario(usuario));

			if (!this.usuarioNegocio.existePerfilParticipante(usuario)) {

				PerfilUsuario perfilUsuario = new PerfilUsuario();

				perfilUsuario.setUsuario(this.usuario);

				perfilUsuario.setPerfil(PerfilEnum.PARTICIPANTE);

				usuario.getPerfilList().add(perfilUsuario);

				this.definirObjetoNaSessao(PERFIL_SELECIONADO, perfilUsuario.getPerfil());

			} else {

				this.definirObjetoNaSessao(PERFIL_SELECIONADO, this.usuarioNegocio.obterPerfilPrincipal(usuario));
			}

			this.usuario.setPessoa(this.getEntidade().getPessoa() );
			this.usuarioNegocio.salvarSimples(this.usuario);

		} else {

			this.usuario.setPessoa(this.getEntidade().getPessoa());

			this.usuarioNegocio.salvarSimples(this.usuario);

			usuario.setPerfilList(this.usuarioNegocio.listarPerfisUsuario(usuario));

			this.definirObjetoNaSessao(PERFIL_SELECIONADO, this.usuarioNegocio.obterPerfilPrincipal(usuario));

			this.definirObjetoNaSessao(USUARIO_LOGADO, this.usuario);

		}

		this.navegacaoControlador.acaoAbrirTela(PaginaEnum.EVENTOS_VISUALIZAR);

	}

	public void inicializarTela() {

		if (AjudanteObjeto.eReferencia(this.getUsuarioLogado())
				&& AjudanteObjeto.eReferencia(this.getUsuarioLogado().getId())) {

			usuario = this.usuarioNegocio.obter(this.getUsuarioLogado().getId());

		} else {

			usuario = usuarioCadastro;
		}

		if (!AjudanteObjeto.eReferencia(this.getEntidade().getPessoa())
				&& AjudanteObjeto.eReferencia(usuario.getParticipante())) {

			Participante participante = usuario.getParticipante();

			if (AjudanteObjeto.eReferencia(participante)) {

				if (AjudanteObjeto.eReferencia(participante.getEndereco())
						&& AjudanteObjeto.eReferencia(participante.getEndereco().getCidade())) {

					this.estado = participante.getEndereco().getCidade().getEstado();

				} else {

					this.estado = this.estadoNegocio.obterPorAtributo("uf", "GO");

				}

				this.listarCidades();

				this.setEntidade(participante);

				if (!AjudanteObjeto.eReferencia(this.getEntidade().getEndereco())) {

					this.getEntidade().setEndereco(new Endereco());
				}
			}

		} else if (!AjudanteObjeto.eReferencia(this.getEntidade().getPessoa())) {

			this.setEntidade(new Participante());

			this.getEntidade().setDataCadastro(new Date());

			this.getEntidade().setEndereco(new Endereco());

			this.getEntidade().setPessoa(usuario.getPessoa());

			if (usuario.getTipoUsuario().equals(TipoUsuario.class.getSimpleName(),
					TipoUsuario.USUARIO_INTERNO.getCodigo())) {

				this.getEntidade().setMatricula(usuario.getMatricula());

				// USUARIO TCM
				this.getEntidade().setTipo(this.dominioNegocio.obterDominio(TipoDominioEnum.TipoPublicoAlvo, 3L));

				this.selecionarTipoParticipante();
			}

			
			this.getNegocio().PreecherParticipanteBaseRH(this.getEntidade());

			if (AjudanteObjeto.eReferencia(this.getEntidade().getTipo())
					&& (this.getEntidade().getTipo().equals(TipoDominioEnum.TipoPublicoAlvo.name(), 3L)
							|| this.getEntidade().getTipo().equals(TipoDominioEnum.TipoPublicoAlvo.name(), 2L))) {

				this.setEstado(this.estadoNegocio.obterPorAtributo("uf", "GO"));
			}

			this.listarCidades();

		}

		if (this.getEntidade().getCargo() != null && !this.getEntidade().getCargo().equals("")) {

			Dominio cargo = this.dominioNegocio.obterPorAtributo("descricao", this.getEntidade().getCargo());
			if (cargo != null) {

				this.setOutroCargo(false);
				this.setDominioCargo(cargo);

			} else {

				this.setOutroCargo(true);
				this.setDominioCargo(this.dominioNegocio.obterDominio(TipoDominioEnum.NomeCargo, 24L));
			}

		}
	}

	public void completarCadastro() {

		try {
			
			boolean temServidorTCMComCPF = pessoaNegocio.existePessoaComCPFBaseRH(this.usuarioCadastro.getPessoa().getCpf());
			
			if (temServidorTCMComCPF) {
			
				this.adicionarMensagemInformativa("Existe um servidor do TCM-GO com o CPF informado (" +  this.usuarioCadastro.getPessoa().getCpf() + "). " +
				                                  "Se você for um servidor do TCM-GO, não é necessário realizar este cadastro direcionado a jurisdicionados e sociedade. " + 
						                          "Por favor, clique em VOLTAR e realize o login com usuário e senha do MONITOR TCM na seção SERVIDORES TCM-GO, à direita na tela de acesso do Sophos. ");
			} else {
			

				this.usuarioNegocio.validarUsuario(this.usuarioCadastro);
	
				usuarioCadastro.setSenha(AjudanteCriptografia.gerarMD5(usuarioCadastro.getSenha()));
	
				usuarioCadastro.setTipoUsuario(this.dominioNegocio.obterDominio(TipoDominioEnum.TipoUsuario,
						TipoUsuario.USUARIO_EXTERNO.getCodigo()));
	
				this.navegacaoControlador.acaoAbrirTela(PaginaEnum.PARTICIPANTES_MEU_CADASTRO);
			
			}

		} catch (final ValidacaoException e) {

			this.adicionarMensagemExcecao(e);

		} catch (SQLGrammarException sQLGrammarException) {

			this.adicionarMensagemDeErro("Ocorreu um erro na comunicação com a base de dados da Siedos.");

		} catch (Exception e) {

			this.adicionarMensagemDeErro("Ocorreu um erro durante o cadastro. Tente novamente mais tarde.");
		}
	}

	public void verificaCPF() {

		try {

			if (!StringUtils.isEmpty(this.getUsuarioCadastro().getPessoa().getCpf())) {

				this.usuarioNegocio.verificaCPFUnico(this.getUsuarioCadastro().getPessoa().getCpf());

			}

		} catch (final ValidacaoException e) {

			this.getUsuarioCadastro().getPessoa().setCpf("");

			this.adicionarMensagemExcecao(e);
		}

	}

	public void verificaExistenciaCPF() {

		if (!StringUtils.isEmpty(this.getEntidade().getPessoa().getCpf())) {

			this.getNegocio().PreecherParticipanteBaseRH(this.getEntidade());

			if (AjudanteObjeto.eReferencia(this.getEntidade().getTipo())
					&& (this.getEntidade().getTipo().equals(TipoDominioEnum.TipoPublicoAlvo.name(), 3L)
							|| this.getEntidade().getTipo().equals(TipoDominioEnum.TipoPublicoAlvo.name(), 2L))) {

				this.setEstado(this.estadoNegocio.obterPorAtributo("uf", "GO"));
			}

			this.listarCidades();
		}

	}

	public void verificaMatricula() {

		if (!StringUtils.isEmpty(this.getEntidade().getMatricula())) {

			this.getEntidade().setMatricula(this.getEntidade().getMatricula().replace("_", ""));

			if (this.getEntidade().getMatricula().length() == 5) {
				this.getEntidade().setMatricula("0".concat(this.getEntidade().getMatricula()));
			}

			if (!usuarioNegocio.isExisteMatriculaBaseUsuariosInterno(this.getEntidade().getMatricula())) {
				this.getEntidade().setMatricula("");
				this.adicionarMensagemDeAlerta(this.obterMensagemPelaChave("matricula_nao_encontrada"));
			}

		}

	}

	public void listarCidades() {

		if (AjudanteObjeto.eReferencia(estado)) {

			this.cidades = this.cidadeNegocio.listaPorAtributo("estado", estado, SortOrder.ASCENDING, "descricao");

			if (estado.getUf().equalsIgnoreCase("GO")) {

				this.municipioList = this.cidadeNegocio.listaMunicipios();
				Collections.sort(this.municipioList);
			}

		}
	}

	public void listarOrgaos() {

		if (AjudanteObjeto.eReferencia(this.getEntidade().getEndereco().getMunicipioId())) {

			this.orgaosList = this.getNegocio().listarOrgaosAtivos(this.getEntidade().getEndereco().getMunicipioId());

		}
	}

	public void selecionarTipoParticipante() {

		if (this.getEntidade().getTipo().getCodigo().equals(TipoPublicoAlvo.JURISDICIONADO.getCodigo())) {

			this.selecionarEstado("GO");
			this.limparMunicipio();

		} else if (this.getEntidade().getTipo().getCodigo().equals(TipoPublicoAlvo.SOCIEDADE.getCodigo())) {

			this.limparEstado();
			this.limparMunicipio();

		} else if (this.getEntidade().getTipo().getCodigo().equals(TipoPublicoAlvo.INTERNO_TCM.getCodigo())) {

			this.selecionarEstado("GO");
			this.selecionarMunicipioGoiania();

		}

	}

	private void selecionarEstado(String uf) {
		this.estado = this.estadoNegocio.obterPorAtributo("uf", uf.toUpperCase());
		this.listarCidades();
	}

	private void selecionarMunicipioGoiania() {
		this.getEntidade().getEndereco().setMunicipioId(89L);
	}

	private void limparEstado() {
		this.estado = null;
	}

	private void limparMunicipio() {
		this.getEntidade().getEndereco().setCidade(null);
		this.getEntidade().getEndereco().setMunicipioId(null);
		this.orgaosList = null;
		this.getEntidade().setCodigoOrgao(null);
		this.getEntidade().setCodigoMunicipio(null);
	}

	public List<Estado> getEstados() {

		if (!AjudanteObjeto.eReferencia(estados)) {

			this.estados = this.estadoNegocio.listar();
		}

		return estados;
	}

	public List<Dominio> getFormacaoAcademicaList() {

		if (!AjudanteObjeto.eReferencia(formacaoAcademicaList)) {

			this.formacaoAcademicaList = this.dominioNegocio.listaPorAtributo("nome",
					TipoDominioEnum.FormacaoAcademica);
		}

		return formacaoAcademicaList;
	}

	public List<Dominio> getNivelEscolaridadeList() {

		if (!AjudanteObjeto.eReferencia(nivelEscolaridadeList)) {

			this.nivelEscolaridadeList = this.dominioNegocio.listaPorAtributo("nome",
					TipoDominioEnum.NivelEscolaridade);
		}

		return nivelEscolaridadeList;
	}

	public List<Dominio> getSexoList() {

		if (!AjudanteObjeto.eReferencia(sexoList)) {

			this.sexoList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.TipoSexo);
		}

		return sexoList;
	}

	public List<Dominio> getTipoPublicAlvoList() {

		if (!AjudanteObjeto.eReferencia(tipoPublicAlvoList)) {

			this.tipoPublicAlvoList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.TipoPublicoAlvo);
		}

		return tipoPublicAlvoList;
	}

	public List<Cidade> getCidades() {

		return cidades;
	}

	public List<Orgao> getOrgaosList() {

		if (!AjudanteObjeto.eReferencia(this.orgaosList)) {
			this.listarOrgaos();
		}

		return orgaosList;
	}

	public List<Municipio> getMunicipioList() {

		return municipioList;
	}

	public Estado getEstado() {

		return estado;
	}

	public void setEstado(Estado estado) {

		this.estado = estado;
	}

	public Usuario getUsuarioCadastro() {

		if (usuarioCadastro == null) {

			usuarioCadastro = new Usuario();

			usuarioCadastro.setPessoa(new Pessoa());
		}

		return usuarioCadastro;
	}

	public void setUsuarioCadastro(Usuario usuarioCadastro) {

		this.usuarioCadastro = usuarioCadastro;
	}

	@Override
	public TableLazyPadrao<Participante> getLista() {

		return null;
	}

	@Override
	protected ParticipanteNegocio getNegocio() {

		return negocio;
	}

	@Override
	public void acaoInserir() {

		if (!AjudanteData.dataEstaEntreOPeriodo(this.getEntidade().getPessoa().getDataNascimento(),
				this.getDataMinima(), this.getDataMaxima())) {

			this.adicionarMensagemInformativa(
					"A data de nascimento deve estar entre " + dateFormat.format(this.getDataMinima()) + " e "
							+ dateFormat.format(this.getDataMaxima()) + ". Verifique!");

		} else if (AjudanteObjeto.eReferencia(this.getEntidade().getDataAdmissao())
				&& !AjudanteData.dataEstaEntreOPeriodo(this.getEntidade().getDataAdmissao(), this.getDataMinima(),
						this.getDataMaxima())) {

			this.adicionarMensagemInformativa(
					"A data de admissão no cargo deve estar entre " + dateFormat.format(this.getDataMinima()) + " e "
							+ dateFormat.format(this.getDataMaxima()) + ". Verifique!");

		} else {

			this.getEntidade().getPessoa().setNome(this.getEntidade().getPessoa().getNome().toUpperCase());
			super.acaoInserir();

		}

	};

	public void acaoInserirSemMensagemSucesso() {
		this.getEntidade().getPessoa().setNome(this.getEntidade().getPessoa().getNome().toUpperCase());
		this.getNegocio().salvar(this.getEntidade());
		this.executarAposInserirSucesso();
	}

	public boolean isOutroCargo() {
		return outroCargo;
	}

	public void setOutroCargo(boolean outroCargo) {
		this.outroCargo = outroCargo;
	}

	public List<Dominio> getCargosList() {

		if (!AjudanteObjeto.eReferencia(cargosList)) {

			this.cargosList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.NomeCargo, SortOrder.ASCENDING, "descricao");

		}

		return cargosList;

	}

	public void setCargosList(List<Dominio> cargosList) {
		this.cargosList = cargosList;
	}

	public void setOrgaosList(List<Orgao> orgaosList) {
		this.orgaosList = orgaosList;
	}

	public Dominio getDominioCargo() {
		return dominioCargo;
	}

	public void setDominioCargo(Dominio dominioCargo) {
		this.dominioCargo = dominioCargo;
	}

	public List<String> getLotacaoList() {
		if (!AjudanteObjeto.eReferencia(lotacaoList)) {
			lotacaoList = this.getNegocio().listarLotacoes();
		}
		return lotacaoList;
	}

	public void setLotacaoList(List<String> lotacaoList) {
		this.lotacaoList = lotacaoList;
	}

	public void verificaDominioCargo() {

		if (this.dominioCargo.equals(this.dominioNegocio.obterDominio(TipoDominioEnum.NomeCargo, 24L))) {

			this.setOutroCargo(true);
			this.getEntidade().setCargo("");

		} else {

			this.getEntidade().setCargo(this.dominioCargo.getDescricao());
			this.setOutroCargo(false);
		}

	}

	public List<Dominio> getTiposNecessidadesEspeciaisList() {

		if (!AjudanteObjeto.eReferencia(tiposNecessidadesEspeciaisList)) {

			this.tiposNecessidadesEspeciaisList = this.dominioNegocio.listaPorAtributo("nome",
					TipoDominioEnum.TipoNecessidadeEspecial, SortOrder.ASCENDING, "codigo");
		}

		return tiposNecessidadesEspeciaisList;
	}

	public void setTiposNecessidadesEspeciaisList(List<Dominio> tiposNecessidadesEspeciaisList) {
		this.tiposNecessidadesEspeciaisList = tiposNecessidadesEspeciaisList;
	}

	public List<Dominio> getMandatosList() {

		if (!AjudanteObjeto.eReferencia(mandatosList)) {

			this.mandatosList = this.dominioNegocio.listaPorAtributo("nome", TipoDominioEnum.Mandato,
					SortOrder.ASCENDING, "codigo");
		}

		return mandatosList;
	}

	public void setMandatosList(List<Dominio> mandatosList) {
		this.mandatosList = mandatosList;
	}

}
