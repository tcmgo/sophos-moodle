/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "avaliacao")
@Banco(nome = Banco.SOPHOS)
public class Avaliacao extends EntidadePadrao {

	private static final long serialVersionUID = 351828610665868345L;

	@Column(name = "observacao", length = 2000)
	private String observacao;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "avaliacao")
	private List<QuestionarioAvaliacao> questionarioAvaliacaoList;

	@JoinColumn(name = "modulo_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Modulo modulo;

	@JoinColumn(name = "participante_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Participante participante;

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Modulo getModulo() {
		return modulo;
	}

	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}

	public Participante getParticipante() {
		return participante;
	}

	public void setParticipante(Participante participante) {
		this.participante = participante;
	}

	public List<QuestionarioAvaliacao> getQuestionarioAvaliacaoList() {
		return questionarioAvaliacaoList;
	}

	public void setQuestionarioAvaliacaoList(List<QuestionarioAvaliacao> questionarioAvaliacaoList) {
		this.questionarioAvaliacaoList = questionarioAvaliacaoList;
	}

}
