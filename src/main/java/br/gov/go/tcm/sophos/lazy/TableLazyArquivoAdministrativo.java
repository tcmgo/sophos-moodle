/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.lazy;

import br.gov.go.tcm.sophos.entidade.ArquivoAdministrativo;
import br.gov.go.tcm.sophos.lazy.base.TableLazyPadrao;
import br.gov.go.tcm.sophos.negocio.ArquivoAdministrativoNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class TableLazyArquivoAdministrativo extends TableLazyPadrao<ArquivoAdministrativo> {

	private static final long serialVersionUID = 4527367137678337041L;
	
	@Autowired
	private ArquivoAdministrativoNegocio negocio;

	@Override
	protected ArquivoAdministrativoNegocio getServico() {

		return this.negocio;
	}

}
