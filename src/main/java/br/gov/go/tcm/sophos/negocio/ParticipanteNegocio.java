/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.estrutura.entidade.orcafi.Municipio;
import br.gov.go.tcm.estrutura.entidade.orcafi.Orgao;
import br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato.UsuarioMonitorDAO;
import br.gov.go.tcm.estrutura.persistencia.orcafi.contrato.MunicipioDAO;
import br.gov.go.tcm.estrutura.persistencia.orcafi.contrato.OrgaoDAO;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.*;
import br.gov.go.tcm.sophos.entidade.enumeracao.TipoDominioEnum;
import br.gov.go.tcm.sophos.excecoes.ValidacaoException;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import br.gov.go.tcm.sophos.persistencia.ParticipanteDAO;
import br.gov.go.tcm.sophos.persistencia.PessoaDAO;
import br.gov.go.tcm.sophos.persistencia.UsuarioDAO;
import br.gov.go.tcm.sophos.rh.PessoaVo;
import br.gov.go.tcm.sophos.rh.RHPessoaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class ParticipanteNegocio extends NegocioBase<Participante> {

	private static final long serialVersionUID = -8426728536010364058L;

	@Autowired
	private ParticipanteDAO dao;

	@Autowired
	private OrgaoDAO orgaoDAO;

	@Autowired
	private MunicipioDAO municipioDAO;

	@Autowired
	private PessoaNegocio pessoaNegocio;

	@Autowired
	private EnderecoNegocio enderecoNegocio;

	@Autowired
	private UsuarioNegocio usuarioNegocio;

	@Autowired
	private InscricaoNegocio inscricaoNegocio;

	@Autowired
	private EventoNegocio eventoNegocio;

	@Autowired
	private DominioNegocio dominioNegocio;

	@Autowired
	private RHPessoaDAO RHPessoaDAO;

	@Autowired
	private MunicipioDAO municipioDao;
	
	@Autowired
	private UsuarioMonitorDAO usuarioMonitorDAO;
	
	@Autowired
	private UsuarioDAO usuarioDao;
	
	@Autowired
	private PessoaDAO pessoaDao;

	@Override
	protected ParticipanteDAO getDAO() {

		return this.dao;
	}

	public List<Orgao> listarOrgaosAtivos(Long municipioId) {

		return this.orgaoDAO.obterLstOrgaoPorMunicipio(
				(Municipio) this.municipioDAO.obterEntidadePorId(Municipio.class, municipioId));
	}

	@Override
	protected void executarValidacaoAntesDeSalvar(Participante entidade) {

		Participante participante = null;
		Usuario usuario = null; 
		
		//Servidor do TCM
		if (!StringUtils.isEmpty(entidade.getMatricula())) {

			participante = this.obterPorAtributo("matricula", entidade.getMatricula());
		}

		if (AjudanteObjeto.eReferencia(participante)) {

			if (AjudanteObjeto.eReferencia(entidade.getId())) {

				if (!entidade.getId().equals(participante.getId())) {
					throw new ValidacaoException("Já existe um participante com esta matrícula.");
				}

			} else {
				throw new ValidacaoException("Já existe um participante com esta matrícula.");
			}

		}
		
		//Usuário externo
		if (!StringUtils.isEmpty(entidade.getPessoa().getCpf())) {
			participante = this.obterPorAtributo("pessoa.cpf", entidade.getPessoa().getCpf());
			usuario = this.usuarioDao.obterUsuarioPorCPF(entidade.getPessoa().getCpf());

		}

		if (AjudanteObjeto.eReferencia(participante) && AjudanteObjeto.eReferencia(usuario)) {

			if (AjudanteObjeto.eReferencia(entidade.getId())) {

				if (!entidade.getId().equals(participante.getId())) {
					throw new ValidacaoException("Já existe um participante com este CPF. Para alterações, busque o participante e clique em EDITAR (menu AÇÕES, ícone de lápis).");
				}

			} else {
				throw new ValidacaoException("Já existe um participante com este CPF. Para alterações, busque o participante e clique em EDITAR (menu AÇÕES, ícone de lápis).");
			}

		}

		if (AjudanteObjeto.eReferencia(participante) && AjudanteObjeto.eReferencia(participante.getPessoa())
				&& !AjudanteObjeto.eReferencia(participante.getPessoa().getCpf())) {

			throw new ValidacaoException("Por favor, informe o CPF.");
		}

		participante = this.obterPorAtributo("pessoa.cpf", entidade.getPessoa().getCpf());

		if (AjudanteObjeto.eReferencia(participante)  && (participante.getMatricula() != null)
				&& (!AjudanteObjeto.eReferencia(entidade.getId()) || !entidade.getId().equals(participante.getId()))) {

			throw new ValidacaoException("Já existe um participante com este CPF.");
		}

		if (AjudanteObjeto.eReferencia(participante) && AjudanteObjeto.eReferencia(participante.getPessoa())
				&& !AjudanteObjeto.eReferencia(participante.getPessoa().getNome())) {

			throw new ValidacaoException("Por favor, informe o nome.");
		}

		if (AjudanteObjeto.eReferencia(participante) && AjudanteObjeto.eReferencia(participante.getPessoa())
				&& !AjudanteObjeto.eReferencia(participante.getPessoa().getDataNascimento())) {

			throw new ValidacaoException("Por favor, informe a data de nascimento.");
		}

		if (AjudanteObjeto.eReferencia(participante) && AjudanteObjeto.eReferencia(participante.getPessoa())
				&& !AjudanteObjeto.eReferencia(participante.getPessoa().getSexo())) {

			throw new ValidacaoException("Por favor, informe o sexo.");
		}

		if (AjudanteObjeto.eReferencia(participante) && AjudanteObjeto.eReferencia(participante.getPessoa())
				&& !AjudanteObjeto.eReferencia(participante.getPessoa().getEmail())) {

			throw new ValidacaoException("Por favor, informe o e-mail.");
		}

		if (AjudanteObjeto.eReferencia(participante) && AjudanteObjeto.eReferencia(participante.getPessoa())
				&& !AjudanteObjeto.eReferencia(participante.getPessoa().getTelefone())) {

			throw new ValidacaoException("Por favor, informe o telefone.");
		}

		if (AjudanteObjeto.eReferencia(participante) && !AjudanteObjeto.eReferencia(participante.getTipo())) {

			throw new ValidacaoException("Por favor, informe o tipo.");
		}

		if (AjudanteObjeto.eReferencia(participante) && AjudanteObjeto.eReferencia(participante.getEndereco())
				&& !AjudanteObjeto.eReferencia(participante.getEndereco().getBairro())) {

			throw new ValidacaoException("Por favor, informe o bairro.");
		}

		if (AjudanteObjeto.eReferencia(participante) && AjudanteObjeto.eReferencia(participante.getEndereco())
				&& !AjudanteObjeto.eReferencia(participante.getEndereco().getLogradouro())) {

			throw new ValidacaoException("Por favor, informe o logradouro.");
		}

		if (AjudanteObjeto.eReferencia(participante) && AjudanteObjeto.eReferencia(participante.getEndereco())
				&& !AjudanteObjeto.eReferencia(participante.getEndereco().getCep())) {

			throw new ValidacaoException("Por favor, informe o CEP.");
		}
	}
	
	
	@SuppressWarnings("unchecked")
	private void verificaExistenciaECorrigiUsuarioInternoPorCPF(Participante entidade) {

		String cpf = entidade.getPessoa().getCpf();
		
		List<Pessoa> lstPessoa = (List<Pessoa>) pessoaDao.listarPessoaCpf(cpf);
		
		if (lstPessoa.size() > 1 ) {
		
			Long idPessoaMenor = 0l;
			Long idPessoaMaior = 0l;
			
			for (int i = 0; i < lstPessoa.size(); i++) {
				if (i == 0) {
					idPessoaMenor = lstPessoa.get(i).getId(); 
				}
				
				if (idPessoaMenor > lstPessoa.get(i).getId()) {
					idPessoaMenor = lstPessoa.get(i).getId();
				}
					
				if (idPessoaMaior < lstPessoa.get(i).getId()) {
					idPessoaMaior = lstPessoa.get(i).getId();
				}
							
			}
			
			Pessoa pessoaIdMenor = this.pessoaNegocio.obter(idPessoaMenor);
			
			Pessoa pessoaIdMaior = this.pessoaNegocio.obter(idPessoaMaior);
			
					
			Participante part2 =this.obterPorAtributo("pessoa.id", pessoaIdMaior.getId());
			
			part2.setPessoa(pessoaIdMenor);
			
			super.atualizar(part2);
						
			Usuario usuario = this.usuarioDao.obterPorAtributo("pessoa.id", pessoaIdMaior.getId());
			
			pessoaIdMenor.setCelular(pessoaIdMaior.getCelular());
			pessoaIdMenor.setDataCadastro(pessoaIdMaior.getDataCadastro());
			pessoaIdMenor.setDataNascimento(pessoaIdMaior.getDataNascimento());
			pessoaIdMenor.setEmail(pessoaIdMaior.getEmail());
			pessoaIdMenor.setNome(pessoaIdMaior.getNome());
			pessoaIdMenor.setSexo(pessoaIdMaior.getSexo());
			pessoaIdMenor.setTelefone(pessoaIdMaior.getTelefone());
			this.pessoaDao.atualizar(pessoaIdMenor);
			
			if (AjudanteObjeto.eReferencia(usuario)) {
				usuario.setPessoa(pessoaIdMenor);
				this.usuarioDao.atualizar(usuario);
			}
			
			this.pessoaDao.excluir(pessoaIdMaior);
			
			entidade.setPessoa(pessoaIdMenor);
		}
	}

	@Override
	public void salvar(Participante entidade) {

		this.executarValidacaoAntesDeSalvar(entidade);		
						
		this.pessoaNegocio.salvar(entidade.getPessoa());

		this.enderecoNegocio.salvar(entidade.getEndereco());

		super.salvar(entidade);
		
		this.verificaExistenciaECorrigiUsuarioInternoPorCPF(entidade);
		
	}

	@Override
	protected void executarValidacaoAntesDeExcluir(Participante entidade) {

		super.executarValidacaoAntesDeExcluir(entidade);

		List<Inscricao> inscricaoList = this.inscricaoNegocio.listaPorAtributo("participante", entidade);

		if (AjudanteObjeto.eReferencia(inscricaoList) && inscricaoList.size() > 0) {

			throw new ValidacaoException(
					"Não é possível a exclusão deste participante, pois este já possui inscrição em algum evento.");
		}

		List<Evento> eventoList = this.eventoNegocio.listaPorAtributo("responsavelEvento", entidade);

		if (AjudanteObjeto.eReferencia(eventoList) && eventoList.size() > 0) {

			throw new ValidacaoException(
					"Não é possível a exclusão deste participante, pois este é responsável por algum evento.");
		}
	}

	@Override
	public void excluir(Participante entidade) {

		this.executarValidacaoAntesDeExcluir(entidade);

		Usuario usuario = this.usuarioNegocio.obterPorAtributo("participante", entidade);

		if (AjudanteObjeto.eReferencia(usuario)) {
			usuario.setParticipante(null);
			this.usuarioNegocio.salvarSimples(usuario);
		}

		super.excluir(entidade);
	}

	public boolean PreecherParticipanteBaseRH(Participante participante) {

		PessoaVo pessoaVo = new PessoaVo();
		
		if (AjudanteObjeto.eReferencia(participante.getPessoa().getCpf())) {
			pessoaVo = RHPessoaDAO.obterPorCPF(participante.getPessoa().getCpf().replace(".", "").replace("-", ""));
		} else if (AjudanteObjeto.eReferencia(participante.getMatricula())) {
			pessoaVo = RHPessoaDAO.obterPorMatricula(Long.parseLong(participante.getMatricula()));
		} else {
			pessoaVo = RHPessoaDAO.obterPorEmail(participante.getPessoa().getEmail());
		}

		// Participante é servidor do TCM
		if (AjudanteObjeto.eReferencia(pessoaVo)) {

			participante.getPessoa().setEmail(StringUtils.isEmpty(participante.getPessoa().getEmail())
					? pessoaVo.getEmail() : participante.getPessoa().getEmail());

			participante.getPessoa().setNome(
					StringUtils.isEmpty(pessoaVo.getNome()) ? participante.getPessoa().getNome() : pessoaVo.getNome());

			participante.getPessoa().setCpf(StringUtils.isEmpty(participante.getPessoa().getCpf())
					? formatarCPF(pessoaVo.getCpf()) : participante.getPessoa().getCpf());

			participante.getPessoa().setDataNascimento(pessoaVo.getDataNascimento());

			participante.getPessoa().setCelular(pessoaVo.getCelular());

			participante.setDataAdmissao(pessoaVo.getDataAdmissao());

			participante.getPessoa().setTelefone(pessoaVo.getTelefone());

			if (AjudanteObjeto.eReferencia(pessoaVo.getSexo())) {

				participante.getPessoa()
						.setSexo(pessoaVo.getSexo().equals("F")
								? dominioNegocio.obterDominio(TipoDominioEnum.TipoSexo, 2L)
								: dominioNegocio.obterDominio(TipoDominioEnum.TipoSexo, 1L));

			}

			participante.setCargo(pessoaVo.getCargo());

			participante.setMatricula(
					org.apache.commons.lang.StringUtils.leftPad(pessoaVo.getMatricula().toString(), 6, "0"));

			participante.setTipo(dominioNegocio.obterDominio(TipoDominioEnum.TipoPublicoAlvo, 3L));

			participante.setLotacao(pessoaVo.getLotacao());

			participante.getEndereco().setBairro(pessoaVo.getBairro());

			participante.getEndereco().setCep(pessoaVo.getCep());

			participante.getEndereco().setLogradouro(pessoaVo.getLogradouro());

			participante.getEndereco().setNumero(pessoaVo.getNumeroEndereco());

			participante.getEndereco().setComplemento("Qd." + pessoaVo.getQuadra() + " Lt." + pessoaVo.getLote());

			if (pessoaVo.getEscolaridade().equalsIgnoreCase("Médio ou Equivalente")) {
				participante.setEscolaridade(this.dominioNegocio.obterDominio(TipoDominioEnum.NivelEscolaridade, 2L));
			} else if (pessoaVo.getEscolaridade().equalsIgnoreCase("Superior")) {
				participante.setEscolaridade(this.dominioNegocio.obterDominio(TipoDominioEnum.NivelEscolaridade, 3L));
			} else if (pessoaVo.getEscolaridade().equalsIgnoreCase("Mestrado")) {
				participante.setEscolaridade(this.dominioNegocio.obterDominio(TipoDominioEnum.NivelEscolaridade, 5L));
			} else if (pessoaVo.getEscolaridade().equalsIgnoreCase("Doutorado")) {
				participante.setEscolaridade(this.dominioNegocio.obterDominio(TipoDominioEnum.NivelEscolaridade, 6L));
			}
			
			return true;

		} 
		else {
			// Participante não é servidor do TCM, é usuário externo
			return false;
		}
	}

	public void PreecherParticipanteBaseSOPHOS(Participante participante) {

		Pessoa pessoa = new Pessoa();
		Usuario usuario = new Usuario();
		Participante participanteBD = new Participante();
		
		if (AjudanteObjeto.eReferencia(participante.getPessoa().getCpf())) {
			usuario = usuarioDao.obterUsuarioPorCPF(participante.getPessoa().getCpf());
			pessoa = pessoaDao.obterPorAtributo("cpf", (participante.getPessoa().getCpf()));
			participanteBD = dao.obterPorAtributo("pessoa.id", pessoa.getId());
		}
		
		if (AjudanteObjeto.eReferencia(pessoa)) {
			
			participante.getPessoa().setCpf(StringUtils.isEmpty(participante.getPessoa().getCpf())
					? formatarCPF(pessoa.getCpf()) : participante.getPessoa().getCpf());

			participante.getPessoa().setDataNascimento(pessoa.getDataNascimento());

			participante.getPessoa().setEmail(StringUtils.isEmpty(participante.getPessoa().getEmail())
					? pessoa.getEmail() : participante.getPessoa().getEmail());
		
			participante.getPessoa().setNome(
					StringUtils.isEmpty(participante.getPessoa().getNome()) ? pessoa.getNome() : participante.getPessoa().getNome());

			if (AjudanteObjeto.eReferencia(pessoa.getSexo())) {

				participante.getPessoa()
						.setSexo(pessoa.getSexo().getDescricao().equals("Feminino")
								? dominioNegocio.obterDominio(TipoDominioEnum.TipoSexo, 2L)
								: dominioNegocio.obterDominio(TipoDominioEnum.TipoSexo, 1L));
			}
			
			participante.getPessoa().setCelular(pessoa.getCelular());

			participante.getPessoa().setTelefone(pessoa.getTelefone());
			
			participante.setCargo(participanteBD.getCargo());
			
			participante.setCodigoMunicipio(participanteBD.getCodigoMunicipio());
			
			participante.setCodigoOrgao(participanteBD.getCodigoOrgao());
			
			participante.setLotacao(participanteBD.getLotacao());
			
			participante.setMatricula(participanteBD.getMatricula());
			
			participante.setObservacao(participanteBD.getObservacao());
			
			participante.setProfissao(participanteBD.getProfissao());
			
			participante.getEndereco().setDataCadastro(participanteBD.getEndereco().getDataCadastro());
			participante.getEndereco().setBairro(participanteBD.getEndereco().getBairro());
			participante.getEndereco().setComplemento(participanteBD.getEndereco().getComplemento());
			participante.getEndereco().setLogradouro(participanteBD.getEndereco().getLogradouro());
			participante.getEndereco().setMunicipioId(participanteBD.getEndereco().getMunicipioId());
			participante.getEndereco().setNumero(participanteBD.getEndereco().getNumero());
			participante.getEndereco().setCidade(participanteBD.getEndereco().getCidade());
			participante.getEndereco().setCep(participanteBD.getEndereco().getCep());
			
			if (participanteBD.getEscolaridade().getDescricao().toString().equalsIgnoreCase("Médio ou Equivalente")) {
				participante.setEscolaridade(this.dominioNegocio.obterDominio(TipoDominioEnum.NivelEscolaridade, 2L));
			} else if (participanteBD.getEscolaridade().getDescricao().toString().equalsIgnoreCase("Superior")) {
				participante.setEscolaridade(this.dominioNegocio.obterDominio(TipoDominioEnum.NivelEscolaridade, 3L));
			} else if (participanteBD.getEscolaridade().getDescricao().toString().equalsIgnoreCase("Mestrado")) {
				participante.setEscolaridade(this.dominioNegocio.obterDominio(TipoDominioEnum.NivelEscolaridade, 5L));
			} else if (participanteBD.getEscolaridade().getDescricao().toString().equalsIgnoreCase("Doutorado")) {
				participante.setEscolaridade(this.dominioNegocio.obterDominio(TipoDominioEnum.NivelEscolaridade, 6L));
			}
			
			participante.setFormacaoAcademica(participanteBD.getFormacaoAcademica());
			participante.setDataAdmissao(participanteBD.getDataAdmissao());
			participante.setInstituicao(participanteBD.getInstituicao());
			participante.setPne(participanteBD.getPne());
			participante.setTipoNecessidadeEspecial(participanteBD.getTipoNecessidadeEspecial());
			participante.setOutraNecessidadeEspecial(participanteBD.getOutraNecessidadeEspecial());
			participante.setMandato(participanteBD.getMandato());
			
			if (AjudanteObjeto.eReferencia(usuario)) {
				participante.setTipo(usuario.getTipoUsuario());
			}
		}
	}

	private String formatarCPF(String cpf) {

		String bloco1 = cpf.substring(0, 3);

		String bloco2 = cpf.substring(3, 6);

		String bloco3 = cpf.substring(6, 9);

		String bloco4 = cpf.substring(9, 11);

		return bloco1 + "." + bloco2 + "." + bloco3 + "-" + bloco4;
	}

	public List<String> listarLotacoes() {

		return this.RHPessoaDAO.listarLotacoes();
	}

	public String obterDescricaoMunicipio(Long id) {

		Municipio municipio = (Municipio) municipioDao.obterEntidadePorId(Municipio.class, id);
		return municipio.getDescricao();

	}

}
