/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "modulo_instrutor")
@Banco(nome = Banco.SOPHOS)
public class ModuloInstrutor extends EntidadePadrao{

	private static final long serialVersionUID = 6806214764969172578L;

	@JoinColumn(name = "modulo_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Modulo modulo;
	
	@JoinColumn(name = "instrutor_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Instrutor instrutor;
	
	@JoinColumn(name = "arquivo_id", referencedColumnName = "id")
	@ManyToOne(optional = true)
	private Anexo arquivo;

	public Modulo getModulo() {
		return modulo;
	}

	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}

	public Instrutor getInstrutor() {
		return instrutor;
	}

	public void setInstrutor(Instrutor instrutor) {
		this.instrutor = instrutor;
	}
	
	
	public Anexo getArquivo() {
		return arquivo;
	}

	public void setArquivo(Anexo arquivo) {
		this.arquivo = arquivo;
	}

	@Override
	public boolean equals(Object obj) {
		
		return this.getInstrutor().equals(((ModuloInstrutor) obj).getInstrutor()) &&  this.getModulo().equals(((ModuloInstrutor) obj).getModulo());
	}
	
	@Override
	public int hashCode() {

		return this.getInstrutor().hashCode();
	}
}
