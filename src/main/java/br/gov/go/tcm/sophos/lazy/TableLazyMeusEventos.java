/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.lazy;

import br.gov.go.tcm.sophos.entidade.Inscricao;
import br.gov.go.tcm.sophos.entidade.Usuario;
import br.gov.go.tcm.sophos.entidade.dto.MeuEventoDTO;
import br.gov.go.tcm.sophos.lazy.base.TableLazyDTO;
import br.gov.go.tcm.sophos.negocio.EventoNegocio;
import br.gov.go.tcm.sophos.negocio.InscricaoNegocio;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Scope("session")
public class TableLazyMeusEventos extends LazyDataModel<MeuEventoDTO> {

	private static final long serialVersionUID = -6545514037278206167L;

	@Autowired
	private InscricaoNegocio inscricaoNegocio;
	
	@Autowired
	private EventoNegocio eventoNegocio;
	
	private Usuario usuarioLogado;

	private String consulta = "";

	private List<MeuEventoDTO> lista;

	private Map<String, Object> filters;

	@Override
	public List<MeuEventoDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder, 	Map<String, Object> filters) {

		List<MeuEventoDTO> meusEventos = new ArrayList<MeuEventoDTO>();

		filters = new HashMap<String, Object>();

		filters.put("autorizada", Boolean.TRUE);

		filters.put("participante", this.getUsuarioLogado().getParticipante());

		TableLazyDTO<Inscricao> tableLazyDTO = this.inscricaoNegocio.listarPaginado(first, pageSize, sortField,	sortOrder, filters, this.getConsulta());

		for (Inscricao inscricao : tableLazyDTO.getLista()) {

			meusEventos.add(this.eventoNegocio.montarListaMeusEventos(inscricao, this.getUsuarioLogado().getParticipante()));
		}

		this.setLista(meusEventos);

		this.setRowCount(tableLazyDTO.getQuantidade().intValue());

		this.setPageSize(pageSize);
		
		this.setConsulta("");

		return meusEventos;

	}

	

	public Usuario getUsuarioLogado() {

		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {

		this.usuarioLogado = usuarioLogado;
	}

	public String getConsulta() {
		return consulta;
	}

	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}

	public List<MeuEventoDTO> getLista() {
		return lista;
	}

	public void setLista(List<MeuEventoDTO> lista) {
		this.lista = lista;
	}

	public Map<String, Object> getFilters() {
		return filters;
	}

	public void setFilters(Map<String, Object> filters) {
		this.filters = filters;
	}

}
