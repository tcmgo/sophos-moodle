/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.negocio;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Modulo;
import br.gov.go.tcm.sophos.entidade.Nota;
import br.gov.go.tcm.sophos.entidade.Participante;
import br.gov.go.tcm.sophos.negocio.base.NegocioBase;
import br.gov.go.tcm.sophos.persistencia.NotaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class NotaNegocio extends NegocioBase<Nota> {

	private static final long serialVersionUID = -8426728536010364058L;

	@Autowired
	private NotaDAO dao;
	
	@Autowired
	private ModuloNegocio moduloNegocio;

	@Override
	protected NotaDAO getDAO() {

		return this.dao;
	}

	public Nota obterNotaParticipanteModulo(Modulo modulo, Participante participante) {

		return this.getDAO().obterNotaParticipanteModulo(modulo, participante);
	}

	public void salvar(List<Nota> notaList) {

		for (Nota nota : notaList) {

			super.salvar(nota);
		}
	}

	public BigDecimal obterMediaParticipanteEvento(Participante participante, Evento evento, List<Modulo> modulos) {
		
		BigDecimal somatorio = BigDecimal.valueOf(0);
		
		if(!AjudanteObjeto.eReferencia(modulos)){
			
			modulos = this.moduloNegocio.listaPorAtributo("evento", evento);
		}	
		
		for(Modulo modulo : modulos){
			
			Nota nota = this.obterNotaParticipanteModulo(modulo, participante);
			
			if(AjudanteObjeto.eReferencia(nota)){

				somatorio = somatorio.add(nota.getValor());
			}
		}
		
		return somatorio;
	}
	
	public BigDecimal obterMediaGeralParticipanteEvento(Participante participante, Evento evento, List<Modulo> modulos) {
		
		BigDecimal somatorio = BigDecimal.valueOf(0);
		
		if(!AjudanteObjeto.eReferencia(modulos)){
			
			modulos = this.moduloNegocio.listaPorAtributo("evento", evento);
		}
		
		for(Modulo modulo : modulos){
			
			Nota nota = this.obterNotaParticipanteModulo(modulo, participante);
			
			if(AjudanteObjeto.eReferencia(nota)){

				somatorio = somatorio.add(nota.getValor());
			}
		}
		
		return somatorio.divide(BigDecimal.valueOf(modulos.size()), 2, BigDecimal.ROUND_HALF_UP);
	}
}
