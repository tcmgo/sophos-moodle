/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.entidade;

import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.entidade.base.EntidadePadrao;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity(name = "br.gov.go.tcm.sophos.entidade.Evento")
@Table(name = "evento")
@Banco(nome = Banco.SOPHOS)
public class Evento extends EntidadePadrao {

	private static final long serialVersionUID = -7608918537042861062L;

	@Column(name = "conteudo", length = 2000, nullable = false)
	private String conteudo;

	@Column(name = "data_fim_previsto", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date dataFimPrevisto;

	@Column(name = "data_fim_pre_inscricao")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataFimPreInscricao;

	@Column(name = "data_fim_realizacao")
	@Temporal(TemporalType.DATE)
	private Date dataFimRealizacao;

	@Column(name = "data_inicio_previsto", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date dataInicioPrevisto;

	@Column(name = "data_inicio_pre_inscricao")
	@Temporal(TemporalType.DATE)
	private Date dataInicioPreInscricao;

	@Column(name = "data_inicio_realizacao")
	@Temporal(TemporalType.DATE)
	private Date dataInicioRealizacao;

	@Column(name = "objetivo_especifico", length = 2000)
	private String objetivoEspecifico;

	@Column(name = "objetivo_geral", length = 2000)
	private String objetivoGeral;

	@Column(name = "observacoes", length = 2000)
	private String observacoes;

	@Column(name = "observacoes_publicas", length = 2000)
	private String observacoesPublicas;

	@Column(name = "titulo", nullable = false)
	private String titulo;

	@Column(name = "carga_horaria", nullable = false)
	private BigDecimal cargaHoraria;

	@Column(name = "vagas", nullable = false)
	private Integer vagas;

	@Column(name = "publicado")
	private Boolean publicado;

	@Column(name = "modulo_unico")
	private Boolean moduloUnico;
	
	@Column(name = "mostrar_na_home")
	private Boolean mostrarNaHome = true;

	@Column(name = "resultado_esperado", length = 2000)
	private String resultadoEsperado;

	@Column(name = "permite_pre_inscricao")
	private Boolean permitePreInscricao;

	@Column(name = "permite_certificado")
	private Boolean permiteCertificado;

	@JoinColumn(name = "imagem_id", referencedColumnName = "id")
	@ManyToOne
	private Anexo imagem;

	@JoinColumn(name = "modalidade_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Dominio modalidade;

	@JoinColumn(name = "area_tribunal_id", referencedColumnName = "id")
	@ManyToOne
	private Dominio areaTribunal;

	@JoinColumn(name = "tipo_evento_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Dominio tipoEvento;

	@JoinColumn(name = "eixo_tematico_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Dominio eixoTematico;

	@JoinColumn(name = "provedor_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private ProvedorEvento provedor;

	@JoinColumn(name = "localizacao_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private LocalizacaoEvento localizacao;

	@JoinColumn(name = "responsavel_evento", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Participante responsavelEvento;

	@OneToMany(mappedBy = "evento", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<EventoPublicoAlvo> publicoAlvoList;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "evento")
	private List<Modulo> moduloList;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "evento")
	private List<Inscricao> inscricaoList;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "evento")
	private List<AvaliacaoEficacia> avaliacaoEficaciaList;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "evento")
	private List<Certificado> certificadoList;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "evento")
	private List<Gasto> gastoList;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "evento")
	private List<Material> materialList;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "evento")
	private List<Indicacao> indicacaoList;
	
	@Column(name = "avaliar_por_media")
	private Boolean avaliarPorMedia = true;
	
	@Column(name = "nota_aprovacao")
	private BigDecimal mediaNotaAprovacao = BigDecimal.valueOf(70d);
	
	@Column(name = "frequencia_aprovacao")
	private BigDecimal mediaFrequenciaAprovacao = BigDecimal.valueOf(75d);
	
	@Column(name = "importado")
	private Boolean eventoImportado = false;
	
	@JoinColumn(name = "imagem_certificado_frente_id", referencedColumnName = "id")
	@ManyToOne
	private Anexo imagemCertificadoFrente;
	
	@JoinColumn(name = "imagem_certificado_verso_id", referencedColumnName = "id")
	@ManyToOne
	private Anexo imagemCertificadoVerso;

	@Column(name = "curso_moodle_id")
	private Long cursoMoodleId;
	
	public Anexo getImagem() {
		return imagem;
	}

	public void setImagem(Anexo imagem) {
		this.imagem = imagem;
	}

	public BigDecimal getCargaHoraria() {
		return cargaHoraria;
	}

	public void setCargaHoraria(BigDecimal cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	public Date getDataFimPrevisto() {
		return dataFimPrevisto;
	}

	public void setDataFimPrevisto(Date dataFimPrevisto) {
		this.dataFimPrevisto = dataFimPrevisto;
	}

	public Date getDataFimPreInscricao() {
		return dataFimPreInscricao;
	}

	public void setDataFimPreInscricao(Date dataFimPreInscricao) {
		this.dataFimPreInscricao = dataFimPreInscricao;
	}

	public Date getDataFimRealizacao() {
		return dataFimRealizacao;
	}

	public void setDataFimRealizacao(Date dataFimRealizacao) {
		this.dataFimRealizacao = dataFimRealizacao;
	}

	public Date getDataInicioPrevisto() {
		return dataInicioPrevisto;
	}

	public void setDataInicioPrevisto(Date dataInicioPrevisto) {
		this.dataInicioPrevisto = dataInicioPrevisto;
	}

	public Date getDataInicioPreInscricao() {
		return dataInicioPreInscricao;
	}

	public void setDataInicioPreInscricao(Date dataInicioPreInscricao) {
		this.dataInicioPreInscricao = dataInicioPreInscricao;
	}

	public Date getDataInicioRealizacao() {
		return dataInicioRealizacao;
	}

	public void setDataInicioRealizacao(Date dataInicioRealizacao) {
		this.dataInicioRealizacao = dataInicioRealizacao;
	}

	public String getObjetivoEspecifico() {
		return objetivoEspecifico;
	}

	public void setObjetivoEspecifico(String objetivoEspecifico) {
		this.objetivoEspecifico = objetivoEspecifico;
	}

	public String getObjetivoGeral() {
		return objetivoGeral;
	}

	public void setObjetivoGeral(String objetivoGeral) {
		this.objetivoGeral = objetivoGeral;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public String getObservacoesPublicas() {
		return observacoesPublicas;
	}

	public void setObservacoesPublicas(String observacoesPublicas) {
		this.observacoesPublicas = observacoesPublicas;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Integer getVagas() {
		return vagas;
	}

	public void setVagas(Integer vagas) {
		this.vagas = vagas;
	}

	public Boolean getPublicado() {
		return publicado;
	}

	public void setPublicado(Boolean publicado) {
		this.publicado = publicado;
	}

	public Boolean getModuloUnico() {
		return moduloUnico;
	}

	public void setModuloUnico(Boolean moduloUnico) {
		this.moduloUnico = moduloUnico;
	}

	public String getResultadoEsperado() {
		return resultadoEsperado;
	}

	public void setResultadoEsperado(String resultadoEsperado) {
		this.resultadoEsperado = resultadoEsperado;
	}

	public Boolean getPermitePreInscricao() {
		return permitePreInscricao;
	}

	public void setPermitePreInscricao(Boolean permitePreInscricao) {
		this.permitePreInscricao = permitePreInscricao;
	}

	public Boolean getPermiteCertificado() {
		return permiteCertificado;
	}

	public void setPermiteCertificado(Boolean permiteCertificado) {
		this.permiteCertificado = permiteCertificado;
	}

	public Dominio getModalidade() {
		return modalidade;
	}

	public void setModalidade(Dominio modalidade) {
		this.modalidade = modalidade;
	}

	public Dominio getAreaTribunal() {
		return areaTribunal;
	}

	public void setAreaTribunal(Dominio areaTribunal) {
		this.areaTribunal = areaTribunal;
	}

	public Dominio getTipoEvento() {
		return tipoEvento;
	}

	public void setTipoEvento(Dominio tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public Dominio getEixoTematico() {
		return eixoTematico;
	}

	public void setEixoTematico(Dominio eixoTematico) {
		this.eixoTematico = eixoTematico;
	}

	public ProvedorEvento getProvedor() {
		return provedor;
	}

	public void setProvedor(ProvedorEvento provedor) {
		
		if(AjudanteObjeto.eReferencia(provedor)){
			this.provedor = provedor;
		}
		
	}

	public LocalizacaoEvento getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(LocalizacaoEvento localizacao) {
		
		if(AjudanteObjeto.eReferencia(localizacao)){
			this.localizacao = localizacao; 
		}
		
	}

	public Participante getResponsavelEvento() {
		return responsavelEvento;
	}

	public void setResponsavelEvento(Participante responsavelEvento) {
		
		if(AjudanteObjeto.eReferencia(responsavelEvento)){
			this.responsavelEvento = responsavelEvento;
		}
		
	}

	public List<EventoPublicoAlvo> getPublicoAlvoList() {
		return publicoAlvoList;
	}

	public void setPublicoAlvoList(List<EventoPublicoAlvo> publicoAlvoList) {
		this.publicoAlvoList = publicoAlvoList;
	}

	public List<Modulo> getModuloList() {
		return moduloList;
	}

	public void setModuloList(List<Modulo> moduloList) {
		this.moduloList = moduloList;
	}

	public List<Inscricao> getInscricaoList() {
		return inscricaoList;
	}

	public void setInscricaoList(List<Inscricao> inscricaoList) {
		this.inscricaoList = inscricaoList;
	}

	public List<AvaliacaoEficacia> getAvaliacaoEficaciaList() {
		return avaliacaoEficaciaList;
	}

	public void setAvaliacaoEficaciaList(List<AvaliacaoEficacia> avaliacaoEficaciaList) {
		this.avaliacaoEficaciaList = avaliacaoEficaciaList;
	}

	public List<Certificado> getCertificadoList() {
		return certificadoList;
	}

	public void setCertificadoList(List<Certificado> certificadoList) {
		this.certificadoList = certificadoList;
	}

	public List<Gasto> getGastoList() {
		return gastoList;
	}

	public void setGastoList(List<Gasto> gastoList) {
		this.gastoList = gastoList;
	}

	public List<Material> getMaterialList() {
		return materialList;
	}

	public void setMaterialList(List<Material> materialList) {
		this.materialList = materialList;
	}
	
	public List<Indicacao> getIndicacaoList() {
		return indicacaoList;
	}

	public void setIndicacaoList(List<Indicacao> indicacaoList) {
		this.indicacaoList = indicacaoList;
	}

	public Boolean getMostrarNaHome() {
		return mostrarNaHome;
	}
	
	public Boolean getNaoMostrarNaHome() {
		return !mostrarNaHome;
	}

	public void setMostrarNaHome(Boolean mostrarNaHome) {
		this.mostrarNaHome = mostrarNaHome;
	}

	public Boolean getAvaliarPorMedia() {
		return avaliarPorMedia;
	}

	public void setAvaliarPorMedia(Boolean avaliarPorMedia) {
		this.avaliarPorMedia = avaliarPorMedia;
	}

	public BigDecimal getMediaNotaAprovacao() {
		return mediaNotaAprovacao;
	}

	public void setMediaNotaAprovacao(BigDecimal mediaNotaAprovacao) {
		this.mediaNotaAprovacao = mediaNotaAprovacao;
	}

	public BigDecimal getMediaFrequenciaAprovacao() {
		return mediaFrequenciaAprovacao;
	}

	public void setMediaFrequenciaAprovacao(BigDecimal mediaFrequenciaAprovacao) {
		this.mediaFrequenciaAprovacao = mediaFrequenciaAprovacao;
	}

	public Boolean getEventoImportado() {
		return eventoImportado;
	}

	public void setEventoImportado(Boolean eventoImportado) {
		this.eventoImportado = eventoImportado;
	}

	public Anexo getImagemCertificadoFrente() {
		return imagemCertificadoFrente;
	}

	public void setImagemCertificadoFrente(Anexo imagemCertificadoFrente) {
		this.imagemCertificadoFrente = imagemCertificadoFrente;
	}

	public Anexo getImagemCertificadoVerso() {
		return imagemCertificadoVerso;
	}

	public void setImagemCertificadoVerso(Anexo imagemCertificadoVerso) {
		this.imagemCertificadoVerso = imagemCertificadoVerso;
	}

	public Long getCursoMoodleId() {
		return cursoMoodleId;
	}

	public void setCursoMoodleId(Long cursoMoodleId) {
		this.cursoMoodleId = cursoMoodleId;
	}
}
