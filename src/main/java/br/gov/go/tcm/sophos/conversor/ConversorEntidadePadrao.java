/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.conversor;

import br.gov.go.tcm.estrutura.container.faces.AjudanteContextoFaces;
import br.gov.go.tcm.estrutura.entidade.base.Entidade;
import br.gov.go.tcm.estrutura.entidade.base.EntidadePadrao;
import br.gov.go.tcm.sophos.persistencia.ConversorDAO;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.servlet.ServletContext;

@Component
@Scope("request")
public class ConversorEntidadePadrao implements Converter {

	@Override
	@SuppressWarnings("unchecked")
	public Object getAsObject(final FacesContext context, final UIComponent component, final String value) {

		if (value != null && value.matches("[0-9].*")) {

			final ValueExpression ve = component.getValueExpression("value");

			final Class<? extends Entidade> entidade = (Class<? extends Entidade>) ve.getType(context.getELContext());

			return this.getDao().obterEntidadePorId(entidade, Long.parseLong(value));

		} else {

			return null;
		}
	}

	@Override
	public String getAsString(final FacesContext context, final UIComponent component, final Object value) {

		if (value != null) {

			return String.valueOf(( (EntidadePadrao) value ).getId());
		}

		return null;
	}

	public ConversorDAO getDao() {

		return WebApplicationContextUtils.getWebApplicationContext((ServletContext) AjudanteContextoFaces.getFacesContext().getExternalContext().getContext())
				.getBean(ConversorDAO.class);
	}
}
