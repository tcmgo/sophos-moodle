/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.importador.base;

public class ResultadoImportador {
	
	private int eventosImportados = 0;
	
	private int inscricoesImportadas = 0;
	
	private int notasLancadas = 0;
	
	private int frequenciasLancadas = 0;
	
	private int certificadosGerados = 0;

	public void iterarEventosImportados() {
		this.eventosImportados++;
	}

	public void iterarInscricoesImportadas() {
		this.inscricoesImportadas++;
	}
	
	public void iterarNotasLancadas() {
		this.notasLancadas++;
	}
	
	public void iterarCertificadosGerados() {
		this.certificadosGerados++;
	}

	public void iterarFrequenciasLancadas() {
		this.frequenciasLancadas++;
	}

	public int getEventosImportados() {
		return eventosImportados;
	}

	public int getInscricoesImportadas() {
		return inscricoesImportadas;
	}

	public int getNotasLancadas() {
		return notasLancadas;
	}

	public int getFrequenciasLancadas() {
		return frequenciasLancadas;
	}

	public int getCertificadosGerados() {
		return certificadosGerados;
	}
	
	

}
