/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.rh;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.ResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Repository
public class RHPessoaDAOImpl extends HibernateDaoSupport implements RHPessoaDAO, Serializable {

	private static final long serialVersionUID = -4929437466790304446L;

	@Autowired
	public RHPessoaDAOImpl(@Qualifier("sessionFactoryOracleRH") final SessionFactory factory, @Qualifier("hibernateTemplateOracleRH") final HibernateTemplate hibernateTemplate) {

		this.setHibernateTemplate(hibernateTemplate);

		this.setSessionFactory(factory);
		
	}
	
	@Override
	public PessoaVo obterPorCPF(String cpf) {
		
		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactory());

		final Session session = this.getSession(hibernateTemplate);
		
		try {

			Query query = session.createSQLQuery("SELECT * FROM USISTEMA.REL_INFO_TCMGO WHERE CPF = :cpf and ROWNUM <= 1 and status = 'Normal'");
			
			query.setParameter("cpf", cpf);

			query.setResultTransformer(new PessoaResultTransformer());

			PessoaVo pessoaVo = (PessoaVo) query.uniqueResult();
			
			return pessoaVo;

		} finally {

			session.close();
		}
	}
	
	@Override
	public PessoaVo obterPorMatricula(Long matricula) {
		
		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactory());

		final Session session = this.getSession(hibernateTemplate);
		
		try {

			Query query = session.createSQLQuery("SELECT * FROM USISTEMA.REL_INFO_TCMGO WHERE MATRICULA = :matricula and ROWNUM <= 1");
			
			query.setParameter("matricula", matricula);

			query.setResultTransformer(new PessoaResultTransformer());

			PessoaVo pessoaVo = (PessoaVo) query.uniqueResult();
			
			return pessoaVo;

		} finally {

			session.close();
		}
	}

	@Override
	public PessoaVo obterPorEmail(String email) {
		
		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactory());

		final Session session = this.getSession(hibernateTemplate);
		
		try {

			Query query = session.createSQLQuery("SELECT * FROM USISTEMA.REL_INFO_TCMGO WHERE END_EMAIL = :email and ROWNUM <= 1");
			
			query.setParameter("email", email);

			query.setResultTransformer(new PessoaResultTransformer());

			PessoaVo pessoaVo = (PessoaVo) query.uniqueResult();
			
			return pessoaVo;

		} finally {

			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> listarLotacoes() {
		
		final HibernateTemplate hibernateTemplate = this.getHibernateTemplate(this.getSessionFactory());

		final Session session = this.getSession(hibernateTemplate);
		
		try {

			Query query = session.createSQLQuery("SELECT DISTINCT LOTACAO FROM USISTEMA.REL_INFO_TCMGO WHERE STATUS = 'Normal' ORDER BY LOTACAO");
			
			return query.list();

		} finally {

			session.close();
		}
	}
	
	
	private class PessoaResultTransformer implements ResultTransformer{
		
		private static final long serialVersionUID = 8062532218734561965L;

		@Override
		public Object transformTuple(Object[] tuple, String[] aliases) {
			
			PessoaVo pessoaVo = new PessoaVo();
			
			pessoaVo.setNome(converteParaString(tuple[0]));
			pessoaVo.setCpf(converteParaString(tuple[1]));
			pessoaVo.setDataNascimento(converterParaData(tuple[2]));
			pessoaVo.setSexo(converteParaString(tuple[3]));
			pessoaVo.setEmail(converteParaString(tuple[4]));
			pessoaVo.setTelefone(converteParaString(tuple[5]));
			pessoaVo.setCelular(converteParaString(tuple[6]));
			pessoaVo.setDataAdmissao(converterParaData(tuple[7]));
			pessoaVo.setLogradouro(converteParaString(tuple[8]));
			pessoaVo.setCep(converteParaString(tuple[9]));
			pessoaVo.setMunicipio(converteParaString(tuple[10]));
			pessoaVo.setEstado(converteParaString(tuple[11]));
			pessoaVo.setBairro(converteParaString(tuple[12]));
			pessoaVo.setQuadra(converteParaString(tuple[13]));
			pessoaVo.setLote(converteParaString(tuple[14]));
			pessoaVo.setNumeroEndereco(converteParaString(tuple[15]));
			pessoaVo.setLotacao(converteParaString(tuple[16]));
			pessoaVo.setCargo(converteParaString(tuple[17]));
			pessoaVo.setEscolaridade(converteParaString(tuple[18]));
			pessoaVo.setFormacao(converteParaString(tuple[19]));
			pessoaVo.setMatricula(converterParaInt(tuple[20]));
			
			return pessoaVo;
			
		}

		private Integer converterParaInt(Object tuple) {

			return tuple != null ? ((Number)tuple).intValue() : null;
		}

		private Date converterParaData(Object tuple) {

			return tuple != null ? (Date) tuple : null;
		}

		private String converteParaString(Object tuple) {
			
			return tuple != null ? tuple.toString() : null;
		}
		
		@SuppressWarnings("rawtypes")
		@Override
		public List transformList(List collection) {

			return collection;
		}
	}
	
	public HibernateTemplate getHibernateTemplate(SessionFactory sessionFactory) {
		if(getHibernateTemplate() == null){
			setHibernateTemplate(new HibernateTemplate());
			getHibernateTemplate().setSessionFactory(sessionFactory);
		}
		return super.getHibernateTemplate();
	}
	
	public Session getSession(HibernateTemplate hibernateTemplate){
		if(getSession() == null){
			return getSession(hibernateTemplate);
		}
		return getSession();
	}

}
