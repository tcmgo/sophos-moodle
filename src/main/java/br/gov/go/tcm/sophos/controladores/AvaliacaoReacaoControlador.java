/*******************************************************************************
 * TRIBUNAL DE CONTAS DOS MUNICÍPIOS DO ESTADO DE GOIÁS - TCM-GO
 * 
 * O Sistema de Gestão Educacional SOPHOS foi desenvolvido e é mantido pela Superintendência de Informática (SINFO)
 * do Tribunal de Contas dos Municípios do Estado de Goiás (TCM-GO), órgão da estrutura administrativa do Estado de 
 * Goiás que detém seus direitos de uso, aperfeiçoamento e distribuição. Os direitos de uso, aperfeiçoamento e acesso 
 * ao código-fonte do SOPHOS podem ser estendidos a outras pessoas físicas ou jurídicas via termos de cooperação técnica 
 * e instrumentos congêneres, ficando a parte receptora proibida de distribuí-lo, sob quaisquer formas, sem expressa permissão 
 * do TCM-GO. 
 * 
 * Este cabeçalho deve ser mantido.
 * 
 * Copyright (C) 2017
 ******************************************************************************/
package br.gov.go.tcm.sophos.controladores;

import br.gov.go.tcm.sophos.ajudante.AjudanteObjeto;
import br.gov.go.tcm.sophos.controladores.base.ControladorBaseCRUD;
import br.gov.go.tcm.sophos.entidade.Avaliacao;
import br.gov.go.tcm.sophos.entidade.Evento;
import br.gov.go.tcm.sophos.entidade.Modulo;
import br.gov.go.tcm.sophos.entidade.dto.ModuloAvaliacaoChartDTO;
import br.gov.go.tcm.sophos.entidade.dto.OpcaoQuestionarioChartDTO;
import br.gov.go.tcm.sophos.entidade.dto.QuestionarioAvaliacaoChartDTO;
import br.gov.go.tcm.sophos.lazy.base.TableLazyPadrao;
import br.gov.go.tcm.sophos.negocio.AvaliacaoNegocio;
import br.gov.go.tcm.sophos.negocio.ModuloNegocio;
import org.primefaces.model.chart.PieChartModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Scope("session")
public class AvaliacaoReacaoControlador extends ControladorBaseCRUD<Avaliacao, AvaliacaoNegocio> {

	private static final long serialVersionUID = 2278835461856785040L;

	@Autowired
	private AvaliacaoNegocio negocio;
	
	@Autowired
	private ModuloNegocio moduloNegocio;
	
	private List<PieChartModel> avaliacoesChart;
	
	private ModuloAvaliacaoChartDTO moduloAvaliacaoChartDTO;
	
	private List<String> observacoesList;
	
	private List<Modulo> moduloList;
	
	private Modulo moduloSelecionado;

	private Evento evento;
	
	private Integer tipo = 0;
	
	public void atualizarGrafico(){
		
		if(AjudanteObjeto.eReferencia(evento) && AjudanteObjeto.eReferencia(evento.getId())){
			
			if(AjudanteObjeto.eReferencia(this.getModuloSelecionado()) && AjudanteObjeto.eReferencia(this.getModuloSelecionado().getId())){
				
				List<Avaliacao> avaliacoesModulo = this.negocio.listaPorAtributo("modulo", this.getModuloSelecionado());
				
				observacoesList = new ArrayList<String>();
				
				moduloAvaliacaoChartDTO = new ModuloAvaliacaoChartDTO(this.getModuloSelecionado().getTitulo(), this.getModuloSelecionado().getId());
				
				this.negocio.gerarListaDeAvalicoes(avaliacoesModulo, observacoesList, moduloAvaliacaoChartDTO);
				
			}else{
				
				List<Avaliacao> avaliacoesEvento = new ArrayList<Avaliacao>();
				
				List<Modulo> modulos = this.moduloNegocio.listaPorAtributo("evento", evento);
				
				for(Modulo modulo: modulos){
					
					avaliacoesEvento.addAll(this.negocio.listaPorAtributo("modulo", modulo));
				}
				
				observacoesList = new ArrayList<String>();
				
				moduloAvaliacaoChartDTO = new ModuloAvaliacaoChartDTO(evento.getTitulo(), evento.getId());
				
				this.negocio.gerarListaDeAvalicoes(avaliacoesEvento, observacoesList, moduloAvaliacaoChartDTO);
				
			}
			
			avaliacoesChart = new ArrayList<PieChartModel>();
			
			for(QuestionarioAvaliacaoChartDTO questionarioAvaliacaoChartDTO : moduloAvaliacaoChartDTO.getQuestionarios()){
				
				PieChartModel chart = new PieChartModel();
				
				chart.setTitle(questionarioAvaliacaoChartDTO.getQuestionario());
				
				chart.setLegendPosition("w");
				 
				for(OpcaoQuestionarioChartDTO opcaoQuestionarioChartDTO : questionarioAvaliacaoChartDTO.getOpcoes()){
					
					chart.set(opcaoQuestionarioChartDTO.getDescricao(), opcaoQuestionarioChartDTO.getQuantidade());
				}
				
				avaliacoesChart.add(chart);
			}
		}
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public ModuloAvaliacaoChartDTO getModuloAvaliacaoChartDTO() {
		return moduloAvaliacaoChartDTO;
	}

	public void setModuloAvaliacaoChartDTO(ModuloAvaliacaoChartDTO moduloAvaliacaoChartDTO) {
		this.moduloAvaliacaoChartDTO = moduloAvaliacaoChartDTO;
	}

	public List<PieChartModel> getAvaliacoesChart() {
		return avaliacoesChart;
	}

	public void setAvaliacoesChart(List<PieChartModel> avaliacoesChart) {
		this.avaliacoesChart = avaliacoesChart;
	}
	
	public List<String> getObservacoesList() {
		return observacoesList;
	}

	public void setObservacoesList(List<String> observacoesList) {
		this.observacoesList = observacoesList;
	}

	public List<Modulo> getModuloList() {
		if(AjudanteObjeto.eReferencia(this.evento)){
			moduloList = this.moduloNegocio.listaPorAtributo("evento", evento);
		}
		return moduloList;
	}

	public void setModuloList(List<Modulo> moduloList) {
		this.moduloList = moduloList;
	}

	public Modulo getModuloSelecionado() {
		return moduloSelecionado;
	}

	public void setModuloSelecionado(Modulo moduloSelecionado) {
		this.moduloSelecionado = moduloSelecionado;
	}

	@Override
	public TableLazyPadrao<Avaliacao> getLista() {
		return null;
	}

	@Override
	protected AvaliacaoNegocio getNegocio() {
		return this.negocio;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		if(AjudanteObjeto.eReferencia(evento) && AjudanteObjeto.eReferencia(evento.getId())){
			this.evento = evento;
		}
	}

}
