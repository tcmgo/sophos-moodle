<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="municipios-participantes-ausentes" pageWidth="595" pageHeight="842" whenNoDataType="AllSectionsNoDetail" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="0" bottomMargin="20" uuid="3f87487f-3c64-48bb-8caa-6cbe363dbd72">
	<property name="ireport.zoom" value="2.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="table">
		<box>
			<pen lineWidth="1.0" lineColor="#000000"/>
		</box>
	</style>
	<style name="table_TH" mode="Opaque" backcolor="#D9F2F1">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<style name="table_CH" mode="Opaque" backcolor="#BFE1FF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<style name="table_TD" mode="Opaque" backcolor="#FFFFFF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<subDataset name="dsTituloEventos" uuid="62e59c2f-dee7-4075-b356-89aeb87e597b">
		<parameter name="EVENTOS" class="java.lang.String"/>
		<parameter name="FILTRAR_FREQUENCIA" class="java.lang.Long"/>
		<parameter name="ID_EVENTO" class="java.lang.Long"/>
		<parameter name="APROVADOS" class="java.lang.Long"/>
		<queryString>
			<![CDATA[declare @evento_id int
set @evento_id = $P{ID_EVENTO}

select distinct titulo from dbo.evento where id = @evento_id]]>
		</queryString>
		<field name="titulo" class="java.lang.String"/>
	</subDataset>
	<subDataset name="dsMunicipiosPresentes" uuid="2ed68e46-bd8e-471c-98a5-2387d3aaa687">
		<parameter name="EVENTOS" class="java.lang.String"/>
		<parameter name="FILTRAR_FREQUENCIA" class="java.lang.Long"/>
		<parameter name="DATA_INICIO" class="java.util.Date"/>
		<parameter name="DATA_FIM" class="java.util.Date"/>
		<parameter name="ID_EVENTO" class="java.lang.Long"/>
		<parameter name="APROVADOS" class="java.lang.Long"/>
		<queryString>
			<![CDATA[declare @evento_id int
declare @aprovados int
declare @data_inicio datetime
declare @data_fim datetime

set @evento_id = $P{ID_EVENTO}
set @aprovados = $P{APROVADOS}
set @data_inicio = $P{DATA_INICIO}
set @data_fim = $P{DATA_FIM}

declare @participanteFrequencia TABLE(
	id INT NOT NULL,
	nome varchar(255) NOT NULL,
	publico_alvo_id INT NOT NULL,
	avaliar_media_geral INT NOT NULL,
	frequencia_aprovacao INT NOT NULL,
	quantidade_encontros INT NOT NULL,
	presencas INT NOT NULL,
	percentual_presencas INT NOT NULL,
	media FLOAT NOT NULL,
	media_aprovacao FLOAT NOT NULL
)

INSERT INTO @participanteFrequencia
	select i.id, pe.nome, pa.publico_alvo_id,
	    e.avaliar_por_media as avaliar_media_geral,
		(CASE e.avaliar_por_media
			WHEN 0 THEN ISNULL((SELECT
					CASE (select count(*)
						 from dbo.modulo m
							inner join dbo.evento e on m.evento_id = e.id
						 where e.id = @evento_id)
						WHEN (select count(*)
							from dbo.frequencia f
								inner join dbo.modulo m on f.modulo_id = m.id
								inner join dbo.evento e on m.evento_id = e.id
								inner join dbo.participante p on f.participante_id = p.id
							where e.id =  @evento_id
								and p.id = pa.id
								and f.presenca = 1)
						THEN -1  -- APROVADO (PRESENÇAS SATISFATÓRIAS EM TODOS MÓDULOS), PERCENTUAL DE PRESENÇAS SEMPRE SERÁ MAIOR QUE -1
						ELSE 101 -- REPROVADO (PRESENÇA INSATISFATÓRIA EM ALGUM MÓDULO), PERCENTUAL DE PRESENÇAS NUNCA SERÁ MAIOR QUE 101
					END), 70)
			WHEN 1 THEN ISNULL(e.frequencia_aprovacao, 75)
		END) as frequencia_aprovacao,
		(select SUM(mo.quantidade_encontros) from dbo.modulo as mo where mo.evento_id = @evento_id) as quantidade_encontros,
		ISNULL(((select SUM(fe.presenca) from dbo.frequencia as fe left join dbo.modulo as mo on fe.modulo_id = mo.id where fe.participante_id = pa.id and mo.evento_id = @evento_id )), 0) as presencas,
		ISNULL(((select SUM(fe.presenca) from dbo.frequencia as fe left join dbo.modulo as mo on fe.modulo_id = mo.id where fe.participante_id = pa.id and mo.evento_id = @evento_id ) * 100 / (select SUM(mo.quantidade_encontros) from dbo.modulo as mo where mo.evento_id = @evento_id)), 0) as percentual_presencas,
		ISNULL((select avg(nota_media.valor) from dbo.nota as nota_media
			inner join dbo.modulo as mo_media on nota_media.modulo_id = mo_media.id
			inner join dbo.evento as ev_media on mo_media.evento_id = ev_media.id
			left join dbo.participante as pa_media on nota_media.participante_id = pa_media.id
			left join dbo.inscricao as i_media on i_media.participante_id = pa_media.id
			where ev_media.id = i_media.evento_id and nota_media.participante_id = pa_media.id and ev_media.id = @evento_id and pa_media.id = pa.id), 0) as media,
			(CASE e.avaliar_por_media
				WHEN 0 THEN ISNULL((SELECT
					CASE (select count(*)
						 from dbo.modulo m
							inner join dbo.evento e on m.evento_id = e.id
						 where e.id = @evento_id )
						WHEN (select count(*)
							from dbo.nota n
								inner join dbo.modulo m on n.modulo_id = m.id
								inner join dbo.evento e on m.evento_id = e.id
								inner join dbo.participante p on n.participante_id = p.id
							where e.id = @evento_id
								and p.id = pa.id
								and n.valor >= m.nota_aprovacao)
						THEN -1  -- APROVADO (NOTA SATISFATÓRIO EM TODOS MÓDULOS), NOTA SEMPRE SERÁ MAIOR QUE -1
						ELSE 101 -- REPROVADO (NOTA INSATISFATÓRIA EM ALGUM MÓDULO), NOTA NUNCA SERÁ MAIOR QUE 101
					END), 70)
				WHEN 1 THEN ISNULL(e.nota_aprovacao, 70)
			END) as media_aprovacao
	from dbo.participante as pa
		inner join dbo.dominio as d on pa.publico_alvo_id = d.id
		left join dbo.inscricao as i on i.participante_id = pa.id
		left join dbo.pessoa as pe on pa.pessoa_id = pe.id
		left join dbo.evento as e on i.evento_id = e.id
		where ((@evento_id is null) or (i.evento_id = @evento_id))
		    and ((@data_inicio is null and @data_fim is null) or ((e.data_inicio_realizacao >= @data_inicio and e.data_fim_realizacao <= @data_fim) or (e.data_inicio_realizacao is null) or (e.data_fim_realizacao is null)))
			and (i.autorizada = 1);


   select distinct municipio.codMunicipio, municipio.nomeMunicipio,
	(select count(*) from dbo.inscricao as i
		inner join dbo.participante as p on i.participante_id = p.id
		inner join dbo.endereco as e on p.endereco_id = e.id
		inner join @participanteFrequencia as pf on i.id = pf.id
		where ((@evento_id is null) or (i.evento_id in (@evento_id)))
		and e.municipio_id is not null
		and e.municipio_id = municipio.codMunicipio
		and ((@aprovados is null or @aprovados = 0) or (pf.percentual_presencas >= pf.frequencia_aprovacao and pf.media >= pf.media_aprovacao))) as quantidadeParticipacoes
	from ORCAFI..Municipio as municipio
	order by municipio.nomeMunicipio;]]>
		</queryString>
		<field name="codMunicipio" class="java.lang.Integer"/>
		<field name="nomeMunicipio" class="java.lang.String"/>
		<field name="quantidadeParticipacoes" class="java.lang.Integer"/>
	</subDataset>
	<subDataset name="dsTotalizador" uuid="992d3ca3-b232-4784-a91b-a0ae42fb9c93">
		<parameter name="EVENTOS" class="java.lang.String"/>
		<parameter name="FILTRAR_FREQUENCIA" class="java.lang.Long"/>
		<parameter name="DATA_INICIO" class="java.util.Date"/>
		<parameter name="DATA_FIM" class="java.util.Date"/>
		<parameter name="ID_EVENTO" class="java.lang.Long"/>
		<parameter name="APROVADOS" class="java.lang.Long"/>
		<queryString>
			<![CDATA[declare @evento_id int
declare @aprovados int
declare @data_inicio datetime
declare @data_fim datetime

set @evento_id = $P{ID_EVENTO}
set @aprovados = $P{APROVADOS}
set @data_inicio = $P{DATA_INICIO}
set @data_fim = $P{DATA_FIM}

declare @participanteFrequencia TABLE(
	id INT NOT NULL,
	nome varchar(255) NOT NULL,
	publico_alvo_id INT NOT NULL,
	avaliar_media_geral INT NOT NULL,
	frequencia_aprovacao INT NOT NULL,
	quantidade_encontros INT NOT NULL,
	presencas INT NOT NULL,
	percentual_presencas INT NOT NULL,
	media FLOAT NOT NULL,
	media_aprovacao FLOAT NOT NULL
)


INSERT INTO @participanteFrequencia
	select i.id, pe.nome, pa.publico_alvo_id,
	    e.avaliar_por_media as avaliar_media_geral,
		(CASE e.avaliar_por_media
			WHEN 0 THEN ISNULL((SELECT
					CASE (select count(*)
						 from dbo.modulo m
							inner join dbo.evento e on m.evento_id = e.id
						 where e.id = @evento_id)
						WHEN (select count(*)
							from dbo.frequencia f
								inner join dbo.modulo m on f.modulo_id = m.id
								inner join dbo.evento e on m.evento_id = e.id
								inner join dbo.participante p on f.participante_id = p.id
							where e.id =  @evento_id
								and p.id = pa.id
								and f.presenca = 1)
						THEN -1  -- APROVADO (PRESENÇAS SATISFATÓRIAS EM TODOS MÓDULOS), PERCENTUAL DE PRESENÇAS SEMPRE SERÁ MAIOR QUE -1
						ELSE 101 -- REPROVADO (PRESENÇA INSATISFATÓRIA EM ALGUM MÓDULO), PERCENTUAL DE PRESENÇAS NUNCA SERÁ MAIOR QUE 101
					END), 70)
			WHEN 1 THEN ISNULL(e.frequencia_aprovacao, 75)
		END) as frequencia_aprovacao,
		(select SUM(mo.quantidade_encontros) from dbo.modulo as mo where mo.evento_id = @evento_id) as quantidade_encontros,
		ISNULL(((select SUM(fe.presenca) from dbo.frequencia as fe left join dbo.modulo as mo on fe.modulo_id = mo.id where fe.participante_id = pa.id and mo.evento_id = @evento_id )), 0) as presencas,
		ISNULL(((select SUM(fe.presenca) from dbo.frequencia as fe left join dbo.modulo as mo on fe.modulo_id = mo.id where fe.participante_id = pa.id and mo.evento_id = @evento_id ) * 100 / (select SUM(mo.quantidade_encontros) from dbo.modulo as mo where mo.evento_id = @evento_id)), 0) as percentual_presencas,
		ISNULL((select avg(nota_media.valor) from dbo.nota as nota_media
			inner join dbo.modulo as mo_media on nota_media.modulo_id = mo_media.id
			inner join dbo.evento as ev_media on mo_media.evento_id = ev_media.id
			left join dbo.participante as pa_media on nota_media.participante_id = pa_media.id
			left join dbo.inscricao as i_media on i_media.participante_id = pa_media.id
			where ev_media.id = i_media.evento_id and nota_media.participante_id = pa_media.id and ev_media.id = @evento_id and pa_media.id = pa.id), 0) as media,
			(CASE e.avaliar_por_media
				WHEN 0 THEN ISNULL((SELECT
					CASE (select count(*)
						 from dbo.modulo m
							inner join dbo.evento e on m.evento_id = e.id
						 where e.id = @evento_id )
						WHEN (select count(*)
							from dbo.nota n
								inner join dbo.modulo m on n.modulo_id = m.id
								inner join dbo.evento e on m.evento_id = e.id
								inner join dbo.participante p on n.participante_id = p.id
							where e.id = @evento_id
								and p.id = pa.id
								and n.valor >= m.nota_aprovacao)
						THEN -1  -- APROVADO (NOTA SATISFATÓRIO EM TODOS MÓDULOS), NOTA SEMPRE SERÁ MAIOR QUE -1
						ELSE 101 -- REPROVADO (NOTA INSATISFATÓRIA EM ALGUM MÓDULO), NOTA NUNCA SERÁ MAIOR QUE 101
					END), 70)
				WHEN 1 THEN ISNULL(e.nota_aprovacao, 70)
			END) as media_aprovacao
	from dbo.participante as pa
		inner join dbo.dominio as d on pa.publico_alvo_id = d.id
		left join dbo.inscricao as i on i.participante_id = pa.id
		left join dbo.pessoa as pe on pa.pessoa_id = pe.id
		left join dbo.evento as e on i.evento_id = e.id
		where ((@evento_id is null) or (i.evento_id = @evento_id))
		    and ((@data_inicio is null and @data_fim is null) or ((e.data_inicio_realizacao >= @data_inicio and e.data_fim_realizacao <= @data_fim) or (e.data_inicio_realizacao is null) or (e.data_fim_realizacao is null)))
			and (i.autorizada = 1);


select (select count(*) from ORCAFI..Municipio) as totalMunicipio,
	count(*) as quantidadePresente from (select distinct e.municipio_id from dbo.inscricao as i
							inner join dbo.participante as p on i.participante_id = p.id
							inner join dbo.endereco as e on p.endereco_id = e.id
							inner join @participanteFrequencia as pf on i.id = pf.id
							where ((@evento_id is null) or (i.evento_id in (@evento_id)))
							and e.municipio_id is not null
							and ((@aprovados is null or @aprovados = 0) or (pf.percentual_presencas >= pf.frequencia_aprovacao and pf.media >= pf.media_aprovacao))) as dt]]>
		</queryString>
		<field name="totalMunicipio" class="java.lang.Integer"/>
		<field name="quantidadePresente" class="java.lang.Integer"/>
	</subDataset>
	<parameter name="TITULO_RELATORIO" class="java.lang.String"/>
	<parameter name="EVENTOS" class="java.lang.String"/>
	<parameter name="FILTRAR_FREQUENCIA" class="java.lang.Long"/>
	<parameter name="DATA_INICIO" class="java.util.Date"/>
	<parameter name="DATA_FIM" class="java.util.Date"/>
	<parameter name="ID_EVENTO" class="java.lang.Long"/>
	<parameter name="APROVADOS" class="java.lang.Long"/>
	<queryString>
		<![CDATA[declare @evento_id int
set @evento_id = $P{ID_EVENTO}

select distinct (select GETDATE() as date) as date , titulo from dbo.evento where id = @evento_id;]]>
	</queryString>
	<field name="date" class="java.sql.Timestamp"/>
	<field name="titulo" class="java.lang.String"/>
	<variable name="titulo_1" class="java.lang.Integer" calculation="Count">
		<variableExpression><![CDATA[]]></variableExpression>
	</variable>
	<variable name="titulo_2" class="java.lang.Integer" resetType="Page" calculation="Count">
		<variableExpression><![CDATA[]]></variableExpression>
	</variable>
	<pageHeader>
		<band height="48">
			<textField>
				<reportElement x="0" y="0" width="555" height="20" uuid="f35b9529-d02e-4841-83eb-308d786e819d"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{TITULO_RELATORIO}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Opaque" x="0" y="33" width="136" height="15" backcolor="#D9F2F1" uuid="bcd897ba-ffda-4814-983a-2c1183840a83"/>
				<box leftPadding="2">
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
					<paragraph leftIndent="5"/>
				</textElement>
				<text><![CDATA[EVENTO:]]></text>
			</staticText>
			<textField>
				<reportElement x="136" y="33" width="419" height="15" uuid="05a942e0-a395-45a2-9541-bcac545543b5"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{titulo}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<summary>
		<band height="113">
			<componentElement>
				<reportElement key="table" style="table" x="0" y="46" width="275" height="25" uuid="ae9bb82f-31ea-472f-a278-0237e04fd544"/>
				<jr:table xmlns:jr="http://jasperreports.sourceforge.net/jasperreports/components" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports/components http://jasperreports.sourceforge.net/xsd/components.xsd">
					<datasetRun subDataset="dsMunicipiosPresentes" uuid="442686c7-5236-4a53-ac2e-06854c488a95">
						<datasetParameter name="EVENTOS">
							<datasetParameterExpression><![CDATA[$P{EVENTOS}]]></datasetParameterExpression>
						</datasetParameter>
						<datasetParameter name="DATA_INICIO">
							<datasetParameterExpression><![CDATA[$P{DATA_INICIO}]]></datasetParameterExpression>
						</datasetParameter>
						<datasetParameter name="DATA_FIM">
							<datasetParameterExpression><![CDATA[$P{DATA_FIM}]]></datasetParameterExpression>
						</datasetParameter>
						<datasetParameter name="ID_EVENTO">
							<datasetParameterExpression><![CDATA[$P{ID_EVENTO}]]></datasetParameterExpression>
						</datasetParameter>
						<datasetParameter name="APROVADOS">
							<datasetParameterExpression><![CDATA[$P{APROVADOS}]]></datasetParameterExpression>
						</datasetParameter>
						<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					</datasetRun>
					<jr:column width="183" uuid="fb6224a7-cada-4cf4-90f1-56a2f2d4c75f">
						<jr:columnHeader style="table_TH" height="15" rowSpan="1">
							<staticText>
								<reportElement x="0" y="0" width="183" height="15" backcolor="#D9F2F1" uuid="46e3c13e-68fe-4836-a598-83d103a75147"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="7" isBold="true" pdfFontName="Helvetica-Bold"/>
								</textElement>
								<text><![CDATA[MUNICÍPIO]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:detailCell style="table_TD" height="10" rowSpan="1">
							<textField>
								<reportElement x="0" y="0" width="183" height="10" uuid="43bfde29-f000-4cfb-a6dd-844d2092710e"/>
								<textElement verticalAlignment="Middle">
									<font size="7"/>
									<paragraph leftIndent="5"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{nomeMunicipio}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
					<jr:column width="92" uuid="4872f15e-c1ec-4261-853a-c582484f9cc9">
						<jr:columnHeader style="table_TH" height="15" rowSpan="1">
							<staticText>
								<reportElement x="0" y="0" width="92" height="15" backcolor="#D9F2F1" uuid="2c40bdc6-617c-4b70-87d9-1278f1b4ed3c"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="7" isBold="true" pdfFontName="Helvetica-Bold"/>
								</textElement>
								<text><![CDATA[PRESENTE EM CURSOS]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:detailCell style="table_TD" height="10" rowSpan="1">
							<textField>
								<reportElement x="0" y="0" width="92" height="10" uuid="a99f19ad-f614-4fd2-bdaf-0caca236b09b"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="7"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{quantidadeParticipacoes} > 0 ? "SIM" : "NÃO"]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
				</jr:table>
			</componentElement>
			<componentElement>
				<reportElement x="286" y="61" width="270" height="45" uuid="b3a05058-edca-4cb0-95c2-13f50c46a0ee"/>
				<jr:list xmlns:jr="http://jasperreports.sourceforge.net/jasperreports/components" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports/components http://jasperreports.sourceforge.net/xsd/components.xsd" printOrder="Vertical">
					<datasetRun subDataset="dsTotalizador" uuid="59a1cd5a-ecf9-4c2e-813f-1eeda4884667">
						<datasetParameter name="EVENTOS">
							<datasetParameterExpression><![CDATA[$P{EVENTOS}]]></datasetParameterExpression>
						</datasetParameter>
						<datasetParameter name="DATA_INICIO">
							<datasetParameterExpression><![CDATA[$P{DATA_INICIO}]]></datasetParameterExpression>
						</datasetParameter>
						<datasetParameter name="DATA_FIM">
							<datasetParameterExpression><![CDATA[$P{DATA_FIM}]]></datasetParameterExpression>
						</datasetParameter>
						<datasetParameter name="ID_EVENTO">
							<datasetParameterExpression><![CDATA[$P{ID_EVENTO}]]></datasetParameterExpression>
						</datasetParameter>
						<datasetParameter name="APROVADOS">
							<datasetParameterExpression><![CDATA[$P{APROVADOS}]]></datasetParameterExpression>
						</datasetParameter>
						<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					</datasetRun>
					<jr:listContents height="45" width="270">
						<textField>
							<reportElement style="table_TD" x="85" y="15" width="110" height="15" uuid="7e446824-aecb-49dc-805e-3e714000f15c"/>
							<textElement textAlignment="Center" verticalAlignment="Middle">
								<font size="7"/>
							</textElement>
							<textFieldExpression><![CDATA[$F{totalMunicipio} - $F{quantidadePresente}]]></textFieldExpression>
						</textField>
						<textField>
							<reportElement style="table_TD" x="85" y="0" width="110" height="15" uuid="1495facc-44dd-422c-ab29-ff277ba77c13"/>
							<textElement textAlignment="Center" verticalAlignment="Middle">
								<font size="7"/>
							</textElement>
							<textFieldExpression><![CDATA[$F{quantidadePresente}]]></textFieldExpression>
						</textField>
						<textField>
							<reportElement style="table_TD" x="85" y="30" width="110" height="15" uuid="0058f3d5-0b9b-4e57-a422-b741580c793f"/>
							<textElement textAlignment="Center" verticalAlignment="Middle">
								<font size="7" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<textFieldExpression><![CDATA[$F{totalMunicipio}]]></textFieldExpression>
						</textField>
						<staticText>
							<reportElement style="table_TD" x="0" y="0" width="85" height="15" uuid="caba43f3-4348-499c-9aa1-6a5146bf799a"/>
							<textElement textAlignment="Center" verticalAlignment="Middle">
								<font size="7"/>
								<paragraph leftIndent="5"/>
							</textElement>
							<text><![CDATA[SIM]]></text>
						</staticText>
						<staticText>
							<reportElement style="table_TD" x="0" y="15" width="85" height="15" uuid="8241460f-6d9a-4d74-985e-11235f71cea9"/>
							<textElement textAlignment="Center" verticalAlignment="Middle">
								<font size="7"/>
								<paragraph leftIndent="5"/>
							</textElement>
							<text><![CDATA[NÃO]]></text>
						</staticText>
						<staticText>
							<reportElement style="table_TD" x="0" y="30" width="85" height="15" uuid="c267b6ac-1c5f-483d-aab9-54709d493e0e"/>
							<textElement textAlignment="Center" verticalAlignment="Middle">
								<font size="7" isBold="true" pdfFontName="Helvetica-Bold"/>
								<paragraph leftIndent="5"/>
							</textElement>
							<text><![CDATA[TOTAL GERAL]]></text>
						</staticText>
						<textField pattern="###0.00;-###0.00">
							<reportElement style="table_TD" x="195" y="0" width="75" height="15" uuid="8d0cbff9-8740-4cc3-9d78-de93486444e8"/>
							<textElement textAlignment="Center" verticalAlignment="Middle">
								<font size="7"/>
							</textElement>
							<textFieldExpression><![CDATA[(new Double($F{quantidadePresente}) * 100 / new Double($F{totalMunicipio}))]]></textFieldExpression>
						</textField>
						<textField pattern="###0.00;-###0.00">
							<reportElement style="table_TD" x="195" y="15" width="75" height="15" uuid="2f385ced-2c66-4b48-83fb-585604d847db"/>
							<textElement textAlignment="Center" verticalAlignment="Middle">
								<font size="7"/>
							</textElement>
							<textFieldExpression><![CDATA[(new Double($F{totalMunicipio} - $F{quantidadePresente}) * 100 / new Double($F{totalMunicipio}))]]></textFieldExpression>
						</textField>
						<staticText>
							<reportElement style="table_TD" x="195" y="30" width="75" height="15" uuid="87bdf0b8-946f-46eb-a1f7-6719393e84e0"/>
							<textElement textAlignment="Center" verticalAlignment="Middle">
								<font size="7" isBold="true" pdfFontName="Helvetica-Bold"/>
								<paragraph leftIndent="5"/>
							</textElement>
							<text><![CDATA[100,00%]]></text>
						</staticText>
					</jr:listContents>
				</jr:list>
			</componentElement>
			<staticText>
				<reportElement style="table_TH" mode="Opaque" x="286" y="46" width="85" height="15" backcolor="#D9F2F1" uuid="af4bf927-2551-421f-8877-cbb7759975f8"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="7" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[PRESENTES EM CURSO]]></text>
			</staticText>
			<staticText>
				<reportElement style="table_TH" mode="Opaque" x="371" y="46" width="110" height="15" backcolor="#D9F2F1" uuid="9a8fbdf7-c999-43cd-a7cd-fe6f97720f0d"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="7" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[QUANTIDADE DE MUNICÍPIOS]]></text>
			</staticText>
			<staticText>
				<reportElement style="table_TH" mode="Opaque" x="481" y="46" width="75" height="15" backcolor="#D9F2F1" uuid="ed730fd8-5c1e-4b63-ac2d-cde2f0ffc72c"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="7" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[PERCENTUAL]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="0" y="0" width="136" height="15" backcolor="#D9F2F1" uuid="68d980e6-46b2-4d81-8497-e6884d245314"/>
				<box leftPadding="2">
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
					<paragraph leftIndent="5"/>
				</textElement>
				<text><![CDATA[PARTICIPANTES APROVADOS:]]></text>
			</staticText>
			<textField>
				<reportElement x="136" y="0" width="419" height="15" uuid="7581139a-cfcd-4633-baeb-af7cf0bcd815"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{APROVADOS} == 1 ? "SIM" : "NÃO"]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Opaque" x="0" y="15" width="136" height="15" backcolor="#D9F2F1" uuid="f96409b2-6465-4caa-9014-fe7795ca8497"/>
				<box leftPadding="2">
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
					<paragraph leftIndent="5"/>
				</textElement>
				<text><![CDATA[PERÍODO:]]></text>
			</staticText>
			<textField>
				<reportElement x="137" y="15" width="419" height="15" uuid="19e4ce52-336a-457f-8d58-0cd08575740e"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[( $P{DATA_INICIO} != null ? (new String((new SimpleDateFormat("dd/MM/yyyy").format($P{DATA_INICIO})).toString()) + " a " + new String((new SimpleDateFormat("dd/MM/yyyy").format($P{DATA_FIM})).toString())) : "NÃO ESPECIFICADO" )]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
