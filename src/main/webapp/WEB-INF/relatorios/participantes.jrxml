<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="participantes" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="0" bottomMargin="20" uuid="cdedae05-c70e-4193-9af9-d1dc881e8513">
	<property name="ireport.zoom" value="1.5026296018031562"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="Title" forecolor="#FFFFFF" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>
	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>
	<style name="Column header" forecolor="#666666" fontName="Arial" fontSize="12" isBold="true"/>
	<style name="Detail" fontName="Arial" fontSize="12"/>
	<parameter name="TIPO_EVENTO" class="java.lang.String"/>
	<parameter name="ID_EVENTO" class="java.lang.Long"/>
	<parameter name="DESC_TIPO_PARTICIPANTE" class="java.lang.String"/>
	<parameter name="ID_TIPO_PARTICIPANTE" class="java.lang.Long"/>
	<parameter name="TITULO_EVENTO" class="java.lang.String"/>
	<parameter name="TITULO_RELATORIO" class="java.lang.String"/>
	<parameter name="AUTORIZADA" class="java.lang.Long"/>
	<parameter name="PNE" class="java.lang.Long"/>
	<queryString>
		<![CDATA[declare @id_evento int, @id_tipo_participante int, @autorizada int, @pne int
set @id_evento = $P{ID_EVENTO}
set @id_tipo_participante = $P{ID_TIPO_PARTICIPANTE}
set @autorizada = $P{AUTORIZADA}
set @pne = $P{PNE}
select distinct participante.id as id_participante,
		pessoa.nome as nome,
		pessoa.email as email,
		pessoa.cpf as cpf,
		tipo.descricao as tipo,
		pessoa.celular as celular,
		pessoa.telefone as telefone,
		orgao.descOrgao as desc_orgao,
		participante.lotacao as lotacao,
		participante.cargo as cargo,
		inscricao.autorizada as autorizada,
		participante.pne as pne,
		tipo_necessidade_especial_id as tipo_necessidade,
		dom.descricao as dominioDescr,
		(select top 1 sum(nota.valor)/count(mo.id) from dbo.nota as nota
			inner join dbo.modulo as mo on nota.modulo_id = mo.id
			inner join dbo.evento as ev on mo.evento_id = ev.id
			where ev.id = inscricao.evento_id and nota.participante_id = participante.id
			group by mo.id ) as mediaNota,
		(select count(*) from dbo.frequencia as freq
			inner join dbo.modulo as mo on freq.modulo_id = mo.id
			inner join dbo.evento as ev on mo.evento_id = ev.id
			where ev.id = inscricao.evento_id and freq.participante_id = participante.id and freq.presenca = 1
			group by ev.id ) as qtdPresencas,
		(select sum(mo.quantidade_encontros) from dbo.modulo as mo
			inner join dbo.evento as ev on mo.evento_id = ev.id
			where ev.id = inscricao.evento_id group by ev.id ) as qtdEncontros
	from dbo.participante as participante
		inner join dbo.pessoa as pessoa on pessoa.id = participante.pessoa_id
		inner join dbo.dominio as tipo on tipo.id = participante.publico_alvo_id
		left join dbo.inscricao as inscricao on inscricao.participante_id = participante.id
		left join ORCAFI..Orgaos as orgao on orgao.codOrgao = participante.codOrgao and orgao.codMunicipio = participante.codMunicipio
		left join dbo.dominio as dom on participante.tipo_necessidade_especial_id = dom.id
	where (
		(
			((@autorizada = 3) and inscricao.autorizada is null)
			or
			((@autorizada is not null) and inscricao.autorizada = @autorizada)
			or
			((@autorizada is null) and 1=1)
		)
	)
		and tipo.id = case when @id_tipo_participante is null then tipo.id else @id_tipo_participante end
		and ((@pne is null) or (@pne = 0 and participante.pne is null ) or (participante.pne = @pne) )
		and ((@id_evento is null) or (inscricao.evento_id = @id_evento))
order by pessoa.nome]]>
	</queryString>
	<field name="id_participante" class="java.lang.Integer"/>
	<field name="nome" class="java.lang.String"/>
	<field name="email" class="java.lang.String"/>
	<field name="cpf" class="java.lang.String"/>
	<field name="tipo" class="java.lang.String"/>
	<field name="celular" class="java.lang.String"/>
	<field name="telefone" class="java.lang.String"/>
	<field name="desc_orgao" class="java.lang.String"/>
	<field name="lotacao" class="java.lang.String"/>
	<field name="cargo" class="java.lang.String"/>
	<field name="autorizada" class="java.lang.Integer"/>
	<field name="pne" class="java.lang.Integer"/>
	<field name="tipo_necessidade" class="java.lang.Integer"/>
	<field name="dominioDescr" class="java.lang.String"/>
	<field name="mediaNota" class="java.lang.Double"/>
	<field name="qtdPresencas" class="java.lang.Integer"/>
	<field name="qtdEncontros" class="java.lang.Integer"/>
	<group name="group">
		<groupFooter>
			<band height="15">
				<textField isBlankWhenNull="true">
					<reportElement positionType="Float" stretchType="RelativeToTallestObject" x="492" y="0" width="310" height="15" uuid="5d35a726-0f1b-47ce-aca9-3fb5ee201916"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="7"/>
					</textElement>
					<textFieldExpression><![CDATA["TOTAL DE PARTICIPANTES: " + $V{REPORT_COUNT}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<title>
		<band height="106" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement style="SubTitle" positionType="Float" stretchType="RelativeToTallestObject" mode="Opaque" x="99" y="26" width="703" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="42b24d3e-6204-456a-ac9e-06e462528361"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5" lineColor="#000000"/>
					<topPen lineWidth="0.5" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single" leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[( $P{ID_TIPO_PARTICIPANTE} != null ? $P{DESC_TIPO_PARTICIPANTE} : "TODOS" )]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement style="SubTitle" positionType="Float" stretchType="RelativeToTallestObject" mode="Opaque" x="0" y="26" width="99" height="15" forecolor="#000000" backcolor="#D9F2F1" uuid="b9d44d05-6cf6-4bb0-ab63-df031e66ee37"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5" lineColor="#000000"/>
					<topPen lineWidth="0.5" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single" leftIndent="5"/>
				</textElement>
				<text><![CDATA[PÚBLICO ALVO:]]></text>
			</staticText>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" mode="Opaque" x="99" y="41" width="703" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="2d784890-a2b2-47a5-8543-bbfe2f383732"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5" lineColor="#000000"/>
					<topPen lineWidth="0.5" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single" leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[( $P{ID_EVENTO} != null ? ($P{TIPO_EVENTO} + " " + $P{TITULO_EVENTO}) : "TODOS" )]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement style="SubTitle" positionType="Float" stretchType="RelativeToTallestObject" mode="Opaque" x="0" y="41" width="99" height="15" forecolor="#000000" backcolor="#D9F2F1" uuid="3f7f6bb7-a0fc-4380-a99a-82b5767d2c61"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5" lineColor="#000000"/>
					<topPen lineWidth="0.5" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single" leftIndent="5"/>
				</textElement>
				<text><![CDATA[EVENTO:]]></text>
			</staticText>
			<textField>
				<reportElement x="0" y="0" width="802" height="20" uuid="dc0934ed-fb17-4003-8648-ce6aa706297d"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{TITULO_RELATORIO}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement style="SubTitle" positionType="Float" stretchType="RelativeToTallestObject" mode="Opaque" x="0" y="56" width="99" height="15" forecolor="#000000" backcolor="#D9F2F1" uuid="d9082e29-8038-4920-9074-54ab7eca474b"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5" lineColor="#000000"/>
					<topPen lineWidth="0.5" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single" leftIndent="5"/>
				</textElement>
				<text><![CDATA[AUTORIZADA:]]></text>
			</staticText>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" mode="Opaque" x="99" y="56" width="703" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="3765bf52-33aa-48bb-8bab-62eaec26e75a"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5" lineColor="#000000"/>
					<topPen lineWidth="0.5" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single" leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{AUTORIZADA} != null ? ( $P{AUTORIZADA} == 1 ? "SIM" : ($P{AUTORIZADA} == 3 ? "AGUARDANDO AUTORIZAÇÃO" : "NÃO") ) : "TODOS"]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement style="SubTitle" positionType="Float" stretchType="RelativeToTallestObject" mode="Opaque" x="0" y="71" width="99" height="15" forecolor="#000000" backcolor="#D9F2F1" uuid="e2c7a727-3ef1-4d2a-9952-177c6967d084"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5" lineColor="#000000"/>
					<topPen lineWidth="0.5" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single" leftIndent="5"/>
				</textElement>
				<text><![CDATA[PNE:]]></text>
			</staticText>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" mode="Opaque" x="99" y="71" width="703" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="6f1154f0-ecfa-4db4-a261-c777d4d01747"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5" lineColor="#000000"/>
					<topPen lineWidth="0.5" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single" leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[($P{PNE} != null ? ($P{PNE} == 1 ? "SIM" : "NÃO" ) : "TODOS")]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<columnHeader>
		<band height="15">
			<staticText>
				<reportElement style="SubTitle" positionType="Float" stretchType="RelativeToTallestObject" mode="Opaque" x="0" y="0" width="123" height="15" forecolor="#000000" backcolor="#D9F2F1" uuid="03fb6593-4ffe-450d-9604-aa90a4acba36"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5" lineColor="#000000"/>
					<topPen lineWidth="0.5" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[NOME]]></text>
			</staticText>
			<staticText>
				<reportElement style="SubTitle" positionType="Float" stretchType="RelativeToTallestObject" mode="Opaque" x="123" y="0" width="73" height="15" forecolor="#000000" backcolor="#D9F2F1" uuid="c8e65e5a-163f-4e55-b295-ce4f874d330c"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5" lineColor="#000000"/>
					<topPen lineWidth="0.5" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[TIPO]]></text>
			</staticText>
			<staticText>
				<reportElement style="SubTitle" positionType="Float" stretchType="RelativeToTallestObject" mode="Opaque" x="255" y="0" width="61" height="15" forecolor="#000000" backcolor="#D9F2F1" uuid="f6a8f53e-e285-4732-9c01-88080bf3e68d"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5" lineColor="#000000"/>
					<topPen lineWidth="0.5" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[CPF]]></text>
			</staticText>
			<staticText>
				<reportElement style="SubTitle" positionType="Float" stretchType="RelativeToTallestObject" mode="Opaque" x="415" y="0" width="93" height="15" forecolor="#000000" backcolor="#D9F2F1" uuid="de531ef4-35f5-42bf-bd99-78837b248cf6"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5" lineColor="#000000"/>
					<topPen lineWidth="0.5" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[ÓRGÃO / INSTITUIÇÃO]]></text>
			</staticText>
			<staticText>
				<reportElement style="SubTitle" positionType="Float" stretchType="RelativeToTallestObject" mode="Opaque" x="637" y="0" width="69" height="15" forecolor="#000000" backcolor="#D9F2F1" uuid="4365dd7e-76bf-483d-8854-a2f948fe99f7"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5" lineColor="#000000"/>
					<topPen lineWidth="0.5" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[CONTATO]]></text>
			</staticText>
			<staticText>
				<reportElement style="SubTitle" positionType="Float" stretchType="RelativeToTallestObject" mode="Opaque" x="316" y="0" width="99" height="15" forecolor="#000000" backcolor="#D9F2F1" uuid="93270023-5d63-46f6-a9ff-2da47b8f089b"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5" lineColor="#000000"/>
					<topPen lineWidth="0.5" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[E-MAIL]]></text>
			</staticText>
			<staticText>
				<reportElement style="SubTitle" positionType="Float" stretchType="RelativeToTallestObject" mode="Opaque" x="706" y="0" width="30" height="15" forecolor="#000000" backcolor="#D9F2F1" uuid="46c793f1-c071-4983-ace6-1fa576f05d0f"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5" lineColor="#000000"/>
					<topPen lineWidth="0.5" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[NOTA]]></text>
			</staticText>
			<staticText>
				<reportElement style="SubTitle" positionType="Float" stretchType="RelativeToTallestObject" mode="Opaque" x="736" y="0" width="66" height="15" forecolor="#000000" backcolor="#D9F2F1" uuid="4ef7d0a3-7a95-4971-9ba5-5e44f19bd64e"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5" lineColor="#000000"/>
					<topPen lineWidth="0.5" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[FREQUÊNCIA(%)]]></text>
			</staticText>
			<staticText>
				<reportElement style="SubTitle" positionType="Float" stretchType="RelativeToTallestObject" mode="Opaque" x="196" y="0" width="59" height="15" forecolor="#000000" backcolor="#D9F2F1" uuid="e05624c3-2688-49be-a7a0-5e59b3460577"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5" lineColor="#000000"/>
					<topPen lineWidth="0.5" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[AUTORIZADA]]></text>
			</staticText>
			<staticText>
				<reportElement style="SubTitle" positionType="Float" stretchType="RelativeToTallestObject" mode="Opaque" x="508" y="0" width="37" height="15" forecolor="#000000" backcolor="#D9F2F1" uuid="d52e6e45-d5c3-4d8a-9abd-2200d01b82bb"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5" lineColor="#000000"/>
					<topPen lineWidth="0.5" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[PNE]]></text>
			</staticText>
			<staticText>
				<reportElement style="SubTitle" positionType="Float" stretchType="RelativeToTallestObject" mode="Opaque" x="545" y="0" width="92" height="15" forecolor="#000000" backcolor="#D9F2F1" uuid="8c142c83-9e10-4bdd-90e0-6064e7fc0f84"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5" lineColor="#000000"/>
					<topPen lineWidth="0.5" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Necessidade Especial]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="15" splitType="Prevent">
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" mode="Transparent" x="0" y="0" width="123" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="6ffe84ca-21eb-494e-bb49-a062d8cd4aca"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single" leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{nome}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" mode="Transparent" x="123" y="0" width="73" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="0962456d-96ff-4f84-b856-9301ef22856f"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single" leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{tipo}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" mode="Transparent" x="255" y="0" width="61" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="84c47ae5-be0e-4ce8-b673-3750acf72e81"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single" leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{cpf}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" mode="Transparent" x="415" y="0" width="92" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="aaa69b57-eac3-4008-9c50-e7c87f3db3a4"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single" leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{desc_orgao}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" mode="Transparent" x="637" y="0" width="69" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="06f55bc1-7f1e-41e1-ab2f-abc717d89e46"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single" leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{celular} + " " + $F{telefone}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" mode="Transparent" x="316" y="0" width="99" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="438363b9-5c89-4d19-905c-932ab2a9a97e"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single" leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{email}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="###0.00;-###0.00" isBlankWhenNull="true">
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" mode="Transparent" x="706" y="0" width="30" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="212bea3d-a10c-47e6-995f-5277e65d8488"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single" leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{mediaNota}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="###0.00;-###0.00" isBlankWhenNull="true">
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" mode="Transparent" x="736" y="0" width="66" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="52a0ca98-4068-43ad-a841-3ac9034de092"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single" leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[new Double($F{qtdPresencas}) * 100 / new Double($F{qtdEncontros})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" mode="Transparent" x="196" y="0" width="59" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="2f8176f0-eb0e-4152-b6aa-63b75671ab05"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single" leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[( $F{autorizada} == 1 ? "SIM" : "NÃO" )]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="###0.00;-###0.00" isBlankWhenNull="true">
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" mode="Transparent" x="508" y="0" width="37" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="1fc7ea45-28a0-43c2-beff-5ecef0fffb20"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single" leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{pne} == null || $F{pne} == 0) ? "NÃO" : "SIM"]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" mode="Transparent" x="545" y="0" width="92" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="bf9f79f8-108a-48e4-a0bc-21bb7911c912"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="7" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single" leftIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{dominioDescr}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
