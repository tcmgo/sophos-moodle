var atualizacaoTabela = function() {
	jQuery('.ui-fileupload-files').unbind('DOMNodeInserted');
	jQuery('.ui-fileupload-files .ui-fileupload-cancel').css('bottom', '4px');
	jQuery('.ui-fileupload-files .ui-fileupload-cancel')
			.addClass(
					'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary  mdl-button--icon');
	jQuery('.ui-fileupload-files .ui-fileupload-cancel').html(
			'<i class="material-icons">clear</i>');
	jQuery('.ui-fileupload-files').bind('DOMNodeInserted', atualizacaoTabela);
};

function defineMascaras(){
	jQuery('.hasDatepicker').mask('99/99/9999');
	
	jQuery('.hasTime').timeEntry({
		show24Hours : true,
		showSeconds : false
	});

	jQuery(".telefone").mask("(99)9999-9999?9").focusout(
			function(event) {
				var target, phone, element;
				target = (event.currentTarget) ? event.currentTarget
						: event.srcElement;
				phone = target.value.replace(/\D/g, '');
				element = $(target);
				element.unmask();
				if (phone.length > 10) {
					element.mask("(99)99999-999?9");
				} else {
					element.mask("(99)9999-9999?9");
				}
			});

	jQuery(".money").maskMoney({
		symbol : 'R$ ',
		decimal : ",",
		thousands : ".",
		precision : 2,
		allowZero : false,
		showSymbol : true
	});

	jQuery(".nota").maskMoney({
		decimal : ".",
		thousands : ".",
		precision : 1,
		allowZero : false,
		showSymbol : false
	});
}

function onLoad() {

	deslizarCard();
	defineMascaras();
	
	$('.notificacoes').dropdown({
	      inDuration: 300,
	      outDuration: 225,
	      constrain_width: false, // Does not change width of dropdown to that of the activator
	      hover: false, // Activate on hover
	      gutter: 0, // Spacing from edge
	      belowOrigin: false, // Displays dropdown below the button
	      alignment: 'left' // Displays dropdown with edge aligned to the left of button
	    }
	  );

	jQuery('.timeEntry-control').hide();

	jQuery('.ui-paginator-page')
			.addClass(
					'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary mdl-button--icon');
	jQuery('.ui-paginator-first')
			.addClass(
					'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary mdl-button--icon')
			.html('<i class="material-icons">skip_previous</i>');
	jQuery('.ui-paginator-prev')
			.addClass(
					'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary  mdl-button--icon')
			.html('<i class="material-icons">fast_rewind</i>');
	jQuery('.ui-paginator-next')
			.addClass(
					'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary  mdl-button--icon')
			.html('<i class="material-icons">fast_forward</i>');
	jQuery('.ui-paginator-last')
			.addClass(
					'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary  mdl-button--icon')
			.html('<i class="material-icons">skip_next</i>');
	;
	jQuery('.ui-fileupload-upload')
			.addClass(
					'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary  mdl-button--icon')
			.html('<i class="material-icons">file_upload</i>');
	;
	jQuery('.ui-fileupload-cancel')
			.addClass(
					'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary  mdl-button--icon')
			.html('<i class="material-icons">clear</i>');
	;
	jQuery('.ui-fileupload-choose span.ui-button-text')
			.addClass(
					'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary')
			.html('').append(
					'<i class="material-icons">attach_file</i> ANEXAR ARQUIVO');
	jQuery('.ui-fileupload-files').addClass('mdl-data-table mdl-js-data-table');
	jQuery('.ui-fileupload-files').bind('DOMNodeInserted', atualizacaoTabela);

	jQuery('.verificar-capslock')
			.keypress(
					function(e) {
						e = e || window.event;
						var character = String.fromCharCode(e.keyCode
								|| e.which);
						if (character.toUpperCase() === character.toLowerCase()) {
							return;
						}
						if ((e.shiftKey && character.toLowerCase() === character)
								|| (!e.shiftKey && character.toUpperCase() === character)) {
							if (!window['capslock.ativo.avisado']) {
								Materialize.toast('CAPS LOCK ATIVO', 3000,
										'rounded')
							}
							window['capslock.ativo.avisado'] = true;
						} else {
							if (window['capslock.ativo.avisado']) {
								Materialize.toast('CAPS LOCK INATIVO', 3000,
										'rounded')
							}
							window['capslock.ativo.avisado'] = false;
						}
					});

	habilitarVerMais();
}

function deslizarCard() {
	
	jQuery('.hover-card').hover(function(e) {
		jQuery(e.target).closest('.card').css('overflow', 'hidden');
		jQuery(this.lastElementChild).css({ display: 'block'}).velocity("stop", false).velocity({translateY: '-100%'}, {duration: 300, queue: false, easing: 'easeInOutQuad'});
	}, function(e){
		jQuery(this.lastElementChild).velocity(
            {translateY: 0}, {
              duration: 225,
              queue: false,
              easing: 'easeInOutQuad',
              complete: function() { $(this).css({ display: 'none'}); }
            }
         );
	});
	
	jQuery('.card-title').click(function(e) {
		jQuery(e.target).closest('.card').css('overflow', 'hidden');
		jQuery(this.lastElementChild).css({ display: 'block'}).velocity("stop", false).velocity({translateY: '-100%'}, {duration: 300, queue: false, easing: 'easeInOutQuad'});
	});
	
}

function removerMensagens() {

	jQuery('.ui-messages ul li').remove();
}

function unbindBtnModalConfirmacao() {
	jQuery('#modalConfirmacaoBotaoSim').unbind("click");
	jQuery('#modalConfirmacaoBotaoNao').unbind("click");
	jQuery('#modalConfirmacao').closeModal();
}

function confirmar(texto, acao) {

	jQuery('#modalConfirmacaoTextoConfirmacao').html(texto);
	jQuery('#modalConfirmacaoBotaoSim').click(function() {
		acao();
		jQuery('#modalConfirmacaoBotaoSim').unbind("click");
	});
	jQuery('#modalConfirmacao').openModal({
		dismissible : false
	});
}

function openModal(id) {
	jQuery('#' + id).openModal({
		dismissible : false,
		modal : false
	});
	jQuery('.lean-overlay').hide();
}

function fecharModal(id) {
	jQuery('#' + id).closeModal();
}

function removerDuplicacoesDeMensagens() {

	var mensagens = [];

	var funcaoFiltrarDuplicacoes = function() {

		if (mensagens.indexOf(jQuery(this).text()) == -1) {

			mensagens.push(jQuery(this).text());

		} else {

			jQuery(this).remove();
		}
	}

	jQuery('.ui-messages li').each(funcaoFiltrarDuplicacoes);
}

function exibirMensagem() {

	removerDuplicacoesDeMensagens();

	// VERIFICA SE EXISTE MENSAGENS A SEREM EXIBIDAS, OU SEJA, SE HÃ ELEMENTOS
	// NA LISTA DE MENSAGEM
	if (jQuery('.ui-messages ul li').length > 0) {

		// VERIFICA SE EXISTE A VARIÃVEL PF (PRIMEFACES) E O MODAL REGISTRADO
		// (PF('modalAlerta'))
		if (jQuery('#modalAlerta')) {

			// EXISTE O MODAL
			jQuery('#modalAlerta').openModal({
				dismissible : false
			});
		}
	}
}

function showProgress() {

	jQuery('#progress-bar').css('visibility', 'visible');
}

function hideProgress() {

	jQuery('#progress-bar').css('visibility', 'hidden');
}

function habilitarVerMais() {

	var $el, $ps, $up, totalHeight;

	if ($(".sidebar-box .content").outerHeight() <= 32) {

		$(".sidebar-box .button").css('display', 'none');
	}

	$(".sidebar-box .button").click(function() {

		if ($(".sidebar-box").outerHeight() == 32) {

			totalHeight = $(".sidebar-box .content").outerHeight();

			$el = $(this);

			$up = $el.parent().parent();

			$up.css({
				"height" : '3em',
				"max-height" : 9999
			}).animate({
				"height" : totalHeight
			});

			$('.sidebar-box .mdl-tooltip').html('Recolher');
			$(".sidebar-box .button i").html('remove');

		} else {

			totalHeight = 32;

			$el = $(this);

			$up = $el.parent().parent();

			$up.css({
				"height" : $(".sidebar-box").outerHeight(),
			}).animate({
				"height" : totalHeight
			});

			$('.sidebar-box .mdl-tooltip').html('Expandir');
			$(".sidebar-box .button i").html('add');
		}

		return false;
	});
}

function ativarAba(classeAbaAtiva) {

	$('.mdl-tabs__panel').removeClass('is-active');
	$('.mdl-tabs__tab').removeClass('is-active');

	$('.' + classeAbaAtiva).addClass('is-active');

}

function limparAbaAtiva() {
	$('.mdl-tabs__panel').removeClass('is-active');
	$('.mdl-tabs__tab').removeClass('is-active');
}

function carregaToolTipsTipoParticipante() {
	
    var options = document.getElementById("tipoParticipante").options;
    var texto = '';
    
    for(var i = 0; i < options.length; i++) {
    	var texto = obterTextoToolTipTipoParticipante(options[i].innerHTML);
        options[i].title = texto;
    }
    
}

function obterTextoToolTipTipoParticipante(tipo) {
	
	if (tipo == 'JURISDICIONADO') {
		return 'JURISDICIONADO: Agente político e servidor público municipal.';
	} else if (tipo == 'SOCIEDADE') {
		return 'SOCIEDADE: Sem vínculo com a administração pública municipal';
	} else if (tipo == 'INTERNO TCM') {
		return 'INTERNO TCM: Servidor do Tribunal de Contas dos Municípios do Estado de Goiás (TCM GO).';
	} else {
		return '';
	}
	
	
}
