--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.1
-- Dumped by pg_dump version 9.6.3

-- Started on 2017-08-09 14:30:10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2593 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 186 (class 1259 OID 1722196)
-- Name: alerta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE alerta (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    ativo boolean NOT NULL,
    data_visualizacao timestamp without time zone,
    mensagem character varying(2000),
    pacote_alertas_id bigint,
    participante_id bigint NOT NULL,
    tipo_id bigint
);


ALTER TABLE alerta OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 1722194)
-- Name: alerta_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE alerta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alerta_id_seq OWNER TO postgres;

--
-- TOC entry 2594 (class 0 OID 0)
-- Dependencies: 185
-- Name: alerta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE alerta_id_seq OWNED BY alerta.id;


--
-- TOC entry 188 (class 1259 OID 1722207)
-- Name: anexo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE anexo (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    arquivo_ged character varying(255),
    descricao character varying(255),
    tipo_arquivo character varying(255)
);


ALTER TABLE anexo OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 1722205)
-- Name: anexo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE anexo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE anexo_id_seq OWNER TO postgres;

--
-- TOC entry 2595 (class 0 OID 0)
-- Dependencies: 187
-- Name: anexo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE anexo_id_seq OWNED BY anexo.id;


--
-- TOC entry 190 (class 1259 OID 1722218)
-- Name: arquivo_adm; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE arquivo_adm (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    arquivo_id bigint NOT NULL,
    evento_id bigint NOT NULL,
    tipo_arquivoadm_id bigint NOT NULL
);


ALTER TABLE arquivo_adm OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 1722216)
-- Name: arquivo_adm_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE arquivo_adm_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE arquivo_adm_id_seq OWNER TO postgres;

--
-- TOC entry 2596 (class 0 OID 0)
-- Dependencies: 189
-- Name: arquivo_adm_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE arquivo_adm_id_seq OWNED BY arquivo_adm.id;


--
-- TOC entry 192 (class 1259 OID 1722226)
-- Name: avaliacao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE avaliacao (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    observacao character varying(2000),
    modulo_id bigint NOT NULL,
    participante_id bigint NOT NULL
);


ALTER TABLE avaliacao OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 1722237)
-- Name: avaliacao_eficacia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE avaliacao_eficacia (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    melhoria_desempenho boolean,
    observacao character varying(2000),
    desempenho_servico_id bigint NOT NULL,
    evento_id bigint NOT NULL,
    indicacao_id bigint,
    participante_id bigint NOT NULL
);


ALTER TABLE avaliacao_eficacia OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 1722235)
-- Name: avaliacao_eficacia_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE avaliacao_eficacia_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE avaliacao_eficacia_id_seq OWNER TO postgres;

--
-- TOC entry 2597 (class 0 OID 0)
-- Dependencies: 193
-- Name: avaliacao_eficacia_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE avaliacao_eficacia_id_seq OWNED BY avaliacao_eficacia.id;


--
-- TOC entry 191 (class 1259 OID 1722224)
-- Name: avaliacao_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE avaliacao_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE avaliacao_id_seq OWNER TO postgres;

--
-- TOC entry 2598 (class 0 OID 0)
-- Dependencies: 191
-- Name: avaliacao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE avaliacao_id_seq OWNED BY avaliacao.id;


--
-- TOC entry 196 (class 1259 OID 1722248)
-- Name: banco_febraban; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE banco_febraban (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    codigo character varying(5),
    descricao character varying(255)
);


ALTER TABLE banco_febraban OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 1722246)
-- Name: banco_febraban_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE banco_febraban_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE banco_febraban_id_seq OWNER TO postgres;

--
-- TOC entry 2599 (class 0 OID 0)
-- Dependencies: 195
-- Name: banco_febraban_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE banco_febraban_id_seq OWNED BY banco_febraban.id;


--
-- TOC entry 198 (class 1259 OID 1722256)
-- Name: certificado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE certificado (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    codigo_verificacao character varying(17) NOT NULL,
    data_emissao timestamp without time zone NOT NULL,
    arquivo_id bigint NOT NULL,
    evento_id bigint NOT NULL,
    participante_id bigint NOT NULL
);


ALTER TABLE certificado OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 1722254)
-- Name: certificado_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE certificado_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE certificado_id_seq OWNER TO postgres;

--
-- TOC entry 2600 (class 0 OID 0)
-- Dependencies: 197
-- Name: certificado_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE certificado_id_seq OWNED BY certificado.id;


--
-- TOC entry 200 (class 1259 OID 1722264)
-- Name: cidade; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cidade (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    descricao character varying(255) NOT NULL,
    estado_id bigint NOT NULL
);


ALTER TABLE cidade OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 1722262)
-- Name: cidade_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cidade_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cidade_id_seq OWNER TO postgres;

--
-- TOC entry 2601 (class 0 OID 0)
-- Dependencies: 199
-- Name: cidade_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cidade_id_seq OWNED BY cidade.id;


--
-- TOC entry 202 (class 1259 OID 1722272)
-- Name: dominio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE dominio (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    ativo boolean NOT NULL,
    codigo bigint NOT NULL,
    descricao character varying(255) NOT NULL,
    nome character varying(255) NOT NULL
);


ALTER TABLE dominio OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 1722270)
-- Name: dominio_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE dominio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dominio_id_seq OWNER TO postgres;

--
-- TOC entry 2602 (class 0 OID 0)
-- Dependencies: 201
-- Name: dominio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE dominio_id_seq OWNED BY dominio.id;


--
-- TOC entry 204 (class 1259 OID 1722283)
-- Name: endereco; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE endereco (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    bairro character varying(100),
    cep character varying(9),
    complemento character varying(255),
    logradouro character varying(255),
    municipio_id bigint,
    numero character varying(10),
    cidade_id bigint
);


ALTER TABLE endereco OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 1722281)
-- Name: endereco_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE endereco_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE endereco_id_seq OWNER TO postgres;

--
-- TOC entry 2603 (class 0 OID 0)
-- Dependencies: 203
-- Name: endereco_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE endereco_id_seq OWNED BY endereco.id;


--
-- TOC entry 206 (class 1259 OID 1722294)
-- Name: estado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE estado (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    descricao character varying(150),
    uf character varying(2),
    pais_id bigint
);


ALTER TABLE estado OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 1722292)
-- Name: estado_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE estado_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE estado_id_seq OWNER TO postgres;

--
-- TOC entry 2604 (class 0 OID 0)
-- Dependencies: 205
-- Name: estado_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE estado_id_seq OWNED BY estado.id;


--
-- TOC entry 208 (class 1259 OID 1722302)
-- Name: evento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE evento (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    avaliar_por_media boolean,
    carga_horaria numeric(19,2) NOT NULL,
    conteudo character varying(2000) NOT NULL,
    data_fim_pre_inscricao date,
    data_fim_previsto date NOT NULL,
    data_fim_realizacao date,
    data_inicio_pre_inscricao date,
    data_inicio_previsto date NOT NULL,
    data_inicio_realizacao date,
    frequencia_aprovacao numeric(19,2),
    nota_aprovacao numeric(19,2),
    modulo_unico boolean,
    mostrar_na_home boolean,
    objetivo_especifico character varying(2000),
    objetivo_geral character varying(2000),
    observacoes character varying(2000),
    observacoes_publicas character varying(2000),
    permite_certificado boolean,
    permite_pre_inscricao boolean,
    publicado boolean,
    resultado_esperado character varying(2000),
    titulo character varying(255) NOT NULL,
    vagas integer NOT NULL,
    area_tribunal_id bigint,
    eixo_tematico_id bigint NOT NULL,
    imagem_id bigint,
    localizacao_id bigint NOT NULL,
    modalidade_id bigint NOT NULL,
    provedor_id bigint NOT NULL,
    responsavel_evento bigint NOT NULL,
    tipo_evento_id bigint NOT NULL
);


ALTER TABLE evento OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 1722300)
-- Name: evento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE evento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE evento_id_seq OWNER TO postgres;

--
-- TOC entry 2605 (class 0 OID 0)
-- Dependencies: 207
-- Name: evento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE evento_id_seq OWNED BY evento.id;


--
-- TOC entry 210 (class 1259 OID 1722313)
-- Name: evento_publico_alvo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE evento_publico_alvo (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    evento_id bigint NOT NULL,
    publico_alvo_id bigint NOT NULL
);


ALTER TABLE evento_publico_alvo OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 1722311)
-- Name: evento_publico_alvo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE evento_publico_alvo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE evento_publico_alvo_id_seq OWNER TO postgres;

--
-- TOC entry 2606 (class 0 OID 0)
-- Dependencies: 209
-- Name: evento_publico_alvo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE evento_publico_alvo_id_seq OWNED BY evento_publico_alvo.id;


--
-- TOC entry 212 (class 1259 OID 1722321)
-- Name: frequencia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE frequencia (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    encontro integer NOT NULL,
    presenca boolean,
    modulo_id bigint NOT NULL,
    participante_id bigint NOT NULL
);


ALTER TABLE frequencia OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 1722319)
-- Name: frequencia_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE frequencia_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE frequencia_id_seq OWNER TO postgres;

--
-- TOC entry 2607 (class 0 OID 0)
-- Dependencies: 211
-- Name: frequencia_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE frequencia_id_seq OWNED BY frequencia.id;


--
-- TOC entry 214 (class 1259 OID 1722329)
-- Name: gasto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE gasto (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    ano_processo integer,
    data_empenho timestamp without time zone,
    numero_empenho integer,
    observacao character varying(2000),
    seq_processo integer,
    valor numeric(19,2) NOT NULL,
    evento_id bigint NOT NULL,
    fonte_gasto_id bigint NOT NULL,
    tipo_id bigint NOT NULL
);


ALTER TABLE gasto OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 1722327)
-- Name: gasto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE gasto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gasto_id_seq OWNER TO postgres;

--
-- TOC entry 2608 (class 0 OID 0)
-- Dependencies: 213
-- Name: gasto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE gasto_id_seq OWNED BY gasto.id;


--
-- TOC entry 216 (class 1259 OID 1722340)
-- Name: indicacao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE indicacao (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    aprovado boolean,
    data_avaliacao timestamp without time zone,
    justificativa_chefe character varying(2000),
    avaliador_id bigint,
    chefe_id bigint,
    evento_id bigint NOT NULL,
    participante_id bigint NOT NULL
);


ALTER TABLE indicacao OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 1722338)
-- Name: indicacao_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE indicacao_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE indicacao_id_seq OWNER TO postgres;

--
-- TOC entry 2609 (class 0 OID 0)
-- Dependencies: 215
-- Name: indicacao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE indicacao_id_seq OWNED BY indicacao.id;


--
-- TOC entry 218 (class 1259 OID 1722351)
-- Name: inscricao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE inscricao (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    autorizada boolean,
    data_avaliacao_preinscricao timestamp without time zone,
    justificativa_participante character varying(2000),
    pre_inscricao boolean,
    avaliador_id bigint,
    evento_id bigint NOT NULL,
    indicacao_id bigint,
    participante_id bigint NOT NULL
);


ALTER TABLE inscricao OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 1722349)
-- Name: inscricao_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE inscricao_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inscricao_id_seq OWNER TO postgres;

--
-- TOC entry 2610 (class 0 OID 0)
-- Dependencies: 217
-- Name: inscricao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE inscricao_id_seq OWNED BY inscricao.id;


--
-- TOC entry 220 (class 1259 OID 1722362)
-- Name: instrutor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE instrutor (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    instituicao character varying(255),
    observacao character varying(2000),
    perfil character varying(2000),
    codsecao character varying(255),
    arquivo_assinatura_id bigint,
    arquivo_curriculo_id bigint,
    endereco_id bigint,
    formacao_academica_id bigint,
    nivel_escolaridade_id bigint,
    pessoa_id bigint,
    arquivo_projeto_id bigint,
    situacao_instrutor_id bigint,
    tipo_instrutor_id bigint NOT NULL
);


ALTER TABLE instrutor OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 1722360)
-- Name: instrutor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE instrutor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE instrutor_id_seq OWNER TO postgres;

--
-- TOC entry 2611 (class 0 OID 0)
-- Dependencies: 219
-- Name: instrutor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE instrutor_id_seq OWNED BY instrutor.id;


--
-- TOC entry 222 (class 1259 OID 1722373)
-- Name: localizacao_evento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE localizacao_evento (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    descricao character varying(255) NOT NULL,
    endereco_id bigint
);


ALTER TABLE localizacao_evento OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 1722371)
-- Name: localizacao_evento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE localizacao_evento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE localizacao_evento_id_seq OWNER TO postgres;

--
-- TOC entry 2612 (class 0 OID 0)
-- Dependencies: 221
-- Name: localizacao_evento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE localizacao_evento_id_seq OWNED BY localizacao_evento.id;


--
-- TOC entry 224 (class 1259 OID 1722381)
-- Name: log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE log (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    entidade_id bigint,
    operacao character varying(255),
    tabela character varying(255),
    usuario_id bigint
);


ALTER TABLE log OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 1722379)
-- Name: log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE log_id_seq OWNER TO postgres;

--
-- TOC entry 2613 (class 0 OID 0)
-- Dependencies: 223
-- Name: log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE log_id_seq OWNED BY log.id;


--
-- TOC entry 226 (class 1259 OID 1722392)
-- Name: material; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE material (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    arquivo_id bigint NOT NULL,
    evento_id bigint NOT NULL,
    modulo_id bigint,
    tipo_material_id bigint NOT NULL
);


ALTER TABLE material OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 1722390)
-- Name: material_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE material_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE material_id_seq OWNER TO postgres;

--
-- TOC entry 2614 (class 0 OID 0)
-- Dependencies: 225
-- Name: material_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE material_id_seq OWNED BY material.id;


--
-- TOC entry 228 (class 1259 OID 1722400)
-- Name: modulo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE modulo (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    carga_horaria numeric(19,2) NOT NULL,
    data_fim timestamp without time zone NOT NULL,
    data_inicio timestamp without time zone NOT NULL,
    frequencia_aprovacao numeric(19,2),
    hora_fim_turno1 character varying(5),
    hora_fim_turno2 character varying(5),
    hora_inicio_turno1 character varying(5),
    hora_inicio_turno2 character varying(5),
    nota_aprovacao numeric(19,2) NOT NULL,
    observacao character varying(2000),
    quantidade_encontros integer NOT NULL,
    titulo character varying(255) NOT NULL,
    evento_id bigint NOT NULL
);


ALTER TABLE modulo OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 1722398)
-- Name: modulo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE modulo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE modulo_id_seq OWNER TO postgres;

--
-- TOC entry 2615 (class 0 OID 0)
-- Dependencies: 227
-- Name: modulo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE modulo_id_seq OWNED BY modulo.id;


--
-- TOC entry 230 (class 1259 OID 1722411)
-- Name: modulo_instrutor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE modulo_instrutor (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    instrutor_id bigint NOT NULL,
    modulo_id bigint NOT NULL
);


ALTER TABLE modulo_instrutor OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 1722409)
-- Name: modulo_instrutor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE modulo_instrutor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE modulo_instrutor_id_seq OWNER TO postgres;

--
-- TOC entry 2616 (class 0 OID 0)
-- Dependencies: 229
-- Name: modulo_instrutor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE modulo_instrutor_id_seq OWNED BY modulo_instrutor.id;


--
-- TOC entry 232 (class 1259 OID 1722419)
-- Name: nota; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE nota (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    valor numeric(19,2) NOT NULL,
    modulo_id bigint NOT NULL,
    participante_id bigint NOT NULL
);


ALTER TABLE nota OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 1722417)
-- Name: nota_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE nota_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE nota_id_seq OWNER TO postgres;

--
-- TOC entry 2617 (class 0 OID 0)
-- Dependencies: 231
-- Name: nota_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE nota_id_seq OWNED BY nota.id;


--
-- TOC entry 234 (class 1259 OID 1722427)
-- Name: opcao_questionario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE opcao_questionario (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    ativo boolean NOT NULL,
    descricao character varying(255),
    ordem integer,
    questionario_id bigint NOT NULL
);


ALTER TABLE opcao_questionario OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 1722425)
-- Name: opcao_questionario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE opcao_questionario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE opcao_questionario_id_seq OWNER TO postgres;

--
-- TOC entry 2618 (class 0 OID 0)
-- Dependencies: 233
-- Name: opcao_questionario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE opcao_questionario_id_seq OWNED BY opcao_questionario.id;


--
-- TOC entry 236 (class 1259 OID 1722435)
-- Name: pacote_alertas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE pacote_alertas (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    mensagem character varying(2000) NOT NULL,
    nome character varying(500) NOT NULL,
    evento_id bigint,
    tipo_id bigint NOT NULL
);


ALTER TABLE pacote_alertas OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 1722433)
-- Name: pacote_alertas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE pacote_alertas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pacote_alertas_id_seq OWNER TO postgres;

--
-- TOC entry 2619 (class 0 OID 0)
-- Dependencies: 235
-- Name: pacote_alertas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE pacote_alertas_id_seq OWNED BY pacote_alertas.id;


--
-- TOC entry 238 (class 1259 OID 1722446)
-- Name: pais; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE pais (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    descricao character varying(255) NOT NULL
);


ALTER TABLE pais OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 1722444)
-- Name: pais_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE pais_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pais_id_seq OWNER TO postgres;

--
-- TOC entry 2620 (class 0 OID 0)
-- Dependencies: 237
-- Name: pais_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE pais_id_seq OWNED BY pais.id;


--
-- TOC entry 240 (class 1259 OID 1722454)
-- Name: participante; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE participante (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    cargo character varying(255),
    codmunicipio integer,
    codorgao integer,
    data_admissao date,
    instituicao character varying(255),
    lotacao character varying(255),
    matricula character varying(20),
    observacao character varying(2000),
    outra_necessidade_especial character varying(255),
    pne boolean,
    profissao character varying(255),
    endereco_id bigint,
    escolaridade_id bigint,
    formacao_academica_id bigint,
    mandato_id bigint,
    pessoa_id bigint NOT NULL,
    publico_alvo_id bigint NOT NULL,
    tipo_necessidade_especial_id bigint
);


ALTER TABLE participante OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 1722452)
-- Name: participante_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE participante_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE participante_id_seq OWNER TO postgres;

--
-- TOC entry 2621 (class 0 OID 0)
-- Dependencies: 239
-- Name: participante_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE participante_id_seq OWNED BY participante.id;


--
-- TOC entry 242 (class 1259 OID 1722465)
-- Name: perfil_usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE perfil_usuario (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    perfil character varying(255),
    usuario_id bigint NOT NULL
);


ALTER TABLE perfil_usuario OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 1722463)
-- Name: perfil_usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE perfil_usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE perfil_usuario_id_seq OWNER TO postgres;

--
-- TOC entry 2622 (class 0 OID 0)
-- Dependencies: 241
-- Name: perfil_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE perfil_usuario_id_seq OWNED BY perfil_usuario.id;


--
-- TOC entry 244 (class 1259 OID 1722473)
-- Name: pessoa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE pessoa (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    celular character varying(15),
    cpf character varying(14),
    data_nascimento date,
    email character varying(255),
    nome character varying(255) NOT NULL,
    telefone character varying(15),
    sexo_id bigint
);


ALTER TABLE pessoa OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 1722471)
-- Name: pessoa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE pessoa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pessoa_id_seq OWNER TO postgres;

--
-- TOC entry 2623 (class 0 OID 0)
-- Dependencies: 243
-- Name: pessoa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE pessoa_id_seq OWNED BY pessoa.id;


--
-- TOC entry 246 (class 1259 OID 1722484)
-- Name: provedor_evento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE provedor_evento (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    agencia character varying(50),
    celular character varying(14),
    cnpj character varying(18),
    conta_corrente character varying(20),
    contato character varying(255),
    descricao character varying(255) NOT NULL,
    email character varying(255),
    telefone character varying(14),
    banco_id bigint,
    endereco_id bigint
);


ALTER TABLE provedor_evento OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 1722482)
-- Name: provedor_evento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE provedor_evento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE provedor_evento_id_seq OWNER TO postgres;

--
-- TOC entry 2624 (class 0 OID 0)
-- Dependencies: 245
-- Name: provedor_evento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE provedor_evento_id_seq OWNED BY provedor_evento.id;


--
-- TOC entry 248 (class 1259 OID 1722495)
-- Name: questionario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE questionario (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    ativo boolean NOT NULL,
    para_instrutor boolean,
    pergunta character varying(2000) NOT NULL
);


ALTER TABLE questionario OWNER TO postgres;

--
-- TOC entry 250 (class 1259 OID 1722506)
-- Name: questionario_avaliacao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE questionario_avaliacao (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    avaliacao_id bigint NOT NULL,
    instrutor_id bigint,
    opcao_id bigint NOT NULL,
    questionario_id bigint NOT NULL
);


ALTER TABLE questionario_avaliacao OWNER TO postgres;

--
-- TOC entry 249 (class 1259 OID 1722504)
-- Name: questionario_avaliacao_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE questionario_avaliacao_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE questionario_avaliacao_id_seq OWNER TO postgres;

--
-- TOC entry 2625 (class 0 OID 0)
-- Dependencies: 249
-- Name: questionario_avaliacao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE questionario_avaliacao_id_seq OWNED BY questionario_avaliacao.id;


--
-- TOC entry 247 (class 1259 OID 1722493)
-- Name: questionario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE questionario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE questionario_id_seq OWNER TO postgres;

--
-- TOC entry 2626 (class 0 OID 0)
-- Dependencies: 247
-- Name: questionario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE questionario_id_seq OWNED BY questionario.id;


--
-- TOC entry 252 (class 1259 OID 1722514)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE usuario (
    id bigint NOT NULL,
    data_cadastro timestamp without time zone NOT NULL,
    matricula character varying(255),
    nome_usuario character varying(255),
    codsecao character varying(255),
    senha character varying(255),
    token_recup_senha character varying(255),
    usuario_id bigint,
    participante_id bigint,
    pessoa_id bigint NOT NULL,
    tipo_usuario_id bigint NOT NULL
);


ALTER TABLE usuario OWNER TO postgres;

--
-- TOC entry 251 (class 1259 OID 1722512)
-- Name: usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuario_id_seq OWNER TO postgres;

--
-- TOC entry 2627 (class 0 OID 0)
-- Dependencies: 251
-- Name: usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE usuario_id_seq OWNED BY usuario.id;


--
-- TOC entry 2218 (class 2604 OID 1722199)
-- Name: alerta id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alerta ALTER COLUMN id SET DEFAULT nextval('alerta_id_seq'::regclass);


--
-- TOC entry 2219 (class 2604 OID 1722210)
-- Name: anexo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY anexo ALTER COLUMN id SET DEFAULT nextval('anexo_id_seq'::regclass);


--
-- TOC entry 2220 (class 2604 OID 1722221)
-- Name: arquivo_adm id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY arquivo_adm ALTER COLUMN id SET DEFAULT nextval('arquivo_adm_id_seq'::regclass);


--
-- TOC entry 2221 (class 2604 OID 1722229)
-- Name: avaliacao id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY avaliacao ALTER COLUMN id SET DEFAULT nextval('avaliacao_id_seq'::regclass);


--
-- TOC entry 2222 (class 2604 OID 1722240)
-- Name: avaliacao_eficacia id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY avaliacao_eficacia ALTER COLUMN id SET DEFAULT nextval('avaliacao_eficacia_id_seq'::regclass);


--
-- TOC entry 2223 (class 2604 OID 1722251)
-- Name: banco_febraban id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY banco_febraban ALTER COLUMN id SET DEFAULT nextval('banco_febraban_id_seq'::regclass);


--
-- TOC entry 2224 (class 2604 OID 1722259)
-- Name: certificado id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY certificado ALTER COLUMN id SET DEFAULT nextval('certificado_id_seq'::regclass);


--
-- TOC entry 2225 (class 2604 OID 1722267)
-- Name: cidade id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cidade ALTER COLUMN id SET DEFAULT nextval('cidade_id_seq'::regclass);


--
-- TOC entry 2226 (class 2604 OID 1722275)
-- Name: dominio id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dominio ALTER COLUMN id SET DEFAULT nextval('dominio_id_seq'::regclass);


--
-- TOC entry 2227 (class 2604 OID 1722286)
-- Name: endereco id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY endereco ALTER COLUMN id SET DEFAULT nextval('endereco_id_seq'::regclass);


--
-- TOC entry 2228 (class 2604 OID 1722297)
-- Name: estado id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY estado ALTER COLUMN id SET DEFAULT nextval('estado_id_seq'::regclass);


--
-- TOC entry 2229 (class 2604 OID 1722305)
-- Name: evento id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento ALTER COLUMN id SET DEFAULT nextval('evento_id_seq'::regclass);


--
-- TOC entry 2230 (class 2604 OID 1722316)
-- Name: evento_publico_alvo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento_publico_alvo ALTER COLUMN id SET DEFAULT nextval('evento_publico_alvo_id_seq'::regclass);


--
-- TOC entry 2231 (class 2604 OID 1722324)
-- Name: frequencia id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY frequencia ALTER COLUMN id SET DEFAULT nextval('frequencia_id_seq'::regclass);


--
-- TOC entry 2232 (class 2604 OID 1722332)
-- Name: gasto id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gasto ALTER COLUMN id SET DEFAULT nextval('gasto_id_seq'::regclass);


--
-- TOC entry 2233 (class 2604 OID 1722343)
-- Name: indicacao id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY indicacao ALTER COLUMN id SET DEFAULT nextval('indicacao_id_seq'::regclass);


--
-- TOC entry 2234 (class 2604 OID 1722354)
-- Name: inscricao id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inscricao ALTER COLUMN id SET DEFAULT nextval('inscricao_id_seq'::regclass);


--
-- TOC entry 2235 (class 2604 OID 1722365)
-- Name: instrutor id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY instrutor ALTER COLUMN id SET DEFAULT nextval('instrutor_id_seq'::regclass);


--
-- TOC entry 2236 (class 2604 OID 1722376)
-- Name: localizacao_evento id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY localizacao_evento ALTER COLUMN id SET DEFAULT nextval('localizacao_evento_id_seq'::regclass);


--
-- TOC entry 2237 (class 2604 OID 1722384)
-- Name: log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY log ALTER COLUMN id SET DEFAULT nextval('log_id_seq'::regclass);


--
-- TOC entry 2238 (class 2604 OID 1722395)
-- Name: material id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY material ALTER COLUMN id SET DEFAULT nextval('material_id_seq'::regclass);


--
-- TOC entry 2239 (class 2604 OID 1722403)
-- Name: modulo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY modulo ALTER COLUMN id SET DEFAULT nextval('modulo_id_seq'::regclass);


--
-- TOC entry 2240 (class 2604 OID 1722414)
-- Name: modulo_instrutor id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY modulo_instrutor ALTER COLUMN id SET DEFAULT nextval('modulo_instrutor_id_seq'::regclass);


--
-- TOC entry 2241 (class 2604 OID 1722422)
-- Name: nota id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY nota ALTER COLUMN id SET DEFAULT nextval('nota_id_seq'::regclass);


--
-- TOC entry 2242 (class 2604 OID 1722430)
-- Name: opcao_questionario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY opcao_questionario ALTER COLUMN id SET DEFAULT nextval('opcao_questionario_id_seq'::regclass);


--
-- TOC entry 2243 (class 2604 OID 1722438)
-- Name: pacote_alertas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pacote_alertas ALTER COLUMN id SET DEFAULT nextval('pacote_alertas_id_seq'::regclass);


--
-- TOC entry 2244 (class 2604 OID 1722449)
-- Name: pais id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pais ALTER COLUMN id SET DEFAULT nextval('pais_id_seq'::regclass);


--
-- TOC entry 2245 (class 2604 OID 1722457)
-- Name: participante id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY participante ALTER COLUMN id SET DEFAULT nextval('participante_id_seq'::regclass);


--
-- TOC entry 2246 (class 2604 OID 1722468)
-- Name: perfil_usuario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil_usuario ALTER COLUMN id SET DEFAULT nextval('perfil_usuario_id_seq'::regclass);


--
-- TOC entry 2247 (class 2604 OID 1722476)
-- Name: pessoa id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pessoa ALTER COLUMN id SET DEFAULT nextval('pessoa_id_seq'::regclass);


--
-- TOC entry 2248 (class 2604 OID 1722487)
-- Name: provedor_evento id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY provedor_evento ALTER COLUMN id SET DEFAULT nextval('provedor_evento_id_seq'::regclass);


--
-- TOC entry 2249 (class 2604 OID 1722498)
-- Name: questionario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questionario ALTER COLUMN id SET DEFAULT nextval('questionario_id_seq'::regclass);


--
-- TOC entry 2250 (class 2604 OID 1722509)
-- Name: questionario_avaliacao id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questionario_avaliacao ALTER COLUMN id SET DEFAULT nextval('questionario_avaliacao_id_seq'::regclass);


--
-- TOC entry 2251 (class 2604 OID 1722517)
-- Name: usuario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario ALTER COLUMN id SET DEFAULT nextval('usuario_id_seq'::regclass);


--
-- TOC entry 2520 (class 0 OID 1722196)
-- Dependencies: 186
-- Data for Name: alerta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY alerta (id, data_cadastro, ativo, data_visualizacao, mensagem, pacote_alertas_id, participante_id, tipo_id) FROM stdin;
\.


--
-- TOC entry 2628 (class 0 OID 0)
-- Dependencies: 185
-- Name: alerta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('alerta_id_seq', 1, false);


--
-- TOC entry 2522 (class 0 OID 1722207)
-- Dependencies: 188
-- Data for Name: anexo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY anexo (id, data_cadastro, arquivo_ged, descricao, tipo_arquivo) FROM stdin;
\.


--
-- TOC entry 2629 (class 0 OID 0)
-- Dependencies: 187
-- Name: anexo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('anexo_id_seq', 1, false);


--
-- TOC entry 2524 (class 0 OID 1722218)
-- Dependencies: 190
-- Data for Name: arquivo_adm; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY arquivo_adm (id, data_cadastro, arquivo_id, evento_id, tipo_arquivoadm_id) FROM stdin;
\.


--
-- TOC entry 2630 (class 0 OID 0)
-- Dependencies: 189
-- Name: arquivo_adm_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('arquivo_adm_id_seq', 1, false);


--
-- TOC entry 2526 (class 0 OID 1722226)
-- Dependencies: 192
-- Data for Name: avaliacao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY avaliacao (id, data_cadastro, observacao, modulo_id, participante_id) FROM stdin;
\.


--
-- TOC entry 2528 (class 0 OID 1722237)
-- Dependencies: 194
-- Data for Name: avaliacao_eficacia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY avaliacao_eficacia (id, data_cadastro, melhoria_desempenho, observacao, desempenho_servico_id, evento_id, indicacao_id, participante_id) FROM stdin;
\.


--
-- TOC entry 2631 (class 0 OID 0)
-- Dependencies: 193
-- Name: avaliacao_eficacia_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('avaliacao_eficacia_id_seq', 1, false);


--
-- TOC entry 2632 (class 0 OID 0)
-- Dependencies: 191
-- Name: avaliacao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('avaliacao_id_seq', 1, false);


--
-- TOC entry 2530 (class 0 OID 1722248)
-- Dependencies: 196
-- Data for Name: banco_febraban; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY banco_febraban (id, data_cadastro, codigo, descricao) FROM stdin;
1	2016-02-18 00:00:00	246	banco_febraban ABC Brasil S.A.
2	2016-02-18 00:00:00	356	banco_febraban ABN AMRO Real S.A.
3	2016-02-18 00:00:00	025	banco_febraban Alfa S.A.
4	2016-02-18 00:00:00	641	banco_febraban Alvorada S.A.
5	2016-02-18 00:00:00	029	banco_febraban Banerj S.A.
6	2016-02-18 00:00:00	038	banco_febraban Banestado S.A.
7	2016-02-18 00:00:00	740	banco_febraban Barclays S.A.
8	2016-02-18 00:00:00	107	banco_febraban BBM S.A.
9	2016-02-18 00:00:00	031	banco_febraban Beg S.A.
10	2016-02-18 00:00:00	036	banco_febraban Bem S.A.
11	2016-02-18 00:00:00	096	banco_febraban BM&F de Serviços de Liquidação e Custódia S.A
12	2016-02-18 00:00:00	394	banco_febraban BMC S.A.
13	2016-02-18 00:00:00	318	banco_febraban BMG S.A.
14	2016-02-18 00:00:00	752	banco_febraban BNP Paribas Brasil S.A.
15	2016-02-18 00:00:00	248	banco_febraban Boavista Interatlântico S.A.
16	2016-02-18 00:00:00	237	banco_febraban Bradesco S.A.
17	2016-02-18 00:00:00	225	banco_febraban Brascan S.A.
18	2016-02-18 00:00:00	263	banco_febraban Cacique S.A.
19	2016-02-18 00:00:00	222	banco_febraban Calyon Brasil S.A.
20	2016-02-18 00:00:00	040	banco_febraban Cargill S.A.
21	2016-02-18 00:00:00	745	banco_febraban Citibank S.A.
22	2016-02-18 00:00:00	215	banco_febraban Comercial e de Investimento Sudameris S.A.
23	2016-02-18 00:00:00	756	banco_febraban Cooperativo do Brasil S.A. – banco_febrabanOB
24	2016-02-18 00:00:00	748	banco_febraban Cooperativo Sicredi S.A. – BANSICREDI
25	2016-02-18 00:00:00	505	banco_febraban Credit Suisse (Brasil) S.A.
26	2016-02-18 00:00:00	229	banco_febraban Cruzeiro do Sul S.A.
27	2016-02-18 00:00:00	003	banco_febraban da Amazônia S.A.
28	2016-02-18 00:00:00	707	banco_febraban Daycoval S.A.
29	2016-02-18 00:00:00	024	banco_febraban de Pernambuco S.A. – BANDEPE
30	2016-02-18 00:00:00	456	banco_febraban de Tokyo-Mitsubishi UFJ Brasil S.A.
31	2016-02-18 00:00:00	214	banco_febraban Dibens S.A.
32	2016-02-18 00:00:00	001	banco_febraban do Brasil S.A.
33	2016-02-18 00:00:00	027	banco_febraban do Estado de Santa Catarina S.A.
34	2016-02-18 00:00:00	047	banco_febraban do Estado de Sergipe S.A.
35	2016-02-18 00:00:00	037	banco_febraban do Estado do Pará S.A.
36	2016-02-18 00:00:00	041	banco_febraban do Estado do Rio Grande do Sul S.A.
37	2016-02-18 00:00:00	004	banco_febraban do Nordeste do Brasil S.A.
38	2016-02-18 00:00:00	265	banco_febraban Fator S.A.
39	2016-02-18 00:00:00	224	banco_febraban Fibra S.A.
40	2016-02-18 00:00:00	175	banco_febraban Finasa S.A.
41	2016-02-18 00:00:00	252	banco_febraban Fininvest S.A.
42	2016-02-18 00:00:00	233	banco_febraban GE Capital S.A.
43	2016-02-18 00:00:00	734	banco_febraban Gerdau S.A.
44	2016-02-18 00:00:00	612	banco_febraban Guanabara S.A.
45	2016-02-18 00:00:00	063	banco_febraban Ibi S.A. banco_febraban Múltiplo
46	2016-02-18 00:00:00	604	banco_febraban Industrial do Brasil S.A.
47	2016-02-18 00:00:00	320	banco_febraban Industrial e Comercial S.A.
48	2016-02-18 00:00:00	653	banco_febraban Indusval S.A.
49	2016-02-18 00:00:00	630	banco_febraban Intercap S.A.
50	2016-02-18 00:00:00	249	banco_febraban Investcred Unibanco_febraban S.A.
51	2016-02-18 00:00:00	184-8	banco_febraban Itaú BBA S.A.
52	2016-02-18 00:00:00	652	banco_febraban Itaú Holding Financeira S.A.
53	2016-02-18 00:00:00	341	banco_febraban Itaú S.A.
54	2016-02-18 00:00:00	479	banco_febraban ItaúBank S.A
55	2016-02-18 00:00:00	376	banco_febraban J. P. Morgan S.A.
56	2016-02-18 00:00:00	074	banco_febraban J. Safra S.A.
57	2016-02-18 00:00:00	600	banco_febraban Luso Brasileiro S.A.
58	2016-02-18 00:00:00	392	banco_febraban Mercantil de São Paulo S.A.
59	2016-02-18 00:00:00	389	banco_febraban Mercantil do Brasil S.A.
60	2016-02-18 00:00:00	755	banco_febraban Merrill Lynch de Investimentos S.A.
61	2016-02-18 00:00:00	151	banco_febraban Nossa Caixa S.A.
62	2016-02-18 00:00:00	045	banco_febraban Opportunity S.A.
63	2016-02-18 00:00:00	623	banco_febraban Panamericano S.A.
64	2016-02-18 00:00:00	611	banco_febraban Paulista S.A.
65	2016-02-18 00:00:00	643	banco_febraban Pine S.A.
66	2016-02-18 00:00:00	638	banco_febraban Prosper S.A.
67	2016-02-18 00:00:00	747	banco_febraban Rabobank International Brasil S.A.
68	2016-02-18 00:00:00	633	banco_febraban Rendimento S.A.
69	2016-02-18 00:00:00	072	banco_febraban Rural Mais S.A.
70	2016-02-18 00:00:00	453	banco_febraban Rural S.A.
71	2016-02-18 00:00:00	422	banco_febraban Safra S.A.
72	2016-02-18 00:00:00	008	banco_febraban Santander Banespa S.A.
73	2016-02-18 00:00:00	250	banco_febraban Schahin S.A.
74	2016-02-18 00:00:00	749	banco_febraban Simples S.A.
75	2016-02-18 00:00:00	366	banco_febraban Société Générale Brasil S.A.
76	2016-02-18 00:00:00	637	banco_febraban Sofisa S.A.
77	2016-02-18 00:00:00	347	banco_febraban Sudameris Brasil S.A.
78	2016-02-18 00:00:00	464	banco_febraban Sumitomo Mitsui Brasileiro S.A.
79	2016-02-18 00:00:00	634	banco_febraban Triângulo S.A.
80	2016-02-18 00:00:00	208	banco_febraban UBS Pactual S.A.
81	2016-02-18 00:00:00	247	banco_febraban UBS S.A.
82	2016-02-18 00:00:00	116	banco_febraban Único S.A.
83	2016-02-18 00:00:00	655	banco_febraban Votorantim S.A.
84	2016-02-18 00:00:00	610	banco_febraban VR S.A.
85	2016-02-18 00:00:00	370	banco_febraban WestLB do Brasil S.A.
86	2016-02-18 00:00:00	021	BANESTES S.A. banco_febraban do Estado do Espírito Santo
87	2016-02-18 00:00:00	719	Banif-banco_febraban Internacional do Funchal (Brasil)S.A.
88	2016-02-18 00:00:00	204	Bankpar banco_febraban Multiplo S.A.
89	2016-02-18 00:00:00	073-6	BB banco_febraban Popular do Brasil S.A.
90	2016-02-18 00:00:00	069-8	BPN Brasil banco_febraban Mútiplo S.A.
91	2016-02-18 00:00:00	070	BRB – banco_febraban de Brasília S.A.
92	2016-02-18 00:00:00	104	Caixa Econômica Federal
93	2016-02-18 00:00:00	477	Citibank N.A.
94	2016-02-18 00:00:00	487	Deutsche Bank S.A. – banco_febraban Alemão
95	2016-02-18 00:00:00	751	Dresdner Bank Brasil S.A. – banco_febraban Múltiplo
96	2016-02-18 00:00:00	210	Dresdner Bank Lateinamerika Aktiengesellschaft
97	2016-02-18 00:00:00	062	Hipercard banco_febraban Múltiplo S.A.
98	2016-02-18 00:00:00	399	HSBC Bank Brasil S.A. – banco_febraban Múltiplo
99	2016-02-18 00:00:00	492	ING Bank N.V.
100	2016-02-18 00:00:00	488	JPMorgan Chase Bank
101	2016-02-18 00:00:00	065	Lemon Bank banco_febraban Múltiplo S.A.
102	2016-02-18 00:00:00	409	UNIbanco_febraban – União de banco_febraban Brasileiros S.A.
103	2016-02-18 00:00:00	230	Unicard banco_febraban Múltiplo S.A.
\.


--
-- TOC entry 2633 (class 0 OID 0)
-- Dependencies: 195
-- Name: banco_febraban_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('banco_febraban_id_seq', 1, false);


--
-- TOC entry 2532 (class 0 OID 1722256)
-- Dependencies: 198
-- Data for Name: certificado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY certificado (id, data_cadastro, codigo_verificacao, data_emissao, arquivo_id, evento_id, participante_id) FROM stdin;
\.


--
-- TOC entry 2634 (class 0 OID 0)
-- Dependencies: 197
-- Name: certificado_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('certificado_id_seq', 1, false);


--
-- TOC entry 2534 (class 0 OID 1722264)
-- Dependencies: 200
-- Data for Name: cidade; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cidade (id, data_cadastro, descricao, estado_id) FROM stdin;
1	2016-02-18 00:00:00	Acrelândia	1
2	2016-02-18 00:00:00	Assis Brasil	1
3	2016-02-18 00:00:00	Brasiléia	1
4	2016-02-18 00:00:00	Bujari	1
5	2016-02-18 00:00:00	Capixaba	1
6	2016-02-18 00:00:00	Cruzeiro do Sul	1
7	2016-02-18 00:00:00	Epitaciolândia	1
8	2016-02-18 00:00:00	Feijó	1
9	2016-02-18 00:00:00	Jordão	1
10	2016-02-18 00:00:00	Mâncio Lima	1
11	2016-02-18 00:00:00	Manoel Urbano	1
12	2016-02-18 00:00:00	Marechal Thaumaturgo	1
13	2016-02-18 00:00:00	Plácido de Castro	1
14	2016-02-18 00:00:00	Porto Acre	1
15	2016-02-18 00:00:00	Porto Walter	1
16	2016-02-18 00:00:00	Rio Branco	1
17	2016-02-18 00:00:00	Rodrigues Alves	1
18	2016-02-18 00:00:00	Santa Rosa do Purus	1
19	2016-02-18 00:00:00	Sena Madureira	1
20	2016-02-18 00:00:00	Senador Guiomard	1
21	2016-02-18 00:00:00	Tarauacá	1
22	2016-02-18 00:00:00	Xapuri	1
23	2016-02-18 00:00:00	Água Branca	2
24	2016-02-18 00:00:00	Anadia	2
25	2016-02-18 00:00:00	Arapiraca	2
26	2016-02-18 00:00:00	Atalaia	2
27	2016-02-18 00:00:00	Barra de Santo Antônio	2
28	2016-02-18 00:00:00	Barra de São Miguel	2
29	2016-02-18 00:00:00	Batalha	2
30	2016-02-18 00:00:00	Belém	2
31	2016-02-18 00:00:00	Belo Monte	2
32	2016-02-18 00:00:00	Boca da Mata	2
33	2016-02-18 00:00:00	Branquinha	2
34	2016-02-18 00:00:00	Cacimbinhas	2
35	2016-02-18 00:00:00	Cajueiro	2
36	2016-02-18 00:00:00	Campestre	2
37	2016-02-18 00:00:00	Campo Alegre	2
38	2016-02-18 00:00:00	Campo Grande	2
39	2016-02-18 00:00:00	Canapi	2
40	2016-02-18 00:00:00	Capela	2
41	2016-02-18 00:00:00	Carneiros	2
42	2016-02-18 00:00:00	Chã Preta	2
43	2016-02-18 00:00:00	Coité do Nóia	2
44	2016-02-18 00:00:00	Colônia Leopoldina	2
45	2016-02-18 00:00:00	Coqueiro Seco	2
46	2016-02-18 00:00:00	Coruripe	2
47	2016-02-18 00:00:00	Craíbas	2
48	2016-02-18 00:00:00	Delmiro Gouveia	2
49	2016-02-18 00:00:00	Dois Riachos	2
50	2016-02-18 00:00:00	Estrela de Alagoas	2
51	2016-02-18 00:00:00	Feira Grande	2
52	2016-02-18 00:00:00	Feliz Deserto	2
53	2016-02-18 00:00:00	Flexeiras	2
54	2016-02-18 00:00:00	Girau do Ponciano	2
55	2016-02-18 00:00:00	Ibateguara	2
56	2016-02-18 00:00:00	Igaci	2
57	2016-02-18 00:00:00	Igreja Nova	2
58	2016-02-18 00:00:00	Inhapi	2
59	2016-02-18 00:00:00	Jacaré dos Homens	2
60	2016-02-18 00:00:00	Jacuípe	2
61	2016-02-18 00:00:00	Japaratinga	2
62	2016-02-18 00:00:00	Jaramataia	2
63	2016-02-18 00:00:00	Jequiá da Praia	2
64	2016-02-18 00:00:00	Joaquim Gomes	2
65	2016-02-18 00:00:00	Jundiá	2
66	2016-02-18 00:00:00	Junqueiro	2
67	2016-02-18 00:00:00	Lagoa da Canoa	2
68	2016-02-18 00:00:00	Limoeiro de Anadia	2
69	2016-02-18 00:00:00	Maceió	2
70	2016-02-18 00:00:00	Major Isidoro	2
71	2016-02-18 00:00:00	Mar Vermelho	2
72	2016-02-18 00:00:00	Maragogi	2
73	2016-02-18 00:00:00	Maravilha	2
74	2016-02-18 00:00:00	Marechal Deodoro	2
75	2016-02-18 00:00:00	Maribondo	2
76	2016-02-18 00:00:00	Mata Grande	2
77	2016-02-18 00:00:00	Matriz de Camaragibe	2
78	2016-02-18 00:00:00	Messias	2
79	2016-02-18 00:00:00	Minador do Negrão	2
80	2016-02-18 00:00:00	Monteirópolis	2
81	2016-02-18 00:00:00	Murici	2
82	2016-02-18 00:00:00	Novo Lino	2
83	2016-02-18 00:00:00	Olho d'Água das Flores	2
84	2016-02-18 00:00:00	Olho d'Água do Casado	2
85	2016-02-18 00:00:00	Olho d'Água Grande	2
86	2016-02-18 00:00:00	Olivença	2
87	2016-02-18 00:00:00	Ouro Branco	2
88	2016-02-18 00:00:00	Palestina	2
89	2016-02-18 00:00:00	Palmeira dos Índios	2
90	2016-02-18 00:00:00	Pão de Açúcar	2
91	2016-02-18 00:00:00	Pariconha	2
92	2016-02-18 00:00:00	Paripueira	2
93	2016-02-18 00:00:00	Passo de Camaragibe	2
94	2016-02-18 00:00:00	Paulo Jacinto	2
95	2016-02-18 00:00:00	Penedo	2
96	2016-02-18 00:00:00	Piaçabuçu	2
97	2016-02-18 00:00:00	Pilar	2
98	2016-02-18 00:00:00	Pindoba	2
99	2016-02-18 00:00:00	Piranhas	2
100	2016-02-18 00:00:00	Poço das Trincheiras	2
101	2016-02-18 00:00:00	Porto Calvo	2
102	2016-02-18 00:00:00	Porto de Pedras	2
103	2016-02-18 00:00:00	Porto Real do Colégio	2
104	2016-02-18 00:00:00	Quebrangulo	2
105	2016-02-18 00:00:00	Rio Largo	2
106	2016-02-18 00:00:00	Roteiro	2
107	2016-02-18 00:00:00	Santa Luzia do Norte	2
108	2016-02-18 00:00:00	Santana do Ipanema	2
109	2016-02-18 00:00:00	Santana do Mundaú	2
110	2016-02-18 00:00:00	São Brás	2
111	2016-02-18 00:00:00	São José da Laje	2
112	2016-02-18 00:00:00	São José da Tapera	2
113	2016-02-18 00:00:00	São Luís do Quitunde	2
114	2016-02-18 00:00:00	São Miguel dos Campos	2
115	2016-02-18 00:00:00	São Miguel dos Milagres	2
116	2016-02-18 00:00:00	São Sebastião	2
117	2016-02-18 00:00:00	Satuba	2
118	2016-02-18 00:00:00	Senador Rui Palmeira	2
119	2016-02-18 00:00:00	Tanque d'Arca	2
120	2016-02-18 00:00:00	Taquarana	2
121	2016-02-18 00:00:00	Teotônio Vilela	2
122	2016-02-18 00:00:00	Traipu	2
123	2016-02-18 00:00:00	União dos Palmares	2
124	2016-02-18 00:00:00	Viçosa	2
125	2016-02-18 00:00:00	Amapá	4
126	2016-02-18 00:00:00	Calçoene	4
127	2016-02-18 00:00:00	Cutias	4
128	2016-02-18 00:00:00	Ferreira Gomes	4
129	2016-02-18 00:00:00	Itaubal	4
130	2016-02-18 00:00:00	Laranjal do Jari	4
131	2016-02-18 00:00:00	Macapá	4
132	2016-02-18 00:00:00	Mazagão	4
133	2016-02-18 00:00:00	Oiapoque	4
134	2016-02-18 00:00:00	Pedra Branca do Amapari	4
135	2016-02-18 00:00:00	Porto Grande	4
136	2016-02-18 00:00:00	Pracuúba	4
137	2016-02-18 00:00:00	Santana	4
138	2016-02-18 00:00:00	Serra do Navio	4
139	2016-02-18 00:00:00	Tartarugalzinho	4
140	2016-02-18 00:00:00	Vitória do Jari	4
141	2016-02-18 00:00:00	Alvarães	3
142	2016-02-18 00:00:00	Amaturá	3
143	2016-02-18 00:00:00	Anamã	3
144	2016-02-18 00:00:00	Anori	3
145	2016-02-18 00:00:00	Apuí	3
146	2016-02-18 00:00:00	Atalaia do Norte	3
147	2016-02-18 00:00:00	Autazes	3
148	2016-02-18 00:00:00	Barcelos	3
149	2016-02-18 00:00:00	Barreirinha	3
150	2016-02-18 00:00:00	Benjamin Constant	3
151	2016-02-18 00:00:00	Beruri	3
152	2016-02-18 00:00:00	Boa Vista do Ramos	3
153	2016-02-18 00:00:00	Boca do Acre	3
154	2016-02-18 00:00:00	Borba	3
155	2016-02-18 00:00:00	Caapiranga	3
156	2016-02-18 00:00:00	Canutama	3
157	2016-02-18 00:00:00	Carauari	3
158	2016-02-18 00:00:00	Careiro	3
159	2016-02-18 00:00:00	Careiro da Várzea	3
160	2016-02-18 00:00:00	Coari	3
161	2016-02-18 00:00:00	Codajás	3
162	2016-02-18 00:00:00	Eirunepé	3
163	2016-02-18 00:00:00	Envira	3
164	2016-02-18 00:00:00	Fonte Boa	3
165	2016-02-18 00:00:00	Guajará	3
166	2016-02-18 00:00:00	Humaitá	3
167	2016-02-18 00:00:00	Ipixuna	3
168	2016-02-18 00:00:00	Iranduba	3
169	2016-02-18 00:00:00	Itacoatiara	3
170	2016-02-18 00:00:00	Itamarati	3
171	2016-02-18 00:00:00	Itapiranga	3
172	2016-02-18 00:00:00	Japurá	3
173	2016-02-18 00:00:00	Juruá	3
174	2016-02-18 00:00:00	Jutaí	3
175	2016-02-18 00:00:00	Lábrea	3
176	2016-02-18 00:00:00	Manacapuru	3
177	2016-02-18 00:00:00	Manaquiri	3
178	2016-02-18 00:00:00	Manaus	3
179	2016-02-18 00:00:00	Manicoré	3
180	2016-02-18 00:00:00	Maraã	3
181	2016-02-18 00:00:00	Maués	3
182	2016-02-18 00:00:00	Nhamundá	3
183	2016-02-18 00:00:00	Nova Olinda do Norte	3
184	2016-02-18 00:00:00	Novo Airão	3
185	2016-02-18 00:00:00	Novo Aripuanã	3
186	2016-02-18 00:00:00	Parintins	3
187	2016-02-18 00:00:00	Pauini	3
188	2016-02-18 00:00:00	Presidente Figueiredo	3
189	2016-02-18 00:00:00	Rio Preto da Eva	3
190	2016-02-18 00:00:00	Santa Isabel do Rio Negro	3
191	2016-02-18 00:00:00	Santo Antônio do Içá	3
192	2016-02-18 00:00:00	São Gabriel da Cachoeira	3
193	2016-02-18 00:00:00	São Paulo de Olivença	3
194	2016-02-18 00:00:00	São Sebastião do Uatumã	3
195	2016-02-18 00:00:00	Silves	3
196	2016-02-18 00:00:00	Tabatinga	3
197	2016-02-18 00:00:00	Tapauá	3
198	2016-02-18 00:00:00	Tefé	3
199	2016-02-18 00:00:00	Tonantins	3
200	2016-02-18 00:00:00	Uarini	3
201	2016-02-18 00:00:00	Urucará	3
202	2016-02-18 00:00:00	Urucurituba	3
203	2016-02-18 00:00:00	Abaíra	5
204	2016-02-18 00:00:00	Abaré	5
205	2016-02-18 00:00:00	Acajutiba	5
206	2016-02-18 00:00:00	Adustina	5
207	2016-02-18 00:00:00	Água Fria	5
208	2016-02-18 00:00:00	Aiquara	5
209	2016-02-18 00:00:00	Alagoinhas	5
210	2016-02-18 00:00:00	Alcobaça	5
211	2016-02-18 00:00:00	Almadina	5
212	2016-02-18 00:00:00	Amargosa	5
213	2016-02-18 00:00:00	Amélia Rodrigues	5
214	2016-02-18 00:00:00	América Dourada	5
215	2016-02-18 00:00:00	Anagé	5
216	2016-02-18 00:00:00	Andaraí	5
217	2016-02-18 00:00:00	Andorinha	5
218	2016-02-18 00:00:00	Angical	5
219	2016-02-18 00:00:00	Anguera	5
220	2016-02-18 00:00:00	Antas	5
221	2016-02-18 00:00:00	Antônio Cardoso	5
222	2016-02-18 00:00:00	Antônio Gonçalves	5
223	2016-02-18 00:00:00	Aporá	5
224	2016-02-18 00:00:00	Apuarema	5
225	2016-02-18 00:00:00	Araças	5
226	2016-02-18 00:00:00	Aracatu	5
227	2016-02-18 00:00:00	Araci	5
228	2016-02-18 00:00:00	Aramari	5
229	2016-02-18 00:00:00	Arataca	5
230	2016-02-18 00:00:00	Aratuípe	5
231	2016-02-18 00:00:00	Aurelino Leal	5
232	2016-02-18 00:00:00	Baianópolis	5
233	2016-02-18 00:00:00	Baixa Grande	5
234	2016-02-18 00:00:00	Banzaê	5
235	2016-02-18 00:00:00	Barra	5
236	2016-02-18 00:00:00	Barra da Estiva	5
237	2016-02-18 00:00:00	Barra do Choça	5
238	2016-02-18 00:00:00	Barra do Mendes	5
239	2016-02-18 00:00:00	Barra do Rocha	5
240	2016-02-18 00:00:00	Barreiras	5
241	2016-02-18 00:00:00	Barro Alto	5
242	2016-02-18 00:00:00	Barro Preto	5
243	2016-02-18 00:00:00	Barrocas	5
244	2016-02-18 00:00:00	Belmonte	5
245	2016-02-18 00:00:00	Belo Campo	5
246	2016-02-18 00:00:00	Biritinga	5
247	2016-02-18 00:00:00	Boa Nova	5
248	2016-02-18 00:00:00	Boa Vista do Tupim	5
249	2016-02-18 00:00:00	Bom Jesus da Lapa	5
250	2016-02-18 00:00:00	Bom Jesus da Serra	5
251	2016-02-18 00:00:00	Boninal	5
252	2016-02-18 00:00:00	Bonito	5
253	2016-02-18 00:00:00	Boquira	5
254	2016-02-18 00:00:00	Botuporã	5
255	2016-02-18 00:00:00	Brejões	5
256	2016-02-18 00:00:00	Brejolândia	5
257	2016-02-18 00:00:00	Brotas de Macaúbas	5
258	2016-02-18 00:00:00	Brumado	5
259	2016-02-18 00:00:00	Buerarema	5
260	2016-02-18 00:00:00	Buritirama	5
261	2016-02-18 00:00:00	Caatiba	5
262	2016-02-18 00:00:00	Cabaceiras do Paraguaçu	5
263	2016-02-18 00:00:00	Cachoeira	5
264	2016-02-18 00:00:00	Caculé	5
265	2016-02-18 00:00:00	Caém	5
266	2016-02-18 00:00:00	Caetanos	5
267	2016-02-18 00:00:00	Caetité	5
268	2016-02-18 00:00:00	Cafarnaum	5
269	2016-02-18 00:00:00	Cairu	5
270	2016-02-18 00:00:00	Caldeirão Grande	5
271	2016-02-18 00:00:00	Camacan	5
272	2016-02-18 00:00:00	Camaçari	5
273	2016-02-18 00:00:00	Camamu	5
274	2016-02-18 00:00:00	Campo Alegre de Lourdes	5
275	2016-02-18 00:00:00	Campo Formoso	5
276	2016-02-18 00:00:00	Canápolis	5
277	2016-02-18 00:00:00	Canarana	5
278	2016-02-18 00:00:00	Canavieiras	5
279	2016-02-18 00:00:00	Candeal	5
280	2016-02-18 00:00:00	Candeias	5
281	2016-02-18 00:00:00	Candiba	5
282	2016-02-18 00:00:00	Cândido Sales	5
283	2016-02-18 00:00:00	Cansanção	5
284	2016-02-18 00:00:00	Canudos	5
285	2016-02-18 00:00:00	Capela do Alto Alegre	5
286	2016-02-18 00:00:00	Capim Grosso	5
287	2016-02-18 00:00:00	Caraíbas	5
288	2016-02-18 00:00:00	Caravelas	5
289	2016-02-18 00:00:00	Cardeal da Silva	5
290	2016-02-18 00:00:00	Carinhanha	5
291	2016-02-18 00:00:00	Casa Nova	5
292	2016-02-18 00:00:00	Castro Alves	5
293	2016-02-18 00:00:00	Catolândia	5
294	2016-02-18 00:00:00	Catu	5
295	2016-02-18 00:00:00	Caturama	5
296	2016-02-18 00:00:00	Central	5
297	2016-02-18 00:00:00	Chorrochó	5
298	2016-02-18 00:00:00	Cícero Dantas	5
299	2016-02-18 00:00:00	Cipó	5
300	2016-02-18 00:00:00	Coaraci	5
301	2016-02-18 00:00:00	Cocos	5
302	2016-02-18 00:00:00	Conceição da Feira	5
303	2016-02-18 00:00:00	Conceição do Almeida	5
304	2016-02-18 00:00:00	Conceição do Coité	5
305	2016-02-18 00:00:00	Conceição do Jacuípe	5
306	2016-02-18 00:00:00	Conde	5
307	2016-02-18 00:00:00	Condeúba	5
308	2016-02-18 00:00:00	Contendas do Sincorá	5
309	2016-02-18 00:00:00	Coração de Maria	5
310	2016-02-18 00:00:00	Cordeiros	5
311	2016-02-18 00:00:00	Coribe	5
312	2016-02-18 00:00:00	Coronel João Sá	5
313	2016-02-18 00:00:00	Correntina	5
314	2016-02-18 00:00:00	Cotegipe	5
315	2016-02-18 00:00:00	Cravolândia	5
316	2016-02-18 00:00:00	Crisópolis	5
317	2016-02-18 00:00:00	Cristópolis	5
318	2016-02-18 00:00:00	Cruz das Almas	5
319	2016-02-18 00:00:00	Curaçá	5
320	2016-02-18 00:00:00	Dário Meira	5
321	2016-02-18 00:00:00	Dias d'Ávila	5
322	2016-02-18 00:00:00	Dom Basílio	5
323	2016-02-18 00:00:00	Dom Macedo Costa	5
324	2016-02-18 00:00:00	Elísio Medrado	5
325	2016-02-18 00:00:00	Encruzilhada	5
326	2016-02-18 00:00:00	Entre Rios	5
327	2016-02-18 00:00:00	Érico Cardoso	5
328	2016-02-18 00:00:00	Esplanada	5
329	2016-02-18 00:00:00	Euclides da Cunha	5
330	2016-02-18 00:00:00	Eunápolis	5
331	2016-02-18 00:00:00	Fátima	5
332	2016-02-18 00:00:00	Feira da Mata	5
333	2016-02-18 00:00:00	Feira de Santana	5
334	2016-02-18 00:00:00	Filadélfia	5
335	2016-02-18 00:00:00	Firmino Alves	5
336	2016-02-18 00:00:00	Floresta Azul	5
337	2016-02-18 00:00:00	Formosa do Rio Preto	5
338	2016-02-18 00:00:00	Gandu	5
339	2016-02-18 00:00:00	Gavião	5
340	2016-02-18 00:00:00	Gentio do Ouro	5
341	2016-02-18 00:00:00	Glória	5
342	2016-02-18 00:00:00	Gongogi	5
343	2016-02-18 00:00:00	Governador Mangabeira	5
344	2016-02-18 00:00:00	Guajeru	5
345	2016-02-18 00:00:00	Guanambi	5
346	2016-02-18 00:00:00	Guaratinga	5
347	2016-02-18 00:00:00	Heliópolis	5
348	2016-02-18 00:00:00	Iaçu	5
349	2016-02-18 00:00:00	Ibiassucê	5
350	2016-02-18 00:00:00	Ibicaraí	5
351	2016-02-18 00:00:00	Ibicoara	5
352	2016-02-18 00:00:00	Ibicuí	5
353	2016-02-18 00:00:00	Ibipeba	5
354	2016-02-18 00:00:00	Ibipitanga	5
355	2016-02-18 00:00:00	Ibiquera	5
356	2016-02-18 00:00:00	Ibirapitanga	5
357	2016-02-18 00:00:00	Ibirapuã	5
358	2016-02-18 00:00:00	Ibirataia	5
359	2016-02-18 00:00:00	Ibitiara	5
360	2016-02-18 00:00:00	Ibititá	5
361	2016-02-18 00:00:00	Ibotirama	5
362	2016-02-18 00:00:00	Ichu	5
363	2016-02-18 00:00:00	Igaporã	5
364	2016-02-18 00:00:00	Igrapiúna	5
365	2016-02-18 00:00:00	Iguaí	5
366	2016-02-18 00:00:00	Ilhéus	5
367	2016-02-18 00:00:00	Inhambupe	5
368	2016-02-18 00:00:00	Ipecaetá	5
369	2016-02-18 00:00:00	Ipiaú	5
370	2016-02-18 00:00:00	Ipirá	5
371	2016-02-18 00:00:00	Ipupiara	5
372	2016-02-18 00:00:00	Irajuba	5
373	2016-02-18 00:00:00	Iramaia	5
374	2016-02-18 00:00:00	Iraquara	5
375	2016-02-18 00:00:00	Irará	5
376	2016-02-18 00:00:00	Irecê	5
377	2016-02-18 00:00:00	Itabela	5
378	2016-02-18 00:00:00	Itaberaba	5
379	2016-02-18 00:00:00	Itabuna	5
380	2016-02-18 00:00:00	Itacaré	5
381	2016-02-18 00:00:00	Itaeté	5
382	2016-02-18 00:00:00	Itagi	5
383	2016-02-18 00:00:00	Itagibá	5
384	2016-02-18 00:00:00	Itagimirim	5
385	2016-02-18 00:00:00	Itaguaçu da Bahia	5
386	2016-02-18 00:00:00	Itaju do Colônia	5
387	2016-02-18 00:00:00	Itajuípe	5
388	2016-02-18 00:00:00	Itamaraju	5
389	2016-02-18 00:00:00	Itamari	5
390	2016-02-18 00:00:00	Itambé	5
391	2016-02-18 00:00:00	Itanagra	5
392	2016-02-18 00:00:00	Itanhém	5
393	2016-02-18 00:00:00	Itaparica	5
394	2016-02-18 00:00:00	Itapé	5
395	2016-02-18 00:00:00	Itapebi	5
396	2016-02-18 00:00:00	Itapetinga	5
397	2016-02-18 00:00:00	Itapicuru	5
398	2016-02-18 00:00:00	Itapitanga	5
399	2016-02-18 00:00:00	Itaquara	5
400	2016-02-18 00:00:00	Itarantim	5
401	2016-02-18 00:00:00	Itatim	5
402	2016-02-18 00:00:00	Itiruçu	5
403	2016-02-18 00:00:00	Itiúba	5
404	2016-02-18 00:00:00	Itororó	5
405	2016-02-18 00:00:00	Ituaçu	5
406	2016-02-18 00:00:00	Ituberá	5
407	2016-02-18 00:00:00	Iuiú	5
408	2016-02-18 00:00:00	Jaborandi	5
409	2016-02-18 00:00:00	Jacaraci	5
410	2016-02-18 00:00:00	Jacobina	5
411	2016-02-18 00:00:00	Jaguaquara	5
412	2016-02-18 00:00:00	Jaguarari	5
413	2016-02-18 00:00:00	Jaguaripe	5
414	2016-02-18 00:00:00	Jandaíra	5
415	2016-02-18 00:00:00	Jequié	5
416	2016-02-18 00:00:00	Jeremoabo	5
417	2016-02-18 00:00:00	Jiquiriçá	5
418	2016-02-18 00:00:00	Jitaúna	5
419	2016-02-18 00:00:00	João Dourado	5
420	2016-02-18 00:00:00	Juazeiro	5
421	2016-02-18 00:00:00	Jucuruçu	5
422	2016-02-18 00:00:00	Jussara	5
423	2016-02-18 00:00:00	Jussari	5
424	2016-02-18 00:00:00	Jussiape	5
425	2016-02-18 00:00:00	Lafaiete Coutinho	5
426	2016-02-18 00:00:00	Lagoa Real	5
427	2016-02-18 00:00:00	Laje	5
428	2016-02-18 00:00:00	Lajedão	5
429	2016-02-18 00:00:00	Lajedinho	5
430	2016-02-18 00:00:00	Lajedo do Tabocal	5
431	2016-02-18 00:00:00	Lamarão	5
432	2016-02-18 00:00:00	Lapão	5
433	2016-02-18 00:00:00	Lauro de Freitas	5
434	2016-02-18 00:00:00	Lençóis	5
435	2016-02-18 00:00:00	Licínio de Almeida	5
436	2016-02-18 00:00:00	Livramento de Nossa Senhora	5
437	2016-02-18 00:00:00	Luís Eduardo Magalhães	5
438	2016-02-18 00:00:00	Macajuba	5
439	2016-02-18 00:00:00	Macarani	5
440	2016-02-18 00:00:00	Macaúbas	5
441	2016-02-18 00:00:00	Macururé	5
442	2016-02-18 00:00:00	Madre de Deus	5
443	2016-02-18 00:00:00	Maetinga	5
444	2016-02-18 00:00:00	Maiquinique	5
445	2016-02-18 00:00:00	Mairi	5
446	2016-02-18 00:00:00	Malhada	5
447	2016-02-18 00:00:00	Malhada de Pedras	5
448	2016-02-18 00:00:00	Manoel Vitorino	5
449	2016-02-18 00:00:00	Mansidão	5
450	2016-02-18 00:00:00	Maracás	5
451	2016-02-18 00:00:00	Maragogipe	5
452	2016-02-18 00:00:00	Maraú	5
453	2016-02-18 00:00:00	Marcionílio Souza	5
454	2016-02-18 00:00:00	Mascote	5
455	2016-02-18 00:00:00	Mata de São João	5
456	2016-02-18 00:00:00	Matina	5
457	2016-02-18 00:00:00	Medeiros Neto	5
458	2016-02-18 00:00:00	Miguel Calmon	5
459	2016-02-18 00:00:00	Milagres	5
460	2016-02-18 00:00:00	Mirangaba	5
461	2016-02-18 00:00:00	Mirante	5
462	2016-02-18 00:00:00	Monte Santo	5
463	2016-02-18 00:00:00	Morpará	5
464	2016-02-18 00:00:00	Morro do Chapéu	5
465	2016-02-18 00:00:00	Mortugaba	5
466	2016-02-18 00:00:00	Mucugê	5
467	2016-02-18 00:00:00	Mucuri	5
468	2016-02-18 00:00:00	Mulungu do Morro	5
469	2016-02-18 00:00:00	Mundo Novo	5
470	2016-02-18 00:00:00	Muniz Ferreira	5
471	2016-02-18 00:00:00	Muquém de São Francisco	5
472	2016-02-18 00:00:00	Muritiba	5
473	2016-02-18 00:00:00	Mutuípe	5
474	2016-02-18 00:00:00	Nazaré	5
475	2016-02-18 00:00:00	Nilo Peçanha	5
476	2016-02-18 00:00:00	Nordestina	5
477	2016-02-18 00:00:00	Nova Canaã	5
478	2016-02-18 00:00:00	Nova Fátima	5
479	2016-02-18 00:00:00	Nova Ibiá	5
480	2016-02-18 00:00:00	Nova Itarana	5
481	2016-02-18 00:00:00	Nova Redenção	5
482	2016-02-18 00:00:00	Nova Soure	5
483	2016-02-18 00:00:00	Nova Viçosa	5
484	2016-02-18 00:00:00	Novo Horizonte	5
485	2016-02-18 00:00:00	Novo Triunfo	5
486	2016-02-18 00:00:00	Olindina	5
487	2016-02-18 00:00:00	Oliveira dos Brejinhos	5
488	2016-02-18 00:00:00	Ouriçangas	5
489	2016-02-18 00:00:00	Ourolândia	5
490	2016-02-18 00:00:00	Palmas de Monte Alto	5
491	2016-02-18 00:00:00	Palmeiras	5
492	2016-02-18 00:00:00	Paramirim	5
493	2016-02-18 00:00:00	Paratinga	5
494	2016-02-18 00:00:00	Paripiranga	5
495	2016-02-18 00:00:00	Pau Brasil	5
496	2016-02-18 00:00:00	Paulo Afonso	5
497	2016-02-18 00:00:00	Pé de Serra	5
498	2016-02-18 00:00:00	Pedrão	5
499	2016-02-18 00:00:00	Pedro Alexandre	5
500	2016-02-18 00:00:00	Piatã	5
501	2016-02-18 00:00:00	Pilão Arcado	5
502	2016-02-18 00:00:00	Pindaí	5
503	2016-02-18 00:00:00	Pindobaçu	5
504	2016-02-18 00:00:00	Pintadas	5
505	2016-02-18 00:00:00	Piraí do Norte	5
506	2016-02-18 00:00:00	Piripá	5
507	2016-02-18 00:00:00	Piritiba	5
508	2016-02-18 00:00:00	Planaltino	5
509	2016-02-18 00:00:00	Planalto	5
510	2016-02-18 00:00:00	Poções	5
511	2016-02-18 00:00:00	Pojuca	5
512	2016-02-18 00:00:00	Ponto Novo	5
513	2016-02-18 00:00:00	Porto Seguro	5
514	2016-02-18 00:00:00	Potiraguá	5
515	2016-02-18 00:00:00	Prado	5
516	2016-02-18 00:00:00	Presidente Dutra	5
517	2016-02-18 00:00:00	Presidente Jânio Quadros	5
518	2016-02-18 00:00:00	Presidente Tancredo Neves	5
519	2016-02-18 00:00:00	Queimadas	5
520	2016-02-18 00:00:00	Quijingue	5
521	2016-02-18 00:00:00	Quixabeira	5
522	2016-02-18 00:00:00	Rafael Jambeiro	5
523	2016-02-18 00:00:00	Remanso	5
524	2016-02-18 00:00:00	Retirolândia	5
525	2016-02-18 00:00:00	Riachão das Neves	5
526	2016-02-18 00:00:00	Riachão do Jacuípe	5
527	2016-02-18 00:00:00	Riacho de Santana	5
528	2016-02-18 00:00:00	Ribeira do Amparo	5
529	2016-02-18 00:00:00	Ribeira do Pombal	5
530	2016-02-18 00:00:00	Ribeirão do Largo	5
531	2016-02-18 00:00:00	Rio de Contas	5
532	2016-02-18 00:00:00	Rio do Antônio	5
533	2016-02-18 00:00:00	Rio do Pires	5
534	2016-02-18 00:00:00	Rio Real	5
535	2016-02-18 00:00:00	Rodelas	5
536	2016-02-18 00:00:00	Ruy Barbosa	5
537	2016-02-18 00:00:00	Salinas da Margarida	5
538	2016-02-18 00:00:00	Salvador	5
539	2016-02-18 00:00:00	Santa Bárbara	5
540	2016-02-18 00:00:00	Santa Brígida	5
541	2016-02-18 00:00:00	Santa Cruz Cabrália	5
542	2016-02-18 00:00:00	Santa Cruz da Vitória	5
543	2016-02-18 00:00:00	Santa Inês	5
544	2016-02-18 00:00:00	Santa Luzia	5
545	2016-02-18 00:00:00	Santa Maria da Vitória	5
546	2016-02-18 00:00:00	Santa Rita de Cássia	5
547	2016-02-18 00:00:00	Santa Teresinha	5
548	2016-02-18 00:00:00	Santaluz	5
549	2016-02-18 00:00:00	Santana	5
550	2016-02-18 00:00:00	Santanópolis	5
551	2016-02-18 00:00:00	Santo Amaro	5
552	2016-02-18 00:00:00	Santo Antônio de Jesus	5
553	2016-02-18 00:00:00	Santo Estêvão	5
554	2016-02-18 00:00:00	São Desidério	5
555	2016-02-18 00:00:00	São Domingos	5
556	2016-02-18 00:00:00	São Felipe	5
557	2016-02-18 00:00:00	São Félix	5
558	2016-02-18 00:00:00	São Félix do Coribe	5
559	2016-02-18 00:00:00	São Francisco do Conde	5
560	2016-02-18 00:00:00	São Gabriel	5
561	2016-02-18 00:00:00	São Gonçalo dos Campos	5
562	2016-02-18 00:00:00	São José da Vitória	5
563	2016-02-18 00:00:00	São José do Jacuípe	5
564	2016-02-18 00:00:00	São Miguel das Matas	5
565	2016-02-18 00:00:00	São Sebastião do Passé	5
566	2016-02-18 00:00:00	Sapeaçu	5
567	2016-02-18 00:00:00	Sátiro Dias	5
568	2016-02-18 00:00:00	Saubara	5
569	2016-02-18 00:00:00	Saúde	5
570	2016-02-18 00:00:00	Seabra	5
571	2016-02-18 00:00:00	Sebastião Laranjeiras	5
572	2016-02-18 00:00:00	Senhor do Bonfim	5
573	2016-02-18 00:00:00	Sento Sé	5
574	2016-02-18 00:00:00	Serra do Ramalho	5
575	2016-02-18 00:00:00	Serra Dourada	5
576	2016-02-18 00:00:00	Serra Preta	5
577	2016-02-18 00:00:00	Serrinha	5
578	2016-02-18 00:00:00	Serrolândia	5
579	2016-02-18 00:00:00	Simões Filho	5
580	2016-02-18 00:00:00	Sítio do Mato	5
581	2016-02-18 00:00:00	Sítio do Quinto	5
582	2016-02-18 00:00:00	Sobradinho	5
583	2016-02-18 00:00:00	Souto Soares	5
584	2016-02-18 00:00:00	Tabocas do Brejo Velho	5
585	2016-02-18 00:00:00	Tanhaçu	5
586	2016-02-18 00:00:00	Tanque Novo	5
587	2016-02-18 00:00:00	Tanquinho	5
588	2016-02-18 00:00:00	Taperoá	5
589	2016-02-18 00:00:00	Tapiramutá	5
590	2016-02-18 00:00:00	Teixeira de Freitas	5
591	2016-02-18 00:00:00	Teodoro Sampaio	5
592	2016-02-18 00:00:00	Teofilândia	5
593	2016-02-18 00:00:00	Teolândia	5
594	2016-02-18 00:00:00	Terra Nova	5
595	2016-02-18 00:00:00	Tremedal	5
596	2016-02-18 00:00:00	Tucano	5
597	2016-02-18 00:00:00	Uauá	5
598	2016-02-18 00:00:00	Ubaíra	5
599	2016-02-18 00:00:00	Ubaitaba	5
600	2016-02-18 00:00:00	Ubatã	5
601	2016-02-18 00:00:00	Uibaí	5
602	2016-02-18 00:00:00	Umburanas	5
603	2016-02-18 00:00:00	Una	5
604	2016-02-18 00:00:00	Urandi	5
605	2016-02-18 00:00:00	Uruçuca	5
606	2016-02-18 00:00:00	Utinga	5
607	2016-02-18 00:00:00	Valença	5
608	2016-02-18 00:00:00	Valente	5
609	2016-02-18 00:00:00	Várzea da Roça	5
610	2016-02-18 00:00:00	Várzea do Poço	5
611	2016-02-18 00:00:00	Várzea Nova	5
612	2016-02-18 00:00:00	Varzedo	5
613	2016-02-18 00:00:00	Vera Cruz	5
614	2016-02-18 00:00:00	Vereda	5
615	2016-02-18 00:00:00	Vitória da Conquista	5
616	2016-02-18 00:00:00	Wagner	5
617	2016-02-18 00:00:00	Wanderley	5
618	2016-02-18 00:00:00	Wenceslau Guimarães	5
619	2016-02-18 00:00:00	Xique-Xique	5
620	2016-02-18 00:00:00	Abaiara	6
621	2016-02-18 00:00:00	Acarape	6
622	2016-02-18 00:00:00	Acaraú	6
623	2016-02-18 00:00:00	Acopiara	6
624	2016-02-18 00:00:00	Aiuaba	6
625	2016-02-18 00:00:00	Alcântaras	6
626	2016-02-18 00:00:00	Altaneira	6
627	2016-02-18 00:00:00	Alto Santo	6
628	2016-02-18 00:00:00	Amontada	6
629	2016-02-18 00:00:00	Antonina do Norte	6
630	2016-02-18 00:00:00	Apuiarés	6
631	2016-02-18 00:00:00	Aquiraz	6
632	2016-02-18 00:00:00	Aracati	6
633	2016-02-18 00:00:00	Aracoiaba	6
634	2016-02-18 00:00:00	Ararendá	6
635	2016-02-18 00:00:00	Araripe	6
636	2016-02-18 00:00:00	Aratuba	6
637	2016-02-18 00:00:00	Arneiroz	6
638	2016-02-18 00:00:00	Assaré	6
639	2016-02-18 00:00:00	Aurora	6
640	2016-02-18 00:00:00	Baixio	6
641	2016-02-18 00:00:00	Banabuiú	6
642	2016-02-18 00:00:00	Barbalha	6
643	2016-02-18 00:00:00	Barreira	6
644	2016-02-18 00:00:00	Barro	6
645	2016-02-18 00:00:00	Barroquinha	6
646	2016-02-18 00:00:00	Baturité	6
647	2016-02-18 00:00:00	Beberibe	6
648	2016-02-18 00:00:00	Bela Cruz	6
649	2016-02-18 00:00:00	Boa Viagem	6
650	2016-02-18 00:00:00	Brejo Santo	6
651	2016-02-18 00:00:00	Camocim	6
652	2016-02-18 00:00:00	Campos Sales	6
653	2016-02-18 00:00:00	Canindé	6
654	2016-02-18 00:00:00	Capistrano	6
655	2016-02-18 00:00:00	Caridade	6
656	2016-02-18 00:00:00	Cariré	6
657	2016-02-18 00:00:00	Caririaçu	6
658	2016-02-18 00:00:00	Cariús	6
659	2016-02-18 00:00:00	Carnaubal	6
660	2016-02-18 00:00:00	Cascavel	6
661	2016-02-18 00:00:00	Catarina	6
662	2016-02-18 00:00:00	Catunda	6
663	2016-02-18 00:00:00	Caucaia	6
664	2016-02-18 00:00:00	Cedro	6
665	2016-02-18 00:00:00	Chaval	6
666	2016-02-18 00:00:00	Choró	6
667	2016-02-18 00:00:00	Chorozinho	6
668	2016-02-18 00:00:00	Coreaú	6
669	2016-02-18 00:00:00	Crateús	6
670	2016-02-18 00:00:00	Crato	6
671	2016-02-18 00:00:00	Croatá	6
672	2016-02-18 00:00:00	Cruz	6
673	2016-02-18 00:00:00	Deputado Irapuan Pinheiro	6
674	2016-02-18 00:00:00	Ererê	6
675	2016-02-18 00:00:00	Eusébio	6
676	2016-02-18 00:00:00	Farias Brito	6
677	2016-02-18 00:00:00	Forquilha	6
678	2016-02-18 00:00:00	Fortaleza	6
679	2016-02-18 00:00:00	Fortim	6
680	2016-02-18 00:00:00	Frecheirinha	6
681	2016-02-18 00:00:00	General Sampaio	6
682	2016-02-18 00:00:00	Graça	6
683	2016-02-18 00:00:00	Granja	6
684	2016-02-18 00:00:00	Granjeiro	6
685	2016-02-18 00:00:00	Groaíras	6
686	2016-02-18 00:00:00	Guaiúba	6
687	2016-02-18 00:00:00	Guaraciaba do Norte	6
688	2016-02-18 00:00:00	Guaramiranga	6
689	2016-02-18 00:00:00	Hidrolândia	6
690	2016-02-18 00:00:00	Horizonte	6
691	2016-02-18 00:00:00	Ibaretama	6
692	2016-02-18 00:00:00	Ibiapina	6
693	2016-02-18 00:00:00	Ibicuitinga	6
694	2016-02-18 00:00:00	Icapuí	6
695	2016-02-18 00:00:00	Icó	6
696	2016-02-18 00:00:00	Iguatu	6
697	2016-02-18 00:00:00	Independência	6
698	2016-02-18 00:00:00	Ipaporanga	6
699	2016-02-18 00:00:00	Ipaumirim	6
700	2016-02-18 00:00:00	Ipu	6
701	2016-02-18 00:00:00	Ipueiras	6
702	2016-02-18 00:00:00	Iracema	6
703	2016-02-18 00:00:00	Irauçuba	6
704	2016-02-18 00:00:00	Itaiçaba	6
705	2016-02-18 00:00:00	Itaitinga	6
706	2016-02-18 00:00:00	Itapagé	6
707	2016-02-18 00:00:00	Itapipoca	6
708	2016-02-18 00:00:00	Itapiúna	6
709	2016-02-18 00:00:00	Itarema	6
710	2016-02-18 00:00:00	Itatira	6
711	2016-02-18 00:00:00	Jaguaretama	6
712	2016-02-18 00:00:00	Jaguaribara	6
713	2016-02-18 00:00:00	Jaguaribe	6
714	2016-02-18 00:00:00	Jaguaruana	6
715	2016-02-18 00:00:00	Jardim	6
716	2016-02-18 00:00:00	Jati	6
717	2016-02-18 00:00:00	Jijoca de Jericoacoara	6
718	2016-02-18 00:00:00	Juazeiro do Norte	6
719	2016-02-18 00:00:00	Jucás	6
720	2016-02-18 00:00:00	Lavras da Mangabeira	6
721	2016-02-18 00:00:00	Limoeiro do Norte	6
722	2016-02-18 00:00:00	Madalena	6
723	2016-02-18 00:00:00	Maracanaú	6
724	2016-02-18 00:00:00	Maranguape	6
725	2016-02-18 00:00:00	Marco	6
726	2016-02-18 00:00:00	Martinópole	6
727	2016-02-18 00:00:00	Massapê	6
728	2016-02-18 00:00:00	Mauriti	6
729	2016-02-18 00:00:00	Meruoca	6
730	2016-02-18 00:00:00	Milagres	6
731	2016-02-18 00:00:00	Milhã	6
732	2016-02-18 00:00:00	Miraíma	6
733	2016-02-18 00:00:00	Missão Velha	6
734	2016-02-18 00:00:00	Mombaça	6
735	2016-02-18 00:00:00	Monsenhor Tabosa	6
736	2016-02-18 00:00:00	Morada Nova	6
737	2016-02-18 00:00:00	Moraújo	6
738	2016-02-18 00:00:00	Morrinhos	6
739	2016-02-18 00:00:00	Mucambo	6
740	2016-02-18 00:00:00	Mulungu	6
741	2016-02-18 00:00:00	Nova Olinda	6
742	2016-02-18 00:00:00	Nova Russas	6
743	2016-02-18 00:00:00	Novo Oriente	6
744	2016-02-18 00:00:00	Ocara	6
745	2016-02-18 00:00:00	Orós	6
746	2016-02-18 00:00:00	Pacajus	6
747	2016-02-18 00:00:00	Pacatuba	6
748	2016-02-18 00:00:00	Pacoti	6
749	2016-02-18 00:00:00	Pacujá	6
750	2016-02-18 00:00:00	Palhano	6
751	2016-02-18 00:00:00	Palmácia	6
752	2016-02-18 00:00:00	Paracuru	6
753	2016-02-18 00:00:00	Paraipaba	6
754	2016-02-18 00:00:00	Parambu	6
755	2016-02-18 00:00:00	Paramoti	6
756	2016-02-18 00:00:00	Pedra Branca	6
757	2016-02-18 00:00:00	Penaforte	6
758	2016-02-18 00:00:00	Pentecoste	6
759	2016-02-18 00:00:00	Pereiro	6
760	2016-02-18 00:00:00	Pindoretama	6
761	2016-02-18 00:00:00	Piquet Carneiro	6
762	2016-02-18 00:00:00	Pires Ferreira	6
763	2016-02-18 00:00:00	Poranga	6
764	2016-02-18 00:00:00	Porteiras	6
765	2016-02-18 00:00:00	Potengi	6
766	2016-02-18 00:00:00	Potiretama	6
767	2016-02-18 00:00:00	Quiterianópolis	6
768	2016-02-18 00:00:00	Quixadá	6
769	2016-02-18 00:00:00	Quixelô	6
770	2016-02-18 00:00:00	Quixeramobim	6
771	2016-02-18 00:00:00	Quixeré	6
772	2016-02-18 00:00:00	Redenção	6
773	2016-02-18 00:00:00	Reriutaba	6
774	2016-02-18 00:00:00	Russas	6
775	2016-02-18 00:00:00	Saboeiro	6
776	2016-02-18 00:00:00	Salitre	6
777	2016-02-18 00:00:00	Santa Quitéria	6
778	2016-02-18 00:00:00	Santana do Acaraú	6
779	2016-02-18 00:00:00	Santana do Cariri	6
780	2016-02-18 00:00:00	São Benedito	6
781	2016-02-18 00:00:00	São Gonçalo do Amarante	6
782	2016-02-18 00:00:00	São João do Jaguaribe	6
783	2016-02-18 00:00:00	São Luís do Curu	6
784	2016-02-18 00:00:00	Senador Pompeu	6
785	2016-02-18 00:00:00	Senador Sá	6
786	2016-02-18 00:00:00	Sobral	6
787	2016-02-18 00:00:00	Solonópole	6
788	2016-02-18 00:00:00	Tabuleiro do Norte	6
789	2016-02-18 00:00:00	Tamboril	6
790	2016-02-18 00:00:00	Tarrafas	6
791	2016-02-18 00:00:00	Tauá	6
792	2016-02-18 00:00:00	Tejuçuoca	6
793	2016-02-18 00:00:00	Tianguá	6
794	2016-02-18 00:00:00	Trairi	6
795	2016-02-18 00:00:00	Tururu	6
796	2016-02-18 00:00:00	Ubajara	6
797	2016-02-18 00:00:00	Umari	6
798	2016-02-18 00:00:00	Umirim	6
799	2016-02-18 00:00:00	Uruburetama	6
800	2016-02-18 00:00:00	Uruoca	6
801	2016-02-18 00:00:00	Varjota	6
802	2016-02-18 00:00:00	Várzea Alegre	6
803	2016-02-18 00:00:00	Viçosa do Ceará	6
804	2016-02-18 00:00:00	Brasília	7
805	2016-02-18 00:00:00	Afonso Cláudio	8
806	2016-02-18 00:00:00	Água Doce do Norte	8
807	2016-02-18 00:00:00	Águia Branca	8
808	2016-02-18 00:00:00	Alegre	8
809	2016-02-18 00:00:00	Alfredo Chaves	8
810	2016-02-18 00:00:00	Alto Rio Novo	8
811	2016-02-18 00:00:00	Anchieta	8
812	2016-02-18 00:00:00	Apiacá	8
813	2016-02-18 00:00:00	Aracruz	8
814	2016-02-18 00:00:00	Atilio Vivacqua	8
815	2016-02-18 00:00:00	Baixo Guandu	8
816	2016-02-18 00:00:00	Barra de São Francisco	8
817	2016-02-18 00:00:00	Boa Esperança	8
818	2016-02-18 00:00:00	Bom Jesus do Norte	8
819	2016-02-18 00:00:00	Brejetuba	8
820	2016-02-18 00:00:00	Cachoeiro de Itapemirim	8
821	2016-02-18 00:00:00	Cariacica	8
822	2016-02-18 00:00:00	Castelo	8
823	2016-02-18 00:00:00	Colatina	8
824	2016-02-18 00:00:00	Conceição da Barra	8
825	2016-02-18 00:00:00	Conceição do Castelo	8
826	2016-02-18 00:00:00	Divino de São Lourenço	8
827	2016-02-18 00:00:00	Domingos Martins	8
828	2016-02-18 00:00:00	Dores do Rio Preto	8
829	2016-02-18 00:00:00	Ecoporanga	8
830	2016-02-18 00:00:00	Fundão	8
831	2016-02-18 00:00:00	Governador Lindenberg	8
832	2016-02-18 00:00:00	Guaçuí	8
833	2016-02-18 00:00:00	Guarapari	8
834	2016-02-18 00:00:00	Ibatiba	8
835	2016-02-18 00:00:00	Ibiraçu	8
836	2016-02-18 00:00:00	Ibitirama	8
837	2016-02-18 00:00:00	Iconha	8
838	2016-02-18 00:00:00	Irupi	8
839	2016-02-18 00:00:00	Itaguaçu	8
840	2016-02-18 00:00:00	Itapemirim	8
841	2016-02-18 00:00:00	Itarana	8
842	2016-02-18 00:00:00	Iúna	8
843	2016-02-18 00:00:00	Jaguaré	8
844	2016-02-18 00:00:00	Jerônimo Monteiro	8
845	2016-02-18 00:00:00	João Neiva	8
846	2016-02-18 00:00:00	Laranja da Terra	8
847	2016-02-18 00:00:00	Linhares	8
848	2016-02-18 00:00:00	Mantenópolis	8
849	2016-02-18 00:00:00	Marataízes	8
850	2016-02-18 00:00:00	Marechal Floriano	8
851	2016-02-18 00:00:00	Marilândia	8
852	2016-02-18 00:00:00	Mimoso do Sul	8
853	2016-02-18 00:00:00	Montanha	8
854	2016-02-18 00:00:00	Mucurici	8
855	2016-02-18 00:00:00	Muniz Freire	8
856	2016-02-18 00:00:00	Muqui	8
857	2016-02-18 00:00:00	Nova Venécia	8
858	2016-02-18 00:00:00	Pancas	8
859	2016-02-18 00:00:00	Pedro Canário	8
860	2016-02-18 00:00:00	Pinheiros	8
861	2016-02-18 00:00:00	Piúma	8
862	2016-02-18 00:00:00	Ponto Belo	8
863	2016-02-18 00:00:00	Presidente Kennedy	8
864	2016-02-18 00:00:00	Rio Bananal	8
865	2016-02-18 00:00:00	Rio Novo do Sul	8
866	2016-02-18 00:00:00	Santa Leopoldina	8
867	2016-02-18 00:00:00	Santa Maria de Jetibá	8
868	2016-02-18 00:00:00	Santa Teresa	8
869	2016-02-18 00:00:00	São Domingos do Norte	8
870	2016-02-18 00:00:00	São Gabriel da Palha	8
871	2016-02-18 00:00:00	São José do Calçado	8
872	2016-02-18 00:00:00	São Mateus	8
873	2016-02-18 00:00:00	São Roque do Canaã	8
874	2016-02-18 00:00:00	Serra	8
875	2016-02-18 00:00:00	Sooretama	8
876	2016-02-18 00:00:00	Vargem Alta	8
877	2016-02-18 00:00:00	Venda Nova do Imigrante	8
878	2016-02-18 00:00:00	Viana	8
879	2016-02-18 00:00:00	Vila Pavão	8
880	2016-02-18 00:00:00	Vila Valério	8
881	2016-02-18 00:00:00	Vila Velha	8
882	2016-02-18 00:00:00	Vitória	8
883	2016-02-18 00:00:00	Abadia de Goiás	9
884	2016-02-18 00:00:00	Abadiânia	9
885	2016-02-18 00:00:00	Acreúna	9
886	2016-02-18 00:00:00	Adelândia	9
887	2016-02-18 00:00:00	Água Fria de Goiás	9
888	2016-02-18 00:00:00	Água Limpa	9
889	2016-02-18 00:00:00	Águas Lindas de Goiás	9
890	2016-02-18 00:00:00	Alexânia	9
891	2016-02-18 00:00:00	Aloândia	9
892	2016-02-18 00:00:00	Alto Horizonte	9
893	2016-02-18 00:00:00	Alto Paraíso de Goiás	9
894	2016-02-18 00:00:00	Alvorada do Norte	9
895	2016-02-18 00:00:00	Amaralina	9
896	2016-02-18 00:00:00	Americano do Brasil	9
897	2016-02-18 00:00:00	Amorinópolis	9
898	2016-02-18 00:00:00	Anápolis	9
899	2016-02-18 00:00:00	Anhanguera	9
900	2016-02-18 00:00:00	Anicuns	9
901	2016-02-18 00:00:00	Aparecida de Goiânia	9
902	2016-02-18 00:00:00	Aparecida do Rio Doce	9
903	2016-02-18 00:00:00	Aporé	9
904	2016-02-18 00:00:00	Araçu	9
905	2016-02-18 00:00:00	Aragarças	9
906	2016-02-18 00:00:00	Aragoiânia	9
907	2016-02-18 00:00:00	Araguapaz	9
908	2016-02-18 00:00:00	Arenópolis	9
909	2016-02-18 00:00:00	Aruanã	9
910	2016-02-18 00:00:00	Aurilândia	9
911	2016-02-18 00:00:00	Avelinópolis	9
912	2016-02-18 00:00:00	Baliza	9
913	2016-02-18 00:00:00	Barro Alto	9
914	2016-02-18 00:00:00	Bela Vista de Goiás	9
915	2016-02-18 00:00:00	Bom Jardim de Goiás	9
916	2016-02-18 00:00:00	Bom Jesus de Goiás	9
917	2016-02-18 00:00:00	Bonfinópolis	9
918	2016-02-18 00:00:00	Bonópolis	9
919	2016-02-18 00:00:00	Brazabrantes	9
920	2016-02-18 00:00:00	Britânia	9
921	2016-02-18 00:00:00	Buriti Alegre	9
922	2016-02-18 00:00:00	Buriti de Goiás	9
923	2016-02-18 00:00:00	Buritinópolis	9
924	2016-02-18 00:00:00	Cabeceiras	9
925	2016-02-18 00:00:00	Cachoeira Alta	9
926	2016-02-18 00:00:00	Cachoeira de Goiás	9
927	2016-02-18 00:00:00	Cachoeira Dourada	9
928	2016-02-18 00:00:00	Caçu	9
929	2016-02-18 00:00:00	Caiapônia	9
930	2016-02-18 00:00:00	Caldas Novas	9
931	2016-02-18 00:00:00	Caldazinha	9
932	2016-02-18 00:00:00	Campestre de Goiás	9
933	2016-02-18 00:00:00	Campinaçu	9
934	2016-02-18 00:00:00	Campinorte	9
935	2016-02-18 00:00:00	Campo Alegre de Goiás	9
936	2016-02-18 00:00:00	Campo Limpo de Goiás	9
937	2016-02-18 00:00:00	Campos Belos	9
938	2016-02-18 00:00:00	Campos Verdes	9
939	2016-02-18 00:00:00	Carmo do Rio Verde	9
940	2016-02-18 00:00:00	Castelândia	9
941	2016-02-18 00:00:00	Catalão	9
942	2016-02-18 00:00:00	Caturaí	9
943	2016-02-18 00:00:00	Cavalcante	9
944	2016-02-18 00:00:00	Ceres	9
945	2016-02-18 00:00:00	Cezarina	9
946	2016-02-18 00:00:00	Chapadão do Céu	9
947	2016-02-18 00:00:00	Cidade Ocidental	9
948	2016-02-18 00:00:00	Cocalzinho de Goiás	9
949	2016-02-18 00:00:00	Colinas do Sul	9
950	2016-02-18 00:00:00	Córrego do Ouro	9
951	2016-02-18 00:00:00	Corumbá de Goiás	9
952	2016-02-18 00:00:00	Corumbaíba	9
953	2016-02-18 00:00:00	Cristalina	9
954	2016-02-18 00:00:00	Cristianópolis	9
955	2016-02-18 00:00:00	Crixás	9
956	2016-02-18 00:00:00	Cromínia	9
957	2016-02-18 00:00:00	Cumari	9
958	2016-02-18 00:00:00	Damianópolis	9
959	2016-02-18 00:00:00	Damolândia	9
960	2016-02-18 00:00:00	Davinópolis	9
961	2016-02-18 00:00:00	Diorama	9
962	2016-02-18 00:00:00	Divinópolis de Goiás	9
963	2016-02-18 00:00:00	Doverlândia	9
964	2016-02-18 00:00:00	Edealina	9
965	2016-02-18 00:00:00	Edéia	9
966	2016-02-18 00:00:00	Estrela do Norte	9
967	2016-02-18 00:00:00	Faina	9
968	2016-02-18 00:00:00	Fazenda Nova	9
969	2016-02-18 00:00:00	Firminópolis	9
970	2016-02-18 00:00:00	Flores de Goiás	9
971	2016-02-18 00:00:00	Formosa	9
972	2016-02-18 00:00:00	Formoso	9
973	2016-02-18 00:00:00	Gameleira de Goiás	9
974	2016-02-18 00:00:00	Goianápolis	9
975	2016-02-18 00:00:00	Goiandira	9
976	2016-02-18 00:00:00	Goianésia	9
977	2016-02-18 00:00:00	Goiânia	9
978	2016-02-18 00:00:00	Goianira	9
979	2016-02-18 00:00:00	Goiás	9
980	2016-02-18 00:00:00	Goiatuba	9
981	2016-02-18 00:00:00	Gouvelândia	9
982	2016-02-18 00:00:00	Guapó	9
983	2016-02-18 00:00:00	Guaraíta	9
984	2016-02-18 00:00:00	Guarani de Goiás	9
985	2016-02-18 00:00:00	Guarinos	9
986	2016-02-18 00:00:00	Heitoraí	9
987	2016-02-18 00:00:00	Hidrolândia	9
988	2016-02-18 00:00:00	Hidrolina	9
989	2016-02-18 00:00:00	Iaciara	9
990	2016-02-18 00:00:00	Inaciolândia	9
991	2016-02-18 00:00:00	Indiara	9
992	2016-02-18 00:00:00	Inhumas	9
993	2016-02-18 00:00:00	Ipameri	9
994	2016-02-18 00:00:00	Ipiranga de Goiás	9
995	2016-02-18 00:00:00	Iporá	9
996	2016-02-18 00:00:00	Israelândia	9
997	2016-02-18 00:00:00	Itaberaí	9
998	2016-02-18 00:00:00	Itaguari	9
999	2016-02-18 00:00:00	Itaguaru	9
1000	2016-02-18 00:00:00	Itajá	9
1001	2016-02-18 00:00:00	Itapaci	9
1002	2016-02-18 00:00:00	Itapirapuã	9
1003	2016-02-18 00:00:00	Itapuranga	9
1004	2016-02-18 00:00:00	Itarumã	9
1005	2016-02-18 00:00:00	Itauçu	9
1006	2016-02-18 00:00:00	Itumbiara	9
1007	2016-02-18 00:00:00	Ivolândia	9
1008	2016-02-18 00:00:00	Jandaia	9
1009	2016-02-18 00:00:00	Jaraguá	9
1010	2016-02-18 00:00:00	Jataí	9
1011	2016-02-18 00:00:00	Jaupaci	9
1012	2016-02-18 00:00:00	Jesúpolis	9
1013	2016-02-18 00:00:00	Joviânia	9
1014	2016-02-18 00:00:00	Jussara	9
1015	2016-02-18 00:00:00	Lagoa Santa	9
1016	2016-02-18 00:00:00	Leopoldo de Bulhões	9
1017	2016-02-18 00:00:00	Luziânia	9
1018	2016-02-18 00:00:00	Mairipotaba	9
1019	2016-02-18 00:00:00	Mambaí	9
1020	2016-02-18 00:00:00	Mara Rosa	9
1021	2016-02-18 00:00:00	Marzagão	9
1022	2016-02-18 00:00:00	Matrinchã	9
1023	2016-02-18 00:00:00	Maurilândia	9
1024	2016-02-18 00:00:00	Mimoso de Goiás	9
1025	2016-02-18 00:00:00	Minaçu	9
1026	2016-02-18 00:00:00	Mineiros	9
1027	2016-02-18 00:00:00	Moiporá	9
1028	2016-02-18 00:00:00	Monte Alegre de Goiás	9
1029	2016-02-18 00:00:00	Montes Claros de Goiás	9
1030	2016-02-18 00:00:00	Montividiu	9
1031	2016-02-18 00:00:00	Montividiu do Norte	9
1032	2016-02-18 00:00:00	Morrinhos	9
1033	2016-02-18 00:00:00	Morro Agudo de Goiás	9
1034	2016-02-18 00:00:00	Mossâmedes	9
1035	2016-02-18 00:00:00	Mozarlândia	9
1036	2016-02-18 00:00:00	Mundo Novo	9
1037	2016-02-18 00:00:00	Mutunópolis	9
1038	2016-02-18 00:00:00	Nazário	9
1039	2016-02-18 00:00:00	Nerópolis	9
1040	2016-02-18 00:00:00	Niquelândia	9
1041	2016-02-18 00:00:00	Nova América	9
1042	2016-02-18 00:00:00	Nova Aurora	9
1043	2016-02-18 00:00:00	Nova Crixás	9
1044	2016-02-18 00:00:00	Nova Glória	9
1045	2016-02-18 00:00:00	Nova Iguaçu de Goiás	9
1046	2016-02-18 00:00:00	Nova Roma	9
1047	2016-02-18 00:00:00	Nova Veneza	9
1048	2016-02-18 00:00:00	Novo Brasil	9
1049	2016-02-18 00:00:00	Novo Gama	9
1050	2016-02-18 00:00:00	Novo Planalto	9
1051	2016-02-18 00:00:00	Orizona	9
1052	2016-02-18 00:00:00	Ouro Verde de Goiás	9
1053	2016-02-18 00:00:00	Ouvidor	9
1054	2016-02-18 00:00:00	Padre Bernardo	9
1055	2016-02-18 00:00:00	Palestina de Goiás	9
1056	2016-02-18 00:00:00	Palmeiras de Goiás	9
1057	2016-02-18 00:00:00	Palmelo	9
1058	2016-02-18 00:00:00	Palminópolis	9
1059	2016-02-18 00:00:00	Panamá	9
1060	2016-02-18 00:00:00	Paranaiguara	9
1061	2016-02-18 00:00:00	Paraúna	9
1062	2016-02-18 00:00:00	Perolândia	9
1063	2016-02-18 00:00:00	Petrolina de Goiás	9
1064	2016-02-18 00:00:00	Pilar de Goiás	9
1065	2016-02-18 00:00:00	Piracanjuba	9
1066	2016-02-18 00:00:00	Piranhas	9
1067	2016-02-18 00:00:00	Pirenópolis	9
1068	2016-02-18 00:00:00	Pires do Rio	9
1069	2016-02-18 00:00:00	Planaltina	9
1070	2016-02-18 00:00:00	Pontalina	9
1071	2016-02-18 00:00:00	Porangatu	9
1072	2016-02-18 00:00:00	Porteirão	9
1073	2016-02-18 00:00:00	Portelândia	9
1074	2016-02-18 00:00:00	Posse	9
1075	2016-02-18 00:00:00	Professor Jamil	9
1076	2016-02-18 00:00:00	Quirinópolis	9
1077	2016-02-18 00:00:00	Rialma	9
1078	2016-02-18 00:00:00	Rianápolis	9
1079	2016-02-18 00:00:00	Rio Quente	9
1080	2016-02-18 00:00:00	Rio Verde	9
1081	2016-02-18 00:00:00	Rubiataba	9
1082	2016-02-18 00:00:00	Sanclerlândia	9
1083	2016-02-18 00:00:00	Santa Bárbara de Goiás	9
1084	2016-02-18 00:00:00	Santa Cruz de Goiás	9
1085	2016-02-18 00:00:00	Santa Fé de Goiás	9
1086	2016-02-18 00:00:00	Santa Helena de Goiás	9
1087	2016-02-18 00:00:00	Santa Isabel	9
1088	2016-02-18 00:00:00	Santa Rita do Araguaia	9
1089	2016-02-18 00:00:00	Santa Rita do Novo Destino	9
1090	2016-02-18 00:00:00	Santa Rosa de Goiás	9
1091	2016-02-18 00:00:00	Santa Tereza de Goiás	9
1092	2016-02-18 00:00:00	Santa Terezinha de Goiás	9
1093	2016-02-18 00:00:00	Santo Antônio da Barra	9
1094	2016-02-18 00:00:00	Santo Antônio de Goiás	9
1095	2016-02-18 00:00:00	Santo Antônio do Descoberto	9
1096	2016-02-18 00:00:00	São Domingos	9
1097	2016-02-18 00:00:00	São Francisco de Goiás	9
1098	2016-02-18 00:00:00	São João d'Aliança	9
1099	2016-02-18 00:00:00	São João da Paraúna	9
1100	2016-02-18 00:00:00	São Luís de Montes Belos	9
1101	2016-02-18 00:00:00	São Luíz do Norte	9
1102	2016-02-18 00:00:00	São Miguel do Araguaia	9
1103	2016-02-18 00:00:00	São Miguel do Passa Quatro	9
1104	2016-02-18 00:00:00	São Patrício	9
1105	2016-02-18 00:00:00	São Simão	9
1106	2016-02-18 00:00:00	Senador Canedo	9
1107	2016-02-18 00:00:00	Serranópolis	9
1108	2016-02-18 00:00:00	Silvânia	9
1109	2016-02-18 00:00:00	Simolândia	9
1110	2016-02-18 00:00:00	Sítio d'Abadia	9
1111	2016-02-18 00:00:00	Taquaral de Goiás	9
1112	2016-02-18 00:00:00	Teresina de Goiás	9
1113	2016-02-18 00:00:00	Terezópolis de Goiás	9
1114	2016-02-18 00:00:00	Três Ranchos	9
1115	2016-02-18 00:00:00	Trindade	9
1116	2016-02-18 00:00:00	Trombas	9
1117	2016-02-18 00:00:00	Turvânia	9
1118	2016-02-18 00:00:00	Turvelândia	9
1119	2016-02-18 00:00:00	Uirapuru	9
1120	2016-02-18 00:00:00	Uruaçu	9
1121	2016-02-18 00:00:00	Uruana	9
1122	2016-02-18 00:00:00	Urutaí	9
1123	2016-02-18 00:00:00	Valparaíso de Goiás	9
1124	2016-02-18 00:00:00	Varjão	9
1125	2016-02-18 00:00:00	Vianópolis	9
1126	2016-02-18 00:00:00	Vicentinópolis	9
1127	2016-02-18 00:00:00	Vila Boa	9
1128	2016-02-18 00:00:00	Vila Propício	9
1129	2016-02-18 00:00:00	Açailândia	10
1130	2016-02-18 00:00:00	Afonso Cunha	10
1131	2016-02-18 00:00:00	Água Doce do Maranhão	10
1132	2016-02-18 00:00:00	Alcântara	10
1133	2016-02-18 00:00:00	Aldeias Altas	10
1134	2016-02-18 00:00:00	Altamira do Maranhão	10
1135	2016-02-18 00:00:00	Alto Alegre do Maranhão	10
1136	2016-02-18 00:00:00	Alto Alegre do Pindaré	10
1137	2016-02-18 00:00:00	Alto Parnaíba	10
1138	2016-02-18 00:00:00	Amapá do Maranhão	10
1139	2016-02-18 00:00:00	Amarante do Maranhão	10
1140	2016-02-18 00:00:00	Anajatuba	10
1141	2016-02-18 00:00:00	Anapurus	10
1142	2016-02-18 00:00:00	Apicum-Açu	10
1143	2016-02-18 00:00:00	Araguanã	10
1144	2016-02-18 00:00:00	Araioses	10
1145	2016-02-18 00:00:00	Arame	10
1146	2016-02-18 00:00:00	Arari	10
1147	2016-02-18 00:00:00	Axixá	10
1148	2016-02-18 00:00:00	Bacabal	10
1149	2016-02-18 00:00:00	Bacabeira	10
1150	2016-02-18 00:00:00	Bacuri	10
1151	2016-02-18 00:00:00	Bacurituba	10
1152	2016-02-18 00:00:00	Balsas	10
1153	2016-02-18 00:00:00	Barão de Grajaú	10
1154	2016-02-18 00:00:00	Barra do Corda	10
1155	2016-02-18 00:00:00	Barreirinhas	10
1156	2016-02-18 00:00:00	Bela Vista do Maranhão	10
1157	2016-02-18 00:00:00	Belágua	10
1158	2016-02-18 00:00:00	Benedito Leite	10
1159	2016-02-18 00:00:00	Bequimão	10
1160	2016-02-18 00:00:00	Bernardo do Mearim	10
1161	2016-02-18 00:00:00	Boa Vista do Gurupi	10
1162	2016-02-18 00:00:00	Bom Jardim	10
1163	2016-02-18 00:00:00	Bom Jesus das Selvas	10
1164	2016-02-18 00:00:00	Bom Lugar	10
1165	2016-02-18 00:00:00	Brejo	10
1166	2016-02-18 00:00:00	Brejo de Areia	10
1167	2016-02-18 00:00:00	Buriti	10
1168	2016-02-18 00:00:00	Buriti Bravo	10
1169	2016-02-18 00:00:00	Buriticupu	10
1170	2016-02-18 00:00:00	Buritirana	10
1171	2016-02-18 00:00:00	Cachoeira Grande	10
1172	2016-02-18 00:00:00	Cajapió	10
1173	2016-02-18 00:00:00	Cajari	10
1174	2016-02-18 00:00:00	Campestre do Maranhão	10
1175	2016-02-18 00:00:00	Cândido Mendes	10
1176	2016-02-18 00:00:00	Cantanhede	10
1177	2016-02-18 00:00:00	Capinzal do Norte	10
1178	2016-02-18 00:00:00	Carolina	10
1179	2016-02-18 00:00:00	Carutapera	10
1180	2016-02-18 00:00:00	Caxias	10
1181	2016-02-18 00:00:00	Cedral	10
1182	2016-02-18 00:00:00	Central do Maranhão	10
1183	2016-02-18 00:00:00	Centro do Guilherme	10
1184	2016-02-18 00:00:00	Centro Novo do Maranhão	10
1185	2016-02-18 00:00:00	Chapadinha	10
1186	2016-02-18 00:00:00	Cidelândia	10
1187	2016-02-18 00:00:00	Codó	10
1188	2016-02-18 00:00:00	Coelho Neto	10
1189	2016-02-18 00:00:00	Colinas	10
1190	2016-02-18 00:00:00	Conceição do Lago-Açu	10
1191	2016-02-18 00:00:00	Coroatá	10
1192	2016-02-18 00:00:00	Cururupu	10
1193	2016-02-18 00:00:00	Davinópolis	10
1194	2016-02-18 00:00:00	Dom Pedro	10
1195	2016-02-18 00:00:00	Duque Bacelar	10
1196	2016-02-18 00:00:00	Esperantinópolis	10
1197	2016-02-18 00:00:00	Estreito	10
1198	2016-02-18 00:00:00	Feira Nova do Maranhão	10
1199	2016-02-18 00:00:00	Fernando Falcão	10
1200	2016-02-18 00:00:00	Formosa da Serra Negra	10
1201	2016-02-18 00:00:00	Fortaleza dos Nogueiras	10
1202	2016-02-18 00:00:00	Fortuna	10
1203	2016-02-18 00:00:00	Godofredo Viana	10
1204	2016-02-18 00:00:00	Gonçalves Dias	10
1205	2016-02-18 00:00:00	Governador Archer	10
1206	2016-02-18 00:00:00	Governador Edison Lobão	10
1207	2016-02-18 00:00:00	Governador Eugênio Barros	10
1208	2016-02-18 00:00:00	Governador Luiz Rocha	10
1209	2016-02-18 00:00:00	Governador Newton Bello	10
1210	2016-02-18 00:00:00	Governador Nunes Freire	10
1211	2016-02-18 00:00:00	Graça Aranha	10
1212	2016-02-18 00:00:00	Grajaú	10
1213	2016-02-18 00:00:00	Guimarães	10
1214	2016-02-18 00:00:00	Humberto de Campos	10
1215	2016-02-18 00:00:00	Icatu	10
1216	2016-02-18 00:00:00	Igarapé do Meio	10
1217	2016-02-18 00:00:00	Igarapé Grande	10
1218	2016-02-18 00:00:00	Imperatriz	10
1219	2016-02-18 00:00:00	Itaipava do Grajaú	10
1220	2016-02-18 00:00:00	Itapecuru Mirim	10
1221	2016-02-18 00:00:00	Itinga do Maranhão	10
1222	2016-02-18 00:00:00	Jatobá	10
1223	2016-02-18 00:00:00	Jenipapo dos Vieiras	10
1224	2016-02-18 00:00:00	João Lisboa	10
1225	2016-02-18 00:00:00	Joselândia	10
1226	2016-02-18 00:00:00	Junco do Maranhão	10
1227	2016-02-18 00:00:00	Lago da Pedra	10
1228	2016-02-18 00:00:00	Lago do Junco	10
1229	2016-02-18 00:00:00	Lago dos Rodrigues	10
1230	2016-02-18 00:00:00	Lago Verde	10
1231	2016-02-18 00:00:00	Lagoa do Mato	10
1232	2016-02-18 00:00:00	Lagoa Grande do Maranhão	10
1233	2016-02-18 00:00:00	Lajeado Novo	10
1234	2016-02-18 00:00:00	Lima Campos	10
1235	2016-02-18 00:00:00	Loreto	10
1236	2016-02-18 00:00:00	Luís Domingues	10
1237	2016-02-18 00:00:00	Magalhães de Almeida	10
1238	2016-02-18 00:00:00	Maracaçumé	10
1239	2016-02-18 00:00:00	Marajá do Sena	10
1240	2016-02-18 00:00:00	Maranhãozinho	10
1241	2016-02-18 00:00:00	Mata Roma	10
1242	2016-02-18 00:00:00	Matinha	10
1243	2016-02-18 00:00:00	Matões	10
1244	2016-02-18 00:00:00	Matões do Norte	10
1245	2016-02-18 00:00:00	Milagres do Maranhão	10
1246	2016-02-18 00:00:00	Mirador	10
1247	2016-02-18 00:00:00	Miranda do Norte	10
1248	2016-02-18 00:00:00	Mirinzal	10
1249	2016-02-18 00:00:00	Monção	10
1250	2016-02-18 00:00:00	Montes Altos	10
1251	2016-02-18 00:00:00	Morros	10
1252	2016-02-18 00:00:00	Nina Rodrigues	10
1253	2016-02-18 00:00:00	Nova Colinas	10
1254	2016-02-18 00:00:00	Nova Iorque	10
1255	2016-02-18 00:00:00	Nova Olinda do Maranhão	10
1256	2016-02-18 00:00:00	Olho d'Água das Cunhãs	10
1257	2016-02-18 00:00:00	Olinda Nova do Maranhão	10
1258	2016-02-18 00:00:00	Paço do Lumiar	10
1259	2016-02-18 00:00:00	Palmeirândia	10
1260	2016-02-18 00:00:00	Paraibano	10
1261	2016-02-18 00:00:00	Parnarama	10
1262	2016-02-18 00:00:00	Passagem Franca	10
1263	2016-02-18 00:00:00	Pastos Bons	10
1264	2016-02-18 00:00:00	Paulino Neves	10
1265	2016-02-18 00:00:00	Paulo Ramos	10
1266	2016-02-18 00:00:00	Pedreiras	10
1267	2016-02-18 00:00:00	Pedro do Rosário	10
1268	2016-02-18 00:00:00	Penalva	10
1269	2016-02-18 00:00:00	Peri Mirim	10
1270	2016-02-18 00:00:00	Peritoró	10
1271	2016-02-18 00:00:00	Pindaré-Mirim	10
1272	2016-02-18 00:00:00	Pinheiro	10
1273	2016-02-18 00:00:00	Pio XII	10
1274	2016-02-18 00:00:00	Pirapemas	10
1275	2016-02-18 00:00:00	Poção de Pedras	10
1276	2016-02-18 00:00:00	Porto Franco	10
1277	2016-02-18 00:00:00	Porto Rico do Maranhão	10
1278	2016-02-18 00:00:00	Presidente Dutra	10
1279	2016-02-18 00:00:00	Presidente Juscelino	10
1280	2016-02-18 00:00:00	Presidente Médici	10
1281	2016-02-18 00:00:00	Presidente Sarney	10
1282	2016-02-18 00:00:00	Presidente Vargas	10
1283	2016-02-18 00:00:00	Primeira Cruz	10
1284	2016-02-18 00:00:00	Raposa	10
1285	2016-02-18 00:00:00	Riachão	10
1286	2016-02-18 00:00:00	Ribamar Fiquene	10
1287	2016-02-18 00:00:00	Rosário	10
1288	2016-02-18 00:00:00	Sambaíba	10
1289	2016-02-18 00:00:00	Santa Filomena do Maranhão	10
1290	2016-02-18 00:00:00	Santa Helena	10
1291	2016-02-18 00:00:00	Santa Inês	10
1292	2016-02-18 00:00:00	Santa Luzia	10
1293	2016-02-18 00:00:00	Santa Luzia do Paruá	10
1294	2016-02-18 00:00:00	Santa Quitéria do Maranhão	10
1295	2016-02-18 00:00:00	Santa Rita	10
1296	2016-02-18 00:00:00	Santana do Maranhão	10
1297	2016-02-18 00:00:00	Santo Amaro do Maranhão	10
1298	2016-02-18 00:00:00	Santo Antônio dos Lopes	10
1299	2016-02-18 00:00:00	São Benedito do Rio Preto	10
1300	2016-02-18 00:00:00	São Bento	10
1301	2016-02-18 00:00:00	São Bernardo	10
1302	2016-02-18 00:00:00	São Domingos do Azeitão	10
1303	2016-02-18 00:00:00	São Domingos do Maranhão	10
1304	2016-02-18 00:00:00	São Félix de Balsas	10
1305	2016-02-18 00:00:00	São Francisco do Brejão	10
1306	2016-02-18 00:00:00	São Francisco do Maranhão	10
1307	2016-02-18 00:00:00	São João Batista	10
1308	2016-02-18 00:00:00	São João do Carú	10
1309	2016-02-18 00:00:00	São João do Paraíso	10
1310	2016-02-18 00:00:00	São João do Soter	10
1311	2016-02-18 00:00:00	São João dos Patos	10
1312	2016-02-18 00:00:00	São José de Ribamar	10
1429	2016-02-18 00:00:00	Nova Xavantina	13
1313	2016-02-18 00:00:00	São José dos Basílios	10
1314	2016-02-18 00:00:00	São Luís	10
1315	2016-02-18 00:00:00	São Luís Gonzaga do Maranhão	10
1316	2016-02-18 00:00:00	São Mateus do Maranhão	10
1317	2016-02-18 00:00:00	São Pedro da Água Branca	10
1318	2016-02-18 00:00:00	São Pedro dos Crentes	10
1319	2016-02-18 00:00:00	São Raimundo das Mangabeiras	10
1320	2016-02-18 00:00:00	São Raimundo do Doca Bezerra	10
1321	2016-02-18 00:00:00	São Roberto	10
1322	2016-02-18 00:00:00	São Vicente Ferrer	10
1323	2016-02-18 00:00:00	Satubinha	10
1324	2016-02-18 00:00:00	Senador Alexandre Costa	10
1325	2016-02-18 00:00:00	Senador La Rocque	10
1326	2016-02-18 00:00:00	Serrano do Maranhão	10
1327	2016-02-18 00:00:00	Sítio Novo	10
1328	2016-02-18 00:00:00	Sucupira do Norte	10
1329	2016-02-18 00:00:00	Sucupira do Riachão	10
1330	2016-02-18 00:00:00	Tasso Fragoso	10
1331	2016-02-18 00:00:00	Timbiras	10
1332	2016-02-18 00:00:00	Timon	10
1333	2016-02-18 00:00:00	Trizidela do Vale	10
1334	2016-02-18 00:00:00	Tufilândia	10
1335	2016-02-18 00:00:00	Tuntum	10
1336	2016-02-18 00:00:00	Turiaçu	10
1337	2016-02-18 00:00:00	Turilândia	10
1338	2016-02-18 00:00:00	Tutóia	10
1339	2016-02-18 00:00:00	Urbano Santos	10
1340	2016-02-18 00:00:00	Vargem Grande	10
1341	2016-02-18 00:00:00	Viana	10
1342	2016-02-18 00:00:00	Vila Nova dos Martírios	10
1343	2016-02-18 00:00:00	Vitória do Mearim	10
1344	2016-02-18 00:00:00	Vitorino Freire	10
1345	2016-02-18 00:00:00	Zé Doca	10
1346	2016-02-18 00:00:00	Acorizal	13
1347	2016-02-18 00:00:00	Água Boa	13
1348	2016-02-18 00:00:00	Alta Floresta	13
1349	2016-02-18 00:00:00	Alto Araguaia	13
1350	2016-02-18 00:00:00	Alto Boa Vista	13
1351	2016-02-18 00:00:00	Alto Garças	13
1352	2016-02-18 00:00:00	Alto Paraguai	13
1353	2016-02-18 00:00:00	Alto Taquari	13
1354	2016-02-18 00:00:00	Apiacás	13
1355	2016-02-18 00:00:00	Araguaiana	13
1356	2016-02-18 00:00:00	Araguainha	13
1357	2016-02-18 00:00:00	Araputanga	13
1358	2016-02-18 00:00:00	Arenápolis	13
1359	2016-02-18 00:00:00	Aripuanã	13
1360	2016-02-18 00:00:00	Barão de Melgaço	13
1361	2016-02-18 00:00:00	Barra do Bugres	13
1362	2016-02-18 00:00:00	Barra do Garças	13
1363	2016-02-18 00:00:00	Bom Jesus do Araguaia	13
1364	2016-02-18 00:00:00	Brasnorte	13
1365	2016-02-18 00:00:00	Cáceres	13
1366	2016-02-18 00:00:00	Campinápolis	13
1367	2016-02-18 00:00:00	Campo Novo do Parecis	13
1368	2016-02-18 00:00:00	Campo Verde	13
1369	2016-02-18 00:00:00	Campos de Júlio	13
1370	2016-02-18 00:00:00	Canabrava do Norte	13
1371	2016-02-18 00:00:00	Canarana	13
1372	2016-02-18 00:00:00	Carlinda	13
1373	2016-02-18 00:00:00	Castanheira	13
1374	2016-02-18 00:00:00	Chapada dos Guimarães	13
1375	2016-02-18 00:00:00	Cláudia	13
1376	2016-02-18 00:00:00	Cocalinho	13
1377	2016-02-18 00:00:00	Colíder	13
1378	2016-02-18 00:00:00	Colniza	13
1379	2016-02-18 00:00:00	Comodoro	13
1380	2016-02-18 00:00:00	Confresa	13
1381	2016-02-18 00:00:00	Conquista D'Oeste	13
1382	2016-02-18 00:00:00	Cotriguaçu	13
1383	2016-02-18 00:00:00	Cuiabá	13
1384	2016-02-18 00:00:00	Curvelândia	13
1385	2016-02-18 00:00:00	Denise	13
1386	2016-02-18 00:00:00	Diamantino	13
1387	2016-02-18 00:00:00	Dom Aquino	13
1388	2016-02-18 00:00:00	Feliz Natal	13
1389	2016-02-18 00:00:00	Figueirópolis D'Oeste	13
1390	2016-02-18 00:00:00	Gaúcha do Norte	13
1391	2016-02-18 00:00:00	General Carneiro	13
1392	2016-02-18 00:00:00	Glória D'Oeste	13
1393	2016-02-18 00:00:00	Guarantã do Norte	13
1394	2016-02-18 00:00:00	Guiratinga	13
1395	2016-02-18 00:00:00	Indiavaí	13
1396	2016-02-18 00:00:00	Ipiranga do Norte	13
1397	2016-02-18 00:00:00	Itanhangá	13
1398	2016-02-18 00:00:00	Itaúba	13
1399	2016-02-18 00:00:00	Itiquira	13
1400	2016-02-18 00:00:00	Jaciara	13
1401	2016-02-18 00:00:00	Jangada	13
1402	2016-02-18 00:00:00	Jauru	13
1403	2016-02-18 00:00:00	Juara	13
1404	2016-02-18 00:00:00	Juína	13
1405	2016-02-18 00:00:00	Juruena	13
1406	2016-02-18 00:00:00	Juscimeira	13
1407	2016-02-18 00:00:00	Lambari D'Oeste	13
1408	2016-02-18 00:00:00	Lucas do Rio Verde	13
1409	2016-02-18 00:00:00	Luciara	13
1410	2016-02-18 00:00:00	Marcelândia	13
1411	2016-02-18 00:00:00	Matupá	13
1412	2016-02-18 00:00:00	Mirassol d'Oeste	13
1413	2016-02-18 00:00:00	Nobres	13
1414	2016-02-18 00:00:00	Nortelândia	13
1415	2016-02-18 00:00:00	Nossa Senhora do Livramento	13
1416	2016-02-18 00:00:00	Nova Bandeirantes	13
1417	2016-02-18 00:00:00	Nova Brasilândia	13
1418	2016-02-18 00:00:00	Nova Canaã do Norte	13
1419	2016-02-18 00:00:00	Nova Guarita	13
1420	2016-02-18 00:00:00	Nova Lacerda	13
1421	2016-02-18 00:00:00	Nova Marilândia	13
1422	2016-02-18 00:00:00	Nova Maringá	13
1423	2016-02-18 00:00:00	Nova Monte Verde	13
1424	2016-02-18 00:00:00	Nova Mutum	13
1425	2016-02-18 00:00:00	Nova Nazaré	13
1426	2016-02-18 00:00:00	Nova Olímpia	13
1427	2016-02-18 00:00:00	Nova Santa Helena	13
1428	2016-02-18 00:00:00	Nova Ubiratã	13
1430	2016-02-18 00:00:00	Novo Horizonte do Norte	13
1431	2016-02-18 00:00:00	Novo Mundo	13
1432	2016-02-18 00:00:00	Novo Santo Antônio	13
1433	2016-02-18 00:00:00	Novo São Joaquim	13
1434	2016-02-18 00:00:00	Paranaíta	13
1435	2016-02-18 00:00:00	Paranatinga	13
1436	2016-02-18 00:00:00	Pedra Preta	13
1437	2016-02-18 00:00:00	Peixoto de Azevedo	13
1438	2016-02-18 00:00:00	Planalto da Serra	13
1439	2016-02-18 00:00:00	Poconé	13
1440	2016-02-18 00:00:00	Pontal do Araguaia	13
1441	2016-02-18 00:00:00	Ponte Branca	13
1442	2016-02-18 00:00:00	Pontes e Lacerda	13
1443	2016-02-18 00:00:00	Porto Alegre do Norte	13
1444	2016-02-18 00:00:00	Porto dos Gaúchos	13
1445	2016-02-18 00:00:00	Porto Esperidião	13
1446	2016-02-18 00:00:00	Porto Estrela	13
1447	2016-02-18 00:00:00	Poxoréo	13
1448	2016-02-18 00:00:00	Primavera do Leste	13
1449	2016-02-18 00:00:00	Querência	13
1450	2016-02-18 00:00:00	Reserva do Cabaçal	13
1451	2016-02-18 00:00:00	Ribeirão Cascalheira	13
1452	2016-02-18 00:00:00	Ribeirãozinho	13
1453	2016-02-18 00:00:00	Rio Branco	13
1454	2016-02-18 00:00:00	Rondolândia	13
1455	2016-02-18 00:00:00	Rondonópolis	13
1456	2016-02-18 00:00:00	Rosário Oeste	13
1457	2016-02-18 00:00:00	Salto do Céu	13
1458	2016-02-18 00:00:00	Santa Carmem	13
1459	2016-02-18 00:00:00	Santa Cruz do Xingu	13
1460	2016-02-18 00:00:00	Santa Rita do Trivelato	13
1461	2016-02-18 00:00:00	Santa Terezinha	13
1462	2016-02-18 00:00:00	Santo Afonso	13
1463	2016-02-18 00:00:00	Santo Antônio do Leste	13
1464	2016-02-18 00:00:00	Santo Antônio do Leverger	13
1465	2016-02-18 00:00:00	São Félix do Araguaia	13
1466	2016-02-18 00:00:00	São José do Povo	13
1467	2016-02-18 00:00:00	São José do Rio Claro	13
1468	2016-02-18 00:00:00	São José do Xingu	13
1469	2016-02-18 00:00:00	São José dos Quatro Marcos	13
1470	2016-02-18 00:00:00	São Pedro da Cipa	13
1471	2016-02-18 00:00:00	Sapezal	13
1472	2016-02-18 00:00:00	Serra Nova Dourada	13
1473	2016-02-18 00:00:00	Sinop	13
1474	2016-02-18 00:00:00	Sorriso	13
1475	2016-02-18 00:00:00	Tabaporã	13
1476	2016-02-18 00:00:00	Tangará da Serra	13
1477	2016-02-18 00:00:00	Tapurah	13
1478	2016-02-18 00:00:00	Terra Nova do Norte	13
1479	2016-02-18 00:00:00	Tesouro	13
1480	2016-02-18 00:00:00	Torixoréu	13
1481	2016-02-18 00:00:00	União do Sul	13
1482	2016-02-18 00:00:00	Vale de São Domingos	13
1483	2016-02-18 00:00:00	Várzea Grande	13
1484	2016-02-18 00:00:00	Vera	13
1485	2016-02-18 00:00:00	Vila Bela da Santíssima Trindade	13
1486	2016-02-18 00:00:00	Vila Rica	13
1487	2016-02-18 00:00:00	Água Clara	12
1488	2016-02-18 00:00:00	Alcinópolis	12
1489	2016-02-18 00:00:00	Amambai	12
1490	2016-02-18 00:00:00	Anastácio	12
1491	2016-02-18 00:00:00	Anaurilândia	12
1492	2016-02-18 00:00:00	Angélica	12
1493	2016-02-18 00:00:00	Antônio João	12
1494	2016-02-18 00:00:00	Aparecida do Taboado	12
1495	2016-02-18 00:00:00	Aquidauana	12
1496	2016-02-18 00:00:00	Aral Moreira	12
1497	2016-02-18 00:00:00	Bandeirantes	12
1498	2016-02-18 00:00:00	Bataguassu	12
1499	2016-02-18 00:00:00	Batayporã	12
1500	2016-02-18 00:00:00	Bela Vista	12
1501	2016-02-18 00:00:00	Bodoquena	12
1502	2016-02-18 00:00:00	Bonito	12
1503	2016-02-18 00:00:00	Brasilândia	12
1504	2016-02-18 00:00:00	Caarapó	12
1505	2016-02-18 00:00:00	Camapuã	12
1506	2016-02-18 00:00:00	Campo Grande	12
1507	2016-02-18 00:00:00	Caracol	12
1508	2016-02-18 00:00:00	Cassilândia	12
1509	2016-02-18 00:00:00	Chapadão do Sul	12
1510	2016-02-18 00:00:00	Corguinho	12
1511	2016-02-18 00:00:00	Coronel Sapucaia	12
1512	2016-02-18 00:00:00	Corumbá	12
1513	2016-02-18 00:00:00	Costa Rica	12
1514	2016-02-18 00:00:00	Coxim	12
1515	2016-02-18 00:00:00	Deodápolis	12
1516	2016-02-18 00:00:00	Dois Irmãos do Buriti	12
1517	2016-02-18 00:00:00	Douradina	12
1518	2016-02-18 00:00:00	Dourados	12
1519	2016-02-18 00:00:00	Eldorado	12
1520	2016-02-18 00:00:00	Fátima do Sul	12
1521	2016-02-18 00:00:00	Figueirão	12
1522	2016-02-18 00:00:00	Glória de Dourados	12
1523	2016-02-18 00:00:00	Guia Lopes da Laguna	12
1524	2016-02-18 00:00:00	Iguatemi	12
1525	2016-02-18 00:00:00	Inocência	12
1526	2016-02-18 00:00:00	Itaporã	12
1527	2016-02-18 00:00:00	Itaquiraí	12
1528	2016-02-18 00:00:00	Ivinhema	12
1529	2016-02-18 00:00:00	Japorã	12
1530	2016-02-18 00:00:00	Jaraguari	12
1531	2016-02-18 00:00:00	Jardim	12
1532	2016-02-18 00:00:00	Jateí	12
1533	2016-02-18 00:00:00	Juti	12
1534	2016-02-18 00:00:00	Ladário	12
1535	2016-02-18 00:00:00	Laguna Carapã	12
1536	2016-02-18 00:00:00	Maracaju	12
1537	2016-02-18 00:00:00	Miranda	12
1538	2016-02-18 00:00:00	Mundo Novo	12
1539	2016-02-18 00:00:00	Naviraí	12
1540	2016-02-18 00:00:00	Nioaque	12
1541	2016-02-18 00:00:00	Nova Alvorada do Sul	12
1542	2016-02-18 00:00:00	Nova Andradina	12
1543	2016-02-18 00:00:00	Novo Horizonte do Sul	12
1544	2016-02-18 00:00:00	Paranaíba	12
1545	2016-02-18 00:00:00	Paranhos	12
1546	2016-02-18 00:00:00	Pedro Gomes	12
1547	2016-02-18 00:00:00	Ponta Porã	12
1548	2016-02-18 00:00:00	Porto Murtinho	12
1549	2016-02-18 00:00:00	Ribas do Rio Pardo	12
1550	2016-02-18 00:00:00	Rio Brilhante	12
1551	2016-02-18 00:00:00	Rio Negro	12
1552	2016-02-18 00:00:00	Rio Verde de Mato Grosso	12
1553	2016-02-18 00:00:00	Rochedo	12
1554	2016-02-18 00:00:00	Santa Rita do Pardo	12
1555	2016-02-18 00:00:00	São Gabriel do Oeste	12
1556	2016-02-18 00:00:00	Selvíria	12
1557	2016-02-18 00:00:00	Sete Quedas	12
1558	2016-02-18 00:00:00	Sidrolândia	12
1559	2016-02-18 00:00:00	Sonora	12
1560	2016-02-18 00:00:00	Tacuru	12
1561	2016-02-18 00:00:00	Taquarussu	12
1562	2016-02-18 00:00:00	Terenos	12
1563	2016-02-18 00:00:00	Três Lagoas	12
1564	2016-02-18 00:00:00	Vicentina	12
1565	2016-02-18 00:00:00	Abadia dos Dourados	11
1566	2016-02-18 00:00:00	Abaeté	11
1567	2016-02-18 00:00:00	Abre Campo	11
1568	2016-02-18 00:00:00	Acaiaca	11
1569	2016-02-18 00:00:00	Açucena	11
1570	2016-02-18 00:00:00	Água Boa	11
1571	2016-02-18 00:00:00	Água Comprida	11
1572	2016-02-18 00:00:00	Aguanil	11
1573	2016-02-18 00:00:00	Águas Formosas	11
1574	2016-02-18 00:00:00	Águas Vermelhas	11
1575	2016-02-18 00:00:00	Aimorés	11
1576	2016-02-18 00:00:00	Aiuruoca	11
1577	2016-02-18 00:00:00	Alagoa	11
1578	2016-02-18 00:00:00	Albertina	11
1579	2016-02-18 00:00:00	Além Paraíba	11
1580	2016-02-18 00:00:00	Alfenas	11
1581	2016-02-18 00:00:00	Alfredo Vasconcelos	11
1582	2016-02-18 00:00:00	Almenara	11
1583	2016-02-18 00:00:00	Alpercata	11
1584	2016-02-18 00:00:00	Alpinópolis	11
1585	2016-02-18 00:00:00	Alterosa	11
1586	2016-02-18 00:00:00	Alto Caparaó	11
1587	2016-02-18 00:00:00	Alto Jequitibá	11
1588	2016-02-18 00:00:00	Alto Rio Doce	11
1589	2016-02-18 00:00:00	Alvarenga	11
1590	2016-02-18 00:00:00	Alvinópolis	11
1591	2016-02-18 00:00:00	Alvorada de Minas	11
1592	2016-02-18 00:00:00	Amparo do Serra	11
1593	2016-02-18 00:00:00	Andradas	11
1594	2016-02-18 00:00:00	Andrelândia	11
1595	2016-02-18 00:00:00	Angelândia	11
1596	2016-02-18 00:00:00	Antônio Carlos	11
1597	2016-02-18 00:00:00	Antônio Dias	11
1598	2016-02-18 00:00:00	Antônio Prado de Minas	11
1599	2016-02-18 00:00:00	Araçaí	11
1600	2016-02-18 00:00:00	Aracitaba	11
1601	2016-02-18 00:00:00	Araçuaí	11
1602	2016-02-18 00:00:00	Araguari	11
1603	2016-02-18 00:00:00	Arantina	11
1604	2016-02-18 00:00:00	Araponga	11
1605	2016-02-18 00:00:00	Araporã	11
1606	2016-02-18 00:00:00	Arapuá	11
1607	2016-02-18 00:00:00	Araújos	11
1608	2016-02-18 00:00:00	Araxá	11
1609	2016-02-18 00:00:00	Arceburgo	11
1610	2016-02-18 00:00:00	Arcos	11
1611	2016-02-18 00:00:00	Areado	11
1612	2016-02-18 00:00:00	Argirita	11
1613	2016-02-18 00:00:00	Aricanduva	11
1614	2016-02-18 00:00:00	Arinos	11
1615	2016-02-18 00:00:00	Astolfo Dutra	11
1616	2016-02-18 00:00:00	Ataléia	11
1617	2016-02-18 00:00:00	Augusto de Lima	11
1618	2016-02-18 00:00:00	Baependi	11
1619	2016-02-18 00:00:00	Baldim	11
1620	2016-02-18 00:00:00	Bambuí	11
1621	2016-02-18 00:00:00	Bandeira	11
1622	2016-02-18 00:00:00	Bandeira do Sul	11
1623	2016-02-18 00:00:00	Barão de Cocais	11
1624	2016-02-18 00:00:00	Barão de Monte Alto	11
1625	2016-02-18 00:00:00	Barbacena	11
1626	2016-02-18 00:00:00	Barra Longa	11
1627	2016-02-18 00:00:00	Barroso	11
1628	2016-02-18 00:00:00	Bela Vista de Minas	11
1629	2016-02-18 00:00:00	Belmiro Braga	11
1630	2016-02-18 00:00:00	Belo Horizonte	11
1631	2016-02-18 00:00:00	Belo Oriente	11
1632	2016-02-18 00:00:00	Belo Vale	11
1633	2016-02-18 00:00:00	Berilo	11
1634	2016-02-18 00:00:00	Berizal	11
1635	2016-02-18 00:00:00	Bertópolis	11
1636	2016-02-18 00:00:00	Betim	11
1637	2016-02-18 00:00:00	Bias Fortes	11
1638	2016-02-18 00:00:00	Bicas	11
1639	2016-02-18 00:00:00	Biquinhas	11
1640	2016-02-18 00:00:00	Boa Esperança	11
1641	2016-02-18 00:00:00	Bocaina de Minas	11
1642	2016-02-18 00:00:00	Bocaiúva	11
1643	2016-02-18 00:00:00	Bom Despacho	11
1644	2016-02-18 00:00:00	Bom Jardim de Minas	11
1645	2016-02-18 00:00:00	Bom Jesus da Penha	11
1646	2016-02-18 00:00:00	Bom Jesus do Amparo	11
1647	2016-02-18 00:00:00	Bom Jesus do Galho	11
1648	2016-02-18 00:00:00	Bom Repouso	11
1649	2016-02-18 00:00:00	Bom Sucesso	11
1650	2016-02-18 00:00:00	Bonfim	11
1651	2016-02-18 00:00:00	Bonfinópolis de Minas	11
1652	2016-02-18 00:00:00	Bonito de Minas	11
1653	2016-02-18 00:00:00	Borda da Mata	11
1654	2016-02-18 00:00:00	Botelhos	11
1655	2016-02-18 00:00:00	Botumirim	11
1656	2016-02-18 00:00:00	Brás Pires	11
1657	2016-02-18 00:00:00	Brasilândia de Minas	11
1658	2016-02-18 00:00:00	Brasília de Minas	11
1659	2016-02-18 00:00:00	Brasópolis	11
1660	2016-02-18 00:00:00	Braúnas	11
1661	2016-02-18 00:00:00	Brumadinho	11
1662	2016-02-18 00:00:00	Bueno Brandão	11
1663	2016-02-18 00:00:00	Buenópolis	11
1664	2016-02-18 00:00:00	Bugre	11
1665	2016-02-18 00:00:00	Buritis	11
1666	2016-02-18 00:00:00	Buritizeiro	11
1667	2016-02-18 00:00:00	Cabeceira Grande	11
1668	2016-02-18 00:00:00	Cabo Verde	11
1669	2016-02-18 00:00:00	Cachoeira da Prata	11
1670	2016-02-18 00:00:00	Cachoeira de Minas	11
1671	2016-02-18 00:00:00	Cachoeira de Pajeú	11
1672	2016-02-18 00:00:00	Cachoeira Dourada	11
1673	2016-02-18 00:00:00	Caetanópolis	11
1674	2016-02-18 00:00:00	Caeté	11
1675	2016-02-18 00:00:00	Caiana	11
1676	2016-02-18 00:00:00	Cajuri	11
1677	2016-02-18 00:00:00	Caldas	11
1678	2016-02-18 00:00:00	Camacho	11
1679	2016-02-18 00:00:00	Camanducaia	11
1680	2016-02-18 00:00:00	Cambuí	11
1681	2016-02-18 00:00:00	Cambuquira	11
1682	2016-02-18 00:00:00	Campanário	11
1683	2016-02-18 00:00:00	Campanha	11
1684	2016-02-18 00:00:00	Campestre	11
1685	2016-02-18 00:00:00	Campina Verde	11
1686	2016-02-18 00:00:00	Campo Azul	11
1687	2016-02-18 00:00:00	Campo Belo	11
1688	2016-02-18 00:00:00	Campo do Meio	11
1689	2016-02-18 00:00:00	Campo Florido	11
1690	2016-02-18 00:00:00	Campos Altos	11
1691	2016-02-18 00:00:00	Campos Gerais	11
1692	2016-02-18 00:00:00	Cana Verde	11
1693	2016-02-18 00:00:00	Canaã	11
1694	2016-02-18 00:00:00	Canápolis	11
1695	2016-02-18 00:00:00	Candeias	11
1696	2016-02-18 00:00:00	Cantagalo	11
1697	2016-02-18 00:00:00	Caparaó	11
1698	2016-02-18 00:00:00	Capela Nova	11
1699	2016-02-18 00:00:00	Capelinha	11
1700	2016-02-18 00:00:00	Capetinga	11
1701	2016-02-18 00:00:00	Capim Branco	11
1702	2016-02-18 00:00:00	Capinópolis	11
1703	2016-02-18 00:00:00	Capitão Andrade	11
1704	2016-02-18 00:00:00	Capitão Enéas	11
1705	2016-02-18 00:00:00	Capitólio	11
1706	2016-02-18 00:00:00	Caputira	11
1707	2016-02-18 00:00:00	Caraí	11
1708	2016-02-18 00:00:00	Caranaíba	11
1709	2016-02-18 00:00:00	Carandaí	11
1710	2016-02-18 00:00:00	Carangola	11
1711	2016-02-18 00:00:00	Caratinga	11
1712	2016-02-18 00:00:00	Carbonita	11
1713	2016-02-18 00:00:00	Careaçu	11
1714	2016-02-18 00:00:00	Carlos Chagas	11
1715	2016-02-18 00:00:00	Carmésia	11
1716	2016-02-18 00:00:00	Carmo da Cachoeira	11
1717	2016-02-18 00:00:00	Carmo da Mata	11
1718	2016-02-18 00:00:00	Carmo de Minas	11
1719	2016-02-18 00:00:00	Carmo do Cajuru	11
1720	2016-02-18 00:00:00	Carmo do Paranaíba	11
1721	2016-02-18 00:00:00	Carmo do Rio Claro	11
1722	2016-02-18 00:00:00	Carmópolis de Minas	11
1723	2016-02-18 00:00:00	Carneirinho	11
1724	2016-02-18 00:00:00	Carrancas	11
1725	2016-02-18 00:00:00	Carvalhópolis	11
1726	2016-02-18 00:00:00	Carvalhos	11
1727	2016-02-18 00:00:00	Casa Grande	11
1728	2016-02-18 00:00:00	Cascalho Rico	11
1729	2016-02-18 00:00:00	Cássia	11
1730	2016-02-18 00:00:00	Cataguases	11
1731	2016-02-18 00:00:00	Catas Altas	11
1732	2016-02-18 00:00:00	Catas Altas da Noruega	11
1733	2016-02-18 00:00:00	Catuji	11
1734	2016-02-18 00:00:00	Catuti	11
1735	2016-02-18 00:00:00	Caxambu	11
1736	2016-02-18 00:00:00	Cedro do Abaeté	11
1737	2016-02-18 00:00:00	Central de Minas	11
1738	2016-02-18 00:00:00	Centralina	11
1739	2016-02-18 00:00:00	Chácara	11
1740	2016-02-18 00:00:00	Chalé	11
1741	2016-02-18 00:00:00	Chapada do Norte	11
1742	2016-02-18 00:00:00	Chapada Gaúcha	11
1743	2016-02-18 00:00:00	Chiador	11
1744	2016-02-18 00:00:00	Cipotânea	11
1745	2016-02-18 00:00:00	Claraval	11
1746	2016-02-18 00:00:00	Claro dos Poções	11
1747	2016-02-18 00:00:00	Cláudio	11
1748	2016-02-18 00:00:00	Coimbra	11
1749	2016-02-18 00:00:00	Coluna	11
1750	2016-02-18 00:00:00	Comendador Gomes	11
1751	2016-02-18 00:00:00	Comercinho	11
1752	2016-02-18 00:00:00	Conceição da Aparecida	11
1753	2016-02-18 00:00:00	Conceição da Barra de Minas	11
1754	2016-02-18 00:00:00	Conceição das Alagoas	11
1755	2016-02-18 00:00:00	Conceição das Pedras	11
1756	2016-02-18 00:00:00	Conceição de Ipanema	11
1757	2016-02-18 00:00:00	Conceição do Mato Dentro	11
1758	2016-02-18 00:00:00	Conceição do Pará	11
1759	2016-02-18 00:00:00	Conceição do Rio Verde	11
1760	2016-02-18 00:00:00	Conceição dos Ouros	11
1761	2016-02-18 00:00:00	Cônego Marinho	11
1762	2016-02-18 00:00:00	Confins	11
1763	2016-02-18 00:00:00	Congonhal	11
1764	2016-02-18 00:00:00	Congonhas	11
1765	2016-02-18 00:00:00	Congonhas do Norte	11
1766	2016-02-18 00:00:00	Conquista	11
1767	2016-02-18 00:00:00	Conselheiro Lafaiete	11
1768	2016-02-18 00:00:00	Conselheiro Pena	11
1769	2016-02-18 00:00:00	Consolação	11
1770	2016-02-18 00:00:00	Contagem	11
1771	2016-02-18 00:00:00	Coqueiral	11
1772	2016-02-18 00:00:00	Coração de Jesus	11
1773	2016-02-18 00:00:00	Cordisburgo	11
1774	2016-02-18 00:00:00	Cordislândia	11
1775	2016-02-18 00:00:00	Corinto	11
1776	2016-02-18 00:00:00	Coroaci	11
1777	2016-02-18 00:00:00	Coromandel	11
1778	2016-02-18 00:00:00	Coronel Fabriciano	11
1779	2016-02-18 00:00:00	Coronel Murta	11
1780	2016-02-18 00:00:00	Coronel Pacheco	11
1781	2016-02-18 00:00:00	Coronel Xavier Chaves	11
1782	2016-02-18 00:00:00	Córrego Danta	11
1783	2016-02-18 00:00:00	Córrego do Bom Jesus	11
1784	2016-02-18 00:00:00	Córrego Fundo	11
1785	2016-02-18 00:00:00	Córrego Novo	11
1786	2016-02-18 00:00:00	Couto de Magalhães de Minas	11
1787	2016-02-18 00:00:00	Crisólita	11
1788	2016-02-18 00:00:00	Cristais	11
1789	2016-02-18 00:00:00	Cristália	11
1790	2016-02-18 00:00:00	Cristiano Otoni	11
1791	2016-02-18 00:00:00	Cristina	11
1792	2016-02-18 00:00:00	Crucilândia	11
1793	2016-02-18 00:00:00	Cruzeiro da Fortaleza	11
1794	2016-02-18 00:00:00	Cruzília	11
1795	2016-02-18 00:00:00	Cuparaque	11
1796	2016-02-18 00:00:00	Curral de Dentro	11
1797	2016-02-18 00:00:00	Curvelo	11
1798	2016-02-18 00:00:00	Datas	11
1799	2016-02-18 00:00:00	Delfim Moreira	11
1800	2016-02-18 00:00:00	Delfinópolis	11
1801	2016-02-18 00:00:00	Delta	11
1802	2016-02-18 00:00:00	Descoberto	11
1803	2016-02-18 00:00:00	Desterro de Entre Rios	11
1804	2016-02-18 00:00:00	Desterro do Melo	11
1805	2016-02-18 00:00:00	Diamantina	11
1806	2016-02-18 00:00:00	Diogo de Vasconcelos	11
1807	2016-02-18 00:00:00	Dionísio	11
1808	2016-02-18 00:00:00	Divinésia	11
1809	2016-02-18 00:00:00	Divino	11
1810	2016-02-18 00:00:00	Divino das Laranjeiras	11
1811	2016-02-18 00:00:00	Divinolândia de Minas	11
1812	2016-02-18 00:00:00	Divinópolis	11
1813	2016-02-18 00:00:00	Divisa Alegre	11
1814	2016-02-18 00:00:00	Divisa Nova	11
1815	2016-02-18 00:00:00	Divisópolis	11
1816	2016-02-18 00:00:00	Dom Bosco	11
1817	2016-02-18 00:00:00	Dom Cavati	11
1818	2016-02-18 00:00:00	Dom Joaquim	11
1819	2016-02-18 00:00:00	Dom Silvério	11
1820	2016-02-18 00:00:00	Dom Viçoso	11
1821	2016-02-18 00:00:00	Dona Eusébia	11
1822	2016-02-18 00:00:00	Dores de Campos	11
1823	2016-02-18 00:00:00	Dores de Guanhães	11
1824	2016-02-18 00:00:00	Dores do Indaiá	11
1825	2016-02-18 00:00:00	Dores do Turvo	11
1826	2016-02-18 00:00:00	Doresópolis	11
1827	2016-02-18 00:00:00	Douradoquara	11
1828	2016-02-18 00:00:00	Durandé	11
1829	2016-02-18 00:00:00	Elói Mendes	11
1830	2016-02-18 00:00:00	Engenheiro Caldas	11
1831	2016-02-18 00:00:00	Engenheiro Navarro	11
1832	2016-02-18 00:00:00	Entre Folhas	11
1833	2016-02-18 00:00:00	Entre Rios de Minas	11
1834	2016-02-18 00:00:00	Ervália	11
1835	2016-02-18 00:00:00	Esmeraldas	11
1836	2016-02-18 00:00:00	Espera Feliz	11
1837	2016-02-18 00:00:00	Espinosa	11
1838	2016-02-18 00:00:00	Espírito Santo do Dourado	11
1839	2016-02-18 00:00:00	Estiva	11
1840	2016-02-18 00:00:00	Estrela Dalva	11
1841	2016-02-18 00:00:00	Estrela do Indaiá	11
1842	2016-02-18 00:00:00	Estrela do Sul	11
1843	2016-02-18 00:00:00	Eugenópolis	11
1844	2016-02-18 00:00:00	Ewbank da Câmara	11
1845	2016-02-18 00:00:00	Extrema	11
1846	2016-02-18 00:00:00	Fama	11
1847	2016-02-18 00:00:00	Faria Lemos	11
1848	2016-02-18 00:00:00	Felício dos Santos	11
1849	2016-02-18 00:00:00	Felisburgo	11
1850	2016-02-18 00:00:00	Felixlândia	11
1851	2016-02-18 00:00:00	Fernandes Tourinho	11
1852	2016-02-18 00:00:00	Ferros	11
1853	2016-02-18 00:00:00	Fervedouro	11
1854	2016-02-18 00:00:00	Florestal	11
1855	2016-02-18 00:00:00	Formiga	11
1856	2016-02-18 00:00:00	Formoso	11
1857	2016-02-18 00:00:00	Fortaleza de Minas	11
1858	2016-02-18 00:00:00	Fortuna de Minas	11
1859	2016-02-18 00:00:00	Francisco Badaró	11
1860	2016-02-18 00:00:00	Francisco Dumont	11
1861	2016-02-18 00:00:00	Francisco Sá	11
1862	2016-02-18 00:00:00	Franciscópolis	11
1863	2016-02-18 00:00:00	Frei Gaspar	11
1864	2016-02-18 00:00:00	Frei Inocêncio	11
1865	2016-02-18 00:00:00	Frei Lagonegro	11
1866	2016-02-18 00:00:00	Fronteira	11
1867	2016-02-18 00:00:00	Fronteira dos Vales	11
1868	2016-02-18 00:00:00	Fruta de Leite	11
1869	2016-02-18 00:00:00	Frutal	11
1870	2016-02-18 00:00:00	Funilândia	11
1871	2016-02-18 00:00:00	Galiléia	11
1872	2016-02-18 00:00:00	Gameleiras	11
1873	2016-02-18 00:00:00	Glaucilândia	11
1874	2016-02-18 00:00:00	Goiabeira	11
1875	2016-02-18 00:00:00	Goianá	11
1876	2016-02-18 00:00:00	Gonçalves	11
1877	2016-02-18 00:00:00	Gonzaga	11
1878	2016-02-18 00:00:00	Gouveia	11
1879	2016-02-18 00:00:00	Governador Valadares	11
1880	2016-02-18 00:00:00	Grão Mogol	11
1881	2016-02-18 00:00:00	Grupiara	11
1882	2016-02-18 00:00:00	Guanhães	11
1883	2016-02-18 00:00:00	Guapé	11
1884	2016-02-18 00:00:00	Guaraciaba	11
1885	2016-02-18 00:00:00	Guaraciama	11
1886	2016-02-18 00:00:00	Guaranésia	11
1887	2016-02-18 00:00:00	Guarani	11
1888	2016-02-18 00:00:00	Guarará	11
1889	2016-02-18 00:00:00	Guarda-Mor	11
1890	2016-02-18 00:00:00	Guaxupé	11
1891	2016-02-18 00:00:00	Guidoval	11
1892	2016-02-18 00:00:00	Guimarânia	11
1893	2016-02-18 00:00:00	Guiricema	11
1894	2016-02-18 00:00:00	Gurinhatã	11
1895	2016-02-18 00:00:00	Heliodora	11
1896	2016-02-18 00:00:00	Iapu	11
1897	2016-02-18 00:00:00	Ibertioga	11
1898	2016-02-18 00:00:00	Ibiá	11
1899	2016-02-18 00:00:00	Ibiaí	11
1900	2016-02-18 00:00:00	Ibiracatu	11
1901	2016-02-18 00:00:00	Ibiraci	11
1902	2016-02-18 00:00:00	Ibirité	11
1903	2016-02-18 00:00:00	Ibitiúra de Minas	11
1904	2016-02-18 00:00:00	Ibituruna	11
1905	2016-02-18 00:00:00	Icaraí de Minas	11
1906	2016-02-18 00:00:00	Igarapé	11
1907	2016-02-18 00:00:00	Igaratinga	11
1908	2016-02-18 00:00:00	Iguatama	11
1909	2016-02-18 00:00:00	Ijaci	11
1910	2016-02-18 00:00:00	Ilicínea	11
1911	2016-02-18 00:00:00	Imbé de Minas	11
1912	2016-02-18 00:00:00	Inconfidentes	11
1913	2016-02-18 00:00:00	Indaiabira	11
1914	2016-02-18 00:00:00	Indianópolis	11
1915	2016-02-18 00:00:00	Ingaí	11
1916	2016-02-18 00:00:00	Inhapim	11
1917	2016-02-18 00:00:00	Inhaúma	11
1918	2016-02-18 00:00:00	Inimutaba	11
1919	2016-02-18 00:00:00	Ipaba	11
1920	2016-02-18 00:00:00	Ipanema	11
1921	2016-02-18 00:00:00	Ipatinga	11
1922	2016-02-18 00:00:00	Ipiaçu	11
1923	2016-02-18 00:00:00	Ipuiúna	11
1924	2016-02-18 00:00:00	Iraí de Minas	11
1925	2016-02-18 00:00:00	Itabira	11
1926	2016-02-18 00:00:00	Itabirinha	11
1927	2016-02-18 00:00:00	Itabirito	11
1928	2016-02-18 00:00:00	Itacambira	11
1929	2016-02-18 00:00:00	Itacarambi	11
1930	2016-02-18 00:00:00	Itaguara	11
1931	2016-02-18 00:00:00	Itaipé	11
1932	2016-02-18 00:00:00	Itajubá	11
1933	2016-02-18 00:00:00	Itamarandiba	11
1934	2016-02-18 00:00:00	Itamarati de Minas	11
1935	2016-02-18 00:00:00	Itambacuri	11
1936	2016-02-18 00:00:00	Itambé do Mato Dentro	11
1937	2016-02-18 00:00:00	Itamogi	11
1938	2016-02-18 00:00:00	Itamonte	11
1939	2016-02-18 00:00:00	Itanhandu	11
1940	2016-02-18 00:00:00	Itanhomi	11
1941	2016-02-18 00:00:00	Itaobim	11
1942	2016-02-18 00:00:00	Itapagipe	11
1943	2016-02-18 00:00:00	Itapecerica	11
1944	2016-02-18 00:00:00	Itapeva	11
1945	2016-02-18 00:00:00	Itatiaiuçu	11
1946	2016-02-18 00:00:00	Itaú de Minas	11
1947	2016-02-18 00:00:00	Itaúna	11
1948	2016-02-18 00:00:00	Itaverava	11
1949	2016-02-18 00:00:00	Itinga	11
1950	2016-02-18 00:00:00	Itueta	11
1951	2016-02-18 00:00:00	Ituiutaba	11
1952	2016-02-18 00:00:00	Itumirim	11
1953	2016-02-18 00:00:00	Iturama	11
1954	2016-02-18 00:00:00	Itutinga	11
1955	2016-02-18 00:00:00	Jaboticatubas	11
1956	2016-02-18 00:00:00	Jacinto	11
1957	2016-02-18 00:00:00	Jacuí	11
1958	2016-02-18 00:00:00	Jacutinga	11
1959	2016-02-18 00:00:00	Jaguaraçu	11
1960	2016-02-18 00:00:00	Jaíba	11
1961	2016-02-18 00:00:00	Jampruca	11
1962	2016-02-18 00:00:00	Janaúba	11
1963	2016-02-18 00:00:00	Januária	11
1964	2016-02-18 00:00:00	Japaraíba	11
1965	2016-02-18 00:00:00	Japonvar	11
1966	2016-02-18 00:00:00	Jeceaba	11
1967	2016-02-18 00:00:00	Jenipapo de Minas	11
1968	2016-02-18 00:00:00	Jequeri	11
1969	2016-02-18 00:00:00	Jequitaí	11
1970	2016-02-18 00:00:00	Jequitibá	11
1971	2016-02-18 00:00:00	Jequitinhonha	11
1972	2016-02-18 00:00:00	Jesuânia	11
1973	2016-02-18 00:00:00	Joaíma	11
1974	2016-02-18 00:00:00	Joanésia	11
1975	2016-02-18 00:00:00	João Monlevade	11
1976	2016-02-18 00:00:00	João Pinheiro	11
1977	2016-02-18 00:00:00	Joaquim Felício	11
1978	2016-02-18 00:00:00	Jordânia	11
1979	2016-02-18 00:00:00	José Gonçalves de Minas	11
1980	2016-02-18 00:00:00	José Raydan	11
1981	2016-02-18 00:00:00	Josenópolis	11
1982	2016-02-18 00:00:00	Juatuba	11
1983	2016-02-18 00:00:00	Juiz de Fora	11
1984	2016-02-18 00:00:00	Juramento	11
1985	2016-02-18 00:00:00	Juruaia	11
1986	2016-02-18 00:00:00	Juvenília	11
1987	2016-02-18 00:00:00	Ladainha	11
1988	2016-02-18 00:00:00	Lagamar	11
1989	2016-02-18 00:00:00	Lagoa da Prata	11
1990	2016-02-18 00:00:00	Lagoa dos Patos	11
1991	2016-02-18 00:00:00	Lagoa Dourada	11
1992	2016-02-18 00:00:00	Lagoa Formosa	11
1993	2016-02-18 00:00:00	Lagoa Grande	11
1994	2016-02-18 00:00:00	Lagoa Santa	11
1995	2016-02-18 00:00:00	Lajinha	11
1996	2016-02-18 00:00:00	Lambari	11
1997	2016-02-18 00:00:00	Lamim	11
1998	2016-02-18 00:00:00	Laranjal	11
1999	2016-02-18 00:00:00	Lassance	11
2000	2016-02-18 00:00:00	Lavras	11
2001	2016-02-18 00:00:00	Leandro Ferreira	11
2002	2016-02-18 00:00:00	Leme do Prado	11
2003	2016-02-18 00:00:00	Leopoldina	11
2004	2016-02-18 00:00:00	Liberdade	11
2005	2016-02-18 00:00:00	Lima Duarte	11
2006	2016-02-18 00:00:00	Limeira do Oeste	11
2007	2016-02-18 00:00:00	Lontra	11
2008	2016-02-18 00:00:00	Luisburgo	11
2009	2016-02-18 00:00:00	Luislândia	11
2010	2016-02-18 00:00:00	Luminárias	11
2011	2016-02-18 00:00:00	Luz	11
2012	2016-02-18 00:00:00	Machacalis	11
2013	2016-02-18 00:00:00	Machado	11
2014	2016-02-18 00:00:00	Madre de Deus de Minas	11
2015	2016-02-18 00:00:00	Malacacheta	11
2016	2016-02-18 00:00:00	Mamonas	11
2017	2016-02-18 00:00:00	Manga	11
2018	2016-02-18 00:00:00	Manhuaçu	11
2019	2016-02-18 00:00:00	Manhumirim	11
2020	2016-02-18 00:00:00	Mantena	11
2021	2016-02-18 00:00:00	Mar de Espanha	11
2022	2016-02-18 00:00:00	Maravilhas	11
2023	2016-02-18 00:00:00	Maria da Fé	11
2024	2016-02-18 00:00:00	Mariana	11
2025	2016-02-18 00:00:00	Marilac	11
2026	2016-02-18 00:00:00	Mário Campos	11
2027	2016-02-18 00:00:00	Maripá de Minas	11
2028	2016-02-18 00:00:00	Marliéria	11
2029	2016-02-18 00:00:00	Marmelópolis	11
2030	2016-02-18 00:00:00	Martinho Campos	11
2031	2016-02-18 00:00:00	Martins Soares	11
2032	2016-02-18 00:00:00	Mata Verde	11
2033	2016-02-18 00:00:00	Materlândia	11
2034	2016-02-18 00:00:00	Mateus Leme	11
2035	2016-02-18 00:00:00	Mathias Lobato	11
2036	2016-02-18 00:00:00	Matias Barbosa	11
2037	2016-02-18 00:00:00	Matias Cardoso	11
2038	2016-02-18 00:00:00	Matipó	11
2039	2016-02-18 00:00:00	Mato Verde	11
2040	2016-02-18 00:00:00	Matozinhos	11
2041	2016-02-18 00:00:00	Matutina	11
2042	2016-02-18 00:00:00	Medeiros	11
2043	2016-02-18 00:00:00	Medina	11
2044	2016-02-18 00:00:00	Mendes Pimentel	11
2045	2016-02-18 00:00:00	Mercês	11
2046	2016-02-18 00:00:00	Mesquita	11
2047	2016-02-18 00:00:00	Minas Novas	11
2048	2016-02-18 00:00:00	Minduri	11
2049	2016-02-18 00:00:00	Mirabela	11
2050	2016-02-18 00:00:00	Miradouro	11
2051	2016-02-18 00:00:00	Miraí	11
2052	2016-02-18 00:00:00	Miravânia	11
2053	2016-02-18 00:00:00	Moeda	11
2054	2016-02-18 00:00:00	Moema	11
2055	2016-02-18 00:00:00	Monjolos	11
2056	2016-02-18 00:00:00	Monsenhor Paulo	11
2057	2016-02-18 00:00:00	Montalvânia	11
2058	2016-02-18 00:00:00	Monte Alegre de Minas	11
2059	2016-02-18 00:00:00	Monte Azul	11
2060	2016-02-18 00:00:00	Monte Belo	11
2061	2016-02-18 00:00:00	Monte Carmelo	11
2062	2016-02-18 00:00:00	Monte Formoso	11
2063	2016-02-18 00:00:00	Monte Santo de Minas	11
2064	2016-02-18 00:00:00	Monte Sião	11
2065	2016-02-18 00:00:00	Montes Claros	11
2066	2016-02-18 00:00:00	Montezuma	11
2067	2016-02-18 00:00:00	Morada Nova de Minas	11
2068	2016-02-18 00:00:00	Morro da Garça	11
2069	2016-02-18 00:00:00	Morro do Pilar	11
2070	2016-02-18 00:00:00	Munhoz	11
2071	2016-02-18 00:00:00	Muriaé	11
2072	2016-02-18 00:00:00	Mutum	11
2073	2016-02-18 00:00:00	Muzambinho	11
2074	2016-02-18 00:00:00	Nacip Raydan	11
2075	2016-02-18 00:00:00	Nanuque	11
2076	2016-02-18 00:00:00	Naque	11
2077	2016-02-18 00:00:00	Natalândia	11
2078	2016-02-18 00:00:00	Natércia	11
2079	2016-02-18 00:00:00	Nazareno	11
2080	2016-02-18 00:00:00	Nepomuceno	11
2081	2016-02-18 00:00:00	Ninheira	11
2082	2016-02-18 00:00:00	Nova Belém	11
2083	2016-02-18 00:00:00	Nova Era	11
2084	2016-02-18 00:00:00	Nova Lima	11
2085	2016-02-18 00:00:00	Nova Módica	11
2086	2016-02-18 00:00:00	Nova Ponte	11
2087	2016-02-18 00:00:00	Nova Porteirinha	11
2088	2016-02-18 00:00:00	Nova Resende	11
2089	2016-02-18 00:00:00	Nova Serrana	11
2090	2016-02-18 00:00:00	Nova União	11
2091	2016-02-18 00:00:00	Novo Cruzeiro	11
2092	2016-02-18 00:00:00	Novo Oriente de Minas	11
2093	2016-02-18 00:00:00	Novorizonte	11
2094	2016-02-18 00:00:00	Olaria	11
2095	2016-02-18 00:00:00	Olhos-d'Água	11
2096	2016-02-18 00:00:00	Olímpio Noronha	11
2097	2016-02-18 00:00:00	Oliveira	11
2098	2016-02-18 00:00:00	Oliveira Fortes	11
2099	2016-02-18 00:00:00	Onça de Pitangui	11
2100	2016-02-18 00:00:00	Oratórios	11
2101	2016-02-18 00:00:00	Orizânia	11
2102	2016-02-18 00:00:00	Ouro Branco	11
2103	2016-02-18 00:00:00	Ouro Fino	11
2104	2016-02-18 00:00:00	Ouro Preto	11
2105	2016-02-18 00:00:00	Ouro Verde de Minas	11
2106	2016-02-18 00:00:00	Padre Carvalho	11
2107	2016-02-18 00:00:00	Padre Paraíso	11
2108	2016-02-18 00:00:00	Pai Pedro	11
2109	2016-02-18 00:00:00	Paineiras	11
2110	2016-02-18 00:00:00	Pains	11
2111	2016-02-18 00:00:00	Paiva	11
2112	2016-02-18 00:00:00	Palma	11
2113	2016-02-18 00:00:00	Palmópolis	11
2114	2016-02-18 00:00:00	Papagaios	11
2115	2016-02-18 00:00:00	Pará de Minas	11
2116	2016-02-18 00:00:00	Paracatu	11
2117	2016-02-18 00:00:00	Paraguaçu	11
2118	2016-02-18 00:00:00	Paraisópolis	11
2119	2016-02-18 00:00:00	Paraopeba	11
2120	2016-02-18 00:00:00	Passa Quatro	11
2121	2016-02-18 00:00:00	Passa Tempo	11
2122	2016-02-18 00:00:00	Passa-Vinte	11
2123	2016-02-18 00:00:00	Passabém	11
2124	2016-02-18 00:00:00	Passos	11
2125	2016-02-18 00:00:00	Patis	11
2126	2016-02-18 00:00:00	Patos de Minas	11
2127	2016-02-18 00:00:00	Patrocínio	11
2128	2016-02-18 00:00:00	Patrocínio do Muriaé	11
2129	2016-02-18 00:00:00	Paula Cândido	11
2130	2016-02-18 00:00:00	Paulistas	11
2131	2016-02-18 00:00:00	Pavão	11
2132	2016-02-18 00:00:00	Peçanha	11
2133	2016-02-18 00:00:00	Pedra Azul	11
2134	2016-02-18 00:00:00	Pedra Bonita	11
2135	2016-02-18 00:00:00	Pedra do Anta	11
2136	2016-02-18 00:00:00	Pedra do Indaiá	11
2137	2016-02-18 00:00:00	Pedra Dourada	11
2138	2016-02-18 00:00:00	Pedralva	11
2139	2016-02-18 00:00:00	Pedras de Maria da Cruz	11
2140	2016-02-18 00:00:00	Pedrinópolis	11
2141	2016-02-18 00:00:00	Pedro Leopoldo	11
2142	2016-02-18 00:00:00	Pedro Teixeira	11
2143	2016-02-18 00:00:00	Pequeri	11
2144	2016-02-18 00:00:00	Pequi	11
2145	2016-02-18 00:00:00	Perdigão	11
2146	2016-02-18 00:00:00	Perdizes	11
2147	2016-02-18 00:00:00	Perdões	11
2148	2016-02-18 00:00:00	Periquito	11
2149	2016-02-18 00:00:00	Pescador	11
2150	2016-02-18 00:00:00	Piau	11
2151	2016-02-18 00:00:00	Piedade de Caratinga	11
2152	2016-02-18 00:00:00	Piedade de Ponte Nova	11
2153	2016-02-18 00:00:00	Piedade do Rio Grande	11
2154	2016-02-18 00:00:00	Piedade dos Gerais	11
2155	2016-02-18 00:00:00	Pimenta	11
2156	2016-02-18 00:00:00	Pingo-d'Água	11
2157	2016-02-18 00:00:00	Pintópolis	11
2158	2016-02-18 00:00:00	Piracema	11
2159	2016-02-18 00:00:00	Pirajuba	11
2160	2016-02-18 00:00:00	Piranga	11
2161	2016-02-18 00:00:00	Piranguçu	11
2162	2016-02-18 00:00:00	Piranguinho	11
2163	2016-02-18 00:00:00	Pirapetinga	11
2164	2016-02-18 00:00:00	Pirapora	11
2165	2016-02-18 00:00:00	Piraúba	11
2166	2016-02-18 00:00:00	Pitangui	11
2167	2016-02-18 00:00:00	Piumhi	11
2168	2016-02-18 00:00:00	Planura	11
2169	2016-02-18 00:00:00	Poço Fundo	11
2170	2016-02-18 00:00:00	Poços de Caldas	11
2171	2016-02-18 00:00:00	Pocrane	11
2172	2016-02-18 00:00:00	Pompéu	11
2173	2016-02-18 00:00:00	Ponte Nova	11
2174	2016-02-18 00:00:00	Ponto Chique	11
2175	2016-02-18 00:00:00	Ponto dos Volantes	11
2176	2016-02-18 00:00:00	Porteirinha	11
2177	2016-02-18 00:00:00	Porto Firme	11
2178	2016-02-18 00:00:00	Poté	11
2179	2016-02-18 00:00:00	Pouso Alegre	11
2180	2016-02-18 00:00:00	Pouso Alto	11
2181	2016-02-18 00:00:00	Prados	11
2182	2016-02-18 00:00:00	Prata	11
2183	2016-02-18 00:00:00	Pratápolis	11
2184	2016-02-18 00:00:00	Pratinha	11
2185	2016-02-18 00:00:00	Presidente Bernardes	11
2186	2016-02-18 00:00:00	Presidente Juscelino	11
2187	2016-02-18 00:00:00	Presidente Kubitschek	11
2188	2016-02-18 00:00:00	Presidente Olegário	11
2189	2016-02-18 00:00:00	Prudente de Morais	11
2190	2016-02-18 00:00:00	Quartel Geral	11
2191	2016-02-18 00:00:00	Queluzito	11
2192	2016-02-18 00:00:00	Raposos	11
2193	2016-02-18 00:00:00	Raul Soares	11
2194	2016-02-18 00:00:00	Recreio	11
2195	2016-02-18 00:00:00	Reduto	11
2196	2016-02-18 00:00:00	Resende Costa	11
2197	2016-02-18 00:00:00	Resplendor	11
2198	2016-02-18 00:00:00	Ressaquinha	11
2199	2016-02-18 00:00:00	Riachinho	11
2200	2016-02-18 00:00:00	Riacho dos Machados	11
2201	2016-02-18 00:00:00	Ribeirão das Neves	11
2202	2016-02-18 00:00:00	Ribeirão Vermelho	11
2203	2016-02-18 00:00:00	Rio Acima	11
2204	2016-02-18 00:00:00	Rio Casca	11
2205	2016-02-18 00:00:00	Rio do Prado	11
2206	2016-02-18 00:00:00	Rio Doce	11
2207	2016-02-18 00:00:00	Rio Espera	11
2208	2016-02-18 00:00:00	Rio Manso	11
2209	2016-02-18 00:00:00	Rio Novo	11
2210	2016-02-18 00:00:00	Rio Paranaíba	11
2211	2016-02-18 00:00:00	Rio Pardo de Minas	11
2212	2016-02-18 00:00:00	Rio Piracicaba	11
2213	2016-02-18 00:00:00	Rio Pomba	11
2214	2016-02-18 00:00:00	Rio Preto	11
2215	2016-02-18 00:00:00	Rio Vermelho	11
2216	2016-02-18 00:00:00	Ritápolis	11
2217	2016-02-18 00:00:00	Rochedo de Minas	11
2218	2016-02-18 00:00:00	Rodeiro	11
2219	2016-02-18 00:00:00	Romaria	11
2220	2016-02-18 00:00:00	Rosário da Limeira	11
2221	2016-02-18 00:00:00	Rubelita	11
2222	2016-02-18 00:00:00	Rubim	11
2223	2016-02-18 00:00:00	Sabará	11
2224	2016-02-18 00:00:00	Sabinópolis	11
2225	2016-02-18 00:00:00	Sacramento	11
2226	2016-02-18 00:00:00	Salinas	11
2227	2016-02-18 00:00:00	Salto da Divisa	11
2228	2016-02-18 00:00:00	Santa Bárbara	11
2229	2016-02-18 00:00:00	Santa Bárbara do Leste	11
2230	2016-02-18 00:00:00	Santa Bárbara do Monte Verde	11
2231	2016-02-18 00:00:00	Santa Bárbara do Tugúrio	11
2232	2016-02-18 00:00:00	Santa Cruz de Minas	11
2233	2016-02-18 00:00:00	Santa Cruz de Salinas	11
2234	2016-02-18 00:00:00	Santa Cruz do Escalvado	11
2235	2016-02-18 00:00:00	Santa Efigênia de Minas	11
2236	2016-02-18 00:00:00	Santa Fé de Minas	11
2237	2016-02-18 00:00:00	Santa Helena de Minas	11
2238	2016-02-18 00:00:00	Santa Juliana	11
2239	2016-02-18 00:00:00	Santa Luzia	11
2240	2016-02-18 00:00:00	Santa Margarida	11
2241	2016-02-18 00:00:00	Santa Maria de Itabira	11
2242	2016-02-18 00:00:00	Santa Maria do Salto	11
2243	2016-02-18 00:00:00	Santa Maria do Suaçuí	11
2244	2016-02-18 00:00:00	Santa Rita de Caldas	11
2245	2016-02-18 00:00:00	Santa Rita de Ibitipoca	11
2246	2016-02-18 00:00:00	Santa Rita de Jacutinga	11
2247	2016-02-18 00:00:00	Santa Rita de Minas	11
2248	2016-02-18 00:00:00	Santa Rita do Itueto	11
2249	2016-02-18 00:00:00	Santa Rita do Sapucaí	11
2250	2016-02-18 00:00:00	Santa Rosa da Serra	11
2251	2016-02-18 00:00:00	Santa Vitória	11
2252	2016-02-18 00:00:00	Santana da Vargem	11
2253	2016-02-18 00:00:00	Santana de Cataguases	11
2254	2016-02-18 00:00:00	Santana de Pirapama	11
2255	2016-02-18 00:00:00	Santana do Deserto	11
2256	2016-02-18 00:00:00	Santana do Garambéu	11
2257	2016-02-18 00:00:00	Santana do Jacaré	11
2258	2016-02-18 00:00:00	Santana do Manhuaçu	11
2259	2016-02-18 00:00:00	Santana do Paraíso	11
2260	2016-02-18 00:00:00	Santana do Riacho	11
2261	2016-02-18 00:00:00	Santana dos Montes	11
2262	2016-02-18 00:00:00	Santo Antônio do Amparo	11
2263	2016-02-18 00:00:00	Santo Antônio do Aventureiro	11
2264	2016-02-18 00:00:00	Santo Antônio do Grama	11
2265	2016-02-18 00:00:00	Santo Antônio do Itambé	11
2266	2016-02-18 00:00:00	Santo Antônio do Jacinto	11
2267	2016-02-18 00:00:00	Santo Antônio do Monte	11
2268	2016-02-18 00:00:00	Santo Antônio do Retiro	11
2269	2016-02-18 00:00:00	Santo Antônio do Rio Abaixo	11
2270	2016-02-18 00:00:00	Santo Hipólito	11
2271	2016-02-18 00:00:00	Santos Dumont	11
2272	2016-02-18 00:00:00	São Bento Abade	11
2273	2016-02-18 00:00:00	São Brás do Suaçuí	11
2274	2016-02-18 00:00:00	São Domingos das Dores	11
2275	2016-02-18 00:00:00	São Domingos do Prata	11
2276	2016-02-18 00:00:00	São Félix de Minas	11
2277	2016-02-18 00:00:00	São Francisco	11
2278	2016-02-18 00:00:00	São Francisco de Paula	11
2279	2016-02-18 00:00:00	São Francisco de Sales	11
2280	2016-02-18 00:00:00	São Francisco do Glória	11
2281	2016-02-18 00:00:00	São Geraldo	11
2282	2016-02-18 00:00:00	São Geraldo da Piedade	11
2283	2016-02-18 00:00:00	São Geraldo do Baixio	11
2284	2016-02-18 00:00:00	São Gonçalo do Abaeté	11
2285	2016-02-18 00:00:00	São Gonçalo do Pará	11
2286	2016-02-18 00:00:00	São Gonçalo do Rio Abaixo	11
2287	2016-02-18 00:00:00	São Gonçalo do Rio Preto	11
2288	2016-02-18 00:00:00	São Gonçalo do Sapucaí	11
2289	2016-02-18 00:00:00	São Gotardo	11
2290	2016-02-18 00:00:00	São João Batista do Glória	11
2291	2016-02-18 00:00:00	São João da Lagoa	11
2292	2016-02-18 00:00:00	São João da Mata	11
2293	2016-02-18 00:00:00	São João da Ponte	11
2294	2016-02-18 00:00:00	São João das Missões	11
2295	2016-02-18 00:00:00	São João del Rei	11
2296	2016-02-18 00:00:00	São João do Manhuaçu	11
2297	2016-02-18 00:00:00	São João do Manteninha	11
2298	2016-02-18 00:00:00	São João do Oriente	11
2299	2016-02-18 00:00:00	São João do Pacuí	11
2300	2016-02-18 00:00:00	São João do Paraíso	11
2301	2016-02-18 00:00:00	São João Evangelista	11
2302	2016-02-18 00:00:00	São João Nepomuceno	11
2303	2016-02-18 00:00:00	São Joaquim de Bicas	11
2304	2016-02-18 00:00:00	São José da Barra	11
2305	2016-02-18 00:00:00	São José da Lapa	11
2306	2016-02-18 00:00:00	São José da Safira	11
2307	2016-02-18 00:00:00	São José da Varginha	11
2308	2016-02-18 00:00:00	São José do Alegre	11
2309	2016-02-18 00:00:00	São José do Divino	11
2310	2016-02-18 00:00:00	São José do Goiabal	11
2311	2016-02-18 00:00:00	São José do Jacuri	11
2312	2016-02-18 00:00:00	São José do Mantimento	11
2313	2016-02-18 00:00:00	São Lourenço	11
2314	2016-02-18 00:00:00	São Miguel do Anta	11
2315	2016-02-18 00:00:00	São Pedro da União	11
2316	2016-02-18 00:00:00	São Pedro do Suaçuí	11
2317	2016-02-18 00:00:00	São Pedro dos Ferros	11
2318	2016-02-18 00:00:00	São Romão	11
2319	2016-02-18 00:00:00	São Roque de Minas	11
2320	2016-02-18 00:00:00	São Sebastião da Bela Vista	11
2321	2016-02-18 00:00:00	São Sebastião da Vargem Alegre	11
2322	2016-02-18 00:00:00	São Sebastião do Anta	11
2323	2016-02-18 00:00:00	São Sebastião do Maranhão	11
2324	2016-02-18 00:00:00	São Sebastião do Oeste	11
2325	2016-02-18 00:00:00	São Sebastião do Paraíso	11
2326	2016-02-18 00:00:00	São Sebastião do Rio Preto	11
2327	2016-02-18 00:00:00	São Sebastião do Rio Verde	11
2328	2016-02-18 00:00:00	São Thomé das Letras	11
2329	2016-02-18 00:00:00	São Tiago	11
2330	2016-02-18 00:00:00	São Tomás de Aquino	11
2331	2016-02-18 00:00:00	São Vicente de Minas	11
2332	2016-02-18 00:00:00	Sapucaí-Mirim	11
2333	2016-02-18 00:00:00	Sardoá	11
2334	2016-02-18 00:00:00	Sarzedo	11
2335	2016-02-18 00:00:00	Sem-Peixe	11
2336	2016-02-18 00:00:00	Senador Amaral	11
2337	2016-02-18 00:00:00	Senador Cortes	11
2338	2016-02-18 00:00:00	Senador Firmino	11
2339	2016-02-18 00:00:00	Senador José Bento	11
2340	2016-02-18 00:00:00	Senador Modestino Gonçalves	11
2341	2016-02-18 00:00:00	Senhora de Oliveira	11
2342	2016-02-18 00:00:00	Senhora do Porto	11
2343	2016-02-18 00:00:00	Senhora dos Remédios	11
2344	2016-02-18 00:00:00	Sericita	11
2345	2016-02-18 00:00:00	Seritinga	11
2346	2016-02-18 00:00:00	Serra Azul de Minas	11
2347	2016-02-18 00:00:00	Serra da Saudade	11
2348	2016-02-18 00:00:00	Serra do Salitre	11
2349	2016-02-18 00:00:00	Serra dos Aimorés	11
2350	2016-02-18 00:00:00	Serrania	11
2351	2016-02-18 00:00:00	Serranópolis de Minas	11
2352	2016-02-18 00:00:00	Serranos	11
2353	2016-02-18 00:00:00	Serro	11
2354	2016-02-18 00:00:00	Sete Lagoas	11
2355	2016-02-18 00:00:00	Setubinha	11
2356	2016-02-18 00:00:00	Silveirânia	11
2357	2016-02-18 00:00:00	Silvianópolis	11
2358	2016-02-18 00:00:00	Simão Pereira	11
2359	2016-02-18 00:00:00	Simonésia	11
2360	2016-02-18 00:00:00	Sobrália	11
2361	2016-02-18 00:00:00	Soledade de Minas	11
2362	2016-02-18 00:00:00	Tabuleiro	11
2363	2016-02-18 00:00:00	Taiobeiras	11
2364	2016-02-18 00:00:00	Taparuba	11
2365	2016-02-18 00:00:00	Tapira	11
2366	2016-02-18 00:00:00	Tapiraí	11
2367	2016-02-18 00:00:00	Taquaraçu de Minas	11
2368	2016-02-18 00:00:00	Tarumirim	11
2369	2016-02-18 00:00:00	Teixeiras	11
2370	2016-02-18 00:00:00	Teófilo Otoni	11
2371	2016-02-18 00:00:00	Timóteo	11
2372	2016-02-18 00:00:00	Tiradentes	11
2373	2016-02-18 00:00:00	Tiros	11
2374	2016-02-18 00:00:00	Tocantins	11
2375	2016-02-18 00:00:00	Tocos do Moji	11
2376	2016-02-18 00:00:00	Toledo	11
2377	2016-02-18 00:00:00	Tombos	11
2378	2016-02-18 00:00:00	Três Corações	11
2379	2016-02-18 00:00:00	Três Marias	11
2380	2016-02-18 00:00:00	Três Pontas	11
2381	2016-02-18 00:00:00	Tumiritinga	11
2382	2016-02-18 00:00:00	Tupaciguara	11
2383	2016-02-18 00:00:00	Turmalina	11
2384	2016-02-18 00:00:00	Turvolândia	11
2385	2016-02-18 00:00:00	Ubá	11
2386	2016-02-18 00:00:00	Ubaí	11
2387	2016-02-18 00:00:00	Ubaporanga	11
2388	2016-02-18 00:00:00	Uberaba	11
2389	2016-02-18 00:00:00	Uberlândia	11
2390	2016-02-18 00:00:00	Umburatiba	11
2391	2016-02-18 00:00:00	Unaí	11
2392	2016-02-18 00:00:00	União de Minas	11
2393	2016-02-18 00:00:00	Uruana de Minas	11
2394	2016-02-18 00:00:00	Urucânia	11
2395	2016-02-18 00:00:00	Urucuia	11
2396	2016-02-18 00:00:00	Vargem Alegre	11
2397	2016-02-18 00:00:00	Vargem Bonita	11
2398	2016-02-18 00:00:00	Vargem Grande do Rio Pardo	11
2399	2016-02-18 00:00:00	Varginha	11
2400	2016-02-18 00:00:00	Varjão de Minas	11
2401	2016-02-18 00:00:00	Várzea da Palma	11
2402	2016-02-18 00:00:00	Varzelândia	11
2403	2016-02-18 00:00:00	Vazante	11
2404	2016-02-18 00:00:00	Verdelândia	11
2405	2016-02-18 00:00:00	Veredinha	11
2406	2016-02-18 00:00:00	Veríssimo	11
2407	2016-02-18 00:00:00	Vermelho Novo	11
2408	2016-02-18 00:00:00	Vespasiano	11
2409	2016-02-18 00:00:00	Viçosa	11
2410	2016-02-18 00:00:00	Vieiras	11
2411	2016-02-18 00:00:00	Virgem da Lapa	11
2412	2016-02-18 00:00:00	Virgínia	11
2413	2016-02-18 00:00:00	Virginópolis	11
2414	2016-02-18 00:00:00	Virgolândia	11
2415	2016-02-18 00:00:00	Visconde do Rio Branco	11
2416	2016-02-18 00:00:00	Volta Grande	11
2417	2016-02-18 00:00:00	Wenceslau Braz	11
2418	2016-02-18 00:00:00	Abaetetuba	14
2419	2016-02-18 00:00:00	Abel Figueiredo	14
2420	2016-02-18 00:00:00	Acará	14
2421	2016-02-18 00:00:00	Afuá	14
2422	2016-02-18 00:00:00	Água Azul do Norte	14
2423	2016-02-18 00:00:00	Alenquer	14
2424	2016-02-18 00:00:00	Almeirim	14
2425	2016-02-18 00:00:00	Altamira	14
2426	2016-02-18 00:00:00	Anajás	14
2427	2016-02-18 00:00:00	Ananindeua	14
2428	2016-02-18 00:00:00	Anapu	14
2429	2016-02-18 00:00:00	Augusto Corrêa	14
2430	2016-02-18 00:00:00	Aurora do Pará	14
2431	2016-02-18 00:00:00	Aveiro	14
2432	2016-02-18 00:00:00	Bagre	14
2433	2016-02-18 00:00:00	Baião	14
2434	2016-02-18 00:00:00	Bannach	14
2435	2016-02-18 00:00:00	Barcarena	14
2436	2016-02-18 00:00:00	Belém	14
2437	2016-02-18 00:00:00	Belterra	14
2438	2016-02-18 00:00:00	Benevides	14
2439	2016-02-18 00:00:00	Bom Jesus do Tocantins	14
2440	2016-02-18 00:00:00	Bonito	14
2441	2016-02-18 00:00:00	Bragança	14
2442	2016-02-18 00:00:00	Brasil Novo	14
2443	2016-02-18 00:00:00	Brejo Grande do Araguaia	14
2444	2016-02-18 00:00:00	Breu Branco	14
2445	2016-02-18 00:00:00	Breves	14
2446	2016-02-18 00:00:00	Bujaru	14
2447	2016-02-18 00:00:00	Cachoeira do Arari	14
2448	2016-02-18 00:00:00	Cachoeira do Piriá	14
2449	2016-02-18 00:00:00	Cametá	14
2450	2016-02-18 00:00:00	Canaã dos Carajás	14
2451	2016-02-18 00:00:00	Capanema	14
2452	2016-02-18 00:00:00	Capitão Poço	14
2453	2016-02-18 00:00:00	Castanhal	14
2454	2016-02-18 00:00:00	Chaves	14
2455	2016-02-18 00:00:00	Colares	14
2456	2016-02-18 00:00:00	Conceição do Araguaia	14
2457	2016-02-18 00:00:00	Concórdia do Pará	14
2458	2016-02-18 00:00:00	Cumaru do Norte	14
2459	2016-02-18 00:00:00	Curionópolis	14
2460	2016-02-18 00:00:00	Curralinho	14
2461	2016-02-18 00:00:00	Curuá	14
2462	2016-02-18 00:00:00	Curuçá	14
2463	2016-02-18 00:00:00	Dom Eliseu	14
2464	2016-02-18 00:00:00	Eldorado dos Carajás	14
2465	2016-02-18 00:00:00	Faro	14
2466	2016-02-18 00:00:00	Floresta do Araguaia	14
2467	2016-02-18 00:00:00	Garrafão do Norte	14
2468	2016-02-18 00:00:00	Goianésia do Pará	14
2469	2016-02-18 00:00:00	Gurupá	14
2470	2016-02-18 00:00:00	Igarapé-Açu	14
2471	2016-02-18 00:00:00	Igarapé-Miri	14
2472	2016-02-18 00:00:00	Inhangapi	14
2473	2016-02-18 00:00:00	Ipixuna do Pará	14
2474	2016-02-18 00:00:00	Irituia	14
2475	2016-02-18 00:00:00	Itaituba	14
2476	2016-02-18 00:00:00	Itupiranga	14
2477	2016-02-18 00:00:00	Jacareacanga	14
2478	2016-02-18 00:00:00	Jacundá	14
2479	2016-02-18 00:00:00	Juruti	14
2480	2016-02-18 00:00:00	Limoeiro do Ajuru	14
2481	2016-02-18 00:00:00	Mãe do Rio	14
2482	2016-02-18 00:00:00	Magalhães Barata	14
2483	2016-02-18 00:00:00	Marabá	14
2484	2016-02-18 00:00:00	Maracanã	14
2485	2016-02-18 00:00:00	Marapanim	14
2486	2016-02-18 00:00:00	Marituba	14
2487	2016-02-18 00:00:00	Medicilândia	14
2488	2016-02-18 00:00:00	Melgaço	14
2489	2016-02-18 00:00:00	Mocajuba	14
2490	2016-02-18 00:00:00	Moju	14
2491	2016-02-18 00:00:00	Monte Alegre	14
2492	2016-02-18 00:00:00	Muaná	14
2493	2016-02-18 00:00:00	Nova Esperança do Piriá	14
2494	2016-02-18 00:00:00	Nova Ipixuna	14
2495	2016-02-18 00:00:00	Nova Timboteua	14
2496	2016-02-18 00:00:00	Novo Progresso	14
2497	2016-02-18 00:00:00	Novo Repartimento	14
2498	2016-02-18 00:00:00	Óbidos	14
2499	2016-02-18 00:00:00	Oeiras do Pará	14
2500	2016-02-18 00:00:00	Oriximiná	14
2501	2016-02-18 00:00:00	Ourém	14
2502	2016-02-18 00:00:00	Ourilândia do Norte	14
2503	2016-02-18 00:00:00	Pacajá	14
2504	2016-02-18 00:00:00	Palestina do Pará	14
2505	2016-02-18 00:00:00	Paragominas	14
2506	2016-02-18 00:00:00	Parauapebas	14
2507	2016-02-18 00:00:00	Pau D'Arco	14
2508	2016-02-18 00:00:00	Peixe-Boi	14
2509	2016-02-18 00:00:00	Piçarra	14
2510	2016-02-18 00:00:00	Placas	14
2511	2016-02-18 00:00:00	Ponta de Pedras	14
2512	2016-02-18 00:00:00	Portel	14
2513	2016-02-18 00:00:00	Porto de Moz	14
2514	2016-02-18 00:00:00	Prainha	14
2515	2016-02-18 00:00:00	Primavera	14
2516	2016-02-18 00:00:00	Quatipuru	14
2517	2016-02-18 00:00:00	Redenção	14
2518	2016-02-18 00:00:00	Rio Maria	14
2519	2016-02-18 00:00:00	Rondon do Pará	14
2520	2016-02-18 00:00:00	Rurópolis	14
2521	2016-02-18 00:00:00	Salinópolis	14
2522	2016-02-18 00:00:00	Salvaterra	14
2523	2016-02-18 00:00:00	Santa Bárbara do Pará	14
2524	2016-02-18 00:00:00	Santa Cruz do Arari	14
2525	2016-02-18 00:00:00	Santa Isabel do Pará	14
2526	2016-02-18 00:00:00	Santa Luzia do Pará	14
2527	2016-02-18 00:00:00	Santa Maria das Barreiras	14
2528	2016-02-18 00:00:00	Santa Maria do Pará	14
2529	2016-02-18 00:00:00	Santana do Araguaia	14
2530	2016-02-18 00:00:00	Santarém	14
2531	2016-02-18 00:00:00	Santarém Novo	14
2532	2016-02-18 00:00:00	Santo Antônio do Tauá	14
2533	2016-02-18 00:00:00	São Caetano de Odivelas	14
2534	2016-02-18 00:00:00	São Domingos do Araguaia	14
2535	2016-02-18 00:00:00	São Domingos do Capim	14
2536	2016-02-18 00:00:00	São Félix do Xingu	14
2537	2016-02-18 00:00:00	São Francisco do Pará	14
2538	2016-02-18 00:00:00	São Geraldo do Araguaia	14
2539	2016-02-18 00:00:00	São João da Ponta	14
2540	2016-02-18 00:00:00	São João de Pirabas	14
2541	2016-02-18 00:00:00	São João do Araguaia	14
2542	2016-02-18 00:00:00	São Miguel do Guamá	14
2543	2016-02-18 00:00:00	São Sebastião da Boa Vista	14
2544	2016-02-18 00:00:00	Sapucaia	14
2545	2016-02-18 00:00:00	Senador José Porfírio	14
2546	2016-02-18 00:00:00	Soure	14
2547	2016-02-18 00:00:00	Tailândia	14
2548	2016-02-18 00:00:00	Terra Alta	14
2549	2016-02-18 00:00:00	Terra Santa	14
2550	2016-02-18 00:00:00	Tomé-Açu	14
2551	2016-02-18 00:00:00	Tracuateua	14
2552	2016-02-18 00:00:00	Trairão	14
2553	2016-02-18 00:00:00	Tucumã	14
2554	2016-02-18 00:00:00	Tucuruí	14
2555	2016-02-18 00:00:00	Ulianópolis	14
2556	2016-02-18 00:00:00	Uruará	14
2557	2016-02-18 00:00:00	Vigia	14
2558	2016-02-18 00:00:00	Viseu	14
2559	2016-02-18 00:00:00	Vitória do Xingu	14
2560	2016-02-18 00:00:00	Xinguara	14
2561	2016-02-18 00:00:00	Água Branca	15
2562	2016-02-18 00:00:00	Aguiar	15
2563	2016-02-18 00:00:00	Alagoa Grande	15
2564	2016-02-18 00:00:00	Alagoa Nova	15
2565	2016-02-18 00:00:00	Alagoinha	15
2566	2016-02-18 00:00:00	Alcantil	15
2567	2016-02-18 00:00:00	Algodão de Jandaíra	15
2568	2016-02-18 00:00:00	Alhandra	15
2569	2016-02-18 00:00:00	Amparo	15
2570	2016-02-18 00:00:00	Aparecida	15
2571	2016-02-18 00:00:00	Araçagi	15
2572	2016-02-18 00:00:00	Arara	15
2573	2016-02-18 00:00:00	Araruna	15
2574	2016-02-18 00:00:00	Areia	15
2575	2016-02-18 00:00:00	Areia de Baraúnas	15
2576	2016-02-18 00:00:00	Areial	15
2577	2016-02-18 00:00:00	Aroeiras	15
2578	2016-02-18 00:00:00	Assunção	15
2579	2016-02-18 00:00:00	Baía da Traição	15
2580	2016-02-18 00:00:00	Bananeiras	15
2581	2016-02-18 00:00:00	Baraúna	15
2582	2016-02-18 00:00:00	Barra de Santa Rosa	15
2583	2016-02-18 00:00:00	Barra de Santana	15
2584	2016-02-18 00:00:00	Barra de São Miguel	15
2585	2016-02-18 00:00:00	Bayeux	15
2586	2016-02-18 00:00:00	Belém	15
2587	2016-02-18 00:00:00	Belém do Brejo do Cruz	15
2588	2016-02-18 00:00:00	Bernardino Batista	15
2589	2016-02-18 00:00:00	Boa Ventura	15
2590	2016-02-18 00:00:00	Boa Vista	15
2591	2016-02-18 00:00:00	Bom Jesus	15
2592	2016-02-18 00:00:00	Bom Sucesso	15
2593	2016-02-18 00:00:00	Bonito de Santa Fé	15
2594	2016-02-18 00:00:00	Boqueirão	15
2595	2016-02-18 00:00:00	Borborema	15
2596	2016-02-18 00:00:00	Brejo do Cruz	15
2597	2016-02-18 00:00:00	Brejo dos Santos	15
2598	2016-02-18 00:00:00	Caaporã	15
2599	2016-02-18 00:00:00	Cabaceiras	15
2600	2016-02-18 00:00:00	Cabedelo	15
2601	2016-02-18 00:00:00	Cachoeira dos Índios	15
2602	2016-02-18 00:00:00	Cacimba de Areia	15
2603	2016-02-18 00:00:00	Cacimba de Dentro	15
2604	2016-02-18 00:00:00	Cacimbas	15
2605	2016-02-18 00:00:00	Caiçara	15
2606	2016-02-18 00:00:00	Cajazeiras	15
2607	2016-02-18 00:00:00	Cajazeirinhas	15
2608	2016-02-18 00:00:00	Caldas Brandão	15
2609	2016-02-18 00:00:00	Camalaú	15
2610	2016-02-18 00:00:00	Campina Grande	15
2611	2016-02-18 00:00:00	Campo de Santana	15
2612	2016-02-18 00:00:00	Capim	15
2613	2016-02-18 00:00:00	Caraúbas	15
2614	2016-02-18 00:00:00	Carrapateira	15
2615	2016-02-18 00:00:00	Casserengue	15
2616	2016-02-18 00:00:00	Catingueira	15
2617	2016-02-18 00:00:00	Catolé do Rocha	15
2618	2016-02-18 00:00:00	Caturité	15
2619	2016-02-18 00:00:00	Conceição	15
2620	2016-02-18 00:00:00	Condado	15
2621	2016-02-18 00:00:00	Conde	15
2622	2016-02-18 00:00:00	Congo	15
2623	2016-02-18 00:00:00	Coremas	15
2624	2016-02-18 00:00:00	Coxixola	15
2625	2016-02-18 00:00:00	Cruz do Espírito Santo	15
2626	2016-02-18 00:00:00	Cubati	15
2627	2016-02-18 00:00:00	Cuité	15
2628	2016-02-18 00:00:00	Cuité de Mamanguape	15
2629	2016-02-18 00:00:00	Cuitegi	15
2630	2016-02-18 00:00:00	Curral de Cima	15
2631	2016-02-18 00:00:00	Curral Velho	15
2632	2016-02-18 00:00:00	Damião	15
2633	2016-02-18 00:00:00	Desterro	15
2634	2016-02-18 00:00:00	Diamante	15
2635	2016-02-18 00:00:00	Dona Inês	15
2636	2016-02-18 00:00:00	Duas Estradas	15
2637	2016-02-18 00:00:00	Emas	15
2638	2016-02-18 00:00:00	Esperança	15
2639	2016-02-18 00:00:00	Fagundes	15
2640	2016-02-18 00:00:00	Frei Martinho	15
2641	2016-02-18 00:00:00	Gado Bravo	15
2642	2016-02-18 00:00:00	Guarabira	15
2643	2016-02-18 00:00:00	Gurinhém	15
2644	2016-02-18 00:00:00	Gurjão	15
2645	2016-02-18 00:00:00	Ibiara	15
2646	2016-02-18 00:00:00	Igaracy	15
2647	2016-02-18 00:00:00	Imaculada	15
2648	2016-02-18 00:00:00	Ingá	15
2649	2016-02-18 00:00:00	Itabaiana	15
2650	2016-02-18 00:00:00	Itaporanga	15
2651	2016-02-18 00:00:00	Itapororoca	15
2652	2016-02-18 00:00:00	Itatuba	15
2653	2016-02-18 00:00:00	Jacaraú	15
2654	2016-02-18 00:00:00	Jericó	15
2655	2016-02-18 00:00:00	João Pessoa	15
2656	2016-02-18 00:00:00	Juarez Távora	15
2657	2016-02-18 00:00:00	Juazeirinho	15
2658	2016-02-18 00:00:00	Junco do Seridó	15
2659	2016-02-18 00:00:00	Juripiranga	15
2660	2016-02-18 00:00:00	Juru	15
2661	2016-02-18 00:00:00	Lagoa	15
2662	2016-02-18 00:00:00	Lagoa de Dentro	15
2663	2016-02-18 00:00:00	Lagoa Seca	15
2664	2016-02-18 00:00:00	Lastro	15
2665	2016-02-18 00:00:00	Livramento	15
2666	2016-02-18 00:00:00	Logradouro	15
2667	2016-02-18 00:00:00	Lucena	15
2668	2016-02-18 00:00:00	Mãe d'Água	15
2669	2016-02-18 00:00:00	Malta	15
2670	2016-02-18 00:00:00	Mamanguape	15
2671	2016-02-18 00:00:00	Manaíra	15
2672	2016-02-18 00:00:00	Marcação	15
2673	2016-02-18 00:00:00	Mari	15
2674	2016-02-18 00:00:00	Marizópolis	15
2675	2016-02-18 00:00:00	Massaranduba	15
2676	2016-02-18 00:00:00	Mataraca	15
2677	2016-02-18 00:00:00	Matinhas	15
2678	2016-02-18 00:00:00	Mato Grosso	15
2679	2016-02-18 00:00:00	Maturéia	15
2680	2016-02-18 00:00:00	Mogeiro	15
2681	2016-02-18 00:00:00	Montadas	15
2682	2016-02-18 00:00:00	Monte Horebe	15
2683	2016-02-18 00:00:00	Monteiro	15
2684	2016-02-18 00:00:00	Mulungu	15
2685	2016-02-18 00:00:00	Natuba	15
2686	2016-02-18 00:00:00	Nazarezinho	15
2687	2016-02-18 00:00:00	Nova Floresta	15
2688	2016-02-18 00:00:00	Nova Olinda	15
2689	2016-02-18 00:00:00	Nova Palmeira	15
2690	2016-02-18 00:00:00	Olho d'Água	15
2691	2016-02-18 00:00:00	Olivedos	15
2692	2016-02-18 00:00:00	Ouro Velho	15
2693	2016-02-18 00:00:00	Parari	15
2694	2016-02-18 00:00:00	Passagem	15
2695	2016-02-18 00:00:00	Patos	15
2696	2016-02-18 00:00:00	Paulista	15
2697	2016-02-18 00:00:00	Pedra Branca	15
2698	2016-02-18 00:00:00	Pedra Lavrada	15
2699	2016-02-18 00:00:00	Pedras de Fogo	15
2700	2016-02-18 00:00:00	Pedro Régis	15
2701	2016-02-18 00:00:00	Piancó	15
2702	2016-02-18 00:00:00	Picuí	15
2703	2016-02-18 00:00:00	Pilar	15
2704	2016-02-18 00:00:00	Pilões	15
2705	2016-02-18 00:00:00	Pilõezinhos	15
2706	2016-02-18 00:00:00	Pirpirituba	15
2707	2016-02-18 00:00:00	Pitimbu	15
2708	2016-02-18 00:00:00	Pocinhos	15
2709	2016-02-18 00:00:00	Poço Dantas	15
2710	2016-02-18 00:00:00	Poço de José de Moura	15
2711	2016-02-18 00:00:00	Pombal	15
2712	2016-02-18 00:00:00	Prata	15
2713	2016-02-18 00:00:00	Princesa Isabel	15
2714	2016-02-18 00:00:00	Puxinanã	15
2715	2016-02-18 00:00:00	Queimadas	15
2716	2016-02-18 00:00:00	Quixabá	15
2717	2016-02-18 00:00:00	Remígio	15
2718	2016-02-18 00:00:00	Riachão	15
2719	2016-02-18 00:00:00	Riachão do Bacamarte	15
2720	2016-02-18 00:00:00	Riachão do Poço	15
2721	2016-02-18 00:00:00	Riacho de Santo Antônio	15
2722	2016-02-18 00:00:00	Riacho dos Cavalos	15
2723	2016-02-18 00:00:00	Rio Tinto	15
2724	2016-02-18 00:00:00	Salgadinho	15
2725	2016-02-18 00:00:00	Salgado de São Félix	15
2726	2016-02-18 00:00:00	Santa Cecília	15
2727	2016-02-18 00:00:00	Santa Cruz	15
2728	2016-02-18 00:00:00	Santa Helena	15
2729	2016-02-18 00:00:00	Santa Inês	15
2730	2016-02-18 00:00:00	Santa Luzia	15
2731	2016-02-18 00:00:00	Santa Rita	15
2732	2016-02-18 00:00:00	Santa Teresinha	15
2733	2016-02-18 00:00:00	Santana de Mangueira	15
2734	2016-02-18 00:00:00	Santana dos Garrotes	15
2735	2016-02-18 00:00:00	Santarém	15
2736	2016-02-18 00:00:00	Santo André	15
2737	2016-02-18 00:00:00	São Bentinho	15
2738	2016-02-18 00:00:00	São Bento	15
2739	2016-02-18 00:00:00	São Domingos	15
2740	2016-02-18 00:00:00	São Domingos do Cariri	15
2741	2016-02-18 00:00:00	São Francisco	15
2742	2016-02-18 00:00:00	São João do Cariri	15
2743	2016-02-18 00:00:00	São João do Rio do Peixe	15
2744	2016-02-18 00:00:00	São João do Tigre	15
2745	2016-02-18 00:00:00	São José da Lagoa Tapada	15
2746	2016-02-18 00:00:00	São José de Caiana	15
2747	2016-02-18 00:00:00	São José de Espinharas	15
2748	2016-02-18 00:00:00	São José de Piranhas	15
2749	2016-02-18 00:00:00	São José de Princesa	15
2750	2016-02-18 00:00:00	São José do Bonfim	15
2751	2016-02-18 00:00:00	São José do Brejo do Cruz	15
2752	2016-02-18 00:00:00	São José do Sabugi	15
2753	2016-02-18 00:00:00	São José dos Cordeiros	15
2754	2016-02-18 00:00:00	São José dos Ramos	15
2755	2016-02-18 00:00:00	São Mamede	15
2756	2016-02-18 00:00:00	São Miguel de Taipu	15
2757	2016-02-18 00:00:00	São Sebastião de Lagoa de Roça	15
2758	2016-02-18 00:00:00	São Sebastião do Umbuzeiro	15
2759	2016-02-18 00:00:00	Sapé	15
2760	2016-02-18 00:00:00	Seridó	15
2761	2016-02-18 00:00:00	Serra Branca	15
2762	2016-02-18 00:00:00	Serra da Raiz	15
2763	2016-02-18 00:00:00	Serra Grande	15
2764	2016-02-18 00:00:00	Serra Redonda	15
2765	2016-02-18 00:00:00	Serraria	15
2766	2016-02-18 00:00:00	Sertãozinho	15
2767	2016-02-18 00:00:00	Sobrado	15
2768	2016-02-18 00:00:00	Solânea	15
2769	2016-02-18 00:00:00	Soledade	15
2770	2016-02-18 00:00:00	Sossêgo	15
2771	2016-02-18 00:00:00	Sousa	15
2772	2016-02-18 00:00:00	Sumé	15
2773	2016-02-18 00:00:00	Taperoá	15
2774	2016-02-18 00:00:00	Tavares	15
2775	2016-02-18 00:00:00	Teixeira	15
2776	2016-02-18 00:00:00	Tenório	15
2777	2016-02-18 00:00:00	Triunfo	15
2778	2016-02-18 00:00:00	Uiraúna	15
2779	2016-02-18 00:00:00	Umbuzeiro	15
2780	2016-02-18 00:00:00	Várzea	15
2781	2016-02-18 00:00:00	Vieirópolis	15
2782	2016-02-18 00:00:00	Vista Serrana	15
2783	2016-02-18 00:00:00	Zabelê	15
2784	2016-02-18 00:00:00	Abatiá	18
2785	2016-02-18 00:00:00	Adrianópolis	18
2786	2016-02-18 00:00:00	Agudos do Sul	18
2787	2016-02-18 00:00:00	Almirante Tamandaré	18
2788	2016-02-18 00:00:00	Altamira do Paraná	18
2789	2016-02-18 00:00:00	Alto Paraíso	18
2790	2016-02-18 00:00:00	Alto Paraná	18
2791	2016-02-18 00:00:00	Alto Piquiri	18
2792	2016-02-18 00:00:00	Altônia	18
2793	2016-02-18 00:00:00	Alvorada do Sul	18
2794	2016-02-18 00:00:00	Amaporã	18
2795	2016-02-18 00:00:00	Ampére	18
2796	2016-02-18 00:00:00	Anahy	18
2797	2016-02-18 00:00:00	Andirá	18
2798	2016-02-18 00:00:00	Ângulo	18
2799	2016-02-18 00:00:00	Antonina	18
2800	2016-02-18 00:00:00	Antônio Olinto	18
2801	2016-02-18 00:00:00	Apucarana	18
2802	2016-02-18 00:00:00	Arapongas	18
2803	2016-02-18 00:00:00	Arapoti	18
2804	2016-02-18 00:00:00	Arapuã	18
2805	2016-02-18 00:00:00	Araruna	18
2806	2016-02-18 00:00:00	Araucária	18
2807	2016-02-18 00:00:00	Ariranha do Ivaí	18
2808	2016-02-18 00:00:00	Assaí	18
2809	2016-02-18 00:00:00	Assis Chateaubriand	18
2810	2016-02-18 00:00:00	Astorga	18
2811	2016-02-18 00:00:00	Atalaia	18
2812	2016-02-18 00:00:00	Balsa Nova	18
2813	2016-02-18 00:00:00	Bandeirantes	18
2814	2016-02-18 00:00:00	Barbosa Ferraz	18
2815	2016-02-18 00:00:00	Barra do Jacaré	18
2816	2016-02-18 00:00:00	Barracão	18
2817	2016-02-18 00:00:00	Bela Vista da Caroba	18
2818	2016-02-18 00:00:00	Bela Vista do Paraíso	18
2819	2016-02-18 00:00:00	Bituruna	18
2820	2016-02-18 00:00:00	Boa Esperança	18
2821	2016-02-18 00:00:00	Boa Esperança do Iguaçu	18
2822	2016-02-18 00:00:00	Boa Ventura de São Roque	18
2823	2016-02-18 00:00:00	Boa Vista da Aparecida	18
2824	2016-02-18 00:00:00	Bocaiúva do Sul	18
2825	2016-02-18 00:00:00	Bom Jesus do Sul	18
2826	2016-02-18 00:00:00	Bom Sucesso	18
2827	2016-02-18 00:00:00	Bom Sucesso do Sul	18
2828	2016-02-18 00:00:00	Borrazópolis	18
2829	2016-02-18 00:00:00	Braganey	18
2830	2016-02-18 00:00:00	Brasilândia do Sul	18
2831	2016-02-18 00:00:00	Cafeara	18
2832	2016-02-18 00:00:00	Cafelândia	18
2833	2016-02-18 00:00:00	Cafezal do Sul	18
2834	2016-02-18 00:00:00	Califórnia	18
2835	2016-02-18 00:00:00	Cambará	18
2836	2016-02-18 00:00:00	Cambé	18
2837	2016-02-18 00:00:00	Cambira	18
2838	2016-02-18 00:00:00	Campina da Lagoa	18
2839	2016-02-18 00:00:00	Campina do Simão	18
2840	2016-02-18 00:00:00	Campina Grande do Sul	18
2841	2016-02-18 00:00:00	Campo Bonito	18
2842	2016-02-18 00:00:00	Campo do Tenente	18
2843	2016-02-18 00:00:00	Campo Largo	18
2844	2016-02-18 00:00:00	Campo Magro	18
2845	2016-02-18 00:00:00	Campo Mourão	18
2846	2016-02-18 00:00:00	Cândido de Abreu	18
2847	2016-02-18 00:00:00	Candói	18
2848	2016-02-18 00:00:00	Cantagalo	18
2849	2016-02-18 00:00:00	Capanema	18
2850	2016-02-18 00:00:00	Capitão Leônidas Marques	18
2851	2016-02-18 00:00:00	Carambeí	18
2852	2016-02-18 00:00:00	Carlópolis	18
2853	2016-02-18 00:00:00	Cascavel	18
2854	2016-02-18 00:00:00	Castro	18
2855	2016-02-18 00:00:00	Catanduvas	18
2856	2016-02-18 00:00:00	Centenário do Sul	18
2857	2016-02-18 00:00:00	Cerro Azul	18
2858	2016-02-18 00:00:00	Céu Azul	18
2859	2016-02-18 00:00:00	Chopinzinho	18
2860	2016-02-18 00:00:00	Cianorte	18
2861	2016-02-18 00:00:00	Cidade Gaúcha	18
2862	2016-02-18 00:00:00	Clevelândia	18
2863	2016-02-18 00:00:00	Colombo	18
2864	2016-02-18 00:00:00	Colorado	18
2865	2016-02-18 00:00:00	Congonhinhas	18
2866	2016-02-18 00:00:00	Conselheiro Mairinck	18
2867	2016-02-18 00:00:00	Contenda	18
2868	2016-02-18 00:00:00	Corbélia	18
2869	2016-02-18 00:00:00	Cornélio Procópio	18
2870	2016-02-18 00:00:00	Coronel Domingos Soares	18
2871	2016-02-18 00:00:00	Coronel Vivida	18
2872	2016-02-18 00:00:00	Corumbataí do Sul	18
2873	2016-02-18 00:00:00	Cruz Machado	18
2874	2016-02-18 00:00:00	Cruzeiro do Iguaçu	18
2875	2016-02-18 00:00:00	Cruzeiro do Oeste	18
2876	2016-02-18 00:00:00	Cruzeiro do Sul	18
2877	2016-02-18 00:00:00	Cruzmaltina	18
2878	2016-02-18 00:00:00	Curitiba	18
2879	2016-02-18 00:00:00	Curiúva	18
2880	2016-02-18 00:00:00	Diamante D'Oeste	18
2881	2016-02-18 00:00:00	Diamante do Norte	18
2882	2016-02-18 00:00:00	Diamante do Sul	18
2883	2016-02-18 00:00:00	Dois Vizinhos	18
2884	2016-02-18 00:00:00	Douradina	18
2885	2016-02-18 00:00:00	Doutor Camargo	18
2886	2016-02-18 00:00:00	Doutor Ulysses	18
2887	2016-02-18 00:00:00	Enéas Marques	18
2888	2016-02-18 00:00:00	Engenheiro Beltrão	18
2889	2016-02-18 00:00:00	Entre Rios do Oeste	18
2890	2016-02-18 00:00:00	Esperança Nova	18
2891	2016-02-18 00:00:00	Espigão Alto do Iguaçu	18
2892	2016-02-18 00:00:00	Farol	18
2893	2016-02-18 00:00:00	Faxinal	18
2894	2016-02-18 00:00:00	Fazenda Rio Grande	18
2895	2016-02-18 00:00:00	Fênix	18
2896	2016-02-18 00:00:00	Fernandes Pinheiro	18
2897	2016-02-18 00:00:00	Figueira	18
2898	2016-02-18 00:00:00	Flor da Serra do Sul	18
2899	2016-02-18 00:00:00	Floraí	18
2900	2016-02-18 00:00:00	Floresta	18
2901	2016-02-18 00:00:00	Florestópolis	18
2902	2016-02-18 00:00:00	Flórida	18
2903	2016-02-18 00:00:00	Formosa do Oeste	18
2904	2016-02-18 00:00:00	Foz do Iguaçu	18
2905	2016-02-18 00:00:00	Foz do Jordão	18
2906	2016-02-18 00:00:00	Francisco Alves	18
2907	2016-02-18 00:00:00	Francisco Beltrão	18
2908	2016-02-18 00:00:00	General Carneiro	18
2909	2016-02-18 00:00:00	Godoy Moreira	18
2910	2016-02-18 00:00:00	Goioerê	18
2911	2016-02-18 00:00:00	Goioxim	18
2912	2016-02-18 00:00:00	Grandes Rios	18
2913	2016-02-18 00:00:00	Guaíra	18
2914	2016-02-18 00:00:00	Guairaçá	18
2915	2016-02-18 00:00:00	Guamiranga	18
2916	2016-02-18 00:00:00	Guapirama	18
2917	2016-02-18 00:00:00	Guaporema	18
2918	2016-02-18 00:00:00	Guaraci	18
2919	2016-02-18 00:00:00	Guaraniaçu	18
2920	2016-02-18 00:00:00	Guarapuava	18
2921	2016-02-18 00:00:00	Guaraqueçaba	18
2922	2016-02-18 00:00:00	Guaratuba	18
2923	2016-02-18 00:00:00	Honório Serpa	18
2924	2016-02-18 00:00:00	Ibaiti	18
2925	2016-02-18 00:00:00	Ibema	18
2926	2016-02-18 00:00:00	Ibiporã	18
2927	2016-02-18 00:00:00	Icaraíma	18
2928	2016-02-18 00:00:00	Iguaraçu	18
2929	2016-02-18 00:00:00	Iguatu	18
2930	2016-02-18 00:00:00	Imbaú	18
2931	2016-02-18 00:00:00	Imbituva	18
2932	2016-02-18 00:00:00	Inácio Martins	18
2933	2016-02-18 00:00:00	Inajá	18
2934	2016-02-18 00:00:00	Indianópolis	18
2935	2016-02-18 00:00:00	Ipiranga	18
2936	2016-02-18 00:00:00	Iporã	18
2937	2016-02-18 00:00:00	Iracema do Oeste	18
2938	2016-02-18 00:00:00	Irati	18
2939	2016-02-18 00:00:00	Iretama	18
2940	2016-02-18 00:00:00	Itaguajé	18
2941	2016-02-18 00:00:00	Itaipulândia	18
2942	2016-02-18 00:00:00	Itambaracá	18
2943	2016-02-18 00:00:00	Itambé	18
2944	2016-02-18 00:00:00	Itapejara d'Oeste	18
2945	2016-02-18 00:00:00	Itaperuçu	18
2946	2016-02-18 00:00:00	Itaúna do Sul	18
2947	2016-02-18 00:00:00	Ivaí	18
2948	2016-02-18 00:00:00	Ivaiporã	18
2949	2016-02-18 00:00:00	Ivaté	18
2950	2016-02-18 00:00:00	Ivatuba	18
2951	2016-02-18 00:00:00	Jaboti	18
2952	2016-02-18 00:00:00	Jacarezinho	18
2953	2016-02-18 00:00:00	Jaguapitã	18
2954	2016-02-18 00:00:00	Jaguariaíva	18
2955	2016-02-18 00:00:00	Jandaia do Sul	18
2956	2016-02-18 00:00:00	Janiópolis	18
2957	2016-02-18 00:00:00	Japira	18
2958	2016-02-18 00:00:00	Japurá	18
2959	2016-02-18 00:00:00	Jardim Alegre	18
2960	2016-02-18 00:00:00	Jardim Olinda	18
2961	2016-02-18 00:00:00	Jataizinho	18
2962	2016-02-18 00:00:00	Jesuítas	18
2963	2016-02-18 00:00:00	Joaquim Távora	18
2964	2016-02-18 00:00:00	Jundiaí do Sul	18
2965	2016-02-18 00:00:00	Juranda	18
2966	2016-02-18 00:00:00	Jussara	18
2967	2016-02-18 00:00:00	Kaloré	18
2968	2016-02-18 00:00:00	Lapa	18
2969	2016-02-18 00:00:00	Laranjal	18
2970	2016-02-18 00:00:00	Laranjeiras do Sul	18
2971	2016-02-18 00:00:00	Leópolis	18
2972	2016-02-18 00:00:00	Lidianópolis	18
2973	2016-02-18 00:00:00	Lindoeste	18
2974	2016-02-18 00:00:00	Loanda	18
2975	2016-02-18 00:00:00	Lobato	18
2976	2016-02-18 00:00:00	Londrina	18
2977	2016-02-18 00:00:00	Luiziana	18
2978	2016-02-18 00:00:00	Lunardelli	18
2979	2016-02-18 00:00:00	Lupionópolis	18
2980	2016-02-18 00:00:00	Mallet	18
2981	2016-02-18 00:00:00	Mamborê	18
2982	2016-02-18 00:00:00	Mandaguaçu	18
2983	2016-02-18 00:00:00	Mandaguari	18
2984	2016-02-18 00:00:00	Mandirituba	18
2985	2016-02-18 00:00:00	Manfrinópolis	18
2986	2016-02-18 00:00:00	Mangueirinha	18
2987	2016-02-18 00:00:00	Manoel Ribas	18
2988	2016-02-18 00:00:00	Marechal Cândido Rondon	18
2989	2016-02-18 00:00:00	Maria Helena	18
2990	2016-02-18 00:00:00	Marialva	18
2991	2016-02-18 00:00:00	Marilândia do Sul	18
2992	2016-02-18 00:00:00	Marilena	18
2993	2016-02-18 00:00:00	Mariluz	18
2994	2016-02-18 00:00:00	Maringá	18
2995	2016-02-18 00:00:00	Mariópolis	18
2996	2016-02-18 00:00:00	Maripá	18
2997	2016-02-18 00:00:00	Marmeleiro	18
2998	2016-02-18 00:00:00	Marquinho	18
2999	2016-02-18 00:00:00	Marumbi	18
3000	2016-02-18 00:00:00	Matelândia	18
3001	2016-02-18 00:00:00	Matinhos	18
3002	2016-02-18 00:00:00	Mato Rico	18
3003	2016-02-18 00:00:00	Mauá da Serra	18
3004	2016-02-18 00:00:00	Medianeira	18
3005	2016-02-18 00:00:00	Mercedes	18
3006	2016-02-18 00:00:00	Mirador	18
3007	2016-02-18 00:00:00	Miraselva	18
3008	2016-02-18 00:00:00	Missal	18
3009	2016-02-18 00:00:00	Moreira Sales	18
3010	2016-02-18 00:00:00	Morretes	18
3011	2016-02-18 00:00:00	Munhoz de Melo	18
3012	2016-02-18 00:00:00	Nossa Senhora das Graças	18
3013	2016-02-18 00:00:00	Nova Aliança do Ivaí	18
3014	2016-02-18 00:00:00	Nova América da Colina	18
3015	2016-02-18 00:00:00	Nova Aurora	18
3016	2016-02-18 00:00:00	Nova Cantu	18
3017	2016-02-18 00:00:00	Nova Esperança	18
3018	2016-02-18 00:00:00	Nova Esperança do Sudoeste	18
3019	2016-02-18 00:00:00	Nova Fátima	18
3020	2016-02-18 00:00:00	Nova Laranjeiras	18
3021	2016-02-18 00:00:00	Nova Londrina	18
3022	2016-02-18 00:00:00	Nova Olímpia	18
3023	2016-02-18 00:00:00	Nova Prata do Iguaçu	18
3024	2016-02-18 00:00:00	Nova Santa Bárbara	18
3025	2016-02-18 00:00:00	Nova Santa Rosa	18
3026	2016-02-18 00:00:00	Nova Tebas	18
3027	2016-02-18 00:00:00	Novo Itacolomi	18
3028	2016-02-18 00:00:00	Ortigueira	18
3029	2016-02-18 00:00:00	Ourizona	18
3030	2016-02-18 00:00:00	Ouro Verde do Oeste	18
3031	2016-02-18 00:00:00	Paiçandu	18
3032	2016-02-18 00:00:00	Palmas	18
3033	2016-02-18 00:00:00	Palmeira	18
3034	2016-02-18 00:00:00	Palmital	18
3035	2016-02-18 00:00:00	Palotina	18
3036	2016-02-18 00:00:00	Paraíso do Norte	18
3037	2016-02-18 00:00:00	Paranacity	18
3038	2016-02-18 00:00:00	Paranaguá	18
3039	2016-02-18 00:00:00	Paranapoema	18
3040	2016-02-18 00:00:00	Paranavaí	18
3041	2016-02-18 00:00:00	Pato Bragado	18
3042	2016-02-18 00:00:00	Pato Branco	18
3043	2016-02-18 00:00:00	Paula Freitas	18
3044	2016-02-18 00:00:00	Paulo Frontin	18
3045	2016-02-18 00:00:00	Peabiru	18
3046	2016-02-18 00:00:00	Perobal	18
3047	2016-02-18 00:00:00	Pérola	18
3048	2016-02-18 00:00:00	Pérola d'Oeste	18
3049	2016-02-18 00:00:00	Piên	18
3050	2016-02-18 00:00:00	Pinhais	18
3051	2016-02-18 00:00:00	Pinhal de São Bento	18
3052	2016-02-18 00:00:00	Pinhalão	18
3053	2016-02-18 00:00:00	Pinhão	18
3054	2016-02-18 00:00:00	Piraí do Sul	18
3055	2016-02-18 00:00:00	Piraquara	18
3056	2016-02-18 00:00:00	Pitanga	18
3057	2016-02-18 00:00:00	Pitangueiras	18
3058	2016-02-18 00:00:00	Planaltina do Paraná	18
3059	2016-02-18 00:00:00	Planalto	18
3060	2016-02-18 00:00:00	Ponta Grossa	18
3061	2016-02-18 00:00:00	Pontal do Paraná	18
3062	2016-02-18 00:00:00	Porecatu	18
3063	2016-02-18 00:00:00	Porto Amazonas	18
3064	2016-02-18 00:00:00	Porto Barreiro	18
3065	2016-02-18 00:00:00	Porto Rico	18
3066	2016-02-18 00:00:00	Porto Vitória	18
3067	2016-02-18 00:00:00	Prado Ferreira	18
3068	2016-02-18 00:00:00	Pranchita	18
3069	2016-02-18 00:00:00	Presidente Castelo Branco	18
3070	2016-02-18 00:00:00	Primeiro de Maio	18
3071	2016-02-18 00:00:00	Prudentópolis	18
3072	2016-02-18 00:00:00	Quarto Centenário	18
3073	2016-02-18 00:00:00	Quatiguá	18
3074	2016-02-18 00:00:00	Quatro Barras	18
3075	2016-02-18 00:00:00	Quatro Pontes	18
3076	2016-02-18 00:00:00	Quedas do Iguaçu	18
3077	2016-02-18 00:00:00	Querência do Norte	18
3078	2016-02-18 00:00:00	Quinta do Sol	18
3079	2016-02-18 00:00:00	Quitandinha	18
3080	2016-02-18 00:00:00	Ramilândia	18
3081	2016-02-18 00:00:00	Rancho Alegre	18
3082	2016-02-18 00:00:00	Rancho Alegre D'Oeste	18
3083	2016-02-18 00:00:00	Realeza	18
3084	2016-02-18 00:00:00	Rebouças	18
3085	2016-02-18 00:00:00	Renascença	18
3086	2016-02-18 00:00:00	Reserva	18
3087	2016-02-18 00:00:00	Reserva do Iguaçu	18
3088	2016-02-18 00:00:00	Ribeirão Claro	18
3089	2016-02-18 00:00:00	Ribeirão do Pinhal	18
3090	2016-02-18 00:00:00	Rio Azul	18
3091	2016-02-18 00:00:00	Rio Bom	18
3092	2016-02-18 00:00:00	Rio Bonito do Iguaçu	18
3093	2016-02-18 00:00:00	Rio Branco do Ivaí	18
3094	2016-02-18 00:00:00	Rio Branco do Sul	18
3095	2016-02-18 00:00:00	Rio Negro	18
3096	2016-02-18 00:00:00	Rolândia	18
3097	2016-02-18 00:00:00	Roncador	18
3098	2016-02-18 00:00:00	Rondon	18
3099	2016-02-18 00:00:00	Rosário do Ivaí	18
3100	2016-02-18 00:00:00	Sabáudia	18
3101	2016-02-18 00:00:00	Salgado Filho	18
3102	2016-02-18 00:00:00	Salto do Itararé	18
3103	2016-02-18 00:00:00	Salto do Lontra	18
3104	2016-02-18 00:00:00	Santa Amélia	18
3105	2016-02-18 00:00:00	Santa Cecília do Pavão	18
3106	2016-02-18 00:00:00	Santa Cruz de Monte Castelo	18
3107	2016-02-18 00:00:00	Santa Fé	18
3108	2016-02-18 00:00:00	Santa Helena	18
3109	2016-02-18 00:00:00	Santa Inês	18
3110	2016-02-18 00:00:00	Santa Isabel do Ivaí	18
3111	2016-02-18 00:00:00	Santa Izabel do Oeste	18
3112	2016-02-18 00:00:00	Santa Lúcia	18
3113	2016-02-18 00:00:00	Santa Maria do Oeste	18
3114	2016-02-18 00:00:00	Santa Mariana	18
3115	2016-02-18 00:00:00	Santa Mônica	18
3116	2016-02-18 00:00:00	Santa Tereza do Oeste	18
3117	2016-02-18 00:00:00	Santa Terezinha de Itaipu	18
3118	2016-02-18 00:00:00	Santana do Itararé	18
3119	2016-02-18 00:00:00	Santo Antônio da Platina	18
3120	2016-02-18 00:00:00	Santo Antônio do Caiuá	18
3121	2016-02-18 00:00:00	Santo Antônio do Paraíso	18
3122	2016-02-18 00:00:00	Santo Antônio do Sudoeste	18
3123	2016-02-18 00:00:00	Santo Inácio	18
3124	2016-02-18 00:00:00	São Carlos do Ivaí	18
3125	2016-02-18 00:00:00	São Jerônimo da Serra	18
3126	2016-02-18 00:00:00	São João	18
3127	2016-02-18 00:00:00	São João do Caiuá	18
3128	2016-02-18 00:00:00	São João do Ivaí	18
3129	2016-02-18 00:00:00	São João do Triunfo	18
3130	2016-02-18 00:00:00	São Jorge d'Oeste	18
3131	2016-02-18 00:00:00	São Jorge do Ivaí	18
3132	2016-02-18 00:00:00	São Jorge do Patrocínio	18
3133	2016-02-18 00:00:00	São José da Boa Vista	18
3134	2016-02-18 00:00:00	São José das Palmeiras	18
3135	2016-02-18 00:00:00	São José dos Pinhais	18
3136	2016-02-18 00:00:00	São Manoel do Paraná	18
3137	2016-02-18 00:00:00	São Mateus do Sul	18
3138	2016-02-18 00:00:00	São Miguel do Iguaçu	18
3139	2016-02-18 00:00:00	São Pedro do Iguaçu	18
3140	2016-02-18 00:00:00	São Pedro do Ivaí	18
3141	2016-02-18 00:00:00	São Pedro do Paraná	18
3142	2016-02-18 00:00:00	São Sebastião da Amoreira	18
3143	2016-02-18 00:00:00	São Tomé	18
3144	2016-02-18 00:00:00	Sapopema	18
3145	2016-02-18 00:00:00	Sarandi	18
3146	2016-02-18 00:00:00	Saudade do Iguaçu	18
3147	2016-02-18 00:00:00	Sengés	18
3148	2016-02-18 00:00:00	Serranópolis do Iguaçu	18
3149	2016-02-18 00:00:00	Sertaneja	18
3150	2016-02-18 00:00:00	Sertanópolis	18
3151	2016-02-18 00:00:00	Siqueira Campos	18
3152	2016-02-18 00:00:00	Sulina	18
3153	2016-02-18 00:00:00	Tamarana	18
3154	2016-02-18 00:00:00	Tamboara	18
3155	2016-02-18 00:00:00	Tapejara	18
3156	2016-02-18 00:00:00	Tapira	18
3157	2016-02-18 00:00:00	Teixeira Soares	18
3158	2016-02-18 00:00:00	Telêmaco Borba	18
3159	2016-02-18 00:00:00	Terra Boa	18
3160	2016-02-18 00:00:00	Terra Rica	18
3161	2016-02-18 00:00:00	Terra Roxa	18
3162	2016-02-18 00:00:00	Tibagi	18
3163	2016-02-18 00:00:00	Tijucas do Sul	18
3164	2016-02-18 00:00:00	Toledo	18
3165	2016-02-18 00:00:00	Tomazina	18
3166	2016-02-18 00:00:00	Três Barras do Paraná	18
3167	2016-02-18 00:00:00	Tunas do Paraná	18
3168	2016-02-18 00:00:00	Tuneiras do Oeste	18
3169	2016-02-18 00:00:00	Tupãssi	18
3170	2016-02-18 00:00:00	Turvo	18
3171	2016-02-18 00:00:00	Ubiratã	18
3172	2016-02-18 00:00:00	Umuarama	18
3173	2016-02-18 00:00:00	União da Vitória	18
3174	2016-02-18 00:00:00	Uniflor	18
3175	2016-02-18 00:00:00	Uraí	18
3176	2016-02-18 00:00:00	Ventania	18
3177	2016-02-18 00:00:00	Vera Cruz do Oeste	18
3178	2016-02-18 00:00:00	Verê	18
3179	2016-02-18 00:00:00	Virmond	18
3180	2016-02-18 00:00:00	Vitorino	18
3181	2016-02-18 00:00:00	Wenceslau Braz	18
3182	2016-02-18 00:00:00	Xambrê	18
3183	2016-02-18 00:00:00	Abreu e Lima	16
3184	2016-02-18 00:00:00	Afogados da Ingazeira	16
3185	2016-02-18 00:00:00	Afrânio	16
3186	2016-02-18 00:00:00	Agrestina	16
3187	2016-02-18 00:00:00	Água Preta	16
3188	2016-02-18 00:00:00	Águas Belas	16
3189	2016-02-18 00:00:00	Alagoinha	16
3190	2016-02-18 00:00:00	Aliança	16
3191	2016-02-18 00:00:00	Altinho	16
3192	2016-02-18 00:00:00	Amaraji	16
3193	2016-02-18 00:00:00	Angelim	16
3194	2016-02-18 00:00:00	Araçoiaba	16
3195	2016-02-18 00:00:00	Araripina	16
3196	2016-02-18 00:00:00	Arcoverde	16
3197	2016-02-18 00:00:00	Barra de Guabiraba	16
3198	2016-02-18 00:00:00	Barreiros	16
3199	2016-02-18 00:00:00	Belém de Maria	16
3200	2016-02-18 00:00:00	Belém de São Francisco	16
3201	2016-02-18 00:00:00	Belo Jardim	16
3202	2016-02-18 00:00:00	Betânia	16
3203	2016-02-18 00:00:00	Bezerros	16
3204	2016-02-18 00:00:00	Bodocó	16
3205	2016-02-18 00:00:00	Bom Conselho	16
3206	2016-02-18 00:00:00	Bom Jardim	16
3207	2016-02-18 00:00:00	Bonito	16
3208	2016-02-18 00:00:00	Brejão	16
3209	2016-02-18 00:00:00	Brejinho	16
3210	2016-02-18 00:00:00	Brejo da Madre de Deus	16
3211	2016-02-18 00:00:00	Buenos Aires	16
3212	2016-02-18 00:00:00	Buíque	16
3213	2016-02-18 00:00:00	Cabo de Santo Agostinho	16
3214	2016-02-18 00:00:00	Cabrobó	16
3215	2016-02-18 00:00:00	Cachoeirinha	16
3216	2016-02-18 00:00:00	Caetés	16
3217	2016-02-18 00:00:00	Calçado	16
3218	2016-02-18 00:00:00	Calumbi	16
3219	2016-02-18 00:00:00	Camaragibe	16
3220	2016-02-18 00:00:00	Camocim de São Félix	16
3221	2016-02-18 00:00:00	Camutanga	16
3222	2016-02-18 00:00:00	Canhotinho	16
3223	2016-02-18 00:00:00	Capoeiras	16
3224	2016-02-18 00:00:00	Carnaíba	16
3225	2016-02-18 00:00:00	Carnaubeira da Penha	16
3226	2016-02-18 00:00:00	Carpina	16
3227	2016-02-18 00:00:00	Caruaru	16
3228	2016-02-18 00:00:00	Casinhas	16
3229	2016-02-18 00:00:00	Catende	16
3230	2016-02-18 00:00:00	Cedro	16
3231	2016-02-18 00:00:00	Chã de Alegria	16
3232	2016-02-18 00:00:00	Chã Grande	16
3233	2016-02-18 00:00:00	Condado	16
3234	2016-02-18 00:00:00	Correntes	16
3235	2016-02-18 00:00:00	Cortês	16
3236	2016-02-18 00:00:00	Cumaru	16
3237	2016-02-18 00:00:00	Cupira	16
3238	2016-02-18 00:00:00	Custódia	16
3239	2016-02-18 00:00:00	Dormentes	16
3240	2016-02-18 00:00:00	Escada	16
3241	2016-02-18 00:00:00	Exu	16
3242	2016-02-18 00:00:00	Feira Nova	16
3243	2016-02-18 00:00:00	Fernando de Noronha	16
3244	2016-02-18 00:00:00	Ferreiros	16
3245	2016-02-18 00:00:00	Flores	16
3246	2016-02-18 00:00:00	Floresta	16
3247	2016-02-18 00:00:00	Frei Miguelinho	16
3248	2016-02-18 00:00:00	Gameleira	16
3249	2016-02-18 00:00:00	Garanhuns	16
3250	2016-02-18 00:00:00	Glória do Goitá	16
3251	2016-02-18 00:00:00	Goiana	16
3252	2016-02-18 00:00:00	Granito	16
3253	2016-02-18 00:00:00	Gravatá	16
3254	2016-02-18 00:00:00	Iati	16
3255	2016-02-18 00:00:00	Ibimirim	16
3256	2016-02-18 00:00:00	Ibirajuba	16
3257	2016-02-18 00:00:00	Igarassu	16
3258	2016-02-18 00:00:00	Iguaraci	16
3259	2016-02-18 00:00:00	Ilha de Itamaracá	16
3260	2016-02-18 00:00:00	Inajá	16
3261	2016-02-18 00:00:00	Ingazeira	16
3262	2016-02-18 00:00:00	Ipojuca	16
3263	2016-02-18 00:00:00	Ipubi	16
3264	2016-02-18 00:00:00	Itacuruba	16
3265	2016-02-18 00:00:00	Itaíba	16
3266	2016-02-18 00:00:00	Itambé	16
3267	2016-02-18 00:00:00	Itapetim	16
3268	2016-02-18 00:00:00	Itapissuma	16
3269	2016-02-18 00:00:00	Itaquitinga	16
3270	2016-02-18 00:00:00	Jaboatão dos Guararapes	16
3271	2016-02-18 00:00:00	Jaqueira	16
3272	2016-02-18 00:00:00	Jataúba	16
3273	2016-02-18 00:00:00	Jatobá	16
3274	2016-02-18 00:00:00	João Alfredo	16
3275	2016-02-18 00:00:00	Joaquim Nabuco	16
3276	2016-02-18 00:00:00	Jucati	16
3277	2016-02-18 00:00:00	Jupi	16
3278	2016-02-18 00:00:00	Jurema	16
3279	2016-02-18 00:00:00	Lagoa do Carro	16
3280	2016-02-18 00:00:00	Lagoa do Itaenga	16
3281	2016-02-18 00:00:00	Lagoa do Ouro	16
3282	2016-02-18 00:00:00	Lagoa dos Gatos	16
3283	2016-02-18 00:00:00	Lagoa Grande	16
3284	2016-02-18 00:00:00	Lajedo	16
3285	2016-02-18 00:00:00	Limoeiro	16
3286	2016-02-18 00:00:00	Macaparana	16
3287	2016-02-18 00:00:00	Machados	16
3288	2016-02-18 00:00:00	Manari	16
3289	2016-02-18 00:00:00	Maraial	16
3290	2016-02-18 00:00:00	Mirandiba	16
3291	2016-02-18 00:00:00	Moreilândia	16
3292	2016-02-18 00:00:00	Moreno	16
3293	2016-02-18 00:00:00	Nazaré da Mata	16
3294	2016-02-18 00:00:00	Olinda	16
3295	2016-02-18 00:00:00	Orobó	16
3296	2016-02-18 00:00:00	Orocó	16
3297	2016-02-18 00:00:00	Ouricuri	16
3298	2016-02-18 00:00:00	Palmares	16
3299	2016-02-18 00:00:00	Palmeirina	16
3300	2016-02-18 00:00:00	Panelas	16
3301	2016-02-18 00:00:00	Paranatama	16
3302	2016-02-18 00:00:00	Parnamirim	16
3303	2016-02-18 00:00:00	Passira	16
3304	2016-02-18 00:00:00	Paudalho	16
3305	2016-02-18 00:00:00	Paulista	16
3306	2016-02-18 00:00:00	Pedra	16
3307	2016-02-18 00:00:00	Pesqueira	16
3308	2016-02-18 00:00:00	Petrolândia	16
3309	2016-02-18 00:00:00	Petrolina	16
3310	2016-02-18 00:00:00	Poção	16
3311	2016-02-18 00:00:00	Pombos	16
3312	2016-02-18 00:00:00	Primavera	16
3313	2016-02-18 00:00:00	Quipapá	16
3314	2016-02-18 00:00:00	Quixaba	16
3315	2016-02-18 00:00:00	Recife	16
3316	2016-02-18 00:00:00	Riacho das Almas	16
3317	2016-02-18 00:00:00	Ribeirão	16
3318	2016-02-18 00:00:00	Rio Formoso	16
3319	2016-02-18 00:00:00	Sairé	16
3320	2016-02-18 00:00:00	Salgadinho	16
3321	2016-02-18 00:00:00	Salgueiro	16
3322	2016-02-18 00:00:00	Saloá	16
3323	2016-02-18 00:00:00	Sanharó	16
3324	2016-02-18 00:00:00	Santa Cruz	16
3325	2016-02-18 00:00:00	Santa Cruz da Baixa Verde	16
3326	2016-02-18 00:00:00	Santa Cruz do Capibaribe	16
3327	2016-02-18 00:00:00	Santa Filomena	16
3328	2016-02-18 00:00:00	Santa Maria da Boa Vista	16
3329	2016-02-18 00:00:00	Santa Maria do Cambucá	16
3330	2016-02-18 00:00:00	Santa Terezinha	16
3331	2016-02-18 00:00:00	São Benedito do Sul	16
3332	2016-02-18 00:00:00	São Bento do Una	16
3333	2016-02-18 00:00:00	São Caitano	16
3334	2016-02-18 00:00:00	São João	16
3335	2016-02-18 00:00:00	São Joaquim do Monte	16
3336	2016-02-18 00:00:00	São José da Coroa Grande	16
3337	2016-02-18 00:00:00	São José do Belmonte	16
3338	2016-02-18 00:00:00	São José do Egito	16
3339	2016-02-18 00:00:00	São Lourenço da Mata	16
3340	2016-02-18 00:00:00	São Vicente Ferrer	16
3341	2016-02-18 00:00:00	Serra Talhada	16
3342	2016-02-18 00:00:00	Serrita	16
3343	2016-02-18 00:00:00	Sertânia	16
3344	2016-02-18 00:00:00	Sirinhaém	16
3345	2016-02-18 00:00:00	Solidão	16
3346	2016-02-18 00:00:00	Surubim	16
3347	2016-02-18 00:00:00	Tabira	16
3348	2016-02-18 00:00:00	Tacaimbó	16
3349	2016-02-18 00:00:00	Tacaratu	16
3350	2016-02-18 00:00:00	Tamandaré	16
3351	2016-02-18 00:00:00	Taquaritinga do Norte	16
3352	2016-02-18 00:00:00	Terezinha	16
3353	2016-02-18 00:00:00	Terra Nova	16
3354	2016-02-18 00:00:00	Timbaúba	16
3355	2016-02-18 00:00:00	Toritama	16
3356	2016-02-18 00:00:00	Tracunhaém	16
3357	2016-02-18 00:00:00	Trindade	16
3358	2016-02-18 00:00:00	Triunfo	16
3359	2016-02-18 00:00:00	Tupanatinga	16
3360	2016-02-18 00:00:00	Tuparetama	16
3361	2016-02-18 00:00:00	Venturosa	16
3362	2016-02-18 00:00:00	Verdejante	16
3363	2016-02-18 00:00:00	Vertente do Lério	16
3364	2016-02-18 00:00:00	Vertentes	16
3365	2016-02-18 00:00:00	Vicência	16
3366	2016-02-18 00:00:00	Vitória de Santo Antão	16
3367	2016-02-18 00:00:00	Xexéu	16
3368	2016-02-18 00:00:00	Acauã	17
3369	2016-02-18 00:00:00	Agricolândia	17
3370	2016-02-18 00:00:00	Água Branca	17
3371	2016-02-18 00:00:00	Alagoinha do Piauí	17
3372	2016-02-18 00:00:00	Alegrete do Piauí	17
3373	2016-02-18 00:00:00	Alto Longá	17
3374	2016-02-18 00:00:00	Altos	17
3375	2016-02-18 00:00:00	Alvorada do Gurguéia	17
3376	2016-02-18 00:00:00	Amarante	17
3377	2016-02-18 00:00:00	Angical do Piauí	17
3378	2016-02-18 00:00:00	Anísio de Abreu	17
3379	2016-02-18 00:00:00	Antônio Almeida	17
3380	2016-02-18 00:00:00	Aroazes	17
3381	2016-02-18 00:00:00	Aroeiras do Itaim	17
3382	2016-02-18 00:00:00	Arraial	17
3383	2016-02-18 00:00:00	Assunção do Piauí	17
3384	2016-02-18 00:00:00	Avelino Lopes	17
3385	2016-02-18 00:00:00	Baixa Grande do Ribeiro	17
3386	2016-02-18 00:00:00	Barra D'Alcântara	17
3387	2016-02-18 00:00:00	Barras	17
3388	2016-02-18 00:00:00	Barreiras do Piauí	17
3389	2016-02-18 00:00:00	Barro Duro	17
3390	2016-02-18 00:00:00	Batalha	17
3391	2016-02-18 00:00:00	Bela Vista do Piauí	17
3392	2016-02-18 00:00:00	Belém do Piauí	17
3393	2016-02-18 00:00:00	Beneditinos	17
3394	2016-02-18 00:00:00	Bertolínia	17
3395	2016-02-18 00:00:00	Betânia do Piauí	17
3396	2016-02-18 00:00:00	Boa Hora	17
3397	2016-02-18 00:00:00	Bocaina	17
3398	2016-02-18 00:00:00	Bom Jesus	17
3399	2016-02-18 00:00:00	Bom Princípio do Piauí	17
3400	2016-02-18 00:00:00	Bonfim do Piauí	17
3401	2016-02-18 00:00:00	Boqueirão do Piauí	17
3402	2016-02-18 00:00:00	Brasileira	17
3403	2016-02-18 00:00:00	Brejo do Piauí	17
3404	2016-02-18 00:00:00	Buriti dos Lopes	17
3405	2016-02-18 00:00:00	Buriti dos Montes	17
3406	2016-02-18 00:00:00	Cabeceiras do Piauí	17
3407	2016-02-18 00:00:00	Cajazeiras do Piauí	17
3408	2016-02-18 00:00:00	Cajueiro da Praia	17
3409	2016-02-18 00:00:00	Caldeirão Grande do Piauí	17
3410	2016-02-18 00:00:00	Campinas do Piauí	17
3411	2016-02-18 00:00:00	Campo Alegre do Fidalgo	17
3412	2016-02-18 00:00:00	Campo Grande do Piauí	17
3413	2016-02-18 00:00:00	Campo Largo do Piauí	17
3414	2016-02-18 00:00:00	Campo Maior	17
3415	2016-02-18 00:00:00	Canavieira	17
3416	2016-02-18 00:00:00	Canto do Buriti	17
3417	2016-02-18 00:00:00	Capitão de Campos	17
3418	2016-02-18 00:00:00	Capitão Gervásio Oliveira	17
3419	2016-02-18 00:00:00	Caracol	17
3420	2016-02-18 00:00:00	Caraúbas do Piauí	17
3421	2016-02-18 00:00:00	Caridade do Piauí	17
3422	2016-02-18 00:00:00	Castelo do Piauí	17
3423	2016-02-18 00:00:00	Caxingó	17
3424	2016-02-18 00:00:00	Cocal	17
3425	2016-02-18 00:00:00	Cocal de Telha	17
3426	2016-02-18 00:00:00	Cocal dos Alves	17
3427	2016-02-18 00:00:00	Coivaras	17
3428	2016-02-18 00:00:00	Colônia do Gurguéia	17
3429	2016-02-18 00:00:00	Colônia do Piauí	17
3430	2016-02-18 00:00:00	Conceição do Canindé	17
3431	2016-02-18 00:00:00	Coronel José Dias	17
3432	2016-02-18 00:00:00	Corrente	17
3433	2016-02-18 00:00:00	Cristalândia do Piauí	17
3434	2016-02-18 00:00:00	Cristino Castro	17
3435	2016-02-18 00:00:00	Curimatá	17
3436	2016-02-18 00:00:00	Currais	17
3437	2016-02-18 00:00:00	Curral Novo do Piauí	17
3438	2016-02-18 00:00:00	Curralinhos	17
3439	2016-02-18 00:00:00	Demerval Lobão	17
3440	2016-02-18 00:00:00	Dirceu Arcoverde	17
3671	2016-02-18 00:00:00	Sapucaia	19
3441	2016-02-18 00:00:00	Dom Expedito Lopes	17
3442	2016-02-18 00:00:00	Dom Inocêncio	17
3443	2016-02-18 00:00:00	Domingos Mourão	17
3444	2016-02-18 00:00:00	Elesbão Veloso	17
3445	2016-02-18 00:00:00	Eliseu Martins	17
3446	2016-02-18 00:00:00	Esperantina	17
3447	2016-02-18 00:00:00	Fartura do Piauí	17
3448	2016-02-18 00:00:00	Flores do Piauí	17
3449	2016-02-18 00:00:00	Floresta do Piauí	17
3450	2016-02-18 00:00:00	Floriano	17
3451	2016-02-18 00:00:00	Francinópolis	17
3452	2016-02-18 00:00:00	Francisco Ayres	17
3453	2016-02-18 00:00:00	Francisco Macedo	17
3454	2016-02-18 00:00:00	Francisco Santos	17
3455	2016-02-18 00:00:00	Fronteiras	17
3456	2016-02-18 00:00:00	Geminiano	17
3457	2016-02-18 00:00:00	Gilbués	17
3458	2016-02-18 00:00:00	Guadalupe	17
3459	2016-02-18 00:00:00	Guaribas	17
3460	2016-02-18 00:00:00	Hugo Napoleão	17
3461	2016-02-18 00:00:00	Ilha Grande	17
3462	2016-02-18 00:00:00	Inhuma	17
3463	2016-02-18 00:00:00	Ipiranga do Piauí	17
3464	2016-02-18 00:00:00	Isaías Coelho	17
3465	2016-02-18 00:00:00	Itainópolis	17
3466	2016-02-18 00:00:00	Itaueira	17
3467	2016-02-18 00:00:00	Jacobina do Piauí	17
3468	2016-02-18 00:00:00	Jaicós	17
3469	2016-02-18 00:00:00	Jardim do Mulato	17
3470	2016-02-18 00:00:00	Jatobá do Piauí	17
3471	2016-02-18 00:00:00	Jerumenha	17
3472	2016-02-18 00:00:00	João Costa	17
3473	2016-02-18 00:00:00	Joaquim Pires	17
3474	2016-02-18 00:00:00	Joca Marques	17
3475	2016-02-18 00:00:00	José de Freitas	17
3476	2016-02-18 00:00:00	Juazeiro do Piauí	17
3477	2016-02-18 00:00:00	Júlio Borges	17
3478	2016-02-18 00:00:00	Jurema	17
3479	2016-02-18 00:00:00	Lagoa Alegre	17
3480	2016-02-18 00:00:00	Lagoa de São Francisco	17
3481	2016-02-18 00:00:00	Lagoa do Barro do Piauí	17
3482	2016-02-18 00:00:00	Lagoa do Piauí	17
3483	2016-02-18 00:00:00	Lagoa do Sítio	17
3484	2016-02-18 00:00:00	Lagoinha do Piauí	17
3485	2016-02-18 00:00:00	Landri Sales	17
3486	2016-02-18 00:00:00	Luís Correia	17
3487	2016-02-18 00:00:00	Luzilândia	17
3488	2016-02-18 00:00:00	Madeiro	17
3489	2016-02-18 00:00:00	Manoel Emídio	17
3490	2016-02-18 00:00:00	Marcolândia	17
3491	2016-02-18 00:00:00	Marcos Parente	17
3492	2016-02-18 00:00:00	Massapê do Piauí	17
3493	2016-02-18 00:00:00	Matias Olímpio	17
3494	2016-02-18 00:00:00	Miguel Alves	17
3495	2016-02-18 00:00:00	Miguel Leão	17
3496	2016-02-18 00:00:00	Milton Brandão	17
3497	2016-02-18 00:00:00	Monsenhor Gil	17
3498	2016-02-18 00:00:00	Monsenhor Hipólito	17
3499	2016-02-18 00:00:00	Monte Alegre do Piauí	17
3500	2016-02-18 00:00:00	Morro Cabeça no Tempo	17
3501	2016-02-18 00:00:00	Morro do Chapéu do Piauí	17
3502	2016-02-18 00:00:00	Murici dos Portelas	17
3503	2016-02-18 00:00:00	Nazaré do Piauí	17
3504	2016-02-18 00:00:00	Nazária	17
3505	2016-02-18 00:00:00	Nossa Senhora de Nazaré	17
3506	2016-02-18 00:00:00	Nossa Senhora dos Remédios	17
3507	2016-02-18 00:00:00	Nova Santa Rita	17
3508	2016-02-18 00:00:00	Novo Oriente do Piauí	17
3509	2016-02-18 00:00:00	Novo Santo Antônio	17
3510	2016-02-18 00:00:00	Oeiras	17
3511	2016-02-18 00:00:00	Olho D'Água do Piauí	17
3512	2016-02-18 00:00:00	Padre Marcos	17
3513	2016-02-18 00:00:00	Paes Landim	17
3514	2016-02-18 00:00:00	Pajeú do Piauí	17
3515	2016-02-18 00:00:00	Palmeira do Piauí	17
3516	2016-02-18 00:00:00	Palmeirais	17
3517	2016-02-18 00:00:00	Paquetá	17
3518	2016-02-18 00:00:00	Parnaguá	17
3519	2016-02-18 00:00:00	Parnaíba	17
3520	2016-02-18 00:00:00	Passagem Franca do Piauí	17
3521	2016-02-18 00:00:00	Patos do Piauí	17
3522	2016-02-18 00:00:00	Pau D'Arco do Piauí	17
3523	2016-02-18 00:00:00	Paulistana	17
3524	2016-02-18 00:00:00	Pavussu	17
3525	2016-02-18 00:00:00	Pedro II	17
3526	2016-02-18 00:00:00	Pedro Laurentino	17
3527	2016-02-18 00:00:00	Picos	17
3528	2016-02-18 00:00:00	Pimenteiras	17
3529	2016-02-18 00:00:00	Pio IX	17
3530	2016-02-18 00:00:00	Piracuruca	17
3531	2016-02-18 00:00:00	Piripiri	17
3532	2016-02-18 00:00:00	Porto	17
3533	2016-02-18 00:00:00	Porto Alegre do Piauí	17
3534	2016-02-18 00:00:00	Prata do Piauí	17
3535	2016-02-18 00:00:00	Queimada Nova	17
3536	2016-02-18 00:00:00	Redenção do Gurguéia	17
3537	2016-02-18 00:00:00	Regeneração	17
3538	2016-02-18 00:00:00	Riacho Frio	17
3539	2016-02-18 00:00:00	Ribeira do Piauí	17
3540	2016-02-18 00:00:00	Ribeiro Gonçalves	17
3541	2016-02-18 00:00:00	Rio Grande do Piauí	17
3542	2016-02-18 00:00:00	Santa Cruz do Piauí	17
3543	2016-02-18 00:00:00	Santa Cruz dos Milagres	17
3544	2016-02-18 00:00:00	Santa Filomena	17
3545	2016-02-18 00:00:00	Santa Luz	17
3546	2016-02-18 00:00:00	Santa Rosa do Piauí	17
3547	2016-02-18 00:00:00	Santana do Piauí	17
3548	2016-02-18 00:00:00	Santo Antônio de Lisboa	17
3549	2016-02-18 00:00:00	Santo Antônio dos Milagres	17
3550	2016-02-18 00:00:00	Santo Inácio do Piauí	17
3551	2016-02-18 00:00:00	São Braz do Piauí	17
3552	2016-02-18 00:00:00	São Félix do Piauí	17
3553	2016-02-18 00:00:00	São Francisco de Assis do Piauí	17
3554	2016-02-18 00:00:00	São Francisco do Piauí	17
3555	2016-02-18 00:00:00	São Gonçalo do Gurguéia	17
3556	2016-02-18 00:00:00	São Gonçalo do Piauí	17
3557	2016-02-18 00:00:00	São João da Canabrava	17
3558	2016-02-18 00:00:00	São João da Fronteira	17
3559	2016-02-18 00:00:00	São João da Serra	17
3560	2016-02-18 00:00:00	São João da Varjota	17
3561	2016-02-18 00:00:00	São João do Arraial	17
3562	2016-02-18 00:00:00	São João do Piauí	17
3563	2016-02-18 00:00:00	São José do Divino	17
3564	2016-02-18 00:00:00	São José do Peixe	17
3565	2016-02-18 00:00:00	São José do Piauí	17
3566	2016-02-18 00:00:00	São Julião	17
3567	2016-02-18 00:00:00	São Lourenço do Piauí	17
3568	2016-02-18 00:00:00	São Luis do Piauí	17
3569	2016-02-18 00:00:00	São Miguel da Baixa Grande	17
3570	2016-02-18 00:00:00	São Miguel do Fidalgo	17
3571	2016-02-18 00:00:00	São Miguel do Tapuio	17
3572	2016-02-18 00:00:00	São Pedro do Piauí	17
3573	2016-02-18 00:00:00	São Raimundo Nonato	17
3574	2016-02-18 00:00:00	Sebastião Barros	17
3575	2016-02-18 00:00:00	Sebastião Leal	17
3576	2016-02-18 00:00:00	Sigefredo Pacheco	17
3577	2016-02-18 00:00:00	Simões	17
3578	2016-02-18 00:00:00	Simplício Mendes	17
3579	2016-02-18 00:00:00	Socorro do Piauí	17
3580	2016-02-18 00:00:00	Sussuapara	17
3581	2016-02-18 00:00:00	Tamboril do Piauí	17
3582	2016-02-18 00:00:00	Tanque do Piauí	17
3583	2016-02-18 00:00:00	Teresina	17
3584	2016-02-18 00:00:00	União	17
3585	2016-02-18 00:00:00	Uruçuí	17
3586	2016-02-18 00:00:00	Valença do Piauí	17
3587	2016-02-18 00:00:00	Várzea Branca	17
3588	2016-02-18 00:00:00	Várzea Grande	17
3589	2016-02-18 00:00:00	Vera Mendes	17
3590	2016-02-18 00:00:00	Vila Nova do Piauí	17
3591	2016-02-18 00:00:00	Wall Ferraz	17
3592	2016-02-18 00:00:00	Angra dos Reis	19
3593	2016-02-18 00:00:00	Aperibé	19
3594	2016-02-18 00:00:00	Araruama	19
3595	2016-02-18 00:00:00	Areal	19
3596	2016-02-18 00:00:00	Armação dos Búzios	19
3597	2016-02-18 00:00:00	Arraial do Cabo	19
3598	2016-02-18 00:00:00	Barra do Piraí	19
3599	2016-02-18 00:00:00	Barra Mansa	19
3600	2016-02-18 00:00:00	Belford Roxo	19
3601	2016-02-18 00:00:00	Bom Jardim	19
3602	2016-02-18 00:00:00	Bom Jesus do Itabapoana	19
3603	2016-02-18 00:00:00	Cabo Frio	19
3604	2016-02-18 00:00:00	Cachoeiras de Macacu	19
3605	2016-02-18 00:00:00	Cambuci	19
3606	2016-02-18 00:00:00	Campos dos Goytacazes	19
3607	2016-02-18 00:00:00	Cantagalo	19
3608	2016-02-18 00:00:00	Carapebus	19
3609	2016-02-18 00:00:00	Cardoso Moreira	19
3610	2016-02-18 00:00:00	Carmo	19
3611	2016-02-18 00:00:00	Casimiro de Abreu	19
3612	2016-02-18 00:00:00	Comendador Levy Gasparian	19
3613	2016-02-18 00:00:00	Conceição de Macabu	19
3614	2016-02-18 00:00:00	Cordeiro	19
3615	2016-02-18 00:00:00	Duas Barras	19
3616	2016-02-18 00:00:00	Duque de Caxias	19
3617	2016-02-18 00:00:00	Engenheiro Paulo de Frontin	19
3618	2016-02-18 00:00:00	Guapimirim	19
3619	2016-02-18 00:00:00	Iguaba Grande	19
3620	2016-02-18 00:00:00	Itaboraí	19
3621	2016-02-18 00:00:00	Itaguaí	19
3622	2016-02-18 00:00:00	Italva	19
3623	2016-02-18 00:00:00	Itaocara	19
3624	2016-02-18 00:00:00	Itaperuna	19
3625	2016-02-18 00:00:00	Itatiaia	19
3626	2016-02-18 00:00:00	Japeri	19
3627	2016-02-18 00:00:00	Laje do Muriaé	19
3628	2016-02-18 00:00:00	Macaé	19
3629	2016-02-18 00:00:00	Macuco	19
3630	2016-02-18 00:00:00	Magé	19
3631	2016-02-18 00:00:00	Mangaratiba	19
3632	2016-02-18 00:00:00	Maricá	19
3633	2016-02-18 00:00:00	Mendes	19
3634	2016-02-18 00:00:00	Mesquita	19
3635	2016-02-18 00:00:00	Miguel Pereira	19
3636	2016-02-18 00:00:00	Miracema	19
3637	2016-02-18 00:00:00	Natividade	19
3638	2016-02-18 00:00:00	Nilópolis	19
3639	2016-02-18 00:00:00	Niterói	19
3640	2016-02-18 00:00:00	Nova Friburgo	19
3641	2016-02-18 00:00:00	Nova Iguaçu	19
3642	2016-02-18 00:00:00	Paracambi	19
3643	2016-02-18 00:00:00	Paraíba do Sul	19
3644	2016-02-18 00:00:00	Paraty	19
3645	2016-02-18 00:00:00	Paty do Alferes	19
3646	2016-02-18 00:00:00	Petrópolis	19
3647	2016-02-18 00:00:00	Pinheiral	19
3648	2016-02-18 00:00:00	Piraí	19
3649	2016-02-18 00:00:00	Porciúncula	19
3650	2016-02-18 00:00:00	Porto Real	19
3651	2016-02-18 00:00:00	Quatis	19
3652	2016-02-18 00:00:00	Queimados	19
3653	2016-02-18 00:00:00	Quissamã	19
3654	2016-02-18 00:00:00	Resende	19
3655	2016-02-18 00:00:00	Rio Bonito	19
3656	2016-02-18 00:00:00	Rio Claro	19
3657	2016-02-18 00:00:00	Rio das Flores	19
3658	2016-02-18 00:00:00	Rio das Ostras	19
3659	2016-02-18 00:00:00	Rio de Janeiro	19
3660	2016-02-18 00:00:00	Santa Maria Madalena	19
3661	2016-02-18 00:00:00	Santo Antônio de Pádua	19
3662	2016-02-18 00:00:00	São Fidélis	19
3663	2016-02-18 00:00:00	São Francisco de Itabapoana	19
3664	2016-02-18 00:00:00	São Gonçalo	19
3665	2016-02-18 00:00:00	São João da Barra	19
3666	2016-02-18 00:00:00	São João de Meriti	19
3667	2016-02-18 00:00:00	São José de Ubá	19
3668	2016-02-18 00:00:00	São José do Vale do Rio Preto	19
3669	2016-02-18 00:00:00	São Pedro da Aldeia	19
3670	2016-02-18 00:00:00	São Sebastião do Alto	19
3672	2016-02-18 00:00:00	Saquarema	19
3673	2016-02-18 00:00:00	Seropédica	19
3674	2016-02-18 00:00:00	Silva Jardim	19
3675	2016-02-18 00:00:00	Sumidouro	19
3676	2016-02-18 00:00:00	Tanguá	19
3677	2016-02-18 00:00:00	Teresópolis	19
3678	2016-02-18 00:00:00	Trajano de Moraes	19
3679	2016-02-18 00:00:00	Três Rios	19
3680	2016-02-18 00:00:00	Valença	19
3681	2016-02-18 00:00:00	Varre-Sai	19
3682	2016-02-18 00:00:00	Vassouras	19
3683	2016-02-18 00:00:00	Volta Redonda	19
3684	2016-02-18 00:00:00	Acari	20
3685	2016-02-18 00:00:00	Açu	20
3686	2016-02-18 00:00:00	Afonso Bezerra	20
3687	2016-02-18 00:00:00	Água Nova	20
3688	2016-02-18 00:00:00	Alexandria	20
3689	2016-02-18 00:00:00	Almino Afonso	20
3690	2016-02-18 00:00:00	Alto do Rodrigues	20
3691	2016-02-18 00:00:00	Angicos	20
3692	2016-02-18 00:00:00	Antônio Martins	20
3693	2016-02-18 00:00:00	Apodi	20
3694	2016-02-18 00:00:00	Areia Branca	20
3695	2016-02-18 00:00:00	Arês	20
3696	2016-02-18 00:00:00	Augusto Severo	20
3697	2016-02-18 00:00:00	Baía Formosa	20
3698	2016-02-18 00:00:00	Baraúna	20
3699	2016-02-18 00:00:00	Barcelona	20
3700	2016-02-18 00:00:00	Bento Fernandes	20
3701	2016-02-18 00:00:00	Bodó	20
3702	2016-02-18 00:00:00	Bom Jesus	20
3703	2016-02-18 00:00:00	Brejinho	20
3704	2016-02-18 00:00:00	Caiçara do Norte	20
3705	2016-02-18 00:00:00	Caiçara do Rio do Vento	20
3706	2016-02-18 00:00:00	Caicó	20
3707	2016-02-18 00:00:00	Campo Redondo	20
3708	2016-02-18 00:00:00	Canguaretama	20
3709	2016-02-18 00:00:00	Caraúbas	20
3710	2016-02-18 00:00:00	Carnaúba dos Dantas	20
3711	2016-02-18 00:00:00	Carnaubais	20
3712	2016-02-18 00:00:00	Ceará-Mirim	20
3713	2016-02-18 00:00:00	Cerro Corá	20
3714	2016-02-18 00:00:00	Coronel Ezequiel	20
3715	2016-02-18 00:00:00	Coronel João Pessoa	20
3716	2016-02-18 00:00:00	Cruzeta	20
3717	2016-02-18 00:00:00	Currais Novos	20
3718	2016-02-18 00:00:00	Doutor Severiano	20
3719	2016-02-18 00:00:00	Encanto	20
3720	2016-02-18 00:00:00	Equador	20
3721	2016-02-18 00:00:00	Espírito Santo	20
3722	2016-02-18 00:00:00	Extremoz	20
3723	2016-02-18 00:00:00	Felipe Guerra	20
3724	2016-02-18 00:00:00	Fernando Pedroza	20
3725	2016-02-18 00:00:00	Florânia	20
3726	2016-02-18 00:00:00	Francisco Dantas	20
3727	2016-02-18 00:00:00	Frutuoso Gomes	20
3728	2016-02-18 00:00:00	Galinhos	20
3729	2016-02-18 00:00:00	Goianinha	20
3730	2016-02-18 00:00:00	Governador Dix-Sept Rosado	20
3731	2016-02-18 00:00:00	Grossos	20
3732	2016-02-18 00:00:00	Guamaré	20
3733	2016-02-18 00:00:00	Ielmo Marinho	20
3734	2016-02-18 00:00:00	Ipanguaçu	20
3735	2016-02-18 00:00:00	Ipueira	20
3736	2016-02-18 00:00:00	Itajá	20
3737	2016-02-18 00:00:00	Itaú	20
3738	2016-02-18 00:00:00	Jaçanã	20
3739	2016-02-18 00:00:00	Jandaíra	20
3740	2016-02-18 00:00:00	Janduís	20
3741	2016-02-18 00:00:00	Januário Cicco	20
3742	2016-02-18 00:00:00	Japi	20
3743	2016-02-18 00:00:00	Jardim de Angicos	20
3744	2016-02-18 00:00:00	Jardim de Piranhas	20
3745	2016-02-18 00:00:00	Jardim do Seridó	20
3746	2016-02-18 00:00:00	João Câmara	20
3747	2016-02-18 00:00:00	João Dias	20
3748	2016-02-18 00:00:00	José da Penha	20
3749	2016-02-18 00:00:00	Jucurutu	20
3750	2016-02-18 00:00:00	Jundiá	20
3751	2016-02-18 00:00:00	Lagoa d'Anta	20
3752	2016-02-18 00:00:00	Lagoa de Pedras	20
3753	2016-02-18 00:00:00	Lagoa de Velhos	20
3754	2016-02-18 00:00:00	Lagoa Nova	20
3755	2016-02-18 00:00:00	Lagoa Salgada	20
3756	2016-02-18 00:00:00	Lajes	20
3757	2016-02-18 00:00:00	Lajes Pintadas	20
3758	2016-02-18 00:00:00	Lucrécia	20
3759	2016-02-18 00:00:00	Luís Gomes	20
3760	2016-02-18 00:00:00	Macaíba	20
3761	2016-02-18 00:00:00	Macau	20
3762	2016-02-18 00:00:00	Major Sales	20
3763	2016-02-18 00:00:00	Marcelino Vieira	20
3764	2016-02-18 00:00:00	Martins	20
3765	2016-02-18 00:00:00	Maxaranguape	20
3766	2016-02-18 00:00:00	Messias Targino	20
3767	2016-02-18 00:00:00	Montanhas	20
3768	2016-02-18 00:00:00	Monte Alegre	20
3769	2016-02-18 00:00:00	Monte das Gameleiras	20
3770	2016-02-18 00:00:00	Mossoró	20
3771	2016-02-18 00:00:00	Natal	20
3772	2016-02-18 00:00:00	Nísia Floresta	20
3773	2016-02-18 00:00:00	Nova Cruz	20
3774	2016-02-18 00:00:00	Olho-d'Água do Borges	20
3775	2016-02-18 00:00:00	Ouro Branco	20
3776	2016-02-18 00:00:00	Paraná	20
3777	2016-02-18 00:00:00	Paraú	20
3778	2016-02-18 00:00:00	Parazinho	20
3779	2016-02-18 00:00:00	Parelhas	20
3780	2016-02-18 00:00:00	Parnamirim	20
3781	2016-02-18 00:00:00	Passa e Fica	20
3782	2016-02-18 00:00:00	Passagem	20
3783	2016-02-18 00:00:00	Patu	20
3784	2016-02-18 00:00:00	Pau dos Ferros	20
3785	2016-02-18 00:00:00	Pedra Grande	20
3786	2016-02-18 00:00:00	Pedra Preta	20
3787	2016-02-18 00:00:00	Pedro Avelino	20
3788	2016-02-18 00:00:00	Pedro Velho	20
3789	2016-02-18 00:00:00	Pendências	20
3790	2016-02-18 00:00:00	Pilões	20
3791	2016-02-18 00:00:00	Poço Branco	20
3792	2016-02-18 00:00:00	Portalegre	20
3793	2016-02-18 00:00:00	Porto do Mangue	20
3794	2016-02-18 00:00:00	Presidente Juscelino	20
3795	2016-02-18 00:00:00	Pureza	20
3796	2016-02-18 00:00:00	Rafael Fernandes	20
3797	2016-02-18 00:00:00	Rafael Godeiro	20
3798	2016-02-18 00:00:00	Riacho da Cruz	20
3799	2016-02-18 00:00:00	Riacho de Santana	20
3800	2016-02-18 00:00:00	Riachuelo	20
3801	2016-02-18 00:00:00	Rio do Fogo	20
3802	2016-02-18 00:00:00	Rodolfo Fernandes	20
3803	2016-02-18 00:00:00	Ruy Barbosa	20
3804	2016-02-18 00:00:00	Santa Cruz	20
3805	2016-02-18 00:00:00	Santa Maria	20
3806	2016-02-18 00:00:00	Santana do Matos	20
3807	2016-02-18 00:00:00	Santana do Seridó	20
3808	2016-02-18 00:00:00	Santo Antônio	20
3809	2016-02-18 00:00:00	São Bento do Norte	20
3810	2016-02-18 00:00:00	São Bento do Trairí	20
3811	2016-02-18 00:00:00	São Fernando	20
3812	2016-02-18 00:00:00	São Francisco do Oeste	20
3813	2016-02-18 00:00:00	São Gonçalo do Amarante	20
3814	2016-02-18 00:00:00	São João do Sabugi	20
3815	2016-02-18 00:00:00	São José de Mipibu	20
3816	2016-02-18 00:00:00	São José do Campestre	20
3817	2016-02-18 00:00:00	São José do Seridó	20
3818	2016-02-18 00:00:00	São Miguel	20
3819	2016-02-18 00:00:00	São Miguel do Gostoso	20
3820	2016-02-18 00:00:00	São Paulo do Potengi	20
3821	2016-02-18 00:00:00	São Pedro	20
3822	2016-02-18 00:00:00	São Rafael	20
3823	2016-02-18 00:00:00	São Tomé	20
3824	2016-02-18 00:00:00	São Vicente	20
3825	2016-02-18 00:00:00	Senador Elói de Souza	20
3826	2016-02-18 00:00:00	Senador Georgino Avelino	20
3827	2016-02-18 00:00:00	Serra de São Bento	20
3828	2016-02-18 00:00:00	Serra do Mel	20
3829	2016-02-18 00:00:00	Serra Negra do Norte	20
3830	2016-02-18 00:00:00	Serrinha	20
3831	2016-02-18 00:00:00	Serrinha dos Pintos	20
3832	2016-02-18 00:00:00	Severiano Melo	20
3833	2016-02-18 00:00:00	Sítio Novo	20
3834	2016-02-18 00:00:00	Taboleiro Grande	20
3835	2016-02-18 00:00:00	Taipu	20
3836	2016-02-18 00:00:00	Tangará	20
3837	2016-02-18 00:00:00	Tenente Ananias	20
3838	2016-02-18 00:00:00	Tenente Laurentino Cruz	20
3839	2016-02-18 00:00:00	Tibau	20
3840	2016-02-18 00:00:00	Tibau do Sul	20
3841	2016-02-18 00:00:00	Timbaúba dos Batistas	20
3842	2016-02-18 00:00:00	Touros	20
3843	2016-02-18 00:00:00	Triunfo Potiguar	20
3844	2016-02-18 00:00:00	Umarizal	20
3845	2016-02-18 00:00:00	Upanema	20
3846	2016-02-18 00:00:00	Várzea	20
3847	2016-02-18 00:00:00	Venha-Ver	20
3848	2016-02-18 00:00:00	Vera Cruz	20
3849	2016-02-18 00:00:00	Viçosa	20
3850	2016-02-18 00:00:00	Vila Flor	20
3851	2016-02-18 00:00:00	Aceguá	23
3852	2016-02-18 00:00:00	Água Santa	23
3853	2016-02-18 00:00:00	Agudo	23
3854	2016-02-18 00:00:00	Ajuricaba	23
3855	2016-02-18 00:00:00	Alecrim	23
3856	2016-02-18 00:00:00	Alegrete	23
3857	2016-02-18 00:00:00	Alegria	23
3858	2016-02-18 00:00:00	Almirante Tamandaré do Sul	23
3859	2016-02-18 00:00:00	Alpestre	23
3860	2016-02-18 00:00:00	Alto Alegre	23
3861	2016-02-18 00:00:00	Alto Feliz	23
3862	2016-02-18 00:00:00	Alvorada	23
3863	2016-02-18 00:00:00	Amaral Ferrador	23
3864	2016-02-18 00:00:00	Ametista do Sul	23
3865	2016-02-18 00:00:00	André da Rocha	23
3866	2016-02-18 00:00:00	Anta Gorda	23
3867	2016-02-18 00:00:00	Antônio Prado	23
3868	2016-02-18 00:00:00	Arambaré	23
3869	2016-02-18 00:00:00	Araricá	23
3870	2016-02-18 00:00:00	Aratiba	23
3871	2016-02-18 00:00:00	Arroio do Meio	23
3872	2016-02-18 00:00:00	Arroio do Padre	23
3873	2016-02-18 00:00:00	Arroio do Sal	23
3874	2016-02-18 00:00:00	Arroio do Tigre	23
3875	2016-02-18 00:00:00	Arroio dos Ratos	23
3876	2016-02-18 00:00:00	Arroio Grande	23
3877	2016-02-18 00:00:00	Arvorezinha	23
3878	2016-02-18 00:00:00	Augusto Pestana	23
3879	2016-02-18 00:00:00	Áurea	23
3880	2016-02-18 00:00:00	Bagé	23
3881	2016-02-18 00:00:00	Balneário Pinhal	23
3882	2016-02-18 00:00:00	Barão	23
3883	2016-02-18 00:00:00	Barão de Cotegipe	23
3884	2016-02-18 00:00:00	Barão do Triunfo	23
3885	2016-02-18 00:00:00	Barra do Guarita	23
3886	2016-02-18 00:00:00	Barra do Quaraí	23
3887	2016-02-18 00:00:00	Barra do Ribeiro	23
3888	2016-02-18 00:00:00	Barra do Rio Azul	23
3889	2016-02-18 00:00:00	Barra Funda	23
3890	2016-02-18 00:00:00	Barracão	23
3891	2016-02-18 00:00:00	Barros Cassal	23
3892	2016-02-18 00:00:00	Benjamin Constant do Sul	23
3893	2016-02-18 00:00:00	Bento Gonçalves	23
3894	2016-02-18 00:00:00	Boa Vista das Missões	23
3895	2016-02-18 00:00:00	Boa Vista do Buricá	23
3896	2016-02-18 00:00:00	Boa Vista do Cadeado	23
3897	2016-02-18 00:00:00	Boa Vista do Incra	23
3898	2016-02-18 00:00:00	Boa Vista do Sul	23
3899	2016-02-18 00:00:00	Bom Jesus	23
3900	2016-02-18 00:00:00	Bom Princípio	23
3901	2016-02-18 00:00:00	Bom Progresso	23
3902	2016-02-18 00:00:00	Bom Retiro do Sul	23
3903	2016-02-18 00:00:00	Boqueirão do Leão	23
3904	2016-02-18 00:00:00	Bossoroca	23
3905	2016-02-18 00:00:00	Bozano	23
3906	2016-02-18 00:00:00	Braga	23
3907	2016-02-18 00:00:00	Brochier	23
3908	2016-02-18 00:00:00	Butiá	23
3909	2016-02-18 00:00:00	Caçapava do Sul	23
3910	2016-02-18 00:00:00	Cacequi	23
3911	2016-02-18 00:00:00	Cachoeira do Sul	23
3912	2016-02-18 00:00:00	Cachoeirinha	23
3913	2016-02-18 00:00:00	Cacique Doble	23
3914	2016-02-18 00:00:00	Caibaté	23
3915	2016-02-18 00:00:00	Caiçara	23
3916	2016-02-18 00:00:00	Camaquã	23
3917	2016-02-18 00:00:00	Camargo	23
3918	2016-02-18 00:00:00	Cambará do Sul	23
3919	2016-02-18 00:00:00	Campestre da Serra	23
3920	2016-02-18 00:00:00	Campina das Missões	23
3921	2016-02-18 00:00:00	Campinas do Sul	23
3922	2016-02-18 00:00:00	Campo Bom	23
3923	2016-02-18 00:00:00	Campo Novo	23
3924	2016-02-18 00:00:00	Campos Borges	23
3925	2016-02-18 00:00:00	Candelária	23
3926	2016-02-18 00:00:00	Cândido Godói	23
3927	2016-02-18 00:00:00	Candiota	23
3928	2016-02-18 00:00:00	Canela	23
3929	2016-02-18 00:00:00	Canguçu	23
3930	2016-02-18 00:00:00	Canoas	23
3931	2016-02-18 00:00:00	Canudos do Vale	23
3932	2016-02-18 00:00:00	Capão Bonito do Sul	23
3933	2016-02-18 00:00:00	Capão da Canoa	23
3934	2016-02-18 00:00:00	Capão do Cipó	23
3935	2016-02-18 00:00:00	Capão do Leão	23
3936	2016-02-18 00:00:00	Capela de Santana	23
3937	2016-02-18 00:00:00	Capitão	23
3938	2016-02-18 00:00:00	Capivari do Sul	23
3939	2016-02-18 00:00:00	Caraá	23
3940	2016-02-18 00:00:00	Carazinho	23
3941	2016-02-18 00:00:00	Carlos Barbosa	23
3942	2016-02-18 00:00:00	Carlos Gomes	23
3943	2016-02-18 00:00:00	Casca	23
3944	2016-02-18 00:00:00	Caseiros	23
3945	2016-02-18 00:00:00	Catuípe	23
3946	2016-02-18 00:00:00	Caxias do Sul	23
3947	2016-02-18 00:00:00	Centenário	23
3948	2016-02-18 00:00:00	Cerrito	23
3949	2016-02-18 00:00:00	Cerro Branco	23
3950	2016-02-18 00:00:00	Cerro Grande	23
3951	2016-02-18 00:00:00	Cerro Grande do Sul	23
3952	2016-02-18 00:00:00	Cerro Largo	23
3953	2016-02-18 00:00:00	Chapada	23
3954	2016-02-18 00:00:00	Charqueadas	23
3955	2016-02-18 00:00:00	Charrua	23
3956	2016-02-18 00:00:00	Chiapetta	23
3957	2016-02-18 00:00:00	Chuí	23
3958	2016-02-18 00:00:00	Chuvisca	23
3959	2016-02-18 00:00:00	Cidreira	23
3960	2016-02-18 00:00:00	Ciríaco	23
3961	2016-02-18 00:00:00	Colinas	23
3962	2016-02-18 00:00:00	Colorado	23
3963	2016-02-18 00:00:00	Condor	23
3964	2016-02-18 00:00:00	Constantina	23
3965	2016-02-18 00:00:00	Coqueiro Baixo	23
3966	2016-02-18 00:00:00	Coqueiros do Sul	23
3967	2016-02-18 00:00:00	Coronel Barros	23
3968	2016-02-18 00:00:00	Coronel Bicaco	23
3969	2016-02-18 00:00:00	Coronel Pilar	23
3970	2016-02-18 00:00:00	Cotiporã	23
3971	2016-02-18 00:00:00	Coxilha	23
3972	2016-02-18 00:00:00	Crissiumal	23
3973	2016-02-18 00:00:00	Cristal	23
3974	2016-02-18 00:00:00	Cristal do Sul	23
3975	2016-02-18 00:00:00	Cruz Alta	23
3976	2016-02-18 00:00:00	Cruzaltense	23
3977	2016-02-18 00:00:00	Cruzeiro do Sul	23
3978	2016-02-18 00:00:00	David Canabarro	23
3979	2016-02-18 00:00:00	Derrubadas	23
3980	2016-02-18 00:00:00	Dezesseis de Novembro	23
3981	2016-02-18 00:00:00	Dilermando de Aguiar	23
3982	2016-02-18 00:00:00	Dois Irmãos	23
3983	2016-02-18 00:00:00	Dois Irmãos das Missões	23
3984	2016-02-18 00:00:00	Dois Lajeados	23
3985	2016-02-18 00:00:00	Dom Feliciano	23
3986	2016-02-18 00:00:00	Dom Pedrito	23
3987	2016-02-18 00:00:00	Dom Pedro de Alcântara	23
3988	2016-02-18 00:00:00	Dona Francisca	23
3989	2016-02-18 00:00:00	Doutor Maurício Cardoso	23
3990	2016-02-18 00:00:00	Doutor Ricardo	23
3991	2016-02-18 00:00:00	Eldorado do Sul	23
3992	2016-02-18 00:00:00	Encantado	23
3993	2016-02-18 00:00:00	Encruzilhada do Sul	23
3994	2016-02-18 00:00:00	Engenho Velho	23
3995	2016-02-18 00:00:00	Entre Rios do Sul	23
3996	2016-02-18 00:00:00	Entre-Ijuís	23
3997	2016-02-18 00:00:00	Erebango	23
3998	2016-02-18 00:00:00	Erechim	23
3999	2016-02-18 00:00:00	Ernestina	23
4000	2016-02-18 00:00:00	Erval Grande	23
4001	2016-02-18 00:00:00	Erval Seco	23
4002	2016-02-18 00:00:00	Esmeralda	23
4003	2016-02-18 00:00:00	Esperança do Sul	23
4004	2016-02-18 00:00:00	Espumoso	23
4005	2016-02-18 00:00:00	Estação	23
4006	2016-02-18 00:00:00	Estância Velha	23
4007	2016-02-18 00:00:00	Esteio	23
4008	2016-02-18 00:00:00	Estrela	23
4009	2016-02-18 00:00:00	Estrela Velha	23
4010	2016-02-18 00:00:00	Eugênio de Castro	23
4011	2016-02-18 00:00:00	Fagundes Varela	23
4012	2016-02-18 00:00:00	Farroupilha	23
4013	2016-02-18 00:00:00	Faxinal do Soturno	23
4014	2016-02-18 00:00:00	Faxinalzinho	23
4015	2016-02-18 00:00:00	Fazenda Vilanova	23
4016	2016-02-18 00:00:00	Feliz	23
4017	2016-02-18 00:00:00	Flores da Cunha	23
4018	2016-02-18 00:00:00	Floriano Peixoto	23
4019	2016-02-18 00:00:00	Fontoura Xavier	23
4020	2016-02-18 00:00:00	Formigueiro	23
4021	2016-02-18 00:00:00	Forquetinha	23
4022	2016-02-18 00:00:00	Fortaleza dos Valos	23
4023	2016-02-18 00:00:00	Frederico Westphalen	23
4024	2016-02-18 00:00:00	Garibaldi	23
4025	2016-02-18 00:00:00	Garruchos	23
4026	2016-02-18 00:00:00	Gaurama	23
4027	2016-02-18 00:00:00	General Câmara	23
4028	2016-02-18 00:00:00	Gentil	23
4029	2016-02-18 00:00:00	Getúlio Vargas	23
4030	2016-02-18 00:00:00	Giruá	23
4031	2016-02-18 00:00:00	Glorinha	23
4032	2016-02-18 00:00:00	Gramado	23
4033	2016-02-18 00:00:00	Gramado dos Loureiros	23
4034	2016-02-18 00:00:00	Gramado Xavier	23
4035	2016-02-18 00:00:00	Gravataí	23
4036	2016-02-18 00:00:00	Guabiju	23
4037	2016-02-18 00:00:00	Guaíba	23
4038	2016-02-18 00:00:00	Guaporé	23
4039	2016-02-18 00:00:00	Guarani das Missões	23
4040	2016-02-18 00:00:00	Harmonia	23
4041	2016-02-18 00:00:00	Herval	23
4042	2016-02-18 00:00:00	Herveiras	23
4043	2016-02-18 00:00:00	Horizontina	23
4044	2016-02-18 00:00:00	Hulha Negra	23
4045	2016-02-18 00:00:00	Humaitá	23
4046	2016-02-18 00:00:00	Ibarama	23
4047	2016-02-18 00:00:00	Ibiaçá	23
4048	2016-02-18 00:00:00	Ibiraiaras	23
4049	2016-02-18 00:00:00	Ibirapuitã	23
4050	2016-02-18 00:00:00	Ibirubá	23
4051	2016-02-18 00:00:00	Igrejinha	23
4052	2016-02-18 00:00:00	Ijuí	23
4053	2016-02-18 00:00:00	Ilópolis	23
4054	2016-02-18 00:00:00	Imbé	23
4055	2016-02-18 00:00:00	Imigrante	23
4056	2016-02-18 00:00:00	Independência	23
4057	2016-02-18 00:00:00	Inhacorá	23
4058	2016-02-18 00:00:00	Ipê	23
4059	2016-02-18 00:00:00	Ipiranga do Sul	23
4060	2016-02-18 00:00:00	Iraí	23
4061	2016-02-18 00:00:00	Itaara	23
4062	2016-02-18 00:00:00	Itacurubi	23
4063	2016-02-18 00:00:00	Itapuca	23
4064	2016-02-18 00:00:00	Itaqui	23
4065	2016-02-18 00:00:00	Itati	23
4066	2016-02-18 00:00:00	Itatiba do Sul	23
4067	2016-02-18 00:00:00	Ivorá	23
4068	2016-02-18 00:00:00	Ivoti	23
4069	2016-02-18 00:00:00	Jaboticaba	23
4070	2016-02-18 00:00:00	Jacuizinho	23
4071	2016-02-18 00:00:00	Jacutinga	23
4072	2016-02-18 00:00:00	Jaguarão	23
4073	2016-02-18 00:00:00	Jaguari	23
4074	2016-02-18 00:00:00	Jaquirana	23
4075	2016-02-18 00:00:00	Jari	23
4076	2016-02-18 00:00:00	Jóia	23
4077	2016-02-18 00:00:00	Júlio de Castilhos	23
4078	2016-02-18 00:00:00	Lagoa Bonita do Sul	23
4079	2016-02-18 00:00:00	Lagoa dos Três Cantos	23
4080	2016-02-18 00:00:00	Lagoa Vermelha	23
4081	2016-02-18 00:00:00	Lagoão	23
4082	2016-02-18 00:00:00	Lajeado	23
4083	2016-02-18 00:00:00	Lajeado do Bugre	23
4084	2016-02-18 00:00:00	Lavras do Sul	23
4085	2016-02-18 00:00:00	Liberato Salzano	23
4086	2016-02-18 00:00:00	Lindolfo Collor	23
4087	2016-02-18 00:00:00	Linha Nova	23
4088	2016-02-18 00:00:00	Maçambará	23
4089	2016-02-18 00:00:00	Machadinho	23
4090	2016-02-18 00:00:00	Mampituba	23
4091	2016-02-18 00:00:00	Manoel Viana	23
4092	2016-02-18 00:00:00	Maquiné	23
4093	2016-02-18 00:00:00	Maratá	23
4094	2016-02-18 00:00:00	Marau	23
4095	2016-02-18 00:00:00	Marcelino Ramos	23
4096	2016-02-18 00:00:00	Mariana Pimentel	23
4097	2016-02-18 00:00:00	Mariano Moro	23
4098	2016-02-18 00:00:00	Marques de Souza	23
4099	2016-02-18 00:00:00	Mata	23
4100	2016-02-18 00:00:00	Mato Castelhano	23
4101	2016-02-18 00:00:00	Mato Leitão	23
4102	2016-02-18 00:00:00	Mato Queimado	23
4103	2016-02-18 00:00:00	Maximiliano de Almeida	23
4104	2016-02-18 00:00:00	Minas do Leão	23
4105	2016-02-18 00:00:00	Miraguaí	23
4106	2016-02-18 00:00:00	Montauri	23
4107	2016-02-18 00:00:00	Monte Alegre dos Campos	23
4108	2016-02-18 00:00:00	Monte Belo do Sul	23
4109	2016-02-18 00:00:00	Montenegro	23
4110	2016-02-18 00:00:00	Mormaço	23
4111	2016-02-18 00:00:00	Morrinhos do Sul	23
4112	2016-02-18 00:00:00	Morro Redondo	23
4113	2016-02-18 00:00:00	Morro Reuter	23
4114	2016-02-18 00:00:00	Mostardas	23
4115	2016-02-18 00:00:00	Muçum	23
4116	2016-02-18 00:00:00	Muitos Capões	23
4117	2016-02-18 00:00:00	Muliterno	23
4118	2016-02-18 00:00:00	Não-Me-Toque	23
4119	2016-02-18 00:00:00	Nicolau Vergueiro	23
4120	2016-02-18 00:00:00	Nonoai	23
4121	2016-02-18 00:00:00	Nova Alvorada	23
4122	2016-02-18 00:00:00	Nova Araçá	23
4123	2016-02-18 00:00:00	Nova Bassano	23
4124	2016-02-18 00:00:00	Nova Boa Vista	23
4125	2016-02-18 00:00:00	Nova Bréscia	23
4126	2016-02-18 00:00:00	Nova Candelária	23
4127	2016-02-18 00:00:00	Nova Esperança do Sul	23
4128	2016-02-18 00:00:00	Nova Hartz	23
4129	2016-02-18 00:00:00	Nova Pádua	23
4130	2016-02-18 00:00:00	Nova Palma	23
4131	2016-02-18 00:00:00	Nova Petrópolis	23
4132	2016-02-18 00:00:00	Nova Prata	23
4133	2016-02-18 00:00:00	Nova Ramada	23
4134	2016-02-18 00:00:00	Nova Roma do Sul	23
4135	2016-02-18 00:00:00	Nova Santa Rita	23
4136	2016-02-18 00:00:00	Novo Barreiro	23
4137	2016-02-18 00:00:00	Novo Cabrais	23
4138	2016-02-18 00:00:00	Novo Hamburgo	23
4139	2016-02-18 00:00:00	Novo Machado	23
4140	2016-02-18 00:00:00	Novo Tiradentes	23
4141	2016-02-18 00:00:00	Novo Xingu	23
4142	2016-02-18 00:00:00	Osório	23
4143	2016-02-18 00:00:00	Paim Filho	23
4144	2016-02-18 00:00:00	Palmares do Sul	23
4145	2016-02-18 00:00:00	Palmeira das Missões	23
4146	2016-02-18 00:00:00	Palmitinho	23
4147	2016-02-18 00:00:00	Panambi	23
4148	2016-02-18 00:00:00	Pantano Grande	23
4149	2016-02-18 00:00:00	Paraí	23
4150	2016-02-18 00:00:00	Paraíso do Sul	23
4151	2016-02-18 00:00:00	Pareci Novo	23
4152	2016-02-18 00:00:00	Parobé	23
4153	2016-02-18 00:00:00	Passa Sete	23
4154	2016-02-18 00:00:00	Passo do Sobrado	23
4155	2016-02-18 00:00:00	Passo Fundo	23
4156	2016-02-18 00:00:00	Paulo Bento	23
4157	2016-02-18 00:00:00	Paverama	23
4158	2016-02-18 00:00:00	Pedras Altas	23
4159	2016-02-18 00:00:00	Pedro Osório	23
4160	2016-02-18 00:00:00	Pejuçara	23
4161	2016-02-18 00:00:00	Pelotas	23
4162	2016-02-18 00:00:00	Picada Café	23
4163	2016-02-18 00:00:00	Pinhal	23
4164	2016-02-18 00:00:00	Pinhal da Serra	23
4165	2016-02-18 00:00:00	Pinhal Grande	23
4166	2016-02-18 00:00:00	Pinheirinho do Vale	23
4167	2016-02-18 00:00:00	Pinheiro Machado	23
4168	2016-02-18 00:00:00	Pirapó	23
4169	2016-02-18 00:00:00	Piratini	23
4170	2016-02-18 00:00:00	Planalto	23
4171	2016-02-18 00:00:00	Poço das Antas	23
4172	2016-02-18 00:00:00	Pontão	23
4173	2016-02-18 00:00:00	Ponte Preta	23
4174	2016-02-18 00:00:00	Portão	23
4175	2016-02-18 00:00:00	Porto Alegre	23
4176	2016-02-18 00:00:00	Porto Lucena	23
4177	2016-02-18 00:00:00	Porto Mauá	23
4178	2016-02-18 00:00:00	Porto Vera Cruz	23
4179	2016-02-18 00:00:00	Porto Xavier	23
4180	2016-02-18 00:00:00	Pouso Novo	23
4181	2016-02-18 00:00:00	Presidente Lucena	23
4182	2016-02-18 00:00:00	Progresso	23
4183	2016-02-18 00:00:00	Protásio Alves	23
4184	2016-02-18 00:00:00	Putinga	23
4185	2016-02-18 00:00:00	Quaraí	23
4186	2016-02-18 00:00:00	Quatro Irmãos	23
4187	2016-02-18 00:00:00	Quevedos	23
4188	2016-02-18 00:00:00	Quinze de Novembro	23
4189	2016-02-18 00:00:00	Redentora	23
4190	2016-02-18 00:00:00	Relvado	23
4191	2016-02-18 00:00:00	Restinga Seca	23
4192	2016-02-18 00:00:00	Rio dos Índios	23
4193	2016-02-18 00:00:00	Rio Grande	23
4194	2016-02-18 00:00:00	Rio Pardo	23
4195	2016-02-18 00:00:00	Riozinho	23
4196	2016-02-18 00:00:00	Roca Sales	23
4197	2016-02-18 00:00:00	Rodeio Bonito	23
4198	2016-02-18 00:00:00	Rolador	23
4199	2016-02-18 00:00:00	Rolante	23
4200	2016-02-18 00:00:00	Ronda Alta	23
4201	2016-02-18 00:00:00	Rondinha	23
4202	2016-02-18 00:00:00	Roque Gonzales	23
4203	2016-02-18 00:00:00	Rosário do Sul	23
4204	2016-02-18 00:00:00	Sagrada Família	23
4205	2016-02-18 00:00:00	Saldanha Marinho	23
4206	2016-02-18 00:00:00	Salto do Jacuí	23
4207	2016-02-18 00:00:00	Salvador das Missões	23
4208	2016-02-18 00:00:00	Salvador do Sul	23
4209	2016-02-18 00:00:00	Sananduva	23
4210	2016-02-18 00:00:00	Sant'Ana do Livramento	23
4211	2016-02-18 00:00:00	Santa Bárbara do Sul	23
4212	2016-02-18 00:00:00	Santa Cecília do Sul	23
4213	2016-02-18 00:00:00	Santa Clara do Sul	23
4214	2016-02-18 00:00:00	Santa Cruz do Sul	23
4215	2016-02-18 00:00:00	Santa Margarida do Sul	23
4216	2016-02-18 00:00:00	Santa Maria	23
4217	2016-02-18 00:00:00	Santa Maria do Herval	23
4218	2016-02-18 00:00:00	Santa Rosa	23
4219	2016-02-18 00:00:00	Santa Tereza	23
4220	2016-02-18 00:00:00	Santa Vitória do Palmar	23
4221	2016-02-18 00:00:00	Santana da Boa Vista	23
4222	2016-02-18 00:00:00	Santiago	23
4223	2016-02-18 00:00:00	Santo Ângelo	23
4224	2016-02-18 00:00:00	Santo Antônio da Patrulha	23
4225	2016-02-18 00:00:00	Santo Antônio das Missões	23
4226	2016-02-18 00:00:00	Santo Antônio do Palma	23
4227	2016-02-18 00:00:00	Santo Antônio do Planalto	23
4228	2016-02-18 00:00:00	Santo Augusto	23
4229	2016-02-18 00:00:00	Santo Cristo	23
4230	2016-02-18 00:00:00	Santo Expedito do Sul	23
4231	2016-02-18 00:00:00	São Borja	23
4232	2016-02-18 00:00:00	São Domingos do Sul	23
4233	2016-02-18 00:00:00	São Francisco de Assis	23
4234	2016-02-18 00:00:00	São Francisco de Paula	23
4235	2016-02-18 00:00:00	São Gabriel	23
4236	2016-02-18 00:00:00	São Jerônimo	23
4237	2016-02-18 00:00:00	São João da Urtiga	23
4238	2016-02-18 00:00:00	São João do Polêsine	23
4239	2016-02-18 00:00:00	São Jorge	23
4240	2016-02-18 00:00:00	São José das Missões	23
4241	2016-02-18 00:00:00	São José do Herval	23
4242	2016-02-18 00:00:00	São José do Hortêncio	23
4243	2016-02-18 00:00:00	São José do Inhacorá	23
4244	2016-02-18 00:00:00	São José do Norte	23
4245	2016-02-18 00:00:00	São José do Ouro	23
4246	2016-02-18 00:00:00	São José do Sul	23
4247	2016-02-18 00:00:00	São José dos Ausentes	23
4248	2016-02-18 00:00:00	São Leopoldo	23
4249	2016-02-18 00:00:00	São Lourenço do Sul	23
4250	2016-02-18 00:00:00	São Luiz Gonzaga	23
4251	2016-02-18 00:00:00	São Marcos	23
4252	2016-02-18 00:00:00	São Martinho	23
4253	2016-02-18 00:00:00	São Martinho da Serra	23
4254	2016-02-18 00:00:00	São Miguel das Missões	23
4255	2016-02-18 00:00:00	São Nicolau	23
4256	2016-02-18 00:00:00	São Paulo das Missões	23
4257	2016-02-18 00:00:00	São Pedro da Serra	23
4258	2016-02-18 00:00:00	São Pedro das Missões	23
4259	2016-02-18 00:00:00	São Pedro do Butiá	23
4260	2016-02-18 00:00:00	São Pedro do Sul	23
4261	2016-02-18 00:00:00	São Sebastião do Caí	23
4262	2016-02-18 00:00:00	São Sepé	23
4263	2016-02-18 00:00:00	São Valentim	23
4264	2016-02-18 00:00:00	São Valentim do Sul	23
4265	2016-02-18 00:00:00	São Valério do Sul	23
4266	2016-02-18 00:00:00	São Vendelino	23
4267	2016-02-18 00:00:00	São Vicente do Sul	23
4268	2016-02-18 00:00:00	Sapiranga	23
4269	2016-02-18 00:00:00	Sapucaia do Sul	23
4270	2016-02-18 00:00:00	Sarandi	23
4271	2016-02-18 00:00:00	Seberi	23
4272	2016-02-18 00:00:00	Sede Nova	23
4273	2016-02-18 00:00:00	Segredo	23
4274	2016-02-18 00:00:00	Selbach	23
4275	2016-02-18 00:00:00	Senador Salgado Filho	23
4276	2016-02-18 00:00:00	Sentinela do Sul	23
4277	2016-02-18 00:00:00	Serafina Corrêa	23
4278	2016-02-18 00:00:00	Sério	23
4279	2016-02-18 00:00:00	Sertão	23
4280	2016-02-18 00:00:00	Sertão Santana	23
4281	2016-02-18 00:00:00	Sete de Setembro	23
4282	2016-02-18 00:00:00	Severiano de Almeida	23
4283	2016-02-18 00:00:00	Silveira Martins	23
4284	2016-02-18 00:00:00	Sinimbu	23
4285	2016-02-18 00:00:00	Sobradinho	23
4286	2016-02-18 00:00:00	Soledade	23
4287	2016-02-18 00:00:00	Tabaí	23
4288	2016-02-18 00:00:00	Tapejara	23
4289	2016-02-18 00:00:00	Tapera	23
4290	2016-02-18 00:00:00	Tapes	23
4291	2016-02-18 00:00:00	Taquara	23
4292	2016-02-18 00:00:00	Taquari	23
4293	2016-02-18 00:00:00	Taquaruçu do Sul	23
4294	2016-02-18 00:00:00	Tavares	23
4295	2016-02-18 00:00:00	Tenente Portela	23
4296	2016-02-18 00:00:00	Terra de Areia	23
4297	2016-02-18 00:00:00	Teutônia	23
4298	2016-02-18 00:00:00	Tio Hugo	23
4299	2016-02-18 00:00:00	Tiradentes do Sul	23
4300	2016-02-18 00:00:00	Toropi	23
4301	2016-02-18 00:00:00	Torres	23
4302	2016-02-18 00:00:00	Tramandaí	23
4303	2016-02-18 00:00:00	Travesseiro	23
4304	2016-02-18 00:00:00	Três Arroios	23
4305	2016-02-18 00:00:00	Três Cachoeiras	23
4306	2016-02-18 00:00:00	Três Coroas	23
4307	2016-02-18 00:00:00	Três de Maio	23
4308	2016-02-18 00:00:00	Três Forquilhas	23
4309	2016-02-18 00:00:00	Três Palmeiras	23
4310	2016-02-18 00:00:00	Três Passos	23
4311	2016-02-18 00:00:00	Trindade do Sul	23
4312	2016-02-18 00:00:00	Triunfo	23
4313	2016-02-18 00:00:00	Tucunduva	23
4314	2016-02-18 00:00:00	Tunas	23
4315	2016-02-18 00:00:00	Tupanci do Sul	23
4316	2016-02-18 00:00:00	Tupanciretã	23
4317	2016-02-18 00:00:00	Tupandi	23
4318	2016-02-18 00:00:00	Tuparendi	23
4319	2016-02-18 00:00:00	Turuçu	23
4320	2016-02-18 00:00:00	Ubiretama	23
4321	2016-02-18 00:00:00	União da Serra	23
4322	2016-02-18 00:00:00	Unistalda	23
4323	2016-02-18 00:00:00	Uruguaiana	23
4324	2016-02-18 00:00:00	Vacaria	23
4325	2016-02-18 00:00:00	Vale do Sol	23
4326	2016-02-18 00:00:00	Vale Real	23
4327	2016-02-18 00:00:00	Vale Verde	23
4328	2016-02-18 00:00:00	Vanini	23
4329	2016-02-18 00:00:00	Venâncio Aires	23
4330	2016-02-18 00:00:00	Vera Cruz	23
4331	2016-02-18 00:00:00	Veranópolis	23
4332	2016-02-18 00:00:00	Vespasiano Correa	23
4333	2016-02-18 00:00:00	Viadutos	23
4334	2016-02-18 00:00:00	Viamão	23
4335	2016-02-18 00:00:00	Vicente Dutra	23
4336	2016-02-18 00:00:00	Victor Graeff	23
4337	2016-02-18 00:00:00	Vila Flores	23
4338	2016-02-18 00:00:00	Vila Lângaro	23
4339	2016-02-18 00:00:00	Vila Maria	23
4340	2016-02-18 00:00:00	Vila Nova do Sul	23
4341	2016-02-18 00:00:00	Vista Alegre	23
4342	2016-02-18 00:00:00	Vista Alegre do Prata	23
4343	2016-02-18 00:00:00	Vista Gaúcha	23
4344	2016-02-18 00:00:00	Vitória das Missões	23
4345	2016-02-18 00:00:00	Westfalia	23
4346	2016-02-18 00:00:00	Xangri-lá	23
4347	2016-02-18 00:00:00	Alta Floresta D'Oeste	21
4348	2016-02-18 00:00:00	Alto Alegre dos Parecis	21
4349	2016-02-18 00:00:00	Alto Paraíso	21
4350	2016-02-18 00:00:00	Alvorada D'Oeste	21
4351	2016-02-18 00:00:00	Ariquemes	21
4352	2016-02-18 00:00:00	Buritis	21
4353	2016-02-18 00:00:00	Cabixi	21
4354	2016-02-18 00:00:00	Cacaulândia	21
4355	2016-02-18 00:00:00	Cacoal	21
4356	2016-02-18 00:00:00	Campo Novo de Rondônia	21
4357	2016-02-18 00:00:00	Candeias do Jamari	21
4358	2016-02-18 00:00:00	Castanheiras	21
4359	2016-02-18 00:00:00	Cerejeiras	21
4360	2016-02-18 00:00:00	Chupinguaia	21
4361	2016-02-18 00:00:00	Colorado do Oeste	21
4362	2016-02-18 00:00:00	Corumbiara	21
4363	2016-02-18 00:00:00	Costa Marques	21
4364	2016-02-18 00:00:00	Cujubim	21
4365	2016-02-18 00:00:00	Espigão D'Oeste	21
4366	2016-02-18 00:00:00	Governador Jorge Teixeira	21
4367	2016-02-18 00:00:00	Guajará-Mirim	21
4368	2016-02-18 00:00:00	Itapuã do Oeste	21
4369	2016-02-18 00:00:00	Jaru	21
4370	2016-02-18 00:00:00	Ji-Paraná	21
4371	2016-02-18 00:00:00	Machadinho D'Oeste	21
4372	2016-02-18 00:00:00	Ministro Andreazza	21
4373	2016-02-18 00:00:00	Mirante da Serra	21
4374	2016-02-18 00:00:00	Monte Negro	21
4375	2016-02-18 00:00:00	Nova Brasilândia D'Oeste	21
4376	2016-02-18 00:00:00	Nova Mamoré	21
4377	2016-02-18 00:00:00	Nova União	21
4378	2016-02-18 00:00:00	Novo Horizonte do Oeste	21
4379	2016-02-18 00:00:00	Ouro Preto do Oeste	21
4380	2016-02-18 00:00:00	Parecis	21
4381	2016-02-18 00:00:00	Pimenta Bueno	21
4382	2016-02-18 00:00:00	Pimenteiras do Oeste	21
4383	2016-02-18 00:00:00	Porto Velho	21
4384	2016-02-18 00:00:00	Presidente Médici	21
4385	2016-02-18 00:00:00	Primavera de Rondônia	21
4386	2016-02-18 00:00:00	Rio Crespo	21
4387	2016-02-18 00:00:00	Rolim de Moura	21
4388	2016-02-18 00:00:00	Santa Luzia D'Oeste	21
4389	2016-02-18 00:00:00	São Felipe D'Oeste	21
4390	2016-02-18 00:00:00	São Francisco do Guaporé	21
4391	2016-02-18 00:00:00	São Miguel do Guaporé	21
4392	2016-02-18 00:00:00	Seringueiras	21
4393	2016-02-18 00:00:00	Teixeirópolis	21
4394	2016-02-18 00:00:00	Theobroma	21
4395	2016-02-18 00:00:00	Urupá	21
4396	2016-02-18 00:00:00	Vale do Anari	21
4397	2016-02-18 00:00:00	Vale do Paraíso	21
4398	2016-02-18 00:00:00	Vilhena	21
4399	2016-02-18 00:00:00	Alto Alegre	22
4400	2016-02-18 00:00:00	Amajari	22
4401	2016-02-18 00:00:00	Boa Vista	22
4402	2016-02-18 00:00:00	Bonfim	22
4403	2016-02-18 00:00:00	Cantá	22
4404	2016-02-18 00:00:00	Caracaraí	22
4405	2016-02-18 00:00:00	Caroebe	22
4406	2016-02-18 00:00:00	Iracema	22
4407	2016-02-18 00:00:00	Mucajaí	22
4408	2016-02-18 00:00:00	Normandia	22
4409	2016-02-18 00:00:00	Pacaraima	22
4410	2016-02-18 00:00:00	Rorainópolis	22
4411	2016-02-18 00:00:00	São João da Baliza	22
4412	2016-02-18 00:00:00	São Luiz	22
4413	2016-02-18 00:00:00	Uiramutã	22
4414	2016-02-18 00:00:00	Abdon Batista	24
4415	2016-02-18 00:00:00	Abelardo Luz	24
4416	2016-02-18 00:00:00	Agrolândia	24
4417	2016-02-18 00:00:00	Agronômica	24
4418	2016-02-18 00:00:00	Água Doce	24
4419	2016-02-18 00:00:00	Águas de Chapecó	24
4420	2016-02-18 00:00:00	Águas Frias	24
4421	2016-02-18 00:00:00	Águas Mornas	24
4422	2016-02-18 00:00:00	Alfredo Wagner	24
4423	2016-02-18 00:00:00	Alto Bela Vista	24
4424	2016-02-18 00:00:00	Anchieta	24
4425	2016-02-18 00:00:00	Angelina	24
4426	2016-02-18 00:00:00	Anita Garibaldi	24
4427	2016-02-18 00:00:00	Anitápolis	24
4428	2016-02-18 00:00:00	Antônio Carlos	24
4429	2016-02-18 00:00:00	Apiúna	24
4430	2016-02-18 00:00:00	Arabutã	24
4431	2016-02-18 00:00:00	Araquari	24
4432	2016-02-18 00:00:00	Araranguá	24
4433	2016-02-18 00:00:00	Armazém	24
4434	2016-02-18 00:00:00	Arroio Trinta	24
4435	2016-02-18 00:00:00	Arvoredo	24
4436	2016-02-18 00:00:00	Ascurra	24
4437	2016-02-18 00:00:00	Atalanta	24
4438	2016-02-18 00:00:00	Aurora	24
4439	2016-02-18 00:00:00	Balneário Arroio do Silva	24
4440	2016-02-18 00:00:00	Balneário Barra do Sul	24
4441	2016-02-18 00:00:00	Balneário Camboriú	24
4442	2016-02-18 00:00:00	Balneário Gaivota	24
4443	2016-02-18 00:00:00	Balneário Piçarras	24
4444	2016-02-18 00:00:00	Bandeirante	24
4445	2016-02-18 00:00:00	Barra Bonita	24
4446	2016-02-18 00:00:00	Barra Velha	24
4447	2016-02-18 00:00:00	Bela Vista do Toldo	24
4448	2016-02-18 00:00:00	Belmonte	24
4449	2016-02-18 00:00:00	Benedito Novo	24
4450	2016-02-18 00:00:00	Biguaçu	24
4451	2016-02-18 00:00:00	Blumenau	24
4452	2016-02-18 00:00:00	Bocaina do Sul	24
4453	2016-02-18 00:00:00	Bom Jardim da Serra	24
4454	2016-02-18 00:00:00	Bom Jesus	24
4455	2016-02-18 00:00:00	Bom Jesus do Oeste	24
4456	2016-02-18 00:00:00	Bom Retiro	24
4457	2016-02-18 00:00:00	Bombinhas	24
4458	2016-02-18 00:00:00	Botuverá	24
4459	2016-02-18 00:00:00	Braço do Norte	24
4460	2016-02-18 00:00:00	Braço do Trombudo	24
4461	2016-02-18 00:00:00	Brunópolis	24
4462	2016-02-18 00:00:00	Brusque	24
4463	2016-02-18 00:00:00	Caçador	24
4464	2016-02-18 00:00:00	Caibi	24
4465	2016-02-18 00:00:00	Calmon	24
4466	2016-02-18 00:00:00	Camboriú	24
4467	2016-02-18 00:00:00	Campo Alegre	24
4468	2016-02-18 00:00:00	Campo Belo do Sul	24
4469	2016-02-18 00:00:00	Campo Erê	24
4470	2016-02-18 00:00:00	Campos Novos	24
4471	2016-02-18 00:00:00	Canelinha	24
4472	2016-02-18 00:00:00	Canoinhas	24
4473	2016-02-18 00:00:00	Capão Alto	24
4474	2016-02-18 00:00:00	Capinzal	24
4475	2016-02-18 00:00:00	Capivari de Baixo	24
4476	2016-02-18 00:00:00	Catanduvas	24
4477	2016-02-18 00:00:00	Caxambu do Sul	24
4478	2016-02-18 00:00:00	Celso Ramos	24
4479	2016-02-18 00:00:00	Cerro Negro	24
4480	2016-02-18 00:00:00	Chapadão do Lageado	24
4481	2016-02-18 00:00:00	Chapecó	24
4482	2016-02-18 00:00:00	Cocal do Sul	24
4483	2016-02-18 00:00:00	Concórdia	24
4484	2016-02-18 00:00:00	Cordilheira Alta	24
4485	2016-02-18 00:00:00	Coronel Freitas	24
4486	2016-02-18 00:00:00	Coronel Martins	24
4487	2016-02-18 00:00:00	Correia Pinto	24
4488	2016-02-18 00:00:00	Corupá	24
4489	2016-02-18 00:00:00	Criciúma	24
4490	2016-02-18 00:00:00	Cunha Porã	24
4491	2016-02-18 00:00:00	Cunhataí	24
4492	2016-02-18 00:00:00	Curitibanos	24
4493	2016-02-18 00:00:00	Descanso	24
4494	2016-02-18 00:00:00	Dionísio Cerqueira	24
4495	2016-02-18 00:00:00	Dona Emma	24
4496	2016-02-18 00:00:00	Doutor Pedrinho	24
4497	2016-02-18 00:00:00	Entre Rios	24
4498	2016-02-18 00:00:00	Ermo	24
4499	2016-02-18 00:00:00	Erval Velho	24
4500	2016-02-18 00:00:00	Faxinal dos Guedes	24
4501	2016-02-18 00:00:00	Flor do Sertão	24
4502	2016-02-18 00:00:00	Florianópolis	24
4503	2016-02-18 00:00:00	Formosa do Sul	24
4504	2016-02-18 00:00:00	Forquilhinha	24
4505	2016-02-18 00:00:00	Fraiburgo	24
4506	2016-02-18 00:00:00	Frei Rogério	24
4507	2016-02-18 00:00:00	Galvão	24
4508	2016-02-18 00:00:00	Garopaba	24
4509	2016-02-18 00:00:00	Garuva	24
4510	2016-02-18 00:00:00	Gaspar	24
4511	2016-02-18 00:00:00	Governador Celso Ramos	24
4512	2016-02-18 00:00:00	Grão Pará	24
4513	2016-02-18 00:00:00	Gravatal	24
4514	2016-02-18 00:00:00	Guabiruba	24
4515	2016-02-18 00:00:00	Guaraciaba	24
4516	2016-02-18 00:00:00	Guaramirim	24
4517	2016-02-18 00:00:00	Guarujá do Sul	24
4518	2016-02-18 00:00:00	Guatambú	24
4519	2016-02-18 00:00:00	Herval d'Oeste	24
4520	2016-02-18 00:00:00	Ibiam	24
4521	2016-02-18 00:00:00	Ibicaré	24
4522	2016-02-18 00:00:00	Ibirama	24
4523	2016-02-18 00:00:00	Içara	24
4524	2016-02-18 00:00:00	Ilhota	24
4525	2016-02-18 00:00:00	Imaruí	24
4526	2016-02-18 00:00:00	Imbituba	24
4527	2016-02-18 00:00:00	Imbuia	24
4528	2016-02-18 00:00:00	Indaial	24
4529	2016-02-18 00:00:00	Iomerê	24
4530	2016-02-18 00:00:00	Ipira	24
4531	2016-02-18 00:00:00	Iporã do Oeste	24
4532	2016-02-18 00:00:00	Ipuaçu	24
4533	2016-02-18 00:00:00	Ipumirim	24
4534	2016-02-18 00:00:00	Iraceminha	24
4535	2016-02-18 00:00:00	Irani	24
4536	2016-02-18 00:00:00	Irati	24
4537	2016-02-18 00:00:00	Irineópolis	24
4538	2016-02-18 00:00:00	Itá	24
4539	2016-02-18 00:00:00	Itaiópolis	24
4540	2016-02-18 00:00:00	Itajaí	24
4541	2016-02-18 00:00:00	Itapema	24
4542	2016-02-18 00:00:00	Itapiranga	24
4543	2016-02-18 00:00:00	Itapoá	24
4544	2016-02-18 00:00:00	Ituporanga	24
4545	2016-02-18 00:00:00	Jaborá	24
4546	2016-02-18 00:00:00	Jacinto Machado	24
4547	2016-02-18 00:00:00	Jaguaruna	24
4548	2016-02-18 00:00:00	Jaraguá do Sul	24
4549	2016-02-18 00:00:00	Jardinópolis	24
4550	2016-02-18 00:00:00	Joaçaba	24
4551	2016-02-18 00:00:00	Joinville	24
4552	2016-02-18 00:00:00	José Boiteux	24
4553	2016-02-18 00:00:00	Jupiá	24
4554	2016-02-18 00:00:00	Lacerdópolis	24
4555	2016-02-18 00:00:00	Lages	24
4556	2016-02-18 00:00:00	Laguna	24
4557	2016-02-18 00:00:00	Lajeado Grande	24
4558	2016-02-18 00:00:00	Laurentino	24
4559	2016-02-18 00:00:00	Lauro Muller	24
4560	2016-02-18 00:00:00	Lebon Régis	24
4561	2016-02-18 00:00:00	Leoberto Leal	24
4562	2016-02-18 00:00:00	Lindóia do Sul	24
4563	2016-02-18 00:00:00	Lontras	24
4564	2016-02-18 00:00:00	Luiz Alves	24
4565	2016-02-18 00:00:00	Luzerna	24
4566	2016-02-18 00:00:00	Macieira	24
4567	2016-02-18 00:00:00	Mafra	24
4568	2016-02-18 00:00:00	Major Gercino	24
4569	2016-02-18 00:00:00	Major Vieira	24
4570	2016-02-18 00:00:00	Maracajá	24
4571	2016-02-18 00:00:00	Maravilha	24
4572	2016-02-18 00:00:00	Marema	24
4573	2016-02-18 00:00:00	Massaranduba	24
4574	2016-02-18 00:00:00	Matos Costa	24
4575	2016-02-18 00:00:00	Meleiro	24
4576	2016-02-18 00:00:00	Mirim Doce	24
4577	2016-02-18 00:00:00	Modelo	24
4578	2016-02-18 00:00:00	Mondaí	24
4579	2016-02-18 00:00:00	Monte Carlo	24
4580	2016-02-18 00:00:00	Monte Castelo	24
4581	2016-02-18 00:00:00	Morro da Fumaça	24
4582	2016-02-18 00:00:00	Morro Grande	24
4583	2016-02-18 00:00:00	Navegantes	24
4584	2016-02-18 00:00:00	Nova Erechim	24
4585	2016-02-18 00:00:00	Nova Itaberaba	24
4586	2016-02-18 00:00:00	Nova Trento	24
4587	2016-02-18 00:00:00	Nova Veneza	24
4588	2016-02-18 00:00:00	Novo Horizonte	24
4589	2016-02-18 00:00:00	Orleans	24
4590	2016-02-18 00:00:00	Otacílio Costa	24
4591	2016-02-18 00:00:00	Ouro	24
4592	2016-02-18 00:00:00	Ouro Verde	24
4593	2016-02-18 00:00:00	Paial	24
4594	2016-02-18 00:00:00	Painel	24
4595	2016-02-18 00:00:00	Palhoça	24
4596	2016-02-18 00:00:00	Palma Sola	24
4597	2016-02-18 00:00:00	Palmeira	24
4598	2016-02-18 00:00:00	Palmitos	24
4599	2016-02-18 00:00:00	Papanduva	24
4600	2016-02-18 00:00:00	Paraíso	24
4601	2016-02-18 00:00:00	Passo de Torres	24
4602	2016-02-18 00:00:00	Passos Maia	24
4603	2016-02-18 00:00:00	Paulo Lopes	24
4604	2016-02-18 00:00:00	Pedras Grandes	24
4605	2016-02-18 00:00:00	Penha	24
4606	2016-02-18 00:00:00	Peritiba	24
4607	2016-02-18 00:00:00	Petrolândia	24
4608	2016-02-18 00:00:00	Pinhalzinho	24
4609	2016-02-18 00:00:00	Pinheiro Preto	24
4610	2016-02-18 00:00:00	Piratuba	24
4611	2016-02-18 00:00:00	Planalto Alegre	24
4612	2016-02-18 00:00:00	Pomerode	24
4613	2016-02-18 00:00:00	Ponte Alta	24
4614	2016-02-18 00:00:00	Ponte Alta do Norte	24
4615	2016-02-18 00:00:00	Ponte Serrada	24
4616	2016-02-18 00:00:00	Porto Belo	24
4617	2016-02-18 00:00:00	Porto União	24
4618	2016-02-18 00:00:00	Pouso Redondo	24
4619	2016-02-18 00:00:00	Praia Grande	24
4620	2016-02-18 00:00:00	Presidente Castello Branco	24
4621	2016-02-18 00:00:00	Presidente Getúlio	24
4622	2016-02-18 00:00:00	Presidente Nereu	24
4623	2016-02-18 00:00:00	Princesa	24
4624	2016-02-18 00:00:00	Quilombo	24
4625	2016-02-18 00:00:00	Rancho Queimado	24
4626	2016-02-18 00:00:00	Rio das Antas	24
4627	2016-02-18 00:00:00	Rio do Campo	24
4628	2016-02-18 00:00:00	Rio do Oeste	24
4629	2016-02-18 00:00:00	Rio do Sul	24
4630	2016-02-18 00:00:00	Rio dos Cedros	24
4631	2016-02-18 00:00:00	Rio Fortuna	24
4632	2016-02-18 00:00:00	Rio Negrinho	24
4633	2016-02-18 00:00:00	Rio Rufino	24
4634	2016-02-18 00:00:00	Riqueza	24
4635	2016-02-18 00:00:00	Rodeio	24
4636	2016-02-18 00:00:00	Romelândia	24
4637	2016-02-18 00:00:00	Salete	24
4638	2016-02-18 00:00:00	Saltinho	24
4639	2016-02-18 00:00:00	Salto Veloso	24
4640	2016-02-18 00:00:00	Sangão	24
4641	2016-02-18 00:00:00	Santa Cecília	24
4642	2016-02-18 00:00:00	Santa Helena	24
4643	2016-02-18 00:00:00	Santa Rosa de Lima	24
4644	2016-02-18 00:00:00	Santa Rosa do Sul	24
4645	2016-02-18 00:00:00	Santa Terezinha	24
4646	2016-02-18 00:00:00	Santa Terezinha do Progresso	24
4647	2016-02-18 00:00:00	Santiago do Sul	24
4648	2016-02-18 00:00:00	Santo Amaro da Imperatriz	24
4649	2016-02-18 00:00:00	São Bento do Sul	24
4650	2016-02-18 00:00:00	São Bernardino	24
4651	2016-02-18 00:00:00	São Bonifácio	24
4652	2016-02-18 00:00:00	São Carlos	24
4653	2016-02-18 00:00:00	São Cristovão do Sul	24
4654	2016-02-18 00:00:00	São Domingos	24
4655	2016-02-18 00:00:00	São Francisco do Sul	24
4656	2016-02-18 00:00:00	São João Batista	24
4657	2016-02-18 00:00:00	São João do Itaperiú	24
4658	2016-02-18 00:00:00	São João do Oeste	24
4659	2016-02-18 00:00:00	São João do Sul	24
4660	2016-02-18 00:00:00	São Joaquim	24
4661	2016-02-18 00:00:00	São José	24
4662	2016-02-18 00:00:00	São José do Cedro	24
4663	2016-02-18 00:00:00	São José do Cerrito	24
4664	2016-02-18 00:00:00	São Lourenço do Oeste	24
4665	2016-02-18 00:00:00	São Ludgero	24
4666	2016-02-18 00:00:00	São Martinho	24
4667	2016-02-18 00:00:00	São Miguel da Boa Vista	24
4668	2016-02-18 00:00:00	São Miguel do Oeste	24
4669	2016-02-18 00:00:00	São Pedro de Alcântara	24
4670	2016-02-18 00:00:00	Saudades	24
4671	2016-02-18 00:00:00	Schroeder	24
4672	2016-02-18 00:00:00	Seara	24
4673	2016-02-18 00:00:00	Serra Alta	24
4674	2016-02-18 00:00:00	Siderópolis	24
4675	2016-02-18 00:00:00	Sombrio	24
4676	2016-02-18 00:00:00	Sul Brasil	24
4677	2016-02-18 00:00:00	Taió	24
4678	2016-02-18 00:00:00	Tangará	24
4679	2016-02-18 00:00:00	Tigrinhos	24
4680	2016-02-18 00:00:00	Tijucas	24
4681	2016-02-18 00:00:00	Timbé do Sul	24
4682	2016-02-18 00:00:00	Timbó	24
4683	2016-02-18 00:00:00	Timbó Grande	24
4684	2016-02-18 00:00:00	Três Barras	24
4685	2016-02-18 00:00:00	Treviso	24
4686	2016-02-18 00:00:00	Treze de Maio	24
4687	2016-02-18 00:00:00	Treze Tílias	24
4688	2016-02-18 00:00:00	Trombudo Central	24
4689	2016-02-18 00:00:00	Tubarão	24
4690	2016-02-18 00:00:00	Tunápolis	24
4691	2016-02-18 00:00:00	Turvo	24
4692	2016-02-18 00:00:00	União do Oeste	24
4693	2016-02-18 00:00:00	Urubici	24
4694	2016-02-18 00:00:00	Urupema	24
4695	2016-02-18 00:00:00	Urussanga	24
4696	2016-02-18 00:00:00	Vargeão	24
4697	2016-02-18 00:00:00	Vargem	24
4698	2016-02-18 00:00:00	Vargem Bonita	24
4699	2016-02-18 00:00:00	Vidal Ramos	24
4700	2016-02-18 00:00:00	Videira	24
4701	2016-02-18 00:00:00	Vitor Meireles	24
4702	2016-02-18 00:00:00	Witmarsum	24
4703	2016-02-18 00:00:00	Xanxerê	24
4704	2016-02-18 00:00:00	Xavantina	24
4705	2016-02-18 00:00:00	Xaxim	24
4706	2016-02-18 00:00:00	Zortéa	24
4707	2016-02-18 00:00:00	Adamantina	26
4708	2016-02-18 00:00:00	Adolfo	26
4709	2016-02-18 00:00:00	Aguaí	26
4710	2016-02-18 00:00:00	Águas da Prata	26
4711	2016-02-18 00:00:00	Águas de Lindóia	26
4712	2016-02-18 00:00:00	Águas de Santa Bárbara	26
4713	2016-02-18 00:00:00	Águas de São Pedro	26
4714	2016-02-18 00:00:00	Agudos	26
4715	2016-02-18 00:00:00	Alambari	26
4716	2016-02-18 00:00:00	Alfredo Marcondes	26
4717	2016-02-18 00:00:00	Altair	26
4718	2016-02-18 00:00:00	Altinópolis	26
4719	2016-02-18 00:00:00	Alto Alegre	26
4720	2016-02-18 00:00:00	Alumínio	26
4721	2016-02-18 00:00:00	Álvares Florence	26
4722	2016-02-18 00:00:00	Álvares Machado	26
4723	2016-02-18 00:00:00	Álvaro de Carvalho	26
4724	2016-02-18 00:00:00	Alvinlândia	26
4725	2016-02-18 00:00:00	Americana	26
4726	2016-02-18 00:00:00	Américo Brasiliense	26
4727	2016-02-18 00:00:00	Américo de Campos	26
4728	2016-02-18 00:00:00	Amparo	26
4729	2016-02-18 00:00:00	Analândia	26
4730	2016-02-18 00:00:00	Andradina	26
4731	2016-02-18 00:00:00	Angatuba	26
4732	2016-02-18 00:00:00	Anhembi	26
4733	2016-02-18 00:00:00	Anhumas	26
4734	2016-02-18 00:00:00	Aparecida	26
4735	2016-02-18 00:00:00	Aparecida d'Oeste	26
4736	2016-02-18 00:00:00	Apiaí	26
4737	2016-02-18 00:00:00	Araçariguama	26
4738	2016-02-18 00:00:00	Araçatuba	26
4739	2016-02-18 00:00:00	Araçoiaba da Serra	26
4740	2016-02-18 00:00:00	Aramina	26
4741	2016-02-18 00:00:00	Arandu	26
4742	2016-02-18 00:00:00	Arapeí	26
4743	2016-02-18 00:00:00	Araraquara	26
4744	2016-02-18 00:00:00	Araras	26
4745	2016-02-18 00:00:00	Arco-Íris	26
4746	2016-02-18 00:00:00	Arealva	26
4747	2016-02-18 00:00:00	Areias	26
4748	2016-02-18 00:00:00	Areiópolis	26
4749	2016-02-18 00:00:00	Ariranha	26
4750	2016-02-18 00:00:00	Artur Nogueira	26
4751	2016-02-18 00:00:00	Arujá	26
4752	2016-02-18 00:00:00	Aspásia	26
4753	2016-02-18 00:00:00	Assis	26
4754	2016-02-18 00:00:00	Atibaia	26
4755	2016-02-18 00:00:00	Auriflama	26
4756	2016-02-18 00:00:00	Avaí	26
4757	2016-02-18 00:00:00	Avanhandava	26
4758	2016-02-18 00:00:00	Avaré	26
4759	2016-02-18 00:00:00	Bady Bassitt	26
4760	2016-02-18 00:00:00	Balbinos	26
4761	2016-02-18 00:00:00	Bálsamo	26
4762	2016-02-18 00:00:00	Bananal	26
4763	2016-02-18 00:00:00	Barão de Antonina	26
4764	2016-02-18 00:00:00	Barbosa	26
4765	2016-02-18 00:00:00	Bariri	26
4766	2016-02-18 00:00:00	Barra Bonita	26
4767	2016-02-18 00:00:00	Barra do Chapéu	26
4768	2016-02-18 00:00:00	Barra do Turvo	26
4769	2016-02-18 00:00:00	Barretos	26
4770	2016-02-18 00:00:00	Barrinha	26
4771	2016-02-18 00:00:00	Barueri	26
4772	2016-02-18 00:00:00	Bastos	26
4773	2016-02-18 00:00:00	Batatais	26
4774	2016-02-18 00:00:00	Bauru	26
4775	2016-02-18 00:00:00	Bebedouro	26
4776	2016-02-18 00:00:00	Bento de Abreu	26
4777	2016-02-18 00:00:00	Bernardino de Campos	26
4778	2016-02-18 00:00:00	Bertioga	26
4779	2016-02-18 00:00:00	Bilac	26
4780	2016-02-18 00:00:00	Birigui	26
4781	2016-02-18 00:00:00	Biritiba-Mirim	26
4782	2016-02-18 00:00:00	Boa Esperança do Sul	26
4783	2016-02-18 00:00:00	Bocaina	26
4784	2016-02-18 00:00:00	Bofete	26
4785	2016-02-18 00:00:00	Boituva	26
4786	2016-02-18 00:00:00	Bom Jesus dos Perdões	26
4787	2016-02-18 00:00:00	Bom Sucesso de Itararé	26
4788	2016-02-18 00:00:00	Borá	26
4789	2016-02-18 00:00:00	Boracéia	26
4790	2016-02-18 00:00:00	Borborema	26
4791	2016-02-18 00:00:00	Borebi	26
4792	2016-02-18 00:00:00	Botucatu	26
4793	2016-02-18 00:00:00	Bragança Paulista	26
4794	2016-02-18 00:00:00	Braúna	26
4795	2016-02-18 00:00:00	Brejo Alegre	26
4796	2016-02-18 00:00:00	Brodowski	26
4797	2016-02-18 00:00:00	Brotas	26
4798	2016-02-18 00:00:00	Buri	26
4799	2016-02-18 00:00:00	Buritama	26
4800	2016-02-18 00:00:00	Buritizal	26
4801	2016-02-18 00:00:00	Cabrália Paulista	26
4802	2016-02-18 00:00:00	Cabreúva	26
4803	2016-02-18 00:00:00	Caçapava	26
4804	2016-02-18 00:00:00	Cachoeira Paulista	26
4805	2016-02-18 00:00:00	Caconde	26
4806	2016-02-18 00:00:00	Cafelândia	26
4807	2016-02-18 00:00:00	Caiabu	26
4808	2016-02-18 00:00:00	Caieiras	26
4809	2016-02-18 00:00:00	Caiuá	26
4810	2016-02-18 00:00:00	Cajamar	26
4811	2016-02-18 00:00:00	Cajati	26
4812	2016-02-18 00:00:00	Cajobi	26
4813	2016-02-18 00:00:00	Cajuru	26
4814	2016-02-18 00:00:00	Campina do Monte Alegre	26
4815	2016-02-18 00:00:00	Campinas	26
4816	2016-02-18 00:00:00	Campo Limpo Paulista	26
4817	2016-02-18 00:00:00	Campos do Jordão	26
4818	2016-02-18 00:00:00	Campos Novos Paulista	26
4819	2016-02-18 00:00:00	Cananéia	26
4820	2016-02-18 00:00:00	Canas	26
4821	2016-02-18 00:00:00	Cândido Mota	26
4822	2016-02-18 00:00:00	Cândido Rodrigues	26
4823	2016-02-18 00:00:00	Canitar	26
4824	2016-02-18 00:00:00	Capão Bonito	26
4825	2016-02-18 00:00:00	Capela do Alto	26
4826	2016-02-18 00:00:00	Capivari	26
4827	2016-02-18 00:00:00	Caraguatatuba	26
4828	2016-02-18 00:00:00	Carapicuíba	26
4829	2016-02-18 00:00:00	Cardoso	26
4830	2016-02-18 00:00:00	Casa Branca	26
4831	2016-02-18 00:00:00	Cássia dos Coqueiros	26
4832	2016-02-18 00:00:00	Castilho	26
4833	2016-02-18 00:00:00	Catanduva	26
4834	2016-02-18 00:00:00	Catiguá	26
4835	2016-02-18 00:00:00	Cedral	26
4836	2016-02-18 00:00:00	Cerqueira César	26
4837	2016-02-18 00:00:00	Cerquilho	26
4838	2016-02-18 00:00:00	Cesário Lange	26
4839	2016-02-18 00:00:00	Charqueada	26
4840	2016-02-18 00:00:00	Chavantes	26
4841	2016-02-18 00:00:00	Clementina	26
4842	2016-02-18 00:00:00	Colina	26
4843	2016-02-18 00:00:00	Colômbia	26
4844	2016-02-18 00:00:00	Conchal	26
4845	2016-02-18 00:00:00	Conchas	26
4846	2016-02-18 00:00:00	Cordeirópolis	26
4847	2016-02-18 00:00:00	Coroados	26
4848	2016-02-18 00:00:00	Coronel Macedo	26
4849	2016-02-18 00:00:00	Corumbataí	26
4850	2016-02-18 00:00:00	Cosmópolis	26
4851	2016-02-18 00:00:00	Cosmorama	26
4852	2016-02-18 00:00:00	Cotia	26
4853	2016-02-18 00:00:00	Cravinhos	26
4854	2016-02-18 00:00:00	Cristais Paulista	26
4855	2016-02-18 00:00:00	Cruzália	26
4856	2016-02-18 00:00:00	Cruzeiro	26
4857	2016-02-18 00:00:00	Cubatão	26
4858	2016-02-18 00:00:00	Cunha	26
4859	2016-02-18 00:00:00	Descalvado	26
4860	2016-02-18 00:00:00	Diadema	26
4861	2016-02-18 00:00:00	Dirce Reis	26
4862	2016-02-18 00:00:00	Divinolândia	26
4863	2016-02-18 00:00:00	Dobrada	26
4864	2016-02-18 00:00:00	Dois Córregos	26
4865	2016-02-18 00:00:00	Dolcinópolis	26
4866	2016-02-18 00:00:00	Dourado	26
4867	2016-02-18 00:00:00	Dracena	26
4868	2016-02-18 00:00:00	Duartina	26
4869	2016-02-18 00:00:00	Dumont	26
4870	2016-02-18 00:00:00	Echaporã	26
4871	2016-02-18 00:00:00	Eldorado	26
4872	2016-02-18 00:00:00	Elias Fausto	26
4873	2016-02-18 00:00:00	Elisiário	26
4874	2016-02-18 00:00:00	Embaúba	26
4875	2016-02-18 00:00:00	Embu	26
4876	2016-02-18 00:00:00	Embu-Guaçu	26
4877	2016-02-18 00:00:00	Emilianópolis	26
4878	2016-02-18 00:00:00	Engenheiro Coelho	26
4879	2016-02-18 00:00:00	Espírito Santo do Pinhal	26
4880	2016-02-18 00:00:00	Espírito Santo do Turvo	26
4881	2016-02-18 00:00:00	Estiva Gerbi	26
4882	2016-02-18 00:00:00	Estrela d'Oeste	26
4883	2016-02-18 00:00:00	Estrela do Norte	26
4884	2016-02-18 00:00:00	Euclides da Cunha Paulista	26
4885	2016-02-18 00:00:00	Fartura	26
4886	2016-02-18 00:00:00	Fernando Prestes	26
4887	2016-02-18 00:00:00	Fernandópolis	26
4888	2016-02-18 00:00:00	Fernão	26
4889	2016-02-18 00:00:00	Ferraz de Vasconcelos	26
4890	2016-02-18 00:00:00	Flora Rica	26
4891	2016-02-18 00:00:00	Floreal	26
4892	2016-02-18 00:00:00	Flórida Paulista	26
4893	2016-02-18 00:00:00	Florínia	26
4894	2016-02-18 00:00:00	Franca	26
4895	2016-02-18 00:00:00	Francisco Morato	26
4896	2016-02-18 00:00:00	Franco da Rocha	26
4897	2016-02-18 00:00:00	Gabriel Monteiro	26
4898	2016-02-18 00:00:00	Gália	26
4899	2016-02-18 00:00:00	Garça	26
4900	2016-02-18 00:00:00	Gastão Vidigal	26
4901	2016-02-18 00:00:00	Gavião Peixoto	26
4902	2016-02-18 00:00:00	General Salgado	26
4903	2016-02-18 00:00:00	Getulina	26
4904	2016-02-18 00:00:00	Glicério	26
4905	2016-02-18 00:00:00	Guaiçara	26
4906	2016-02-18 00:00:00	Guaimbê	26
4907	2016-02-18 00:00:00	Guaíra	26
4908	2016-02-18 00:00:00	Guapiaçu	26
4909	2016-02-18 00:00:00	Guapiara	26
4910	2016-02-18 00:00:00	Guará	26
4911	2016-02-18 00:00:00	Guaraçaí	26
4912	2016-02-18 00:00:00	Guaraci	26
4913	2016-02-18 00:00:00	Guarani d'Oeste	26
4914	2016-02-18 00:00:00	Guarantã	26
4915	2016-02-18 00:00:00	Guararapes	26
4916	2016-02-18 00:00:00	Guararema	26
4917	2016-02-18 00:00:00	Guaratinguetá	26
4918	2016-02-18 00:00:00	Guareí	26
4919	2016-02-18 00:00:00	Guariba	26
4920	2016-02-18 00:00:00	Guarujá	26
4921	2016-02-18 00:00:00	Guarulhos	26
4922	2016-02-18 00:00:00	Guatapará	26
4923	2016-02-18 00:00:00	Guzolândia	26
4924	2016-02-18 00:00:00	Herculândia	26
4925	2016-02-18 00:00:00	Holambra	26
4926	2016-02-18 00:00:00	Hortolândia	26
4927	2016-02-18 00:00:00	Iacanga	26
4928	2016-02-18 00:00:00	Iacri	26
4929	2016-02-18 00:00:00	Iaras	26
4930	2016-02-18 00:00:00	Ibaté	26
4931	2016-02-18 00:00:00	Ibirá	26
4932	2016-02-18 00:00:00	Ibirarema	26
4933	2016-02-18 00:00:00	Ibitinga	26
4934	2016-02-18 00:00:00	Ibiúna	26
4935	2016-02-18 00:00:00	Icém	26
4936	2016-02-18 00:00:00	Iepê	26
4937	2016-02-18 00:00:00	Igaraçu do Tietê	26
4938	2016-02-18 00:00:00	Igarapava	26
4939	2016-02-18 00:00:00	Igaratá	26
4940	2016-02-18 00:00:00	Iguape	26
4941	2016-02-18 00:00:00	Ilha Comprida	26
4942	2016-02-18 00:00:00	Ilha Solteira	26
4943	2016-02-18 00:00:00	Ilhabela	26
4944	2016-02-18 00:00:00	Indaiatuba	26
4945	2016-02-18 00:00:00	Indiana	26
4946	2016-02-18 00:00:00	Indiaporã	26
4947	2016-02-18 00:00:00	Inúbia Paulista	26
4948	2016-02-18 00:00:00	Ipaussu	26
4949	2016-02-18 00:00:00	Iperó	26
4950	2016-02-18 00:00:00	Ipeúna	26
4951	2016-02-18 00:00:00	Ipiguá	26
4952	2016-02-18 00:00:00	Iporanga	26
4953	2016-02-18 00:00:00	Ipuã	26
4954	2016-02-18 00:00:00	Iracemápolis	26
4955	2016-02-18 00:00:00	Irapuã	26
4956	2016-02-18 00:00:00	Irapuru	26
4957	2016-02-18 00:00:00	Itaberá	26
4958	2016-02-18 00:00:00	Itaí	26
4959	2016-02-18 00:00:00	Itajobi	26
4960	2016-02-18 00:00:00	Itaju	26
4961	2016-02-18 00:00:00	Itanhaém	26
4962	2016-02-18 00:00:00	Itaóca	26
4963	2016-02-18 00:00:00	Itapecerica da Serra	26
4964	2016-02-18 00:00:00	Itapetininga	26
4965	2016-02-18 00:00:00	Itapeva	26
4966	2016-02-18 00:00:00	Itapevi	26
4967	2016-02-18 00:00:00	Itapira	26
4968	2016-02-18 00:00:00	Itapirapuã Paulista	26
4969	2016-02-18 00:00:00	Itápolis	26
4970	2016-02-18 00:00:00	Itaporanga	26
4971	2016-02-18 00:00:00	Itapuí	26
4972	2016-02-18 00:00:00	Itapura	26
4973	2016-02-18 00:00:00	Itaquaquecetuba	26
4974	2016-02-18 00:00:00	Itararé	26
4975	2016-02-18 00:00:00	Itariri	26
4976	2016-02-18 00:00:00	Itatiba	26
4977	2016-02-18 00:00:00	Itatinga	26
4978	2016-02-18 00:00:00	Itirapina	26
4979	2016-02-18 00:00:00	Itirapuã	26
4980	2016-02-18 00:00:00	Itobi	26
4981	2016-02-18 00:00:00	Itu	26
4982	2016-02-18 00:00:00	Itupeva	26
4983	2016-02-18 00:00:00	Ituverava	26
4984	2016-02-18 00:00:00	Jaborandi	26
4985	2016-02-18 00:00:00	Jaboticabal	26
4986	2016-02-18 00:00:00	Jacareí	26
4987	2016-02-18 00:00:00	Jaci	26
4988	2016-02-18 00:00:00	Jacupiranga	26
4989	2016-02-18 00:00:00	Jaguariúna	26
4990	2016-02-18 00:00:00	Jales	26
4991	2016-02-18 00:00:00	Jambeiro	26
4992	2016-02-18 00:00:00	Jandira	26
4993	2016-02-18 00:00:00	Jardinópolis	26
4994	2016-02-18 00:00:00	Jarinu	26
4995	2016-02-18 00:00:00	Jaú	26
4996	2016-02-18 00:00:00	Jeriquara	26
4997	2016-02-18 00:00:00	Joanópolis	26
4998	2016-02-18 00:00:00	João Ramalho	26
4999	2016-02-18 00:00:00	José Bonifácio	26
5000	2016-02-18 00:00:00	Júlio Mesquita	26
5001	2016-02-18 00:00:00	Jumirim	26
5002	2016-02-18 00:00:00	Jundiaí	26
5003	2016-02-18 00:00:00	Junqueirópolis	26
5004	2016-02-18 00:00:00	Juquiá	26
5005	2016-02-18 00:00:00	Juquitiba	26
5006	2016-02-18 00:00:00	Lagoinha	26
5007	2016-02-18 00:00:00	Laranjal Paulista	26
5008	2016-02-18 00:00:00	Lavínia	26
5009	2016-02-18 00:00:00	Lavrinhas	26
5010	2016-02-18 00:00:00	Leme	26
5011	2016-02-18 00:00:00	Lençóis Paulista	26
5012	2016-02-18 00:00:00	Limeira	26
5013	2016-02-18 00:00:00	Lindóia	26
5014	2016-02-18 00:00:00	Lins	26
5015	2016-02-18 00:00:00	Lorena	26
5016	2016-02-18 00:00:00	Lourdes	26
5017	2016-02-18 00:00:00	Louveira	26
5018	2016-02-18 00:00:00	Lucélia	26
5019	2016-02-18 00:00:00	Lucianópolis	26
5020	2016-02-18 00:00:00	Luís Antônio	26
5021	2016-02-18 00:00:00	Luiziânia	26
5022	2016-02-18 00:00:00	Lupércio	26
5023	2016-02-18 00:00:00	Lutécia	26
5024	2016-02-18 00:00:00	Macatuba	26
5025	2016-02-18 00:00:00	Macaubal	26
5026	2016-02-18 00:00:00	Macedônia	26
5027	2016-02-18 00:00:00	Magda	26
5028	2016-02-18 00:00:00	Mairinque	26
5029	2016-02-18 00:00:00	Mairiporã	26
5030	2016-02-18 00:00:00	Manduri	26
5031	2016-02-18 00:00:00	Marabá Paulista	26
5032	2016-02-18 00:00:00	Maracaí	26
5033	2016-02-18 00:00:00	Marapoama	26
5034	2016-02-18 00:00:00	Mariápolis	26
5035	2016-02-18 00:00:00	Marília	26
5036	2016-02-18 00:00:00	Marinópolis	26
5037	2016-02-18 00:00:00	Martinópolis	26
5038	2016-02-18 00:00:00	Matão	26
5039	2016-02-18 00:00:00	Mauá	26
5040	2016-02-18 00:00:00	Mendonça	26
5041	2016-02-18 00:00:00	Meridiano	26
5042	2016-02-18 00:00:00	Mesópolis	26
5043	2016-02-18 00:00:00	Miguelópolis	26
5044	2016-02-18 00:00:00	Mineiros do Tietê	26
5045	2016-02-18 00:00:00	Mira Estrela	26
5046	2016-02-18 00:00:00	Miracatu	26
5047	2016-02-18 00:00:00	Mirandópolis	26
5048	2016-02-18 00:00:00	Mirante do Paranapanema	26
5049	2016-02-18 00:00:00	Mirassol	26
5050	2016-02-18 00:00:00	Mirassolândia	26
5051	2016-02-18 00:00:00	Mococa	26
5052	2016-02-18 00:00:00	Mogi das Cruzes	26
5053	2016-02-18 00:00:00	Mogi Guaçu	26
5054	2016-02-18 00:00:00	Moji Mirim	26
5055	2016-02-18 00:00:00	Mombuca	26
5056	2016-02-18 00:00:00	Monções	26
5057	2016-02-18 00:00:00	Mongaguá	26
5058	2016-02-18 00:00:00	Monte Alegre do Sul	26
5059	2016-02-18 00:00:00	Monte Alto	26
5060	2016-02-18 00:00:00	Monte Aprazível	26
5061	2016-02-18 00:00:00	Monte Azul Paulista	26
5062	2016-02-18 00:00:00	Monte Castelo	26
5063	2016-02-18 00:00:00	Monte Mor	26
5064	2016-02-18 00:00:00	Monteiro Lobato	26
5065	2016-02-18 00:00:00	Morro Agudo	26
5066	2016-02-18 00:00:00	Morungaba	26
5067	2016-02-18 00:00:00	Motuca	26
5068	2016-02-18 00:00:00	Murutinga do Sul	26
5069	2016-02-18 00:00:00	Nantes	26
5070	2016-02-18 00:00:00	Narandiba	26
5071	2016-02-18 00:00:00	Natividade da Serra	26
5072	2016-02-18 00:00:00	Nazaré Paulista	26
5073	2016-02-18 00:00:00	Neves Paulista	26
5074	2016-02-18 00:00:00	Nhandeara	26
5075	2016-02-18 00:00:00	Nipoã	26
5076	2016-02-18 00:00:00	Nova Aliança	26
5077	2016-02-18 00:00:00	Nova Campina	26
5078	2016-02-18 00:00:00	Nova Canaã Paulista	26
5079	2016-02-18 00:00:00	Nova Castilho	26
5080	2016-02-18 00:00:00	Nova Europa	26
5081	2016-02-18 00:00:00	Nova Granada	26
5082	2016-02-18 00:00:00	Nova Guataporanga	26
5083	2016-02-18 00:00:00	Nova Independência	26
5084	2016-02-18 00:00:00	Nova Luzitânia	26
5085	2016-02-18 00:00:00	Nova Odessa	26
5086	2016-02-18 00:00:00	Novais	26
5087	2016-02-18 00:00:00	Novo Horizonte	26
5088	2016-02-18 00:00:00	Nuporanga	26
5089	2016-02-18 00:00:00	Ocauçu	26
5090	2016-02-18 00:00:00	Óleo	26
5091	2016-02-18 00:00:00	Olímpia	26
5092	2016-02-18 00:00:00	Onda Verde	26
5093	2016-02-18 00:00:00	Oriente	26
5094	2016-02-18 00:00:00	Orindiúva	26
5095	2016-02-18 00:00:00	Orlândia	26
5096	2016-02-18 00:00:00	Osasco	26
5097	2016-02-18 00:00:00	Oscar Bressane	26
5098	2016-02-18 00:00:00	Osvaldo Cruz	26
5099	2016-02-18 00:00:00	Ourinhos	26
5100	2016-02-18 00:00:00	Ouro Verde	26
5101	2016-02-18 00:00:00	Ouroeste	26
5102	2016-02-18 00:00:00	Pacaembu	26
5103	2016-02-18 00:00:00	Palestina	26
5104	2016-02-18 00:00:00	Palmares Paulista	26
5105	2016-02-18 00:00:00	Palmeira d'Oeste	26
5106	2016-02-18 00:00:00	Palmital	26
5107	2016-02-18 00:00:00	Panorama	26
5345	2016-02-18 00:00:00	Vinhedo	26
5108	2016-02-18 00:00:00	Paraguaçu Paulista	26
5109	2016-02-18 00:00:00	Paraibuna	26
5110	2016-02-18 00:00:00	Paraíso	26
5111	2016-02-18 00:00:00	Paranapanema	26
5112	2016-02-18 00:00:00	Paranapuã	26
5113	2016-02-18 00:00:00	Parapuã	26
5114	2016-02-18 00:00:00	Pardinho	26
5115	2016-02-18 00:00:00	Pariquera-Açu	26
5116	2016-02-18 00:00:00	Parisi	26
5117	2016-02-18 00:00:00	Patrocínio Paulista	26
5118	2016-02-18 00:00:00	Paulicéia	26
5119	2016-02-18 00:00:00	Paulínia	26
5120	2016-02-18 00:00:00	Paulistânia	26
5121	2016-02-18 00:00:00	Paulo de Faria	26
5122	2016-02-18 00:00:00	Pederneiras	26
5123	2016-02-18 00:00:00	Pedra Bela	26
5124	2016-02-18 00:00:00	Pedranópolis	26
5125	2016-02-18 00:00:00	Pedregulho	26
5126	2016-02-18 00:00:00	Pedreira	26
5127	2016-02-18 00:00:00	Pedrinhas Paulista	26
5128	2016-02-18 00:00:00	Pedro de Toledo	26
5129	2016-02-18 00:00:00	Penápolis	26
5130	2016-02-18 00:00:00	Pereira Barreto	26
5131	2016-02-18 00:00:00	Pereiras	26
5132	2016-02-18 00:00:00	Peruíbe	26
5133	2016-02-18 00:00:00	Piacatu	26
5134	2016-02-18 00:00:00	Piedade	26
5135	2016-02-18 00:00:00	Pilar do Sul	26
5136	2016-02-18 00:00:00	Pindamonhangaba	26
5137	2016-02-18 00:00:00	Pindorama	26
5138	2016-02-18 00:00:00	Pinhalzinho	26
5139	2016-02-18 00:00:00	Piquerobi	26
5140	2016-02-18 00:00:00	Piquete	26
5141	2016-02-18 00:00:00	Piracaia	26
5142	2016-02-18 00:00:00	Piracicaba	26
5143	2016-02-18 00:00:00	Piraju	26
5144	2016-02-18 00:00:00	Pirajuí	26
5145	2016-02-18 00:00:00	Pirangi	26
5146	2016-02-18 00:00:00	Pirapora do Bom Jesus	26
5147	2016-02-18 00:00:00	Pirapozinho	26
5148	2016-02-18 00:00:00	Pirassununga	26
5149	2016-02-18 00:00:00	Piratininga	26
5150	2016-02-18 00:00:00	Pitangueiras	26
5151	2016-02-18 00:00:00	Planalto	26
5152	2016-02-18 00:00:00	Platina	26
5153	2016-02-18 00:00:00	Poá	26
5154	2016-02-18 00:00:00	Poloni	26
5155	2016-02-18 00:00:00	Pompéia	26
5156	2016-02-18 00:00:00	Pongaí	26
5157	2016-02-18 00:00:00	Pontal	26
5158	2016-02-18 00:00:00	Pontalinda	26
5159	2016-02-18 00:00:00	Pontes Gestal	26
5160	2016-02-18 00:00:00	Populina	26
5161	2016-02-18 00:00:00	Porangaba	26
5162	2016-02-18 00:00:00	Porto Feliz	26
5163	2016-02-18 00:00:00	Porto Ferreira	26
5164	2016-02-18 00:00:00	Potim	26
5165	2016-02-18 00:00:00	Potirendaba	26
5166	2016-02-18 00:00:00	Pracinha	26
5167	2016-02-18 00:00:00	Pradópolis	26
5168	2016-02-18 00:00:00	Praia Grande	26
5169	2016-02-18 00:00:00	Pratânia	26
5170	2016-02-18 00:00:00	Presidente Alves	26
5171	2016-02-18 00:00:00	Presidente Bernardes	26
5172	2016-02-18 00:00:00	Presidente Epitácio	26
5173	2016-02-18 00:00:00	Presidente Prudente	26
5174	2016-02-18 00:00:00	Presidente Venceslau	26
5175	2016-02-18 00:00:00	Promissão	26
5176	2016-02-18 00:00:00	Quadra	26
5177	2016-02-18 00:00:00	Quatá	26
5178	2016-02-18 00:00:00	Queiroz	26
5179	2016-02-18 00:00:00	Queluz	26
5180	2016-02-18 00:00:00	Quintana	26
5181	2016-02-18 00:00:00	Rafard	26
5182	2016-02-18 00:00:00	Rancharia	26
5183	2016-02-18 00:00:00	Redenção da Serra	26
5184	2016-02-18 00:00:00	Regente Feijó	26
5185	2016-02-18 00:00:00	Reginópolis	26
5186	2016-02-18 00:00:00	Registro	26
5187	2016-02-18 00:00:00	Restinga	26
5188	2016-02-18 00:00:00	Ribeira	26
5189	2016-02-18 00:00:00	Ribeirão Bonito	26
5190	2016-02-18 00:00:00	Ribeirão Branco	26
5191	2016-02-18 00:00:00	Ribeirão Corrente	26
5192	2016-02-18 00:00:00	Ribeirão do Sul	26
5193	2016-02-18 00:00:00	Ribeirão dos Índios	26
5194	2016-02-18 00:00:00	Ribeirão Grande	26
5195	2016-02-18 00:00:00	Ribeirão Pires	26
5196	2016-02-18 00:00:00	Ribeirão Preto	26
5197	2016-02-18 00:00:00	Rifaina	26
5198	2016-02-18 00:00:00	Rincão	26
5199	2016-02-18 00:00:00	Rinópolis	26
5200	2016-02-18 00:00:00	Rio Claro	26
5201	2016-02-18 00:00:00	Rio das Pedras	26
5202	2016-02-18 00:00:00	Rio Grande da Serra	26
5203	2016-02-18 00:00:00	Riolândia	26
5204	2016-02-18 00:00:00	Riversul	26
5205	2016-02-18 00:00:00	Rosana	26
5206	2016-02-18 00:00:00	Roseira	26
5207	2016-02-18 00:00:00	Rubiácea	26
5208	2016-02-18 00:00:00	Rubinéia	26
5209	2016-02-18 00:00:00	Sabino	26
5210	2016-02-18 00:00:00	Sagres	26
5211	2016-02-18 00:00:00	Sales	26
5212	2016-02-18 00:00:00	Sales Oliveira	26
5213	2016-02-18 00:00:00	Salesópolis	26
5214	2016-02-18 00:00:00	Salmourão	26
5215	2016-02-18 00:00:00	Saltinho	26
5216	2016-02-18 00:00:00	Salto	26
5217	2016-02-18 00:00:00	Salto de Pirapora	26
5218	2016-02-18 00:00:00	Salto Grande	26
5219	2016-02-18 00:00:00	Sandovalina	26
5220	2016-02-18 00:00:00	Santa Adélia	26
5221	2016-02-18 00:00:00	Santa Albertina	26
5222	2016-02-18 00:00:00	Santa Bárbara d'Oeste	26
5223	2016-02-18 00:00:00	Santa Branca	26
5224	2016-02-18 00:00:00	Santa Clara d'Oeste	26
5225	2016-02-18 00:00:00	Santa Cruz da Conceição	26
5226	2016-02-18 00:00:00	Santa Cruz da Esperança	26
5227	2016-02-18 00:00:00	Santa Cruz das Palmeiras	26
5228	2016-02-18 00:00:00	Santa Cruz do Rio Pardo	26
5229	2016-02-18 00:00:00	Santa Ernestina	26
5230	2016-02-18 00:00:00	Santa Fé do Sul	26
5231	2016-02-18 00:00:00	Santa Gertrudes	26
5232	2016-02-18 00:00:00	Santa Isabel	26
5233	2016-02-18 00:00:00	Santa Lúcia	26
5234	2016-02-18 00:00:00	Santa Maria da Serra	26
5235	2016-02-18 00:00:00	Santa Mercedes	26
5236	2016-02-18 00:00:00	Santa Rita d'Oeste	26
5237	2016-02-18 00:00:00	Santa Rita do Passa Quatro	26
5238	2016-02-18 00:00:00	Santa Rosa de Viterbo	26
5239	2016-02-18 00:00:00	Santa Salete	26
5240	2016-02-18 00:00:00	Santana da Ponte Pensa	26
5241	2016-02-18 00:00:00	Santana de Parnaíba	26
5242	2016-02-18 00:00:00	Santo Anastácio	26
5243	2016-02-18 00:00:00	Santo André	26
5244	2016-02-18 00:00:00	Santo Antônio da Alegria	26
5245	2016-02-18 00:00:00	Santo Antônio de Posse	26
5246	2016-02-18 00:00:00	Santo Antônio do Aracanguá	26
5247	2016-02-18 00:00:00	Santo Antônio do Jardim	26
5248	2016-02-18 00:00:00	Santo Antônio do Pinhal	26
5249	2016-02-18 00:00:00	Santo Expedito	26
5250	2016-02-18 00:00:00	Santópolis do Aguapeí	26
5251	2016-02-18 00:00:00	Santos	26
5252	2016-02-18 00:00:00	São Bento do Sapucaí	26
5253	2016-02-18 00:00:00	São Bernardo do Campo	26
5254	2016-02-18 00:00:00	São Caetano do Sul	26
5255	2016-02-18 00:00:00	São Carlos	26
5256	2016-02-18 00:00:00	São Francisco	26
5257	2016-02-18 00:00:00	São João da Boa Vista	26
5258	2016-02-18 00:00:00	São João das Duas Pontes	26
5259	2016-02-18 00:00:00	São João de Iracema	26
5260	2016-02-18 00:00:00	São João do Pau d'Alho	26
5261	2016-02-18 00:00:00	São Joaquim da Barra	26
5262	2016-02-18 00:00:00	São José da Bela Vista	26
5263	2016-02-18 00:00:00	São José do Barreiro	26
5264	2016-02-18 00:00:00	São José do Rio Pardo	26
5265	2016-02-18 00:00:00	São José do Rio Preto	26
5266	2016-02-18 00:00:00	São José dos Campos	26
5267	2016-02-18 00:00:00	São Lourenço da Serra	26
5268	2016-02-18 00:00:00	São Luís do Paraitinga	26
5269	2016-02-18 00:00:00	São Manuel	26
5270	2016-02-18 00:00:00	São Miguel Arcanjo	26
5271	2016-02-18 00:00:00	São Paulo	26
5272	2016-02-18 00:00:00	São Pedro	26
5273	2016-02-18 00:00:00	São Pedro do Turvo	26
5274	2016-02-18 00:00:00	São Roque	26
5275	2016-02-18 00:00:00	São Sebastião	26
5276	2016-02-18 00:00:00	São Sebastião da Grama	26
5277	2016-02-18 00:00:00	São Simão	26
5278	2016-02-18 00:00:00	São Vicente	26
5279	2016-02-18 00:00:00	Sarapuí	26
5280	2016-02-18 00:00:00	Sarutaiá	26
5281	2016-02-18 00:00:00	Sebastianópolis do Sul	26
5282	2016-02-18 00:00:00	Serra Azul	26
5283	2016-02-18 00:00:00	Serra Negra	26
5284	2016-02-18 00:00:00	Serrana	26
5285	2016-02-18 00:00:00	Sertãozinho	26
5286	2016-02-18 00:00:00	Sete Barras	26
5287	2016-02-18 00:00:00	Severínia	26
5288	2016-02-18 00:00:00	Silveiras	26
5289	2016-02-18 00:00:00	Socorro	26
5290	2016-02-18 00:00:00	Sorocaba	26
5291	2016-02-18 00:00:00	Sud Mennucci	26
5292	2016-02-18 00:00:00	Sumaré	26
5293	2016-02-18 00:00:00	Suzanápolis	26
5294	2016-02-18 00:00:00	Suzano	26
5295	2016-02-18 00:00:00	Tabapuã	26
5296	2016-02-18 00:00:00	Tabatinga	26
5297	2016-02-18 00:00:00	Taboão da Serra	26
5298	2016-02-18 00:00:00	Taciba	26
5299	2016-02-18 00:00:00	Taguaí	26
5300	2016-02-18 00:00:00	Taiaçu	26
5301	2016-02-18 00:00:00	Taiúva	26
5302	2016-02-18 00:00:00	Tambaú	26
5303	2016-02-18 00:00:00	Tanabi	26
5304	2016-02-18 00:00:00	Tapiraí	26
5305	2016-02-18 00:00:00	Tapiratiba	26
5306	2016-02-18 00:00:00	Taquaral	26
5307	2016-02-18 00:00:00	Taquaritinga	26
5308	2016-02-18 00:00:00	Taquarituba	26
5309	2016-02-18 00:00:00	Taquarivaí	26
5310	2016-02-18 00:00:00	Tarabai	26
5311	2016-02-18 00:00:00	Tarumã	26
5312	2016-02-18 00:00:00	Tatuí	26
5313	2016-02-18 00:00:00	Taubaté	26
5314	2016-02-18 00:00:00	Tejupá	26
5315	2016-02-18 00:00:00	Teodoro Sampaio	26
5316	2016-02-18 00:00:00	Terra Roxa	26
5317	2016-02-18 00:00:00	Tietê	26
5318	2016-02-18 00:00:00	Timburi	26
5319	2016-02-18 00:00:00	Torre de Pedra	26
5320	2016-02-18 00:00:00	Torrinha	26
5321	2016-02-18 00:00:00	Trabiju	26
5322	2016-02-18 00:00:00	Tremembé	26
5323	2016-02-18 00:00:00	Três Fronteiras	26
5324	2016-02-18 00:00:00	Tuiuti	26
5325	2016-02-18 00:00:00	Tupã	26
5326	2016-02-18 00:00:00	Tupi Paulista	26
5327	2016-02-18 00:00:00	Turiúba	26
5328	2016-02-18 00:00:00	Turmalina	26
5329	2016-02-18 00:00:00	Ubarana	26
5330	2016-02-18 00:00:00	Ubatuba	26
5331	2016-02-18 00:00:00	Ubirajara	26
5332	2016-02-18 00:00:00	Uchoa	26
5333	2016-02-18 00:00:00	União Paulista	26
5334	2016-02-18 00:00:00	Urânia	26
5335	2016-02-18 00:00:00	Uru	26
5336	2016-02-18 00:00:00	Urupês	26
5337	2016-02-18 00:00:00	Valentim Gentil	26
5338	2016-02-18 00:00:00	Valinhos	26
5339	2016-02-18 00:00:00	Valparaíso	26
5340	2016-02-18 00:00:00	Vargem	26
5341	2016-02-18 00:00:00	Vargem Grande do Sul	26
5342	2016-02-18 00:00:00	Vargem Grande Paulista	26
5343	2016-02-18 00:00:00	Várzea Paulista	26
5344	2016-02-18 00:00:00	Vera Cruz	26
5346	2016-02-18 00:00:00	Viradouro	26
5347	2016-02-18 00:00:00	Vista Alegre do Alto	26
5348	2016-02-18 00:00:00	Vitória Brasil	26
5349	2016-02-18 00:00:00	Votorantim	26
5350	2016-02-18 00:00:00	Votuporanga	26
5351	2016-02-18 00:00:00	Zacarias	26
5352	2016-02-18 00:00:00	Amparo de São Francisco	25
5353	2016-02-18 00:00:00	Aquidabã	25
5354	2016-02-18 00:00:00	Aracaju	25
5355	2016-02-18 00:00:00	Arauá	25
5356	2016-02-18 00:00:00	Areia Branca	25
5357	2016-02-18 00:00:00	Barra dos Coqueiros	25
5358	2016-02-18 00:00:00	Boquim	25
5359	2016-02-18 00:00:00	Brejo Grande	25
5360	2016-02-18 00:00:00	Campo do Brito	25
5361	2016-02-18 00:00:00	Canhoba	25
5362	2016-02-18 00:00:00	Canindé de São Francisco	25
5363	2016-02-18 00:00:00	Capela	25
5364	2016-02-18 00:00:00	Carira	25
5365	2016-02-18 00:00:00	Carmópolis	25
5366	2016-02-18 00:00:00	Cedro de São João	25
5367	2016-02-18 00:00:00	Cristinápolis	25
5368	2016-02-18 00:00:00	Cumbe	25
5369	2016-02-18 00:00:00	Divina Pastora	25
5370	2016-02-18 00:00:00	Estância	25
5371	2016-02-18 00:00:00	Feira Nova	25
5372	2016-02-18 00:00:00	Frei Paulo	25
5373	2016-02-18 00:00:00	Gararu	25
5374	2016-02-18 00:00:00	General Maynard	25
5375	2016-02-18 00:00:00	Gracho Cardoso	25
5376	2016-02-18 00:00:00	Ilha das Flores	25
5377	2016-02-18 00:00:00	Indiaroba	25
5378	2016-02-18 00:00:00	Itabaiana	25
5379	2016-02-18 00:00:00	Itabaianinha	25
5380	2016-02-18 00:00:00	Itabi	25
5381	2016-02-18 00:00:00	Itaporanga d'Ajuda	25
5382	2016-02-18 00:00:00	Japaratuba	25
5383	2016-02-18 00:00:00	Japoatã	25
5384	2016-02-18 00:00:00	Lagarto	25
5385	2016-02-18 00:00:00	Laranjeiras	25
5386	2016-02-18 00:00:00	Macambira	25
5387	2016-02-18 00:00:00	Malhada dos Bois	25
5388	2016-02-18 00:00:00	Malhador	25
5389	2016-02-18 00:00:00	Maruim	25
5390	2016-02-18 00:00:00	Moita Bonita	25
5391	2016-02-18 00:00:00	Monte Alegre de Sergipe	25
5392	2016-02-18 00:00:00	Muribeca	25
5393	2016-02-18 00:00:00	Neópolis	25
5394	2016-02-18 00:00:00	Nossa Senhora Aparecida	25
5395	2016-02-18 00:00:00	Nossa Senhora da Glória	25
5396	2016-02-18 00:00:00	Nossa Senhora das Dores	25
5397	2016-02-18 00:00:00	Nossa Senhora de Lourdes	25
5398	2016-02-18 00:00:00	Nossa Senhora do Socorro	25
5399	2016-02-18 00:00:00	Pacatuba	25
5400	2016-02-18 00:00:00	Pedra Mole	25
5401	2016-02-18 00:00:00	Pedrinhas	25
5402	2016-02-18 00:00:00	Pinhão	25
5403	2016-02-18 00:00:00	Pirambu	25
5404	2016-02-18 00:00:00	Poço Redondo	25
5405	2016-02-18 00:00:00	Poço Verde	25
5406	2016-02-18 00:00:00	Porto da Folha	25
5407	2016-02-18 00:00:00	Propriá	25
5408	2016-02-18 00:00:00	Riachão do Dantas	25
5409	2016-02-18 00:00:00	Riachuelo	25
5410	2016-02-18 00:00:00	Ribeirópolis	25
5411	2016-02-18 00:00:00	Rosário do Catete	25
5412	2016-02-18 00:00:00	Salgado	25
5413	2016-02-18 00:00:00	Santa Luzia do Itanhy	25
5414	2016-02-18 00:00:00	Santa Rosa de Lima	25
5415	2016-02-18 00:00:00	Santana do São Francisco	25
5416	2016-02-18 00:00:00	Santo Amaro das Brotas	25
5417	2016-02-18 00:00:00	São Cristóvão	25
5418	2016-02-18 00:00:00	São Domingos	25
5419	2016-02-18 00:00:00	São Francisco	25
5420	2016-02-18 00:00:00	São Miguel do Aleixo	25
5421	2016-02-18 00:00:00	Simão Dias	25
5422	2016-02-18 00:00:00	Siriri	25
5423	2016-02-18 00:00:00	Telha	25
5424	2016-02-18 00:00:00	Tobias Barreto	25
5425	2016-02-18 00:00:00	Tomar do Geru	25
5426	2016-02-18 00:00:00	Umbaúba	25
5427	2016-02-18 00:00:00	Abreulândia	27
5428	2016-02-18 00:00:00	Aguiarnópolis	27
5429	2016-02-18 00:00:00	Aliança do Tocantins	27
5430	2016-02-18 00:00:00	Almas	27
5431	2016-02-18 00:00:00	Alvorada	27
5432	2016-02-18 00:00:00	Ananás	27
5433	2016-02-18 00:00:00	Angico	27
5434	2016-02-18 00:00:00	Aparecida do Rio Negro	27
5435	2016-02-18 00:00:00	Aragominas	27
5436	2016-02-18 00:00:00	Araguacema	27
5437	2016-02-18 00:00:00	Araguaçu	27
5438	2016-02-18 00:00:00	Araguaína	27
5439	2016-02-18 00:00:00	Araguanã	27
5440	2016-02-18 00:00:00	Araguatins	27
5441	2016-02-18 00:00:00	Arapoema	27
5442	2016-02-18 00:00:00	Arraias	27
5443	2016-02-18 00:00:00	Augustinópolis	27
5444	2016-02-18 00:00:00	Aurora do Tocantins	27
5445	2016-02-18 00:00:00	Axixá do Tocantins	27
5446	2016-02-18 00:00:00	Babaçulândia	27
5447	2016-02-18 00:00:00	Bandeirantes do Tocantins	27
5448	2016-02-18 00:00:00	Barra do Ouro	27
5449	2016-02-18 00:00:00	Barrolândia	27
5450	2016-02-18 00:00:00	Bernardo Sayão	27
5451	2016-02-18 00:00:00	Bom Jesus do Tocantins	27
5452	2016-02-18 00:00:00	Brasilândia do Tocantins	27
5453	2016-02-18 00:00:00	Brejinho de Nazaré	27
5454	2016-02-18 00:00:00	Buriti do Tocantins	27
5455	2016-02-18 00:00:00	Cachoeirinha	27
5456	2016-02-18 00:00:00	Campos Lindos	27
5457	2016-02-18 00:00:00	Cariri do Tocantins	27
5458	2016-02-18 00:00:00	Carmolândia	27
5459	2016-02-18 00:00:00	Carrasco Bonito	27
5460	2016-02-18 00:00:00	Caseara	27
5461	2016-02-18 00:00:00	Centenário	27
5462	2016-02-18 00:00:00	Chapada da Natividade	27
5463	2016-02-18 00:00:00	Chapada de Areia	27
5464	2016-02-18 00:00:00	Colinas do Tocantins	27
5465	2016-02-18 00:00:00	Colméia	27
5466	2016-02-18 00:00:00	Combinado	27
5467	2016-02-18 00:00:00	Conceição do Tocantins	27
5468	2016-02-18 00:00:00	Couto Magalhães	27
5469	2016-02-18 00:00:00	Cristalândia	27
5470	2016-02-18 00:00:00	Crixás do Tocantins	27
5471	2016-02-18 00:00:00	Darcinópolis	27
5472	2016-02-18 00:00:00	Dianópolis	27
5473	2016-02-18 00:00:00	Divinópolis do Tocantins	27
5474	2016-02-18 00:00:00	Dois Irmãos do Tocantins	27
5475	2016-02-18 00:00:00	Dueré	27
5476	2016-02-18 00:00:00	Esperantina	27
5477	2016-02-18 00:00:00	Fátima	27
5478	2016-02-18 00:00:00	Figueirópolis	27
5479	2016-02-18 00:00:00	Filadélfia	27
5480	2016-02-18 00:00:00	Formoso do Araguaia	27
5481	2016-02-18 00:00:00	Fortaleza do Tabocão	27
5482	2016-02-18 00:00:00	Goianorte	27
5483	2016-02-18 00:00:00	Goiatins	27
5484	2016-02-18 00:00:00	Guaraí	27
5485	2016-02-18 00:00:00	Gurupi	27
5486	2016-02-18 00:00:00	Ipueiras	27
5487	2016-02-18 00:00:00	Itacajá	27
5488	2016-02-18 00:00:00	Itaguatins	27
5489	2016-02-18 00:00:00	Itapiratins	27
5490	2016-02-18 00:00:00	Itaporã do Tocantins	27
5491	2016-02-18 00:00:00	Jaú do Tocantins	27
5492	2016-02-18 00:00:00	Juarina	27
5493	2016-02-18 00:00:00	Lagoa da Confusão	27
5494	2016-02-18 00:00:00	Lagoa do Tocantins	27
5495	2016-02-18 00:00:00	Lajeado	27
5496	2016-02-18 00:00:00	Lavandeira	27
5497	2016-02-18 00:00:00	Lizarda	27
5498	2016-02-18 00:00:00	Luzinópolis	27
5499	2016-02-18 00:00:00	Marianópolis do Tocantins	27
5500	2016-02-18 00:00:00	Mateiros	27
5501	2016-02-18 00:00:00	Maurilândia do Tocantins	27
5502	2016-02-18 00:00:00	Miracema do Tocantins	27
5503	2016-02-18 00:00:00	Miranorte	27
5504	2016-02-18 00:00:00	Monte do Carmo	27
5505	2016-02-18 00:00:00	Monte Santo do Tocantins	27
5506	2016-02-18 00:00:00	Muricilândia	27
5507	2016-02-18 00:00:00	Natividade	27
5508	2016-02-18 00:00:00	Nazaré	27
5509	2016-02-18 00:00:00	Nova Olinda	27
5510	2016-02-18 00:00:00	Nova Rosalândia	27
5511	2016-02-18 00:00:00	Novo Acordo	27
5512	2016-02-18 00:00:00	Novo Alegre	27
5513	2016-02-18 00:00:00	Novo Jardim	27
5514	2016-02-18 00:00:00	Oliveira de Fátima	27
5515	2016-02-18 00:00:00	Palmas	27
5516	2016-02-18 00:00:00	Palmeirante	27
5517	2016-02-18 00:00:00	Palmeiras do Tocantins	27
5518	2016-02-18 00:00:00	Palmeirópolis	27
5519	2016-02-18 00:00:00	Paraíso do Tocantins	27
5520	2016-02-18 00:00:00	Paranã	27
5521	2016-02-18 00:00:00	Pau D'Arco	27
5522	2016-02-18 00:00:00	Pedro Afonso	27
5523	2016-02-18 00:00:00	Peixe	27
5524	2016-02-18 00:00:00	Pequizeiro	27
5525	2016-02-18 00:00:00	Pindorama do Tocantins	27
5526	2016-02-18 00:00:00	Piraquê	27
5527	2016-02-18 00:00:00	Pium	27
5528	2016-02-18 00:00:00	Ponte Alta do Bom Jesus	27
5529	2016-02-18 00:00:00	Ponte Alta do Tocantins	27
5530	2016-02-18 00:00:00	Porto Alegre do Tocantins	27
5531	2016-02-18 00:00:00	Porto Nacional	27
5532	2016-02-18 00:00:00	Praia Norte	27
5533	2016-02-18 00:00:00	Presidente Kennedy	27
5534	2016-02-18 00:00:00	Pugmil	27
5535	2016-02-18 00:00:00	Recursolândia	27
5536	2016-02-18 00:00:00	Riachinho	27
5537	2016-02-18 00:00:00	Rio da Conceição	27
5538	2016-02-18 00:00:00	Rio dos Bois	27
5539	2016-02-18 00:00:00	Rio Sono	27
5540	2016-02-18 00:00:00	Sampaio	27
5541	2016-02-18 00:00:00	Sandolândia	27
5542	2016-02-18 00:00:00	Santa Fé do Araguaia	27
5543	2016-02-18 00:00:00	Santa Maria do Tocantins	27
5544	2016-02-18 00:00:00	Santa Rita do Tocantins	27
5545	2016-02-18 00:00:00	Santa Rosa do Tocantins	27
5546	2016-02-18 00:00:00	Santa Tereza do Tocantins	27
5547	2016-02-18 00:00:00	Santa Terezinha do Tocantins	27
5548	2016-02-18 00:00:00	São Bento do Tocantins	27
5549	2016-02-18 00:00:00	São Félix do Tocantins	27
5550	2016-02-18 00:00:00	São Miguel do Tocantins	27
5551	2016-02-18 00:00:00	São Salvador do Tocantins	27
5552	2016-02-18 00:00:00	São Sebastião do Tocantins	27
5553	2016-02-18 00:00:00	São Valério	27
5554	2016-02-18 00:00:00	Silvanópolis	27
5555	2016-02-18 00:00:00	Sítio Novo do Tocantins	27
5556	2016-02-18 00:00:00	Sucupira	27
5557	2016-02-18 00:00:00	Taguatinga	27
5558	2016-02-18 00:00:00	Taipas do Tocantins	27
5559	2016-02-18 00:00:00	Talismã	27
5560	2016-02-18 00:00:00	Tocantínia	27
5561	2016-02-18 00:00:00	Tocantinópolis	27
5562	2016-02-18 00:00:00	Tupirama	27
5563	2016-02-18 00:00:00	Tupiratins	27
5564	2016-02-18 00:00:00	Wanderlândia	27
5565	2016-02-18 00:00:00	Xambioá	27
\.


--
-- TOC entry 2635 (class 0 OID 0)
-- Dependencies: 199
-- Name: cidade_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cidade_id_seq', 1, false);


--
-- TOC entry 2536 (class 0 OID 1722272)
-- Dependencies: 202
-- Data for Name: dominio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY dominio (id, data_cadastro, ativo, codigo, descricao, nome) FROM stdin;
1	2016-02-18 00:00:00	t	1	Masculino	TipoSexo
2	2016-02-18 00:00:00	t	2	Feminino	TipoSexo
3	2016-02-18 00:00:00	t	1	Usuário Interno	TipoUsuario
4	2016-02-18 00:00:00	t	2	Usuário Externo	TipoUsuario
5	2016-02-18 00:00:00	t	1	ADMINISTRAÇÃO	AreaConhecimento
6	2016-02-18 00:00:00	t	2	DIREITO	AreaConhecimento
7	2016-02-18 00:00:00	t	3	NUTRIÇÃO	AreaConhecimento
8	2016-02-18 00:00:00	t	4	ENGENHARIA	AreaConhecimento
9	2016-02-18 00:00:00	t	5	MEDICINA	AreaConhecimento
10	2016-02-18 00:00:00	t	6	ODONTOLOGIA	AreaConhecimento
11	2016-02-18 00:00:00	t	7	CONTABILIDADE	AreaConhecimento
12	2016-02-18 00:00:00	t	8	TECNOLOGIA DA INFORMAÇÃO	AreaConhecimento
13	2016-02-18 00:00:00	t	9	ARQUITETURA	AreaConhecimento
14	2016-02-18 00:00:00	t	10	LETRAS	AreaConhecimento
15	2016-02-18 00:00:00	t	11	CIÊNCIAS ECONÔMICAS	AreaConhecimento
16	2016-02-18 00:00:00	t	13	PEDAGOGIA	AreaConhecimento
17	2016-02-18 00:00:00	t	14	CIÊNCIAS SOCIAIS	AreaConhecimento
18	2016-02-18 00:00:00	t	15	HISTÓRIA	AreaConhecimento
19	2016-02-18 00:00:00	t	16	FARMÁCIA	AreaConhecimento
20	2016-02-18 00:00:00	t	1	Eixo temático geral	EixoTematico
21	2016-02-18 00:00:00	t	2	Direito Público	EixoTematico
22	2016-02-18 00:00:00	f	1	TESOURO ESTADUAL	FonteGasto
23	2016-02-18 00:00:00	f	2	PROMOEX	FonteGasto
24	2016-02-18 00:00:00	f	3	SWAP	FonteGasto
25	2016-02-18 00:00:00	t	1	CIÊNCIAS DA TECNOLOGIA DA INFORMAÇÃO	FormacaoAcademica
26	2016-02-18 00:00:00	t	2	LETRAS	FormacaoAcademica
27	2016-02-18 00:00:00	t	3	MEDICINA	FormacaoAcademica
28	2016-02-18 00:00:00	t	4	ODONTOLOGIA	FormacaoAcademica
29	2016-02-18 00:00:00	t	5	DIREITO	FormacaoAcademica
30	2016-02-18 00:00:00	t	6	CIÊNCIAS CONTÁBEIS	FormacaoAcademica
31	2016-02-18 00:00:00	t	7	ADMINISTRAÇÃO	FormacaoAcademica
32	2016-02-18 00:00:00	t	8	CIÊNCIAS ECONÔMICAS	FormacaoAcademica
33	2016-02-18 00:00:00	t	9	ENGENHARIA	FormacaoAcademica
34	2016-02-18 00:00:00	t	10	PEDAGOGIA	FormacaoAcademica
35	2016-02-18 00:00:00	t	11	CIÊNCIAS SOCIAIS	FormacaoAcademica
36	2016-02-18 00:00:00	t	12	PSICOPEDAGOGIA	FormacaoAcademica
37	2016-02-18 00:00:00	t	13	EDUCAÇÃO	FormacaoAcademica
38	2016-02-18 00:00:00	t	14	CONTROLE EXTERNO	FormacaoAcademica
39	2016-02-18 00:00:00	t	15	GESTÃO PÚBLICA	FormacaoAcademica
40	2016-02-18 00:00:00	t	16	ARQUITETURA E URBANISMO	FormacaoAcademica
41	2016-02-18 00:00:00	t	17	HISTÓRIA	FormacaoAcademica
42	2016-02-18 00:00:00	t	18	GESTÃO E AVALIAÇÃO DA EDUCAÇÃO PÚBLICA	FormacaoAcademica
43	2016-02-18 00:00:00	t	19	GESTÃO FINANCEIRA E CONTROLADORIA	FormacaoAcademica
44	2016-02-18 00:00:00	t	20	FILOSOFIA	FormacaoAcademica
45	2016-02-18 00:00:00	t	21	ADMINISTRAÇÃO E CONTROLADORIA	FormacaoAcademica
46	2016-02-18 00:00:00	t	22	CONTROLADORIA E CONTABILIDADE	FormacaoAcademica
47	2016-02-18 00:00:00	t	23	FÍSICA	FormacaoAcademica
48	2016-02-18 00:00:00	t	24	CIÊNCIAS EM ENGENHARIA DE SISTEMA	FormacaoAcademica
49	2016-02-18 00:00:00	t	25	GERENCIAMENTO DE PROJETOS	FormacaoAcademica
50	2016-02-18 00:00:00	t	26	PSICOLOGIA	FormacaoAcademica
51	2016-02-18 00:00:00	t	1	Presencial	Modalidade
52	2016-02-18 00:00:00	t	2	EAD	Modalidade
53	2016-02-18 00:00:00	t	1	ENSINO FUNDAMENTAL	NivelEscolaridade
54	2016-02-18 00:00:00	t	2	ENSINO MÉDIO	NivelEscolaridade
55	2016-02-18 00:00:00	t	3	GRADUAÇÃO	NivelEscolaridade
56	2016-02-18 00:00:00	t	4	ESPECIALIZAÇÃO	NivelEscolaridade
57	2016-02-18 00:00:00	t	5	MESTRADO	NivelEscolaridade
58	2016-02-18 00:00:00	t	6	DOUTORADO	NivelEscolaridade
59	2016-02-18 00:00:00	t	7	MBA	NivelEscolaridade
60	2016-02-18 00:00:00	t	8	PÓS-DOUTORADO	NivelEscolaridade
61	2016-02-18 00:00:00	t	1	FIM	TipoAreaTribunal
62	2016-02-18 00:00:00	t	2	MEIO	TipoAreaTribunal
63	2016-02-18 00:00:00	t	3	MEIO/FIM	TipoAreaTribunal
64	2016-02-18 00:00:00	t	4	NÃO SE APLICA	TipoAreaTribunal
65	2016-02-18 00:00:00	t	1	CURSO	TipoEvento
66	2016-02-18 00:00:00	t	2	TREINAMENTO	TipoEvento
67	2016-02-18 00:00:00	t	3	CONGRESSO	TipoEvento
68	2016-02-18 00:00:00	t	4	FÓRUM	TipoEvento
69	2016-02-18 00:00:00	t	5	SIMPÓSIO	TipoEvento
70	2016-02-18 00:00:00	t	6	PALESTRA	TipoEvento
71	2016-02-18 00:00:00	t	7	SEMINÁRIO	TipoEvento
72	2016-02-18 00:00:00	t	8	MINI CURSO	TipoEvento
73	2016-02-18 00:00:00	t	9	OFICINA	TipoEvento
74	2016-02-18 00:00:00	t	10	CONVENÇÃO	TipoEvento
75	2016-02-18 00:00:00	t	11	ENCONTRO	TipoEvento
76	2016-02-18 00:00:00	t	12	JORNADA	TipoEvento
77	2016-02-18 00:00:00	t	13	EVENTO	TipoEvento
78	2016-02-18 00:00:00	t	14	CICLO DE DEBATES	TipoEvento
79	2016-02-18 00:00:00	t	1	DIÁRIA	TipoGasto
80	2016-02-18 00:00:00	t	2	PASSAGEM	TipoGasto
81	2016-02-18 00:00:00	t	3	INSCRIÇÃO	TipoGasto
82	2016-02-18 00:00:00	t	4	SERVIÇO DE BUFFET	TipoGasto
83	2016-02-18 00:00:00	t	5	PRODUÇÃO DE MATERIAL DIDÁTICO (apostila, cartilha, manual, etc.)	TipoGasto
84	2016-02-18 00:00:00	t	6	INSTRUTOR	TipoGasto
85	2016-02-18 00:00:00	t	1	INSTRUTOR	TipoInstrutor
86	2016-02-18 00:00:00	t	2	PALESTRANTE	TipoInstrutor
87	2016-02-18 00:00:00	t	3	TUTOR	TipoInstrutor
88	2016-02-18 00:00:00	t	4	CONTEUDISTA	TipoInstrutor
89	2016-02-18 00:00:00	t	5	PROFESSOR	TipoInstrutor
90	2016-02-18 00:00:00	t	6	INTERNO	TipoInstrutor
91	2016-02-18 00:00:00	t	7	EXTERNO	TipoInstrutor
92	2016-02-18 00:00:00	t	1	JURISDICIONADO	TipoPublicoAlvo
93	2016-02-18 00:00:00	t	2	SOCIEDADE	TipoPublicoAlvo
94	2016-02-18 00:00:00	t	3	SERVIDOR	TipoPublicoAlvo
95	2016-02-18 00:00:00	t	1	CADASTRADO	SituacaoInstrutor
96	2016-02-18 00:00:00	t	2	PRÉ-CADASTRADO	SituacaoInstrutor
97	2016-02-18 00:00:00	t	3	NÃO ACEITO	SituacaoInstrutor
98	2016-02-18 00:00:00	t	1	APOSTILA	TipoMaterial
99	2016-02-18 00:00:00	t	2	APRESENTAÇÃO	TipoMaterial
100	2016-02-18 00:00:00	t	3	OUTRO	TipoMaterial
101	2016-02-18 00:00:00	t	1	NENHUMA MELHORIA	TipoDesempenhoServico
102	2016-02-18 00:00:00	t	2	POUCA MELHORIA	TipoDesempenhoServico
103	2016-02-18 00:00:00	t	3	MELHORIA	TipoDesempenhoServico
104	2016-02-18 00:00:00	t	4	SIGNIFICATIVA MELHORIA	TipoDesempenhoServico
105	2016-06-21 00:00:00	f	1	2017021000912	Configuracao
106	2016-06-21 00:00:00	f	2	2016080900916	Configuracao
107	2016-06-21 00:00:00	t	3	2017041200169	Configuracao
108	2016-06-21 00:00:00	t	6	Superintendente da Escola de Contas	Configuracao
109	2016-06-21 00:00:00	t	4	SOPHOS - Sistema de Gestão Educacional	Configuracao
110	2016-06-21 00:00:00	t	5	Vivian Borim Borges Moreira	Configuracao
111	2016-07-04 00:00:00	t	3	Gestão Pública	EixoTematico
112	2016-07-04 00:00:00	t	4	Controle Externo	EixoTematico
113	2016-07-04 00:00:00	t	5	Controle Social	EixoTematico
114	2016-07-04 00:00:00	t	6	Engenharia	EixoTematico
115	2016-07-04 00:00:00	t	7	Gestão de Pessoas	EixoTematico
116	2016-08-10 00:00:00	t	17	COMUNICAÇÃO SOCIAL	AreaConhecimento
117	2016-08-10 00:00:00	t	27	COMUNICAÇÃO SOCIAL	FormacaoAcademica
118	2016-08-10 00:00:00	t	4	Gabinete Presidente do TCM	FonteGasto
119	2016-08-10 00:00:00	t	5	Fundo Especial de Reaparelhamento do TCM - FUNNER	FonteGasto
120	2016-02-18 00:00:00	t	1	Analista	NomeCargo
121	2016-02-18 00:00:00	t	2	Assessor Contábil (Prestador de Serviço)	NomeCargo
122	2016-02-18 00:00:00	t	3	Assessor Jurídico (Prestador de Serviço)	NomeCargo
123	2016-02-18 00:00:00	t	4	Assistente	NomeCargo
124	2016-02-18 00:00:00	t	5	Auxiliar	NomeCargo
125	2016-02-18 00:00:00	t	6	Chefe de Gabinete	NomeCargo
126	2016-02-18 00:00:00	t	7	Conselheiro Municipal	NomeCargo
127	2016-02-18 00:00:00	t	8	Contador	NomeCargo
128	2016-02-18 00:00:00	t	9	Controlador Interno	NomeCargo
129	2016-02-18 00:00:00	t	10	Diretor/Superintendente	NomeCargo
130	2016-02-18 00:00:00	t	11	Encarregado	NomeCargo
131	2016-02-18 00:00:00	t	12	Fiscal	NomeCargo
132	2016-02-18 00:00:00	t	13	Gestor/Gerente/Chefe/Coordenador	NomeCargo
133	2016-02-18 00:00:00	t	14	Prefeito	NomeCargo
134	2016-02-18 00:00:00	t	15	Presidente da Câmara Municipal	NomeCargo
135	2016-02-18 00:00:00	t	16	Presidente de Conselho	NomeCargo
136	2016-02-18 00:00:00	t	17	Procurador	NomeCargo
137	2016-02-18 00:00:00	t	18	Professor	NomeCargo
138	2016-02-18 00:00:00	t	19	Profissional de Saúde	NomeCargo
139	2016-02-18 00:00:00	t	20	Secretário Municipal	NomeCargo
140	2016-02-18 00:00:00	t	21	Técnico	NomeCargo
141	2016-02-18 00:00:00	t	22	Vereador	NomeCargo
142	2016-02-18 00:00:00	t	23	Vice-Prefeito	NomeCargo
143	2016-02-18 00:00:00	t	24	Outro	NomeCargo
144	2016-08-26 00:00:00	t	6	Fundo Rotativo	FonteGasto
145	2016-02-18 00:00:00	t	1	LEVANTAMENTO DE NECESSIDADES	TipoArquivoAdministrativo
146	2016-02-18 00:00:00	t	2	PLANEJAMENTO	TipoArquivoAdministrativo
147	2016-02-18 00:00:00	t	3	EXECUÇÃO	TipoArquivoAdministrativo
148	2016-02-18 00:00:00	t	4	AVALIAÇÃO	TipoArquivoAdministrativo
149	2016-02-18 00:00:00	t	5	OUTRO	TipoArquivoAdministrativo
150	2016-10-05 00:00:00	t	18	Farmácia	AreaConhecimento
151	2016-10-05 00:00:00	t	19	ENFERMAGEM	AreaConhecimento
152	2016-10-05 00:00:00	t	28	FARMÁCIA	FormacaoAcademica
153	2016-10-05 00:00:00	t	29	ENFERMAGEM	FormacaoAcademica
154	2016-10-05 00:00:00	t	30	FISIOTERAPIA	FormacaoAcademica
155	2016-02-18 00:00:00	t	1	VISUAL	TipoNecessidadeEspecial
156	2016-02-18 00:00:00	t	2	AUDITIVA	TipoNecessidadeEspecial
157	2016-02-18 00:00:00	t	3	FÍSICA	TipoNecessidadeEspecial
158	2016-02-18 00:00:00	t	4	OUTRA	TipoNecessidadeEspecial
159	2016-02-18 00:00:00	t	1	ALERTAS DE NÃO PREENCHIMENTO DE AVALIAÇÃO DE REAÇÃO	TipoAlertas
160	2016-02-18 00:00:00	t	2	ALERTAS PERSONALIZADAS	TipoAlertas
161	2016-10-18 00:00:00	t	31	GEOGRAFIA	FormacaoAcademica
162	2016-10-18 00:00:00	t	32	MATEMÁTICA	FormacaoAcademica
163	2016-10-20 00:00:00	t	7	LOCAÇÃO DE ESPAÇO	TipoGasto
164	2016-10-20 00:00:00	t	33	OUTRO	FormacaoAcademica
165	2016-02-18 00:00:00	t	1	2013 a 2016	Mandato
166	2016-02-18 00:00:00	t	2	2017 a 2020	Mandato
167	2016-10-27 00:00:00	t	3	Semi-presencial	Modalidade
168	2016-11-28 00:00:00	t	8	OUTRO	TipoGasto
169	2017-04-05 00:00:00	t	25	Conselheiro	NomeCargo
170	2017-04-05 00:00:00	t	26	Conselheiro Substitutito	NomeCargo
171	2017-04-05 00:00:00	t	27	Procurador de Contas	NomeCargo
\.


--
-- TOC entry 2636 (class 0 OID 0)
-- Dependencies: 201
-- Name: dominio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('dominio_id_seq', 1, false);


--
-- TOC entry 2538 (class 0 OID 1722283)
-- Dependencies: 204
-- Data for Name: endereco; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY endereco (id, data_cadastro, bairro, cep, complemento, logradouro, municipio_id, numero, cidade_id) FROM stdin;
1	2016-06-07 00:00:00	Centro	74353-220	Qd 40 Lt 20	Rua 1	89	s/n	\N
\.


--
-- TOC entry 2637 (class 0 OID 0)
-- Dependencies: 203
-- Name: endereco_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('endereco_id_seq', 1, false);


--
-- TOC entry 2540 (class 0 OID 1722294)
-- Dependencies: 206
-- Data for Name: estado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY estado (id, data_cadastro, descricao, uf, pais_id) FROM stdin;
1	2016-02-18 00:00:00	Acre	AC	1
2	2016-02-18 00:00:00	Alagoas	AL	1
3	2016-02-18 00:00:00	Amazonas	AM	1
4	2016-02-18 00:00:00	Amapá	AP	1
5	2016-02-18 00:00:00	Bahia	BA	1
6	2016-02-18 00:00:00	Ceará	CE	1
7	2016-02-18 00:00:00	Distrito Federal	DF	1
8	2016-02-18 00:00:00	Espírito Santo	ES	1
9	2016-02-18 00:00:00	Goiás	GO	1
10	2016-02-18 00:00:00	Maranhão	MA	1
11	2016-02-18 00:00:00	Minas Gerais	MG	1
12	2016-02-18 00:00:00	Mato Grosso do Sul	MS	1
13	2016-02-18 00:00:00	Mato Grosso	MT	1
14	2016-02-18 00:00:00	Pará	PA	1
15	2016-02-18 00:00:00	Paraíba	PB	1
16	2016-02-18 00:00:00	Pernambuco	PE	1
17	2016-02-18 00:00:00	Piauí	PI	1
18	2016-02-18 00:00:00	Paraná	PR	1
19	2016-02-18 00:00:00	Rio de Janeiro	RJ	1
20	2016-02-18 00:00:00	Rio Grande do Norte	RN	1
21	2016-02-18 00:00:00	Rondônia	RO	1
22	2016-02-18 00:00:00	Roraima	RR	1
23	2016-02-18 00:00:00	Rio Grande do Sul	RS	1
24	2016-02-18 00:00:00	Santa Catarina	SC	1
25	2016-02-18 00:00:00	Sergipe	SE	1
26	2016-02-18 00:00:00	São Paulo	SP	1
27	2016-02-18 00:00:00	Tocantins	TO	1
\.


--
-- TOC entry 2638 (class 0 OID 0)
-- Dependencies: 205
-- Name: estado_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('estado_id_seq', 1, false);


--
-- TOC entry 2542 (class 0 OID 1722302)
-- Dependencies: 208
-- Data for Name: evento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY evento (id, data_cadastro, avaliar_por_media, carga_horaria, conteudo, data_fim_pre_inscricao, data_fim_previsto, data_fim_realizacao, data_inicio_pre_inscricao, data_inicio_previsto, data_inicio_realizacao, frequencia_aprovacao, nota_aprovacao, modulo_unico, mostrar_na_home, objetivo_especifico, objetivo_geral, observacoes, observacoes_publicas, permite_certificado, permite_pre_inscricao, publicado, resultado_esperado, titulo, vagas, area_tribunal_id, eixo_tematico_id, imagem_id, localizacao_id, modalidade_id, provedor_id, responsavel_evento, tipo_evento_id) FROM stdin;
\.


--
-- TOC entry 2639 (class 0 OID 0)
-- Dependencies: 207
-- Name: evento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('evento_id_seq', 1, false);


--
-- TOC entry 2544 (class 0 OID 1722313)
-- Dependencies: 210
-- Data for Name: evento_publico_alvo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY evento_publico_alvo (id, data_cadastro, evento_id, publico_alvo_id) FROM stdin;
\.


--
-- TOC entry 2640 (class 0 OID 0)
-- Dependencies: 209
-- Name: evento_publico_alvo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('evento_publico_alvo_id_seq', 1, false);


--
-- TOC entry 2546 (class 0 OID 1722321)
-- Dependencies: 212
-- Data for Name: frequencia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY frequencia (id, data_cadastro, encontro, presenca, modulo_id, participante_id) FROM stdin;
\.


--
-- TOC entry 2641 (class 0 OID 0)
-- Dependencies: 211
-- Name: frequencia_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('frequencia_id_seq', 1, false);


--
-- TOC entry 2548 (class 0 OID 1722329)
-- Dependencies: 214
-- Data for Name: gasto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY gasto (id, data_cadastro, ano_processo, data_empenho, numero_empenho, observacao, seq_processo, valor, evento_id, fonte_gasto_id, tipo_id) FROM stdin;
\.


--
-- TOC entry 2642 (class 0 OID 0)
-- Dependencies: 213
-- Name: gasto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('gasto_id_seq', 1, false);


--
-- TOC entry 2550 (class 0 OID 1722340)
-- Dependencies: 216
-- Data for Name: indicacao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY indicacao (id, data_cadastro, aprovado, data_avaliacao, justificativa_chefe, avaliador_id, chefe_id, evento_id, participante_id) FROM stdin;
\.


--
-- TOC entry 2643 (class 0 OID 0)
-- Dependencies: 215
-- Name: indicacao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('indicacao_id_seq', 1, false);


--
-- TOC entry 2552 (class 0 OID 1722351)
-- Dependencies: 218
-- Data for Name: inscricao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY inscricao (id, data_cadastro, autorizada, data_avaliacao_preinscricao, justificativa_participante, pre_inscricao, avaliador_id, evento_id, indicacao_id, participante_id) FROM stdin;
\.


--
-- TOC entry 2644 (class 0 OID 0)
-- Dependencies: 217
-- Name: inscricao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('inscricao_id_seq', 1, false);


--
-- TOC entry 2554 (class 0 OID 1722362)
-- Dependencies: 220
-- Data for Name: instrutor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY instrutor (id, data_cadastro, instituicao, observacao, perfil, codsecao, arquivo_assinatura_id, arquivo_curriculo_id, endereco_id, formacao_academica_id, nivel_escolaridade_id, pessoa_id, arquivo_projeto_id, situacao_instrutor_id, tipo_instrutor_id) FROM stdin;
\.


--
-- TOC entry 2645 (class 0 OID 0)
-- Dependencies: 219
-- Name: instrutor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('instrutor_id_seq', 1, false);


--
-- TOC entry 2556 (class 0 OID 1722373)
-- Dependencies: 222
-- Data for Name: localizacao_evento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY localizacao_evento (id, data_cadastro, descricao, endereco_id) FROM stdin;
\.


--
-- TOC entry 2646 (class 0 OID 0)
-- Dependencies: 221
-- Name: localizacao_evento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('localizacao_evento_id_seq', 1, false);


--
-- TOC entry 2558 (class 0 OID 1722381)
-- Dependencies: 224
-- Data for Name: log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY log (id, data_cadastro, entidade_id, operacao, tabela, usuario_id) FROM stdin;
\.


--
-- TOC entry 2647 (class 0 OID 0)
-- Dependencies: 223
-- Name: log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('log_id_seq', 1, false);


--
-- TOC entry 2560 (class 0 OID 1722392)
-- Dependencies: 226
-- Data for Name: material; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY material (id, data_cadastro, arquivo_id, evento_id, modulo_id, tipo_material_id) FROM stdin;
\.


--
-- TOC entry 2648 (class 0 OID 0)
-- Dependencies: 225
-- Name: material_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('material_id_seq', 1, false);


--
-- TOC entry 2562 (class 0 OID 1722400)
-- Dependencies: 228
-- Data for Name: modulo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY modulo (id, data_cadastro, carga_horaria, data_fim, data_inicio, frequencia_aprovacao, hora_fim_turno1, hora_fim_turno2, hora_inicio_turno1, hora_inicio_turno2, nota_aprovacao, observacao, quantidade_encontros, titulo, evento_id) FROM stdin;
\.


--
-- TOC entry 2649 (class 0 OID 0)
-- Dependencies: 227
-- Name: modulo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('modulo_id_seq', 1, false);


--
-- TOC entry 2564 (class 0 OID 1722411)
-- Dependencies: 230
-- Data for Name: modulo_instrutor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY modulo_instrutor (id, data_cadastro, instrutor_id, modulo_id) FROM stdin;
\.


--
-- TOC entry 2650 (class 0 OID 0)
-- Dependencies: 229
-- Name: modulo_instrutor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('modulo_instrutor_id_seq', 1, false);


--
-- TOC entry 2566 (class 0 OID 1722419)
-- Dependencies: 232
-- Data for Name: nota; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY nota (id, data_cadastro, valor, modulo_id, participante_id) FROM stdin;
\.


--
-- TOC entry 2651 (class 0 OID 0)
-- Dependencies: 231
-- Name: nota_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('nota_id_seq', 1, false);


--
-- TOC entry 2568 (class 0 OID 1722427)
-- Dependencies: 234
-- Data for Name: opcao_questionario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY opcao_questionario (id, data_cadastro, ativo, descricao, ordem, questionario_id) FROM stdin;
\.


--
-- TOC entry 2652 (class 0 OID 0)
-- Dependencies: 233
-- Name: opcao_questionario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('opcao_questionario_id_seq', 1, false);


--
-- TOC entry 2570 (class 0 OID 1722435)
-- Dependencies: 236
-- Data for Name: pacote_alertas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY pacote_alertas (id, data_cadastro, mensagem, nome, evento_id, tipo_id) FROM stdin;
\.


--
-- TOC entry 2653 (class 0 OID 0)
-- Dependencies: 235
-- Name: pacote_alertas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('pacote_alertas_id_seq', 1, false);


--
-- TOC entry 2572 (class 0 OID 1722446)
-- Dependencies: 238
-- Data for Name: pais; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY pais (id, data_cadastro, descricao) FROM stdin;
1	2016-02-18 00:00:00	BRASIL
2	2016-02-18 00:00:00	ESTADOS UNIDOS
3	2016-02-18 00:00:00	ARGENTINA
4	2016-02-18 00:00:00	CHILE
5	2016-02-18 00:00:00	PORTUGAL
6	2016-02-18 00:00:00	ESPANHA
7	2016-02-18 00:00:00	JAPÃO
8	2016-02-18 00:00:00	INGLATERRA
9	2016-02-18 00:00:00	ITÁLIA
10	2016-02-18 00:00:00	CANADÁ
11	2016-02-18 00:00:00	MÉXICO
\.


--
-- TOC entry 2654 (class 0 OID 0)
-- Dependencies: 237
-- Name: pais_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('pais_id_seq', 1, false);


--
-- TOC entry 2574 (class 0 OID 1722454)
-- Dependencies: 240
-- Data for Name: participante; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY participante (id, data_cadastro, cargo, codmunicipio, codorgao, data_admissao, instituicao, lotacao, matricula, observacao, outra_necessidade_especial, pne, profissao, endereco_id, escolaridade_id, formacao_academica_id, mandato_id, pessoa_id, publico_alvo_id, tipo_necessidade_especial_id) FROM stdin;
1	2017-07-11 00:00:00	ANALISTA DE SISTEMAS	\N	\N	2016-03-16	\N	SUPERINTENDÊNCIA DE INFORMÁTICA	000001		\N	f	ANALISTA DE SISTEMAS	1	55	25	\N	1	93	\N
\.


--
-- TOC entry 2655 (class 0 OID 0)
-- Dependencies: 239
-- Name: participante_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('participante_id_seq', 1, false);


--
-- TOC entry 2576 (class 0 OID 1722465)
-- Dependencies: 242
-- Data for Name: perfil_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY perfil_usuario (id, data_cadastro, perfil, usuario_id) FROM stdin;
2	2017-07-11 00:00:00	ADMINISTRADOR	1
3	2017-07-11 00:00:00	PARTICIPANTE	1
\.


--
-- TOC entry 2656 (class 0 OID 0)
-- Dependencies: 241
-- Name: perfil_usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('perfil_usuario_id_seq', 1, false);


--
-- TOC entry 2578 (class 0 OID 1722473)
-- Dependencies: 244
-- Data for Name: pessoa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY pessoa (id, data_cadastro, celular, cpf, data_nascimento, email, nome, telefone, sexo_id) FROM stdin;
1	2017-07-11 00:00:00	(62)9999-9999	012.345.678-90	1985-01-01	admin@gmail.com	ADMINISTRADOR	(62)3200-0000	1
\.


--
-- TOC entry 2657 (class 0 OID 0)
-- Dependencies: 243
-- Name: pessoa_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('pessoa_id_seq', 1, false);


--
-- TOC entry 2580 (class 0 OID 1722484)
-- Dependencies: 246
-- Data for Name: provedor_evento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY provedor_evento (id, data_cadastro, agencia, celular, cnpj, conta_corrente, contato, descricao, email, telefone, banco_id, endereco_id) FROM stdin;
\.


--
-- TOC entry 2658 (class 0 OID 0)
-- Dependencies: 245
-- Name: provedor_evento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('provedor_evento_id_seq', 1, false);


--
-- TOC entry 2582 (class 0 OID 1722495)
-- Dependencies: 248
-- Data for Name: questionario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY questionario (id, data_cadastro, ativo, para_instrutor, pergunta) FROM stdin;
\.


--
-- TOC entry 2584 (class 0 OID 1722506)
-- Dependencies: 250
-- Data for Name: questionario_avaliacao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY questionario_avaliacao (id, data_cadastro, avaliacao_id, instrutor_id, opcao_id, questionario_id) FROM stdin;
\.


--
-- TOC entry 2659 (class 0 OID 0)
-- Dependencies: 249
-- Name: questionario_avaliacao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('questionario_avaliacao_id_seq', 1, false);


--
-- TOC entry 2660 (class 0 OID 0)
-- Dependencies: 247
-- Name: questionario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('questionario_id_seq', 1, false);


--
-- TOC entry 2586 (class 0 OID 1722514)
-- Dependencies: 252
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY usuario (id, data_cadastro, matricula, nome_usuario, codsecao, senha, token_recup_senha, usuario_id, participante_id, pessoa_id, tipo_usuario_id) FROM stdin;
1	2017-07-11 00:00:00	\N	admin	\N	E10ADC3949BA59ABBE56E057F20F883E	\N	\N	1	1	4
\.


--
-- TOC entry 2661 (class 0 OID 0)
-- Dependencies: 251
-- Name: usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('usuario_id_seq', 1, false);


--
-- TOC entry 2253 (class 2606 OID 1722204)
-- Name: alerta alerta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alerta
    ADD CONSTRAINT alerta_pkey PRIMARY KEY (id);


--
-- TOC entry 2255 (class 2606 OID 1722215)
-- Name: anexo anexo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY anexo
    ADD CONSTRAINT anexo_pkey PRIMARY KEY (id);


--
-- TOC entry 2257 (class 2606 OID 1722223)
-- Name: arquivo_adm arquivo_adm_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY arquivo_adm
    ADD CONSTRAINT arquivo_adm_pkey PRIMARY KEY (id);


--
-- TOC entry 2261 (class 2606 OID 1722245)
-- Name: avaliacao_eficacia avaliacao_eficacia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY avaliacao_eficacia
    ADD CONSTRAINT avaliacao_eficacia_pkey PRIMARY KEY (id);


--
-- TOC entry 2259 (class 2606 OID 1722234)
-- Name: avaliacao avaliacao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY avaliacao
    ADD CONSTRAINT avaliacao_pkey PRIMARY KEY (id);


--
-- TOC entry 2263 (class 2606 OID 1722253)
-- Name: banco_febraban banco_febraban_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY banco_febraban
    ADD CONSTRAINT banco_febraban_pkey PRIMARY KEY (id);


--
-- TOC entry 2265 (class 2606 OID 1722261)
-- Name: certificado certificado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY certificado
    ADD CONSTRAINT certificado_pkey PRIMARY KEY (id);


--
-- TOC entry 2267 (class 2606 OID 1722269)
-- Name: cidade cidade_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cidade
    ADD CONSTRAINT cidade_pkey PRIMARY KEY (id);


--
-- TOC entry 2269 (class 2606 OID 1722280)
-- Name: dominio dominio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dominio
    ADD CONSTRAINT dominio_pkey PRIMARY KEY (id);


--
-- TOC entry 2271 (class 2606 OID 1722291)
-- Name: endereco endereco_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY endereco
    ADD CONSTRAINT endereco_pkey PRIMARY KEY (id);


--
-- TOC entry 2273 (class 2606 OID 1722299)
-- Name: estado estado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY estado
    ADD CONSTRAINT estado_pkey PRIMARY KEY (id);


--
-- TOC entry 2275 (class 2606 OID 1722310)
-- Name: evento evento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT evento_pkey PRIMARY KEY (id);


--
-- TOC entry 2277 (class 2606 OID 1722318)
-- Name: evento_publico_alvo evento_publico_alvo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento_publico_alvo
    ADD CONSTRAINT evento_publico_alvo_pkey PRIMARY KEY (id);


--
-- TOC entry 2279 (class 2606 OID 1722326)
-- Name: frequencia frequencia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY frequencia
    ADD CONSTRAINT frequencia_pkey PRIMARY KEY (id);


--
-- TOC entry 2281 (class 2606 OID 1722337)
-- Name: gasto gasto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gasto
    ADD CONSTRAINT gasto_pkey PRIMARY KEY (id);


--
-- TOC entry 2283 (class 2606 OID 1722348)
-- Name: indicacao indicacao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY indicacao
    ADD CONSTRAINT indicacao_pkey PRIMARY KEY (id);


--
-- TOC entry 2285 (class 2606 OID 1722359)
-- Name: inscricao inscricao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inscricao
    ADD CONSTRAINT inscricao_pkey PRIMARY KEY (id);


--
-- TOC entry 2287 (class 2606 OID 1722370)
-- Name: instrutor instrutor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY instrutor
    ADD CONSTRAINT instrutor_pkey PRIMARY KEY (id);


--
-- TOC entry 2289 (class 2606 OID 1722378)
-- Name: localizacao_evento localizacao_evento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY localizacao_evento
    ADD CONSTRAINT localizacao_evento_pkey PRIMARY KEY (id);


--
-- TOC entry 2291 (class 2606 OID 1722389)
-- Name: log log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY log
    ADD CONSTRAINT log_pkey PRIMARY KEY (id);


--
-- TOC entry 2293 (class 2606 OID 1722397)
-- Name: material material_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY material
    ADD CONSTRAINT material_pkey PRIMARY KEY (id);


--
-- TOC entry 2297 (class 2606 OID 1722416)
-- Name: modulo_instrutor modulo_instrutor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY modulo_instrutor
    ADD CONSTRAINT modulo_instrutor_pkey PRIMARY KEY (id);


--
-- TOC entry 2295 (class 2606 OID 1722408)
-- Name: modulo modulo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY modulo
    ADD CONSTRAINT modulo_pkey PRIMARY KEY (id);


--
-- TOC entry 2299 (class 2606 OID 1722424)
-- Name: nota nota_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY nota
    ADD CONSTRAINT nota_pkey PRIMARY KEY (id);


--
-- TOC entry 2301 (class 2606 OID 1722432)
-- Name: opcao_questionario opcao_questionario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY opcao_questionario
    ADD CONSTRAINT opcao_questionario_pkey PRIMARY KEY (id);


--
-- TOC entry 2303 (class 2606 OID 1722443)
-- Name: pacote_alertas pacote_alertas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pacote_alertas
    ADD CONSTRAINT pacote_alertas_pkey PRIMARY KEY (id);


--
-- TOC entry 2305 (class 2606 OID 1722451)
-- Name: pais pais_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pais
    ADD CONSTRAINT pais_pkey PRIMARY KEY (id);


--
-- TOC entry 2307 (class 2606 OID 1722462)
-- Name: participante participante_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY participante
    ADD CONSTRAINT participante_pkey PRIMARY KEY (id);


--
-- TOC entry 2309 (class 2606 OID 1722470)
-- Name: perfil_usuario perfil_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil_usuario
    ADD CONSTRAINT perfil_usuario_pkey PRIMARY KEY (id);


--
-- TOC entry 2311 (class 2606 OID 1722481)
-- Name: pessoa pessoa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pessoa
    ADD CONSTRAINT pessoa_pkey PRIMARY KEY (id);


--
-- TOC entry 2313 (class 2606 OID 1722492)
-- Name: provedor_evento provedor_evento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY provedor_evento
    ADD CONSTRAINT provedor_evento_pkey PRIMARY KEY (id);


--
-- TOC entry 2317 (class 2606 OID 1722511)
-- Name: questionario_avaliacao questionario_avaliacao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questionario_avaliacao
    ADD CONSTRAINT questionario_avaliacao_pkey PRIMARY KEY (id);


--
-- TOC entry 2315 (class 2606 OID 1722503)
-- Name: questionario questionario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questionario
    ADD CONSTRAINT questionario_pkey PRIMARY KEY (id);


--
-- TOC entry 2319 (class 2606 OID 1722522)
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);


--
-- TOC entry 2374 (class 2606 OID 1722793)
-- Name: material fk11d365273cfd23e8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY material
    ADD CONSTRAINT fk11d365273cfd23e8 FOREIGN KEY (arquivo_id) REFERENCES anexo(id);


--
-- TOC entry 2372 (class 2606 OID 1722783)
-- Name: material fk11d3652779230e4e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY material
    ADD CONSTRAINT fk11d3652779230e4e FOREIGN KEY (modulo_id) REFERENCES modulo(id);


--
-- TOC entry 2375 (class 2606 OID 1722798)
-- Name: material fk11d365279210f621; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY material
    ADD CONSTRAINT fk11d365279210f621 FOREIGN KEY (tipo_material_id) REFERENCES dominio(id);


--
-- TOC entry 2373 (class 2606 OID 1722788)
-- Name: material fk11d36527ca4915ee; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY material
    ADD CONSTRAINT fk11d36527ca4915ee FOREIGN KEY (evento_id) REFERENCES evento(id);


--
-- TOC entry 2371 (class 2606 OID 1722778)
-- Name: log fk1a3441b6ecd26; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY log
    ADD CONSTRAINT fk1a3441b6ecd26 FOREIGN KEY (usuario_id) REFERENCES usuario(id);


--
-- TOC entry 2364 (class 2606 OID 1722743)
-- Name: instrutor fk2aefdae206523f0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY instrutor
    ADD CONSTRAINT fk2aefdae206523f0 FOREIGN KEY (tipo_instrutor_id) REFERENCES dominio(id);


--
-- TOC entry 2368 (class 2606 OID 1722763)
-- Name: instrutor fk2aefdae3cb4ce84; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY instrutor
    ADD CONSTRAINT fk2aefdae3cb4ce84 FOREIGN KEY (formacao_academica_id) REFERENCES dominio(id);


--
-- TOC entry 2363 (class 2606 OID 1722738)
-- Name: instrutor fk2aefdae41f4b4ae; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY instrutor
    ADD CONSTRAINT fk2aefdae41f4b4ae FOREIGN KEY (pessoa_id) REFERENCES pessoa(id);


--
-- TOC entry 2367 (class 2606 OID 1722758)
-- Name: instrutor fk2aefdae487b165d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY instrutor
    ADD CONSTRAINT fk2aefdae487b165d FOREIGN KEY (situacao_instrutor_id) REFERENCES dominio(id);


--
-- TOC entry 2369 (class 2606 OID 1722768)
-- Name: instrutor fk2aefdae59170eb2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY instrutor
    ADD CONSTRAINT fk2aefdae59170eb2 FOREIGN KEY (nivel_escolaridade_id) REFERENCES dominio(id);


--
-- TOC entry 2362 (class 2606 OID 1722733)
-- Name: instrutor fk2aefdae5ad8646e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY instrutor
    ADD CONSTRAINT fk2aefdae5ad8646e FOREIGN KEY (endereco_id) REFERENCES endereco(id);


--
-- TOC entry 2365 (class 2606 OID 1722748)
-- Name: instrutor fk2aefdae7e99318; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY instrutor
    ADD CONSTRAINT fk2aefdae7e99318 FOREIGN KEY (arquivo_assinatura_id) REFERENCES anexo(id);


--
-- TOC entry 2361 (class 2606 OID 1722728)
-- Name: instrutor fk2aefdaea58c1284; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY instrutor
    ADD CONSTRAINT fk2aefdaea58c1284 FOREIGN KEY (arquivo_projeto_id) REFERENCES anexo(id);


--
-- TOC entry 2366 (class 2606 OID 1722753)
-- Name: instrutor fk2aefdaef6c3d01b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY instrutor
    ADD CONSTRAINT fk2aefdaef6c3d01b FOREIGN KEY (arquivo_curriculo_id) REFERENCES anexo(id);


--
-- TOC entry 2328 (class 2606 OID 1722563)
-- Name: avaliacao_eficacia fk2bfdd59b93e399ce; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY avaliacao_eficacia
    ADD CONSTRAINT fk2bfdd59b93e399ce FOREIGN KEY (participante_id) REFERENCES participante(id);


--
-- TOC entry 2330 (class 2606 OID 1722573)
-- Name: avaliacao_eficacia fk2bfdd59bca4915ee; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY avaliacao_eficacia
    ADD CONSTRAINT fk2bfdd59bca4915ee FOREIGN KEY (evento_id) REFERENCES evento(id);


--
-- TOC entry 2329 (class 2606 OID 1722568)
-- Name: avaliacao_eficacia fk2bfdd59bdc93b046; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY avaliacao_eficacia
    ADD CONSTRAINT fk2bfdd59bdc93b046 FOREIGN KEY (indicacao_id) REFERENCES indicacao(id);


--
-- TOC entry 2331 (class 2606 OID 1722578)
-- Name: avaliacao_eficacia fk2bfdd59be0f5f80d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY avaliacao_eficacia
    ADD CONSTRAINT fk2bfdd59be0f5f80d FOREIGN KEY (desempenho_servico_id) REFERENCES dominio(id);


--
-- TOC entry 2380 (class 2606 OID 1722823)
-- Name: nota fk33afee79230e4e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY nota
    ADD CONSTRAINT fk33afee79230e4e FOREIGN KEY (modulo_id) REFERENCES modulo(id);


--
-- TOC entry 2379 (class 2606 OID 1722818)
-- Name: nota fk33afee93e399ce; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY nota
    ADD CONSTRAINT fk33afee93e399ce FOREIGN KEY (participante_id) REFERENCES participante(id);


--
-- TOC entry 2381 (class 2606 OID 1722828)
-- Name: opcao_questionario fk34abbb8caa1efdee; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY opcao_questionario
    ADD CONSTRAINT fk34abbb8caa1efdee FOREIGN KEY (questionario_id) REFERENCES questionario(id);


--
-- TOC entry 2382 (class 2606 OID 1722833)
-- Name: pacote_alertas fk3fad849db4c9ad7f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pacote_alertas
    ADD CONSTRAINT fk3fad849db4c9ad7f FOREIGN KEY (tipo_id) REFERENCES dominio(id);


--
-- TOC entry 2383 (class 2606 OID 1722838)
-- Name: pacote_alertas fk3fad849dca4915ee; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pacote_alertas
    ADD CONSTRAINT fk3fad849dca4915ee FOREIGN KEY (evento_id) REFERENCES evento(id);


--
-- TOC entry 2352 (class 2606 OID 1722683)
-- Name: gasto fk5d94b143a1831c8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gasto
    ADD CONSTRAINT fk5d94b143a1831c8 FOREIGN KEY (fonte_gasto_id) REFERENCES dominio(id);


--
-- TOC entry 2350 (class 2606 OID 1722673)
-- Name: gasto fk5d94b14b4c9ad7f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gasto
    ADD CONSTRAINT fk5d94b14b4c9ad7f FOREIGN KEY (tipo_id) REFERENCES dominio(id);


--
-- TOC entry 2351 (class 2606 OID 1722678)
-- Name: gasto fk5d94b14ca4915ee; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gasto
    ADD CONSTRAINT fk5d94b14ca4915ee FOREIGN KEY (evento_id) REFERENCES evento(id);


--
-- TOC entry 2370 (class 2606 OID 1722773)
-- Name: localizacao_evento fk66cd1d085ad8646e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY localizacao_evento
    ADD CONSTRAINT fk66cd1d085ad8646e FOREIGN KEY (endereco_id) REFERENCES endereco(id);


--
-- TOC entry 2336 (class 2606 OID 1722603)
-- Name: endereco fk672d67c9b34278e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY endereco
    ADD CONSTRAINT fk672d67c9b34278e FOREIGN KEY (cidade_id) REFERENCES cidade(id);


--
-- TOC entry 2334 (class 2606 OID 1722593)
-- Name: certificado fk745f3fb13cfd23e8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY certificado
    ADD CONSTRAINT fk745f3fb13cfd23e8 FOREIGN KEY (arquivo_id) REFERENCES anexo(id);


--
-- TOC entry 2332 (class 2606 OID 1722583)
-- Name: certificado fk745f3fb193e399ce; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY certificado
    ADD CONSTRAINT fk745f3fb193e399ce FOREIGN KEY (participante_id) REFERENCES participante(id);


--
-- TOC entry 2333 (class 2606 OID 1722588)
-- Name: certificado fk745f3fb1ca4915ee; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY certificado
    ADD CONSTRAINT fk745f3fb1ca4915ee FOREIGN KEY (evento_id) REFERENCES evento(id);


--
-- TOC entry 2397 (class 2606 OID 1722908)
-- Name: questionario_avaliacao fk7e7fdcd7990a7646; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questionario_avaliacao
    ADD CONSTRAINT fk7e7fdcd7990a7646 FOREIGN KEY (avaliacao_id) REFERENCES avaliacao(id);


--
-- TOC entry 2398 (class 2606 OID 1722913)
-- Name: questionario_avaliacao fk7e7fdcd7aa1efdee; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questionario_avaliacao
    ADD CONSTRAINT fk7e7fdcd7aa1efdee FOREIGN KEY (questionario_id) REFERENCES questionario(id);


--
-- TOC entry 2396 (class 2606 OID 1722903)
-- Name: questionario_avaliacao fk7e7fdcd7c3cf7ce6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questionario_avaliacao
    ADD CONSTRAINT fk7e7fdcd7c3cf7ce6 FOREIGN KEY (opcao_id) REFERENCES opcao_questionario(id);


--
-- TOC entry 2395 (class 2606 OID 1722898)
-- Name: questionario_avaliacao fk7e7fdcd7ee8e226; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questionario_avaliacao
    ADD CONSTRAINT fk7e7fdcd7ee8e226 FOREIGN KEY (instrutor_id) REFERENCES instrutor(id);


--
-- TOC entry 2349 (class 2606 OID 1722668)
-- Name: frequencia fk7e9d249579230e4e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY frequencia
    ADD CONSTRAINT fk7e9d249579230e4e FOREIGN KEY (modulo_id) REFERENCES modulo(id);


--
-- TOC entry 2348 (class 2606 OID 1722663)
-- Name: frequencia fk7e9d249593e399ce; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY frequencia
    ADD CONSTRAINT fk7e9d249593e399ce FOREIGN KEY (participante_id) REFERENCES participante(id);


--
-- TOC entry 2389 (class 2606 OID 1722868)
-- Name: participante fk89fff792206bc203; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY participante
    ADD CONSTRAINT fk89fff792206bc203 FOREIGN KEY (tipo_necessidade_especial_id) REFERENCES dominio(id);


--
-- TOC entry 2387 (class 2606 OID 1722858)
-- Name: participante fk89fff7922cec13e1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY participante
    ADD CONSTRAINT fk89fff7922cec13e1 FOREIGN KEY (mandato_id) REFERENCES dominio(id);


--
-- TOC entry 2390 (class 2606 OID 1722873)
-- Name: participante fk89fff7923cb4ce84; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY participante
    ADD CONSTRAINT fk89fff7923cb4ce84 FOREIGN KEY (formacao_academica_id) REFERENCES dominio(id);


--
-- TOC entry 2385 (class 2606 OID 1722848)
-- Name: participante fk89fff79241f4b4ae; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY participante
    ADD CONSTRAINT fk89fff79241f4b4ae FOREIGN KEY (pessoa_id) REFERENCES pessoa(id);


--
-- TOC entry 2386 (class 2606 OID 1722853)
-- Name: participante fk89fff792450c1a76; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY participante
    ADD CONSTRAINT fk89fff792450c1a76 FOREIGN KEY (publico_alvo_id) REFERENCES dominio(id);


--
-- TOC entry 2384 (class 2606 OID 1722843)
-- Name: participante fk89fff7925ad8646e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY participante
    ADD CONSTRAINT fk89fff7925ad8646e FOREIGN KEY (endereco_id) REFERENCES endereco(id);


--
-- TOC entry 2388 (class 2606 OID 1722863)
-- Name: participante fk89fff792d71a60ef; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY participante
    ADD CONSTRAINT fk89fff792d71a60ef FOREIGN KEY (escolaridade_id) REFERENCES dominio(id);


--
-- TOC entry 2323 (class 2606 OID 1722538)
-- Name: arquivo_adm fka18d9c181a23236b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY arquivo_adm
    ADD CONSTRAINT fka18d9c181a23236b FOREIGN KEY (tipo_arquivoadm_id) REFERENCES dominio(id);


--
-- TOC entry 2325 (class 2606 OID 1722548)
-- Name: arquivo_adm fka18d9c183cfd23e8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY arquivo_adm
    ADD CONSTRAINT fka18d9c183cfd23e8 FOREIGN KEY (arquivo_id) REFERENCES anexo(id);


--
-- TOC entry 2324 (class 2606 OID 1722543)
-- Name: arquivo_adm fka18d9c18ca4915ee; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY arquivo_adm
    ADD CONSTRAINT fka18d9c18ca4915ee FOREIGN KEY (evento_id) REFERENCES evento(id);


--
-- TOC entry 2322 (class 2606 OID 1722533)
-- Name: alerta fkaba7a2857bf4c289; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alerta
    ADD CONSTRAINT fkaba7a2857bf4c289 FOREIGN KEY (pacote_alertas_id) REFERENCES pacote_alertas(id);


--
-- TOC entry 2321 (class 2606 OID 1722528)
-- Name: alerta fkaba7a28593e399ce; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alerta
    ADD CONSTRAINT fkaba7a28593e399ce FOREIGN KEY (participante_id) REFERENCES participante(id);


--
-- TOC entry 2320 (class 2606 OID 1722523)
-- Name: alerta fkaba7a285b4c9ad7f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alerta
    ADD CONSTRAINT fkaba7a285b4c9ad7f FOREIGN KEY (tipo_id) REFERENCES dominio(id);


--
-- TOC entry 2335 (class 2606 OID 1722598)
-- Name: cidade fkaee6572494eee84e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cidade
    ADD CONSTRAINT fkaee6572494eee84e FOREIGN KEY (estado_id) REFERENCES estado(id);


--
-- TOC entry 2337 (class 2606 OID 1722608)
-- Name: estado fkb2e439663ac17d2e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY estado
    ADD CONSTRAINT fkb2e439663ac17d2e FOREIGN KEY (pais_id) REFERENCES pais(id);


--
-- TOC entry 2340 (class 2606 OID 1722623)
-- Name: evento fkb307e1151d45f3d9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT fkb307e1151d45f3d9 FOREIGN KEY (modalidade_id) REFERENCES dominio(id);


--
-- TOC entry 2345 (class 2606 OID 1722648)
-- Name: evento fkb307e11596030e9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT fkb307e11596030e9 FOREIGN KEY (eixo_tematico_id) REFERENCES dominio(id);


--
-- TOC entry 2339 (class 2606 OID 1722618)
-- Name: evento fkb307e115990a3d86; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT fkb307e115990a3d86 FOREIGN KEY (area_tribunal_id) REFERENCES dominio(id);


--
-- TOC entry 2342 (class 2606 OID 1722633)
-- Name: evento fkb307e115b0e5e203; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT fkb307e115b0e5e203 FOREIGN KEY (imagem_id) REFERENCES anexo(id);


--
-- TOC entry 2343 (class 2606 OID 1722638)
-- Name: evento fkb307e115b14b299b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT fkb307e115b14b299b FOREIGN KEY (localizacao_id) REFERENCES localizacao_evento(id);


--
-- TOC entry 2338 (class 2606 OID 1722613)
-- Name: evento fkb307e115c53724b3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT fkb307e115c53724b3 FOREIGN KEY (tipo_evento_id) REFERENCES dominio(id);


--
-- TOC entry 2344 (class 2606 OID 1722643)
-- Name: evento fkb307e115ceb3d323; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT fkb307e115ceb3d323 FOREIGN KEY (provedor_id) REFERENCES provedor_evento(id);


--
-- TOC entry 2341 (class 2606 OID 1722628)
-- Name: evento fkb307e115ed7488da; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT fkb307e115ed7488da FOREIGN KEY (responsavel_evento) REFERENCES participante(id);


--
-- TOC entry 2378 (class 2606 OID 1722813)
-- Name: modulo_instrutor fkb5ce832579230e4e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY modulo_instrutor
    ADD CONSTRAINT fkb5ce832579230e4e FOREIGN KEY (modulo_id) REFERENCES modulo(id);


--
-- TOC entry 2377 (class 2606 OID 1722808)
-- Name: modulo_instrutor fkb5ce8325ee8e226; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY modulo_instrutor
    ADD CONSTRAINT fkb5ce8325ee8e226 FOREIGN KEY (instrutor_id) REFERENCES instrutor(id);


--
-- TOC entry 2376 (class 2606 OID 1722803)
-- Name: modulo fkc04ba676ca4915ee; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY modulo
    ADD CONSTRAINT fkc04ba676ca4915ee FOREIGN KEY (evento_id) REFERENCES evento(id);


--
-- TOC entry 2392 (class 2606 OID 1722883)
-- Name: pessoa fkc4e40fa77980cbca; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pessoa
    ADD CONSTRAINT fkc4e40fa77980cbca FOREIGN KEY (sexo_id) REFERENCES dominio(id);


--
-- TOC entry 2354 (class 2606 OID 1722693)
-- Name: indicacao fkd58f7c898d70f2a5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY indicacao
    ADD CONSTRAINT fkd58f7c898d70f2a5 FOREIGN KEY (avaliador_id) REFERENCES usuario(id);


--
-- TOC entry 2353 (class 2606 OID 1722688)
-- Name: indicacao fkd58f7c8993e399ce; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY indicacao
    ADD CONSTRAINT fkd58f7c8993e399ce FOREIGN KEY (participante_id) REFERENCES participante(id);


--
-- TOC entry 2355 (class 2606 OID 1722698)
-- Name: indicacao fkd58f7c89ca4915ee; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY indicacao
    ADD CONSTRAINT fkd58f7c89ca4915ee FOREIGN KEY (evento_id) REFERENCES evento(id);


--
-- TOC entry 2356 (class 2606 OID 1722703)
-- Name: indicacao fkd58f7c89e938e155; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY indicacao
    ADD CONSTRAINT fkd58f7c89e938e155 FOREIGN KEY (chefe_id) REFERENCES usuario(id);


--
-- TOC entry 2327 (class 2606 OID 1722558)
-- Name: avaliacao fkd935d09979230e4e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY avaliacao
    ADD CONSTRAINT fkd935d09979230e4e FOREIGN KEY (modulo_id) REFERENCES modulo(id);


--
-- TOC entry 2326 (class 2606 OID 1722553)
-- Name: avaliacao fkd935d09993e399ce; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY avaliacao
    ADD CONSTRAINT fkd935d09993e399ce FOREIGN KEY (participante_id) REFERENCES participante(id);


--
-- TOC entry 2359 (class 2606 OID 1722718)
-- Name: inscricao fke5a6e5258d70f2a5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inscricao
    ADD CONSTRAINT fke5a6e5258d70f2a5 FOREIGN KEY (avaliador_id) REFERENCES usuario(id);


--
-- TOC entry 2357 (class 2606 OID 1722708)
-- Name: inscricao fke5a6e52593e399ce; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inscricao
    ADD CONSTRAINT fke5a6e52593e399ce FOREIGN KEY (participante_id) REFERENCES participante(id);


--
-- TOC entry 2360 (class 2606 OID 1722723)
-- Name: inscricao fke5a6e525ca4915ee; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inscricao
    ADD CONSTRAINT fke5a6e525ca4915ee FOREIGN KEY (evento_id) REFERENCES evento(id);


--
-- TOC entry 2358 (class 2606 OID 1722713)
-- Name: inscricao fke5a6e525dc93b046; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inscricao
    ADD CONSTRAINT fke5a6e525dc93b046 FOREIGN KEY (indicacao_id) REFERENCES indicacao(id);


--
-- TOC entry 2393 (class 2606 OID 1722888)
-- Name: provedor_evento fke6c902295ad8646e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY provedor_evento
    ADD CONSTRAINT fke6c902295ad8646e FOREIGN KEY (endereco_id) REFERENCES endereco(id);


--
-- TOC entry 2394 (class 2606 OID 1722893)
-- Name: provedor_evento fke6c9022998e02803; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY provedor_evento
    ADD CONSTRAINT fke6c9022998e02803 FOREIGN KEY (banco_id) REFERENCES banco_febraban(id);


--
-- TOC entry 2391 (class 2606 OID 1722878)
-- Name: perfil_usuario fkee9777bb1b6ecd26; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil_usuario
    ADD CONSTRAINT fkee9777bb1b6ecd26 FOREIGN KEY (usuario_id) REFERENCES usuario(id);


--
-- TOC entry 2347 (class 2606 OID 1722658)
-- Name: evento_publico_alvo fkf6cf9487450c1a76; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento_publico_alvo
    ADD CONSTRAINT fkf6cf9487450c1a76 FOREIGN KEY (publico_alvo_id) REFERENCES dominio(id);


--
-- TOC entry 2346 (class 2606 OID 1722653)
-- Name: evento_publico_alvo fkf6cf9487ca4915ee; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento_publico_alvo
    ADD CONSTRAINT fkf6cf9487ca4915ee FOREIGN KEY (evento_id) REFERENCES evento(id);


--
-- TOC entry 2401 (class 2606 OID 1722928)
-- Name: usuario fkf814f32e41f4b4ae; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT fkf814f32e41f4b4ae FOREIGN KEY (pessoa_id) REFERENCES pessoa(id);


--
-- TOC entry 2400 (class 2606 OID 1722923)
-- Name: usuario fkf814f32e93e399ce; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT fkf814f32e93e399ce FOREIGN KEY (participante_id) REFERENCES participante(id);


--
-- TOC entry 2399 (class 2606 OID 1722918)
-- Name: usuario fkf814f32edc800d30; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT fkf814f32edc800d30 FOREIGN KEY (tipo_usuario_id) REFERENCES dominio(id);


-- Completed on 2017-08-09 14:30:11

--
-- PostgreSQL database dump complete
--

