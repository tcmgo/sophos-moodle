--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.1
-- Dumped by pg_dump version 10.0

-- Started on 2017-11-22 08:23:21

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2267 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 186 (class 1259 OID 1724527)
-- Name: modulo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE modulo (
    id bigint NOT NULL,
    descricao character varying(255),
    interno boolean,
    loginurl character varying(255),
    padraourl character varying(255)
);


ALTER TABLE modulo OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 1724525)
-- Name: modulo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE modulo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE modulo_id_seq OWNER TO postgres;

--
-- TOC entry 2268 (class 0 OID 0)
-- Dependencies: 185
-- Name: modulo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE modulo_id_seq OWNED BY modulo.id;


--
-- TOC entry 188 (class 1259 OID 1724538)
-- Name: municipio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE municipio (
    codmunicipio bigint NOT NULL,
    cnpjmunicipio character varying(255),
    nomemunicipio character varying(255),
    regiao character varying(255)
);


ALTER TABLE municipio OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 1724536)
-- Name: municipio_codmunicipio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE municipio_codmunicipio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE municipio_codmunicipio_seq OWNER TO postgres;

--
-- TOC entry 2269 (class 0 OID 0)
-- Dependencies: 187
-- Name: municipio_codmunicipio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE municipio_codmunicipio_seq OWNED BY municipio.codmunicipio;


--
-- TOC entry 189 (class 1259 OID 1724547)
-- Name: orgaos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE orgaos (
    codmunicipio bigint NOT NULL,
    codorgao bigint NOT NULL,
    ativo boolean,
    ceplograorgao character varying(255),
    descorgao character varying(255),
    lograorgao character varying(255),
    setorlograorgao character varying(255),
    tipoorgao character varying(255)
);


ALTER TABLE orgaos OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 1724557)
-- Name: papel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE papel (
    id bigint NOT NULL,
    descricao character varying(255),
    modulo_id bigint
);


ALTER TABLE papel OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 1724555)
-- Name: papel_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE papel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE papel_id_seq OWNER TO postgres;

--
-- TOC entry 2270 (class 0 OID 0)
-- Dependencies: 190
-- Name: papel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE papel_id_seq OWNED BY papel.id;


--
-- TOC entry 192 (class 1259 OID 1724563)
-- Name: papelregra; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE papelregra (
    papel_id bigint NOT NULL,
    regra_id bigint NOT NULL
);


ALTER TABLE papelregra OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 1724568)
-- Name: pessoa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE pessoa (
    id bigint NOT NULL,
    bairro character varying(255),
    cep character varying(255),
    cidade character varying(255),
    cpf character varying(255),
    datanascimento timestamp without time zone,
    email character varying(255),
    identidade character varying(255),
    logradouro character varying(255),
    nome character varying(255),
    nomemae character varying(255),
    orgaoexpedidor character varying(255),
    sexo character varying(255),
    uf character varying(255)
);


ALTER TABLE pessoa OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 1724566)
-- Name: pessoa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE pessoa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pessoa_id_seq OWNER TO postgres;

--
-- TOC entry 2271 (class 0 OID 0)
-- Dependencies: 193
-- Name: pessoa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE pessoa_id_seq OWNED BY pessoa.id;


--
-- TOC entry 195 (class 1259 OID 1724577)
-- Name: pessoatelefone; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE pessoatelefone (
    pessoa_id bigint NOT NULL,
    telefone_id bigint NOT NULL
);


ALTER TABLE pessoatelefone OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 1724582)
-- Name: regra; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE regra (
    id bigint NOT NULL,
    descricao character varying(255),
    rotulo character varying(255),
    modulo_id bigint
);


ALTER TABLE regra OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 1724580)
-- Name: regra_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE regra_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE regra_id_seq OWNER TO postgres;

--
-- TOC entry 2272 (class 0 OID 0)
-- Dependencies: 196
-- Name: regra_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE regra_id_seq OWNED BY regra.id;


--
-- TOC entry 198 (class 1259 OID 1724591)
-- Name: secao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE secao (
    codsecao character varying(255) NOT NULL,
    abrevsecao character varying(255),
    situacao character varying(255),
    descrsecao character varying(255),
    distribuir boolean,
    faztramitacao boolean,
    prazoanalise smallint,
    senhasecao character varying(255),
    tpsecao character varying(255)
);


ALTER TABLE secao OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 1724601)
-- Name: telefone; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE telefone (
    id bigint NOT NULL,
    ddd character varying(255),
    telefone character varying(255),
    tipo character varying(255)
);


ALTER TABLE telefone OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 1724599)
-- Name: telefone_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE telefone_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE telefone_id_seq OWNER TO postgres;

--
-- TOC entry 2273 (class 0 OID 0)
-- Dependencies: 199
-- Name: telefone_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE telefone_id_seq OWNED BY telefone.id;


--
-- TOC entry 202 (class 1259 OID 1724612)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE usuario (
    usuarioid bigint NOT NULL,
    situacao boolean,
    secaoid character varying(255),
    email character varying(255),
    master character varying(255),
    matricula character varying(255),
    nomecompleto character varying(255),
    nomecriador character varying(255),
    nomeusuario character varying(255),
    observacao character varying(255),
    resetsenha boolean,
    senha character varying(255),
    tipousuarioid integer
);


ALTER TABLE usuario OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 1724610)
-- Name: usuario_usuarioid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE usuario_usuarioid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuario_usuarioid_seq OWNER TO postgres;

--
-- TOC entry 2274 (class 0 OID 0)
-- Dependencies: 201
-- Name: usuario_usuarioid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE usuario_usuarioid_seq OWNED BY usuario.usuarioid;


--
-- TOC entry 203 (class 1259 OID 1724621)
-- Name: usuariochefesecao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE usuariochefesecao (
    secaoid bigint NOT NULL,
    usuarioid bigint NOT NULL
);


ALTER TABLE usuariochefesecao OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 1724628)
-- Name: usuarioexterno; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE usuarioexterno (
    id bigint NOT NULL,
    ativoweb boolean,
    cargo character varying(255),
    datacriacao timestamp without time zone,
    login character varying(255),
    observacao character varying(255),
    senha character varying(255),
    pessoa_id bigint
);


ALTER TABLE usuarioexterno OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 1724626)
-- Name: usuarioexterno_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE usuarioexterno_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuarioexterno_id_seq OWNER TO postgres;

--
-- TOC entry 2275 (class 0 OID 0)
-- Dependencies: 204
-- Name: usuarioexterno_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE usuarioexterno_id_seq OWNED BY usuarioexterno.id;


--
-- TOC entry 207 (class 1259 OID 1724639)
-- Name: usuariopapelmodulo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE usuariopapelmodulo (
    id bigint NOT NULL,
    tipousuario character varying(255),
    usuarioid bigint,
    modulo_id bigint,
    papel_id bigint
);


ALTER TABLE usuariopapelmodulo OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 1724637)
-- Name: usuariopapelmodulo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE usuariopapelmodulo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuariopapelmodulo_id_seq OWNER TO postgres;

--
-- TOC entry 2276 (class 0 OID 0)
-- Dependencies: 206
-- Name: usuariopapelmodulo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE usuariopapelmodulo_id_seq OWNED BY usuariopapelmodulo.id;


--
-- TOC entry 2078 (class 2604 OID 1724530)
-- Name: modulo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY modulo ALTER COLUMN id SET DEFAULT nextval('modulo_id_seq'::regclass);


--
-- TOC entry 2079 (class 2604 OID 1724541)
-- Name: municipio codmunicipio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY municipio ALTER COLUMN codmunicipio SET DEFAULT nextval('municipio_codmunicipio_seq'::regclass);


--
-- TOC entry 2080 (class 2604 OID 1724560)
-- Name: papel id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY papel ALTER COLUMN id SET DEFAULT nextval('papel_id_seq'::regclass);


--
-- TOC entry 2081 (class 2604 OID 1724571)
-- Name: pessoa id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pessoa ALTER COLUMN id SET DEFAULT nextval('pessoa_id_seq'::regclass);


--
-- TOC entry 2082 (class 2604 OID 1724585)
-- Name: regra id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY regra ALTER COLUMN id SET DEFAULT nextval('regra_id_seq'::regclass);


--
-- TOC entry 2083 (class 2604 OID 1724604)
-- Name: telefone id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY telefone ALTER COLUMN id SET DEFAULT nextval('telefone_id_seq'::regclass);


--
-- TOC entry 2084 (class 2604 OID 1724615)
-- Name: usuario usuarioid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario ALTER COLUMN usuarioid SET DEFAULT nextval('usuario_usuarioid_seq'::regclass);


--
-- TOC entry 2085 (class 2604 OID 1724631)
-- Name: usuarioexterno id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuarioexterno ALTER COLUMN id SET DEFAULT nextval('usuarioexterno_id_seq'::regclass);


--
-- TOC entry 2086 (class 2604 OID 1724642)
-- Name: usuariopapelmodulo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuariopapelmodulo ALTER COLUMN id SET DEFAULT nextval('usuariopapelmodulo_id_seq'::regclass);


--
-- TOC entry 2239 (class 0 OID 1724527)
-- Dependencies: 186
-- Data for Name: modulo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY modulo (id, descricao, interno, loginurl, padraourl) FROM stdin;
\.


--
-- TOC entry 2241 (class 0 OID 1724538)
-- Dependencies: 188
-- Data for Name: municipio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY municipio (codmunicipio, cnpjmunicipio, nomemunicipio, regiao) FROM stdin;
386	01613940000119	ABADIA GOIAS	1
1	01298330000178	ABADIANIA	3
249	02218683000183	ACREUNA	5
2	25108291000167	ADELANDIA	2
3	25141292000103	AGUA FRIA GOIAS	3
4	01173053000177	AGUA LIMPA	6
387	01616520000196	AGUAS LINDAS GOIAS	3
5	01298975000100	ALEXANIA	3
6	01345537000156	ALOANDIA	6
287	33331604000170	ALTO HORIZONTE	4
7	01740455000106	ALTO PARAISO	3
8	02367597000132	ALVORADA NORTE	3
388	01492098000104	AMARALINA	4
9	00007344000122	AMERICANO BRASIL	2
10	01067073000163	AMORINOPOLIS	2
11	01067479000146	ANAPOLIS	4
16	01127430000131	ANHANGUERA	6
17	02262368000153	ANICUNS	5
19	01005727000124	APARECIDA GOIANIA	1
290	24859316000100	APARECIDA RIO DOCE	5
20	02186336000116	APORE	5
21	01318898000103	ARACU	2
22	02125227000199	ARAGARCAS	2
23	01215474000113	ARAGOIANIA	1
24	00163147000100	ARAGUAPAZ	2
25	00007914000184	ARENOPOLIS	2
26	01067081000100	ARUANA	2
27	02320364000184	AURILANDIA	2
28	01215839000100	AVELINOPOLIS	5
29	01067131000159	BALIZA	2
30	02355675000189	BARRO ALTO	4
32	01005917000141	BELA VISTA GOIAS	6
33	02186708000104	BOM JARDIM GOIAS	2
34	01149624000138	BOM JESUS GOIAS	6
35	24857096000177	BONFINOPOLIS	1
389	01634272000106	BONOPOLIS	4
36	01756741000160	BRAZABRANTES	2
37	02296325000199	BRITANIA	2
38	01345909000144	BURITI ALEGRE	6
289	26867770000120	BURITI GOIAS	2
281	24856569000111	BURITINOPOLIS	3
39	01740430000102	CABECEIRAS	3
40	02056760000146	CACHOEIRA ALTA	5
41	00079806000117	CACHOEIRA DOURADA	6
42	02164820000144	CACHOEIRA GOIAS	2
43	01164292000160	CACU	5
44	01164946000156	CAIAPONIA	5
45	01787506000155	CALDAS NOVAS	6
297	37622149000112	CALDAZINHA	1
46	02262236000121	CAMPESTRE	1
47	00145789000179	CAMPINACU	4
48	02215747000192	CAMPINORTE	4
49	01763614000198	CAMPO ALEGRE GOIAS	6
512	04216593000189	CAMPO LIMPO GOIAS	4
50	01126143000107	CAMPOS BELOS	3
51	01493998000176	CAMPOS VERDES	4
52	02542538000153	CARMO RIO VERDE	4
293	37275849000188	CASTELANDIA	5
53	01505643000150	CATALAO	6
58	01319326000149	CATURAI	2
59	01738772000198	CAVALCANTE	3
60	01131713000157	CERES	4
61	25043530000148	CEZARINA	5
291	24859332000194	CHAPADAO CEU	5
283	36862621000121	CIDADE OCIDENTAL	3
275	36985463000105	COCALZINHO GOIAS	3
62	25105255000140	COLINAS SUL	3
63	02321115000103	CORREGO OURO	2
64	01118850000151	CORUMBA GOIAS	3
65	01302603000100	CORUMBAIBA	6
66	01138122000101	CRISTALINA	3
67	01180645000116	CRISTIANOPOLIS	6
68	02382067000163	CRIXAS	4
69	02073211000180	CROMINIA	6
70	01302728000130	CUMARI	6
71	01740505000155	DAMIANOPOLIS	3
72	01067164000107	DAMOLANDIA	2
73	01130277000100	DAVINOPOLIS	6
74	01335363000140	DIORAMA	2
75	01067206000100	DIVINOPOLIS	3
76	00078790000128	DOVERLANDIA	5
77	24852618000148	EDEALINA	6
78	01788082000143	EDEIA	5
79	01800465000190	ESTRELA NORTE	4
80	25141318000113	FAINA	2
81	01915313000132	FAZENDA NOVA	2
82	02321917000113	FIRMINOPOLIS	2
83	01740497000147	FLORES GOIAS	3
84	01738780000134	FORMOSA	3
85	02395812000109	FORMOSO	4
475	04223461000184	GAMELEIRA GOIAS	6
86	02506012000118	GOIANAPOLIS	1
87	01303221000100	GOIANDIRA	6
88	01065846000172	GOIANESIA	4
89	01612092000123	GOIANIA	1
99	01291707000167	GOIANIRA	1
100	02295772000123	GOIAS	2
101	01753722000180	GOIATUBA	6
102	25040122000132	GOUVELANDIA	5
103	01373497000156	GUAPO	1
280	26873059000188	GUARAITA	2
104	01740588000182	GUARANI GOIAS	3
105	01494178000107	GUARINOS	4
106	02296002000103	HEITORAI	2
107	01105329000180	HIDROLANDIA	6
108	01067230000130	HIDROLINA	4
109	01740448000104	IACIARA	3
295	26923755000151	INACIOLANDIA	5
110	00005959000110	INDIARA	5
111	01153030000109	INHUMAS	2
112	01763606000141	IPAMERI	6
496	04215377000119	IPIRANGA GOIAS	4
113	01157536000188	IPORA	2
115	01067248000132	ISRAELANDIA	2
116	02451938000153	ITABERAI	2
118	24850109000186	ITAGUARI	2
119	01067255000134	ITAGUARU	2
120	02186757000147	ITAJA	5
121	01134808000124	ITAPACI	4
122	02024933000144	ITAPIRAPUA	2
123	01146604000103	ITAPURANGA	2
124	01067271000127	ITARUMA	5
125	00167437000114	ITAUCU	2
126	02204196000161	ITUMBIARA	6
135	02321891000103	IVOLANDIA	2
136	02879138000138	JANDAIA	5
137	01223916000173	JARAGUA	4
139	01165729000180	JATAI	5
141	01767342000102	JAUPACI	2
285	37623501000134	JESUPOLIS	4
142	02029957000196	JOVIANIA	6
143	02922128000138	JUSSARA	2
514	04215178000100	LAGOA SANTA	5
144	01067305000183	LEOPOLDO BULHOES	6
145	01169416000109	LUZIANIA	3
146	01067842000123	MAIRIPOTABA	6
147	01740463000152	MAMBAI	3
148	00007468000108	MARA ROSA	4
149	01174580000104	MARZAGAO	6
150	24850216000104	MATRINCHA	2
251	02056752000108	MAURILANDIA	5
151	25053430000100	MIMOSO GOIAS	3
152	02215275000178	MINACU	4
153	02316537000190	MINEIROS	5
156	02321909000177	MOIPORA	2
157	01126341000170	MONTE ALEGRE GOIAS	3
250	01767722000139	MONTES CLAROS GOIAS	2
158	25043571000134	MONTIVIDIU	5
286	25005166000121	MONTIVIDIU NORTE	4
159	01789551000149	MORRINHOS	6
163	25043621000183	MORRO AGUDO GOIAS	2
164	02267698000131	MOSSAMEDES	2
165	01135227000107	MOZARLANDIA	2
166	00163055000112	MUNDO NOVO	4
168	01799683000151	MUTUNOPOLIS	4
169	01373620000139	NAZARIO	5
170	01105626000125	NEROPOLIS	1
171	02215895000107	NIQUELANDIA	4
172	01135409000188	NOVA AMERICA	4
173	01303619000138	NOVA AURORA	6
174	00236968000111	NOVA CRIXAS	4
175	00098095000128	NOVA GLORIA	4
288	33331661000159	NOVA IGUACU	4
176	01067925000112	NOVA ROMA	3
177	01123678000124	NOVA VENEZA	1
178	00006874000156	NOVO BRASIL	2
390	01629276000104	NOVO GAMA	3
179	25041005000193	NOVO PLANALTO	4
180	02385839000110	ORIZONA	6
181	01485531000184	OURO VERDE	1
182	01131010000129	OUVIDOR	6
183	01170331000132	PADRE BERNARDO	3
184	24858102000100	PALESTINA GOIAS	2
185	02394757000132	PALMEIRAS GOIAS	5
186	01181239000178	PALMELO	6
187	01178573000172	PALMINOPOLIS	5
188	00079830000156	PANAMA	6
189	02056745000106	PARANAIGUARA	5
190	02394765000189	PARAUNA	5
292	24859324000148	PEROLANDIA	5
191	01825413000178	PETROLINA GOIAS	4
192	02647303000126	PILAR GOIAS	4
193	01179647000195	PIRACANJUBA	6
194	01168145000169	PIRANHAS	2
195	01067941000105	PIRENOPOLIS	3
196	01181585000156	PIRES RIO	6
197	01740422000166	PLANALTINA	3
198	01791276000106	PONTALINA	6
199	01801612000146	PORANGATU	4
391	01617413000182	PORTEIRAO	6
200	02317378000149	PORTELANDIA	5
201	01743335000162	POSSE	3
296	37388295000125	PROFESSOR JAMIL	6
248	02056737000151	QUIRINOPOLIS	1
202	01135904000197	RIALMA	4
203	01300094000187	RIANAPOLIS	4
204	24852675000127	RIO QUENTE	6
205	02056729000105	RIO VERDE	5
207	02382836000123	RUBIATABA	4
208	02164804000151	SANCLERLANDIA	2
209	02264166000140	SANTA BARBARA GOIAS	1
210	02669976000187	SANTA CRUZ GOIAS	6
211	25107517000105	SANTA FE GOIAS	2
212	02056711000103	SANTA HELENA GOIAS	5
213	00027722000130	SANTA ISABEL	4
214	01067990000148	SANTA RITA ARAGUAIA	5
392	01612756000154	SANTA RITA NOVO DESTINO	4
215	01761113000172	SANTA ROSA GOIAS	2
216	02073484000124	SANTA TEREZA GOIAS	4
217	01137116000130	SANTA TEREZINHA GOIAS	4
294	37275823000130	SANTO ANTONIO BARRA	5
218	00097857000171	SANTO ANTONIO DESCOBERTO	3
298	37623485000180	SANTO ANTONIO GOIAS	1
219	01068014000100	SAO DOMINGOS	3
220	02468437000180	SAO FRANCISCO GOIAS	4
221	01313113000100	SAO JOAO DALIANCA	3
222	25105222000108	SAO JOAO PARAUNA	5
224	02320406000187	SAO LUIS MONTES BELOS	2
225	25043639000185	SAO LUIZ NORTE	4
226	02391654000119	SAO MIGUEL ARAGUAIA	4
227	24862864000180	SAO MIGUEL PASSA QUATRO	6
393	01616670000108	SAO PATRICIO	4
228	02056778000148	SAO SIMAO	5
229	25107525000151	SENADOR CANEDO	1
230	01343086000118	SERRANOPOLIS	5
231	01068030000100	SILVANIA	6
232	24855058000185	SIMOLANDIA	3
233	01740489000109	SITIO DABADIA	3
234	01068055000104	TAQUARAL	2
235	25105339000183	TERESINA GOIAS	3
279	36985455000150	TEREZOPOLIS	1
236	01304286000161	TRES RANCHOS	6
237	01217538000115	TRINDADE	1
238	25004771000188	TROMBAS	4
239	02321883000167	TURVANIA	5
240	25107657000183	TURVELANDIA	5
284	37622164000160	UIRAPURU	4
241	01219807000182	URUACU	4
243	02295640000100	URUANA	4
244	01763622000134	URUTAI	6
394	01616319000109	VALPARAISO GOIAS	3
245	01218643000179	VARJAO	5
246	01299692000183	VIANOPOLIS	6
247	00044834000107	VICENTINOPOLIS	6
282	37388378000114	VILA BOA	3
395	01612817000183	VILA PROPICIO	3
\.


--
-- TOC entry 2242 (class 0 OID 1724547)
-- Dependencies: 189
-- Data for Name: orgaos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY orgaos (codmunicipio, codorgao, ativo, ceplograorgao, descorgao, lograorgao, setorlograorgao, tipoorgao) FROM stdin;
\.


--
-- TOC entry 2244 (class 0 OID 1724557)
-- Dependencies: 191
-- Data for Name: papel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY papel (id, descricao, modulo_id) FROM stdin;
\.


--
-- TOC entry 2245 (class 0 OID 1724563)
-- Dependencies: 192
-- Data for Name: papelregra; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY papelregra (papel_id, regra_id) FROM stdin;
\.


--
-- TOC entry 2247 (class 0 OID 1724568)
-- Dependencies: 194
-- Data for Name: pessoa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY pessoa (id, bairro, cep, cidade, cpf, datanascimento, email, identidade, logradouro, nome, nomemae, orgaoexpedidor, sexo, uf) FROM stdin;
\.


--
-- TOC entry 2248 (class 0 OID 1724577)
-- Dependencies: 195
-- Data for Name: pessoatelefone; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY pessoatelefone (pessoa_id, telefone_id) FROM stdin;
\.


--
-- TOC entry 2250 (class 0 OID 1724582)
-- Dependencies: 197
-- Data for Name: regra; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY regra (id, descricao, rotulo, modulo_id) FROM stdin;
\.


--
-- TOC entry 2251 (class 0 OID 1724591)
-- Dependencies: 198
-- Data for Name: secao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY secao (codsecao, abrevsecao, situacao, descrsecao, distribuir, faztramitacao, prazoanalise, senhasecao, tpsecao) FROM stdin;
\.


--
-- TOC entry 2253 (class 0 OID 1724601)
-- Dependencies: 200
-- Data for Name: telefone; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY telefone (id, ddd, telefone, tipo) FROM stdin;
\.


--
-- TOC entry 2255 (class 0 OID 1724612)
-- Dependencies: 202
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY usuario (usuarioid, situacao, secaoid, email, master, matricula, nomecompleto, nomecriador, nomeusuario, observacao, resetsenha, senha, tipousuarioid) FROM stdin;
\.


--
-- TOC entry 2256 (class 0 OID 1724621)
-- Dependencies: 203
-- Data for Name: usuariochefesecao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY usuariochefesecao (secaoid, usuarioid) FROM stdin;
\.


--
-- TOC entry 2258 (class 0 OID 1724628)
-- Dependencies: 205
-- Data for Name: usuarioexterno; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY usuarioexterno (id, ativoweb, cargo, datacriacao, login, observacao, senha, pessoa_id) FROM stdin;
\.


--
-- TOC entry 2260 (class 0 OID 1724639)
-- Dependencies: 207
-- Data for Name: usuariopapelmodulo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY usuariopapelmodulo (id, tipousuario, usuarioid, modulo_id, papel_id) FROM stdin;
\.


--
-- TOC entry 2277 (class 0 OID 0)
-- Dependencies: 185
-- Name: modulo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('modulo_id_seq', 1, false);


--
-- TOC entry 2278 (class 0 OID 0)
-- Dependencies: 187
-- Name: municipio_codmunicipio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('municipio_codmunicipio_seq', 1, false);


--
-- TOC entry 2279 (class 0 OID 0)
-- Dependencies: 190
-- Name: papel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('papel_id_seq', 1, false);


--
-- TOC entry 2280 (class 0 OID 0)
-- Dependencies: 193
-- Name: pessoa_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('pessoa_id_seq', 1, false);


--
-- TOC entry 2281 (class 0 OID 0)
-- Dependencies: 196
-- Name: regra_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('regra_id_seq', 1, false);


--
-- TOC entry 2282 (class 0 OID 0)
-- Dependencies: 199
-- Name: telefone_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('telefone_id_seq', 1, false);


--
-- TOC entry 2283 (class 0 OID 0)
-- Dependencies: 201
-- Name: usuario_usuarioid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('usuario_usuarioid_seq', 1, false);


--
-- TOC entry 2284 (class 0 OID 0)
-- Dependencies: 204
-- Name: usuarioexterno_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('usuarioexterno_id_seq', 1, false);


--
-- TOC entry 2285 (class 0 OID 0)
-- Dependencies: 206
-- Name: usuariopapelmodulo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('usuariopapelmodulo_id_seq', 1, false);


--
-- TOC entry 2088 (class 2606 OID 1724535)
-- Name: modulo modulo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY modulo
    ADD CONSTRAINT modulo_pkey PRIMARY KEY (id);


--
-- TOC entry 2090 (class 2606 OID 1724546)
-- Name: municipio municipio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY municipio
    ADD CONSTRAINT municipio_pkey PRIMARY KEY (codmunicipio);


--
-- TOC entry 2092 (class 2606 OID 1724554)
-- Name: orgaos orgaos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY orgaos
    ADD CONSTRAINT orgaos_pkey PRIMARY KEY (codmunicipio, codorgao);


--
-- TOC entry 2094 (class 2606 OID 1724562)
-- Name: papel papel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY papel
    ADD CONSTRAINT papel_pkey PRIMARY KEY (id);


--
-- TOC entry 2096 (class 2606 OID 1724576)
-- Name: pessoa pessoa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pessoa
    ADD CONSTRAINT pessoa_pkey PRIMARY KEY (id);


--
-- TOC entry 2098 (class 2606 OID 1724590)
-- Name: regra regra_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY regra
    ADD CONSTRAINT regra_pkey PRIMARY KEY (id);


--
-- TOC entry 2100 (class 2606 OID 1724598)
-- Name: secao secao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY secao
    ADD CONSTRAINT secao_pkey PRIMARY KEY (codsecao);


--
-- TOC entry 2102 (class 2606 OID 1724609)
-- Name: telefone telefone_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY telefone
    ADD CONSTRAINT telefone_pkey PRIMARY KEY (id);


--
-- TOC entry 2104 (class 2606 OID 1724620)
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (usuarioid);


--
-- TOC entry 2106 (class 2606 OID 1724625)
-- Name: usuariochefesecao usuariochefesecao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuariochefesecao
    ADD CONSTRAINT usuariochefesecao_pkey PRIMARY KEY (secaoid, usuarioid);


--
-- TOC entry 2108 (class 2606 OID 1724636)
-- Name: usuarioexterno usuarioexterno_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuarioexterno
    ADD CONSTRAINT usuarioexterno_pkey PRIMARY KEY (id);


--
-- TOC entry 2110 (class 2606 OID 1724644)
-- Name: usuariopapelmodulo usuariopapelmodulo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuariopapelmodulo
    ADD CONSTRAINT usuariopapelmodulo_pkey PRIMARY KEY (id);


--
-- TOC entry 2120 (class 2606 OID 1724690)
-- Name: usuariopapelmodulo fk2ef92fae22ea9174; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuariopapelmodulo
    ADD CONSTRAINT fk2ef92fae22ea9174 FOREIGN KEY (papel_id) REFERENCES papel(id);


--
-- TOC entry 2119 (class 2606 OID 1724685)
-- Name: usuariopapelmodulo fk2ef92fae7f33d32e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuariopapelmodulo
    ADD CONSTRAINT fk2ef92fae7f33d32e FOREIGN KEY (modulo_id) REFERENCES modulo(id);


--
-- TOC entry 2112 (class 2606 OID 1724650)
-- Name: papel fk49521667f33d32e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY papel
    ADD CONSTRAINT fk49521667f33d32e FOREIGN KEY (modulo_id) REFERENCES modulo(id);


--
-- TOC entry 2117 (class 2606 OID 1724675)
-- Name: regra fk4b301a37f33d32e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY regra
    ADD CONSTRAINT fk4b301a37f33d32e FOREIGN KEY (modulo_id) REFERENCES modulo(id);


--
-- TOC entry 2111 (class 2606 OID 1724645)
-- Name: orgaos fk8d459cc11aa7141d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY orgaos
    ADD CONSTRAINT fk8d459cc11aa7141d FOREIGN KEY (codmunicipio) REFERENCES municipio(codmunicipio);


--
-- TOC entry 2113 (class 2606 OID 1724655)
-- Name: papelregra fk9338cffd22ea9174; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY papelregra
    ADD CONSTRAINT fk9338cffd22ea9174 FOREIGN KEY (papel_id) REFERENCES papel(id);


--
-- TOC entry 2114 (class 2606 OID 1724660)
-- Name: papelregra fk9338cffdb24a0fb; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY papelregra
    ADD CONSTRAINT fk9338cffdb24a0fb FOREIGN KEY (regra_id) REFERENCES regra(id);


--
-- TOC entry 2118 (class 2606 OID 1724680)
-- Name: usuarioexterno fkcb2655e1aac81659; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuarioexterno
    ADD CONSTRAINT fkcb2655e1aac81659 FOREIGN KEY (pessoa_id) REFERENCES pessoa(id);


--
-- TOC entry 2116 (class 2606 OID 1724670)
-- Name: pessoatelefone fkd31313d17fe627b9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pessoatelefone
    ADD CONSTRAINT fkd31313d17fe627b9 FOREIGN KEY (telefone_id) REFERENCES telefone(id);


--
-- TOC entry 2115 (class 2606 OID 1724665)
-- Name: pessoatelefone fkd31313d1aac81659; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pessoatelefone
    ADD CONSTRAINT fkd31313d1aac81659 FOREIGN KEY (pessoa_id) REFERENCES pessoa(id);


-- Completed on 2017-11-22 08:23:22

--
-- PostgreSQL database dump complete
--

