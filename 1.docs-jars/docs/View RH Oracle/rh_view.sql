--------------------------------------------------------
--  Arquivo criado - Quarta-feira-Agosto-30-2017   
--------------------------------------------------------
-- N�o � poss�vel renderizar a DLL VIEW para o objeto USISTEMA.REL_INFO_TCMGO com o DBMS_METADATA tentando um gerador interno.
CREATE VIEW USISTEMA.REL_INFO_TCMGO
AS SELECT  DISTINCT
          P.NOME,
          P.NMR_CPF,
          P.DTA_NASCIMENTO,
          P.SEXO,
          P.END_EMAIL,
          P.END_TELEFONE,
          P.NMR_CELULAR,
          FUN.DTA_ADM,
          ENDERECO.NME_NOME_LOGRADOURO,
          ENDERECO.NM_CEP_LOGRADOURO,
          ENDERECO.NME_MUNIC,
          ENDERECO.UF,
          ENDERECO.NME_BAIRRO,
          ENDERECO.NR_QUADRA,
          ENDERECO.NR_LOTE,
          ENDERECO.NR_ENDERECO,
          USISTEMA.NME_CAMPO_LOTACAO_DOSSIE(FUN.CDG_ORGAO, FUN.CDG_ORDEM, FUN.CDG_FUNCIONAL, FUN.REF_ANOMES,'U.NME_UNIDADE') AS UNIDADE,
          USISTEMA.SF_NME_CARGO_PRINCIPAL(FUN.CDG_ORGAO,FUN.CDG_ORDEM, FUN.CDG_FUNCIONAL, FUN.REF_ANOMES) AS CARGO,
          USISTEMA.NME_CAMPO_INSTRUCAO(FUN.CDG_ORDEM,'1,2,3,4,6,7,8',1,0,'GI.DESCRICAO') AS GRAU_INSTRUCAO,
          USISTEMA.NME_CAMPO_INSTRUCAO(FUN.CDG_ORDEM,'1,2,3,4,6,7,8',1,0,'TC.NME_CURSO') AS CURSO,
          FUN.CDG_ORDEM AS MATRICULA,

          (SELECT NME_ORGAO
             FROM USISTEMA.TBORGAO O
            WHERE O.CDG_ORGAO = FUN.CDG_ORGAO) AS ORGAO,

          DECODE(STATUS,'1','Exclus�o',2,'Inclus�o',3,'Desativado',4,'Normal','Desvinculado') as STATUS,


          DECODE(STATUS,5,(SELECT DECODE(DSE.CDG_SITESP,
                                     16, DSE.DTA_INI_SITESP,
                                     17, DSE.DTA_INI_SITESP,
                                     29, DSE.DTA_INI_SITESP,
                                     99, DSE.DTA_INI_SITESP,
                                     100, DSE.DTA_FIM_SITESP,
                                     101, DSE.DTA_FIM_SITESP,
                                     102, DSE.DTA_FIM_SITESP,
                                     103, DSE.DTA_FIM_SITESP,
                                     104, DSE.DTA_FIM_SITESP,
                                     150, DSE.DTA_INI_SITESP,
                                     151, DSE.DTA_INI_SITESP,
                                     152, DSE.DTA_FIM_SITESP,
                                     153, DSE.DTA_FIM_SITESP,
                                     '')

             FROM USISTEMA.TBDSITESP DSE
            WHERE (DSE.CDG_ORGAO     = FUN.CDG_ORGAO)
              AND (DSE.CDG_ORDEM     = FUN.CDG_ORDEM)
              AND (DSE.CDG_FUNCIONAL = FUN.CDG_FUNCIONAL)
              AND (DSE.DTA_INI_SITESP = (SELECT MAX(DSE2.DTA_INI_SITESP)
                                           FROM USISTEMA.TBDSITESP DSE2
                                          WHERE DSE2.CDG_ORGAO = DSE.CDG_ORGAO
                                            AND DSE2.CDG_ORDEM = DSE.CDG_ORDEM
                                            AND DSE2.CDG_FUNCIONAL = DSE.CDG_FUNCIONAL))),'') AS DATA_SAIDA



    FROM USISTEMA.TBCADFUNCIONAL FUN
   INNER JOIN USISTEMA.TBCADPESSOAL P ON P.CDG_ORDEM = FUN.CDG_ORDEM
    LEFT JOIN (SELECT PE.CDG_PESSOA,  TPLOG.NME_TIPO_LOGRADOURO,
                      NMLOG.NME_NOME_LOGRADOURO,
                      EN.NR_QUADRA,
                      EN.NR_ENDERECO,
                      EN.NR_LOTE,
                      EN.DESC_COMPLEMENTO_ENDERECO,
                      BA.NME_BAIRRO,
                      LOGR.NM_CEP_LOGRADOURO,
                      MU.CDG_MUNIC,
                      MU.NME_MUNIC,
                      MU.UF
                 FROM USISTEMA.TB_PESSOA_ENDERECO PE,
                      USISTEMA.TB_ENDERECO EN,
                      USISTEMA.TB_LOGRADOURO LOGR,
                      USISTEMA.TBNOME_LOGRADOURO NMLOG ,
                      USISTEMA.TBTIPO_LOGRADOURO TPLOG,
                      USISTEMA.TBTIPO_ENDERECO TPEND,
                      USISTEMA.TBBAIRRO BA,
                      USISTEMA.TBMUNIC MU
                WHERE TPEND.CDG_TIPO_ENDERECO = PE.TP_ENDERECO_PESSOA
                  AND EN.CDG_ENDERECO         = PE.CDG_ENDERECO
                  AND LOGR.CDG_LOGRADOURO     = EN.CDG_LOGRADOURO
                  AND MU.CDG_MUNIC            = LOGR.CDG_MUNIC
                  AND TPLOG.CDG_TIPO_LOGRADOURO = LOGR.CDG_TIPO_LOGRADOURO
                  AND BA.CDG_BAIRRO           = LOGR.CDG_BAIRRO
                  AND NMLOG.CDG_NOME_LOGRADOURO = LOGR.CDG_NOME_LOGRADOURO
                  AND PE.TP_ENDERECO_PESSOA = 1
             ) ENDERECO ON ENDERECO.CDG_PESSOA = FUN.CDG_ORDEM

   WHERE FUN.CDG_ORGAO IN (103,960)
    AND FUN.REF_ANOMES = (SELECT PA.REF_ANOMES
                            FROM USISTEMA.TBPARAMETRO PA
                           WHERE PA.CDG_ORGAO = FUN.CDG_ORGAO)
    AND FUN.ID_AUX = 0
    AND P.CDG_ORDEM = FUN.CDG_ORDEM


    AND FUN.CDG_SITFUNC NOT IN (4,11,99)
    AND ((FUN.CDG_ORGAO, FUN.CDG_ORDEM,FUN.CDG_FUNCIONAL) NOT IN ((SELECT SE2.CDG_ORGAO, SE2.CDG_ORDEM,SE2.CDG_FUNCIONAL
  								FROM USISTEMA.TBDSITESP SE2
  							   WHERE (SE2.CDG_ORGAO = FUN.CDG_ORGAO)
  								 AND (SE2.CDG_SITESP = 154)
  								 AND (TO_NUMBER(TO_CHAR(SE2.DTA_INI_SITESP,'YYYYMM')) <= FUN.REF_ANOMES)
  								 AND ((SE2.DTA_FIM_SITESP IS NULL) OR (TO_NUMBER(TO_CHAR(SE2.DTA_FIM_SITESP,'YYYYMM')) >= FUN.REF_ANOMES)))
  							   MINUS
  							 (SELECT SE2.CDG_ORGAO, SE2.CDG_ORDEM,SE2.CDG_FUNCIONAL
  								FROM USISTEMA.TBDSITESP SE2,
  									 USISTEMA.TBSITESP S
  							   WHERE (SE2.CDG_ORGAO = FUN.CDG_ORGAO)
  								 AND (S.CDG_SITESP  = SE2.CDG_SITESP)
  								 AND (NVL(S.TIP_SITESP,0) = 1)
  								 AND (TO_NUMBER(TO_CHAR(SE2.DTA_INI_SITESP,'YYYYMM')) <= FUN.REF_ANOMES)
  								 AND ((SE2.DTA_FIM_SITESP IS NULL) OR (TO_NUMBER(TO_CHAR(SE2.DTA_FIM_SITESP,'YYYYMM')) >= FUN.REF_ANOMES)))))
  ORDER BY P.NOME
