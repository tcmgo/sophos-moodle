USE [master]
GO
/****** Object:  Database [SOPHOS]    Script Date: 20/11/2017 14:46:55 ******/
CREATE DATABASE [SOPHOS]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SIGED_Data', FILENAME = N'E:\Dados\SIGED_DATA.MDF' , SIZE = 46336KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SIGED_Log', FILENAME = N'E:\Dados\SIGED_LOG.LDF' , SIZE = 16576KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [SOPHOS] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SOPHOS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SOPHOS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SOPHOS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SOPHOS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SOPHOS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SOPHOS] SET ARITHABORT OFF 
GO
ALTER DATABASE [SOPHOS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SOPHOS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SOPHOS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SOPHOS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SOPHOS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SOPHOS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SOPHOS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SOPHOS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SOPHOS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SOPHOS] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SOPHOS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SOPHOS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SOPHOS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SOPHOS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SOPHOS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SOPHOS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SOPHOS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SOPHOS] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SOPHOS] SET  MULTI_USER 
GO
ALTER DATABASE [SOPHOS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SOPHOS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SOPHOS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SOPHOS] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [SOPHOS] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'SOPHOS', N'ON'
GO
ALTER DATABASE [SOPHOS] SET QUERY_STORE = OFF
GO
USE [SOPHOS]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [SOPHOS]
GO
/****** Object:  User [usr_sophos]    Script Date: 20/11/2017 14:46:56 ******/
CREATE USER [usr_sophos] FOR LOGIN [usr_sophos] WITH DEFAULT_SCHEMA=[dbo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cidade](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[descricao] [varchar](255) NOT NULL,
	[estado_id] [int] NOT NULL,
 CONSTRAINT [PK_cidade] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dominio]    Script Date: 20/11/2017 14:46:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dominio](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[ativo] [tinyint] NOT NULL,
	[codigo] [int] NOT NULL,
	[descricao] [varchar](255) NOT NULL,
	[nome] [varchar](100) NOT NULL,
 CONSTRAINT [PK_dominio] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[endereco]    Script Date: 20/11/2017 14:46:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[endereco](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[bairro] [varchar](100) NULL,
	[complemento] [varchar](255) NULL,
	[logradouro] [varchar](255) NULL,
	[municipio_id] [smallint] NULL,
	[numero] [varchar](10) NULL,
	[cidade_id] [int] NULL,
	[cep] [varchar](9) NULL,
 CONSTRAINT [PK_endereco] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[estado]    Script Date: 20/11/2017 14:46:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[estado](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[descricao] [varchar](150) NULL,
	[uf] [varchar](2) NULL,
	[pais_id] [int] NULL,
 CONSTRAINT [PK_estado] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[participante]    Script Date: 20/11/2017 14:46:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[participante](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[cargo] [varchar](255) NULL,
	[codMunicipio] [smallint] NULL,
	[codOrgao] [smallint] NULL,
	[lotacao] [varchar](255) NULL,
	[matricula] [varchar](6) NULL,
	[observacao] [varchar](2000) NULL,
	[profissao] [varchar](255) NULL,
	[endereco_id] [int] NULL,
	[escolaridade_id] [int] NULL,
	[formacao_academica_id] [int] NULL,
	[pessoa_id] [int] NOT NULL,
	[publico_alvo_id] [int] NOT NULL,
	[data_admissao] [datetime] NULL,
	[instituicao] [varchar](255) NULL,
	[pne] [tinyint] NULL,
	[tipo_necessidade_especial_id] [int] NULL,
	[outra_necessidade_especial] [varchar](255) NULL,
	[mandato_id] [int] NULL,
 CONSTRAINT [PK_participante] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pessoa]    Script Date: 20/11/2017 14:46:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pessoa](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[cpf] [varchar](14) NULL,
	[data_nascimento] [datetime] NULL,
	[email] [varchar](255) NULL,
	[nome] [varchar](255) NOT NULL,
	[sexo_id] [int] NULL,
	[celular] [varchar](15) NULL,
	[telefone] [varchar](15) NULL,
 CONSTRAINT [PK_pessoa] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[usuario]    Script Date: 20/11/2017 14:46:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuario](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[matricula] [varchar](6) NULL,
	[nome_usuario] [varchar](255) NULL,
	[codSecao] [varchar](3) NULL,
	[senha] [varchar](255) NULL,
	[token_recup_senha] [varchar](255) NULL,
	[usuario_id] [int] NULL,
	[participante_id] [int] NULL,
	[pessoa_id] [int] NOT NULL,
	[tipo_usuario_id] [int] NOT NULL,
 CONSTRAINT [PK_usuario] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vw_Usuario]    Script Date: 20/11/2017 14:46:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* VIEW PARA CARGA DE USUÁRIOS PELO MOODLE
 CAMPOS---------------------------------------------------
 tipo_usuario
 tipo_participante
 nome completo
 usuario
 senha
 nome
 sobrenome
 email
 cidade
 pais
 matricula
 instituicao
 lotacao
 telefone
 celular
 endereco
 cpf
-----------------------------------------------------------*/
CREATE VIEW [dbo].[vw_Usuario]
AS
SELECT        UPPER(dom.descricao) AS tipo_usuario, UPPER(dom2.descricao) AS tipo_participante, pes.nome AS nome_completo, LOWER(usu.nome_usuario) AS usuario, CASE dom2.descricao WHEN
                             (SELECT        descricao
                               FROM            dbo.dominio
                               WHERE        nome = 'TipoPublicoAlvo' AND codigo = 3) THEN isnull(LOWER(usuint.Senha), LOWER(usu.senha)) COLLATE Latin1_General_CI_AS WHEN
                             (SELECT        descricao
                               FROM            dbo.dominio
                               WHERE        nome = 'TipoPublicoAlvo' AND codigo = 1) THEN LOWER(usu.senha) COLLATE Latin1_General_CI_AS WHEN
                             (SELECT        descricao
                               FROM            dbo.dominio
                               WHERE        nome = 'TipoPublicoAlvo' AND codigo = 2) THEN LOWER(usu.senha) COLLATE Latin1_General_CI_AS END AS senha, SUBSTRING(pes.nome, 0, CHARINDEX(' ', pes.nome)) AS nome, 
                         SUBSTRING(pes.nome, CHARINDEX(' ', pes.nome), LEN(pes.nome)) AS sobrenome, pes.email, CASE dom2.descricao WHEN
                             (SELECT        descricao
                               FROM            dbo.dominio
                               WHERE        nome = 'TipoPublicoAlvo' AND codigo = 3) THEN UPPER(mun.nomeMunicipio) COLLATE Latin1_General_CI_AS WHEN
                             (SELECT        descricao
                               FROM            dbo.dominio
                               WHERE        nome = 'TipoPublicoAlvo' AND codigo = 1) THEN UPPER(mun.nomeMunicipio) COLLATE Latin1_General_CI_AS WHEN
                             (SELECT        descricao
                               FROM            dbo.dominio
                               WHERE        nome = 'TipoPublicoAlvo' AND codigo = 2) THEN UPPER(cid.descricao) COLLATE Latin1_General_CI_AS END AS cidade, 'BRASIL' AS pais, usu.matricula, CASE dom2.descricao WHEN
                             (SELECT        descricao
                               FROM            dbo.dominio
                               WHERE        nome = 'TipoPublicoAlvo' AND codigo = 3) THEN 'TRIBUNAL DE CONTAS DOS MUNICÃPIOS DO ESTADO DE GOIÃS (TCM GO)' WHEN
                             (SELECT        descricao
                               FROM            dbo.dominio
                               WHERE        nome = 'TipoPublicoAlvo' AND codigo = 1) THEN org.descOrgao COLLATE Latin1_General_CI_AS WHEN
                             (SELECT        descricao
                               FROM            dbo.dominio
                               WHERE        nome = 'TipoPublicoAlvo' AND codigo = 2) THEN part.instituicao COLLATE Latin1_General_CI_AS END AS instituicao, UPPER(part.lotacao) AS lotacao, pes.telefone, pes.celular, 
                         CASE dom2.descricao WHEN
                             (SELECT        descricao
                               FROM            dbo.dominio
                               WHERE        nome = 'TipoPublicoAlvo' AND codigo = 3) 
                         THEN ende.logradouro + ' ' + ende.complemento + ' - ' + ende.bairro + ' - ' + mun.nomeMunicipio COLLATE Latin1_General_CI_AS + ' - ' + mun.ufCidadeLogra WHEN
                             (SELECT        descricao
                               FROM            dbo.dominio
                               WHERE        nome = 'TipoPublicoAlvo' AND codigo = 1) 
                         THEN ende.logradouro + ' ' + ende.complemento + ' - ' + ende.bairro + ' - ' + mun.nomeMunicipio COLLATE Latin1_General_CI_AS + ' - ' + mun.ufCidadeLogra WHEN
                             (SELECT        descricao
                               FROM            dbo.dominio
                               WHERE        nome = 'TipoPublicoAlvo' AND codigo = 2) 
                         THEN ende.logradouro + ' ' + ende.complemento + ' - ' + ende.bairro + ' - ' + cid.descricao + ' - ' + est.descricao COLLATE Latin1_General_CI_AS END AS endereco, REPLACE(REPLACE(pes.cpf, '.', ''), '-', '') 
                         AS cpf
FROM            dbo.usuario AS usu INNER JOIN
                         dbo.participante AS part ON usu.participante_id = part.id LEFT OUTER JOIN
                         USUARIO.dbo.Usuario AS usuint ON usu.usuario_id = usuint.UsuarioID INNER JOIN
                         dbo.dominio AS dom ON usu.tipo_usuario_id = dom.id INNER JOIN
                         dbo.dominio AS dom2 ON part.publico_alvo_id = dom2.id LEFT OUTER JOIN
                         dbo.pessoa AS pes ON usu.pessoa_id = pes.id LEFT OUTER JOIN
                         dbo.endereco AS ende ON part.endereco_id = ende.id LEFT OUTER JOIN
                         dbo.cidade AS cid ON ende.cidade_id = cid.id LEFT OUTER JOIN
                         ORCAFI.dbo.Municipio AS mun ON ende.municipio_id = mun.codMunicipio LEFT OUTER JOIN
                         ORCAFI.dbo.Orgaos AS org ON part.codOrgao = org.codOrgao AND org.codMunicipio = ende.municipio_id LEFT OUTER JOIN
                         dbo.estado AS est ON cid.estado_id = est.id
GO
/****** Object:  Table [dbo].[anexo]    Script Date: 20/11/2017 14:46:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[anexo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[arquivo_ged] [varchar](255) NULL,
	[descricao] [varchar](255) NULL,
	[tipo_arquivo] [varchar](255) NULL,
 CONSTRAINT [PK_anexo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[certificado]    Script Date: 20/11/2017 14:46:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[certificado](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[codigo_verificacao] [varchar](17) NOT NULL,
	[data_emissao] [datetime] NOT NULL,
	[arquivo_id] [int] NOT NULL,
	[evento_id] [int] NOT NULL,
	[participante_id] [int] NOT NULL,
 CONSTRAINT [PK_certificado] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[evento]    Script Date: 20/11/2017 14:46:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[evento](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[carga_horaria] [float] NOT NULL,
	[conteudo] [varchar](2000) NOT NULL,
	[data_fim_pre_inscricao] [datetime] NULL,
	[data_fim_previsto] [datetime] NOT NULL,
	[data_fim_realizacao] [datetime] NULL,
	[data_inicio_pre_inscricao] [datetime] NULL,
	[data_inicio_previsto] [datetime] NOT NULL,
	[data_inicio_realizacao] [datetime] NULL,
	[modulo_unico] [tinyint] NULL,
	[objetivo_especifico] [varchar](2000) NULL,
	[objetivo_geral] [varchar](2000) NULL,
	[observacoes] [varchar](2000) NULL,
	[observacoes_publicas] [varchar](2000) NULL,
	[permite_certificado] [tinyint] NULL,
	[permite_pre_inscricao] [tinyint] NULL,
	[publicado] [tinyint] NULL,
	[resultado_esperado] [varchar](2000) NULL,
	[titulo] [varchar](255) NOT NULL,
	[vagas] [int] NOT NULL,
	[area_tribunal_id] [int] NULL,
	[eixo_tematico_id] [int] NOT NULL,
	[imagem_id] [int] NULL,
	[localizacao_id] [int] NOT NULL,
	[modalidade_id] [int] NOT NULL,
	[provedor_id] [int] NOT NULL,
	[responsavel_evento] [int] NOT NULL,
	[tipo_evento_id] [int] NOT NULL,
	[mostrar_na_home] [tinyint] NULL,
	[avaliar_por_media] [tinyint] NULL,
	[nota_aprovacao] [float] NULL,
	[frequencia_aprovacao] [float] NULL,
	[importado] [bit] NULL,
	[imagem_certificado_frente_id] [int] NULL,
	[imagem_certificado_verso_id] [int] NULL,
 CONSTRAINT [PK_evento] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[frequencia]    Script Date: 20/11/2017 14:46:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[frequencia](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[encontro] [int] NOT NULL,
	[presenca] [tinyint] NULL,
	[modulo_id] [int] NOT NULL,
	[participante_id] [int] NOT NULL,
 CONSTRAINT [PK_frequencia] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[inscricao]    Script Date: 20/11/2017 14:46:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[inscricao](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[justificativa_participante] [varchar](2000) NULL,
	[pre_inscricao] [tinyint] NULL,
	[evento_id] [int] NOT NULL,
	[indicacao_id] [int] NULL,
	[participante_id] [int] NOT NULL,
	[autorizada] [tinyint] NULL,
	[data_avaliacao_preinscricao] [datetime] NULL,
	[avaliador_id] [int] NULL,
 CONSTRAINT [PK_inscricao] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[localizacao_evento]    Script Date: 20/11/2017 14:46:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[localizacao_evento](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[descricao] [varchar](255) NOT NULL,
	[endereco_id] [int] NULL,
 CONSTRAINT [PK_localizacao_evento] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[modulo]    Script Date: 20/11/2017 14:46:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[modulo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[carga_horaria] [float] NOT NULL,
	[data_fim] [datetime] NOT NULL,
	[data_inicio] [datetime] NOT NULL,
	[frequencia_aprovacao] [float] NULL,
	[hora_fim_turno1] [varchar](5) NULL,
	[hora_fim_turno2] [varchar](5) NULL,
	[hora_inicio_turno1] [varchar](5) NULL,
	[hora_inicio_turno2] [varchar](5) NULL,
	[nota_aprovacao] [float] NOT NULL,
	[observacao] [varchar](2000) NULL,
	[quantidade_encontros] [int] NOT NULL,
	[titulo] [varchar](255) NOT NULL,
	[evento_id] [int] NOT NULL,
 CONSTRAINT [PK_modulo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[nota]    Script Date: 20/11/2017 14:46:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nota](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[valor] [float] NOT NULL,
	[modulo_id] [int] NOT NULL,
	[participante_id] [int] NOT NULL,
 CONSTRAINT [PK_nota] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[provedor_evento]    Script Date: 20/11/2017 14:46:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[provedor_evento](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[agencia] [varchar](10) NULL,
	[celular] [varchar](15) NULL,
	[cnpj] [varchar](18) NULL,
	[conta_corrente] [varchar](20) NULL,
	[contato] [varchar](255) NULL,
	[descricao] [varchar](255) NOT NULL,
	[email] [varchar](255) NULL,
	[telefone] [varchar](15) NULL,
	[banco_id] [int] NULL,
	[endereco_id] [int] NULL,
 CONSTRAINT [PK_provedor_evento] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vw_EventoSophos]    Script Date: 20/11/2017 14:46:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_EventoSophos] 
AS
select distinct
		event.titulo as evento,
		event.data_inicio_realizacao as data_inicio,
		event.data_fim_realizacao as data_fim,
		event.carga_horaria as carga_horaria,
		md.titulo,
		loc.descricao as Localização,
		participante.id as id_participante,
		pessoa.nome as nome,
		pessoa.cpf as cpf,
		tipo.descricao as tipo,
		participante.cargo as cargo,
		participante.lotacao as lotacao,
		inscricao.autorizada as autorizada,
		(select top 1 sum(nota.valor)/count(mo.id) from dbo.nota as nota
			right outer join dbo.modulo as mo on nota.modulo_id = mo.id
			inner join dbo.evento as ev on mo.evento_id = ev.id
			where ev.id = inscricao.evento_id and nota.participante_id = participante.id
			group by mo.id ) as mediaNota,
		(select count(*) from dbo.frequencia as freq
			inner join dbo.modulo as mo on freq.modulo_id = mo.id
			inner join dbo.evento as ev on mo.evento_id = ev.id
			where ev.id = inscricao.evento_id and freq.participante_id = participante.id and freq.presenca = 1
			group by ev.id ) as qtdPresencasEvento,
		(select sum(mo.quantidade_encontros) from dbo.modulo as mo
			inner join dbo.evento as ev on mo.evento_id = ev.id
			where ev.id = inscricao.evento_id group by ev.id ) as qtdEncontrosEvento,
		(select top 1 anexo.arquivo_ged from dbo.anexo as anexo
			inner join dbo.certificado as cert on cert.arquivo_id = anexo.id
			inner join dbo.evento as ev on ev.id = cert.evento_id
			inner join dbo.participante as part on part.id = cert.participante_id
			inner join dbo.inscricao as insc on insc.participante_id = part.id and insc.evento_id = ev.id
		where inscricao.participante_id = cert.participante_id and inscricao.evento_id = cert.evento_id order by cert.data_emissao desc) as arquivo_idGed,
		md.nota_aprovacao,
		inscricao.id as id_inscricao,
		nota_modulo.valor,
		md.frequencia_aprovacao,
		cidloc.descricao as cidade_localizacao,
		estloc.descricao as estado_localizacao,
		modalidade.descricao as modalidade,
		provedor.descricao as provedor,
		case event.permite_certificado when 1 then 'SIM' else 'NÃO' end as certificado_liberado
	from dbo.participante as participante
		inner join dbo.pessoa as pessoa on pessoa.id = participante.pessoa_id
		inner join dbo.dominio as tipo on tipo.id = participante.publico_alvo_id
		left join dbo.inscricao as inscricao on inscricao.participante_id = participante.id
		inner join dbo.evento as event on inscricao.evento_id = event.id
		inner join dbo.modulo as md on event.id = md.evento_id
		inner join dbo.localizacao_evento as loc on event.localizacao_id = loc.id
		inner join dbo.endereco endloc on loc.endereco_id = endloc.id
		inner join dbo.cidade as cidloc on endloc.cidade_id = cidloc.id
		inner join dbo.estado as estloc on cidloc.estado_id = estloc.id
		inner join dbo.dominio as modalidade on event.modalidade_id = modalidade.id
		inner join dbo.provedor_evento as provedor on event.provedor_id = provedor.id
		left join dbo.nota as nota_modulo on md.id = nota_modulo.modulo_id and inscricao.participante_id = nota_modulo.participante_id
		and inscricao.autorizada = 1
GO
/****** Object:  Table [dbo].[instrutor]    Script Date: 20/11/2017 14:46:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[instrutor](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[instituicao] [varchar](255) NULL,
	[observacao] [varchar](2000) NULL,
	[perfil] [varchar](2000) NULL,
	[CodSecao] [varchar](3) NULL,
	[arquivo_assinatura_id] [int] NULL,
	[arquivo_curriculo_id] [int] NULL,
	[endereco_id] [int] NULL,
	[formacao_academica_id] [int] NULL,
	[nivel_escolaridade_id] [int] NULL,
	[pessoa_id] [int] NULL,
	[arquivo_projeto_id] [int] NULL,
	[situacao_instrutor_id] [int] NULL,
	[tipo_Instrutor_id] [int] NOT NULL,
 CONSTRAINT [PK_instrutor] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[modulo_instrutor]    Script Date: 20/11/2017 14:46:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[modulo_instrutor](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[instrutor_id] [int] NOT NULL,
	[modulo_id] [int] NOT NULL,
	[arquivo_id] [int] NULL,
 CONSTRAINT [PK_modulo_instrutor] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vw_InstrutorEventoSophos]    Script Date: 20/11/2017 14:46:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_InstrutorEventoSophos] 
AS
select distinct
		event.titulo as evento,
		modalidade.descricao as modalidade,
		event.data_inicio_realizacao as data_inicio,
		event.data_fim_realizacao as data_fim,
		event.carga_horaria as carga_horaria_evento,
		(select sum(m.carga_horaria) 
		 from dbo.modulo m
		    inner join dbo.modulo_instrutor as mi on mi.modulo_id = m.id
		    inner join dbo.instrutor as i on i.id = mi.instrutor_id
		where i.id = instrutor.id and m.id in (select id from dbo.modulo where evento_id = event.id)
		) as carga_horaria_instrutor,
		loc.descricao as localizacao,
		cidloc.descricao as cidade_localizacao,
		estloc.descricao as estado_localizacao,
		pessoa.nome as nome,
		pessoa.cpf as cpf,
		tipo.descricao as tipo,
		formacao.descricao as formacao_academica,
		instrutor.instituicao as instituicao,
		arquivo.arquivo_ged as id_arquivo_ged
	from dbo.instrutor as instrutor
		inner join dbo.pessoa as pessoa on pessoa.id = instrutor.pessoa_id
		inner join dbo.dominio as tipo on tipo.id = instrutor.tipo_Instrutor_id
		inner join dbo.dominio as formacao on formacao.id = instrutor.formacao_academica_id
		inner join dbo.modulo_instrutor as mod_instrutor on mod_instrutor.instrutor_id = instrutor.id
		left join dbo.anexo as arquivo on mod_instrutor.arquivo_id = arquivo.id
		inner join dbo.modulo as modulo on mod_instrutor.modulo_id = modulo.id
		inner join dbo.evento as event on modulo.evento_id = event.id
		inner join dbo.dominio as modalidade on event.modalidade_id = modalidade.id
		inner join dbo.localizacao_evento as loc on event.localizacao_id = loc.id
		inner join dbo.endereco endloc on loc.endereco_id = endloc.id
		inner join dbo.cidade as cidloc on endloc.cidade_id = cidloc.id
		inner join dbo.estado as estloc on cidloc.estado_id = estloc.id
GO
/****** Object:  Table [dbo].[alerta]    Script Date: 20/11/2017 14:46:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[alerta](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[ativo] [tinyint] NOT NULL,
	[data_visualizacao] [datetime] NULL,
	[mensagem] [varchar](2000) NULL,
	[pacote_alertas_id] [int] NULL,
	[participante_id] [int] NOT NULL,
	[tipo_id] [int] NULL,
 CONSTRAINT [PK_alerta] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[arquivo_adm]    Script Date: 20/11/2017 14:46:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[arquivo_adm](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[arquivo_id] [int] NOT NULL,
	[evento_id] [int] NOT NULL,
	[tipo_arquivoadm_id] [int] NOT NULL,
 CONSTRAINT [PK_arquivo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[avaliacao]    Script Date: 20/11/2017 14:46:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[avaliacao](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[observacao] [varchar](2000) NULL,
	[modulo_id] [int] NOT NULL,
	[participante_id] [int] NOT NULL,
 CONSTRAINT [PK_avaliacao] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[avaliacao_eficacia]    Script Date: 20/11/2017 14:46:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[avaliacao_eficacia](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[melhoria_desempenho] [tinyint] NOT NULL,
	[desempenho_servico_id] [int] NOT NULL,
	[observacao] [varchar](2000) NULL,
	[evento_id] [int] NOT NULL,
	[indicacao_id] [int] NULL,
	[participante_id] [int] NOT NULL,
 CONSTRAINT [PK_avaliacao_eficacia] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[banco_febraban]    Script Date: 20/11/2017 14:46:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[banco_febraban](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[codigo] [varchar](5) NULL,
	[descricao] [varchar](255) NULL,
 CONSTRAINT [PK_banco_febraban] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[evento_publico_alvo]    Script Date: 20/11/2017 14:46:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[evento_publico_alvo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[evento_id] [int] NOT NULL,
	[publico_alvo_id] [int] NOT NULL,
 CONSTRAINT [PK_evento_publico_alvo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gasto]    Script Date: 20/11/2017 14:46:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gasto](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[data_empenho] [datetime] NULL,
	[numero_empenho] [int] NULL,
	[ano_processo] [smallint] NULL,
	[seq_processo] [int] NULL,
	[observacao] [varchar](2000) NULL,
	[valor] [float] NOT NULL,
	[evento_id] [int] NOT NULL,
	[fonte_gasto_id] [int] NOT NULL,
	[tipo_id] [int] NOT NULL,
 CONSTRAINT [PK_gasto] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[indicacao]    Script Date: 20/11/2017 14:46:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[indicacao](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[aprovado] [tinyint] NULL,
	[data_avaliacao] [datetime] NULL,
	[justificativa_chefe] [varchar](2000) NULL,
	[avaliador_id] [int] NULL,
	[chefe_id] [int] NULL,
	[evento_id] [int] NOT NULL,
	[participante_id] [int] NOT NULL,
 CONSTRAINT [PK_indicacao] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[log]    Script Date: 20/11/2017 14:46:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[entidade_id] [int] NULL,
	[operacao] [varchar](255) NULL,
	[tabela] [varchar](255) NULL,
	[usuario_id] [int] NULL,
 CONSTRAINT [PK_log] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[material]    Script Date: 20/11/2017 14:46:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[material](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[arquivo_id] [int] NOT NULL,
	[evento_id] [int] NOT NULL,
	[modulo_id] [int] NULL,
	[tipo_material_id] [int] NOT NULL,
 CONSTRAINT [PK_material] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[opcao_questionario]    Script Date: 20/11/2017 14:46:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[opcao_questionario](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[ativo] [tinyint] NOT NULL,
	[descricao] [varchar](255) NULL,
	[ordem] [int] NULL,
	[questionario_id] [int] NOT NULL,
 CONSTRAINT [PK_opcao_questionario] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pacote_alertas]    Script Date: 20/11/2017 14:46:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pacote_alertas](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[mensagem] [varchar](2000) NOT NULL,
	[nome] [varchar](500) NOT NULL,
	[evento_id] [int] NULL,
	[tipo_id] [int] NOT NULL,
 CONSTRAINT [PK_pacote_alertas] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pais]    Script Date: 20/11/2017 14:46:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pais](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[descricao] [varchar](255) NOT NULL,
 CONSTRAINT [PK_pais] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[perfil_usuario]    Script Date: 20/11/2017 14:46:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[perfil_usuario](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[perfil] [varchar](255) NULL,
	[usuario_id] [int] NOT NULL,
 CONSTRAINT [PK_perfil_usuario] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[questionario]    Script Date: 20/11/2017 14:46:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[questionario](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[ativo] [tinyint] NOT NULL,
	[para_instrutor] [tinyint] NULL,
	[pergunta] [varchar](2000) NULL,
 CONSTRAINT [PK_questionario] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[questionario_avaliacao]    Script Date: 20/11/2017 14:46:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[questionario_avaliacao](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_cadastro] [datetime] NOT NULL,
	[avaliacao_id] [int] NOT NULL,
	[instrutor_id] [int] NULL,
	[opcao_id] [int] NOT NULL,
	[questionario_id] [int] NOT NULL,
 CONSTRAINT [PK_questionario_avaliacao] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[alerta]  WITH CHECK ADD  CONSTRAINT [FK_alerta_dominio] FOREIGN KEY([tipo_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[alerta] CHECK CONSTRAINT [FK_alerta_dominio]
GO
ALTER TABLE [dbo].[alerta]  WITH CHECK ADD  CONSTRAINT [FK_alerta_pacote_alertas] FOREIGN KEY([pacote_alertas_id])
REFERENCES [dbo].[pacote_alertas] ([id])
GO
ALTER TABLE [dbo].[alerta] CHECK CONSTRAINT [FK_alerta_pacote_alertas]
GO
ALTER TABLE [dbo].[alerta]  WITH CHECK ADD  CONSTRAINT [FK_alerta_participante] FOREIGN KEY([participante_id])
REFERENCES [dbo].[participante] ([id])
GO
ALTER TABLE [dbo].[alerta] CHECK CONSTRAINT [FK_alerta_participante]
GO
ALTER TABLE [dbo].[arquivo_adm]  WITH CHECK ADD  CONSTRAINT [FK_anexo_arquivo_adm] FOREIGN KEY([arquivo_id])
REFERENCES [dbo].[anexo] ([id])
GO
ALTER TABLE [dbo].[arquivo_adm] CHECK CONSTRAINT [FK_anexo_arquivo_adm]
GO
ALTER TABLE [dbo].[arquivo_adm]  WITH CHECK ADD  CONSTRAINT [FK_dominio_arquivo_adm] FOREIGN KEY([tipo_arquivoadm_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[arquivo_adm] CHECK CONSTRAINT [FK_dominio_arquivo_adm]
GO
ALTER TABLE [dbo].[arquivo_adm]  WITH CHECK ADD  CONSTRAINT [FK_evento_arquivo_adm] FOREIGN KEY([evento_id])
REFERENCES [dbo].[evento] ([id])
GO
ALTER TABLE [dbo].[arquivo_adm] CHECK CONSTRAINT [FK_evento_arquivo_adm]
GO
ALTER TABLE [dbo].[avaliacao]  WITH CHECK ADD  CONSTRAINT [FK_modulo_avaliacao] FOREIGN KEY([modulo_id])
REFERENCES [dbo].[modulo] ([id])
GO
ALTER TABLE [dbo].[avaliacao] CHECK CONSTRAINT [FK_modulo_avaliacao]
GO
ALTER TABLE [dbo].[avaliacao]  WITH CHECK ADD  CONSTRAINT [FK_participante_avaliacao] FOREIGN KEY([participante_id])
REFERENCES [dbo].[participante] ([id])
GO
ALTER TABLE [dbo].[avaliacao] CHECK CONSTRAINT [FK_participante_avaliacao]
GO
ALTER TABLE [dbo].[avaliacao_eficacia]  WITH CHECK ADD  CONSTRAINT [FK_dominio_avaliacao_eficacia] FOREIGN KEY([desempenho_servico_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[avaliacao_eficacia] CHECK CONSTRAINT [FK_dominio_avaliacao_eficacia]
GO
ALTER TABLE [dbo].[avaliacao_eficacia]  WITH CHECK ADD  CONSTRAINT [FK_evento_avaliacao_eficacia] FOREIGN KEY([evento_id])
REFERENCES [dbo].[evento] ([id])
GO
ALTER TABLE [dbo].[avaliacao_eficacia] CHECK CONSTRAINT [FK_evento_avaliacao_eficacia]
GO
ALTER TABLE [dbo].[avaliacao_eficacia]  WITH CHECK ADD  CONSTRAINT [FK_indicacao_avaliacao_eficacia] FOREIGN KEY([indicacao_id])
REFERENCES [dbo].[indicacao] ([id])
GO
ALTER TABLE [dbo].[avaliacao_eficacia] CHECK CONSTRAINT [FK_indicacao_avaliacao_eficacia]
GO
ALTER TABLE [dbo].[avaliacao_eficacia]  WITH CHECK ADD  CONSTRAINT [FK_participante_avaliacao_eficacia] FOREIGN KEY([participante_id])
REFERENCES [dbo].[participante] ([id])
GO
ALTER TABLE [dbo].[avaliacao_eficacia] CHECK CONSTRAINT [FK_participante_avaliacao_eficacia]
GO
ALTER TABLE [dbo].[certificado]  WITH CHECK ADD  CONSTRAINT [FK_anexo_certificado] FOREIGN KEY([arquivo_id])
REFERENCES [dbo].[anexo] ([id])
GO
ALTER TABLE [dbo].[certificado] CHECK CONSTRAINT [FK_anexo_certificado]
GO
ALTER TABLE [dbo].[certificado]  WITH CHECK ADD  CONSTRAINT [FK_evento_certificado] FOREIGN KEY([evento_id])
REFERENCES [dbo].[evento] ([id])
GO
ALTER TABLE [dbo].[certificado] CHECK CONSTRAINT [FK_evento_certificado]
GO
ALTER TABLE [dbo].[certificado]  WITH CHECK ADD  CONSTRAINT [FK_participante_certificado] FOREIGN KEY([participante_id])
REFERENCES [dbo].[participante] ([id])
GO
ALTER TABLE [dbo].[certificado] CHECK CONSTRAINT [FK_participante_certificado]
GO
ALTER TABLE [dbo].[cidade]  WITH CHECK ADD  CONSTRAINT [FK_estado_cidade] FOREIGN KEY([estado_id])
REFERENCES [dbo].[estado] ([id])
GO
ALTER TABLE [dbo].[cidade] CHECK CONSTRAINT [FK_estado_cidade]
GO
ALTER TABLE [dbo].[endereco]  WITH CHECK ADD  CONSTRAINT [FK_cidade_endereco] FOREIGN KEY([cidade_id])
REFERENCES [dbo].[cidade] ([id])
GO
ALTER TABLE [dbo].[endereco] CHECK CONSTRAINT [FK_cidade_endereco]
GO
ALTER TABLE [dbo].[estado]  WITH CHECK ADD  CONSTRAINT [FK_pais_estado] FOREIGN KEY([pais_id])
REFERENCES [dbo].[pais] ([id])
GO
ALTER TABLE [dbo].[estado] CHECK CONSTRAINT [FK_pais_estado]
GO
ALTER TABLE [dbo].[evento]  WITH CHECK ADD  CONSTRAINT [FK_anexo_evento] FOREIGN KEY([imagem_id])
REFERENCES [dbo].[anexo] ([id])
GO
ALTER TABLE [dbo].[evento] CHECK CONSTRAINT [FK_anexo_evento]
GO
ALTER TABLE [dbo].[evento]  WITH CHECK ADD  CONSTRAINT [FK_anexo_imagem_certficado_frente_evento] FOREIGN KEY([imagem_certificado_frente_id])
REFERENCES [dbo].[anexo] ([id])
GO
ALTER TABLE [dbo].[evento] CHECK CONSTRAINT [FK_anexo_imagem_certficado_frente_evento]
GO
ALTER TABLE [dbo].[evento]  WITH CHECK ADD  CONSTRAINT [FK_anexo_imagem_certficado_verso_evento] FOREIGN KEY([imagem_certificado_verso_id])
REFERENCES [dbo].[anexo] ([id])
GO
ALTER TABLE [dbo].[evento] CHECK CONSTRAINT [FK_anexo_imagem_certficado_verso_evento]
GO
ALTER TABLE [dbo].[evento]  WITH CHECK ADD  CONSTRAINT [FK_dominio_evento] FOREIGN KEY([modalidade_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[evento] CHECK CONSTRAINT [FK_dominio_evento]
GO
ALTER TABLE [dbo].[evento]  WITH CHECK ADD  CONSTRAINT [FK_dominio_evento1] FOREIGN KEY([eixo_tematico_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[evento] CHECK CONSTRAINT [FK_dominio_evento1]
GO
ALTER TABLE [dbo].[evento]  WITH CHECK ADD  CONSTRAINT [FK_dominio_evento2] FOREIGN KEY([area_tribunal_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[evento] CHECK CONSTRAINT [FK_dominio_evento2]
GO
ALTER TABLE [dbo].[evento]  WITH CHECK ADD  CONSTRAINT [FK_dominio_evento3] FOREIGN KEY([tipo_evento_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[evento] CHECK CONSTRAINT [FK_dominio_evento3]
GO
ALTER TABLE [dbo].[evento]  WITH CHECK ADD  CONSTRAINT [FK_localizacao_evento_evento] FOREIGN KEY([localizacao_id])
REFERENCES [dbo].[localizacao_evento] ([id])
GO
ALTER TABLE [dbo].[evento] CHECK CONSTRAINT [FK_localizacao_evento_evento]
GO
ALTER TABLE [dbo].[evento]  WITH CHECK ADD  CONSTRAINT [FK_participante_evento] FOREIGN KEY([responsavel_evento])
REFERENCES [dbo].[participante] ([id])
GO
ALTER TABLE [dbo].[evento] CHECK CONSTRAINT [FK_participante_evento]
GO
ALTER TABLE [dbo].[evento]  WITH CHECK ADD  CONSTRAINT [FK_provedor_evento_evento] FOREIGN KEY([provedor_id])
REFERENCES [dbo].[provedor_evento] ([id])
GO
ALTER TABLE [dbo].[evento] CHECK CONSTRAINT [FK_provedor_evento_evento]
GO
ALTER TABLE [dbo].[evento_publico_alvo]  WITH CHECK ADD  CONSTRAINT [FK_dominio_evento_publico_alvo] FOREIGN KEY([publico_alvo_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[evento_publico_alvo] CHECK CONSTRAINT [FK_dominio_evento_publico_alvo]
GO
ALTER TABLE [dbo].[evento_publico_alvo]  WITH CHECK ADD  CONSTRAINT [FK_evento_evento_publico_alvo] FOREIGN KEY([evento_id])
REFERENCES [dbo].[evento] ([id])
GO
ALTER TABLE [dbo].[evento_publico_alvo] CHECK CONSTRAINT [FK_evento_evento_publico_alvo]
GO
ALTER TABLE [dbo].[frequencia]  WITH CHECK ADD  CONSTRAINT [FK_modulo_frequencia] FOREIGN KEY([modulo_id])
REFERENCES [dbo].[modulo] ([id])
GO
ALTER TABLE [dbo].[frequencia] CHECK CONSTRAINT [FK_modulo_frequencia]
GO
ALTER TABLE [dbo].[frequencia]  WITH CHECK ADD  CONSTRAINT [FK_participante_frequencia] FOREIGN KEY([participante_id])
REFERENCES [dbo].[participante] ([id])
GO
ALTER TABLE [dbo].[frequencia] CHECK CONSTRAINT [FK_participante_frequencia]
GO
ALTER TABLE [dbo].[gasto]  WITH CHECK ADD  CONSTRAINT [FK_gasto_dominio] FOREIGN KEY([fonte_gasto_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[gasto] CHECK CONSTRAINT [FK_gasto_dominio]
GO
ALTER TABLE [dbo].[gasto]  WITH CHECK ADD  CONSTRAINT [FK_gasto_dominio1] FOREIGN KEY([tipo_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[gasto] CHECK CONSTRAINT [FK_gasto_dominio1]
GO
ALTER TABLE [dbo].[gasto]  WITH CHECK ADD  CONSTRAINT [FK_gasto_evento] FOREIGN KEY([evento_id])
REFERENCES [dbo].[evento] ([id])
GO
ALTER TABLE [dbo].[gasto] CHECK CONSTRAINT [FK_gasto_evento]
GO
ALTER TABLE [dbo].[indicacao]  WITH CHECK ADD  CONSTRAINT [FK_evento_indicacao] FOREIGN KEY([evento_id])
REFERENCES [dbo].[evento] ([id])
GO
ALTER TABLE [dbo].[indicacao] CHECK CONSTRAINT [FK_evento_indicacao]
GO
ALTER TABLE [dbo].[indicacao]  WITH CHECK ADD  CONSTRAINT [FK_participante_indicacao] FOREIGN KEY([participante_id])
REFERENCES [dbo].[participante] ([id])
GO
ALTER TABLE [dbo].[indicacao] CHECK CONSTRAINT [FK_participante_indicacao]
GO
ALTER TABLE [dbo].[indicacao]  WITH CHECK ADD  CONSTRAINT [FK_usuario_indicacao] FOREIGN KEY([avaliador_id])
REFERENCES [dbo].[usuario] ([id])
GO
ALTER TABLE [dbo].[indicacao] CHECK CONSTRAINT [FK_usuario_indicacao]
GO
ALTER TABLE [dbo].[indicacao]  WITH CHECK ADD  CONSTRAINT [FK_usuario_indicacao1] FOREIGN KEY([chefe_id])
REFERENCES [dbo].[usuario] ([id])
GO
ALTER TABLE [dbo].[indicacao] CHECK CONSTRAINT [FK_usuario_indicacao1]
GO
ALTER TABLE [dbo].[inscricao]  WITH CHECK ADD  CONSTRAINT [FK_evento_inscricao] FOREIGN KEY([evento_id])
REFERENCES [dbo].[evento] ([id])
GO
ALTER TABLE [dbo].[inscricao] CHECK CONSTRAINT [FK_evento_inscricao]
GO
ALTER TABLE [dbo].[inscricao]  WITH CHECK ADD  CONSTRAINT [FK_indicacao_inscricao] FOREIGN KEY([indicacao_id])
REFERENCES [dbo].[indicacao] ([id])
GO
ALTER TABLE [dbo].[inscricao] CHECK CONSTRAINT [FK_indicacao_inscricao]
GO
ALTER TABLE [dbo].[inscricao]  WITH CHECK ADD  CONSTRAINT [FK_participante_inscricao] FOREIGN KEY([participante_id])
REFERENCES [dbo].[participante] ([id])
GO
ALTER TABLE [dbo].[inscricao] CHECK CONSTRAINT [FK_participante_inscricao]
GO
ALTER TABLE [dbo].[inscricao]  WITH CHECK ADD  CONSTRAINT [FK_usuario_inscricao] FOREIGN KEY([avaliador_id])
REFERENCES [dbo].[usuario] ([id])
GO
ALTER TABLE [dbo].[inscricao] CHECK CONSTRAINT [FK_usuario_inscricao]
GO
ALTER TABLE [dbo].[instrutor]  WITH CHECK ADD  CONSTRAINT [FK_anexo_instrutor] FOREIGN KEY([arquivo_assinatura_id])
REFERENCES [dbo].[anexo] ([id])
GO
ALTER TABLE [dbo].[instrutor] CHECK CONSTRAINT [FK_anexo_instrutor]
GO
ALTER TABLE [dbo].[instrutor]  WITH CHECK ADD  CONSTRAINT [FK_anexo_instrutor1] FOREIGN KEY([arquivo_projeto_id])
REFERENCES [dbo].[anexo] ([id])
GO
ALTER TABLE [dbo].[instrutor] CHECK CONSTRAINT [FK_anexo_instrutor1]
GO
ALTER TABLE [dbo].[instrutor]  WITH CHECK ADD  CONSTRAINT [FK_anexo_instrutor2] FOREIGN KEY([arquivo_curriculo_id])
REFERENCES [dbo].[anexo] ([id])
GO
ALTER TABLE [dbo].[instrutor] CHECK CONSTRAINT [FK_anexo_instrutor2]
GO
ALTER TABLE [dbo].[instrutor]  WITH CHECK ADD  CONSTRAINT [FK_dominio_instrutor] FOREIGN KEY([tipo_Instrutor_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[instrutor] CHECK CONSTRAINT [FK_dominio_instrutor]
GO
ALTER TABLE [dbo].[instrutor]  WITH CHECK ADD  CONSTRAINT [FK_dominio_instrutor1] FOREIGN KEY([formacao_academica_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[instrutor] CHECK CONSTRAINT [FK_dominio_instrutor1]
GO
ALTER TABLE [dbo].[instrutor]  WITH CHECK ADD  CONSTRAINT [FK_dominio_instrutor2] FOREIGN KEY([situacao_instrutor_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[instrutor] CHECK CONSTRAINT [FK_dominio_instrutor2]
GO
ALTER TABLE [dbo].[instrutor]  WITH CHECK ADD  CONSTRAINT [FK_dominio_instrutor3] FOREIGN KEY([nivel_escolaridade_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[instrutor] CHECK CONSTRAINT [FK_dominio_instrutor3]
GO
ALTER TABLE [dbo].[instrutor]  WITH CHECK ADD  CONSTRAINT [FK_endereco_instrutor] FOREIGN KEY([endereco_id])
REFERENCES [dbo].[endereco] ([id])
GO
ALTER TABLE [dbo].[instrutor] CHECK CONSTRAINT [FK_endereco_instrutor]
GO
ALTER TABLE [dbo].[instrutor]  WITH CHECK ADD  CONSTRAINT [FK_pessoa_instrutor] FOREIGN KEY([pessoa_id])
REFERENCES [dbo].[pessoa] ([id])
GO
ALTER TABLE [dbo].[instrutor] CHECK CONSTRAINT [FK_pessoa_instrutor]
GO
ALTER TABLE [dbo].[localizacao_evento]  WITH CHECK ADD  CONSTRAINT [FK_endereco_localizacao_evento] FOREIGN KEY([endereco_id])
REFERENCES [dbo].[endereco] ([id])
GO
ALTER TABLE [dbo].[localizacao_evento] CHECK CONSTRAINT [FK_endereco_localizacao_evento]
GO
ALTER TABLE [dbo].[log]  WITH CHECK ADD  CONSTRAINT [FK_usuario_log] FOREIGN KEY([usuario_id])
REFERENCES [dbo].[usuario] ([id])
GO
ALTER TABLE [dbo].[log] CHECK CONSTRAINT [FK_usuario_log]
GO
ALTER TABLE [dbo].[material]  WITH CHECK ADD  CONSTRAINT [FK_anexo_material] FOREIGN KEY([arquivo_id])
REFERENCES [dbo].[anexo] ([id])
GO
ALTER TABLE [dbo].[material] CHECK CONSTRAINT [FK_anexo_material]
GO
ALTER TABLE [dbo].[material]  WITH CHECK ADD  CONSTRAINT [FK_dominio_material] FOREIGN KEY([tipo_material_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[material] CHECK CONSTRAINT [FK_dominio_material]
GO
ALTER TABLE [dbo].[material]  WITH CHECK ADD  CONSTRAINT [FK_evento_material] FOREIGN KEY([evento_id])
REFERENCES [dbo].[evento] ([id])
GO
ALTER TABLE [dbo].[material] CHECK CONSTRAINT [FK_evento_material]
GO
ALTER TABLE [dbo].[material]  WITH CHECK ADD  CONSTRAINT [FK_modulo_material] FOREIGN KEY([modulo_id])
REFERENCES [dbo].[modulo] ([id])
GO
ALTER TABLE [dbo].[material] CHECK CONSTRAINT [FK_modulo_material]
GO
ALTER TABLE [dbo].[modulo]  WITH CHECK ADD  CONSTRAINT [FK_modulo_evento] FOREIGN KEY([evento_id])
REFERENCES [dbo].[evento] ([id])
GO
ALTER TABLE [dbo].[modulo] CHECK CONSTRAINT [FK_modulo_evento]
GO
ALTER TABLE [dbo].[modulo_instrutor]  WITH CHECK ADD  CONSTRAINT [FK_instrutor_modulo_instrutor] FOREIGN KEY([instrutor_id])
REFERENCES [dbo].[instrutor] ([id])
GO
ALTER TABLE [dbo].[modulo_instrutor] CHECK CONSTRAINT [FK_instrutor_modulo_instrutor]
GO
ALTER TABLE [dbo].[modulo_instrutor]  WITH CHECK ADD  CONSTRAINT [FK_modulo_modulo_instrutor] FOREIGN KEY([modulo_id])
REFERENCES [dbo].[modulo] ([id])
GO
ALTER TABLE [dbo].[modulo_instrutor] CHECK CONSTRAINT [FK_modulo_modulo_instrutor]
GO
ALTER TABLE [dbo].[modulo_instrutor]  WITH CHECK ADD  CONSTRAINT [modulo_instrutor_anexo_FK] FOREIGN KEY([arquivo_id])
REFERENCES [dbo].[anexo] ([id])
GO
ALTER TABLE [dbo].[modulo_instrutor] CHECK CONSTRAINT [modulo_instrutor_anexo_FK]
GO
ALTER TABLE [dbo].[nota]  WITH CHECK ADD  CONSTRAINT [FK_modulo_nota] FOREIGN KEY([modulo_id])
REFERENCES [dbo].[modulo] ([id])
GO
ALTER TABLE [dbo].[nota] CHECK CONSTRAINT [FK_modulo_nota]
GO
ALTER TABLE [dbo].[nota]  WITH CHECK ADD  CONSTRAINT [FK_participante_nota] FOREIGN KEY([participante_id])
REFERENCES [dbo].[participante] ([id])
GO
ALTER TABLE [dbo].[nota] CHECK CONSTRAINT [FK_participante_nota]
GO
ALTER TABLE [dbo].[opcao_questionario]  WITH CHECK ADD  CONSTRAINT [FK_questionario_opcao_questionario] FOREIGN KEY([questionario_id])
REFERENCES [dbo].[questionario] ([id])
GO
ALTER TABLE [dbo].[opcao_questionario] CHECK CONSTRAINT [FK_questionario_opcao_questionario]
GO
ALTER TABLE [dbo].[pacote_alertas]  WITH CHECK ADD  CONSTRAINT [FK_pacote_alertas_dominio] FOREIGN KEY([tipo_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[pacote_alertas] CHECK CONSTRAINT [FK_pacote_alertas_dominio]
GO
ALTER TABLE [dbo].[pacote_alertas]  WITH CHECK ADD  CONSTRAINT [FK_pacote_alertas_evento] FOREIGN KEY([evento_id])
REFERENCES [dbo].[evento] ([id])
GO
ALTER TABLE [dbo].[pacote_alertas] CHECK CONSTRAINT [FK_pacote_alertas_evento]
GO
ALTER TABLE [dbo].[participante]  WITH CHECK ADD  CONSTRAINT [FK_dominio_participante] FOREIGN KEY([formacao_academica_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[participante] CHECK CONSTRAINT [FK_dominio_participante]
GO
ALTER TABLE [dbo].[participante]  WITH CHECK ADD  CONSTRAINT [FK_dominio_participante1] FOREIGN KEY([publico_alvo_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[participante] CHECK CONSTRAINT [FK_dominio_participante1]
GO
ALTER TABLE [dbo].[participante]  WITH CHECK ADD  CONSTRAINT [FK_dominio_participante2] FOREIGN KEY([escolaridade_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[participante] CHECK CONSTRAINT [FK_dominio_participante2]
GO
ALTER TABLE [dbo].[participante]  WITH CHECK ADD  CONSTRAINT [FK_dominio_participante3] FOREIGN KEY([mandato_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[participante] CHECK CONSTRAINT [FK_dominio_participante3]
GO
ALTER TABLE [dbo].[participante]  WITH CHECK ADD  CONSTRAINT [FK_endereco_participante] FOREIGN KEY([endereco_id])
REFERENCES [dbo].[endereco] ([id])
GO
ALTER TABLE [dbo].[participante] CHECK CONSTRAINT [FK_endereco_participante]
GO
ALTER TABLE [dbo].[participante]  WITH CHECK ADD  CONSTRAINT [FK_participante_dominio] FOREIGN KEY([tipo_necessidade_especial_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[participante] CHECK CONSTRAINT [FK_participante_dominio]
GO
ALTER TABLE [dbo].[participante]  WITH CHECK ADD  CONSTRAINT [FK_pessoa_participante] FOREIGN KEY([pessoa_id])
REFERENCES [dbo].[pessoa] ([id])
GO
ALTER TABLE [dbo].[participante] CHECK CONSTRAINT [FK_pessoa_participante]
GO
ALTER TABLE [dbo].[perfil_usuario]  WITH CHECK ADD  CONSTRAINT [FK_usuario_perfil_usuario] FOREIGN KEY([usuario_id])
REFERENCES [dbo].[usuario] ([id])
GO
ALTER TABLE [dbo].[perfil_usuario] CHECK CONSTRAINT [FK_usuario_perfil_usuario]
GO
ALTER TABLE [dbo].[pessoa]  WITH CHECK ADD  CONSTRAINT [FK_dominio_pessoa] FOREIGN KEY([sexo_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[pessoa] CHECK CONSTRAINT [FK_dominio_pessoa]
GO
ALTER TABLE [dbo].[provedor_evento]  WITH CHECK ADD  CONSTRAINT [FK_banco_febraban_provedor_evento] FOREIGN KEY([banco_id])
REFERENCES [dbo].[banco_febraban] ([id])
GO
ALTER TABLE [dbo].[provedor_evento] CHECK CONSTRAINT [FK_banco_febraban_provedor_evento]
GO
ALTER TABLE [dbo].[provedor_evento]  WITH CHECK ADD  CONSTRAINT [FK_endereco_provedor_evento] FOREIGN KEY([endereco_id])
REFERENCES [dbo].[endereco] ([id])
GO
ALTER TABLE [dbo].[provedor_evento] CHECK CONSTRAINT [FK_endereco_provedor_evento]
GO
ALTER TABLE [dbo].[questionario_avaliacao]  WITH CHECK ADD  CONSTRAINT [FK_avaliacao_questionario_avaliacao] FOREIGN KEY([avaliacao_id])
REFERENCES [dbo].[avaliacao] ([id])
GO
ALTER TABLE [dbo].[questionario_avaliacao] CHECK CONSTRAINT [FK_avaliacao_questionario_avaliacao]
GO
ALTER TABLE [dbo].[questionario_avaliacao]  WITH CHECK ADD  CONSTRAINT [FK_instrutor_questionario_avaliacao] FOREIGN KEY([instrutor_id])
REFERENCES [dbo].[instrutor] ([id])
GO
ALTER TABLE [dbo].[questionario_avaliacao] CHECK CONSTRAINT [FK_instrutor_questionario_avaliacao]
GO
ALTER TABLE [dbo].[questionario_avaliacao]  WITH CHECK ADD  CONSTRAINT [FK_opcao_questionario_questionario_avaliacao] FOREIGN KEY([opcao_id])
REFERENCES [dbo].[opcao_questionario] ([id])
GO
ALTER TABLE [dbo].[questionario_avaliacao] CHECK CONSTRAINT [FK_opcao_questionario_questionario_avaliacao]
GO
ALTER TABLE [dbo].[questionario_avaliacao]  WITH CHECK ADD  CONSTRAINT [FK_questionario_questionario_avaliacao] FOREIGN KEY([questionario_id])
REFERENCES [dbo].[questionario] ([id])
GO
ALTER TABLE [dbo].[questionario_avaliacao] CHECK CONSTRAINT [FK_questionario_questionario_avaliacao]
GO
ALTER TABLE [dbo].[usuario]  WITH CHECK ADD  CONSTRAINT [FK_dominio_usuario] FOREIGN KEY([tipo_usuario_id])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[usuario] CHECK CONSTRAINT [FK_dominio_usuario]
GO
ALTER TABLE [dbo].[usuario]  WITH CHECK ADD  CONSTRAINT [FK_participante_usuario] FOREIGN KEY([participante_id])
REFERENCES [dbo].[participante] ([id])
GO
ALTER TABLE [dbo].[usuario] CHECK CONSTRAINT [FK_participante_usuario]
GO
ALTER TABLE [dbo].[usuario]  WITH CHECK ADD  CONSTRAINT [FK_pessoa_usuario] FOREIGN KEY([pessoa_id])
REFERENCES [dbo].[pessoa] ([id])
GO
ALTER TABLE [dbo].[usuario] CHECK CONSTRAINT [FK_pessoa_usuario]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "usu"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 228
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "part"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 279
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "usuint"
            Begin Extent = 
               Top = 6
               Left = 266
               Bottom = 136
               Right = 441
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dom"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dom2"
            Begin Extent = 
               Top = 270
               Left = 246
               Bottom = 400
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pes"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 532
               Right = 217
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ende"
            Begin Extent = 
               Top = 402
               Left = 255
               Bottom = 532
               Right = 425
            End
            DisplayFlags = 280
            TopColumn ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Usuario'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'= 0
         End
         Begin Table = "cid"
            Begin Extent = 
               Top = 534
               Left = 38
               Bottom = 664
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "mun"
            Begin Extent = 
               Top = 534
               Left = 246
               Bottom = 664
               Right = 420
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "org"
            Begin Extent = 
               Top = 666
               Left = 38
               Bottom = 796
               Right = 216
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "est"
            Begin Extent = 
               Top = 666
               Left = 254
               Bottom = 796
               Right = 424
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Usuario'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Usuario'
GO
USE [master]
GO
ALTER DATABASE [SOPHOS] SET  READ_WRITE 
GO
