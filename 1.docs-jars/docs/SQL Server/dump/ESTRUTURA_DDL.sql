USE [master]
GO
/****** Object:  Database [ESTRUTURA]    Script Date: 21/11/2017 15:51:49 ******/
CREATE DATABASE [ESTRUTURA]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ESTRUTURA_DATA', FILENAME = N'E:\Dados\ESTRUTURA_DATA.MDF' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ESTRUTURA_LOG', FILENAME = N'E:\Dados\ESTRUTURA_LOG.LDF' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ESTRUTURA] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ESTRUTURA].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ESTRUTURA] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ESTRUTURA] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ESTRUTURA] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ESTRUTURA] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ESTRUTURA] SET ARITHABORT OFF 
GO
ALTER DATABASE [ESTRUTURA] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ESTRUTURA] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ESTRUTURA] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ESTRUTURA] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ESTRUTURA] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ESTRUTURA] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ESTRUTURA] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ESTRUTURA] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ESTRUTURA] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ESTRUTURA] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ESTRUTURA] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ESTRUTURA] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ESTRUTURA] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ESTRUTURA] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ESTRUTURA] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ESTRUTURA] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ESTRUTURA] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ESTRUTURA] SET RECOVERY FULL 
GO
ALTER DATABASE [ESTRUTURA] SET  MULTI_USER 
GO
ALTER DATABASE [ESTRUTURA] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ESTRUTURA] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ESTRUTURA] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ESTRUTURA] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [ESTRUTURA] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'ESTRUTURA', N'ON'
GO
ALTER DATABASE [ESTRUTURA] SET QUERY_STORE = OFF
GO
USE [ESTRUTURA]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [ESTRUTURA]
GO
/****** Object:  User [usr_estrutura]    Script Date: 21/11/2017 15:51:49 ******/
CREATE USER [usr_estrutura] FOR LOGIN [usr_estrutura] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [usr_estrutura]
GO
/****** Object:  Table [dbo].[Modulo]    Script Date: 21/11/2017 15:51:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Modulo](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[descricao] [varchar](255) NULL,
	[interno] [tinyint] NULL,
	[loginUrl] [varchar](255) NULL,
	[padraoUrl] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Municipio]    Script Date: 21/11/2017 15:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Municipio](
	[codMunicipio] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[cnpjMunicipio] [varchar](255) NULL,
	[nomeMunicipio] [varchar](255) NULL,
	[regiao] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[codMunicipio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orgaos]    Script Date: 21/11/2017 15:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orgaos](
	[codMunicipio] [numeric](19, 0) NOT NULL,
	[codOrgao] [numeric](19, 0) NOT NULL,
	[ativo] [tinyint] NULL,
	[cepLograOrgao] [varchar](255) NULL,
	[descOrgao] [varchar](255) NULL,
	[lograOrgao] [varchar](255) NULL,
	[setorLograOrgao] [varchar](255) NULL,
	[tipoOrgao] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[codMunicipio] ASC,
	[codOrgao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Papel]    Script Date: 21/11/2017 15:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Papel](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[descricao] [varchar](255) NULL,
	[modulo_id] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PapelRegra]    Script Date: 21/11/2017 15:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PapelRegra](
	[papel_id] [numeric](19, 0) NOT NULL,
	[regra_id] [numeric](19, 0) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pessoa]    Script Date: 21/11/2017 15:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pessoa](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[bairro] [varchar](255) NULL,
	[cep] [varchar](255) NULL,
	[cidade] [varchar](255) NULL,
	[cpf] [varchar](255) NULL,
	[dataNascimento] [datetime] NULL,
	[email] [varchar](255) NULL,
	[identidade] [varchar](255) NULL,
	[logradouro] [varchar](255) NULL,
	[nome] [varchar](255) NULL,
	[nomeMae] [varchar](255) NULL,
	[orgaoExpedidor] [varchar](255) NULL,
	[sexo] [varchar](255) NULL,
	[uf] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PessoaTelefone]    Script Date: 21/11/2017 15:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PessoaTelefone](
	[pessoa_id] [numeric](19, 0) NOT NULL,
	[telefone_id] [numeric](19, 0) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Regra]    Script Date: 21/11/2017 15:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Regra](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[descricao] [varchar](255) NULL,
	[rotulo] [varchar](255) NULL,
	[modulo_id] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Secao]    Script Date: 21/11/2017 15:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Secao](
	[CodSecao] [varchar](255) NOT NULL,
	[AbrevSecao] [varchar](255) NULL,
	[Situacao] [varchar](255) NULL,
	[DescrSecao] [varchar](255) NULL,
	[Distribuir] [tinyint] NULL,
	[FazTramitacao] [tinyint] NULL,
	[PrazoAnalise] [smallint] NULL,
	[SenhaSecao] [varchar](255) NULL,
	[TpSecao] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[CodSecao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Telefone]    Script Date: 21/11/2017 15:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Telefone](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ddd] [varchar](255) NULL,
	[telefone] [varchar](255) NULL,
	[tipo] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 21/11/2017 15:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario](
	[UsuarioID] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[Situacao] [tinyint] NULL,
	[SecaoID] [varchar](255) NULL,
	[EMail] [varchar](255) NULL,
	[Master] [varchar](255) NULL,
	[Matricula] [varchar](255) NULL,
	[NomeCompleto] [varchar](255) NULL,
	[NomeCriador] [varchar](255) NULL,
	[NomeUsuario] [varchar](255) NULL,
	[Observacao] [varchar](255) NULL,
	[ResetSenha] [tinyint] NULL,
	[senha] [varchar](255) NULL,
	[tipoUsuarioID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[UsuarioID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsuarioChefeSecao]    Script Date: 21/11/2017 15:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuarioChefeSecao](
	[secaoID] [numeric](19, 0) NOT NULL,
	[usuarioID] [numeric](19, 0) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[secaoID] ASC,
	[usuarioID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsuarioExterno]    Script Date: 21/11/2017 15:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuarioExterno](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ativoWeb] [tinyint] NULL,
	[cargo] [varchar](255) NULL,
	[dataCriacao] [datetime] NULL,
	[login] [varchar](255) NULL,
	[observacao] [varchar](255) NULL,
	[senha] [varchar](255) NULL,
	[pessoa_id] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsuarioPapelModulo]    Script Date: 21/11/2017 15:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuarioPapelModulo](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[tipoUsuario] [varchar](255) NULL,
	[usuarioId] [numeric](19, 0) NULL,
	[modulo_id] [numeric](19, 0) NULL,
	[papel_id] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Orgaos]  WITH CHECK ADD  CONSTRAINT [FK8D459CC11AA7141D] FOREIGN KEY([codMunicipio])
REFERENCES [dbo].[Municipio] ([codMunicipio])
GO
ALTER TABLE [dbo].[Orgaos] CHECK CONSTRAINT [FK8D459CC11AA7141D]
GO
ALTER TABLE [dbo].[Papel]  WITH CHECK ADD  CONSTRAINT [FK49521667F33D32E] FOREIGN KEY([modulo_id])
REFERENCES [dbo].[Modulo] ([id])
GO
ALTER TABLE [dbo].[Papel] CHECK CONSTRAINT [FK49521667F33D32E]
GO
ALTER TABLE [dbo].[PapelRegra]  WITH CHECK ADD  CONSTRAINT [FK9338CFFD22EA9174] FOREIGN KEY([papel_id])
REFERENCES [dbo].[Papel] ([id])
GO
ALTER TABLE [dbo].[PapelRegra] CHECK CONSTRAINT [FK9338CFFD22EA9174]
GO
ALTER TABLE [dbo].[PapelRegra]  WITH CHECK ADD  CONSTRAINT [FK9338CFFDB24A0FB] FOREIGN KEY([regra_id])
REFERENCES [dbo].[Regra] ([id])
GO
ALTER TABLE [dbo].[PapelRegra] CHECK CONSTRAINT [FK9338CFFDB24A0FB]
GO
ALTER TABLE [dbo].[PessoaTelefone]  WITH CHECK ADD  CONSTRAINT [FKD31313D17FE627B9] FOREIGN KEY([telefone_id])
REFERENCES [dbo].[Telefone] ([id])
GO
ALTER TABLE [dbo].[PessoaTelefone] CHECK CONSTRAINT [FKD31313D17FE627B9]
GO
ALTER TABLE [dbo].[PessoaTelefone]  WITH CHECK ADD  CONSTRAINT [FKD31313D1AAC81659] FOREIGN KEY([pessoa_id])
REFERENCES [dbo].[Pessoa] ([id])
GO
ALTER TABLE [dbo].[PessoaTelefone] CHECK CONSTRAINT [FKD31313D1AAC81659]
GO
ALTER TABLE [dbo].[Regra]  WITH CHECK ADD  CONSTRAINT [FK4B301A37F33D32E] FOREIGN KEY([modulo_id])
REFERENCES [dbo].[Modulo] ([id])
GO
ALTER TABLE [dbo].[Regra] CHECK CONSTRAINT [FK4B301A37F33D32E]
GO
ALTER TABLE [dbo].[UsuarioExterno]  WITH CHECK ADD  CONSTRAINT [FKCB2655E1AAC81659] FOREIGN KEY([pessoa_id])
REFERENCES [dbo].[Pessoa] ([id])
GO
ALTER TABLE [dbo].[UsuarioExterno] CHECK CONSTRAINT [FKCB2655E1AAC81659]
GO
ALTER TABLE [dbo].[UsuarioPapelModulo]  WITH CHECK ADD  CONSTRAINT [FK2EF92FAE22EA9174] FOREIGN KEY([papel_id])
REFERENCES [dbo].[Papel] ([id])
GO
ALTER TABLE [dbo].[UsuarioPapelModulo] CHECK CONSTRAINT [FK2EF92FAE22EA9174]
GO
ALTER TABLE [dbo].[UsuarioPapelModulo]  WITH CHECK ADD  CONSTRAINT [FK2EF92FAE7F33D32E] FOREIGN KEY([modulo_id])
REFERENCES [dbo].[Modulo] ([id])
GO
ALTER TABLE [dbo].[UsuarioPapelModulo] CHECK CONSTRAINT [FK2EF92FAE7F33D32E]
GO
USE [master]
GO
ALTER DATABASE [ESTRUTURA] SET  READ_WRITE 
GO
