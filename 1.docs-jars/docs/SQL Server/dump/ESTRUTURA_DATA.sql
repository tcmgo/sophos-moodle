USE [ESTRUTURA]
GO
SET IDENTITY_INSERT [dbo].[Municipio] ON 

INSERT [dbo].[Municipio] ([codMunicipio], [cnpjMunicipio], [nomeMunicipio], [regiao]) VALUES (CAST(1 AS Numeric(19, 0)), N'01612092000123', N'Goiânia', NULL)
SET IDENTITY_INSERT [dbo].[Municipio] OFF
INSERT [dbo].[Secao] ([CodSecao], [AbrevSecao], [Situacao], [DescrSecao], [Distribuir], [FazTramitacao], [PrazoAnalise], [SenhaSecao], [TpSecao]) VALUES (N'1', N'SINFO', N'1', N'Superintendência de Informática', NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Usuario] ON 

INSERT [dbo].[Usuario] ([UsuarioID], [Situacao], [SecaoID], [EMail], [Master], [Matricula], [NomeCompleto], [NomeCriador], [NomeUsuario], [Observacao], [ResetSenha], [senha], [tipoUsuarioID]) VALUES (CAST(1 AS Numeric(19, 0)), 1, N'1', N'michel@tce.sc.gov.br', N'S', NULL, N'ADMINISTRADOR', N'', N'admin', N'Cadastro para login inicial', 0, N'E10ADC3949BA59ABBE56E057F20F883E', 1)
SET IDENTITY_INSERT [dbo].[Usuario] OFF
