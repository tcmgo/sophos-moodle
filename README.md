# README #

Este README esclarece, de forma sucinta, as tecnologias, a arquitetura e passos preliminares para a configuração e execução do sistema de gestão educacional Sophos do TCM-GO. 

O Sophos em ambiente de produção do TCM-GO pode ser visitado em: www.tcm.go.gov.br/sophos.

**1) Tecnologia**

Java 7, Spring, Java Server Faces (JSF)/Primefaces, Java Persistence API (JPA)/Hibernate, Jasper Reports, [Material Design Lite](https://getmdl.io/) (MDL) e [Materialize](http://materializecss.com/).

**2) Arquietura**

A Arquitetura do Sophos é composta, na visão macro, pelos projetos Sophos e TCMEstruturaWeb. O primeiro possui o código-fonte relativo ao Sophos propriamente dito, cujo escopo é a gestão educacional em ambiente corporativo. O TCMEstruturaWeb, por sua vez, possui arquitetura base para execução de diversos projetos web do TCM-GO. Este último é uma dependência para o primeiro. Em ambos os projetos adota-se a arquitetura [MVC](https://pt.wikipedia.org/wiki/MVC) (Model-View-Controller), bem como conceitos de [Injeção de Dependências](https://pt.wikipedia.org/wiki/Inje%C3%A7%C3%A3o_de_depend%C3%AAncia), [DTO](https://pt.wikipedia.org/wiki/Objeto_de_Transfer%C3%AAncia_de_Dados) (Data Transfer Object) e [DAO](https://pt.wikipedia.org/wiki/Objeto_de_acesso_a_dados) (Data Access Object). A gestão de dependências dos projetos é orquestrada pelo [Apache Maven](https://pt.wikipedia.org/wiki/Apache_Maven). 

**2.1. Sophos**

O propósito e relações dos pacotes dos Sophos são explicados sucintamente a seguir. Detalhes das dependências deste projeto podem ser conferidos em seu respectivo pom.xml.

**2.1.1. br.gov.go.tcm.sophos.ajudante**

Este pacote possui classes utilitárias para todo o projeto. Possuem propósitos diversos tais como tratamento de imagens, e-mails, relatórios, objetos, etc.

**2.1.2. br.gov.go.tcm.sophos.controladores**

Neste pacote estão os controllers do modelo MVC.

**2.1.3. br.gov.go.tcm.sophos.controladores.base**

Neste pacote estão presentes classes abstratas que os controles do projeto estendem. Essas classes abstratas possuem dados e comportamentos comuns a todos controllers.

**2.1.4. br.gov.go.tcm.sophos.conversor**

Neste pacote estão implementados converters utilizados pelo JSF nas páginas XHTML do projeto.

**2.1.5. br.gov.go.tcm.sophos.entidade**

Aqui estão presentes as classes que mapeiam, via JPA/Hibernate, as tabelas de banco de dados do Sophos. Dados de acesso ao banco de dados devem estar mapeados nos arquivos *contextoPersistencia.xml*. Estas classes representam classes do Model dentro da arquitetura MVC.

**2.1.6. br.gov.go.tcm.sophos.entidade.base**

Neste pacote estão presentes classes que uma ou mais entidades do projeto estendem. Essas classes possuem dados e comportamentos comuns a todas entidades.

**2.1.7. br.gov.go.tcm.sophos.entidade.dto**

Neste pacote estão as classes DTO (Data Transfer Object) do projeto. Estas classes também representam classes do Model dentro da arquitetura MVC.

**2.1.8. br.gov.go.tcm.sophos.entidade.enumeracao**

Aqui estão presentes Enums utilizados ao longo do projeto.

**2.1.9. br.gov.go.tcm.sophos.excecoes**

Este pacote possui apenas uma classe pertencente a estrutura de validação e lançamento de exceções no projeto.

**2.1.10. br.gov.go.tcm.sophos.filter**

Este pacote possui apenas uma classe pertencente a estrutura de filtragem de requisições quando da execução do projeto.

**2.1.11. br.gov.go.tcm.sophos.lazy**

Este é um pacote muito importante para a escalabilidade e desempenho do Sophos. Possui implementações de carregamento e listagens de itens em datagrids de forma paginada. Quando comportamentos específicos (filtragens, etc) precisam ser realizadas estas classes sobrescrevem o método *List<E> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters)* da classe *TableLazyPadrao*.

**2.1.12. br.gov.go.tcm.sophos.lazy.base**

Neste pacote estão presentes classes que as classes Lazy extendem para realizar a paginação de elementos na View. Essas classes possuem dados e comportamentos comuns a todas essas classes lazy tais como os atributos* List<E> lista* e *String consulta*.

**2.1.13. br.gov.go.tcm.sophos.listener**

Este pacote possui apenas uma classe pertencente a estrutura de regitro de logs do sistema.

**2.1.14. br.gov.go.tcm.sophos.mensagem**

Este pacote possui apenas um arquivo .properties que possuem mensagens que podem programaticamnte, via Controllers, serem lançadas na View. Cabe ressaltar que nem todas mensagens do sistema estão mapeadas neste arquivo, pois muitas deles estão explicítas nos Controllers. 

**2.1.15. br.gov.go.tcm.sophos.negocio**

Este pacote possui classes de negócio do projeto largamente utilizadas pelos controladores. As classes de negócio são responsáveis por lidar com regras de negócio e invocar os DAOs (Data Acces Object) nas tarefas que envolvam persistência de dados (inserir, atualizar, remover e obter). Em regra os controladores não devem acessar os DAOs diretamente, mas apenas através das classes de negócio contidas neste pacote. 

**2.1.16. br.gov.go.tcm.sophos.negocio.base**

Neste pacote está a classe de negócio base estendida por todas classes de negócio, portanto detentora de atributos e comportamentos comuns a todas elas.

**2.1.17. br.gov.go.tcm.sophos.persistencia**

Neste pacote estão presentes as interfaces dos DAOs do projeto. Portanto, são classes que apresentam apenas os contratos que devem ser obervados pelos DAOs.

**2.1.18. br.gov.go.tcm.sophos.persistencia.base**

Neste pacote está a interface de DAO base estendida por todas interfaces DAOs, portanto detentora de atributos comuns a todas elas.

**2.1.19. br.gov.go.tcm.sophos.persistencia.impl**

Aqui estão presentes as implmentações das interfaces DAOs. Portanto, apresentam, de fato, métodos para persistência de dados, especialmente consultas JPQL para acesso mais aprimorado à dados quando necessário.

**2.1.20. br.gov.go.tcm.sophos.persistencia.impl.base**

Este pacote possui o DAO genérico utilizado por todas classes DAO de implementação (pacote da seção anterior). A obtenção do SessionFactory e métodos genéricos para busca, inserção, edição e remoção de dados estão implementados nesta classe.  

**2.1.21. br.gov.go.tcm.sophos.rh**

Este pacote agrega classes para integração com banco de dados de RH externo ao Sophos. 

**2.2. TCMEstruturaWeb**

Este projeto é a base para execução do Sophos e está assentado principalmente no framework Spring. Muitas das classes do projeto Sophos implementam interfaces, estendem ou utilizam classes deste projeto. Através da JPA algumas tabelas em bancos de dados também são criadas (caso ainda não existam) a partir de entidades deste projeto. Citamos a entidade *Orgao.java* e *Municipio.java* (package *br.gov.go.tcm.estrutura.entidade.orcafi*). O TCMEstruturaWeb depositado neste repositório não possui todas classes do projeto original utilizado pelo TCM-GO, incluindo apenas aquelas estritamente necessárias à execução do Sophos. Detalhes das dependências deste projeto podem ser conferidos em seu respectivo pom.xml.

Neste documento não nos desdobraremos na explicação de todos pacotes deste projeto. 

**3) Configuração e Execução**

**3.1. Banco de Dados**

Os detalhes de acesso a banco de dados no Sophos são configurados nos arquivos contextoPersistencia.xml do projeto Sophos. No projeto temos três deles em locais distintos:

a)/conf-homologa/contextoPersistencia.xml: considerado quando o contexto do Maven é homologacao

b)/conf-server/contextoPersistencia.xml: considerado quando o contexto do Maven é producao

c)/webapp/WEB-INF/spring-config/contextoPersistencia.xml: considerado por default

Sugerimos a configuração de todos estes arquivos. Neles são configurados todos os bancos de dados acessados pelo Sophos. A propriedade id dos beans ali presentes são os nomes das Sessions Factories/HibernateTemplates utilizados e indicadas nos contrutores dos DAOs implementados através da anotação @Qualifier. 

A seguir os trechos que devem ser configurados para cada banco de dados a ser utilizado:

```
#!xml


<bean id="sessionFactoryOracleRH" class="org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean"
		destroy-method="destroy">
		<property name="configurationClass" value="org.hibernate.cfg.AnnotationConfiguration" />
		<property name="packagesToScan" value="br.gov.go.tcm.sophos.rh.*" />
		<property name="hibernateProperties">
			<props>
				<prop key="hibernate.show_sql">false</prop>
				<prop key="hibernate.bytecode.use_reflection_optimizer">true</prop>
				<prop key="hibernate.generate_statistics">true</prop>
				<prop key="hibernate.cache.provider_class">org.hibernate.cache.HashtableCacheProvider</prop>
				<prop key="hibernate.cache.use_query_cache">true</prop>
				<prop key="hibernate.hbm2ddl.auto">none</prop>
				<prop key="hibernate.connection.driver_class">oracle.jdbc.driver.OracleDriver</prop>
				<prop key="hibernate.connection.url"></prop>
				<prop key="hibernate.connection.username"></prop>
				<prop key="hibernate.connection.password"></prop>
				<prop key="hibernate.connection.autocommit">true</prop>
				<prop key="hibernate.dialect">org.hibernate.dialect.Oracle10gDialect</prop>
				<prop key="hibernate.archive.autodetection">class</prop>
				<prop key="hibernate.connection.provider_class">org.hibernate.connection.C3P0ConnectionProvider</prop>
				<prop key="hibernate.c3p0.acquire_increment">5</prop>
				<prop key="hibernate.c3p0.idle_test_period">100</prop>
				<prop key="hibernate.c3p0.min_size">5</prop>
				<prop key="hibernate.c3p0.max_size">20</prop>
				<prop key="hibernate.c3p0.max_statements">0</prop>
				<prop key="hibernate.c3p0.timeout">0</prop>
				<prop key="hibernate.current_session_context_class">managed</prop>
			</props>
		</property>
 </bean>

 <bean id="txManagerOracleRH" class="org.springframework.orm.hibernate3.HibernateTransactionManager">
        <property name="sessionFactory" ref="sessionFactoryOracleRH" />        
 </bean>

 <bean id="hibernateTemplateOracleRH" class="org.springframework.orm.hibernate3.HibernateTemplate">
		<property name="sessionFactory" ref="sessionFactoryOracleRH" />
 </bean>
```



A seguir um exemplo de utilização destas configurações nas classes DAO de implementação:


	@Autowired
	public RHPessoaDAOImpl(@Qualifier("sessionFactoryOracleRH") final SessionFactory factory, @Qualifier("hibernateTemplateOracleRH") 
        final HibernateTemplate hibernateTemplate) {

		this.setHibernateTemplate(hibernateTemplate);

		this.setSessionFactory(factory);
		
        }

Na pasta docs do repositório do projeto encontra-se exemplos de arquivos contextoPersistencia.xml para banco de dados PostgreSQL e SQL Server. Nesta pasta também estão presentes scripts para dumps dos banco de dados estrutura e sophos.
Por padrão o Sophos presente no repositório está configurado para PostgreSQL.

**3.2. Configurações Gerais**

Configurações gerais também são configuradas nos arquivos contextoGeral.xml e contextoConfiguracao.xml. 

No primeiro configura-se, entre outros detalhes, dados de servidores SMTP para envio de e-mails e e-mails padrões utilizados. No segundo, por sua vez, configura-se caminhos dos diretórios do ambiente de armazenamento de arquivos utilizado (GED/FileSystem) e das imagens frente e verso dos certificados emitidos pelo sistema. 

Estes arquivos, semelhantemente aos arquivos de configuração de persistência, figuram em três locais diferentes no projeto:

a)/conf-homologa/

b)/conf-server/

c)/webapp/WEB-INF/spring-config/

**3.3. Buiid dos Projetos**

Para gerar uma build do projeto deve-se fazer a importação do projeto no Eclipse (ou outra IDE favorita) e realizar Maven Clean (No Eclipse: Run As - Maven Clean) e Maven Install (No Eclipse: Run As - Maven Install) no TCMServicosWeb e, logo em seguida, no Sophos. A ordem é necessária dada a dependência do segundo para o primeiro.  

É uma premissa para o build com sucesso a configuração dos projetos para rodar com o JDK 7. 

**3.4. Execução do Sophos**

A execução do Sophos pode ser realizada no servidor Web Java de sua preferência, implemente ele ou não toda especificação Java EE. Todavia, sugerimos a utilização do Tomcat7 rodando sobre JDK7.

**3.5. Carga de dados inicial do Sophos**

Os scripts de dump dos bancos de dados estrutura e sophos presente na pasta docs do repositório do Sophos permite a criação da estrutura de tabelas necessárias, bem como a carga inicial de dados necessários. 
Uma vez estes scripts executados será possível autenticar no Sophos com usuário admin e senha 123456.

A carga de dados de usuários internos (membros e servidores) deve ser realizada na tabela Usuario do banco de dados estrutura. Apontamentos para estes registros devem ser realizados no campo usuario_id da tabela usuario 
do banco de dados sophos. Para estes casos a senha MD5 presente no campo senha da tabela usuario do banco sophos é desconsiderada, valendo o campo senha MD5 da tabela Usuario do banco estrutura. Para estes usuários internos (membros e servidores)
o campo municipio_id da tabela endereco do banco de dados sophos deve fazer apontamento para a tabela Municipio do banco de dados estrutura, responsável por armazenar municípios do estado da instituição (no caso do TCM, os municípios do Estado de Goiás).
Ou seja, para usuários internos há dois registros de usuario: um no banco de dados sophos e outro no banco de dados estrutura, este último com dados de senha a serem considerados pela aplicação.

Para usuários internos (membros e servidores) a tabela usuario do banco de dados sophos faz, através do campo codSecao, apontamento para a tabela Secao do banco de dados estrutura. Esta tabela é responsável por armazenar todos os setores internos da instituição.
Apontamento semelhante também é realizado da tabela Municipio para a tabela Secao, ambas do banco de dados estrutura.

A carga de dados de usuários externos (jurisdicionados e sociedade) deve ser realizada diretamente na tabela usuario do banco de dados sophos. Para estes casos a senha MD5 presente no campo senha da tabela usuario do banco sophos é considerada e a cidade
do usuário externo (jurisdicionados e sociedade) deve ser indicada no campo cidade_id da tabela endereco. Este faz apontamento para a tabela cidade do banco de dados sophos. Ou seja, no caso de servidores externos o banco de dados estrutura não é utilizado para persistência de dados.

Estas distinções, embora à primeira vista de cárater mais complexo, são necessárias dada a arquitetura de sistemas do TCMGO, onde o projeto estrutura e seu respectivo banco de dados possuem função centralizadora de dados e funcionalidades comuns à vários sistemas institucionais.

**3.6. View de dados de Recursos Humanos (RH)**

O Sophos faz a leitura de uma view de dados de recursos humanos disponibilizada por empresa terceirizada responsável pela gestão da folha de pagamento no TCMGO.
No contexto do TCMGO esta base de dados está sobre o SGBD Oracle e possui a estrutura do arquivo campos_view.png presente na pasta docs/View RH Oracle no repositório do projeto. 
Para ambientes onde está view não exista, indica-se a criação de view com campos idênticos aos descritos no arquivo campos_view.png, no SGBD a escolha da equipe de desenvolvimento. 
Dessa forma, o Sophos estará apto a buscar dados de servidores quando do cadastro, o que inclui dados de endereço, lotação cargo, etc. O arquivo de código-fonte RHPessoaDAOImpl.java 
deve fazer menção correta ao nome dessa view.

**3.7. Arquivos de estilos (CSS)**

Na pasta "css" do repositório há duas versões de arquivos de estilos (CSS) do Sophos, cujo nome é styles.css. O arquivo styles.css desejado deve ser copiada para o caminho src/main/webapp/resources/css do projeto sopphos, substituindo o ali presente. 
O arquivo styles.css da pasta "CSS Padrao" possui as cores padrões do Sophos (tonalidades em verde), usado pelo TCMGO. O arquivo styles.css da pasta "CSS MPGO e TCESC" possui as cores personalizadas em tonalidades de vermelhos, adequadas para uso pelo MPGO e TCESC.
Este último é o arquivo CSS utilizado por padrão no Sophos presente no repositório.

**3.8. Desafios de Configuração e Execução do Sophos em Ambiente Distinto ao TCM-GO**

Apontamos como maiores desafios na configuração e execução do Sophos em ambiente distinto do TCM-GO:

a) Alto grau de acoplamento do Sophos com o TCMServicosWeb, sendo este último um projeto estritamente ligado a arquitetura de dados/software e outras peculiaridades do TCM-GO . Ressalta-se, inclusive, que muitas classes foram retiradas e/ou movidas para diminuir essa peculiaridade e acoplamento.

b) Ajuste para o Sophos utilizar de tabelas de banco de dados centrais e genéricas da instituição de destino. Citamos, neste caso, as tabelas referentes a municípios, órgãos, usuários, pessoas, etc. 

c) Integração com arquitetura de autenticação distinta à praticada pelo TCM-GO, tais como oAuth, LDAP, outras tabelas de BD, etc. O Sophos contido neste repositório possui estrutura para armazenar usuários próprios utilizados na autenticação, bem como recursos para acessar usuários contidos em outras tabelas de mesmo propósito externas ao Sophos.

d) Configuração de outras tecnologias e esquemas de bancos de dados.

e) Ajustes para descartar regras de negócio não aplicáveis ou incluir regras de negócios ainda não contempladas dadas as diferenças naturais entre instituições que possam utilizá-lo.